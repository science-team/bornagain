---
#
# This is the BornAgain configuration for clang-tidy.
#
# To invoke clang-tidy, run
#   setclang
#   cmake -DBA_TIDY=ON -DBORNAGAIN_PYTHON=OFF -DBA_GUI=OFF ..
#   make
#
# Below, we select all checks ('*'), then deselect quite a number of them.
#
# As we are not aware of an official way to insert comments in a long string literal,
# we do a dirty little trick: we write comments as if they were no-check specifiers.
#
Checks: '*,


-SectionComment_We_disagree_with_the_following_checks__They_shall_remain_permanently_disabled,

-*-braces-around-statements,
-*-convert-member-functions-to-static,
-*-implicit-bool-conversion,
-*-magic-numbers,
-*-named-parameter,
-*-narrowing-conversions,
-*-nodiscard,
-*-special-member-functions,
-*-trailing-return*,
-*-uppercase-literal-suffix,
-abseil-*,
-altera-id-dependent-backward-branch,
-altera-struct-pack-align,
-altera-unroll-loops,
-bugprone-branch-clone,
-bugprone-easily-swappable-parameters,
-bugprone-implicit-widening-of-multiplication-result,
-bugprone-suspicious-include,
-cert-err58-cpp,
-cert-err61-cpp,
-cert-msc30-c*,
-cert-msc32-c,
-cert-msc50-cpp,
-cert-msc51-cpp,
-clang-analyzer-alpha*,
-clang-analyzer-alpha.deadcode.UnreachableCode,
-clang-analyzer-security.insecureAPI.strcpy,
-cppcoreguidelines-init-variables,
-cppcoreguidelines-macro-usage,
-cppcoreguidelines-non-private-member-variables-in-classes,
-cppcoreguidelines-pro-bounds-array-to-pointer-decay,
-cppcoreguidelines-pro-bounds-constant-array-index,
-cppcoreguidelines-pro-bounds-pointer-arithmetic,
-cppcoreguidelines-pro-type-cstyle-cast,
-cppcoreguidelines-pro-type-member-init,
-cppcoreguidelines-pro-type-reinterpret-cast,
-cppcoreguidelines-pro-type-vararg,
-cppcoreguidelines-slicing,
-fuchsia-default-arguments-calls,
-fuchsia-default-arguments-declarations,
-fuchsia-overloaded-operator,
-fuchsia-statically-constructed-objects,
-fuchsia-trailing-return,
-google-build-using-namespace,
-google-default-arguments,
-google-explicit-constructor,
-google-readability-avoid-underscore-in-googletest-name,
-google-readability-casting,
-google-readability-todo,
-google-runtime-int,
-hicpp-exception-baseclass,
-hicpp-explicit-conversions,
-hicpp-member-init,
-hicpp-no-array-decay,
-hicpp-noexcept-move,
-hicpp-signed-bitwise,
-hicpp-vararg,
-llvm-include-order,
-llvmlibc-callee-namespace,
-llvmlibc-implementation-in-namespace,
-llvmlibc-restrict-system-libc-headers,
-misc-no-recursion,
-misc-non-private-member-variables-in-classes,
-misc-throw-by-value-catch-by-reference,
-performance-faster-string-find,
-performance-inefficient-string-concatenation,
-performance-no-automatic-move,
-performance-noexcept-move-constructor,
-performance-unnecessary-value-param,
-readability-identifier-length,
-readability-misleading-indentation,
-readability-simplify-boolean-expr,
-readability-use-anyofallof,


-SectionComment_Broken,

-google-runtime-references,


-SectionComment_To_be_manually_checked_from_time_to_time,

-*-move-const-arg,
-bugprone-parent-virtual-call,
-cppcoreguidelines-avoid-non-const-global-variables,
-modernize-use-default-member-init,
-performance-inefficient-vector-operation,
-performance-unnecessary-copy-initialization,
-readability-isolate-declaration,
-readability-redundant-member-init,


-SectionComment_Too_slow_for_frequent_use,

-cppcoreguidelines-pro-type-static-cast-downcast,
-modernize-raw-string-literal,


-SectionComment_Disabled_unless_3rdparty_libraries_are_improved,

-*avoid-goto,
-clang-analyzer-cplusplus.NewDeleteLeaks,
-cppcoreguidelines-avoid-non-const-global-variables,
-cppcoreguidelines-pro-bounds-array-to-pointer-decay,
-hicpp-no-array-decay,
-llvm-namespace-comment,
-readability-redundant-access-specifiers,


-SectionComment_Resolving_the_following_checks_would_be_too_much_work_right_now,

-*avoid-c-arrays,
-cppcoreguidelines-prefer-member-initializer,
-cppcoreguidelines-owning-memory,
-bugprone-parent-virtual-call,


-SectionComment_Temporarily_disabled_checks__We_need_to_investigate_them_one_by_one,

-bugprone-copy-constructor-init,
-bugprone-exception-escape,
-bugprone-misplaced-widening-cast,
-clang-analyzer-core.CallAndMessage,
-clang-analyzer-optin.cplusplus.VirtualCall,
-cppcoreguidelines-pro-type-const-cast,
-readability-function-cognitive-complexity,


-SectionComment_Automizable__To_be_kept_satisfied,

*-use-auto,
*-use-emplace,
*-use-equals-default,
*-use-nullptr,
*-use-override,
bugprone-unused-return-value,
cppcoreguidelines-explicit-virtual-functions,
google-readability-avoid-underscore-in-googletest-name,
llvm-qualified-auto,
misc-uniqueptr-reset-release,
modernize-avoid-bind,
modernize-loop-convert,
modernize-make-unique,
modernize-pass-by-value,
modernize-raw-string-literal,
modernize-use-using,
performance-for-range-copy,
readability-avoid-const-params-in-decls,
readability-const-return-type,
readability-non-const-parameter,
readability-container-size-empty,
readability-delete-null-pointer,
readability-inconsistent-declaration-parameter-name,
readability-qualified-auto,


'
# keep the closing quotation mark