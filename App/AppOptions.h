//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      App/AppOptions.h
//! @brief     Collection of utilities to parse command line options
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_APP_APPOPTIONS_H
#define BORNAGAIN_APP_APPOPTIONS_H

#include <QCommandLineParser>
#include <QSize>
#include <QString>

//! Handles command line and config file program options
class ApplicationOptions {
public:
    ApplicationOptions(int argc = 0, char** argv = nullptr);

    QSize mainWindowSize() const;

    QString projectFile() const;

    //! Returns true if option with the given name has been set
    bool find(const QString& name) const;

private:
    QCommandLineParser m_parser;
    QString m_geometryValue;
    QString m_projectFileValue;
};

#endif // BORNAGAIN_APP_APPOPTIONS_H
