//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      App/main.cpp
//! @brief     Main function of the whole GUI
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "App/AppOptions.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include "GUI/Model/Util/Path.h"
#include "GUI/View/Base/Stylesheet.h"
#include "GUI/View/Main/MainWindow.h"
#include "GUI/View/Widget/AppConfig.h"
#include "config_build.h"

#ifdef BORNAGAIN_PYTHON
#include "PyCore/Embed/PyInterpreter.h" // PyInterpreter::finalize
#endif                                  // BORNAGAIN_PYTHON

#include <QApplication>
#include <QDir>
#include <QIcon>
#include <QLocale>
#include <QMessageBox>
#include <QMetaType>
#include <QtGlobal>
#include <cstdlib>
#include <iostream>


void custom_terminate_handler()
{
    try {
        std::rethrow_exception(std::current_exception());
    } catch (const std::exception& ex) {
        std::cerr << "terminate called after throwing a std exception'\n"
                  << "what():" << std::endl;
        std::cerr << ex.what() << std::endl;

        QMessageBox msgbox(QMessageBox::Critical, "BornAgain: fatal bug",
                           QString("Sorry, you encountered a fatal bug.\n"
                                   "The application will terminate.\n"
                                   "Please note the following and inform the maintainers.\n"
                                   "---\n"
                                   "BornAgain version: ")
                               + GUI::Path::getBornAgainVersionString() + "\n"
                               + QString("Internal error message:\n") + ex.what()
                               + "\n---\n"
                                 "Please help us to fix this bug by reporting the above\n"
                                 "- at https://jugit.fz-juelich.de/mlz/bornagain/-/issues/new\n"
                                 "- or by mail to contact@bornagainproject.org.\n"
                                 "See also\n"
                                 "- https://bornagainproject.org/latest/howto/get-help.",
                           QMessageBox::Ok, nullptr);
        msgbox.exec();
    }
    std::abort();
}

int main(int argc, char* argv[])
{
    std::set_terminate(custom_terminate_handler);

    ApplicationOptions options(argc, argv);

    QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
    qRegisterMetaType<QVector<double>>("QVector<double>");

    int ret = -1;
    QApplication app(argc, argv);
    app.setApplicationName("BornAgain");
    app.setApplicationVersion(GUI::Path::getBornAgainVersionString());
    app.setOrganizationName("BornAgain");
#ifndef Q_OS_MAC
    app.setWindowIcon(QIcon(":/images/BornAgain.ico"));
#endif
    GUI::Style::setInitialStyle();

    gApp = std::make_unique<AppConfig>();
    gDoc = std::make_unique<ProjectDocument>();

    MainWindow win;
    gApp->mainWindow = &win;

    if (options.find("geometry"))
        win.resize(options.mainWindowSize());
    if (options.find("project-file"))
        win.loadProject(options.projectFile());
    win.show();

    ret = app.exec();

#ifdef BORNAGAIN_PYTHON
#ifdef BA_DEBUG
    std::cout << "BornAgain: finalize Python interpreter..." << std::endl;
#endif // BA_DEBUG
    PyInterpreter::finalize();
#endif // BORNAGAIN_PYTHON

    return ret;
}
