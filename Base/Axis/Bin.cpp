//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Axis/Bin.cpp
//! @brief     Implements class Bin1D.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Axis/Bin.h"
#include "Base/Util/Assert.h"
#include <sstream>
#include <stdexcept>

Bin1D Bin1D::FromTo(double lower, double upper)
{
    return {lower, upper};
}

Bin1D Bin1D::At(double center)
{
    return {center, center};
}

Bin1D Bin1D::At(double center, double halfwidth)
{
    return {center - halfwidth, center + halfwidth};
}

Bin1D::Bin1D(double lower, double upper)
    : m_lower(lower)
    , m_upper(upper)
{
    if (lower > upper) {
        std::stringstream s;
        s << "Bin1 constructor called with lower=" << lower << ", upper=" << upper;
        throw std::runtime_error(s.str());
    }
}

double Bin1D::atFraction(double fraction) const
{
    return m_lower * (1 - fraction) + m_upper * fraction;
}

std::optional<Bin1D> Bin1D::clipped_or_nil(double lower, double upper) const
{
    ASSERT(lower <= upper);
    if (upper < m_lower || m_upper < lower)
        return {};
    return Bin1D(std::max(lower, m_lower), std::min(upper, m_upper));
}
