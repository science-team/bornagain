//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Axis/Bin.h
//! @brief     Defines class Bin1D.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_AXIS_BIN_H
#define BORNAGAIN_BASE_AXIS_BIN_H

#include <optional>

//! An real-valued interval.

class Bin1D {
public:
    static Bin1D FromTo(double lower, double upper);
    static Bin1D At(double center);
    static Bin1D At(double center, double halfwidth);

    bool operator==(const Bin1D& other) const
    {
        return (m_lower == other.m_lower && m_upper == other.m_upper);
    }

    double min() const { return m_lower; }
    double max() const { return m_upper; }
    double center() const { return (m_lower + m_upper) / 2; }
    double binSize() const { return m_upper - m_lower; }
    double atFraction(double fraction) const;

    std::optional<Bin1D> clipped_or_nil(double lower, double upper) const;

private:
    Bin1D(double lower, double upper);

    double m_lower; //!< lower bound of the bin
    double m_upper; //!< upper bound of the bin
};

#endif // BORNAGAIN_BASE_AXIS_BIN_H
