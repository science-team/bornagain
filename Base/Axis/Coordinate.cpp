//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Axis/Coordinate.cpp
//! @brief     Implements class Coordinate.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Axis/Coordinate.h"
#include <cstddef>
#include <stdexcept>

namespace {

//! Parses label of form "name (unit)".
//! Tested by Unit.Base.Coordinate.Label via Coordinate(label) constructor.
std::pair<std::string, std::string> parse_label(const std::string& label)
{
    if (label.empty())
        return {"", ""};
    size_t i = label.size() - 1;
    if (label[i] != ')')
        return {label, ""};
    for (size_t j = i - 1; j < label.size(); --j)
        if (label[j] == '(')
            for (size_t jj = j; jj < label.size(); --jj)
                if (jj == 0 || label[jj - 1] != ' ')
                    return {label.substr(0, jj), label.substr(j + 1, i - j - 1)};
    throw std::runtime_error("Invalid coordinate label '" + label + "'");
}

} // namespace


Coordinate::Coordinate(const std::string& name, const std::string& unit)
    : m_name(name)
    , m_unit(unit)
{
}

//! Takes label of form "name (unit)".
//! Tested by Unit.Base.Coordinate.Label.
Coordinate::Coordinate(const std::string& label)
    : Coordinate(parse_label(label).first, parse_label(label).second)
{
}

std::string Coordinate::label() const
{
    return name() + " (" + unit() + ")";
}
