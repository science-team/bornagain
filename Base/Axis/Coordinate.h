//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Axis/Coordinate.h
//! @brief     Defines class Coordinate.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_AXIS_COORDINATE_H
#define BORNAGAIN_BASE_AXIS_COORDINATE_H

#include <string>

//! A name and a unit.

class Coordinate {
public:
    Coordinate(const char* label)
        : Coordinate(std::string(label))
    {
    }
    Coordinate(const std::string& label);
    Coordinate(const std::string& name, const std::string& unit);

    bool operator==(const Coordinate& other) const
    {
        return (m_name == other.m_name && m_unit == other.m_unit);
    }

    const std::string& name() const { return m_name; }
    const std::string& unit() const { return m_unit; }
    std::string label() const;

private:
    std::string m_name;
    std::string m_unit;
};

#endif // BORNAGAIN_BASE_AXIS_COORDINATE_H
