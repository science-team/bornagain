//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Axis/Frame.h
//! @brief     Defines and implements templated class Frame.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_AXIS_FRAME_H
#define BORNAGAIN_BASE_AXIS_FRAME_H

#include "Base/Type/CloneableVector.h"
#include <string>

using std::size_t;

class Bin1D;
class Scale;

//! Holds one or two axes.

class Frame {
public:
    //! Constructor that takes ownership of supplied axes.
    Frame(const std::vector<const Scale*>& axes);
    Frame(const Scale* ax0);
    Frame(const Scale* ax0, const Scale* ax1);
    Frame(const Frame&);
    ~Frame();

    //! Returns number of dimensions.
    size_t rank() const;

    //! Returns total number of bins.
    size_t size() const { return m_size; }

    //! Returns axis with given serial number
    const Scale& axis(size_t k_axis) const;
    const Scale& xAxis() const;
    const Scale& yAxis() const;

    //! Returns the value of selected axis for given i_flat.
    //! @param i_flat The global index of this data structure.
    //! @param k_axis Serial number of selected axis.
    //! @return corresponding bin center of selected axis
    double projectedCoord(size_t i_flat, size_t k_axis) const;

    //! Returns the bin of selected axis for given i_flat.
    //! @param i_flat The global index of this data structure.
    //! @param k_axis Serial number of selected axis.
    //! @return corresponding bin of selected axis
    const Bin1D& projectedBin(size_t i_flat, size_t k_axis) const;

    //! Returns vector of axes indices for given global index
    //! @param i_flat The global index of this data structure.
    //! @return Vector of bin indices for all axes defined
    std::vector<int> allIndices(size_t i_flat) const;

    //! Returns axis bin index for given global index
    //! @param i The flat index of this data structure.
    //! @param k_axis Serial number of selected axis.
    //! @return Corresponding bin index for selected axis
    size_t projectedIndex(size_t i, size_t k_axis) const;

    //! Returns true if both Frame%s have same rank, and all axes have same sizes.
    bool hasSameSizes(const Frame&) const;

    //! Returns true if both Frame%s have same rank, and all axes are equal.
    bool operator==(const Frame&) const;

    Frame plottableFrame() const;
    Frame angularFrame(double lambda, double alpha_i) const;
    Frame qSpaceFrame(double lambda, double alpha_i) const;
    Frame flat() const;

#ifndef SWIG
    Frame* clone() const;

private:
    const CloneableVector<const Scale> m_axes;
    const size_t m_size; // cached product of axis sizes

#endif // SWIG
};

#endif // BORNAGAIN_BASE_AXIS_FRAME_H
