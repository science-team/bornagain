//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Axis/MakeScale.cpp
//! @brief     Implements functions that create instances of class Scale.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Axis/MakeScale.h"
#include "Base/Axis/Scale.h"
#include "Base/Util/Assert.h"
#include <stdexcept>

Scale GenericScale(std::string name, std::vector<double> limits)
{
    std::vector<Bin1D> vec;
    if (limits.size() & 1)
        throw std::runtime_error("GenericScale called with odd number of bin limits");
    for (size_t i = 0; i < limits.size(); i += 2)
        vec.emplace_back(Bin1D::FromTo(limits[i], limits[i + 1]));
    return {name, vec};
}

Scale* newGenericScale(std::string name, std::vector<double> limits)
{
    return new Scale(GenericScale(name, limits));
}


Scale ListScan(std::string name, std::vector<double> points)
{
    std::vector<Bin1D> vec;
    vec.reserve(points.size());
    for (double p : points)
        vec.emplace_back(Bin1D::At(p));
    return {name, vec};
}

Scale* newListScan(std::string name, std::vector<double> points)
{
    return new Scale(ListScan(name, points));
}


Scale EquiDivision(std::string name, size_t N, double start, double end)
{
    if (N == 0)
        throw std::runtime_error("EquiDivision called with N = 0");
    if (start > end)
        throw std::runtime_error("EquiDivision called with end < start");
    std::vector<Bin1D> vec;
    vec.reserve(N);
    for (size_t i = 0; i < N; ++i)
        vec.emplace_back(Bin1D::FromTo((N - i) * (start / N) + i * (end / N),
                                       (N - i - 1) * (start / N) + (i + 1) * (end / N)));
    ASSERT(vec.size() == N);

    return {name, vec};
}

Scale* newEquiDivision(std::string name, size_t N, double start, double end)
{
    return new Scale(EquiDivision(name, N, start, end));
}

Scale EquiScan(std::string name, size_t N, double start, double end)
{
    if (N == 0)
        throw std::runtime_error("EquiScan called with N = 0");
    std::vector<Bin1D> vec;
    vec.reserve(N);
    if (N == 1) {
        if (end != start)
            throw std::runtime_error("EquiScan called with N = 1 but end != start");
        vec.emplace_back(Bin1D::At(start));
    } else {
        if (start > end)
            throw std::runtime_error("EquiScan called with end < start");
        for (size_t i = 0; i < N; ++i)
            vec.emplace_back(Bin1D::At((N - 1 - i) * (start / (N - 1)) + i * (end / (N - 1))));
        ASSERT(vec.size() == N);
    }

    return {name, vec};
}

Scale* newEquiScan(std::string name, size_t N, double start, double end)
{
    return new Scale(EquiScan(name, N, start, end));
}
