//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Axis/MakeScale.h
//! @brief     Declares functions that create instances of class Scale.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_AXIS_MAKESCALE_H
#define BORNAGAIN_BASE_AXIS_MAKESCALE_H

#include <string>
#include <vector>

class Scale;

Scale GenericScale(std::string name, std::vector<double> limits);
#ifndef SWIG
Scale* newGenericScale(std::string name, std::vector<double> limits);
#endif // SWIG

Scale ListScan(std::string name, std::vector<double> points);
#ifndef SWIG
Scale* newListScan(std::string name, std::vector<double> points);
#endif // SWIG

//! Returns axis with fixed bin size.
Scale EquiDivision(std::string name, size_t N, double start, double end);
#ifndef SWIG
Scale* newEquiDivision(std::string name, size_t N, double start, double end);
#endif // SWIG

//! Returns axis with equidistant points (zero bin width).
Scale EquiScan(std::string name, size_t N, double start, double end);
#ifndef SWIG
Scale* newEquiScan(std::string name, size_t N, double start, double end);
#endif // SWIG

#endif // BORNAGAIN_BASE_AXIS_MAKESCALE_H
