//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Axis/Pixel.cpp
//! @brief     Implements class Pixel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Axis/Pixel.h"
#include "Base/Axis/Bin.h"
#include "Base/Vector/GisasDirection.h"

Pixel::Pixel(const Bin1D& phi_bin, const Bin1D& alpha_bin)
    : m_phi(phi_bin.min())
    , m_alpha(alpha_bin.min())
    , m_dphi(phi_bin.binSize())
    , m_dalpha(alpha_bin.binSize())
{
    auto solid_angle_value = std::abs(m_dphi * (std::sin(m_alpha + m_dalpha) - std::sin(m_alpha)));
    m_solid_angle = solid_angle_value <= 0.0 ? 1.0 : solid_angle_value;
}

Pixel* Pixel::createZeroSizePixel(double x, double y) const
{
    const double phi = m_phi + x * m_dphi;
    const double alpha = m_alpha + y * m_dalpha;
    return new Pixel(Bin1D::At(phi), Bin1D::At(alpha));
}

R3 Pixel::getK(double x, double y, double wavelength) const
{
    const double phi = m_phi + x * m_dphi;
    const double alpha = m_alpha + y * m_dalpha;
    return vecOfLambdaAlphaPhi(wavelength, alpha, phi);
}

double Pixel::integrationFactor(double /* x */, double y) const
{
    if (m_dalpha == 0.0)
        return 1.0;
    const double alpha = m_alpha + y * m_dalpha;
    return std::cos(alpha) * m_dalpha / (std::sin(m_alpha + m_dalpha) - std::sin(m_alpha));
}
