//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Axis/Pixel.h
//! @brief     Defines and implements interface Pixel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_AXIS_PIXEL_H
#define BORNAGAIN_BASE_AXIS_PIXEL_H

#include <heinz/Vectors3D.h>

class Bin1D;

//! A rectangular pixel in a two-dimensional detector.

class Pixel {
public:
    Pixel(const Bin1D& phi_bin, const Bin1D& alpha_bin);

#ifndef SWIG
    Pixel* createZeroSizePixel(double x, double y) const;
#endif // SWIG

    R3 getK(double x, double y, double wavelength) const;
    double integrationFactor(double x, double y) const;
    double solidAngle() const { return m_solid_angle; }

private:
    const double m_phi;
    const double m_alpha;
    const double m_dphi;
    const double m_dalpha;
    mutable double m_solid_angle;
};

#endif // BORNAGAIN_BASE_AXIS_PIXEL_H
