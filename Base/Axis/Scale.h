//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Axis/Scale.h
//! @brief     Defines interface Scale.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_AXIS_SCALE_H
#define BORNAGAIN_BASE_AXIS_SCALE_H

#include "Base/Axis/Bin.h"
#include "Base/Axis/Coordinate.h"
#include <functional>
#include <memory>
#include <string>
#include <utility>
#include <vector>

using trafo_t = std::function<double(double)>;

//! An axis, with coordinate and bins.

class Scale {
public:
    Scale(const Coordinate& coord, const std::vector<Bin1D>& bins);
    Scale(const Scale& other);

#ifndef SWIG
    Scale* clone() const;
#endif // SWIG

    //! Returns the label of the axis
    std::string axisLabel() const;

    //... Getter functions: range

    //! Returns the number of bins
    size_t size() const;

    //! Returns value of first point of axis
    double min() const;

    //! Returns value of last point of axis
    double max() const;

    //! Returns lower and upper bound in a pair.
    std::pair<double, double> bounds() const;

    //! Returns true if axis contains given point
    bool rangeComprises(double value) const;

    //! Returns distance from first to last point
    double span() const;

    //! Returns midpoint of axis
    double center() const;

    //... Getter functions: bins

    const Bin1D& bin(size_t i) const;
    double binCenter(size_t i) const;
    const std::vector<Bin1D>& bins() const { return m_bins; }
    std::vector<double> binCenters() const;

    //! find bin index which is best match for given value
    size_t closestIndex(double value) const;

    bool isEquiDivision() const;
    bool isEquiScan() const;
    bool isScan() const;

    Scale clipped(double lower, double upper) const;
    Scale clipped(std::pair<double, double> bounds) const;

    bool operator==(const Scale& other) const;

    friend std::ostream& operator<<(std::ostream& ostr, const Scale& ax);

    std::string coordName() const;
    std::string unit() const;

    Scale plottableScale() const;
    Scale transformedScale(const Coordinate& coord, const trafo_t& axTrafo) const;
    Scale reversedScale() const;

    Scale alpha_f_Scale(double lambda, double alpha_i) const;
    Scale phi_f_Scale(double lambda) const;

    Scale qz_Scale(double lambda, double alpha_i) const;
    Scale qy_Scale(double lambda) const;

protected:
    std::vector<Bin1D> m_bins;
    std::unique_ptr<Coordinate> m_coord;
};

#endif // BORNAGAIN_BASE_AXIS_SCALE_H
