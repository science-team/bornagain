//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Const/PhysicalConstants.h
//! @brief     Defines the values of physical constants (SI).
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_CONST_PHYSICALCONSTANTS_H
#define BORNAGAIN_BASE_CONST_PHYSICALCONSTANTS_H
#ifdef BA_CPP20

#include <numbers>

#endif // BA_CPP20
//! Physical constants.

namespace PhysConsts {

constexpr double m_n = 1.67492749804e-27;  //!< Neutron mass, kg
constexpr double h_bar = 1.054571817e-34;  //!< Reduced Plank constant, J s
constexpr double mu_N = 5.0507837461e-27;  //!< Nuclear magneton (\f$ \mu_N \f$), J/T
constexpr double mu_B = 9.2740100783e-24;  //!< Bohr magneton (\f$ \mu_B \f$), J/T
constexpr double r_e = 2.8179403262e-15;   //!< Thomson scattering length (\f$ r_e \f$), m
constexpr double gamma_n = 1.91304272;     //!< \f$\gamma\f$ factor for neutron magnetic moment,
                                           //!< \f$\mu_n = \gamma \cdot \mu_N\f$
constexpr double g_factor_n = -3.82608545; //!< neutron g-factor
#ifdef BA_CPP20
constexpr double pi = std::numbers::pi;
#else
constexpr double pi = 3.141592653589793238462643383279502884L; // OEIS: A000796
#endif // BA_CPP20

} // namespace PhysConsts

#endif // BORNAGAIN_BASE_CONST_PHYSICALCONSTANTS_H
