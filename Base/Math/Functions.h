//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Math/Functions.h
//! @brief     Defines namespace Math.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_MATH_FUNCTIONS_H
#define BORNAGAIN_BASE_MATH_FUNCTIONS_H

#include <heinz/Complex.h>
#include <vector>

//! Various mathematical functions.

namespace Math {

//  ************************************************************************************************
//  Various functions
//  ************************************************************************************************

double StandardNormal(double x);
double Gaussian(double x, double average, double std_dev);
double IntegratedGaussian(double x, double average, double std_dev);
double Heaviside(double x);

//! cotangent function: \f$cot(x)\equiv1/tan(x)\f$
double cot(double x);

//! sinc function: \f$sinc(x)\equiv\sin(x)/x\f$
double sinc(double x);

//! Complex sinc function: \f$sinc(x)\equiv\sin(x)/x\f$
complex_t sinc(complex_t z);

//! Complex tanhc function: \f$tanhc(x)\equiv\tanh(x)/x\f$
complex_t tanhc(complex_t z);

//! Real Laue function: \f$Laue(x,N)\equiv\sin(Nx)/sin(x)\f$
double Laue(double x, size_t N);

//! Error function of real-valued argument
double erf(double arg);

//  ************************************************************************************************
//  Random number generators
//  ************************************************************************************************

// double GenerateStandardNormalRandom();
// double GenerateNormalRandom(double average, double std_dev);
double GeneratePoissonRandom(double average);

int GenerateNextSeed(unsigned seed);

} // namespace Math

#endif // BORNAGAIN_BASE_MATH_FUNCTIONS_H
