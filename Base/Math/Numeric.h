//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Math/Numeric.h
//! @brief     Defines constants and "almost equal" in namespace Numeric.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_MATH_NUMERIC_H
#define BORNAGAIN_BASE_MATH_NUMERIC_H

#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>

void check_scalar(double a, double b, int ulp);

#ifndef SWIG

//! Floating-point approximations.

namespace Numeric {

//! Returns the safe relative difference, which is 2(|a-b|)/(|a|+|b|) except in special cases.
double relativeDifference(double a, double b);

//! Returns true if two doubles agree within machine epsilon times ulp (units in the last place).
bool almostEqual(double a, double b, int ulp);
bool almostEqual(complex_t a, complex_t b, int ulp);
bool almostEqual(const R3& a, const R3& b, int ulp);

double ignoreDenormalized(double value);

double round_decimal(double val, double digits);

int orderOfMagnitude(double x);

} // namespace Numeric

#endif // SWIG

#endif // BORNAGAIN_BASE_MATH_NUMERIC_H
