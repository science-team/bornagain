//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Py/ArrayWrapper.cpp
//! @brief     Implements ArrayWrapper classes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "ArrayWrapper.h"

#ifdef BORNAGAIN_PYTHON

Arrayf64Wrapper::Arrayf64Wrapper(const std::size_t datasize, const std::size_t n_dims,
                                 const std::size_t* dims, const double* array_ptr,
                                 const bool owndata)
    : m_datasize{datasize}
    , m_array_ptr{array_ptr}
    , m_owndata{owndata}
{
    // check validity
    if (m_datasize < 1 || n_dims < 1 || !dims || !m_array_ptr)
        return;

    for (std::size_t i_d = 0; i_d < n_dims; ++i_d) {
        if (dims[i_d] < 1)
            return;
    }

    // initialize the array descriptor
    m_dims.resize(n_dims);
    for (std::size_t i_d = 0; i_d < n_dims; ++i_d)
        m_dims[i_d] = dims[i_d];

    if (m_owndata) {
        // clone the data
        m_array.resize(m_datasize);
        for (std::size_t i_e = 0; i_e < m_datasize; ++i_e)
            m_array[i_e] = array_ptr[i_e];
        m_array_ptr = m_array.data();
    } else {
        m_array_ptr = array_ptr;
    }
}

Arrayf64Wrapper::Arrayf64Wrapper() {}

Arrayf64Wrapper::~Arrayf64Wrapper() {}

Arrayf64Wrapper::Arrayf64Wrapper(const Arrayf64Wrapper& other)
    : Arrayf64Wrapper::Arrayf64Wrapper(other.size(), other.n_dimensions(), other.dimensions(),
                                       other.arrayPtr(), other.ownData())
{
}

Arrayf64Wrapper& Arrayf64Wrapper::operator=(const Arrayf64Wrapper& other)
{
    m_datasize = other.m_datasize;
    m_dims = other.m_dims;
    m_owndata = other.m_owndata;
    m_array = other.m_array;
    if (m_owndata)
        m_array_ptr = m_array.data();
    else
        m_array_ptr = other.m_array_ptr;

    return *this;
}

#endif // BORNAGAIN_PYTHON
