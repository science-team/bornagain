//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Py/ArrayWrapper.h
//! @brief     Defines ArrayWrapper classes. The array descriptors are required to pass
//!            the type and size information of a C-Array to Python.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_PY_ARRAYWRAPPER_H
#define BORNAGAIN_BASE_PY_ARRAYWRAPPER_H

#ifdef BORNAGAIN_PYTHON

#include <vector>

//! Descriptor of a C-array to be passed to Python to construct a Numpy array.
class Arrayf64Wrapper {

public:
    Arrayf64Wrapper(const std::size_t datasize, const std::size_t n_dims_, const std::size_t* dims_,
                    const double* array_ptr_, const bool owns_data_);
    Arrayf64Wrapper();                                        // needed by SWIG
    Arrayf64Wrapper(const Arrayf64Wrapper& other);            // needed by SWIG
    Arrayf64Wrapper& operator=(const Arrayf64Wrapper& other); // needed by SWIG

    ~Arrayf64Wrapper(); // needed by SWIG

    //! Total size of the array.
    std::size_t size() const { return m_datasize; }

    //! Returns the rank (= number of dimensions) of the array.
    std::size_t n_dimensions() const { return m_dims.size(); }

    //! Raw pointer to the array which stores the size of the underlying C-Array on each dimension.
    const std::size_t* dimensions() const { return m_dims.data(); }

    //! Raw pointer to the underlying C-array.
    const double* arrayPtr() const { return m_array_ptr; }

    //! Returns true if the array owns its data.
    bool ownData() const { return m_owndata; }

private:
    std::size_t m_datasize = 0;      //!< total size of the array
    std::vector<std::size_t> m_dims; //!< dimensions of the arrary
    std::vector<double> m_array;     //!< container for array values, used when data is owned
    const double* m_array_ptr;       //!< raw pointer to the array data
    bool m_owndata = false;          //!< flag to denote if the array owns its data
};

#endif // BORNAGAIN_PYTHON

#endif // BORNAGAIN_BASE_PY_ARRAYWRAPPER_H
