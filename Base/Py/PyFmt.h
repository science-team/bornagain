//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Py/PyFmt.h
//! @brief     Defines namespace pyfmt.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_PY_PYFMT_H
#define BORNAGAIN_BASE_PY_PYFMT_H

#include <heinz/Vectors3D.h>
#include <string>
#include <variant>
#include <vector>

//! Utility functions for writing Python code snippets.

namespace Py::Fmt {

std::string printImportedSymbols(const std::string& code);

std::string printInt(int value);
std::string printBool(double value);
std::string printDouble(double input);
std::string printLightDouble(double input);
std::string printNm(double input, int pow = 1);
std::string printScientificDouble(double input);
std::string printDegrees(double input);
std::string printValue(double value, const std::string& units = "");
std::string printValue(std::variant<double, int> value, const std::string& units = "");
std::string printString(const std::string& value);

//! Takes pairs of value/unit and concatenates them for an argument list.
//! Each pair's content will be processed by printValue(), so the meaning
//! of the content is the same as in printValue().
std::string printArguments(const std::vector<std::pair<double, std::string>>& arguments);

//! Print a function in the form "<name>(<arguments>)".
//! arguments will be processed by printArguments(), see there for details.
std::string printFunction(const std::string& name,
                          const std::vector<std::pair<double, std::string>>& arguments);

//! Convenience overload for printing a function with one argument.
std::string printFunction(const std::string& name, double value, const std::string& unit);

//! Convenience overload for printing a function with two arguments.
std::string printFunction(const std::string& name, double value1, const std::string& unit1,
                          double value2, const std::string& unit2);

std::string printKvector(R3 value);

//! Returns a string of blanks with given width. By default
//! the width equals standard offset in python files.
std::string indent(size_t width = 4u);

} // namespace Py::Fmt

#endif // BORNAGAIN_BASE_PY_PYFMT_H
