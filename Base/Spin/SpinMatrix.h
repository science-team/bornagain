//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Spin/SpinMatrix.h
//! @brief     Defines class SpinMatrix.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_SPIN_SPINMATRIX_H
#define BORNAGAIN_BASE_SPIN_SPINMATRIX_H

#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>

class Spinor;

class SpinMatrix {
public:
    //! Constructs matrix with rows (a_,b_) and (c_,d_).
    SpinMatrix(complex_t a_, complex_t b_, complex_t c_, complex_t d_);
    //! Contructs the null matrix.
    SpinMatrix();

    static SpinMatrix Diag(complex_t a_, complex_t d_);
    static SpinMatrix One();
    static SpinMatrix Null();

    //! Constructs matrix (I+v*s)/2, where s is the Pauli vector.
    static SpinMatrix FromBlochVector(const R3& v);

    SpinMatrix operator-() const;

    SpinMatrix operator+(const SpinMatrix&) const;
    SpinMatrix operator-(const SpinMatrix&) const;
    SpinMatrix operator+=(const SpinMatrix&);

    SpinMatrix operator*(const SpinMatrix&) const;
    SpinMatrix operator*=(const SpinMatrix&);

    Spinor operator*(const Spinor&) const;

    SpinMatrix operator*(complex_t) const;
    SpinMatrix operator*(double) const;

    SpinMatrix operator/(complex_t) const;
    SpinMatrix operator/(double) const;

    SpinMatrix operator*=(complex_t);
    SpinMatrix operator*=(double);
    SpinMatrix operator/=(complex_t);
    SpinMatrix operator/=(double);

    Spinor col0() const;
    Spinor col1() const;

    complex_t trace() const;
    complex_t determinant() const;

    bool allFinite() const;

    SpinMatrix adjoint() const;

    complex_t a, b, c, d;
};

SpinMatrix operator*(complex_t, const SpinMatrix&);
SpinMatrix operator*(double, const SpinMatrix&);

#endif // BORNAGAIN_BASE_SPIN_SPINMATRIX_H
