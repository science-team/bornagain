//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Spin/Spinor.cpp
//! @brief     Implements class Spinor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Spin/Spinor.h"

Spinor::Spinor(complex_t u_, complex_t v_)
    : u(u_)
    , v(v_)
{
}

Spinor Spinor::Zero()
{
    return {0, 0};
}

Spinor Spinor::operator+(const Spinor& o) const
{
    return {u + o.u, v + o.v};
}

Spinor Spinor::operator-(const Spinor& o) const
{
    return {u - o.u, v - o.v};
}


Spinor Spinor::operator*(complex_t f) const
{
    return {u * f, v * f};
}

Spinor Spinor::operator/(complex_t f) const
{
    return {u / f, v / f};
}

Spinor operator*(complex_t f, const Spinor& m)
{
    return m * f;
}


complex_t DotProduct(const Spinor& r, const Spinor& t)
{
    return r.u * t.u + r.v * t.v;
}
