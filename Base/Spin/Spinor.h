//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Spin/Spinor.h
//! @brief     Defines class Spinor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_SPIN_SPINOR_H
#define BORNAGAIN_BASE_SPIN_SPINOR_H

#include <heinz/Complex.h>

class Spinor {
public:
    Spinor(complex_t u_, complex_t v_);
    static Spinor Zero();

    Spinor operator+(const Spinor&) const;
    Spinor operator-(const Spinor&) const;

    Spinor operator*(complex_t) const;
    Spinor operator/(complex_t) const;

    complex_t u, v;
};

Spinor operator*(complex_t, const Spinor&);

complex_t DotProduct(const Spinor& r, const Spinor& t);

#endif // BORNAGAIN_BASE_SPIN_SPINOR_H
