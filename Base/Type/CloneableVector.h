//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Type/CloneableVector.h
//! @brief     Defines and implements templated class CloneableVector.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_TYPE_CLONEABLEVECTOR_H
#define BORNAGAIN_BASE_TYPE_CLONEABLEVECTOR_H

#include "Base/Type/OwningVector.h"

//! A vector of unique pointers to objects that are cloneable.
//!
//! Equips vector<unique_ptr<T>> with copy constructor.
//! For use with polymorphic objects, or in pimpl idiom.

//! If the copy constructor or the copy assignment operator is used,
//! then there must be a function T::clone().

template <typename T> class CloneableVector : public OwningVector<T> {
public:
    CloneableVector() = default;
    //! Constructor that takes over ownership of elements in given vector
    CloneableVector(std::vector<T*> v)
        : OwningVector<T>(v)
    {
    }
    //! Constructor that clones elements in given vector
    CloneableVector(const CloneableVector& other)
        : OwningVector<T>()
    {
        this->m_v.reserve(other.size());
        for (T* e : other)
            this->m_v.emplace_back(e->clone());
    }
    CloneableVector(CloneableVector&& other) = default;
    ~CloneableVector() = default;

    CloneableVector& operator=(const CloneableVector& other)
    {
        if (this == &other)
            return *this;
        CloneableVector ret(other);
        std::swap(this->m_v, ret.m_v);
        return *this;
    }
    CloneableVector& operator=(CloneableVector&& other) = default;
};

#endif // BORNAGAIN_BASE_TYPE_CLONEABLEVECTOR_H
