//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Type/ICloneable.h
//! @brief     Defines and implements the standard mix-in ICloneable.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_TYPE_ICLONEABLE_H
#define BORNAGAIN_BASE_TYPE_ICLONEABLE_H

//! Interface for polymorphic classes that should not be copied, except by explicit cloning.
//!
//! Child classes of ICloneable must provide clone().

class ICloneable {
public:
    ICloneable() = default;
    virtual ~ICloneable() = default;

    ICloneable(const ICloneable&) = delete;
    ICloneable(ICloneable&&) = default;

#ifndef SWIG
    virtual ICloneable* clone() const = 0;
#endif // SWIG
};

#endif // BORNAGAIN_BASE_TYPE_ICLONEABLE_H
