//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Type/Span.cpp
//! @brief     Defines class Span.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Type/Span.h"
#include "Base/Util/Assert.h"

Span::Span(double low, double hig)
    : m_low(low)
    , m_hig(hig)
{
    ASSERT(low <= hig);
}

Span Span::operator+(double increment) const
{
    return {low() + increment, hig() + increment};
}

Span Span::unite(const Span& left, const Span& right)
{
    return {std::min(left.low(), right.low()), std::max(left.hig(), right.hig())};
}
