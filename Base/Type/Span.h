//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Type/Span.h
//! @brief     Defines class Span.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_TYPE_SPAN_H
#define BORNAGAIN_BASE_TYPE_SPAN_H

#include <utility>

//! An interval. Limits are of type double, and may be infinite.
//! Used for the z-coordinate, especially when slicing form factors.

class Span {
public:
    // default c'tor needed by Swig:
    Span()
        : Span(0, 0)
    {
    }
    Span(double low, double hig);

    Span operator+(double increment) const;

    double low() const { return m_low; }
    double hig() const { return m_hig; }

    bool contains(double z) const { return m_low <= z && z <= m_hig; }

    std::pair<double, double> pair() const { return {m_low, m_hig}; }

    //! Returns the union of two Span (the minimal interval that contains both input intervals).
    static Span unite(const Span& left, const Span& right);

private:
    // members cannot be made const lest we loose the assignement operator:
    double m_low;
    double m_hig;
};

#endif // BORNAGAIN_BASE_TYPE_SPAN_H
