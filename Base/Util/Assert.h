//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Util/Assert.h
//! @brief     Defines the macro ASSERT.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_UTIL_ASSERT_H
#define BORNAGAIN_BASE_UTIL_ASSERT_H

#include <stdexcept>
#include <string>

class bug : public std::runtime_error {
public:
    bug(std::string msg)
        : std::runtime_error(msg)
    {
    }
};

// ASSERT macro: terminate if condition is false.
//
// Implementation notes:
// - Must be declared as a macro, not a function, so that we can use preprocessor
//   macros for informative error messages.

#ifdef BA_DEBUG

#include <csignal>
#include <iostream>

#define ASSERT(condition)                                                                          \
    if (!(condition)) {                                                                            \
        std::cerr << "Assertion " #condition " failed in " __FILE__ ", line " << __LINE__          \
                  << std::endl;                                                                    \
        std::raise(SIGTERM); /* abort so that we can inspect the backtrace */                      \
        throw std::runtime_error("Assertion failed ... and we should never get here");             \
    }
#define ASSERT_NEVER                                                                               \
    {                                                                                              \
        std::cerr << "Reached forbidden case in " __FILE__ ", line " << __LINE__ << std::endl;     \
        std::raise(SIGTERM); /* abort so that we can inspect the backtrace */                      \
        throw std::runtime_error("Forbidden case ... and we should never get here");               \
    }

#else // not BA_DEBUG

#define ASSERT(condition)                                                                          \
    if (!(condition))                                                                              \
        throw bug("Assertion " #condition " failed in " __FILE__ ", line "                         \
                  + std::to_string(__LINE__) + ".");

#define ASSERT_NEVER                                                                               \
    throw bug("Reached forbidden case in " __FILE__ ", line " + std::to_string(__LINE__) + ".");

#endif // BA_DEBUG

#endif // BORNAGAIN_BASE_UTIL_ASSERT_H
