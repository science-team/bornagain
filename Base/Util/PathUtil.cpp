//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Util/PathUtil.cpp
//! @brief     Implements namespace Base::Path.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Util/PathUtil.h"
#include "Base/Util/Assert.h"
#include "Base/Util/StringUtil.h"
#include <codecvt> //
#include <filesystem>
#include <locale>
#include <stdexcept>

namespace fs = std::filesystem;

#ifdef _WIN32
std::wstring Base::Path::osPath(std::string path)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> c;
    return c.from_bytes(path);
}
#else
std::string Base::Path::osPath(std::string path)
{
    return path;
}
#endif

std::string Base::Path::extension(const std::string& path)
{
    return fs::path(path).extension().string();
}

bool Base::Path::hasExtension(const std::string& path, const std::string& ref_extension)
{
    return Base::String::to_lower(extension(path)) == ref_extension;
}

std::string Base::Path::extensions(const std::string& path)
{
    const auto name = Base::Path::filename(path);
    if (name == "..")
        return {};

    const auto pos = name.find_first_of('.', 1); // 1: ignore any file-is-hidden dot
    return pos != std::string::npos ? name.substr(pos, name.size() - pos) : "";
}

bool Base::Path::createDirectories(const std::string& dir_name)
{
    return fs::create_directories(Base::Path::osPath(dir_name));
}

std::vector<std::string> Base::Path::filesInDirectory(const std::string& dir_name)
{
    std::vector<std::string> result;
    if (!fs::exists(dir_name))
        throw std::runtime_error("Base::Path::filesInDirectory '" + dir_name + "' does not exist");

    for (const auto& entry : fs::directory_iterator(dir_name))
        if (entry.is_regular_file())
            result.push_back(entry.path().filename().string());

    return result;
}

std::string Base::Path::jointPath(const std::string& path1, const std::string& path2)
{
    ASSERT(!path2.empty());

    if (path1.empty())
        return fs::path(path2).string();

    return (fs::path(path1) / fs::path(path2)).string();
}

std::string Base::Path::filename(const std::string& path)
{
    return fs::path(path).filename().string();
}

std::string Base::Path::stem(const std::string& path)
{
    return fs::path(path).stem().string();
}

std::string Base::Path::stem_ext(const std::string& path)
{
    const auto name = Base::Path::filename(path);
    if (name == "..")
        return "..";

    const auto pos = name.find_first_of('.', 1); // 1: ignore any file-is-hidden dot
    return pos != std::string::npos ? name.substr(0, pos) : name;
}

bool Base::Path::IsFileExists(const std::string& path)
{
    return fs::exists(Base::Path::osPath(path));
}
