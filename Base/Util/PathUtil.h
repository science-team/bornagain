//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Util/PathUtil.h
//! @brief     Defines namespace FileSystemUtils.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_UTIL_PATHUTIL_H
#define BORNAGAIN_BASE_UTIL_PATHUTIL_H

#include <string>
#include <vector>

//! Utility functions to deal with file system.

namespace Base::Path {

//! Converts path to operating-system specific type expected by std::filesystem.
#ifdef _WIN32
std::wstring osPath(std::string path);
#else
std::string osPath(std::string path);
#endif

//! Returns extension of given filename.
//! "/home/user/filename.int" -> ".int", "/home/user/filename.int.gz" -> ".gz"
std::string extension(const std::string& path);

//! Returns true if extension of path, converted to lower case, matches given reference extension
bool hasExtension(const std::string& path, const std::string& ref_extension);

//! Returns extension(s) of given filename.
//! "/home/user/filename.int" -> ".int", "/home/user/filename.int.gz" -> ".int.gz"
std::string extensions(const std::string& path);

//! Creates directory, and its parents, in current directory.
bool createDirectories(const std::string& dir_name);

//! Returns filenames of files in directory
std::vector<std::string> filesInDirectory(const std::string& dir_name);

//! Returns joint path name. Argument path1 may be empty, argument path2 not.
std::string jointPath(const std::string& path1, const std::string& path2);

//! Returns path without directory part ("Foo/Bar/Doz.int.gz" -> "Doz.int.gz")
std::string filename(const std::string& path);

//! Returns filename without (last) extension.
//! "/home/user/filename.int" -> "filename", "/home/user/filename.int.gz" -> "filename.int"
std::string stem(const std::string& path);

//! Returns filename without extension(s).
//! "/home/user/filename.int" -> "filename", "/home/user/filename.int.gz" -> "filename"
std::string stem_ext(const std::string& path);

//! Returns true if file with given name exists on disk.
bool IsFileExists(const std::string& path);

} // namespace Base::Path

#endif // BORNAGAIN_BASE_UTIL_PATHUTIL_H
