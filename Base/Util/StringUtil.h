//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Util/StringUtil.h
//! @brief     Defines a few helper functions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_UTIL_STRINGUTIL_H
#define BORNAGAIN_BASE_UTIL_STRINGUTIL_H

#include <string>
#include <vector>

//! Utility functions to analyze or modify strings.

namespace Base::String {

//! Split string into vector of string using delimiter.
std::vector<std::string> split(const std::string& text, const std::string& delimiter);

//! Replaces all occurrences of items from string text with delimiter
void replaceItemsFromString(std::string& text, const std::vector<std::string>& items,
                            const std::string& replacement = "");

//! Returns string obtain by joining vector elements
std::string join(const std::vector<std::string>& joinable, const std::string& joint);

//! Returns new string which is lower case of text.
std::string to_lower(std::string text);

//! Interprets the contained text as an integer and returns it in \a result. Space chars at its
//! begin and end will be ignored. If the conversion result is of no interest, but only the
//! convertibility, then you can set \a result to nullptr.
//! The conversion is assumed to be successful if:
//! * every character was consumed by the conversion
//! * the value is not too big for int
//! * the text is not empty
//! Returns true if successful.
//! If not successful, the value in result is not changed.
//! Does not throw anything.
bool to_int(const std::string& str, int* result);

bool to_double(const std::string& str, double* result);

//! Cuts any of the chars given in whitespace from start and end of str.
std::string trim(const std::string& str, const std::string& whitespace = " \t\r\n");

//! Cuts any of the chars given in whitespace from start.
std::string trimFront(const std::string& str, const std::string& whitespace = " \t\r\n");

//! True if the string starts with substr. The comparison is case sensitive
bool startsWith(const std::string& str, const std::string& substr);

std::vector<int> expandNumberList(const std::string& pattern);

//! Parse double values from line of text.
std::vector<double> parse_doubles(const std::string& str);

} // namespace Base::String

#endif // BORNAGAIN_BASE_UTIL_STRINGUTIL_H
