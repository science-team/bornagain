//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Util/SysUtil.cpp
//! @brief     Implements various stuff in namespace Utils.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Util/SysUtil.h"
#include <chrono>
#include <iomanip>
#include <iostream>
#include <sstream>

std::string Base::System::getCurrentDateAndTime()
{
    using clock = std::chrono::system_clock;

    std::stringstream output;
    std::time_t current_time = clock::to_time_t(clock::now());
    output << std::put_time(std::gmtime(&current_time), "%Y-%b-%d %T");
    return output.str();
}

std::string Base::System::getenv(const std::string& name)
{
    if (char* c = std::getenv(name.c_str()))
        return c;
    return "";
}
