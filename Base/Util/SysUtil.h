//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Util/SysUtil.h
//! @brief     Defines various stuff in namespace Utils.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_UTIL_SYSUTIL_H
#define BORNAGAIN_BASE_UTIL_SYSUTIL_H

#include <string>

//! System call wrappers.

namespace Base::System {

std::string getCurrentDateAndTime();

//! Returns environment variable.
std::string getenv(const std::string& name);

} // namespace Base::System

#endif // BORNAGAIN_BASE_UTIL_SYSUTIL_H
