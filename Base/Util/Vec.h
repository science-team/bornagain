//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Util/Vec.h
//! @brief     Defines namespace Vec.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_UTIL_VEC_H
#define BORNAGAIN_BASE_UTIL_VEC_H

#include <vector>

namespace Vec {

template <typename T, class S> int indexOfPtr(const T* t, const std::vector<S*>& v)
{
    for (size_t i = 0; i < v.size(); i++)
        if (v[i] == t)
            return int(i);
    return -1;
}

template <typename T, class S> bool containsPtr(const T* t, const std::vector<S*>& v)
{
    for (size_t i = 0; i < v.size(); i++)
        if (v[i] == t)
            return true;
    return false;
}

template <typename C> void concat(std::vector<C>& v, const std::vector<C>& w)
{
    v.insert(v.end(), w.begin(), w.end());
}

} // namespace Vec

#endif // BORNAGAIN_BASE_UTIL_VEC_H
