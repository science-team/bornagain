//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Vector/GisasDirection.cpp
//! @brief     Implements class GisasDirection.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Vector/GisasDirection.h"
#include "Base/Const/PhysicalConstants.h"
#include <cmath>

using PhysConsts::pi;

R3 vecOfKAlphaPhi(double _k, double _alpha, double _phi)
{
    return GisasDirection(_alpha, _phi).declinationVector(_k);
}

R3 vecOfLambdaAlphaPhi(double _lambda, double _alpha, double _phi)
{
    return vecOfKAlphaPhi((2 * pi) / _lambda, _alpha, _phi);
}

R3 GisasDirection::declinationVector(double r) const
{
    return {r * std::cos(m_alpha) * std::cos(m_phi), -r * std::cos(m_alpha) * std::sin(m_phi),
            r * std::sin(m_alpha)};
}
