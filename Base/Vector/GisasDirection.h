//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Vector/GisasDirection.h
//! @brief     Defines class GisasDirection.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_VECTOR_GISASDIRECTION_H
#define BORNAGAIN_BASE_VECTOR_GISASDIRECTION_H

#include <heinz/Vectors3D.h>

R3 vecOfKAlphaPhi(double _k, double _alpha, double _phi = 0);
R3 vecOfLambdaAlphaPhi(double _lambda, double _alpha, double _phi = 0);

//! A direction in three-dimensional space, parameterized by GISAS angles alpha_f, phi_f.

class GisasDirection {
public:
    GisasDirection(double alpha, double phi)
        : m_alpha(alpha)
        , m_phi(phi)
    {
    }

    double alpha() const { return m_alpha; }
    double phi() const { return m_phi; }

    //! Returns Cartesian 3D vector, where alpha, phi parameterize declination away from (r,0,0)
    R3 declinationVector(double r) const;

private:
    double m_alpha; //!< declination in +z direction
    double m_phi;   //!< declination in -y direction
};

#endif // BORNAGAIN_BASE_VECTOR_GISASDIRECTION_H
