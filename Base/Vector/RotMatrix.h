//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Vector/RotMatrix.h
//! @brief     Declares class RotMatrix.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_BASE_VECTOR_ROTMATRIX_H
#define BORNAGAIN_BASE_VECTOR_ROTMATRIX_H

#include <heinz/Rotations3D.h>

using RotMatrix = Rotation3D<double>;

#endif // BORNAGAIN_BASE_VECTOR_ROTMATRIX_H
