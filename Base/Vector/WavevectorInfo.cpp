//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Vector/WavevectorInfo.cpp
//! @brief     Implements WavevectorInfo.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Vector/WavevectorInfo.h"

WavevectorInfo::WavevectorInfo(C3 ki, C3 kf, double wavelength)
    : m_ki(ki)
    , m_kf(kf)
    , m_vacuum_wavelength(wavelength)
{
}

WavevectorInfo::WavevectorInfo(R3 ki, R3 kf, double wavelength)
    : WavevectorInfo(ki.complex(), kf.complex(), wavelength)
{
}

WavevectorInfo WavevectorInfo::transformed(const RotMatrix& transform) const
{
    return {transform.transformed(m_ki), transform.transformed(m_kf), m_vacuum_wavelength};
}
