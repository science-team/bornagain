//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Base/Vector/WavevectorInfo.h
//! @brief     Defines WavevectorInfo.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_BASE_VECTOR_WAVEVECTORINFO_H
#define BORNAGAIN_BASE_VECTOR_WAVEVECTORINFO_H

#include "Base/Vector/RotMatrix.h"
#include <heinz/Vectors3D.h>

//! Holds all wavevector information relevant for calculating form factors.

class WavevectorInfo {
public:
    WavevectorInfo(C3 ki, C3 kf, double wavelength);
    WavevectorInfo(R3 ki, R3 kf, double wavelength);

    WavevectorInfo transformed(const RotMatrix& transform) const;
    C3 getKi() const { return m_ki; }
    C3 getKf() const { return m_kf; }
    C3 getQ() const { return m_ki - m_kf; }
    double vacuumLambda() const { return m_vacuum_wavelength; }

private:
    C3 m_ki;
    C3 m_kf;
    double m_vacuum_wavelength;
};

#endif // BORNAGAIN_BASE_VECTOR_WAVEVECTORINFO_H
