//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Analyze/Fourier.cpp
//! @brief     Implements Fourier transform function in namespace Analyze.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Device/Analyze/Fourier.h"
#include "Base/Axis/Scale.h"
#include "Base/Math/FourierTransform.h"
#include "Device/Data/Datafield.h"
#include <heinz/Complex.h>
#include <iostream>

Datafield Analyze::createFFT(const Datafield& data)
{
    const auto signal = data.values2D();

    FourierTransform ft;
    double2d_t signal2 = ft.ramplitude(signal);
    signal2 = ft.fftshift(signal2); // low frequency to center of array

    return {"~" + data.xAxis().axisLabel(), "~" + data.yAxis().axisLabel(), signal2};
}
