//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Analyze/Fourier.h
//! @brief     Defines Fourier transform function in namespace Analyze.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_DEVICE_ANALYZE_FOURIER_H
#define BORNAGAIN_DEVICE_ANALYZE_FOURIER_H

class Datafield;

namespace Analyze {

//! Creates Fourier Transform (Datafield format) of intensity map (Datafield format).
Datafield createFFT(const Datafield& data);

} // namespace Analyze

#endif // BORNAGAIN_DEVICE_ANALYZE_FOURIER_H
