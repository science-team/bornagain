//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Analyze/Peaks.h
//! @brief     Defines function FindPeaks namespace Analyze.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_ANALYZE_PEAKS_H
#define BORNAGAIN_DEVICE_ANALYZE_PEAKS_H

#include <string>
#include <vector>

class Datafield;

namespace Analyze {

//! Returns vector of peak center coordinates, for peaks in given histogram.
std::vector<std::pair<double, double>> FindPeaks(const Datafield& data, double sigma = 2,
                                                 const std::string& option = {},
                                                 double threshold = 0.05);

} // namespace Analyze

#endif // BORNAGAIN_DEVICE_ANALYZE_PEAKS_H
