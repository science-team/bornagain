//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Beam/Beam.h
//! @brief     Defines class Beam.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_BEAM_BEAM_H
#define BORNAGAIN_DEVICE_BEAM_BEAM_H

#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"
#include <heinz/Vectors3D.h>

class IFootprint;
class SpinMatrix;

//! An incident neutron or x-ray beam.

class Beam : public ICloneable, public INode {
public:
    Beam(double intensity, double wavelength, double alpha, double phi = 0);
    ~Beam() override;

    std::string className() const final { return "Beam"; }

    //... Setters:
    void setFootprint(const IFootprint* shape_factor);
    void setPolarization(const R3& polarization);

#ifndef SWIG
    Beam* clone() const override;

    //... Setters:
    void setIntensity(double intensity);
    void setWavelength(double wavelength);
    void setGrazingAngle(double alpha);
    void setAzimuthalAngle(double value);

    std::vector<const INode*> nodeChildren() const override;

    //... Getters
    double intensity() const { return m_intensity; }
    double wavelength() const { return m_wavelength; }
    double wavenumber() const { return m_wavenumber; }
    double alpha_i() const { return m_alpha; }
    double phi_i() const { return m_phi; }
    R3 ki() const { return m_k; }

    //! Returns polarization density as Bloch vector
    const R3& polVector() const { return m_polarization; }
    //! Returns the polarization density matrix (in spin basis along z-axis)
    SpinMatrix polMatrix() const;

    //! Returns footprint factor.
    const IFootprint* footprint() const { return m_footprint.get(); }
#endif // SWIG

private:
    Beam(); // needed by Swig
#ifndef SWIG

    void precompute();

    double m_intensity; //!< beam intensity (neutrons/sec)
    double m_wavelength;
    double m_alpha;
    double m_phi;
    std::shared_ptr<IFootprint> m_footprint; //!< footprint correction handler
    R3 m_polarization;                       //!< Bloch vector encoding the beam's polarization

    //... Cached:
    double m_wavenumber;
    R3 m_k;
#endif // SWIG
};

#endif // BORNAGAIN_DEVICE_BEAM_BEAM_H
