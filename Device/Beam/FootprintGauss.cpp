//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Beam/FootprintGauss.cpp
//! @brief     Implements class FootprintGauss.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Device/Beam/FootprintGauss.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Functions.h"
#include "Base/Util/Assert.h"

using PhysConsts::pi;

FootprintGauss::FootprintGauss(const std::vector<double> P)
    : IFootprint(P)
{
    validateOrThrow();
}

FootprintGauss::FootprintGauss(double width_ratio)
    : FootprintGauss(std::vector<double>{width_ratio})
{
}

FootprintGauss* FootprintGauss::clone() const
{
    return new FootprintGauss(m_width_ratio);
}

double FootprintGauss::calculate(double alpha) const
{
    ASSERT(m_validated);
    if (alpha < 0.0 || alpha > (pi / 2))
        return 0.0;
    if (widthRatio() == 0.0)
        return 1.0;
    const double arg = std::sin(alpha) * sqrt(0.5) / widthRatio();
    return Math::erf(arg);
}
