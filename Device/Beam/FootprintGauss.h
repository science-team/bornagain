//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Beam/FootprintGauss.h
//! @brief     Defines class FootprintGauss.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_BEAM_FOOTPRINTGAUSS_H
#define BORNAGAIN_DEVICE_BEAM_FOOTPRINTGAUSS_H

#include "Device/Beam/IFootprint.h"

//! Gaussian beam footprint.
//!
//! Beam width is the full width at half maximum.

class FootprintGauss : public IFootprint {
public:
    FootprintGauss(std::vector<double> P);
    FootprintGauss(double width_ratio);

#ifndef SWIG
    FootprintGauss* clone() const override;
#endif // SWIG

    std::string className() const final { return "FootprintGauss"; }

    //! Calculate footprint correction coefficient from the beam incident angle _alpha_.
    double calculate(double alpha) const override;
};

#endif // BORNAGAIN_DEVICE_BEAM_FOOTPRINTGAUSS_H
