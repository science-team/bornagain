//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Beam/FootprintSquare.cpp
//! @brief     Implements class FootprintSquare.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Device/Beam/FootprintSquare.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Util/Assert.h"
#include <algorithm>
#include <cmath>

using PhysConsts::pi;

FootprintSquare::FootprintSquare(const std::vector<double> P)
    : IFootprint(P)
{
    validateOrThrow();
}

FootprintSquare::FootprintSquare(double width_ratio)
    : FootprintSquare(std::vector<double>{width_ratio})
{
}

FootprintSquare* FootprintSquare::clone() const
{
    return new FootprintSquare(m_width_ratio);
}

double FootprintSquare::calculate(double alpha) const
{
    ASSERT(m_validated);
    if (alpha < 0.0 || alpha > (pi / 2))
        return 0.0;
    if (widthRatio() == 0.0)
        return 1.0;
    const double arg = std::sin(alpha) / widthRatio();
    return std::min(arg, 1.0);
}
