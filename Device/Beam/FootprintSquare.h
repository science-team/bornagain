//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Beam/FootprintSquare.h
//! @brief     Defines class FootprintSquare.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_BEAM_FOOTPRINTSQUARE_H
#define BORNAGAIN_DEVICE_BEAM_FOOTPRINTSQUARE_H

#include "Device/Beam/IFootprint.h"

//! Rectangular beam footprint.

class FootprintSquare : public IFootprint {
public:
    FootprintSquare(std::vector<double> P);
    FootprintSquare(double width_ratio);

#ifndef SWIG
    FootprintSquare* clone() const override;
#endif // SWIG

    std::string className() const final { return "FootprintSquare"; }

    //! Calculate footprint correction coefficient from the beam incident angle _alpha_.
    double calculate(double alpha) const override;
};

#endif // BORNAGAIN_DEVICE_BEAM_FOOTPRINTSQUARE_H
