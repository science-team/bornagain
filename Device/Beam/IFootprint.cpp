//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Beam/IFootprint.cpp
//! @brief     Implements interface IFootprint.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Device/Beam/IFootprint.h"
#include <stdexcept>

IFootprint::IFootprint(const std::vector<double>& PValues)
    : INode(PValues)
    , m_width_ratio(m_P[0])
{
    if (m_P[0] < 0.0)
        throw std::runtime_error("Error in IFootprint::setWidthRatio: width ratio is negative");
}

IFootprint::~IFootprint() = default;

std::string IFootprint::validate() const
{
    std::vector<std::string> errs;
    requestGe0(errs, m_width_ratio, "width_ratio");
    if (!errs.empty())
        return jointError(errs);

    m_validated = true;
    return "";
}
