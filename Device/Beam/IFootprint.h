//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Beam/IFootprint.h
//! @brief     Defines interface IFootprint.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_BEAM_IFOOTPRINT_H
#define BORNAGAIN_DEVICE_BEAM_IFOOTPRINT_H

#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"

//! Abstract base for classes that calculate the beam footprint factor

class IFootprint : public ICloneable, public INode {
public:
    IFootprint(const std::vector<double>& PValues);
    ~IFootprint() override;

#ifndef SWIG
    IFootprint* clone() const override = 0;
#endif // SWIG

    std::vector<ParaMeta> parDefs() const final { return {{"BeamToSampleWidthRatio", ""}}; }

    double widthRatio() const { return m_width_ratio; }

    //! Calculate footprint correction coefficient from the beam incident angle _alpha_.
    virtual double calculate(double alpha) const = 0;

    std::string validate() const override;

protected:
    const double& m_width_ratio; //!< Beam to sample width ratio

private:
    void initialize();
};

#endif // BORNAGAIN_DEVICE_BEAM_IFOOTPRINT_H
