//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Data/DataUtil.h
//! @brief     Defines namespace DataUtil.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_DEVICE_DATA_DATAUTIL_H
#define BORNAGAIN_DEVICE_DATA_DATAUTIL_H

#include "Base/Type/Field2D.h"
#include <string>
#include <vector>

class Datafield;

namespace DataUtil {

//! Returns new object with input data rotated by
//! n*90 deg counterclockwise (n > 0) or clockwise (n < 0)
//! Axes are swapped if the data is effectively rotated by 90 or 270 degrees
//! Applicable to 2D arrays only
Datafield rotatedDatafield(const Datafield& data, int n);

//! Changes the direction of rows or columns
double2d_t invertAxis(int axis, const double2d_t& original);

//! Transposes the matrix
double2d_t transpose(const double2d_t& original);

//! Creates a vector of vectors of double (2D Array) from Datafield.
double2d_t create2DArrayfromDatafield(const Datafield& data);

//! Returns Datafield representing relative difference of two histograms.
Datafield relativeDifferenceField(const Datafield& dat, const Datafield& ref);

} // namespace DataUtil

#endif // BORNAGAIN_DEVICE_DATA_DATAUTIL_H
