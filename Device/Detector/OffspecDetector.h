//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Detector/OffspecDetector.h
//! @brief     Defines class OffspecDetector.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_DETECTOR_OFFSPECDETECTOR_H
#define BORNAGAIN_DEVICE_DETECTOR_OFFSPECDETECTOR_H

#include "Device/Pol/PolFilter.h"
#include "Param/Node/INode.h"

class IDetectorResolution;
class Pixel;
class Scale;

//! A detector with coordinate axes along angles phi and alpha.

class OffspecDetector : /*public ICloneable,*/ public INode {
public:
    //! Returns a detector with given phi and alpha axes.
    //! @param n_phi number of phi-axis bins
    //! @param phi_min low edge of first phi-bin
    //! @param phi_max upper edge of last phi-bin
    //! @param n_alpha number of alpha-axis bins
    //! @param alpha_min low edge of first alpha-bin
    //! @param alpha_max upper edge of last alpha-bin
    OffspecDetector(size_t n_phi, double phi_min, double phi_max, size_t n_alpha, double alpha_min,
                    double alpha_max);

    OffspecDetector(const OffspecDetector& other);
    ~OffspecDetector() override = default;

#ifndef SWIG
    OffspecDetector* clone() const /* override */;
#endif // SWIG

    std::string className() const override { return "OffspecDetector"; }

    //! Sets the polarization analyzer characteristics of the detector
    void setAnalyzer(const R3& Bloch_vector = {}, double mean_transmission = 0.5);

    //! One axis of the complete detector.
    //! Any region of interest is not taken into account.
    const Scale& axis(size_t index) const;

    //! Calculate axis index for given global index
    size_t axisBinIndex(size_t index, size_t k_axis) const;

    //! Returns total number of pixels.
    //! Any region of interest is not taken into account.
    size_t totalSize() const;

    //! Creates an Pixel for the given Datafield object and index
    Pixel* createPixel(size_t index) const;

    //! Returns index of pixel that contains the specular wavevector.
    //! If no pixel contains this specular wavevector, the number of pixels is
    //! returned. This corresponds to an overflow index.
    size_t indexOfSpecular(double alpha, double phi) const;

    //! Returns detection properties
    const PolFilter& analyzer() const { return m_pol_analyzer; }

private:
    //! Returns flattened index computed from two axis indices.
    size_t getGlobalIndex(size_t x, size_t y) const;

    std::shared_ptr<Scale> m_axes[2];
    PolFilter m_pol_analyzer;
    std::shared_ptr<IDetectorResolution> m_resolution;
};

#endif // BORNAGAIN_DEVICE_DETECTOR_OFFSPECDETECTOR_H
