//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Detector/SphericalDetector.h
//! @brief     Defines class SphericalDetector.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_DETECTOR_SPHERICALDETECTOR_H
#define BORNAGAIN_DEVICE_DETECTOR_SPHERICALDETECTOR_H

#include "Device/Detector/IDetector.h"

class Pixel;

class SphericalDetector : public IDetector {
public:
    explicit SphericalDetector(size_t n_phi, double phi_min, double phi_max, size_t n_alpha,
                               double alpha_min, double alpha_max);

    ~SphericalDetector() override = default;

#ifndef SWIG
    SphericalDetector* clone() const override;
#endif // SWIG

    std::string className() const override { return "SphericalDetector"; }

    //! Creates an Pixel for the given Datafield object and index
    Pixel* createPixel(size_t index) const override;

    //! Returns index of pixel that contains the specular wavevector.
    //! If no pixel contains this specular wavevector, the number of pixels is
    //! returned. This corresponds to an overflow index.
    size_t indexOfSpecular(const Beam& beam) const override;
};

#endif // BORNAGAIN_DEVICE_DETECTOR_SPHERICALDETECTOR_H
