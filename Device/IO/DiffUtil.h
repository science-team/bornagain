//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/IO/DiffUtil.h
//! @brief     Defines namespace DataUtil.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_IO_DIFFUTIL_H
#define BORNAGAIN_DEVICE_IO_DIFFUTIL_H

#include <memory>
#include <vector>

class Datafield;

namespace DiffUtil {

//! Returns true is relative difference is below threshold; prints informative output
bool checkRelVecDifference(const std::vector<double>& dat, const std::vector<double>& ref,
                           double threshold);

//! Returns true is relative difference is below threshold; prints informative output
bool checkRelativeDifference(const Datafield& dat, const Datafield& ref, double threshold);

//! Returns true is relative and absolute differences are within given limits
bool checkConsistence(const Datafield& dat, const Datafield& ref, double precision, double snr,
                      int allowed_outliers = 0);

} // namespace DiffUtil

#endif // BORNAGAIN_DEVICE_IO_DIFFUTIL_H
