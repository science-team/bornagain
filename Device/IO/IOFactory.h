//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/IO/IOFactory.h
//! @brief     Declares data import/export functions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_IO_IOFACTORY_H
#define BORNAGAIN_DEVICE_IO_IOFACTORY_H

#include <string>

class Datafield;
struct ImportSettings1D;
struct ImportSettings2D;

namespace IO {

enum Filetype1D { unknown1D, csv1D, bornagain1D, mft };
enum Filetype2D { unknown2D, csv2D, bornagain2D, tiff, nicos2D };

#ifndef SWIG
Filetype1D filename2type1D(const std::string& fname);
Filetype2D filename2type2D(const std::string& fname);
#endif

//! Reads 1D data file and returns newly created Datafield object.
//! If file name ends with "*.gz" or "*.bz2" the file will be unzipped on the fly.
//! Otherwise the file extension is ignored, since the file type is given by argument.
//! May throw, but will never return nullptr.
Datafield readData1D(const std::string& fname, Filetype1D ftype,
                     const ImportSettings1D* importSettings = nullptr);

//! Reads 1D data file and returns newly created Datafield object.
//! Determines type from file name extension.
Datafield readData1D(const std::string& fname);

//! Reads 2D data file and returns newly created Datafield object.
//! If file name ends with "*.gz" or "*.bz2" the file will be unzipped on the fly.
//! Otherwise the file extension is ignored, since the file type is given by argument.
//! May throw, but will never return nullptr.
Datafield readData2D(const std::string& fname, Filetype2D ftype,
                     const ImportSettings2D* importSettings = nullptr);

//! Reads 2D data file and returns newly created Datafield object.
//! Determines type from file name extension.
Datafield readData2D(const std::string& fname);

//! Writes Datafield to file.
//! The format is determined by the file extension.
//! If file name ends with "*.gz" or "*.bz2" the file will be zipped on the fly.
void writeDatafield(const Datafield& data, const std::string& fname);

} // namespace IO

namespace IO::Test {

//! Returns true if data matches reference file. For use in tests.
bool dataMatchesFile(const Datafield& data, const std::string& refFileName, double tol);

} // namespace IO::Test

#endif // BORNAGAIN_DEVICE_IO_IOFACTORY_H
