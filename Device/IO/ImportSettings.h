//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/IO/ImportSettings.h
//! @brief     Declares import settings structs.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_IO_IMPORTSETTINGS_H
#define BORNAGAIN_DEVICE_IO_IMPORTSETTINGS_H

#include "Base/Axis/Coordinate.h"
#include <string>

//! This parameterization can be set interactively by GUI users.
//! Therefore, exceptionally, indices are counting from 1, not  from 0.

struct ImportSettings1D {
    ImportSettings1D(Coordinate _xCoord, std::string _prefix, std::string _skip, size_t __Q,
                     size_t __R, size_t __sR = 0, size_t __dQ = 0, size_t __lambda = 0,
                     bool _sort = true, bool _rm_neg = true, bool _rm_dup = true)
        : xCoord(_xCoord)
        , headerPrefix(_prefix)
        , linesToSkip(_skip)
        , col_Q(__Q)
        , col_R(__R)
        , col_sR(__sR)
        , col_dQ(__dQ)
        , col_lambda(__lambda)
        , sort(_sort)
        , rm_negative(_rm_neg)
        , rm_duplications(_rm_dup)
    {
    }
    ImportSettings1D(const char* _xCoord, std::string _prefix, std::string _skip, size_t __Q,
                     size_t __R, size_t __sR = 0, size_t __dQ = 0, size_t __lambda = 0,
                     bool _sort = true, bool _rm_neg = true, bool _rm_dup = true)
        : ImportSettings1D(Coordinate(_xCoord), _prefix, _skip, __Q, __R, __sR, __dQ, __lambda,
                           _sort, _rm_neg, _rm_dup)
    {
    }

    Coordinate xCoord;
    std::string headerPrefix; //!< prefix of header lines (usually a single character like "#")
    std::string linesToSkip;  //!< pattern denoting line to skip (i.e. '1,10-12,42')
    size_t col_Q;             //!< column number of Q (counting from 1!)
    size_t col_R;             //!< column number of R
    size_t col_sR;            //!< column number of sigma R, or 0
    size_t col_dQ;            //!< column number of delta Q, or 0
    size_t col_lambda;        //!< column number of wavelength, or 0
    bool sort;                //!< sort lines by argument
    bool rm_negative;         //!< discard lines with negative arguments
    bool rm_duplications;     //!< discard lines with duplicated arguments
    // bool operator!=(const ImportSettings& other) const;
    // QByteArray serialize() const;
    // void deserialize(const QByteArray& data);
};

//! This parameterization can be set interactively by GUI users.
//! Therefore, exceptionally, indices are counting from 1, not  from 0.

struct ImportSettings2D {
    ImportSettings2D(Coordinate _xCoord, Coordinate _yCoord, bool _axes = false, bool _swap = false,
                     bool _first_row = true, bool _first_col = true)
        : xCoord(_xCoord)
        , yCoord(_yCoord)
        , has_axes(_axes)
        , swap_axes(_swap)
        , first_row(_first_row)
        , first_col(_first_col)
    {
    }
    ImportSettings2D(const char* _xCoord, const char* _yCoord, bool _axes = false,
                     bool _swap = false, bool _first_row = true, bool _first_col = true)
        : ImportSettings2D(Coordinate(_xCoord), Coordinate(_yCoord), _axes, _swap, _first_row,
                           _first_col)
    {
    }

    Coordinate xCoord;
    Coordinate yCoord;
    bool has_axes;  //!< read axes or not
    bool swap_axes; //!< orientation: x <-> y
    bool first_row; //!< axis row: first or last
    bool first_col; //!< axis column: first or last
};

#endif // BORNAGAIN_DEVICE_IO_IMPORTSETTINGS_H
