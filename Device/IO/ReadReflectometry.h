//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/IO/ReadReflectometry.h
//! @brief     Declares function readReflectometryTable.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_DEVICE_IO_READREFLECTOMETRY_H
#define BORNAGAIN_DEVICE_IO_READREFLECTOMETRY_H

#include <istream>

class Datafield;
struct ImportSettings1D;

namespace Util::RW {

//! Read reflectometry scan from not-self-documenting table.
Datafield readReflectometryTable(std::istream& s, const ImportSettings1D& p);

//! Read reflectometry scan from Motofit mft file.
Datafield readMotofit(std::istream& s);

} // namespace Util::RW

#endif // BORNAGAIN_DEVICE_IO_READREFLECTOMETRY_H
