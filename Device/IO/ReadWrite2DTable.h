//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/IO/ReadWrite2DTable.h
//! @brief     Declares functions read|writeNumpyTxt.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_DEVICE_IO_READWRITE2DTABLE_H
#define BORNAGAIN_DEVICE_IO_READWRITE2DTABLE_H

#include <istream>

class Datafield;
struct ImportSettings2D;

namespace Util::RW {

//! Reads 2D table from ASCII file.
Datafield read2DTable(std::istream& input_stream, const ImportSettings2D* pars = nullptr);
//! Writes Datafield to ASCII file with 2D table layout as in numpy.savetxt.
void write2DTable(const Datafield& data, std::ostream& output_stream);

} // namespace Util::RW

#endif // BORNAGAIN_DEVICE_IO_READWRITE2DTABLE_H
