//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/IO/ReadWriteINT.h
//! @brief     Declares functions read|writeBAInt.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_DEVICE_IO_READWRITEINT_H
#define BORNAGAIN_DEVICE_IO_READWRITEINT_H

#include <iostream>

class Datafield;

namespace Util::RW {

//! Write Datafield as BornAgain intensity ASCII file.
Datafield readBAInt(std::istream& input_stream);
//! Read Datafield from BornAgain intensity ASCII file.
void writeBAInt(const Datafield& data, std::ostream& output_stream);

} // namespace Util::RW

#endif // BORNAGAIN_DEVICE_IO_READWRITEINT_H
