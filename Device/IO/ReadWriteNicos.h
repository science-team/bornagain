//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/IO/ReadWriteNicos.h
//! @brief     Declares function Util::RW::readNicos.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_IO_READWRITENICOS_H
#define BORNAGAIN_DEVICE_IO_READWRITENICOS_H

#include <iostream>

class Datafield;

namespace Util::RW {

//! Read/write SANSDRaw files written by Nicos (*.001).
Datafield readNicos(std::istream& input_stream);

}; // namespace Util::RW

#endif // BORNAGAIN_DEVICE_IO_READWRITENICOS_H
