//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/IO/ReadWriteTiff.h
//! @brief     Declares functions readTiff, writeTiff.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_DEVICE_IO_READWRITETIFF_H
#define BORNAGAIN_DEVICE_IO_READWRITETIFF_H
#ifdef BA_TIFF_SUPPORT

#include <iostream>

class Datafield;

namespace Util::RW {

Datafield readTiff(std::istream& input_stream);
void writeTiff(const Datafield& data, std::ostream& output_stream);

} // namespace Util::RW

#endif // BA_TIFF_SUPPORT
#endif // BORNAGAIN_DEVICE_IO_READWRITETIFF_H
