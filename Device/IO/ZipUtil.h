//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/IO/ZipUtil.h
//! @brief     Declares functions in namespace ZipUtil.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_DEVICE_IO_ZIPUTIL_H
#define BORNAGAIN_DEVICE_IO_ZIPUTIL_H

#include <sstream>
#include <string>

namespace ZipUtil {

std::string uncompressedExtension(std::string fname);

std::stringstream file2stream(const std::string& fname);

void stream2file(const std::string& fname, std::stringstream& s);

} // namespace ZipUtil

#endif // BORNAGAIN_DEVICE_IO_ZIPUTIL_H
