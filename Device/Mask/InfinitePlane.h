//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Mask/InfinitePlane.h
//! @brief     Defines class InfinitePlane.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_DEVICE_MASK_INFINITEPLANE_H
#define BORNAGAIN_DEVICE_MASK_INFINITEPLANE_H

#include "Device/Mask/IShape2D.h"

//! The infinite plane is used for masking the entire detector.

class InfinitePlane : public IShape2D {
public:
    InfinitePlane()
        : IShape2D("InfinitePlane")
    {
    }

#ifndef SWIG
    InfinitePlane* clone() const override { return new InfinitePlane(); }
#endif // SWIG

    bool contains(double, double) const override { return true; }
    bool contains(const Bin1D&, const Bin1D&) const override { return true; }
};

#endif // BORNAGAIN_DEVICE_MASK_INFINITEPLANE_H
