//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Mask/Line.h
//! @brief     Defines class Line.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_MASK_LINE_H
#define BORNAGAIN_DEVICE_MASK_LINE_H

#include "Device/Mask/IShape2D.h"

//! A line segment, for use in detector masks.

class Line : public IShape2D {
public:
    Line(double x1, double y1, double x2, double y2);

#ifndef SWIG
    Line* clone() const override { return new Line(m_x1, m_y1, m_x2, m_y2); }
#endif // SWIG

    bool contains(double x, double y) const override;
    bool contains(const Bin1D& binx, const Bin1D& biny) const override;

private:
    double m_x1, m_y1, m_x2, m_y2;
};

//! An infinite vertical line.

class VerticalLine : public IShape2D {
public:
    VerticalLine(double x);

#ifndef SWIG
    VerticalLine* clone() const override { return new VerticalLine(m_x); }
#endif // SWIG

    bool contains(double x, double y) const override;
    bool contains(const Bin1D& binx, const Bin1D& biny) const override;

    double getXpos() const { return m_x; }

private:
    double m_x;
};

//! An infinite horizontal line.

class HorizontalLine : public IShape2D {
public:
    HorizontalLine(double y);

#ifndef SWIG
    HorizontalLine* clone() const override { return new HorizontalLine(m_y); }
#endif // SWIG

    bool contains(double x, double y) const override;
    bool contains(const Bin1D& binx, const Bin1D& biny) const override;

    double getYpos() const { return m_y; }

private:
    double m_y;
};

#endif // BORNAGAIN_DEVICE_MASK_LINE_H
