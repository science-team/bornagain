//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Mask/MaskStack.h
//! @brief     Defines class MaskStack.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif
#ifndef BORNAGAIN_DEVICE_MASK_MASKSTACK_H
#define BORNAGAIN_DEVICE_MASK_MASKSTACK_H

#include "Base/Type/OwningVector.h"
#include <memory>

class Frame;
class IShape2D;
struct MaskPattern;

//! Collection of detector masks.

class MaskStack {
public:
    MaskStack();
    ~MaskStack();

#ifndef SWIG
    MaskStack* clone() const;
#endif // SWIG

    //! Add mask to the stack of detector masks.
    //! Argument mask_value=true means that the area will be excluded from the analysis.
    void pushMask(const IShape2D& shape, bool mask_value);

    bool isMasked(size_t i_flat, const Frame& frame) const;

    //! Returns true if has masks
    bool hasMasks() const;

    size_t numberOfMasks() const;

    std::pair<IShape2D*, bool> patternAt(size_t iMask) const;

private:
    OwningVector<MaskPattern> m_stack;
};

#endif // BORNAGAIN_DEVICE_MASK_MASKSTACK_H
