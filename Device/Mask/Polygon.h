//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Mask/Polygon.h
//! @brief     Defines class Polygon.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_MASK_POLYGON_H
#define BORNAGAIN_DEVICE_MASK_POLYGON_H

#include "Device/Mask/IShape2D.h"
#include <vector>

class PolygonPrivate;

//! A polygon, for use in detector masks.

//! Polygon defined by two arrays with x and y coordinates of points.
//! Sizes of arrays should coincide.  If polygon is unclosed (the last point
//! doesn't repeat the first one), it will be closed automatically.

// Implemented in pimpl style (m_d points to implementation class).
// TODO: simplify, merge classes PolygonPrivate and Polygon.

class Polygon : public IShape2D {
public:
    Polygon(std::vector<std::pair<double, double>> points);
    Polygon(const PolygonPrivate* d);

    ~Polygon() override;

#ifndef SWIG
    Polygon* clone() const override { return new Polygon(m_d); }
#endif // SWIG

    bool contains(double x, double y) const override;
    bool contains(const Bin1D& binx, const Bin1D& biny) const override;

    double getArea() const;

    void getPoints(std::vector<double>& xpos, std::vector<double>& ypos) const;

protected:
    void print(std::ostream& ostr) const override;

private:
    PolygonPrivate* m_d;
};

#endif // BORNAGAIN_DEVICE_MASK_POLYGON_H
