//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Pol/PolFilter.cpp
//! @brief     Implements class DetectionProperties.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Device/Pol/PolFilter.h"
#include "Base/Spin/SpinMatrix.h"
#include "Base/Util/Assert.h"

PolFilter::PolFilter(R3 Bloch_vector, double mean_transmission)
    : m_vec_bloch(Bloch_vector)
    , m_transmission(mean_transmission)
{
    if (m_transmission < 0 || m_transmission > 0.5)
        throw std::runtime_error("Invalid analyzer transmission");
    if (m_vec_bloch.mag2() > 1)
        throw std::runtime_error("Invalid analyzer Bloch vector");
}

SpinMatrix PolFilter::matrix() const
{
    return 2 * m_transmission * SpinMatrix::FromBlochVector(m_vec_bloch);
}
