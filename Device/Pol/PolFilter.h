//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Pol/PolFilter.h
//! @brief     Defines class DetectionProperties.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_DEVICE_POL_POLFILTER_H
#define BORNAGAIN_DEVICE_POL_POLFILTER_H

#include "Param/Node/INode.h"
#include <heinz/Vectors3D.h>

class SpinMatrix;

//! Polarization filter, characterized by Bloch vector and transmission.

class PolFilter : public INode {
public:
    PolFilter(R3 Bloch_vector = {}, double mean_transmission = 0.5);
    ~PolFilter() override = default;

    std::string className() const final { return "PolFilter"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Transmission", ""}}; }

    //! Return the polarization density matrix (in spin basis along z-axis)
    SpinMatrix matrix() const;

    R3 BlochVector() const { return m_vec_bloch; }
    double transmission() const { return m_transmission; }

private:
    R3 m_vec_bloch;        //!< Bloch vector (efficiency * polarization_direction)
    double m_transmission; //!< transmission of unpolarized beam
};

#endif // BORNAGAIN_DEVICE_POL_POLFILTER_H
