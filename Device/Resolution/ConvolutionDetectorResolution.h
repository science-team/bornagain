//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Resolution/ConvolutionDetectorResolution.h
//! @brief     Defines class ConvolutionDetectorResolution.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_DEVICE_RESOLUTION_CONVOLUTIONDETECTORRESOLUTION_H
#define BORNAGAIN_DEVICE_RESOLUTION_CONVOLUTIONDETECTORRESOLUTION_H

#include "Device/Resolution/IDetectorResolution.h"
#include "Device/Resolution/IResolutionFunction2D.h"

//! Convolutes the intensity in 1 or 2 dimensions with a resolution function.

//! Limitation: this class assumes that the data points are evenly distributed on each axis

class ConvolutionDetectorResolution : public IDetectorResolution {
public:
    using cumulative_DF_1d = double (*)(double);

    //! Constructor taking a 1 dimensional resolution function as argument.
    ConvolutionDetectorResolution(cumulative_DF_1d res_function_1d);

    //! Constructor taking a 2 dimensional resolution function as argument.
    ConvolutionDetectorResolution(const IResolutionFunction2D& p_res_function_2d);

    ~ConvolutionDetectorResolution() override;

#ifndef SWIG
    ConvolutionDetectorResolution* clone() const override;

    std::vector<const INode*> nodeChildren() const override;
#endif // SWIG

    std::string className() const final { return "ConvolutionDetectorResolution"; }

    //! Convolve given intensities with the encapsulated resolution.
    void execDetectorResolution(Datafield* df) const override;

    const IResolutionFunction2D* getResolutionFunction2D() const;

protected:
    ConvolutionDetectorResolution(const ConvolutionDetectorResolution& other);

private:
    void setResolutionFunction(const IResolutionFunction2D& resFunc);
    void apply1dConvolution(Datafield* df) const;
    void apply2dConvolution(Datafield* df) const;
    double getIntegratedPDF1d(double x, double step) const;
    double getIntegratedPDF2d(double x, double step_x, double y, double step_y) const;

    size_t m_rank;
    cumulative_DF_1d m_res_function_1d;
    std::unique_ptr<IResolutionFunction2D> m_res_function_2d;
};

inline const IResolutionFunction2D* ConvolutionDetectorResolution::getResolutionFunction2D() const
{
    return m_res_function_2d.get();
}

#endif // BORNAGAIN_DEVICE_RESOLUTION_CONVOLUTIONDETECTORRESOLUTION_H
