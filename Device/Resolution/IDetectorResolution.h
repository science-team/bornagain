//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Resolution/IDetectorResolution.h
//! @brief     Defines interface IDetectorResolution.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_RESOLUTION_IDETECTORRESOLUTION_H
#define BORNAGAIN_DEVICE_RESOLUTION_IDETECTORRESOLUTION_H

#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"

class Datafield;

//! Interface for detector resolution algorithms

class IDetectorResolution : public ICloneable, public INode {
public:
    ~IDetectorResolution() override = default;

#ifndef SWIG
    IDetectorResolution* clone() const override = 0;
#endif // SWIG

    //! Apply the resolution function to the intensity data
    virtual void execDetectorResolution(Datafield* p_intensity_map) const = 0;
};

#endif // BORNAGAIN_DEVICE_RESOLUTION_IDETECTORRESOLUTION_H
