//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Resolution/IResolutionFunction2D.h
//! @brief     Defines class interface IResolutionFunction2D.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_RESOLUTION_IRESOLUTIONFUNCTION2D_H
#define BORNAGAIN_DEVICE_RESOLUTION_IRESOLUTIONFUNCTION2D_H

#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"

//! Interface providing two-dimensional resolution function.

class IResolutionFunction2D : public ICloneable, public INode {
public:
    IResolutionFunction2D() = default;
    IResolutionFunction2D(const std::vector<double>& PValues);

    ~IResolutionFunction2D() override = default;

    virtual double evaluateCDF(double x, double y) const = 0;

#ifndef SWIG
    IResolutionFunction2D* clone() const override = 0;
#endif // SWIG
};

#endif // BORNAGAIN_DEVICE_RESOLUTION_IRESOLUTIONFUNCTION2D_H
