//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Resolution/ResolutionFunction2DGaussian.cpp
//! @brief     Implements class ResolutionFunction2DGaussian.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Device/Resolution/ResolutionFunction2DGaussian.h"
#include "Base/Math/Functions.h"
#include "Base/Util/Assert.h"

ResolutionFunction2DGaussian::ResolutionFunction2DGaussian(double sigma_x, double sigma_y)
    : m_sigma_x(sigma_x)
    , m_sigma_y(sigma_y)
{
    validateOrThrow();
}

double ResolutionFunction2DGaussian::evaluateCDF(double x, double y) const
{
    ASSERT(m_validated);
    return Math::IntegratedGaussian(x, 0.0, m_sigma_x)
           * Math::IntegratedGaussian(y, 0.0, m_sigma_y);
}

std::string ResolutionFunction2DGaussian::validate() const
{
    std::vector<std::string> errs;
    requestGe0(errs, m_sigma_x, "sigma_x");
    requestGe0(errs, m_sigma_y, "sigma_y");
    if (!errs.empty())
        return jointError(errs);
    m_validated = true;
    return "";
}
