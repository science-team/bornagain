//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Device/Resolution/ResolutionFunction2DGaussian.h
//! @brief     Defines class ResolutionFunction2DGaussian.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_DEVICE_RESOLUTION_RESOLUTIONFUNCTION2DGAUSSIAN_H
#define BORNAGAIN_DEVICE_RESOLUTION_RESOLUTIONFUNCTION2DGAUSSIAN_H

#include "Device/Resolution/IResolutionFunction2D.h"

//! Simple gaussian two-dimensional resolution function.

class ResolutionFunction2DGaussian : public IResolutionFunction2D {
public:
    ResolutionFunction2DGaussian(double sigma_x, double sigma_y);

#ifndef SWIG
    ResolutionFunction2DGaussian* clone() const override
    {
        return new ResolutionFunction2DGaussian(m_sigma_x, m_sigma_y);
    }
#endif // SWIG

    std::string className() const final { return "ResolutionFunction2DGaussian"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Sigma_x", "?"}, {"Sigma_y", "?"}}; }

    double evaluateCDF(double x, double y) const override;

    double sigmaX() const { return m_sigma_x; }
    double sigmaY() const { return m_sigma_y; }

    std::string validate() const override;

private:
    ResolutionFunction2DGaussian& operator=(const ResolutionFunction2DGaussian&);

    double m_sigma_x;
    double m_sigma_y;
};

#endif // BORNAGAIN_DEVICE_RESOLUTION_RESOLUTIONFUNCTION2DGAUSSIAN_H
