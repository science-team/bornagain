To build the documentation,
```
cd build
# configure:
cmake -DCONFIGURE_DOXY=ON ..
# make:
make doxycore
make doxygui
make doxy     # to build all
```

The configure step writes Doxyfiles to the directory
`<build>/doxygen`. Do not edit these files.

The make step writes the documentation to directories
`<build>/html/user`, `<build>/html/long`.
