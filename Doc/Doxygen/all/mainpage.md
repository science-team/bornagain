## BornAgain developer reference {#mainpage}

C++ developer reference of BornAgain kernel and GUI.
<a href="http://www.bornagainproject.org">BornAgain</a> is
 an open-source software to simulate and fit
 neutron and x-ray reflectometry and grazing-incidence small-angle scattering.

### License

[GNU General Public License v3 or higher](https://jugit.fz-juelich.de/mlz/bornagain/-/blob/main/COPYING)

Copyright Forschungszentrum Jülich GmbH 2015-

### Authors

Scientific Computing Group at MLZ Garching.

See file [AUTHORS](https://jugit.fz-juelich.de/mlz/bornagain/-/blob/main/AUTHORS).

### Citation

See file [CITATION](https://jugit.fz-juelich.de/mlz/bornagain/-/blob/main/CITATION).
