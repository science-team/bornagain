\documentclass[11pt,a4paper,fleqn]{report}

\usepackage{amssymb,amsmath,bm}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage[top=3cm, left=3cm, bottom=3cm, right=3cm]{geometry}

\def\version{0}
\def\shorttitle{Potential Chapter of BornAgain Physics Reference}
\def\longtitle{\shorttitle}

\input{Setup}

\hyphenation{
Born-Again
equi-dis-tant
Laz-za-ri
MacOS
nano-par-ti-cle nano-par-ti-cles
para-crys-tal
wave-num-ber
Wuttke}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{/G/sw/ba/Doc/PhysRef/Macros.tex}
\def\AVG<#1>{\left\langle#1\right\rangle}
\def\transpose{\mathsf{T}}

\noindent
\textbf{{\huge Form factors averaged over particle sizes and orientations}}\\[1cm]
{\large Incomplete or withdrawn chapter of Physics Reference\\
Joachim Wuttke\\
Scientific Computing Group of JCNS-MLZ}\\[.5cm]
Last change \today.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{TODO}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%==================================================================================================%
\subsection{Literature}
%==================================================================================================%

Schulz distribution \cite{ArPe76,KoCH83}

Orientational averages:
sphere, rod, disk \cite{ArPe76}
spheroid (approximative) \cite{KoCH83}.

Porod limit \cite{Por51}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%==================================================================================================%
\subsection{General formalism for form factor averages}\label{Sffa}
%==================================================================================================%

The \emph{form factor amplitude} of particle~$\Pi$ at wave vector~$\q$ is
\begin{equation}
  F(\q) \coloneqq \int_\Pi\!\d^3r\,\e^{i\q\r}.
\end{equation}
Its squared modulus shall be designated as \emph{form factor intensity},
\begin{equation}
  P(\q) \coloneqq {\left|F(\q)\right|}^2.
\end{equation}
The scattering of a particles mixture depends on the averages $\AVG<F(\q)>^2$
and $\AVG<P(\q)>$.

Förster et al \cite{WaFo23}
studied averages under a distribution~$h(R)$ of the particle size~$R$
and a distribution $f(\Omega)$ of the particle orientation~$\Omega$,
\begin{equation}
  \AVG<A(\Pi)>_{R,\Omega}
  \coloneqq \int_0^\infty\!\d R\,h(R) \int_{4\pi} \!\d\Omega\, f(\Omega)\,A(\Pi(R,\Omega)).
\end{equation}
Here we generalize, and at the same time simplify notation,
by considering an operator~$\Gamma$ with associated matrix~$\v\Gamma$
that may rotate, stretch, or deform a simpler geometric body~$\Pi$,
\begin{equation}
  \Gamma\Pi \coloneqq \{ \r \;|\; \v\Gamma^{-1}\r \in \Pi \}.
\end{equation}
Let $\v\Gamma$ have the distribution~$g(\v\Gamma)$.
The average form factor amplitude is
\begin{align}
  \AVG<F(\q;\Gamma\Pi)>_\Gamma
      &= \int\!\d\Gamma\,g(\v\Gamma)\,\int_{\Gamma\Pi}\!\d^3r\,\e^{i\q\r}\\
      &= \int\!\d\Gamma\,g(\v\Gamma)\,\int_\Pi\!\d^3r'\,
                  |\v\Gamma|\e^{i\q\v\Gamma\r'}\label{EFqTraf2}\\
      &= \int\!\d\Gamma\,g(\v\Gamma)\,|\v\Gamma|\,F(\v\Gamma^\transpose\q;\Pi).\label{EFqTraf3}
\end{align}
By the same reasoning,
the average form factor intensity is
\begin{equation}
  \AVG<P(\q)>_\Gamma
    = \int\!\d\Gamma\,g(\v\Gamma)\,{\left|F(\v\Gamma^\transpose\q;\Pi)\right|}^2.
\end{equation}

%==================================================================================================%
\subsection{Size distribution}
%==================================================================================================%

The particle size~$R$ shall be quantified by its ratio
\begin{equation}
  \rho \coloneqq R/ R_0
\end{equation}
relative to a reference size~$R_0$.
In the formalism of \cref{Sffa},
this is described by the matrix
\begin{equation}
  \v\Gamma_\rho = \rho\v1,
\end{equation}
which transforms a particle $\Pi$ of size~$R_0$ into a particle~$\v\Gamma_\rho\Pi$ of size~$R$.
It contributes a factor $|\v\Gamma_\rho|=\rho^3$ to the substitution~\cref{EFqTraf2}.
If the form factor amplitude is~$F_\Pi(\q)$ for the reference particle,
then its average under the size distribution is
\begin{equation}\label{FqAvg1}
  \AVG<F(\q;\Gamma_\rho\Pi)>_\rho
      = \int\!\d\rho\,\rho^3\,h(\rho)\,F(\rho \q;\Pi).
\end{equation}
Averages over particle sizes can easily be combined with averages over particle orientations
because $\v\Gamma$ is just a multiple of the unit matrix
and therefore commutes with any other matrix.

%==================================================================================================%
\subsection{Gamma distribution}
%==================================================================================================%

From here on, we need to assume a specific distribution~$h$.
Following Refs \cite{WaFo23}, we choose the Schulz-Zimm distribution
\begin{equation}
  h(\rho) \equiv p_\gamma(\rho;\kappa,\kappa),
\end{equation}
which is just a gamma distribution
\begin{equation}
  p_\gamma(x;\alpha,\beta)
  \coloneqq \frac{\beta^\alpha}{\Gamma(\alpha)}
       x^{\alpha - 1} e^{-\beta x},
\end{equation}
scaled such that $\AVG<\rho>=\alpha/\beta=1$.
It has the variance $\alpha/\beta^2=1/\kappa$.
In polymer physics, it is parameterized through the dispersity
$z\coloneqq\AVG<\rho^2>=1+1/\kappa$.

Straightforward computation yields the average
\begin{equation}\label{EavgeXE}
  \AVG<x^m \e^{i\eta x}>
  = \frac{\Gamma(\alpha+m)}{\Gamma(\alpha)}\frac{\beta^\alpha}{(\beta-i\eta)^{\alpha+m}}
  = \alpha^{\overline{m}}\frac{\beta^\alpha}{(\beta-i\eta)^{\alpha+m}}
  = \frac{\alpha^{\overline{m}}}{\beta^m}\frac{1}{(1-i\eta/\beta)^{\alpha+m}}
\end{equation}
with the raising factorial $a^{\overline{n}}\coloneqq a(a+1)\cdots(a+n-1)$.

%==================================================================================================%
\subsection{Sphere}
%==================================================================================================%

The form factor of a sphere with radius~$R$ is
\begin{equation}
  F(q; R) = \frac{4\pi}{q^3} \tilde F(qR)
\end{equation}
with
\begin{equation}
  \tilde F(x) = \sin x - x \cos x.
\end{equation}
To make contact with Ref.~\cite{WaFo23}, we note that
$\tilde F(x)= x^2 j_1(x)$ with the spherical Bessel function
\begin{equation}
  j_1(x) \equiv \sqrt{\frac{\pi}{2x}}J_{3/2}(x) = \frac{\sin x - x \cos x}{x^2},
\end{equation}
which can also be expressed as generalized hypergeometric series $\vphantom{F}_0F_1(5/2;\cdot)$.

The average form factor amplitude \cref{FqAvg1} is
\begin{align}
  \AVG<F(\q;\Gamma_\rho\Pi)>_\rho
     &= \frac{4\pi}{q^{3}}\int\!\d \rho\,p_\gamma(\rho;\kappa,\kappa)\,\tilde F(\rho q R_0)\\
     &= -\frac{4\pi}{q^{3}} \Re \int\!\d \rho\,p_\gamma(\rho;\kappa,\kappa)\,
              \left(i\e^{i\rho q R_0} + (\rho q R_0)\e^{i\rho q R_0} \right),
\end{align}
which has the right form to apply~\cref{EavgeXE}:
\begin{align}
  \AVG<F(\q;\Gamma_\rho\Pi)>_\rho
     &= - \frac{4\pi}{q^{3}} \Re \left[\frac{i}{\left(1-iqR_0/\kappa\right)^\kappa}
                + \frac{1}{\left(1-iqR_0/\kappa\right)^{\kappa+1}}\right]\\
&= - \frac{4\pi}{q^{3}} \Re  \frac{i+(\kappa+1)qR_0/\kappa}{(1-iqR_0/\kappa)^{\kappa+1}}.
\end{align}

%==================================================================================================%
%\subsection{}
%==================================================================================================%

\bibliographystyle{switch}
\bibliography{jw7}

\end{document}
