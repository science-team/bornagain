%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%   BornAgain Developers Reference
%%
%%   homepage:   http://www.bornagainproject.org
%%
%%   copyright:  Forschungszentrum Jülich GmbH 2015-
%%
%%   license:    Creative Commons CC-BY-SA
%%
%%   authors:    Scientific Computing Group at MLZ Garching
%%   editor:     Joachim Wuttke
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Evanescent waves}\label{Seva}

\iftodo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Vanishing vertical wavenumber, evanescent case etc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Fragments from different sources...

%===============================================================================
\subsection{Vanishing vertical wavenumber}\label{Snokz}
%===============================================================================

The general solution of \cref{Ewavez} will be a linear function of $z$:
\begin{equation}\label{Ephilz}
  \phi_l(z) = A^0_l + A^1_lz.
\end{equation}
In BornAgain, such a linear wavefunction amplitude can not be handled by the form factors,
which are only defined in terms of plane waves with complex wavevector components.
The following cases are treated seperately:
\begin{itemize}
  \item There is only one layer: this is a trivial case whithout any need to calculate wave coefficients.
    The solution in the single layer is just the incoming/outgoing plane wave.
  \item The top layer of a multilayer has $\kappa_0=0$: the limit $\kappa_0\to0$ is well-defined and the
    solution is given by $A^+_0 = -A^-_0$ and $A^{\pm}_l=0$ for $l>0$.
    % The boundary condition in the substrate imposes a linear relation between the two coefficients
    % of the linear wavefunction amplitude in the top layer. Since the first order term has to vanish, all amplitudes vanish.
  \item $\kappa_l=0$ for a layer with $l>0$: In this case $\kappa_l$ will be given a very small imaginary value,
    representing a slight absorption. However, this should be inconsequential because the index of refraction
    of non-vacuum layer always contains an absorptive component.
\end{itemize}

%==================================================================================================%
\subsection{Opaque layers and evanescent waves}\label{Sevawa}
%==================================================================================================%

TODO: Rework this fragment (ingested 29may23 from ba-intern/theory/Stratified.tex).

For incident angles below the critical angle $\alpha_i < \alpha_c$
(also at interfaces inside the sample), the $k_z$-component of the wave vector turns imaginary.
This situation then corresponds to an evanescent wave.
As a consequence, the phase factor $\delta$ \cref{Ddell}
turns real and describes the exponential decrease
of the amplitudes $t_j$, $r_j$ in the corresponding layer.
For large layer thicknesses, this means that $\delta$ rapidly approaches zero, and its inverse becomes very large.
This leads to an increasingly ill-conditioned transfer matrix (TODO: ref)
until, at some point, both quantities underflow or overflow, leading to invalid numerical results.

As the simulation approaches this singular situation, the amplitudes $t_j$ and $r_j$ will
rapidly increase towards the top of a sample and potentially overflow at some point
before the computation reaches the top layer.
Intuitively, this can easily be understood as follows.
As we impose the boundary condition $t_\nu = 1$, $r_\nu = 0$ in the substrate for incident
angles below the critical angle with $T = 0$, $R = 1$, this means,
that we must find $t_0 \to \infty$.
Obviously, this cannot be implemented numerically in a sane manner.

In order to detect and handle this over/underflow, BornAgain checks for an
overflow of $t_j$.
%As soon as this situation is encountered, the computation is stopped and
%restarted in order to find the deepest layer where a valid numerical result is obtained.
%Practically, this is done by a bisection algorithm for minimizing the computational load.

It must be mentioned though, that this algorithm still fails if a very thick layer is encountered.
This will lead to the immediate underflow of $\delta$ and hence to an overflow of~$\delta^{-1}$.
This means, that the transmission of a single layer is within the numerical precision zero,
but the mathematical formulation applied cannot handle this corner case and the computation still crashes.
Therefore, the only way to circumvent this problem is by restarting the computation from the
current layer by reapplying the boundary condition $t_j = 1, r_j = 0$ and
setting all amplitudes in the layers below to zero.

%===============================================================================
\subsection{Flux, evanescent waves}\label{SSpecial}
%===============================================================================

We write
\begin{equation}\label{Edecompkperp}
  \kappa \eqqcolon \kappa' + i \kappa''
\end{equation}
for the decomposition into a real and an imaginary part.
Accordingly, full wavevectors have the decomposition
\begin{equation}
  \k^\pm
  \eqqcolon {\k^\pm}' + i{\k^\pm}''
  = \k_\plll \pm (\kappa' + i \kappa'')\v{\OPR z}.
\end{equation}
Per \cref{Endb1}, we have $\beta\ge0$ and $\delta<1$,
from which it follows that $\kappa''$ always has the same sign as $\kappa'$.

After these preparations,
we can compute the flux~\cref{EdefJ}:
\begin{equation}\label{EJ1}
  \begin{array}{@{}l@{\;}l}
  \v{J}(\r)
  =&   \left|A^-\right|^2 \e^{+2\kappa'' (z-z_l)} {\k^-}'
    + \left|A^+\right|^2 \e^{-2\kappa''(z-z_l)} {\k^+}'
\\[2ex]
  &+ \left[
      A^-{A^+}^* \e^{-2i\kappa'(z-z_l)} {\left(\k_\plll-i\kappa''\v{\OPR z}\right)}
    + \text{c.c.}
    \right].
  \end{array}
\end{equation}
The first two terms describe the exponential intensity decrease
due to absorption, while
the oscillatory term in square brackets
is responsible for waveguide effects in layers with finite thickness.
The flux can also be written in terms of the one-dimensional wavefunctions $\phi^{\pm}(z)$:
\begin{equation}\label{EJ2}
  \begin{array}{@{}l@{\;}l}
  \v{J}(\r) =& \left|\phi^+(z)+\phi^-(z)\right|^2\cdot\k_\plll \\
  &+ \left[ \left|\phi^+(z)\right|^2 \kappa' - \left|\phi^-(z)\right|^2 \kappa' +
  2\Im(\phi^-(z){\phi^+}^*(z))\kappa'' \right]\cdot \v{\OPR z}.
  \end{array}
\end{equation}
The first term denotes the horizontal component
of the flux and can be seen to consist of the product
of the particle density at position $z$ and the wavector $\k_\plll$.
The $z$-component consists of the difference
between the up- and downward travelling wave components
and an extra term that encodes the interference between them.

In the special case of a purely imaginary~$\kappa_l$,
the flux becomes:
\begin{equation}\label{EJ3}
  \v{J}(\r) = \left| \psi \right|^2 \k_\plll + 2 \Im (A^-{A^+}^*) \kappa''\v{\OPR z}.
\end{equation}
This flux consists of two clearly distinct parts: an \E{evanescent wave},
travelling horizontally
 and a vertical component that is independent of the $z$ position.
The vertical component is a necessary
 degree of freedom to fulfill the boundary conditions at the layer's top and bottom interfaces.
In the case of a semi-infinite layer, the vertical component becomes zero and
 all incoming radiation at the top of the layer undergoes \E{total reflection}.

\else...\fi
