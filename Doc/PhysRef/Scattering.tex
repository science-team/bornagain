%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%   BornAgain Developers Reference
%%
%%   homepage:   http://www.bornagainproject.org
%%
%%   copyright:  Forschungszentrum Jülich GmbH 2015-
%%
%%   license:    Creative Commons CC-BY-SA
%%
%%   authors:    Scientific Computing Group at MLZ Garching
%%   editor:     Joachim Wuttke
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Wave propagation and scattering}\label{SSca}%
\chaptermark{Scattering}%
\index{Elastic scattering|seealso{Cross section}}%

This chapter introduces the formalism to described neutron and X-ray propagation
and scattering,
as needed for the analysis of grazing-incidence small-angle scattering (GISAS) experiments.
\index{GISAS}%
\index{Scattering!grazing incidence|see{GISAS}}%
\index{Grazing incidence!small-angle scattering|see{GISAS}}%
\index{SAS|see{Small-angle scattering}}%
\index{Distorted wave!Born approximation|see{DWBA}}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Wave propagation}\label{Swave}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\index{Wave propagation|(}%

In this section, we review the wave equations that describe the propagation
of neutrons (\cref{SnScalar}) and X-rays (\cref{SXwave}) in matter,
and combine them into a unified wave equation (\cref{SuniWave})
that is the base for the all following analysis.
This provides justification and background
for Eqns.~1--3 in the BornAgain reference paper~\cite{PoHB20}.

%===============================================================================
\subsection{Neutrons}\label{SnScalar}
%===============================================================================
\index{Wave propagation!neutron|(}%
\index{Neutron!wave propagation|(}%

\def\Vmac{\tilde{V}}

\index{Schrodinger@Schrödinger equation!microscopic}%
The scalar wavefunction $\psi(\r,t)$
\nomenclature[2t020]{$t$}{Time}%
\nomenclature[2r040]{$\r$}{Position}%
\nomenclature[1ψ030 2r040 2t02]{$\psi(\r,t)$}{Microscopic neutron wavefunction}%
of a free neutron
in absence of a magnetic field
is governed by the Schrödinger equation
\begin{equation}\label{ESchrodi1}
  i\hbar\partial_t \psi(\r,t)
  = \left\{-\frac{\hbar^2}{2m}\Nabla^2+V(\r)\right\} \psi(\r,t).
\end{equation}
Since BornAgain only aims at modelling elastic scattering,
any time dependence of the potential is averaged out in the definition
\index{Potential!neutron}%
\index{Neutron!potential}%
$V(\r)\coloneqq \langle V(\r,t)\rangle$.
\nomenclature[2v130 2r040]{$V(\r)$}{Neutron potential}%
Inelastic scattering
\index{Inelastic scattering}%
\index{Scattering!inelastic}%
\index{Attenuation!inelastic scattering}%
\index{Absorption|seealso {Attenuation}}%
\index{Attenuation!inelastic scattering}%
\index{Loss terms|see{Attenuation}}%
has for only effect an attenuation along beam trajectories.\footnote
{This is not explicitly supported in the software,
but users are free to increase the imaginary part of the refractive index
\index{Refractive index!imaginary part}%
\index{Refractive index!imaginary part|seealso{Attenuation}}%
to model inelastic and other losses.\label{Flosses}}
Therefore we only need to consider monochromatic waves
\index{Wave!monochromatic}%
\index{Monochromatic wave}%
with given frequency~$\omega$.
\nomenclature[1ω 020]{$\omega$}{Frequency of incident radiation}%
In consequence, the wavefunction
\begin{equation}\label{Estationarywave}
  \psi(\r,t) = \psi(\r)\e^{-i\omega t}
\end{equation}
\nomenclature[1ψ030 2r040 0]{$\psi(\r)$}{Stationary wavefunction}%
factorizes into a stationary wave and a time-dependent phase factor.
In the following, we will characterize the incoming radiation
not by its energy~$\hbar\omega$,
but by its \E{vacuum wavenumber}~$K$,
\index{Wavenumber!neutron}%
\nomenclature[2k120]{$K$}{Wavenumber in vacuum}%
given by the dispersion relation%
\begin{equation}
  \hbar\omega = \frac{(\hbar K)^2}{2m}.
\end{equation}
The Schrödinger equation~\cref{ESchrodi1} then takes the simple form
\begin{equation}\label{EWvEq}
  \left\{\Nabla^2+K^2-4\pi v_\snuc(\r)\right\} \psi(\r) = 0
\end{equation}
with the rescaled form of Fermi's pseudopotential
\index{Potential!neutron}%
\index{Neutron!potential}%
\begin{equation}\label{Evrraw}
  v_\snuc(\r)
  \coloneqq \frac{m}{2\pi\hbar^2} V(\r)
  = \sum_j\left\langle b_j \delta\left(\r-\r_j(t)\right)\right\rangle.
\end{equation}
The sum runs over all nuclei exposed to~$\psi$.
The subscript ``nucl'' designates nuclear as opposed to magnetic scattering.
The \E{bound scattering length}~$b_j$
\index{Scattering length!bound neutron}%
\nomenclature[2b0]{$b$}{Bound scattering length}%
is isotope specific;
values are tabulated \cite{Sea92}.

In \E{small-angle scattering},
\index{Scattering!small-angle}%
\index{Small-angle scattering}%
\index{SAS|see{Small-angle scattering}}%
as elsewhere in \E{neutron optics} \cite{Sea89},
the potential can be coarse-grained by spatially averaging over at least a few atomic diameters,
\begin{equation}\label{Evrcoarse}
  v_\snuc(\r)
  = \sum_s b_s \rho_s(\r),
\end{equation}
\nomenclature[2v030 2r040]{$v_\snuc(\r)$}
{Rescaled neutron potential, scattering length density (SLD)}%
where the sum now runs over chemical elements,
$b_s\coloneqq\langle b_j\rangle_{j\in s}$ is the bound \E{coherent} scattering length,
\index{Coherent scattering length}%
\index{Scattering length!coherent}%
and $\rho_s$ is a number density.
\nomenclature[1ρ032 2s010]{$\rho_s$}{Number density of chemical element~$s$}%
In passing from \cref{Evrraw} to \cref{Evrcoarse},
we neglected \E{Bragg scattering}
\index{Scattering!Bragg}%
\index{Bragg scattering}%
from atomic-scale correlation,
and \E{incoherent scattering} from spin or isotope related fluctuations of $b_j$.
\index{Scattering!incoherent}%
\index{Incoherent scattering}%
\index{Spin}%
\index{Neutron!spin}%
In small-angle experiments,
 these types of scattering only matter as loss channels.\footnote
{Same remark as in \Cref{Flosses}: To model these losses, use the
\index{Attenuation!losses Bragg scattering}%
\index{Attenuation!incoherent scattering}%
imaginary part of the refractive index.}
Furthermore, incoherent scattering, as inelastic scattering,
 contributes to the diffuse background in the detector.
\index{Scattering!diffuse}%
\index{Scattering!incoherent}%
\index{Incoherent scattering}%
\index{Background!diffuse}%
\index{Detector!background}%
In conclusion, the coarse-grained neutron optical potential~\cref{Evrcoarse}
\index{Potential!optical}%
\index{Potential!optical|seealso{SLD}}%
\index{Neutron!potential}%
is just a \E{scattering length density} (SLD)
\index{Scattering length!density|see{SLD}}%
\index{SLD}%
\cite[eq.\ 2.8.37]{Sea89},
and the effective macroscopic Schrödinger equation still has the form
\cref{ESchrodi1} or~\cref{EWvEq}.

The current density, or \E{flux},
\index{Flux!neutron}%
\index{Current density|see{Flux}}%
of a neutron beam is given by
\begin{equation}\label{Dflux}
  \v{J}(\r)
   = \psi^*\frac{\Nabla}{2i}\psi
                        - \psi\frac{\Nabla}{2i}\psi^*.
\end{equation}
For a monochromatic \E{plane wave}%
\begin{equation}\label{EplaneWave}
  \psi_\k(\r)\coloneqq\e^{i\k\r}
\end{equation}
note that the conjugate wave function
$  \psi_\k(\r)^* = \e^{i\k^*\r} $
involves the conjugate of the complex wavevector~$\k$.
Accordingly, the flux is
\begin{equation}
  \v{J}(\r)
   = |\psi_\k(\r)|^2 \Re \k.
\end{equation}

\index{Wave propagation!neutron|)}%
\index{Neutron!wave propagation|)}%

%===============================================================================
\subsection{X-rays}\label{SXwave}
%===============================================================================

\index{Wave propagation!X-ray|(}%
\index{X-ray!wave equation|(}%
The propagation of X-rays is governed by Maxwell's equations,
\index{Maxwell's equations}%
\begin{equation}\label{EMaxwell}
  \begin{array}{@{}l@{\quad}l@{\quad}l}
    \Nabla\times\v{E}=-\partial_t \v{B},
   &\Nabla\v{B}=0,
   &\v{B}=\v{\mu}(\r)\mu_0\v{H},
   \\[2ex]
    \Nabla\times\v{H}=+\partial_t \v{D},
   &\Nabla\v{D}=0,
   &\v{D}=\v{\eps}(\r)\eps_0\v{E}.
  \end{array}
\end{equation}
\nomenclature[2e150 2r040 2t020]{$\v{E}(\r,t)$}{Electric field}%
\nomenclature[2d150 2r040 2t020]{$\v{D}(\r,t)$}{Displacement field}%
\nomenclature[2b150 2r040 2t020]{$\v{B}(\r,t)$}{Magnetic field}%
\nomenclature[1ε070 2r040]{$\v\eps(\r)$}{Relative dielectric permittivity tensor}%
\nomenclature[1μ070 2r040]{$\v\mu(\r)$}{Relative magnetic permeability tensor}%
\nomenclature[1ε024 00]{$\eps_0$}{Vacuum permittivity, 8.854\ldots As/Vm}%
Since BornAgain only addresses elastic scattering,
we assume the permeability and permittivity tensors $\v\mu$ and~$\v\eps$
to be time-independent.
Therefore, as in~\cref{SnScalar}, we only need to consider monochromatic waves
\index{Wave!monochromatic}%
\index{Monochromatic wave}%
with given frequency~$\omega$,
and each of the fields $\v{E}$, $\v{D}$, $\v{H}$, $\v{B}$
factorizes into a stationary field and a time-dependent phase factor.\footnote
{This phase factor can be defined with a plus or a minus sign in the exponent.
Most texts on X-ray crystallography,
including influential texts on GISAXS \cite{ReLL09},
prefer the \E{crystallographic convention} with a plus sign.
\index{Wave propagation|seealso {Sign convention}}%
\index{Sign convention!wave propagation|(}%
In BornAgain, we prefer the opposite \E{quantum-mechanical convention}
for consistency with the neutron case \cref{Estationarywave},
where the minus sign is an inevitable consequence
of the standard form of the Schrödinger equation.%
\index{Sign convention!wave propagation|)}%
}
We will formulate the following in terms of the electric field
\begin{equation}\label{EstationaryX}
  \v{E}(\r,t) = \v{E}(\r)\e^{-i\omega t}.
\end{equation}
The other three fields can be obtained from~$\v{E}$
by straightforward application of~\cref{EMaxwell}.

Since magnetic refraction or scattering is beyong the scope of BornAgain,
the relative magnetic permeability tensor is always $\v{\mu}(\r)=1$.
\index{Permeability}%
\index{Magnetic permeability}%
As customary in SAXS and GISAXS,
\index{GISAS!X-ray}%
\index{Small-angle scattering!X-ray}%
we assume
that the dielectric properties of the material are those of a polarizable electron cloud.\footnote
{This is occasionally called the \E{Laue model}
\index{Laue model}%
 \cite{Lau31}.}
Thereby the relative dielectric permittivity tensor~$\v{\eps}$
becomes a scalar,
\begin{equation}
  \eps(\r)=1-\frac{4\pi r_e}{K^2}\rho(\r),
\end{equation}
\nomenclature[1ε030 2r040]{$\eps(\r)$}{Relative dielectric permittivity function}%
\nomenclature[1ρ030 2r040]{$\rho(\r)$}{Electron number density}%
with the classical electron radius~$r_e=e^2/mc^2\simeq2.8\cdot10^{-15}$~m,
\nomenclature[2r024 2e000]{$r_e$}{Classical electron radius~$2.817\ldots^{-15}$~m}%
the electron number density~$\rho(\r)$,
and the vacuum wavenumber~$K$,
given by the dispersion relation
\begin{equation}
  K^2 = \mu_0\eps_0\omega^2.
\end{equation}%
With these simplifying assumptions about $\v{\eps}$ and~$\v{\mu}$,
Maxwell's equations yield the wave equation
\begin{equation}\label{ENabCrossNabE}
  \Nabla\times\Nabla\times\v{E} = K^2\eps(\r)\v{E}.
\end{equation}
\index{Wave equation!X-ray}%
Using a standard identity from vector analysis, it can be brought into the more tractable form
\begin{equation}\label{ENabNabE}
  \left\{\Nabla^2-\Nabla\cdot\Nabla+ K^2\eps(\r)\right\}\v{E}(\r)=0.
\end{equation}
\index{Wave equation!X-ray|)}%

\index{X-ray!flux|(}%
\index{Flux!X-ray|(}%
It is well known that the electromagnetic energy flux is given by the Poynting vector.
\index{Poynting vector}%
However, its standard definition, $\v{S}\coloneqq\v{E}\times\v{H}$,
is not applicable here because it only holds for \E{real} fields.
With our complex notation, it must be replaced by
\begin{equation}
  \v{S}\coloneqq \Re\v E(\r,t)\times\Re\v H(\r,t).
\end{equation}
\nomenclature[2s150]{$\v S$}{Poyinting vector}%
For stationary oscillations~\cref{EstationaryX},
the time average is
\begin{equation}
  \braket{\v S} = \frac{1}{4}\braket{\v E(\r) \times \v H(\r)^* + \text{c.~c.}}.
\end{equation}
\nomenclature[2c000 2c000]{$\text{c.~c.}$}{Complex conjugate}%
We specialize to vacuum with $\TENS\eps(\r)=1$,
and obtain
\begin{equation}
  \braket{\v S}
  = \frac{1}{4i\omega\mu_0}
    \left( \v E^*(\r)\times\left(\Nabla\times\v{E}(\r)\right) + \text{c.~c.} \right).
\end{equation}
For a monochromatic plane wave $\v E(\r)=\v E_\k \e^{i\k\r}$, we find
\begin{equation}
  \braket{\v S}
  = \frac{1}{2\omega\mu_0} |\v E_\k|^2 \Re \k,
\end{equation}
which confirms the common knowledge that the radiation intensity
counted in a detector is proportional to the squared electric field amplitude.
\index{X-ray!flux|)}%
\index{Flux!X-ray|)}%

%===============================================================================
\subsection{Unified wave equation}\label{SuniWave}
%===============================================================================

As in Eqns.~1--3 of Ref.~\cite{PoHB20},
we combine all the above in a unified wave equation
\index{Wave equation!generic}%
\begin{equation}\label{EWAVE}
  \left( \TDo - 4\pi v(\r) \right) \psi(\r) = 0
\end{equation}
with the vacuum wave operator
\index{Wave!operator!vacuum}%
\begin{equation}\label{EDo}
  \TDo \coloneqq \left\{ \begin{array}{ll}
      \Nabla^2 + K^2                     &\text{~~~for neutrons,}\\
      \Nabla^2 - \Nabla\cdot\Nabla + K^2 &\text{~~~for X-rays}
  \end{array}\right.
\end{equation}
\nomenclature[2d138 0 2r040]{$\TDo(\r)$}{Differential operator in the vacuum wave equation}%
and the potential\footnote
{This corrects Eq. 3 in our reference paper~\cite{PoHB20},
which had a sign error in the X-ray case.}
\index{Potential!generic}%
\nomenclature[2v170 2r040]{$v(\r)$}{Generic potential}%
\begin{equation}\label{ETV}
  v(\r) \coloneqq \left\{ \begin{array}{ll}
      v_\snuc(\r)                         &\text{~~~for neutrons,}\\
      K^2(1-\epsilon(\r))/(4\pi) &\text{~~~for X-rays.}
  \end{array}\right.
\end{equation}
The generic wave amplitude $\v{\psi}$
\nomenclature[1ψ150 2r040]{$\psi(\r)$}{Generic wave amplitude,
  possibly vectorial or spinorial}%
shall represent
the scalar neutron wavefunction~$\psi$
or the electric field~$\v{E}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Distorted-wave Born approximation}\label{SDWBA}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Neutron or X-ray scattering by condensed matter is usually described
in \E{Born approximation} (BA),
\index{BA|see{Born approximation}}%
\index{Born approximation}%
which is treats the whole potential $v(\r)$ as a small perturbation.
This is not adequate if incident or scattered wave propagate under small glancing angles,
\index{Glancing angle}%
\index{Grazing incidence}%
as refraction and reflection are no longer small.
For grazing-incidence small-angle scattering,
\index{GISAS}%
we need the more generic
\index{DWBA}%
\E{distorted-wave Born approximation} (DWBA).\footnote
{The DWBA was originally devised by Massey and Mott (ca 1933)
for collisions of charged particles.
Summaries can be found in some quantum mechanics textbooks (Messiah, Schiff)
and in monographs on scattering theory (e.~g.\ Newton).
The first explicit applications to grazing-incidence scattering
were published in 1982:
Vineyard \cite{Vin82} discussed X-ray scattering,
but failed to account for the distortion of the scattered wave;
Mazur and Mills \cite{MaMi82} deployed heavy formalism
to compute the inelastic neutron scattering cross section
of ferromagnetic surface spin waves from scratch.
A concise derivation of the DWBA cross section
was provided by Dietrich and Wagner (1984/85)
for X-rays \cite{DiWa84} and neutrons \cite{DiWa85}.
Unfortunately, their work was overlooked in much of the later literature,
which often fell back to less convincing derivations.}

%===============================================================================
\subsection{Distortion versus perturbation}\label{Sdecompose}
%===============================================================================

To get started,
we decompose the potential \cref{ETV}
into a more \E{regular} and a more \E{fluctuating} part:
\begin{equation}\label{Edecompose}
  v(\r) \eqqcolon \TL(\r) + \delta v(\r).
\end{equation}
The \E{distortion field}~$\TL$
\index{Distortion field}%
\nomenclature[1λ170 2r040]{$\TL(\r)$}{Distortion field}%
comprises regular, well-known features of the sample.
The \E{perturbation potential}~$\delta v$
\index{Perturbation potential}%
\index{Potential!perturbation}%
\nomenclature[2u170 2r040]{$\delta v(\r)$}{Perturbation potential}%
stands for the more irregular, unknown features of the sample
one ultimately wants to study in a scattering experiment.
The wave equation~\cref{EWAVE} shall henceforth be written as
\begin{equation}\label{EDPsi}
  \left(\TD(\r) - 4\pi\delta v(\r) \right)\psi(\r) = 0
\end{equation}
with the \E{distorted wave operator}
\index{Distorted wave!operator}%
\index{Wave!operator!distorted}%
\nomenclature[2d138 2r040]{$\TD(\r)$}{Differential operator in the wave equation}%
\begin{equation}
  \TD(\r) \coloneqq \TDo - 4\pi\TL(\r).
\end{equation}
Only $\delta v$ shall be treated as a perturbation.
The propagation of incident and scattered waves under the influence of~$\TL$,
in contrast,
shall be handled exactly,
through analytical solution of the \E{unperturbed distorted wave equation}
\index{Distorted wave!wave equation}%
\index{Wave equation!unperturbed distorted}%
\begin{equation}\label{EDPsi0}
  \TD(\r)\psi(\r) = 0.
\end{equation}
The solutions are called \E{distorted}
\index{Distorted wave}%
\index{Wave!distorted}%
because they differ from the plane waves
obtained in the vacuum case $\TL=0$.

Except for neutrons in a magnetic field
the distortion field is scalar so
that it can be expressed through the \E{refractive index}
\index{Refractive index}%
\index{Index of refraction|see{Refractive index}}%
\nomenclature[2n020]{$n$}{Refractive index}%
\begin{equation}\label{EnkK}
  n(\r)
  \coloneqq\sqrt{1-\frac{4\pi\TL(\r)}{K^2}}
  = \left\{\begin{array}{ll}
       \sqrt{1-4\pi\mv_\snuc(\r)/K^2} &\text{ for neutrons,}\\
       \sqrt{\epsilon(\r)} &\text{ for X-rays.}
    \end{array}\right.
\end{equation}
If $\mv(\r)$ or $\epsilon(\r)$ has an imaginary part, describing absorption,
\index{Absorption}%
then $n(\r)$ is a complex number.
Conventionally, $n$ is parameterized by two real numbers:
\begin{equation}\label{Endb1}
  n \eqqcolon  1-\delta +i\beta.
\end{equation}
\nomenclature[1δ020]{$\delta$}{Small parameter in the refractive index
   $n=1-\delta +i\beta$}%
\nomenclature[1β020]{$\beta$}{Imaginary part of the refractive index}%
For thermal neutrons and X-rays,
$\delta$ and $\beta$ are almost always nonnegative,\footnote
{The plus sign in front of~$i\beta$ is a consequence of
the quantum-mechanical sign convention;
in the X-ray crystallography convention it would be a minus sign.
\index{Refractive index!sign convention}%
\index{Sign convention!refractive index}}
and much smaller than~1.
This explains why in most scattering geometries
\index{Scattering!geometry}%
the ordinary Born approximation
\index{Born approximation}%
with $\TL\equiv0$ is perfectly adequate.
In layered samples under grazing incidence,
\index{Grazing incidence}%
however, even small differences in~$n$ can cause substantial
\E{refraction} and \E{reflection}.
\index{Refraction}%
\index{Reflection}%
To model GISAS, therefore,
it is necessary to use DWBA
\index{Distorted-wave Born approximation}%
with $\TL(z)$ given by
the horizontally averaged refractive index~$\overline{n}(z)$.
\index{Refractive index!horizontally averaged}%

%===============================================================================
\subsection{Differential cross section}\label{SdiffCross}
%===============================================================================

\index{Flux!incident and scattered}%
The ratio of the scattered flux $J(\r)$ hitting an infinitesimal detector area
$r^2\d\Omega$ to the incident flux $J_\si$ is expressed as a
\E{differential cross section}
\index{Cross section}%
\index{Scattering!cross section}%
\begin{equation}\label{Exsectiondef}
  \xElas
  \coloneqq  \frac{r^2 J(\r)}{J_\si}.
\end{equation}
\nomenclature[1ω120]{$\Omega$}{Solid angle}%
\nomenclature[1σ020]{$\sigma$}{Scattering or absorption cross section}%
The geometric factors that are needed to
convert $\d\sigma/\d\Omega$ into detector counts will be discussed
below in \cref{SdetImg}.

From standard textbooks we take the generic differential cross section
of elastic scattering in first order Born approximation,\footnote
{For a particularly detailed derivation
see Schober's lecture notes on neutron scattering~\cite{Sch14}.}%
\index{Born approximation}%
\index{Cross section!Born approximation}%
\index{Scattering!cross section}%
\begin{equation}\label{Exsection}
  \xElas
  =  {\left|\braket{\psi_\si|\delta v|\psi_\sf}\right|}^2,
\end{equation}
where the matrix element in Dirac bra-ket notation stands for the integral
\index{Scattering!matrix}%
\index{Transition matrix|see{Scattering matrix}}%
\begin{equation}\label{Etrama}
  \braket{\psi_\si|\delta v|\psi_\sf}
  \coloneqq  \int\!\d^3r\, \psi^*_\si(\r)\delta v(\r)\psi_\sf(\r).
\end{equation}%
\nomenclature[0$\langle$0]{{$\braket{\ldots|\ldots|\ldots}$}}{Matrix
  element, defined as a volume integral}%
For brevity and mathematical convenience,
the integral has no bounds
and therefore formally runs over the entire space.
However, $\delta v(\r)$ is nonzero only if $\r$ lies inside the finite sample volume.

%-------------------------------------------------------------------------------
\begin{figure}[tb]
\begin{center}
\includegraphics[width=1\textwidth]{fig/drawing/Green1.ps}
\end{center}
\caption{(a)
In a multilayer sample, the scattered wave propagates from the scattering center~S
towards the detector~D through different paths,
due to partial reflection by interfaces.
(b) In far-field approximation, the detector location is so remote
that all rays leaving the sample can be considered parallel.
In consequence, when the scattered wave is traced back from the detector
it can be considered plane until it reaches the sample.}
\label{Fgreen1}
\end{figure}
%-------------------------------------------------------------------------------

In ordinary (non-distorted) Born approximation,
the incident $\psi_\si$ is a plane wave \cref{EplaneWave}.
By means of a far-field expansion, the outgoing spherical wave $\psi_\sf$,
traced back from the detector towards the sample,
is also approximated as a plane wave.
Thereby~\cref{Etrama} becomes a Fourier integral
\begin{equation}\label{Etramaq}
  \braket{\psi_\si|\delta v|\psi_\sf}
  =  \int\!\d^3r\, \e^{-i\k_\si\r} \delta v(\r) \e^{i\k_\sf\r}
  =  \int\!\d^3r\, \e^{i\q\r} \delta v(\r)
\end{equation}%
with the scattering vector
\begin{equation}\label{Dq}
   \q \coloneqq \k_\sf - \k_\si.
\end{equation}

This plane-wave approximation breaks down under grazing incidence
as refraction and reflection by surfaces and interfaces cannot be neglected.
While \cref{Exsection} and \cref{Etrama} still hold, \cref{Etramaq}~does not.
In DWBA,
the incident wave $\psi_\si$ ceases to be plane
when it reaches the sample (\cref{Fgreen1}).
Inside the sample it evolves according to the unperturbed wave equation~\cref{EDPsi0}.
Similarly, the scattered wave $\psi_\sf$, traced back from the detector,
is a plane wave outside the sample,
but is distorted inside the sample as it obeys~\cref{EDPsi0}.
The wave propagation inside a discrete multilayer sample
will be worked out in~\cref{sec:Multilayers}.
