//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Adapter/ObjectiveFunctionAdapter.cpp
//! @brief     Implements class ObjectiveFunctionAdapter.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Fit/Adapter/ObjectiveFunctionAdapter.h"
#include "Fit/Residual/ResidualFunctionAdapter.h"
#include "Fit/Residual/RootResidualFunction.h"
#include "Fit/Residual/RootScalarFunction.h"
#include "Fit/Residual/ScalarFunctionAdapter.h"

using namespace mumufit;

ObjectiveFunctionAdapter::ObjectiveFunctionAdapter() = default;

ObjectiveFunctionAdapter::~ObjectiveFunctionAdapter() = default;

const RootScalarFunction*
ObjectiveFunctionAdapter::rootObjectiveFunction(fcn_scalar_t fcn, const Parameters& parameters)
{
    std::unique_ptr<ScalarFunctionAdapter> tem_adapter(new ScalarFunctionAdapter(fcn, parameters));
    const auto* result = tem_adapter->rootObjectiveFunction();
    m_adapter = std::move(tem_adapter);
    return result;
}

const RootResidualFunction*
ObjectiveFunctionAdapter::rootResidualFunction(fcn_residual_t fcn, const Parameters& parameters)
{
    std::unique_ptr<ResidualFunctionAdapter> tem_adapter(
        new ResidualFunctionAdapter(fcn, parameters));
    const auto* result = tem_adapter->rootResidualFunction();
    m_adapter = std::move(tem_adapter);
    return result;
}

int ObjectiveFunctionAdapter::numberOfCalls() const
{
    return m_adapter ? m_adapter->numberOfCalls() : 0;
}

int ObjectiveFunctionAdapter::numberOfGradientCalls() const
{
    return m_adapter ? m_adapter->numberOfGradientCalls() : 0;
}
