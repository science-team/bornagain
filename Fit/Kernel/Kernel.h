//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Kernel/Kernel.h
//! @brief     Defines class Kernel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_FIT_KERNEL_KERNEL_H
#define BORNAGAIN_FIT_KERNEL_KERNEL_H

#include "Fit/Minimizer/MinimizerResult.h"
#include "Fit/Minimizer/Types.h"
#include <memory>

class IMinimizer;

namespace mumufit {

//! A main class to run fitting.

class Kernel {
public:
    Kernel();
    ~Kernel();

    void setMinimizer(const std::string& minimizerName, const std::string& algorithmName = "",
                      const std::string& options = "");

    void setMinimizer(IMinimizer* minimizer);

    MinimizerResult minimize(fcn_scalar_t fcn, const Parameters& parameters) const;
    MinimizerResult minimize(fcn_residual_t fcn, const Parameters& parameters) const;

private:
    void setParameters(const Parameters& parameters);

    std::unique_ptr<IMinimizer> m_minimizer;
};

} // namespace mumufit

#endif // BORNAGAIN_FIT_KERNEL_KERNEL_H
