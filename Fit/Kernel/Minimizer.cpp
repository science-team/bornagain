//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Kernel/Minimizer.cpp
//! @brief     Implements class Minimizer.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Fit/Kernel/Minimizer.h"
#include "Fit/Kernel/Kernel.h"

using namespace mumufit;

Minimizer::Minimizer()
    : m_kernel(new Kernel)
{
}

void Minimizer::setMinimizer(const std::string& minimizerName, const std::string& algorithmName,
                             const std::string& options)
{
    m_kernel->setMinimizer(minimizerName, algorithmName, options);
}

void Minimizer::setMinimizer(IMinimizer* minimizer)
{
    m_kernel->setMinimizer(minimizer);
}

Minimizer::~Minimizer() = default;

MinimizerResult Minimizer::minimize(const fcn_scalar_t fcn, const Parameters& parameters) const
{
    return m_kernel->minimize(fcn, parameters);
}

MinimizerResult Minimizer::minimize(const fcn_residual_t fcn, const Parameters& parameters) const
{
    return m_kernel->minimize(fcn, parameters);
}
