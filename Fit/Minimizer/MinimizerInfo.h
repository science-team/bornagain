//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Minimizer/MinimizerInfo.h
//! @brief     Declares class MinimizerInfo.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_FIT_MINIMIZER_MINIMIZERINFO_H
#define BORNAGAIN_FIT_MINIMIZER_MINIMIZERINFO_H

#include <string>
#include <utility>
#include <vector>

//! A name and a description.

class AlgorithmInfo {
public:
    AlgorithmInfo(const std::string& itemName, const std::string& itemDescription)
        : m_item_name(itemName)
        , m_item_description(itemDescription)
    {
    }

    std::string name() const { return m_item_name; }
    std::string description() const { return m_item_description; }

private:
    const std::string m_item_name;
    const std::string m_item_description;
};

//! Info about a minimizer, including list of defined minimization algorithms.

class MinimizerInfo {
public:
    MinimizerInfo(const std::string& minimizerType, const std::string& minimizerDescription)
        : m_name(minimizerType)
        , m_description(minimizerDescription)
    {
    }

    //! Sets currently active algorithm
    void setAlgorithmName(const std::string& algorithmName);

    std::string name() const { return m_name; }
    std::string description() const { return m_description; }

    std::string algorithmName() const { return m_current_algorithm; }

    std::vector<std::string> algorithmNames() const;
    std::vector<std::string> algorithmDescriptions() const;

    static MinimizerInfo buildMinuit2Info(const std::string& defaultAlgo = "");
    static MinimizerInfo buildGSLMultiMinInfo(const std::string& defaultAlgo = "");
    static MinimizerInfo buildGSLLMAInfo();
    static MinimizerInfo buildGSLSimAnInfo();
    static MinimizerInfo buildGeneticInfo();

private:
    void addAlgorithm(const AlgorithmInfo& algorithm);
    void addAlgorithm(const std::string& algorithmName, const std::string& algorithmDescription);

    const std::string m_name;
    const std::string m_description;
    std::vector<AlgorithmInfo> m_algorithms;
    std::string m_current_algorithm;
};

#endif // BORNAGAIN_FIT_MINIMIZER_MINIMIZERINFO_H
