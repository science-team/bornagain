//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Option/MinimizerOptions.h
//! @brief     Declares class MinimizerOptions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_FIT_OPTION_MINIMIZEROPTIONS_H
#define BORNAGAIN_FIT_OPTION_MINIMIZEROPTIONS_H

#include "Fit/Option/OptionContainer.h"

//! Collection of internal minimizer settings.

class MinimizerOptions : public OptionContainer {
public:
    //! Returns string with all options (i.e. "Strategy=1;Tolerance=0.01;")
    std::string toOptionString() const;

    //! Set options from their string representation
    void setOptionString(const std::string& options);

private:
    void processCommand(const std::string& command);
};

#endif // BORNAGAIN_FIT_OPTION_MINIMIZEROPTIONS_H
