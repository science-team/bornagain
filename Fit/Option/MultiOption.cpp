//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Option/MultiOption.cpp
//! @brief     Implements class MultiOption.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Fit/Option/MultiOption.h"
#include <utility>

MultiOption::MultiOption(const std::string& name)
    : m_name(name)
{
}
void MultiOption::setDescription(const std::string& description)
{
    m_description = description;
}

//! Sets the value of option from string.
void MultiOption::setFromString(const std::string& value)
{
    const std::size_t idx = m_value.index();
    switch (idx) {
    case 0:
        m_value = std::stoi(value);
        break;
    case 1:
        m_value = std::stod(value);
        break;
    default:
        m_value = value;
    }
}

//! Sets the value of option from string.
std::string MultiOption::value_str()
{
    if (std::holds_alternative<int>(m_value))
        return std::to_string(std::get<int>(m_value));
    else if (std::holds_alternative<double>(m_value))
        return std::to_string(std::get<double>(m_value));
    else // std::holds_alternative<std::string>(m_value)
        return std::get<std::string>(m_value);
}
