//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Option/MultiOption.h
//! @brief     Declares class MultiOption.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_FIT_OPTION_MULTIOPTION_H
#define BORNAGAIN_FIT_OPTION_MULTIOPTION_H

#include <string>
#include <variant>

//! Stores a single option for minimization algorithm. Int, double, string values are available.

class MultiOption {
public:
    using variant_t = std::variant<int, double, std::string>;

    MultiOption(const std::string& name = "");

    template <typename T>
    MultiOption(const std::string& name, const T& t, const std::string& descripion = "");

    std::string name() const { return m_name; }

    std::string description() const { return m_description; }
    void setDescription(const std::string& description);

    variant_t& value() { return m_value; }
    variant_t& defaultValue() { return m_default_value; }

    //! Returns the option's value
    template <typename T> T get() const;

    //! Returns the option's default value (i.e. used during construction)
    template <typename T> T getDefault() const;

    //! Returns a string representation of the option's value
    std::string value_str();

    void setFromString(const std::string& value);

private:
    std::string m_name;
    std::string m_description;
    variant_t m_value;
    variant_t m_default_value;
};

template <typename T>
MultiOption::MultiOption(const std::string& name, const T& t, const std::string& descripion)
{
    m_name = name;
    m_description = descripion;
    m_value = t;
    m_default_value = t;
}

template <typename T> T MultiOption::get() const
{
    return std::get<T>(m_value);
}

template <typename T> T MultiOption::getDefault() const
{
    return std::get<T>(m_default_value);
}

#endif // BORNAGAIN_FIT_OPTION_MULTIOPTION_H
