//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Param/AttLimits.cpp
//! @brief     Implements and implements class AttLimits.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Fit/Param/AttLimits.h"
#include <iomanip>
#include <sstream>

AttLimits::AttLimits()
    : m_limits(RealLimits::limitless())
    , m_fixed(false)
{
}

AttLimits::AttLimits(const RealLimits& limits, bool fixed)
    : m_limits(limits)
    , m_fixed(fixed)
{
}

AttLimits AttLimits::limitless()
{
    return AttLimits();
}

AttLimits AttLimits::lowerLimited(double bound_value)
{
    return AttLimits(RealLimits::lowerLimited(bound_value), false);
}

AttLimits AttLimits::positive()
{
    return AttLimits(RealLimits::positive(), false);
}

AttLimits AttLimits::nonnegative()
{
    return AttLimits(RealLimits::nonnegative(), false);
}

AttLimits AttLimits::upperLimited(double bound_value)
{
    return AttLimits(RealLimits::upperLimited(bound_value), false);
}

AttLimits AttLimits::limited(double left_bound_value, double right_bound_value)
{
    return AttLimits(RealLimits::limited(left_bound_value, right_bound_value), false);
}

AttLimits AttLimits::fixed()
{
    return AttLimits(RealLimits::limitless(), true);
}

bool AttLimits::isFixed() const
{
    return m_fixed;
}

bool AttLimits::isLimited() const
{
    return !m_fixed && m_limits.isLimited();
}

bool AttLimits::isUpperLimited() const
{
    return !m_fixed && !m_limits.hasLowerLimit() && m_limits.hasUpperLimit();
}

bool AttLimits::isLowerLimited() const
{
    return !m_fixed && m_limits.hasLowerLimit() && !m_limits.hasUpperLimit();
}

bool AttLimits::isLimitless() const
{
    return !m_fixed && !m_limits.hasLowerLimit() && !m_limits.hasUpperLimit();
}

double AttLimits::min() const
{
    return m_limits.min();
}

double AttLimits::max() const
{
    return m_limits.max();
}

bool AttLimits::operator==(const AttLimits& other) const
{
    return m_limits == other.m_limits && m_fixed == other.m_fixed;
}

bool AttLimits::operator!=(const AttLimits& other) const
{
    return !(*this == other);
}

std::string AttLimits::toString() const
{
    std::ostringstream result;

    if (isFixed())
        result << "fixed";
    else if (isLimitless())
        result << "free";
    else if (isLowerLimited())
        result << "lowerLimited(" << std::scientific << std::setprecision(2) << min() << ")";
    else if (isUpperLimited())
        result << "upperLimited(" << std::scientific << std::setprecision(2) << max() << ")";
    else if (isLimited())
        result << "limited(" << std::scientific << std::setprecision(2) << min() << ","
               << std::scientific << std::setprecision(2) << max() << ")";

    return result.str();
}
