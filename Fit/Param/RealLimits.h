//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Param/RealLimits.h
//! @brief     Defines class RealLimits.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_FIT_PARAM_REALLIMITS_H
#define BORNAGAIN_FIT_PARAM_REALLIMITS_H

#include <ostream>
#include <string>

//! Limits for a real fit parameter.

class RealLimits {
public:
    //! Default constructor creates a "limitless" instance.
    RealLimits();

    //! Creates an object bounded from the left
    static RealLimits lowerLimited(double bound_value);

    //! Creates an object which can have only positive values (>0., zero is not included)
    static RealLimits positive();

    //! Creates an object which can have only positive values with 0. included
    static RealLimits nonnegative();

    //! Creates an object bounded from the right
    static RealLimits upperLimited(double bound_value);

    //! Creates an object bounded from the left and right
    static RealLimits limited(double left_bound_value, double right_bound_value);

    //! Creates an object without bounds (default)
    static RealLimits limitless();

    //! Returns lower limit
    double min() const { return m_lower_limit; }

    //! Returns upper limit
    double max() const { return m_upper_limit; }

    //! Returns true if proposed value is in limits range
    bool isInRange(double value) const;

    //! Returns value if it is inside. Otherwise returns the lower or upper bound.
    double clamp(double value) const;

    std::string toString() const;

    //! Prints class
    friend std::ostream& operator<<(std::ostream& ostr, const RealLimits& m)
    {
        ostr << m.toString();
        return ostr;
    }

    bool operator==(const RealLimits& other) const;
    bool operator!=(const RealLimits& other) const;

    bool hasLowerLimit() const { return m_has_lower_limit; }
    bool hasUpperLimit() const { return m_has_upper_limit; }
    bool isLimitless() const;
    bool isPositive() const;
    bool isNonnegative() const;
    bool isLowerLimited() const;
    bool isUpperLimited() const;
    bool isLimited() const;

private:
    RealLimits(bool has_lower_limit, bool has_upper_limit, double lower_limit, double upper_limit);

    bool m_has_lower_limit; //! parameter has lower bound
    bool m_has_upper_limit; //! parameter has upper bound
    double m_lower_limit;   //! minimum allowed value
    double m_upper_limit;   //! maximum allowed value
};

#endif // BORNAGAIN_FIT_PARAM_REALLIMITS_H
