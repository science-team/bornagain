//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Residual/IFunctionAdapter.cpp
//! @brief     Implements interface IFunctionAdapter.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Fit/Residual/IFunctionAdapter.h"

using namespace mumufit;

IFunctionAdapter::IFunctionAdapter()
    : m_number_of_calls(0)
    , m_number_of_gradient_calls(0)
{
}

IFunctionAdapter::~IFunctionAdapter() = default;