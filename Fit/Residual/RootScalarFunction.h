//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Residual/RootScalarFunction.h
//! @brief     Defines classes RootScalarFunction.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_FIT_RESIDUAL_ROOTSCALARFUNCTION_H
#define BORNAGAIN_FIT_RESIDUAL_ROOTSCALARFUNCTION_H

#include "Fit/Minimizer/Types.h"

#ifndef _WIN32
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#endif

#include <Math/Functor.h>

#ifndef _WIN32
#pragma GCC diagnostic pop
#endif

//! The chi2 function for use in minimizers.

class RootScalarFunction : public ROOT::Math::Functor {
public:
    RootScalarFunction(root_scalar_t fcn, int ndims);
};

#endif // BORNAGAIN_FIT_RESIDUAL_ROOTSCALARFUNCTION_H
