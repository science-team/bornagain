//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/TestEngine/MinimizerTestPlan.h
//! @brief     Defines class MinimizerTestPlan.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_FIT_TESTENGINE_MINIMIZERTESTPLAN_H
#define BORNAGAIN_FIT_TESTENGINE_MINIMIZERTESTPLAN_H

#include "Fit/Param/Parameter.h"
#include <utility>
#include <vector>

//! Defines initial settings of single fit parameter and the final value which has to be found
//! in the course of the fit.

struct ParameterReference {
    const mumufit::Parameter parameter; //!< initial parameter settings
    const double expected_value;        //!< expected value to find in the fit
    const double tolerance{0.01};       //!< tolerance on found value wrt expected value
};

namespace mumufit {
class Minimizer;
class Parameter;
class Parameters;
} // namespace mumufit

//! Defines objective function to fit, expected minimum, initial fit parameters and
//! expected values of fit parameters at minimum.

class MinimizerTestPlan {
public:
    MinimizerTestPlan() = default;
    explicit MinimizerTestPlan(const std::vector<ParameterReference>& parameter_references)
        : m_parameter_references(parameter_references)
    {
    }

    virtual ~MinimizerTestPlan();

    void addParameter(const mumufit::Parameter& param, double expected_value,
                      double tolerance = 0.01);

    //! Runs minimization and check minimization result.
    virtual bool checkMinimizer(mumufit::Minimizer& minimizer) const = 0;

protected:
    mumufit::Parameters parameters() const;
    std::vector<double> expectedValues() const;
    bool valuesAsExpected(const std::vector<double>& values) const;

    std::vector<ParameterReference> m_parameter_references; //! initial/expected parameter values
};

#endif // BORNAGAIN_FIT_TESTENGINE_MINIMIZERTESTPLAN_H
