//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Tool/MinimizerUtil.h
//! @brief     Declares namespace MinimizerUtils.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_FIT_TOOL_MINIMIZERUTIL_H
#define BORNAGAIN_FIT_TOOL_MINIMIZERUTIL_H

#include <map>
#include <string>
#include <vector>

//! Utility functions for fit library

namespace mumufit::utils {

std::string toString(const std::vector<std::string>& v, const std::string& delim = "");

std::map<int, std::string> gslErrorDescriptionMap();

std::string gslErrorDescription(int errorCode);

std::string sectionString(const std::string& sectionName = "", size_t report_width = 80);

} // namespace mumufit::utils

#endif // BORNAGAIN_FIT_TOOL_MINIMIZERUTIL_H
