//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Tool/StringUtil.cpp
//! @brief     Implements a few helper functions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Fit/Tool/StringUtil.h"
#include <algorithm>
#include <regex>

//! Returns token vector obtained by splitting string at delimiters.
std::vector<std::string> mumufit::stringUtil::split(const std::string& text,
                                                    const std::string& delimiter)
{
    if (text.empty())
        return {};
    std::vector<std::string> result;
    size_t pos = 0;
    while (pos != std::string::npos) {
        size_t next_pos = text.find(delimiter, pos);
        if (next_pos == std::string::npos) {
            result.push_back(text.substr(pos));
            break;
        }
        result.push_back(text.substr(pos, next_pos - pos));
        pos = next_pos + delimiter.length();
    }
    return result;
}
