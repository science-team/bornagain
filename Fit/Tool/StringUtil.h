//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Fit/Tool/StringUtil.h
//! @brief     Defines a few helper functions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_FIT_TOOL_STRINGUTIL_H
#define BORNAGAIN_FIT_TOOL_STRINGUTIL_H

#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

//! Utility functions to analyze or modify strings.

namespace mumufit::stringUtil {

//! Split string into vector of string using delimeter.
std::vector<std::string> split(const std::string& text, const std::string& delimiter);

//! Returns scientific string representing given value of any numeric type.
template <typename T> std::string scientific(T value, int n = 10);

template <typename T> std::string scientific(const T value, int n)
{
    std::ostringstream out;
    out << std::scientific << std::setprecision(n) << value;
    return out.str();
}

} // namespace mumufit::stringUtil

#endif // BORNAGAIN_FIT_TOOL_STRINGUTIL_H
