//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Axis/AmplitudeAxisItem.h
//! @brief     Defines class AmplitudeAxisItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_AXIS_AMPLITUDEAXISITEM_H
#define BORNAGAIN_GUI_MODEL_AXIS_AMPLITUDEAXISITEM_H

#include "GUI/Model/Axis/BasicAxisItem.h"

//! Represents an intensity axis, with controls for lin/log scale.

class AmplitudeAxisItem : public BasicAxisItem {
    Q_OBJECT
public:
    explicit AmplitudeAxisItem(QObject* parent = nullptr);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    bool isLocked() const { return m_lock_min_max; }
    void setLocked(bool b) { m_lock_min_max = b; }

    DoubleProperty& logRangeOrders() { return m_log_range_orders; }
    const DoubleProperty& logRangeOrders() const { return m_log_range_orders; }
    void setLogRangeOrders(double v);
    void adjustLogRangeOrders();

    bool isVisible() const { return m_visible; }
    void setVisible(bool b);

signals:
    void axisVisibilityChanged();

private:
    bool m_lock_min_max;
    DoubleProperty m_log_range_orders;
    bool m_visible;
};

#endif // BORNAGAIN_GUI_MODEL_AXIS_AMPLITUDEAXISITEM_H
