//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Axis/BasicAxisItem.cpp
//! @brief     Implements various axis items.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Axis/BasicAxisItem.h"
#include "Base/Axis/MakeScale.h"
#include "Base/Axis/Scale.h"
#include "Base/Const/Units.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString Axis("Axis");
const QString LogScale("LogScale");

} // namespace Tag
} // namespace

BasicAxisItem::BasicAxisItem(QObject* parent)
    : QObject(parent)
    , m_log_scale(false)
    , m_axis("axis", "deg", -1, -2, RealLimits::limited(0, 90))
{
}

BasicAxisItem::~BasicAxisItem() = default;

void BasicAxisItem::setMin(double value)
{
    m_axis.min().setAndNotify(value);
    emit axisRangeChanged();
}

void BasicAxisItem::setMax(double value)
{
    m_axis.max().setAndNotify(value);
    emit axisRangeChanged();
}

void BasicAxisItem::setLogScale(bool value)
{
    m_log_scale = value;
    emit logScaleChanged(value);
}

Scale BasicAxisItem::makeAlphaScale() const
{
    return EquiScan("alpha_i (rad)", size(), Units::deg2rad(min().dVal()),
                    Units::deg2rad(max().dVal()));
}

Scale BasicAxisItem::makeLambdaScale() const
{
    return EquiScan("lambda (nm)", size(), min().dVal(), max().dVal());
}

Scale BasicAxisItem::makeQzScale() const
{
    return EquiScan("q_z (1/nm)", size(), min().dVal(), max().dVal());
}

void BasicAxisItem::writeTo(QXmlStreamWriter* w) const
{
    XML::writeTaggedElement(w, Tag::Axis, m_axis);
    XML::writeTaggedValue(w, Tag::LogScale, m_log_scale);
}

void BasicAxisItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::Axis)
            XML::readTaggedElement(r, tag, m_axis);
        else if (tag == Tag::LogScale)
            m_log_scale = XML::readTaggedBool(r, tag);
        else
            r->skipCurrentElement();
    }
}
