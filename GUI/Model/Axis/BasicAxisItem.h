//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Axis/BasicAxisItem.h
//! @brief     Defines various axis items.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_AXIS_BASICAXISITEM_H
#define BORNAGAIN_GUI_MODEL_AXIS_BASICAXISITEM_H

#include "GUI/Model/Descriptor/AxisProperty.h"
#include <QXmlStreamReader>

class Scale;

class BasicAxisItem : public QObject {
    Q_OBJECT
public:
    explicit BasicAxisItem(QObject* parent = nullptr);
    ~BasicAxisItem() override;

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);

    Scale makeAlphaScale() const;
    Scale makeLambdaScale() const;
    Scale makeQzScale() const;

    bool isLogScale() const { return m_log_scale; }
    void setLogScale(bool value);

    size_t size() const { return m_axis.nbins(); }
    void resize(size_t v) { m_axis.setNbins(v); }

    DoubleProperty& min() { return m_axis.min(); }
    const DoubleProperty& min() const { return m_axis.min(); }
    void setMin(double value);

    DoubleProperty& max() { return m_axis.max(); }
    const DoubleProperty& max() const { return m_axis.max(); }
    void setMax(double value);

signals:
    void logScaleChanged(bool isLog);
    void axisRangeChanged();
    void axisTitleChanged();

protected:
    bool m_log_scale;

private:
    AxisProperty m_axis;
};

#endif // BORNAGAIN_GUI_MODEL_AXIS_BASICAXISITEM_H
