//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Axis/PointwiseAxisItem.h
//! @brief     Defines pointwise axis item.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_AXIS_POINTWISEAXISITEM_H
#define BORNAGAIN_GUI_MODEL_AXIS_POINTWISEAXISITEM_H

#include "GUI/Model/Axis/BasicAxisItem.h"

class Frame;

//! Item for non-uniform axis with specified coordinates.
class PointwiseAxisItem : public BasicAxisItem {
    Q_OBJECT
public:
    explicit PointwiseAxisItem(QObject* parent = nullptr);
    ~PointwiseAxisItem() override;

    // setters, getters
    void setAxis(const Scale& axis);
    const Scale* axis() const;

    void updateAxIndicators(const Frame& frame);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

private:
    std::unique_ptr<Scale> m_axis;

    QByteArray serializeBinaryData() const;
    void deserializeBinaryData(const QByteArray& data);
};

#endif // BORNAGAIN_GUI_MODEL_AXIS_POINTWISEAXISITEM_H
