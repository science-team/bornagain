//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/BeamDistributionItem.h
//! @brief     Defines class BeamDistributionItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_BEAM_BEAMDISTRIBUTIONITEM_H
#define BORNAGAIN_GUI_MODEL_BEAM_BEAMDISTRIBUTIONITEM_H

#include "GUI/Model/Beam/DistributionCatalog.h"
#include "GUI/Model/Beam/DistributionItems.h"
#include "GUI/Model/Descriptor/PolyPtr.h"
#include "Param/Distrib/ParameterDistribution.h"

//! Holds a DistributionItem, for use as a beam parameter distribution.

class BeamDistributionItem {
public:
    BeamDistributionItem(double scale = 1.);
    virtual ~BeamDistributionItem();

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);

    virtual double centralValue() const;
    void resetToValue(double value);

    void setScaleFactor(double v) { m_scale = v; }
    double scaleFactor() const { return m_scale; }

    DistributionItem* distributionItem() const { return m_distribution.certainItem(); }
    PolyPtr<DistributionItem, DistributionCatalog>& distributionSelection()
    {
        return m_distribution;
    }

protected:
    double m_scale;
    PolyPtr<DistributionItem, DistributionCatalog> m_distribution;
};

#endif // BORNAGAIN_GUI_MODEL_BEAM_BEAMDISTRIBUTIONITEM_H
