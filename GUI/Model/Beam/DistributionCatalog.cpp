//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/DistributionCatalog.cpp
//! @brief     Implements class DistributionCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Beam/DistributionCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Beam/DistributionItems.h"

DistributionItem* DistributionCatalog::create(Type type)
{
    switch (type) {
    case Type::Delta:
        return new DistributionDeltaItem;
    case Type::Gate:
        return new DistributionGateItem;
    case Type::Lorentz:
        return new DistributionLorentzItem;
    case Type::Gaussian:
        return new DistributionGaussianItem;
    case Type::LogNormal:
        return new DistributionLogNormalItem;
    case Type::Cosine:
        return new DistributionCosineItem;
    }
    ASSERT_NEVER;
}

QVector<DistributionCatalog::Type> DistributionCatalog::types()
{
    return {Type::Delta, Type::Gate, Type::Lorentz, Type::Gaussian, Type::LogNormal, Type::Cosine};
}

UiInfo DistributionCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::Delta:
        return {"Delta", "", ""};
    case Type::Gate:
        return {"Gate", "", ""};
    case Type::Lorentz:
        return {"Lorentz", "", ""};
    case Type::Gaussian:
        return {"Gaussian", "", ""};
    case Type::LogNormal:
        return {"Log Normal", "", ""};
    case Type::Cosine:
        return {"Cosine", "", ""};
    }
    ASSERT_NEVER;
}

DistributionCatalog::Type DistributionCatalog::type(const DistributionItem* item)
{
    if (dynamic_cast<const DistributionDeltaItem*>(item))
        return Type::Delta;
    if (dynamic_cast<const DistributionGateItem*>(item))
        return Type::Gate;
    if (dynamic_cast<const DistributionLorentzItem*>(item))
        return Type::Lorentz;
    if (dynamic_cast<const DistributionGaussianItem*>(item))
        return Type::Gaussian;
    if (dynamic_cast<const DistributionLogNormalItem*>(item))
        return Type::LogNormal;
    if (dynamic_cast<const DistributionCosineItem*>(item))
        return Type::Cosine;

    ASSERT_NEVER;
}
