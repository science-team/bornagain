//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/DistributionCatalog.h
//! @brief     Defines class DistributionCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_BEAM_DISTRIBUTIONCATALOG_H
#define BORNAGAIN_GUI_MODEL_BEAM_DISTRIBUTIONCATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class DistributionItem;

class DistributionCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t {
        Delta = 0,
        Gate = 1,
        Lorentz = 2,
        Gaussian = 3,
        LogNormal = 4,
        Cosine = 5,
    };

    //! Creates the item of the given type.
    static DistributionItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const DistributionItem* item);
};

#endif // BORNAGAIN_GUI_MODEL_BEAM_DISTRIBUTIONCATALOG_H
