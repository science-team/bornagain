//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/FootprintCatalog.cpp
//! @brief     Implements class FootprintCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Beam/FootprintCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Beam/FootprintItems.h"

FootprintItem* FootprintCatalog::create(Type type)
{
    switch (type) {
    case Type::None:
        return new FootprintNoneItem();
    case Type::Gaussian:
        return new FootprintGaussianItem();
    case Type::Square:
        return new FootprintSquareItem();
    }
    ASSERT_NEVER;
}

QVector<FootprintCatalog::Type> FootprintCatalog::types()
{
    return {Type::None, Type::Gaussian, Type::Square};
}

UiInfo FootprintCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::None:
        return {"None", "", ""};
    case Type::Gaussian:
        return {"Gaussian footprint", "", ""};
    case Type::Square:
        return {"Square footprint", "", ""};
    }
    ASSERT_NEVER;
}

FootprintCatalog::Type FootprintCatalog::type(const FootprintItem* item)
{
    if (dynamic_cast<const FootprintNoneItem*>(item))
        return Type::None;
    if (dynamic_cast<const FootprintGaussianItem*>(item))
        return Type::Gaussian;
    if (dynamic_cast<const FootprintSquareItem*>(item))
        return Type::Square;

    ASSERT_NEVER;
}
