//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/FootprintItems.cpp
//! @brief     Implements class FootprintItemes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Beam/FootprintItems.h"
#include "Device/Beam/FootprintGauss.h"
#include "Device/Beam/FootprintSquare.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString GaussianFootprint("GaussianFootprint");
const QString SquareFootprint("SquareFootprint");

} // namespace Tag
} // namespace

std::unique_ptr<IFootprint> FootprintNoneItem::createFootprint() const
{
    return {};
}

FootprintGaussianItem::FootprintGaussianItem(double value)
{
    m_gaussian_footprint_value.init("Footprint width ratio", "",
                                    "The ratio of beam and sample full widths", value, 5,
                                    RealLimits::nonnegative(), "ratio");
}

void FootprintGaussianItem::writeTo(QXmlStreamWriter* w) const
{
    m_gaussian_footprint_value.writeTo2(w, Tag::GaussianFootprint);
}

void FootprintGaussianItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::GaussianFootprint)
            m_gaussian_footprint_value.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

std::unique_ptr<IFootprint> FootprintGaussianItem::createFootprint() const
{
    return std::make_unique<FootprintGauss>(m_gaussian_footprint_value.dVal());
}

FootprintSquareItem::FootprintSquareItem(double value)
{
    m_square_footprint_value.init("Footprint width ratio", "",
                                  "The ratio of beam and sample full widths", value, 5,
                                  RealLimits::nonnegative(), "ratio");
}

void FootprintSquareItem::writeTo(QXmlStreamWriter* w) const
{
    m_square_footprint_value.writeTo2(w, Tag::SquareFootprint);
}

void FootprintSquareItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::SquareFootprint)
            m_square_footprint_value.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

std::unique_ptr<IFootprint> FootprintSquareItem::createFootprint() const
{
    return std::make_unique<FootprintSquare>(m_square_footprint_value.dVal());
}
