//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/FootprintItems.h
//! @brief     Declares class FootprintItemes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_BEAM_FOOTPRINTITEMS_H
#define BORNAGAIN_GUI_MODEL_BEAM_FOOTPRINTITEMS_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include <memory>

class IFootprint;

class FootprintItem {
public:
    virtual ~FootprintItem() = default;

    virtual void writeTo(QXmlStreamWriter*) const {}
    virtual void readFrom(QXmlStreamReader*) {}

    virtual std::unique_ptr<IFootprint> createFootprint() const = 0;
};

class FootprintNoneItem : public FootprintItem {
public:
    std::unique_ptr<IFootprint> createFootprint() const override;
};

class FootprintGaussianItem : public FootprintItem {
public:
    explicit FootprintGaussianItem(double value = 0.0);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    std::unique_ptr<IFootprint> createFootprint() const override;

    DoubleProperty& gaussianFootprintValue() { return m_gaussian_footprint_value; }
    const DoubleProperty& gaussianFootprintValue() const { return m_gaussian_footprint_value; }
    void setGaussianFootprintValue(double v) { m_gaussian_footprint_value.setDVal(v); }

protected:
    DoubleProperty m_gaussian_footprint_value;
};

class FootprintSquareItem : public FootprintItem {
public:
    explicit FootprintSquareItem(double value = 0.0);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    std::unique_ptr<IFootprint> createFootprint() const override;

    DoubleProperty& squareFootprintValue() { return m_square_footprint_value; }
    const DoubleProperty& squareFootprintValue() const { return m_square_footprint_value; }
    void setSquareFootprintValue(double v) { m_square_footprint_value.setDVal(v); }

protected:
    DoubleProperty m_square_footprint_value;
};

#endif // BORNAGAIN_GUI_MODEL_BEAM_FOOTPRINTITEMS_H
