//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/ScanTypeCatalog.cpp
//! @brief     Implements class ScanTypeCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Beam/ScanTypeCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Beam/ScanTypeItems.h"

ScanTypeItem* ScanTypeCatalog::create(Type type)
{
    switch (type) {
    case Type::Alpha:
        return new AlphaScanTypeItem();
    case Type::Lambda:
        return new LambdaScanTypeItem();
    case Type::Qz:
        return new QzScanTypeItem();
    }
    ASSERT_NEVER;
}

QVector<ScanTypeCatalog::Type> ScanTypeCatalog::types()
{
    return {Type::Alpha, Type::Lambda, Type::Qz};
}

QVector<ScanTypeCatalog::Type> ScanTypeCatalog::nonPhysicalTypes()
{
    return {Type::Qz};
}

UiInfo ScanTypeCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::Alpha:
        return {"Grazing angle scan", "", ""};
    case Type::Lambda:
        return {"Wavelength scan", "", ""};
    case Type::Qz:
        return {"Qz scan", "", ""};
    }
    ASSERT_NEVER;
}

ScanTypeCatalog::Type ScanTypeCatalog::type(const ScanTypeItem* item)
{
    if (dynamic_cast<const AlphaScanTypeItem*>(item))
        return Type::Alpha;
    if (dynamic_cast<const LambdaScanTypeItem*>(item))
        return Type::Lambda;
    if (dynamic_cast<const QzScanTypeItem*>(item))
        return Type::Qz;

    ASSERT_NEVER;
}
