//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/ScanTypeCatalog.h
//! @brief     Defines class ScanTypeCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_BEAM_SCANTYPECATALOG_H
#define BORNAGAIN_GUI_MODEL_BEAM_SCANTYPECATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class ScanTypeItem;

class ScanTypeCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t { Alpha = 0, Lambda = 1, Qz = 2 };

    //! Creates the item of the given type.
    static ScanTypeItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    static QVector<Type> nonPhysicalTypes();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const ScanTypeItem* item);
};

#endif // BORNAGAIN_GUI_MODEL_BEAM_SCANTYPECATALOG_H
