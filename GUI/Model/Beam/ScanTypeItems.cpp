//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/ScanTypeItems.cpp
//! @brief     Defines ScanTypeItems classes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Beam/ScanTypeItems.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString BaseData("BaseData");
const QString UseRelativeResolution("UseRelativeResolution");

} // namespace Tag
} // namespace

void QzScanTypeItem::writeTo(QXmlStreamWriter* w) const
{
    XML::writeBaseElement<ScanTypeItem>(w, XML::Tag::BaseData, this);
    XML::writeTaggedValue(w, Tag::UseRelativeResolution, m_use_relative_resolution);
}

void QzScanTypeItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::BaseData)
            XML::readBaseElement<ScanTypeItem>(r, tag, this);
        else if (tag == Tag::UseRelativeResolution)
            m_use_relative_resolution = XML::readTaggedBool(r, tag);
        else
            r->skipCurrentElement();
    }
}
