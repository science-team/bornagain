//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/ScanTypeItems.h
//! @brief     Declares class ScanTypeItems.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_BEAM_SCANTYPEITEMS_H
#define BORNAGAIN_GUI_MODEL_BEAM_SCANTYPEITEMS_H

#include <QXmlStreamReader>

class ScanTypeItem {
public:
    virtual ~ScanTypeItem() = default;

    void writeTo(QXmlStreamWriter*) const {}
    void readFrom(QXmlStreamReader*) {}
};

class AlphaScanTypeItem : public ScanTypeItem {};

class LambdaScanTypeItem : public ScanTypeItem {};

#endif // BORNAGAIN_GUI_MODEL_BEAM_SCANTYPEITEMS_H
