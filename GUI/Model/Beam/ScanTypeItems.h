//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/ScanTypeItems.h
//! @brief     Declares ScanTypeItems classes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_BEAM_SCANTYPEITEMS_H
#define BORNAGAIN_GUI_MODEL_BEAM_SCANTYPEITEMS_H

#include <QXmlStreamReader>

class ScanTypeItem {
public:
    virtual ~ScanTypeItem() = default;

    virtual void writeTo(QXmlStreamWriter*) const {}
    virtual void readFrom(QXmlStreamReader*) {}
};

class AlphaScanTypeItem : public ScanTypeItem {};

class LambdaScanTypeItem : public ScanTypeItem {};

class QzScanTypeItem : public ScanTypeItem {
public:
    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    void setUseRelativeResolution(bool b) { m_use_relative_resolution = b; }
    bool isUseRelativeResolution() const { return m_use_relative_resolution; }

private:
    bool m_use_relative_resolution = true;
};

#endif // BORNAGAIN_GUI_MODEL_BEAM_SCANTYPEITEMS_H
