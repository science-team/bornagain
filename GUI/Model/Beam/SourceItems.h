//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Beam/SourceItems.h
//! @brief     Defines BeamItem hierarchy.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_BEAM_SOURCEITEMS_H
#define BORNAGAIN_GUI_MODEL_BEAM_SOURCEITEMS_H

#include "GUI/Model/Beam/FootprintCatalog.h"
#include "GUI/Model/Beam/FootprintItems.h"
#include "GUI/Model/Beam/ScanTypeCatalog.h"
#include "GUI/Model/Beam/ScanTypeItems.h"
#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "GUI/Model/Descriptor/PolyPtr.h"
#include <functional>
#include <heinz/Vectors3D.h>

class BasicAxisItem;
class Beam;
class BeamDistributionItem;
class BeamScan;
class Frame;
class PointwiseAxisItem;
class Scale;

//! Base class for BeamItem and ScanItem. Name refers to radiation source.
class SourceItem {
public:
    DoubleProperty& intensity() { return m_intensity; }
    const DoubleProperty& intensity() const { return m_intensity; }
    void setIntensity(double v) { m_intensity.setDVal(v); }

    BeamDistributionItem* wavelengthItem() const;
    void setWavelength(double value);

    BeamDistributionItem* grazingAngleItem() const;
    void setGrazingAngle(double value);

    BeamDistributionItem* azimuthalAngleItem() const;
    void setAzimuthalAngle(double value);

    PolyPtr<FootprintItem, FootprintCatalog>& footprintSelection() { return m_footprint; }
    void setFootprintItem(const IFootprint* footprint);
    void setGaussianFootprint(double value);
    void setSquareFootprint(double value);

    bool expandBeamParameters = true;
    bool expandFootprint = true;

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

protected:
    SourceItem();

    DoubleProperty m_intensity;
    std::unique_ptr<BeamDistributionItem> m_wavelength_item;
    std::unique_ptr<BeamDistributionItem> m_grazing_angle_item;
    std::unique_ptr<BeamDistributionItem> m_azimuthal_angle_item;
    PolyPtr<FootprintItem, FootprintCatalog> m_footprint;
};

class BeamItem : public SourceItem {
public:
    BeamItem();
    std::unique_ptr<Beam> createBeam() const;
};

class ScanItem : public SourceItem {
public:
    ScanItem();

    void setScan(const BeamScan* scan);

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    PolyPtr<ScanTypeItem, ScanTypeCatalog>& scanTypeSelection() { return m_scan_type; }
    const PolyPtr<ScanTypeItem, ScanTypeCatalog>& scanTypeSelection() const { return m_scan_type; }

    BeamDistributionItem* scanDistributionItem() const { return m_scan_distribution_item.get(); }

    void updateToData(const Scale& axis);

    int nBins() const;

    //! The currently selected axis
    BasicAxisItem* currentAxisItem() const;

    Scale* newUniformScale() const;
    Scale* newPointwiseScale() const;

    //! True if a pointwise axis was defined.
    //!
    //! It still is no necessarily the selected axis!
    //! Not to be confused with pointwiseAxisSelected
    bool pointwiseAxisDefined() const;

    //! True if pointwise axis is selected.
    //!
    //! Not to be confused with pointwiseAxisDefined
    bool pointwiseAxisSelected() const { return !m_current_axis_is_uniform_axis; }

    //! True if uniform axis is selected.
    bool uniformAxisSelected() const { return m_current_axis_is_uniform_axis; }

    void selectUniformAxis();
    void selectListScan();

    void initUniformAxis(const Scale& axis);
    void initListScan(const Scale& axis);
    void updateAxIndicators(const Frame& frame);

private:
    void setAxisPresentationDefaults(BasicAxisItem* axisItem) const;

    PolyPtr<ScanTypeItem, ScanTypeCatalog> m_scan_type;
    bool m_current_axis_is_uniform_axis;
    std::unique_ptr<BasicAxisItem> m_uniform_axis;
    std::unique_ptr<PointwiseAxisItem> m_pointwise_axis;
    std::unique_ptr<BeamDistributionItem> m_scan_distribution_item;
};

#endif // BORNAGAIN_GUI_MODEL_BEAM_SOURCEITEMS_H
