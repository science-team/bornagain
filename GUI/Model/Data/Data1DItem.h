//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Data/Data1DItem.h
//! @brief     Defines class Data1DItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DATA_DATA1DITEM_H
#define BORNAGAIN_GUI_MODEL_DATA_DATA1DITEM_H

#include "GUI/Model/Data/DataItem.h"
#include <qcustomplot.h>
#include <utility>

class Data1DItem : public DataItem {
    Q_OBJECT
public:
    static constexpr auto M_TYPE{"SpecularData"};

    Data1DItem();

    // TODO: consider using index-based functions for axes' handlers

    void setDatafield(const Datafield& data) override;

    double xMin() const override;
    double xMax() const override;

    double yMin() const override;
    double yMax() const override;
    std::pair<double, double> dataRange() const;

    //... logarithmic Y scale
    bool isLogY() const;
    void setLogY(bool islog);

    //... logarithmic X scale
    bool isLogX() const;
    void setLogX(bool islog);

    size_t rank() const override { return 1; }
    size_t axdim(int i) const override;

    bool isValAxisLocked() const override;
    void setValAxisLocked(bool state) override;

    //! Sets axes viewport to original data.
    void resetView() override;

    // Line type for plotting
    QCPGraph::LineStyle lineStyle();
    void setLineStyle(QCPGraph::LineStyle lineStyle);

    //! Color for plotting
    QColor color() const { return m_color; }
    void setColor(Qt::GlobalColor color);

    //! Line thickness for plotting
    double thickness() const { return m_thickness; }
    void setThickness(double thickness);

    //! Scatter shape for plotting
    QCPScatterStyle::ScatterShape scatter();
    void setScatter(QCPScatterStyle::ScatterShape scatter);

    //! Scatter size for plotting
    double scatterSize() const { return m_scatter_size; }
    void setScatterSize(double scatterSize);

    void setSimuPlotStyle();
    void setDiffPlotStyle();
    void setRealPlotStyle();

    //... write/read
    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

private:
    void updateAxesZoomLevel();

    QString m_line_type;
    QColor m_color;
    double m_thickness;
    QString m_scatter_type;
    double m_scatter_size;
};

#endif // BORNAGAIN_GUI_MODEL_DATA_DATA1DITEM_H
