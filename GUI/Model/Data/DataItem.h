//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Data/DataItem.h
//! @brief     Declares class DataItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DATA_DATAITEM_H
#define BORNAGAIN_GUI_MODEL_DATA_DATAITEM_H

#include <QDateTime>
#include <QXmlStreamReader>
#include <mutex>

class AmplitudeAxisItem;
class BasicAxisItem;
class Datafield;
class Frame;

//! Abstract base class for Data2DItem and Data1DItem. Owns one Datafield.

class DataItem : public QObject {
    Q_OBJECT
protected:
    explicit DataItem(const QString& modelType);

public:
    ~DataItem();

    const QString TYPE{"uninitialized"};

    // Returns datafield, owned by this class
    Datafield* p_field() { return m_datafield.get(); }
    const Datafield* c_field() const { return m_datafield.get(); }

    virtual void setDatafield(const Datafield& data) = 0;

    //! Sets the raw data vector from external source.
    //! Checks only the equality of data size; no dimension checks are applied.
    void setRawDataVector(const std::vector<double>& data);

    QString fname() const { return m_fname; }
    void setFileName(const QString& filename);
    QString dataFullPath(const QString& projectDir) const;

    QDateTime lastModified() const { return m_last_modified; }
    void setLastModified(const QDateTime& dtime);

    void loadDatafield(const QString& projectDir, int rank);
    void saveDatafield(const QString& projectDir) const;

    // Number of bins in data
    int xSize() const;
    int ySize() const;

    // Returns min and max range of x-axis as given by data
    virtual double xMin() const = 0;
    virtual double xMax() const = 0;

    // Returns min and max range of y-axis as given by data
    virtual double yMin() const = 0;
    virtual double yMax() const = 0;

    // Lower and upper zoom ranges of x-axis
    double lowerX() const;
    double upperX() const;
    void setXrange(double lower, double upper);

    // Lower and upper zoom ranges of y-axis
    double lowerY() const;
    double upperY() const;
    void setYrange(double lower, double upper);

    // Adjust zoom range of this item to the other items range
    void alignXranges(DataItem* sourceItem);
    void alignYranges(DataItem* sourceItem);
    void alignXYranges(DataItem* sourceItem);

    bool isArgRangeLocked() const { return m_lock_arg_range; }
    void setArgRangeLocked(bool b) { m_lock_arg_range = b; }

    virtual bool isValAxisLocked() const = 0;
    virtual void setValAxisLocked(bool state) = 0;

    const BasicAxisItem* axItemX() const;
    BasicAxisItem* axItemX();
    const AmplitudeAxisItem* axItemY() const;
    AmplitudeAxisItem* axItemY();

    // Axes titles
    QString xAxisLabel() const;
    QString yAxisLabel() const;

    virtual size_t rank() const = 0;
    virtual size_t axdim(int i) const = 0;

    //! Set axes viewport to original data.
    virtual void resetView() = 0;

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);

    bool wasModifiedSinceLastSave() const;

signals:
    void datafieldChanged();
    void updateOtherPlots(DataItem* sender);

protected:
    std::unique_ptr<Datafield> m_datafield; //!< simulation results
    mutable std::mutex m_update_data_mutex;

    QString m_fname;
    bool m_lock_arg_range;
    std::unique_ptr<BasicAxisItem> m_x_axis;
    std::unique_ptr<AmplitudeAxisItem> m_y_axis;

    QDateTime m_last_modified;
    mutable QDateTime m_last_saved;

    void setTheDatafield(const Datafield& data);
};

#endif // BORNAGAIN_GUI_MODEL_DATA_DATAITEM_H
