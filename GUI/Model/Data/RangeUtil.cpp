//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Data/RangeUtil.cpp
//! @brief     Implements namespace RangeUtil.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Data/RangeUtil.h"
#include "GUI/Model/Axis/AmplitudeAxisItem.h"
#include "GUI/Model/Data/Data1DItem.h"
#include "GUI/Model/Data/Data2DItem.h"
#include <utility>

namespace {

double commonMin(const QVector<AmplitudeAxisItem*>& axes)
{
    double min = +std::numeric_limits<double>::max();
    for (auto* axis : axes)
        if (min > axis->min().dVal())
            min = axis->min().dVal();
    return min;
}

double commonMax(const QVector<AmplitudeAxisItem*>& axes)
{
    double max = -std::numeric_limits<double>::max();
    for (auto* axis : axes)
        if (max < axis->max().dVal())
            max = axis->max().dVal();
    return max;
}

std::pair<double, double> commonRange(const QVector<AmplitudeAxisItem*>& axes)
{
    return {commonMin(axes), commonMax(axes)};
}

QVector<AmplitudeAxisItem*> valueAxesFromData1DItems(const QVector<Data1DItem*>& items)
{
    QVector<AmplitudeAxisItem*> axes;
    for (auto* item : items)
        axes.append(item->axItemY());
    return axes;
}

QVector<AmplitudeAxisItem*> valueAxesFromData2DItems(const QVector<Data2DItem*>& items)
{
    QVector<AmplitudeAxisItem*> axes;
    for (auto* item : items)
        axes.append(item->zAxisItem());
    return axes;
}

} // namespace

void GUI::Util::Ranges::setCommonRangeY(QVector<Data1DItem*> items)
{
    std::pair<double, double> range = commonRange(valueAxesFromData1DItems(items));
    for (auto* item : items)
        item->setYrange(range.first, range.second);
}

void GUI::Util::Ranges::setCommonRangeZ(QVector<Data2DItem*> items)
{
    std::pair<double, double> range = commonRange(valueAxesFromData2DItems(items));
    for (auto* item : items)
        item->setZrange(range.first, range.second);
}
