//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Data/RangeUtil.h
//! @brief     Defines namespace RangeUtil.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DATA_RANGEUTIL_H
#define BORNAGAIN_GUI_MODEL_DATA_RANGEUTIL_H

#include <QVector>

class Data1DItem;
class Data2DItem;

//! Provides few helper functions for ColorMapPlot.

namespace GUI::Util::Ranges {

//! Sets common Y range for a list of items (zoom).
void setCommonRangeY(QVector<Data1DItem*> items);

//! Sets common Z range for a list of items (zoom).
void setCommonRangeZ(QVector<Data2DItem*> items);

} // namespace GUI::Util::Ranges

#endif // BORNAGAIN_GUI_MODEL_DATA_RANGEUTIL_H
