//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Descriptor/AxisProperty.cpp
//! @brief     Implements class AxisProperty.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Descriptor/AxisProperty.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString Nbins("Nbins");
const QString Min("Min");
const QString Max("Max");

} // namespace Tag
} // namespace


AxisProperty::AxisProperty(const QString& name, const QString& unit, double _min, double _max,
                           const RealLimits& limit)
{
    m_min.init("Min", unit, "Lower edge of first " + name + "-bin", _min, 3, limit, "min");
    m_max.init("Max", unit, "Upper edge of last " + name + "-bin", _max, 3, limit, "max");
}

void AxisProperty::writeTo(QXmlStreamWriter* w) const
{
    XML::writeTaggedValue(w, Tag::Nbins, m_nbins);

    // min
    m_min.writeTo2(w, Tag::Min);

    // max
    m_max.writeTo2(w, Tag::Max);
}

void AxisProperty::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::Nbins)
            m_nbins = XML::readTaggedUInt(r, tag);
        else if (tag == Tag::Min) {
            m_min.readFrom2(r, tag);
        } else if (tag == Tag::Max) {
            m_max.readFrom2(r, tag);

        } else
            r->skipCurrentElement();
    }
}
