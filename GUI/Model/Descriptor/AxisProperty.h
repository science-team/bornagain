//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Descriptor/AxisProperty.h
//! @brief     Defines class AxisProperty.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DESCRIPTOR_AXISPROPERTY_H
#define BORNAGAIN_GUI_MODEL_DESCRIPTOR_AXISPROPERTY_H

#include "GUI/Model/Descriptor/DoubleProperty.h"

//! Holds values which can be used to describe a EquiDivision.
//!
//! Use this as a member in your class to
//! * reduce dependency to axis items and axis classes
//! * simplify serialization
//! * easily have a UI supporting solution.
//!
//! Do not forget to call all two init functions from within the containing class's constructor!

class AxisProperty {
public:
    AxisProperty(const QString& name, const QString& unit, double _min, double _max,
                 const RealLimits& limit);

    DoubleProperty& min() { return m_min; }
    const DoubleProperty& min() const { return m_min; }

    DoubleProperty& max() { return m_max; }
    const DoubleProperty& max() const { return m_max; }

    uint nbins() const { return m_nbins; }
    void setNbins(uint v) { m_nbins = v; }

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

private:
    uint m_nbins = 100;
    DoubleProperty m_min;
    DoubleProperty m_max;
};

#endif // BORNAGAIN_GUI_MODEL_DESCRIPTOR_AXISPROPERTY_H
