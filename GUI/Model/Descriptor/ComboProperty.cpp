//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Descriptor/ComboProperty.cpp
//! @brief     Implements class ComboProperty.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Descriptor/ComboProperty.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Util/UtilXML.h"

ComboProperty::ComboProperty() = default;

ComboProperty::ComboProperty(const QStringList& values, int index)
    : m_values(values)
    , m_current_index(index)
{
    ASSERT(!values.empty());
    ASSERT(index >= 0 && index < m_values.size());
}

ComboProperty ComboProperty::fromList(const QStringList& values, int index)
{
    return ComboProperty(values, index);
}

ComboProperty ComboProperty::fromStdVec(const std::vector<std::string>& values, int index)
{
    QStringList q_list;
    for (const std::string& val : values)
        q_list << QString::fromStdString(val);
    return ComboProperty(q_list, index);
}

QString ComboProperty::currentValue() const
{
    return currentIndex() < 0 ? QString() : m_values.at(currentIndex());
}

void ComboProperty::setCurrentValue(const QString& name)
{
    ASSERT(m_values.contains(name));
    setCurrentIndex(m_values.indexOf(name));
}

void ComboProperty::setToolTips(const QStringList& tooltips)
{
    ASSERT(tooltips.size() == m_values.size());
    m_tooltips = tooltips;
}

void ComboProperty::setCurrentIndex(int index)
{
    ASSERT(index >= 0 && index < m_values.size());
    m_current_index = index;
}

QVariant ComboProperty::variant() const
{
    QVariant result;
    result.setValue(*this);
    return result;
}

void ComboProperty::writeTo(QXmlStreamWriter* w) const
{
    XML::writeAttribute(w, XML::Attrib::index, currentIndex());
    // The next line allows to see the entry name in XML. It may be skipped while reading.
    XML::writeAttribute(w, XML::Attrib::name, currentValue());
}

void ComboProperty::readFrom(QXmlStreamReader* r)
{
    int index = XML::readInt(r, XML::Attrib::index);
    setCurrentIndex(index);
}
