//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Descriptor/ComboProperty.h
//! @brief     Defines class ComboProperty.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DESCRIPTOR_COMBOPROPERTY_H
#define BORNAGAIN_GUI_MODEL_DESCRIPTOR_COMBOPROPERTY_H

#include <QStringList>
#include <QVariant>
#include <QVector>
#include <QXmlStreamReader>

//! Custom property to define list of string values with multiple selections.
//! Intended for QVariant.

class ComboProperty {
public:
    ComboProperty();

    static ComboProperty fromList(const QStringList& values, int index = 0);
    static ComboProperty fromStdVec(const std::vector<std::string>& values, int index = 0);

    const QStringList& values() const { return m_values; }
    const QStringList& toolTips() const { return m_tooltips; }

    QString currentValue() const;
    void setCurrentValue(const QString& name);

    void setToolTips(const QStringList& tooltips);

    int currentIndex() const { return m_current_index; }
    void setCurrentIndex(int index);

    QVariant variant() const;

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

private:
    ComboProperty(const QStringList& values, int index);

    QStringList m_values;
    QStringList m_tooltips;
    int m_current_index;
};

Q_DECLARE_METATYPE(ComboProperty)

#endif // BORNAGAIN_GUI_MODEL_DESCRIPTOR_COMBOPROPERTY_H
