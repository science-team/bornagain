//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Descriptor/DoubleProperty.cpp
//! @brief     Implements class DoubleProperty.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "GUI/Model/Util/UtilXML.h"
#include <QUuid>

void DoubleProperty::init(const QString& label, const QString& units, const QString& tooltip,
                          double value, const QString& uidPrefix)
{
    RealLimits limits = RealLimits::nonnegative();
    init(label, units, tooltip, value, 3, limits.isLimited(), 0.01, limits, uidPrefix);
}

void DoubleProperty::init(const QString& label, const QString& units, const QString& tooltip,
                          double value, uint decimals, const RealLimits& limits,
                          const QString& uidPrefix)
{
    init(label, units, tooltip, value, decimals, limits.isLimited(), 0.01, limits, uidPrefix);
}

void DoubleProperty::init(const QString& label, const QString& units, const QString& tooltip,
                          double value, uint decimals, double step, const RealLimits& limits,
                          const QString& uidPrefix)
{
    init(label, units, tooltip, value, decimals, limits.isLimited(), step, limits, uidPrefix);
}

void DoubleProperty::init(const QString& label, const QString& units, const QString& tooltip,
                          double value, uint decimals, bool useFixedStep, const RealLimits& limits,
                          const QString& uidPrefix)
{
    init(label, units, tooltip, value, decimals, useFixedStep, 0.01, limits, uidPrefix);
}

void DoubleProperty::init(const QString& label, const QString& units, const QString& tooltip,
                          double value, uint decimals, bool useFixedStep, double step,
                          const RealLimits& limits, const QString& uidPrefix)
{
    m_label = label;
    m_units = units;
    m_tooltip = tooltip;
    m_value = value;
    m_decimals = decimals;
    m_step = step;
    m_use_fixed_step = useFixedStep;
    m_limits = limits;

    if (uidPrefix.size() > 0)
        m_uid = uidPrefix + "/" + QUuid::createUuid().toString();
    else
        m_uid = QUuid::createUuid().toString();
}

void DoubleProperty::setStepAndDecimals(bool useFixedStep, double step, int decimals)
{
    setUseFixedStep(useFixedStep);
    setStep(step);
    setDecimals(decimals);
}

void DoubleProperty::writeTo2(QXmlStreamWriter* w, const QString& tag) const
{
    w->writeStartElement(tag);
    XML::writeAttribute(w, XML::Attrib::value, m_value);
    XML::writeAttribute(w, XML::Attrib::id, m_uid);
    XML::writeAttribute(w, XML::Attrib::units, m_units); // serialization for copying
    w->writeEndElement();
}

void DoubleProperty::readFrom2(QXmlStreamReader* r, const QString& tag)
{
    m_uid = XML::readString(r, XML::Attrib::id);
    m_units = XML::readString(r, XML::Attrib::units);
    m_value = XML::readTaggedDouble(r, tag);
}

void DoubleProperty::setAndNotify(double d)
{
    if (d == m_value)
        return;
    m_value = d;
    emit setAndNotifyCalled();
}

QString DoubleProperty::label() const
{
    if (m_units == "")
        return m_label;

    return m_label + " [" + m_units + "]";
}
