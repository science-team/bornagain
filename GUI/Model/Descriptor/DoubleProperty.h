//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Descriptor/DoubleProperty.h
//! @brief     Defines class DoubleProperty.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DESCRIPTOR_DOUBLEPROPERTY_H
#define BORNAGAIN_GUI_MODEL_DESCRIPTOR_DOUBLEPROPERTY_H

#include "Fit/Param/RealLimits.h"
#include <QString>
#include <QVector>
#include <QXmlStreamReader>

//! Class for representing a double value, its attributes and its accessors.
//!
//! Contained attributes are
//! * the value itself
//! * label: a label of e.g. a spin box
//! * tooltip: tooltip for e.g. a spin box
//! * decimals: how many decimals shall be shown in a spin box
//! * limits: which limits shall be set in a spin box or other validator
//! * uid: a unique id which represents this property. This is used for linking to this property,
//!   also when serializing the link. Right now, this uid is a UUID.

class DoubleProperty : public QObject {
    Q_OBJECT
public:
    void init(const QString& label, const QString& units, const QString& tooltip, double value,
              const QString& uidPrefix);
    void init(const QString& label, const QString& units, const QString& tooltip, double value,
              uint decimals, const RealLimits& limits, const QString& uidPrefix);
    void init(const QString& label, const QString& units, const QString& tooltip, double value,
              uint decimals, double step, const RealLimits& limits, const QString& uidPrefix);
    void init(const QString& label, const QString& units, const QString& tooltip, double value,
              uint decimals, bool useFixedStep, const RealLimits& limits, const QString& uidPrefix);
    void init(const QString& label, const QString& units, const QString& tooltip, double value,
              uint decimals, bool useFixedStep, double step, const RealLimits& limits,
              const QString& uidPrefix);

    double dVal() const { return m_value; }
    void setDVal(double d) { m_value = d; }
    void setAndNotify(double d);

    const QString& units() const { return m_units; }
    void setUnits(const QString& s) { m_units = s; }

    const QString& uid() const { return m_uid; }
    void setUid(const QString& uid) { m_uid = uid; }

    double step() const { return m_step; }
    void setStep(double d) { m_step = d; }

    bool useFixedStep() const { return m_use_fixed_step; }
    void setUseFixedStep(bool b) { m_use_fixed_step = b; }

    QString label() const;

    uint decimals() const { return m_decimals; }
    void setDecimals(uint decimals) { m_decimals = decimals; }

    const QString& tooltip() const { return m_tooltip; }

    const RealLimits& limits() const { return m_limits; }
    void setLimits(const RealLimits& limits) { m_limits = limits; }

    void writeTo2(QXmlStreamWriter* w, const QString& tag) const;
    void readFrom2(QXmlStreamReader* r, const QString& tag);

    void setStepAndDecimals(bool useFixedStep, double step, int decimals);

signals:
    void setAndNotifyCalled() const;

private:
    double m_value = 0.0; //!< Current value
    QString m_uid;        //!< Unique id of this double property.
    QString m_units;      //!< value is given in this units

    // without serialization
    QString m_label;              //!< A label text (short, no trailing colon)
    QString m_tooltip;            //!< Tooltip text
    uint m_decimals = 3;          //!< numbers of decimals to be shown in an edit control
    double m_step = 0.01;         //!< initial step for up/down arrows
    bool m_use_fixed_step = true; //!< use non-adaptive step
    RealLimits m_limits;          //!< Limits of the value.
};

using DoubleProperties = QVector<DoubleProperty*>;

#endif // BORNAGAIN_GUI_MODEL_DESCRIPTOR_DOUBLEPROPERTY_H
