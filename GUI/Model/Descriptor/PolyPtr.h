//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Descriptor/PolyPtr.h
//! @brief     Defines and implements template class PolyPtr.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DESCRIPTOR_POLYPTR_H
#define BORNAGAIN_GUI_MODEL_DESCRIPTOR_POLYPTR_H

#include "GUI/Model/Util/UtilXML.h"

class PolyBase {
public:
    virtual ~PolyBase() = default;
    QString piLabel() const { return m_label; }
    QString piTooltip() const { return m_tooltip; }
    QStringList menuEntries() const { return m_menu_entries; }

    virtual void setCertainIndex(int index) = 0;
    virtual int certainIndex() const = 0;

protected:
    QString m_label;            //!< A label text (short, no trailing colon)
    QString m_tooltip;          //!< Tooltip text
    QStringList m_menu_entries; //!< List of options, usually presented as combo entries
};

//! Holds a polymorphous item. Possible types of the item are specified by a Catalog.
template <typename BaseItem, typename Catalog> class PolyPtrBase : public PolyBase {
public:
    void simpleInit(const QString& label, const QString& tooltip,
                    typename Catalog::Type currentType);

    BaseItem* certainItem() const { return m_item.get(); }

    void setCertainItem(BaseItem* t) { m_item.reset(t); }

    void writeTo(QXmlStreamWriter* w) const;
    template <typename... Args> void readFrom(QXmlStreamReader* r, Args... args);

    int certainIndex() const override { return m_types.indexOf(Catalog::type(m_item.get())); }

protected:
    std::unique_ptr<BaseItem> m_item; //!< Current selection

    QVector<typename Catalog::Type> m_types = Catalog::types();
};

//! Holds a polymorphous item that can be created without extra parameters.
template <typename BaseItem, typename Catalog>
class PolyPtr : public PolyPtrBase<BaseItem, Catalog> {
public:
    void setCertainIndex(int index) override
    {
        this->m_item.reset(Catalog::create(this->m_types[index]));
    }
};

//! Holds a polymorphous item that can only be created with an extra parameter.
template <typename BaseItem, typename Catalog, typename ContextData>
class PolyPtrWithContext : public PolyPtrBase<BaseItem, Catalog> {
public:
    PolyPtrWithContext(const ContextData* contextData)
        : m_contextData(contextData)
    {
    }
    void setCertainIndex(int index) override
    {
        this->m_item.reset(Catalog::create(this->m_types[index], m_contextData));
    }

private:
    const ContextData* m_contextData;
};

//! Initialize by means of a catalog class and optional creation arguments.
//!
//! The current selection will be initialized with the first type in the catalog types. The optional
//! arguments are the arguments which may be necessary for the creation method in the catalog.
template <typename BaseItem, typename Catalog>
void PolyPtrBase<BaseItem, Catalog>::simpleInit(const QString& label, const QString& tooltip,
                                                typename Catalog::Type currentType)
{
    m_label = label;
    m_tooltip = tooltip;

    m_menu_entries.clear();
    for (const auto type : m_types)
        m_menu_entries << Catalog::uiInfo(type).menuEntry;

    int index = Catalog::types().indexOf(currentType);
    setCertainIndex(index);
}

//! Serializes the catalog index of the currently selected type and calls
//! main serialization method of the selected class.
template <typename BaseItem, typename Catalog>
void PolyPtrBase<BaseItem, Catalog>::writeTo(QXmlStreamWriter* w) const
{
    const BaseItem* t = m_item.get();
    const uint typeIndex = static_cast<uint>(Catalog::type(t));
    XML::writeAttribute(w, XML::Attrib::type, typeIndex);
    // The next line allows to see the name of item type in XML. May be skipped while reading.
    XML::writeAttribute(w, XML::Attrib::kind, Catalog::uiInfo(Catalog::type(t)).menuEntry);
    if (t)
        t->writeTo(w);
}

//! Deserializes the catalog index of the currently selected type, creates a new
//! object of this type and calls main deserialization method of the selected class.
template <typename BaseItem, typename Catalog>
template <typename... Args>
void PolyPtrBase<BaseItem, Catalog>::readFrom(QXmlStreamReader* r, Args... args)
{
    const uint typeIndex = XML::readUInt(r, XML::Attrib::type);
    const QString kind = XML::readString(r, XML::Attrib::kind);
    const auto type = static_cast<typename Catalog::Type>(typeIndex);
    BaseItem* t = Catalog::create(type, args...);
    if (t)
        t->readFrom(r);
    m_item.reset(t);
}

#endif // BORNAGAIN_GUI_MODEL_DESCRIPTOR_POLYPTR_H
