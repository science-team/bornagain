//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Descriptor/VectorProperty.h
//! @brief     Defines class VectorProperty.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DESCRIPTOR_VECTORPROPERTY_H
#define BORNAGAIN_GUI_MODEL_DESCRIPTOR_VECTORPROPERTY_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include <heinz/Vectors3D.h>

//! Holds a 3D vector of type R3 as well as additional info like label, tooltip.
//!
//! Use this as a member in your class to hold a 3D vector if you want to have support for
//! serialization, and if this vector is editable on the UI.
//!
//! For a more complete documentation about property classes, please refer to DoubleProperty.
//!
//! \sa DoubleProperty
//!
class VectorProperty {
public:
    void init(const QString& label, const QString& units, const QString& tooltip,
              const QString& uidPrefix);
    void init(const QString& label, const QString& units, const QString& tooltip, const R3& value,
              uint decimals, bool useFixedStep, double step, const RealLimits& limits,
              const QString& uidPrefix);

    operator R3() const { return R3(m_x.dVal(), m_y.dVal(), m_z.dVal()); }
    bool operator==(const VectorProperty& other) const;

    void setX(double _x) { m_x.setDVal(_x); }
    void setY(double _y) { m_y.setDVal(_y); }
    void setZ(double _z) { m_z.setDVal(_z); }
    void setR3(const R3& d)
    {
        setX(d.x());
        setY(d.y());
        setZ(d.z());
    }
    R3 r3() const { return *this; }

    QString label() const;

    DoubleProperty& x() { return m_x; }
    DoubleProperty& y() { return m_y; }
    DoubleProperty& z() { return m_z; }

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

private:
    QString m_label;
    QString m_units;

    DoubleProperty m_x;
    DoubleProperty m_y;
    DoubleProperty m_z;
};

#endif // BORNAGAIN_GUI_MODEL_DESCRIPTOR_VECTORPROPERTY_H
