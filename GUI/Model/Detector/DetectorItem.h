//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Detector/DetectorItem.h
//! @brief     Defines class DetectorItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DETECTOR_DETECTORITEM_H
#define BORNAGAIN_GUI_MODEL_DETECTOR_DETECTORITEM_H

#include "GUI/Model/Descriptor/AxisProperty.h"
#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/Model/Detector/ResolutionFunctionCatalog.h"
#include "GUI/Model/Detector/ResolutionFunctionItems.h"

class Frame;
class IDetector;
class IResolutionFunction2D;
class MasksSet;

class DetectorItem {
public:
    DetectorItem();
    ~DetectorItem();

    Frame createFrame() const;
    std::unique_ptr<IDetector> createDetector() const;

    const MasksSet* masks() const { return m_masks.get(); }
    void setMasks(const MasksSet* source);

    PolyPtr<ResolutionFunctionItem, ResolutionFunctionCatalog>& resolutionFunctionSelection()
    {
        return m_resolution_function;
    }

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    AxisProperty& phiAxis() { return m_phi_axis; }
    const AxisProperty& phiAxis() const { return m_phi_axis; }

    AxisProperty& alphaAxis() { return m_alpha_axis; }
    const AxisProperty& alphaAxis() const { return m_alpha_axis; }

    bool expandResolutionFunc = true;

private:
    std::unique_ptr<IDetector> createDetectorFromAxes() const;

    std::unique_ptr<IResolutionFunction2D> createResolutionFunction() const;

    AxisProperty m_phi_axis;
    AxisProperty m_alpha_axis;

    PolyPtr<ResolutionFunctionItem, ResolutionFunctionCatalog> m_resolution_function;

    std::unique_ptr<MasksSet> m_masks;
};

#endif // BORNAGAIN_GUI_MODEL_DETECTOR_DETECTORITEM_H
