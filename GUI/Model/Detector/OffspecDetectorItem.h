//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Detector/OffspecDetectorItem.h
//! @brief     Defines class OffspecDetectorItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DETECTOR_OFFSPECDETECTORITEM_H
#define BORNAGAIN_GUI_MODEL_DETECTOR_OFFSPECDETECTORITEM_H

#include "GUI/Model/Descriptor/AxisProperty.h"

class OffspecDetector;

class OffspecDetectorItem {
public:
    OffspecDetectorItem();

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    AxisProperty& phiAxis() { return m_phi_axis; }
    const AxisProperty& phiAxis() const { return m_phi_axis; }

    AxisProperty& alphaAxis() { return m_alpha_axis; }
    const AxisProperty& alphaAxis() const { return m_alpha_axis; }

    std::unique_ptr<OffspecDetector> createOffspecDetector() const;

private:
    AxisProperty m_phi_axis;
    AxisProperty m_alpha_axis;
};

#endif // BORNAGAIN_GUI_MODEL_DETECTOR_OFFSPECDETECTORITEM_H
