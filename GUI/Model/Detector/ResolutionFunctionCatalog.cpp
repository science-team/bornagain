//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Detector/ResolutionFunctionCatalog.cpp
//! @brief     Implements class ResolutionFunctionCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Detector/ResolutionFunctionCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Detector/ResolutionFunctionItems.h"

ResolutionFunctionItem* ResolutionFunctionCatalog::create(Type type)
{
    switch (type) {
    case Type::None:
        return new ResolutionFunctionNoneItem;
    case Type::Gaussian:
        return new ResolutionFunction2DGaussianItem;
    }
    ASSERT_NEVER;
}

QVector<ResolutionFunctionCatalog::Type> ResolutionFunctionCatalog::types()
{
    return {Type::None, Type::Gaussian};
}

UiInfo ResolutionFunctionCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::None:
        return {"None", "", ""};
    case Type::Gaussian:
        return {"2D Gaussian", "", ""};
    }
    ASSERT_NEVER;
}

ResolutionFunctionCatalog::Type ResolutionFunctionCatalog::type(const ResolutionFunctionItem* item)
{
    if (dynamic_cast<const ResolutionFunctionNoneItem*>(item))
        return Type::None;
    if (dynamic_cast<const ResolutionFunction2DGaussianItem*>(item))
        return Type::Gaussian;

    ASSERT_NEVER;
}
