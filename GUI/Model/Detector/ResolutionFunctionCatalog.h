//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Detector/ResolutionFunctionCatalog.h
//! @brief     Defines class ResolutionFunctionCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DETECTOR_RESOLUTIONFUNCTIONCATALOG_H
#define BORNAGAIN_GUI_MODEL_DETECTOR_RESOLUTIONFUNCTIONCATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class ResolutionFunctionItem;

class ResolutionFunctionCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t { None = 0, Gaussian = 1 };

    //! Creates the item of the given type.
    static ResolutionFunctionItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const ResolutionFunctionItem* item);
};

#endif // BORNAGAIN_GUI_MODEL_DETECTOR_RESOLUTIONFUNCTIONCATALOG_H
