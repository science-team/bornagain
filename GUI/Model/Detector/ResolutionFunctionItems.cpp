//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Detector/ResolutionFunctionItems.cpp
//! @brief     Implements family of ResolutionFunctionItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Detector/ResolutionFunctionItems.h"
#include "Device/Resolution/ResolutionFunction2DGaussian.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString SigmaX("SigmaX");
const QString SigmaY("SigmaY");

} // namespace Tag
} // namespace

std::unique_ptr<IResolutionFunction2D> ResolutionFunctionNoneItem::createResolutionFunction() const
{
    return {};
}

ResolutionFunction2DGaussianItem::ResolutionFunction2DGaussianItem()
{
    m_sigmaX.init("Sigma X", "deg", "Resolution along horizontal axis", 0.0, 3,
                  RealLimits::lowerLimited(0.0), "sigmaX");
    m_sigmaY.init("Sigma Y", "deg", "Resolution along vertical axis", 0.0, 3,
                  RealLimits::lowerLimited(0.0), "sigmaY");
}

std::unique_ptr<IResolutionFunction2D>
ResolutionFunction2DGaussianItem::createResolutionFunction() const
{
    return std::make_unique<ResolutionFunction2DGaussian>(m_sigmaX.dVal(), m_sigmaY.dVal());
}

void ResolutionFunction2DGaussianItem::writeTo(QXmlStreamWriter* w) const
{
    m_sigmaX.writeTo2(w, Tag::SigmaX);
    m_sigmaY.writeTo2(w, Tag::SigmaY);
}

void ResolutionFunction2DGaussianItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::SigmaX)
            m_sigmaX.readFrom2(r, tag);
        else if (tag == Tag::SigmaY)
            m_sigmaY.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}
