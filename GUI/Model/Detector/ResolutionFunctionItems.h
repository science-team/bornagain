//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Detector/ResolutionFunctionItems.h
//! @brief     Defines family of ResolutionFunctionItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_DETECTOR_RESOLUTIONFUNCTIONITEMS_H
#define BORNAGAIN_GUI_MODEL_DETECTOR_RESOLUTIONFUNCTIONITEMS_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include <memory>

class IResolutionFunction2D;

class ResolutionFunctionItem {
public:
    virtual ~ResolutionFunctionItem() = default;

    virtual std::unique_ptr<IResolutionFunction2D> createResolutionFunction() const = 0;

    virtual void writeTo(QXmlStreamWriter*) const {}
    virtual void readFrom(QXmlStreamReader*) {}
};

class ResolutionFunctionNoneItem : public ResolutionFunctionItem {
public:
    std::unique_ptr<IResolutionFunction2D> createResolutionFunction() const override;
};

class ResolutionFunction2DGaussianItem : public ResolutionFunctionItem {
public:
    ResolutionFunction2DGaussianItem();
    std::unique_ptr<IResolutionFunction2D> createResolutionFunction() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    DoubleProperty& sigmaX() { return m_sigmaX; }
    const DoubleProperty& sigmaX() const { return m_sigmaX; }
    void setSigmaX(double v) { m_sigmaX.setDVal(v); }

    DoubleProperty& sigmaY() { return m_sigmaY; }
    const DoubleProperty& sigmaY() const { return m_sigmaY; }
    void setSigmaY(double v) { m_sigmaY.setDVal(v); }

protected:
    DoubleProperty m_sigmaX;
    DoubleProperty m_sigmaY;
};

#endif // BORNAGAIN_GUI_MODEL_DETECTOR_RESOLUTIONFUNCTIONITEMS_H
