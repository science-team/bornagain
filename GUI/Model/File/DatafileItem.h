//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/File/DatafileItem.h
//! @brief     Defines class DatafileItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_FILE_DATAFILEITEM_H
#define BORNAGAIN_GUI_MODEL_FILE_DATAFILEITEM_H

#include "GUI/Model/Type/NamedItem.h"
#include <QObject>
#include <QXmlStreamReader>

class Data1DItem;
class Data2DItem;
class DataItem;
class Datafield;

//! Provides access to experimental data, for display and fitting.

class DatafileItem : public QObject, public NamedItem {
public:
    DatafileItem();
    DatafileItem(const QString& name, const Datafield& df);
    ~DatafileItem();

    DatafileItem* clone() const;

    //! The name which is presented to the user
    void setDatafileItemName(const QString& name);

    //... Data

    DataItem* dataItem() const;
    Data2DItem* data2DItem() const;
    Data1DItem* data1DItem() const;

    size_t rank() const;
    size_t axdim(int i) const;

    bool holdsDimensionalData() const;

    void rotateData();

    //... Write/read

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    void saveDatafield(const QString& projectDir) const;
    void loadDatafield(const QString& projectDir);

private:
    void updateFileName();

    std::unique_ptr<DataItem> m_data_item; //!< Can be 1D or 2D item.
};

#endif // BORNAGAIN_GUI_MODEL_FILE_DATAFILEITEM_H
