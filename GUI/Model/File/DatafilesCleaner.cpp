//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/File/DatafilesCleaner.cpp
//! @brief     Implements class DatafilesCleaner.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/File/DatafilesCleaner.h"
#include "GUI/Model/Data/DataItem.h"
#include <QFile>

DatafilesCleaner::DatafilesCleaner() = default;

void DatafilesCleaner::recollectDataNames(const QVector<DataItem*>& dataItems)
{
    m_fnames.clear();
    for (DataItem* item : dataItems)
        m_fnames << item->fname();
}

void DatafilesCleaner::cleanOldFiles(const QString& projectDir, const QVector<DataItem*>& dataItems)
{
    QStringList oldSaves = m_fnames;
    recollectDataNames(dataItems);

    for (const QString& name : oldSaves)
        if (!m_fnames.contains(name))
            QFile::remove(projectDir + "/" + name);
}
