//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/File/DatafilesCleaner.h
//! @brief     Defines class DatafilesCleaner.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_FILE_DATAFILESCLEANER_H
#define BORNAGAIN_GUI_MODEL_FILE_DATAFILESCLEANER_H

#include <QVector>

class DataItem;

//! Checks if data files in the project folder are no longer in use and deletes them.

class DatafilesCleaner {
public:
    DatafilesCleaner();

    //! Updates list of data filenames
    void recollectDataNames(const QVector<DataItem*>& dataItems);

    //! Deletes datafiles that were previously used but no longer relevant
    void cleanOldFiles(const QString& projectDir, const QVector<DataItem*>& dataItems);

private:
    QStringList m_fnames;
};

#endif // BORNAGAIN_GUI_MODEL_FILE_DATAFILESCLEANER_H
