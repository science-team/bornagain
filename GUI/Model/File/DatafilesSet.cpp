//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/File/DatafilesSet.cpp
//! @brief     Implements class DatafilesSet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/File/DatafilesSet.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString RealData("RealData");
const QString CurrentIndex("CurrentIndex");

} // namespace Tag
} // namespace

DatafilesSet::DatafilesSet() = default;
DatafilesSet::~DatafilesSet() = default;

void DatafilesSet::writeTo(QXmlStreamWriter* w) const
{
    // real items
    for (auto* dfi : *this) {
        w->writeStartElement(Tag::RealData);
        XML::writeAttribute(w, XML::Attrib::name, dfi->name());
        dfi->writeTo(w);
        w->writeEndElement();
    }
    XML::writeTaggedValue(w, Tag::CurrentIndex, (int)currentIndex());
}

void DatafilesSet::readFrom(QXmlStreamReader* r)
{
    clear();
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::RealData) {
            auto* item = new DatafileItem;
            item->readFrom(r);
            add_item(item);
            XML::gotoEndElementOfTag(r, tag);
        } else if (tag == Tag::CurrentIndex)
            setCurrentIndex(XML::readTaggedInt(r, tag));
        else
            r->skipCurrentElement();
    }

    if (r->hasError())
        throw std::runtime_error(r->errorString().toStdString());
}

void DatafilesSet::writeDatafiles(const QString& projectDir) const
{
    for (const auto* dfi : *this)
        dfi->saveDatafield(projectDir);

    dataFilesCleaner.cleanOldFiles(projectDir, dataItems());
}

void DatafilesSet::readDatafiles(const QString& projectDir)
{
    for (auto* dfi : *this)
        dfi->loadDatafield(projectDir);

    dataFilesCleaner.recollectDataNames(dataItems());
}

QVector<DataItem*> DatafilesSet::dataItems() const
{
    QVector<DataItem*> result;
    for (auto* dfi : *this)
        if (auto* data_item = dfi->dataItem())
            result.push_back(data_item);
    return result;
}
