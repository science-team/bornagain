//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/File/DatafilesSet.h
//! @brief     Defines class DatafilesSet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_FILE_DATAFILESSET_H
#define BORNAGAIN_GUI_MODEL_FILE_DATAFILESSET_H

#include "GUI/Model/File/DatafileItem.h"
#include "GUI/Model/File/DatafilesCleaner.h"
#include "GUI/Model/Type/SetWithModel.h"

class DataItem;

//! The DatafilesSet class is a model to store all imported DatafileItem's.
class DatafilesSet : public SetWithModel<DatafileItem> {
public:
    DatafilesSet();
    virtual ~DatafilesSet();

    void readFrom(QXmlStreamReader* r);
    void readDatafiles(const QString& projectDir);

    void writeTo(QXmlStreamWriter* w) const;
    void writeDatafiles(const QString& projectDir) const;

private:
    QVector<DataItem*> dataItems() const;

    mutable DatafilesCleaner dataFilesCleaner;
};

#endif // BORNAGAIN_GUI_MODEL_FILE_DATAFILESSET_H
