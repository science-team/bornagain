//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/FromCore/GUIExamplesFactory.cpp
//! @brief     Implements class GUI::ExamplesFactory.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/FromCore/GUIExamplesFactory.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/FromCore/ItemizeSample.h"
#include "GUI/Model/Sample/SampleItem.h"
#include "Sample/StandardSample/ExemplarySamples.h"
#include <QMap>

namespace {

const QMap<QString, std::tuple<QString, QString, std::function<Sample*()>>> builders{
    {"CylindersAndPrismsBuilder",
     {"Cylinder and prisms", "Mixture of cylinders and prisms without interference",
      ExemplarySamples::createCylindersAndPrisms}},

    {"RadialParacrystalBuilder",
     {"Interference 1D paracrystal", "Interference 1D radial paracrystal",
      ExemplarySamples::createRadialParacrystal}},

    {"HexParacrystalBuilder",
     {"Interference 2D paracrystal", "Interference 2D paracrystal",
      ExemplarySamples::createHexParacrystal}},

    {"CoreShellParticleBuilder",
     {"Core shell particles", "Core shell particles", ExemplarySamples::createCoreShellParticle}},

    {"MultiLayerWithRoughnessBuilder",
     {"Multilayer with correlated roughness", "Multilayer with correlated roughness",
      ExemplarySamples::createMultiLayerWithRoughness}},

    {"SquareLattice2DBuilder",
     {"Interference 2D square lattice", "Interference 2D lattice",
      ExemplarySamples::createSquareLattice2D}},

    {"RotatedPyramidsBuilder",
     {"Rotated pyramids", "Rotated pyramids on top of substrate",
      ExemplarySamples::createRotatedPyramids}},

    {"CompoundBuilder",
     {"Hexagonal lattice with basis",
      "Hexagonal lattice with basis to represent two layers of spheres",
      ExemplarySamples::createCompound}},

    {"MesocrystalBuilder", {"Mesocrystal", "", ExemplarySamples::createMesocrystal}}};

} // namespace


bool GUI::ExamplesFactory::isValidExampleName(const QString& name)
{
    return builders.find(name) != builders.end();
}

SampleItem* GUI::ExamplesFactory::itemizeSample(const QString& name)
{
    ASSERT(isValidExampleName(name));
    auto [title, description, builder] = builders[name];
    Sample* sample = builder();
    ASSERT(sample);
    SampleItem* result = GUI::FromCore::itemizeSample(*sample, name);
    result->setName(title);
    result->setDescription(description);
    return result;
}

QStringList GUI::ExamplesFactory::exampleNames()
{
    return builders.keys();
}

std::tuple<QString, QString> GUI::ExamplesFactory::exampleInfo(const QString& name)
{
    ASSERT(isValidExampleName(name));
    auto [title, description, builder] = builders[name];
    return {title, description};
}
