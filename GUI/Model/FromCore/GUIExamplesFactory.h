//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/FromCore/GUIExamplesFactory.h
//! @brief     Defines class GUI::ExamplesFactory.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_FROMCORE_GUIEXAMPLESFACTORY_H
#define BORNAGAIN_GUI_MODEL_FROMCORE_GUIEXAMPLESFACTORY_H

#include <QString>

class SampleItem;

//! Class that generates GUI model from
namespace GUI::ExamplesFactory {

bool isValidExampleName(const QString& name);

//! Create a sample item of the built-in example with the given internal name.
SampleItem* itemizeSample(const QString& name);

//! The internal example name, e.g. for creation with itemizeSample.
QStringList exampleNames();

//! Returns human readable name and description
std::tuple<QString, QString> exampleInfo(const QString& name);

} // namespace GUI::ExamplesFactory

#endif // BORNAGAIN_GUI_MODEL_FROMCORE_GUIEXAMPLESFACTORY_H
