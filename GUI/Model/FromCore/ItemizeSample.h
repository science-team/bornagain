//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/FromCore/ItemizeSample.h
//! @brief     Defines class GUISampleBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_FROMCORE_ITEMIZESAMPLE_H
#define BORNAGAIN_GUI_MODEL_FROMCORE_ITEMIZESAMPLE_H

#include <QString>

class Sample;
class SampleItem;

namespace GUI::FromCore {

//! Builds GUI sample structure from a domain sample structure.
SampleItem* itemizeSample(const Sample& sample, const QString& nodeName = "");

} // namespace GUI::FromCore

#endif // BORNAGAIN_GUI_MODEL_FROMCORE_ITEMIZESAMPLE_H
