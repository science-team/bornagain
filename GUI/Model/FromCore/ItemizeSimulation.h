//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/FromCore/ItemizeSimulation.h
//! @brief     Declares namespace GUI::FromCore.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_FROMCORE_ITEMIZESIMULATION_H
#define BORNAGAIN_GUI_MODEL_FROMCORE_ITEMIZESIMULATION_H

class ISimulation;
class InstrumentItem;
class SimulationOptionsItem;

//! Contains set of methods to populate GUI models with content from domain.

namespace GUI::FromCore {

InstrumentItem* itemizeInstrument(const ISimulation& simulation);

SimulationOptionsItem* itemizeOptions(const ISimulation& simulation);

} // namespace GUI::FromCore

#endif // BORNAGAIN_GUI_MODEL_FROMCORE_ITEMIZESIMULATION_H
