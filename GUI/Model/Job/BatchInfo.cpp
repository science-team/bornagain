//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Job/BatchInfo.cpp
//! @brief     Implements class BatchInfo.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Job/BatchInfo.h"
#include "GUI/Model/Job/JobStatus.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString BeginTime("Begin");
const QString Comments("Comments");
const QString Duration("Duration");
const QString EndTime("End");
const QString Name("Name");
const QString Progress("Progress");
const QString Status("Status");

} // namespace Tag
} // namespace

BatchInfo::BatchInfo()
    : m_status(JobStatus::Idle)
{
}

void BatchInfo::setJobName(const QString& name)
{
    m_name = name;
    emit jobNameChanged(name);
}

void BatchInfo::setBeginTime(const QDateTime& begin_time)
{
    m_begin_time = begin_time;
    emit jobBeginTimeChanged(begin_time);
}

void BatchInfo::setEndTime(const QDateTime& end_time)
{
    m_end_time = end_time;
    emit jobEndTimeChanged(end_time);
}

std::optional<size_t> BatchInfo::duration() const
{
    QDateTime begin_time = beginTime();
    QDateTime end_time = endTime();
    if (begin_time.isValid() && end_time.isValid() && begin_time < end_time)
        return begin_time.msecsTo(end_time);
    return std::nullopt;
}

void BatchInfo::setComments(const QString& comments)
{
    m_comments = comments;
    emit jobCommentsChanged(comments);
}

void BatchInfo::setProgress(int progress)
{
    m_progress = progress;
    emit jobProgressChanged(progress);
}

void BatchInfo::setStatus(JobStatus status)
{
    m_status = status;
    emit jobStatusChanged(status);
}

void BatchInfo::writeTo(QXmlStreamWriter* w) const
{
    XML::writeTaggedValue(w, Tag::Name, m_name);
    XML::writeTaggedValue(w, Tag::BeginTime, m_begin_time.toString(Qt::ISODateWithMs));
    XML::writeTaggedValue(w, Tag::EndTime, m_end_time.toString(Qt::ISODateWithMs));
    XML::writeTaggedValue(w, Tag::Progress, m_progress);
    XML::writeTaggedValue(w, Tag::Comments, m_comments);
    XML::writeTaggedValue(w, Tag::Status, jobStatusToString(m_status));
}

void BatchInfo::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Comments)
            m_comments = XML::readTaggedString(r, tag);
        else if (tag == Tag::Progress)
            m_progress = XML::readTaggedString(r, tag).toInt();
        else if (tag == Tag::BeginTime) {
            QString s = XML::readTaggedString(r, tag);
            m_begin_time = QDateTime::fromString(s, Qt::ISODateWithMs);
        } else if (tag == Tag::EndTime) {
            QString s = XML::readTaggedString(r, tag);
            m_end_time = QDateTime::fromString(s, Qt::ISODateWithMs);
        } else if (tag == Tag::Name)
            m_name = XML::readTaggedString(r, tag);
        else if (tag == Tag::Status) {
            QString s = XML::readTaggedString(r, tag);
            m_status = jobStatusFromString(s);
        } else
            r->skipCurrentElement();
    }
}
