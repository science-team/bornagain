//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Job/BatchInfo.h
//! @brief     Defines class BatchInfo.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_JOB_BATCHINFO_H
#define BORNAGAIN_GUI_MODEL_JOB_BATCHINFO_H

#include <QDateTime>
#include <QObject>
#include <QXmlStreamReader>

enum class JobStatus;

class BatchInfo : public QObject {
    Q_OBJECT
public:
    BatchInfo();

    QString jobName() const { return m_name; }
    void setJobName(const QString& name);

    QDateTime beginTime() const { return m_begin_time; }
    void setBeginTime(const QDateTime& begin_time);

    QDateTime endTime() const { return m_end_time; }
    void setEndTime(const QDateTime& end_time);

    //! if begin and end time are both available the duration in ms, otherwise empty
    std::optional<size_t> duration() const;

    QString comments() const { return m_comments; }
    void setComments(const QString& comments);

    int progress() const { return m_progress; }
    void setProgress(int progress);

    JobStatus status() const { return m_status; }
    void setStatus(JobStatus status);

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

signals:
    void jobNameChanged(const QString& name);
    void jobBeginTimeChanged(const QDateTime& begin_time);
    void jobEndTimeChanged(const QDateTime& end_time);
    void jobCommentsChanged(const QString& comments);
    void jobProgressChanged(int progress);
    void jobStatusChanged(JobStatus status);

private:
    QString m_name;
    QString m_comments;
    QDateTime m_begin_time;
    QDateTime m_end_time;
    int m_progress;
    JobStatus m_status;
};

#endif // BORNAGAIN_GUI_MODEL_JOB_BATCHINFO_H
