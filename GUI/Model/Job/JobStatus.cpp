//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Job/JobStatus.cpp
//! @brief     Implements global functions that operate on class JobStatus.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Job/JobStatus.h"
#include "Base/Util/Assert.h"
#include <boost/bimap.hpp>
#include <vector>

namespace {

const std::vector<boost::bimap<JobStatus, QString>::value_type> values = {
    {JobStatus::Idle, "Idle"},         {JobStatus::Running, "Running"},
    {JobStatus::Fitting, "Fitting"},   {JobStatus::Completed, "Completed"},
    {JobStatus::Canceled, "Canceled"}, {JobStatus::Failed, "Failed"}};

const boost::bimap<JobStatus, QString> status2name(values.begin(), values.end());

} // namespace

bool isActive(JobStatus status)
{
    return isRunning(status) || isFitting(status);
}

bool isOver(JobStatus status)
{
    return isCompleted(status) || isCanceled(status) || isFailed(status);
}

bool isRunning(JobStatus status)
{
    return status == JobStatus::Running;
}

bool isCompleted(JobStatus status)
{
    return status == JobStatus::Completed;
}

bool isCanceled(JobStatus status)
{
    return status == JobStatus::Canceled;
}

bool isFailed(JobStatus status)
{
    return status == JobStatus::Failed;
}

bool isFitting(JobStatus status)
{
    return status == JobStatus::Fitting;
}

QString jobStatusToString(JobStatus status)
{
    auto it = status2name.left.find(status);
    ASSERT(it != status2name.left.end());
    return it->second;
}

JobStatus jobStatusFromString(const QString& name)
{
    auto it = status2name.right.find(name);
    ASSERT(it != status2name.right.end());
    return it->second;
}
