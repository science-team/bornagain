//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Job/JobStatus.h
//! @brief     Defines enum JobStatus and global functions that operate on class JobStatus.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_JOB_JOBSTATUS_H
#define BORNAGAIN_GUI_MODEL_JOB_JOBSTATUS_H

#include <QString>

//! The JobStatus enum lists the possible states of a job

enum class JobStatus {
    Idle,      //!< the job has not been started yet
    Running,   //!< the job is busy calculating
    Fitting,   //!< the job is busy fitting
    Completed, //!< the job was successfully completed
    Canceled,  //!< the job was stopped by the user
    Failed     //!< the job aborted because it hit an error
};

bool isActive(JobStatus status);
bool isCanceled(JobStatus status);
bool isCompleted(JobStatus status);
bool isFailed(JobStatus status);
bool isFitting(JobStatus status);
bool isRunning(JobStatus status);
bool isOver(JobStatus status);

//! Returns a string representation of the status
QString jobStatusToString(JobStatus status);

//! Returns status value for given string representation
JobStatus jobStatusFromString(const QString& name);

#endif // BORNAGAIN_GUI_MODEL_JOB_JOBSTATUS_H
