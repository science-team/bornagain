//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Job/JobWorker.cpp
//! @brief     Implements class JobWorker.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Job/JobWorker.h"
#include "Base/Util/Assert.h"
#include "Device/Data/Datafield.h"
#include "GUI/Model/Job/JobStatus.h"
#include "Sim/Simulation/ScatteringSimulation.h"
#include <utility>

JobWorker::JobWorker(ISimulation* simulation)
    : m_simulation(simulation)
    , m_percentage_done(0)
    , m_job_status(JobStatus::Idle)
    , m_terminate_request_flag(false)
{
}

JobWorker::~JobWorker() = default;

void JobWorker::start()
{
    m_terminate_request_flag = false;
    m_simulation_start = QDateTime::currentDateTime();
    m_simulation_end = QDateTime();
    m_result.reset();
    emit started();

    try {
        m_job_status = JobStatus::Running;
        ASSERT(m_simulation);
        m_simulation->subscribe([this](size_t percentage_done) {
            return updateProgress(static_cast<int>(percentage_done));
        });
        Datafield result = m_simulation->simulate();
        if (m_job_status != JobStatus::Canceled)
            m_job_status = JobStatus::Completed;
        m_result = std::make_unique<Datafield>(result);
    } catch (const std::exception& ex) {
        m_job_status = JobStatus::Failed;
        m_percentage_done = 100;
        m_failure_message = ex.what();
    }

    m_simulation_end = QDateTime::currentDateTime();
    emit progressUpdate();
    emit finished();
}

//! Sets request for JobRunner to terminate underlying domain simulation.

void JobWorker::terminate()
{
    m_terminate_request_flag = true;
    m_job_status = JobStatus::Canceled;
}

//! Sets current progress. Returns true if we want to continue the simulation.

bool JobWorker::updateProgress(int percentage_done)
{
    if (percentage_done > m_percentage_done) {
        m_percentage_done = percentage_done;
        emit progressUpdate();
    }
    return !m_terminate_request_flag;
}
