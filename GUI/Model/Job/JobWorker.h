//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Job/JobWorker.h
//! @brief     Defines class JobWorker.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_JOB_JOBWORKER_H
#define BORNAGAIN_GUI_MODEL_JOB_JOBWORKER_H

#include <QDateTime>
#include <QObject>
#include <memory>

enum class JobStatus;

class Datafield;
class ISimulation;

//! The JobWorker class provides running the domain simulation in a thread.

class JobWorker : public QObject {
    Q_OBJECT
public:
    JobWorker(ISimulation* simulation);
    ~JobWorker();

    int percentageDone() const { return m_percentage_done; }

    JobStatus workerStatus() const { return m_job_status; }

    QString workerFailureMessage() const { return m_failure_message; }

    const QDateTime& simulationStart() const { return m_simulation_start; }
    const QDateTime& simulationEnd() const { return m_simulation_end; }

    const Datafield* workerResult() const { return m_result.get(); }

signals:
    void started();
    void finished();
    void progressUpdate();

public slots:
    void start();
    void terminate();

private:
    bool updateProgress(int percentage_done);

    std::unique_ptr<ISimulation> m_simulation;
    int m_percentage_done;
    JobStatus m_job_status;
    bool m_terminate_request_flag;
    QString m_failure_message;
    QDateTime m_simulation_start;
    QDateTime m_simulation_end;
    std::unique_ptr<const Datafield> m_result;
};

#endif // BORNAGAIN_GUI_MODEL_JOB_JOBWORKER_H
