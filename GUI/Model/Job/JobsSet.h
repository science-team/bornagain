//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Job/JobsSet.h
//! @brief     Defines class JobsSet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_JOB_JOBSSET_H
#define BORNAGAIN_GUI_MODEL_JOB_JOBSSET_H

#include "Base/Type/VectorWC.h"
#include "GUI/Model/File/DatafilesCleaner.h"
#include "GUI/Model/Job/JobItem.h"
#include <QObject>
#include <QXmlStreamReader>

class DataItem;
class ISimulation;

class JobsSet : public QObject, public VectorWC<JobItem> {
    Q_OBJECT
public:
    explicit JobsSet(QObject* parent = nullptr);
    ~JobsSet() override;

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    void saveAllDatafields(const QString& projectDir) const;
    void loadAllDatafields(const QString& projectDir);

    JobItem* createJobItem();
    void addJobItem(JobItem* job_item);

    void restoreBackupPars(JobItem* job_item, int index);

    bool hasUnfinishedJobs() const;

    QVector<DataItem*> dataItems() const;

    void runJob(JobItem* job_item);
    void cancelJob(JobItem* job_item);
    void removeJob(JobItem* job_item);

signals:
    void newJobFinished(JobItem* item);
    void jobPlotContextChanged() const;
    void globalProgress(int);
    void jobAdded(JobItem* job_item);

private slots:
    void onFinishedJob(JobItem* job_item);
    void onProgressUpdate();
    void onCancelAllJobs();

private:
    QString generateJobName() const;

    mutable DatafilesCleaner dataFilesCleaner;
};

#endif // BORNAGAIN_GUI_MODEL_JOB_JOBSSET_H
