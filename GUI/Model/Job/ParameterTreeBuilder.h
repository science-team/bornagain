//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Job/ParameterTreeBuilder.h
//! @brief     Defines class ParameterTreeBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_JOB_PARAMETERTREEBUILDER_H
#define BORNAGAIN_GUI_MODEL_JOB_PARAMETERTREEBUILDER_H

#include <QString>

class BackgroundItem;
class BeamDistributionItem;
class DetectorItem;
class DoubleProperty;
class FootprintItem;
class InstrumentItem;
class Interference2DAbstractLatticeItem;
class ItemWithParticles;
class JobItem;
class OffspecDetectorItem;
class ParameterContainerItem;
class ParameterItem;
class ParameterLabelItem;
class ParticleLayoutItem;
class ScanItem;
class VectorProperty;

//! The ParameterTreeBuilder contains helper functions to create container
//! with ParameterItems. The ParameterItem appears in RealTimeView and provides real
//! time tuning of SampleItem and InstrumentItem.

class ParameterTreeBuilder {
public:
    ParameterTreeBuilder(JobItem* jobItem);

    void build();

private:
    //! add the job's materials
    void addMaterials();

    //! add the job's sample
    void addSample();
    void addInstrument();
    void addParameterItem(ParameterLabelItem* parent, DoubleProperty& d, const QString& label = "");
    void addParameterItem(ParameterLabelItem* parent, VectorProperty& d);
    void addMagnetizationNoZ(ParameterLabelItem* parent, VectorProperty& d);
    ParameterContainerItem* parameterContainerItem();
    bool allowMagneticFields() const;

    void addInterference(ParameterLabelItem* layoutLabel, const ParticleLayoutItem* layout);

    //! Returns the top label which was created for the particle
    ParameterLabelItem* addItemWithParticles(ParameterLabelItem* parentLabel, ItemWithParticles* p,
                                             bool enableAbundance);
    void addLattice(ParameterLabelItem* parentLabel, const Interference2DAbstractLatticeItem* itf);
    void addRotation(ParameterLabelItem* parentLabel, ItemWithParticles* p);

    void addBeamDistribution(ParameterLabelItem* parentLabel,
                             BeamDistributionItem* distributionItem, const QString& label,
                             bool withMean = true);

    void addScanDistributions(ParameterLabelItem* parent, ScanItem* scanItem);

    void addDetector(ParameterLabelItem* parentLabel, DetectorItem* detector);
    void addOffspecDetector(ParameterLabelItem* parentLabel, OffspecDetectorItem* detector);
    void addBackground(ParameterLabelItem* instrumentLabel, BackgroundItem* backgroundItem);
    void addFootprint(ParameterLabelItem* instrumentLabel, FootprintItem* footprintItem);
    void addScanFootprint(ParameterLabelItem* instrumentLabel, ScanItem* scanItem);
    void addPolarization(ParameterLabelItem* instrumentLabel, InstrumentItem* instrument);

private:
    JobItem* m_job_item;
};

#endif // BORNAGAIN_GUI_MODEL_JOB_PARAMETERTREEBUILDER_H
