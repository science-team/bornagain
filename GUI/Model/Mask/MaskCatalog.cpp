//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Mask/MaskCatalog.cpp
//! @brief     Implements class MaskCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Mask/MaskCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Mask/MaskItems.h"

MaskItem* MaskCatalog::create(Type type)
{
    switch (type) {
    case Type::RegionOfInterest:
        return new RegionOfInterestItem;
    case Type::Rectangle:
        return new RectangleItem;
    case Type::Polygon:
        return new PolygonItem;
    case Type::VerticalLine:
        return new VerticalLineItem(0.);
    case Type::HorizontalLine:
        return new HorizontalLineItem(0.);
    case Type::MaskAll:
        return new FullframeItem;
    case Type::Ellipse:
        return new EllipseItem;
    }
    ASSERT_NEVER;
}

QVector<MaskCatalog::Type> MaskCatalog::types()
{
    return {Type::RegionOfInterest, Type::Rectangle, Type::Polygon, Type::VerticalLine,
            Type::HorizontalLine,   Type::MaskAll,   Type::Ellipse};
}

UiInfo MaskCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::RegionOfInterest:
        return {"Region of interest", "", ""};
    case Type::Rectangle:
        return {"Rectangle", "", ""};
    case Type::Polygon:
        return {"Polygon", "", ""};
    case Type::VerticalLine:
        return {"Vertical line", "", ""};
    case Type::HorizontalLine:
        return {"Horizontal line", "", ""};
    case Type::MaskAll:
        return {"Mask all", "", ""};
    case Type::Ellipse:
        return {"Ellipse", "", ""};
    }
    ASSERT_NEVER;
}

MaskCatalog::Type MaskCatalog::type(const MaskItem* item)
{
    if (dynamic_cast<const RegionOfInterestItem*>(item)) // has to be before test for Rectangle!
        return Type::RegionOfInterest;
    if (dynamic_cast<const RectangleItem*>(item))
        return Type::Rectangle;
    if (dynamic_cast<const PolygonItem*>(item))
        return Type::Polygon;
    if (dynamic_cast<const VerticalLineItem*>(item))
        return Type::VerticalLine;
    if (dynamic_cast<const HorizontalLineItem*>(item))
        return Type::HorizontalLine;
    if (dynamic_cast<const FullframeItem*>(item))
        return Type::MaskAll;
    if (dynamic_cast<const EllipseItem*>(item))
        return Type::Ellipse;

    ASSERT_NEVER;
}
