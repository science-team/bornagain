//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Mask/MaskCatalog.h
//! @brief     Defines class MaskCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_MASK_MASKCATALOG_H
#define BORNAGAIN_GUI_MODEL_MASK_MASKCATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class MaskItem;

class MaskCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t {
        RegionOfInterest = 0,
        Rectangle = 1,
        Polygon = 2,
        VerticalLine = 3,
        HorizontalLine = 4,
        MaskAll = 5,
        Ellipse = 6
    };

    //! Creates the item of the given type.
    static MaskItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type type);

    //! Returns the enum type of the given item.
    static Type type(const MaskItem* item);
};

#endif // BORNAGAIN_GUI_MODEL_MASK_MASKCATALOG_H
