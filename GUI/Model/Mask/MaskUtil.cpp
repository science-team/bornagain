//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Mask/MaskUtil.cpp
//! @brief     Implements namespace MaskUtil
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Mask/MaskUtil.h"
#include "Base/Axis/Frame.h"
#include "Base/Axis/Scale.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Mask/MaskItems.h"
#include "GUI/Model/Mask/MasksSet.h"
#include "GUI/Model/Mask/PointItem.h"

namespace {

// Converts single position on axis to the coordinates on a new axis
double convert(double value, const Scale& old_axis, const Scale& new_axis)
{
    ASSERT(old_axis.size() >= 2 && new_axis.size() >= 2);

    double old_1 = old_axis.binCenters().front();
    double old_2 = old_axis.binCenters().back();
    ASSERT(old_1 != old_2);

    double new_1 = new_axis.binCenters().front();
    double new_2 = new_axis.binCenters().back();

    return (value - old_1) / (old_1 - old_2) * (new_1 - new_2) + new_1;
}

} // namespace

void MaskUtil::convertMasks(const MasksSet* masks, const Frame& oldFrame, const Frame& newFrame)
{
    ASSERT(masks);

    const Scale& old_X = oldFrame.xAxis();
    const Scale& old_Y = oldFrame.yAxis();
    const Scale& new_X = newFrame.xAxis();
    const Scale& new_Y = newFrame.yAxis();

    for (auto* maskItem : *masks) {
        if (auto* rectItem = dynamic_cast<RectangleItem*>(maskItem)) {
            rectItem->setXLow(::convert(rectItem->xLow().dVal(), old_X, new_X));
            rectItem->setYLow(::convert(rectItem->yLow().dVal(), old_Y, new_Y));
            rectItem->setXHig(::convert(rectItem->xUp().dVal(), old_X, new_X));
            rectItem->setYHig(::convert(rectItem->yUp().dVal(), old_Y, new_Y));
        } else if (auto* poly = dynamic_cast<PolygonItem*>(maskItem)) {
            for (PointItem* pointItem : poly->points()) {
                pointItem->setPosX(::convert(pointItem->posX().dVal(), old_X, new_X));
                pointItem->setPosY(::convert(pointItem->posY().dVal(), old_Y, new_Y));
            }
        } else if (auto* vlineItem = dynamic_cast<VerticalLineItem*>(maskItem)) {
            vlineItem->setPos(::convert(vlineItem->pos().dVal(), old_X, new_X));
        } else if (auto* hlineItem = dynamic_cast<HorizontalLineItem*>(maskItem)) {
            hlineItem->setPos(::convert(hlineItem->pos().dVal(), old_Y, new_Y));
        } else if (auto* ellItem = dynamic_cast<EllipseItem*>(maskItem)) {
            double xc_old = ellItem->xCenter().dVal();
            double yc_old = ellItem->yCenter().dVal();
            double xR_old = ellItem->xRadius().dVal();
            double yR_old = ellItem->yRadius().dVal();
            double x2_old = xc_old + xR_old;
            double y2_old = yc_old + yR_old;

            double xc_new = ::convert(xc_old, old_X, new_X);
            double yc_new = ::convert(yc_old, old_Y, new_Y);
            double x2_new = ::convert(x2_old, old_X, new_X);
            double y2_new = ::convert(y2_old, old_Y, new_Y);
            double xR_new = x2_new - xc_new;
            double yR_new = y2_new - yc_new;

            ellItem->setXCenter(xc_new);
            ellItem->setYCenter(yc_new);
            ellItem->setXRadius(xR_new);
            ellItem->setYRadius(yR_new);
        }
    }
}
