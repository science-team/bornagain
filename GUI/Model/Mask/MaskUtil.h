//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Mask/MaskUtil.h
//! @brief     Defines namespace MaskUtil
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_MASK_MASKUTIL_H
#define BORNAGAIN_GUI_MODEL_MASK_MASKUTIL_H

class Frame;
class MasksSet;

namespace MaskUtil {

//! Transforms coordinates of all masks from one Frame to another.
//! I.e. masks were created for the 'real' datafield between phi in [-1...1] deg. But after that
//! the datafield intensity was reframed to another axes and the phi range is now in [0...5] deg.
//! Masks should get the new coordinates that correspond the same pixels.
void convertMasks(const MasksSet* masks, const Frame& oldFrame, const Frame& newFrame);

} // namespace MaskUtil

#endif // BORNAGAIN_GUI_MODEL_MASK_MASKUTIL_H
