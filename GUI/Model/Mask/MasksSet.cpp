//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Mask/MasksSet.cpp
//! @brief     Implements class MasksSet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Mask/MasksSet.h"
#include "GUI/Model/Mask/MaskCatalog.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString Mask("Mask");

} // namespace Tag
} // namespace


MasksSet::MasksSet() = default;
MasksSet::~MasksSet() = default;

RegionOfInterestItem* MasksSet::regionOfInterestItem() const
{
    for (auto* t : *this)
        if (auto* roi = dynamic_cast<RegionOfInterestItem*>(t))
            return roi;

    return nullptr;
}

void MasksSet::writeTo(QXmlStreamWriter* w) const
{
    for (const MaskItem* t : *this)
        XML::writeChosen<MaskItem, MaskCatalog>(t, w, Tag::Mask);
}

void MasksSet::readFrom(QXmlStreamReader* r)
{
    clear();
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::Mask)
            add_item(XML::readChosen<MaskItem, MaskCatalog>(r, tag));
        else
            r->skipCurrentElement();
    }
    // since the current index is not serialized, the default state of the Set is then "unselected"
    setCurrentIndex(-1);
}
