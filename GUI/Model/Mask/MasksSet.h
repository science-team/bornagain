//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Mask/MasksSet.h
//! @brief     Defines class MasksSet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_MASK_MASKSSET_H
#define BORNAGAIN_GUI_MODEL_MASK_MASKSSET_H

#include "GUI/Model/Mask/MaskItems.h"
#include "GUI/Model/Type/SetWithModel.h"
#include <QXmlStreamReader>

class MaskItem;
class RegionOfInterestItem;

//! Container holding various masks as children

class MasksSet : public SetWithModel<MaskItem> {
    Q_OBJECT
public:
    MasksSet();
    virtual ~MasksSet();

    //! Can be nullptr.
    RegionOfInterestItem* regionOfInterestItem() const;

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);
};

#endif // BORNAGAIN_GUI_MODEL_MASK_MASKSSET_H
