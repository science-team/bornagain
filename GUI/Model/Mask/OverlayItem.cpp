//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Mask/OverlayItem.cpp
//! @brief     Implements class OverlayItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Mask/OverlayItem.h"

OverlayItem::OverlayItem() = default;

OverlayItem::~OverlayItem()
{
    emit maskToBeDestroyed();
}
