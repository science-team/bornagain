//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Mask/OverlayItem.h
//! @brief     Defines class OverlayItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_MASK_OVERLAYITEM_H
#define BORNAGAIN_GUI_MODEL_MASK_OVERLAYITEM_H

#include "GUI/Model/Type/NamedItem.h"
#include <QObject>

//! Something to be shown as overlay in a graphics scene. Base class for point and mask items.

class OverlayItem : public QObject, public NamedItem {
    Q_OBJECT
public:
    OverlayItem();
    virtual ~OverlayItem();

signals:
    void maskGeometryChanged() const;
    void maskVisibilityChanged();
    void maskToBeDestroyed();
};

#endif // BORNAGAIN_GUI_MODEL_MASK_OVERLAYITEM_H
