//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Mask/PointItem.cpp
//! @brief     Implements class PointItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Mask/PointItem.h"
#include "GUI/Model/Util/Backup.h"
#include "GUI/Model/Util/UtilXML.h"
#include <QRegularExpression>

PointItem::PointItem()
{
    m_posX.init("x", "", "x coordinate", 0, 4, RealLimits::limitless(), "xPos");
    m_posY.init("y", "", "y coordinate", 0, 4, RealLimits::limitless(), "yPos");
}

void PointItem::writeTo(QXmlStreamWriter* w) const
{
    XML::writeAttribute(w, "x", posX().dVal());
    XML::writeAttribute(w, "y", posY().dVal());
}

void PointItem::readFrom(QXmlStreamReader* r)
{
    setPosX(XML::readDouble(r, "x"));
    setPosY(XML::readDouble(r, "y"));
}
