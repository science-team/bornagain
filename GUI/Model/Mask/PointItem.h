//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Mask/PointItem.h
//! @brief     Defines class PointItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_MASK_POINTITEM_H
#define BORNAGAIN_GUI_MODEL_MASK_POINTITEM_H

#include "Base/Type/OwningVector.h"
#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "GUI/Model/Mask/OverlayItem.h"
#include <QXmlStreamReader>

class PointItem : public OverlayItem {
public:
    PointItem();

    DoubleProperty& posX() { return m_posX; }
    const DoubleProperty& posX() const { return m_posX; }
    void setPosX(double val) { m_posX.setDVal(val); }
    DoubleProperty& posY() { return m_posY; }
    const DoubleProperty& posY() const { return m_posY; }
    void setPosY(double val) { m_posY.setDVal(val); }

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

private:
    DoubleProperty m_posX;
    DoubleProperty m_posY;
};

#endif // BORNAGAIN_GUI_MODEL_MASK_POINTITEM_H
