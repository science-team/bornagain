//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Material/MaterialItem.h
//! @brief     Defines class MaterialItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_MATERIAL_MATERIALITEM_H
#define BORNAGAIN_GUI_MODEL_MATERIAL_MATERIALITEM_H

#include "GUI/Model/Descriptor/VectorProperty.h"
#include <QColor>
#include <QObject>
#include <QXmlStreamReader>
#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>
#include <memory>

class Material;

class MaterialItem : public QObject {
    Q_OBJECT
public:
    MaterialItem();

    //! Turns material into refractive index material.
    //!
    //! Set refractive index as in 1 - delta + i * beta
    void setRefractiveIndex(double delta, double beta);

    //! Turns material into SLD based material.
    void setScatteringLengthDensity(complex_t sld);

    DoubleProperty& delta() { return m_delta; }
    const DoubleProperty& delta() const { return m_delta; }

    DoubleProperty& beta() { return m_beta; }
    const DoubleProperty& beta() const { return m_beta; }

    DoubleProperty& sldRe() { return m_sld_re; }
    const DoubleProperty& sldRe() const { return m_sld_re; }

    DoubleProperty& sldIm() { return m_sld_im; }
    const DoubleProperty& sldIm() const { return m_sld_im; }

    VectorProperty& magnetization() { return m_magnetization; }
    const VectorProperty& magnetization() const { return m_magnetization; }
    void setMagnetization(const R3& magnetization);

    /// \return true if refractive index was given, otherwise SLD was given
    bool hasRefractiveIndex() const { return m_use_refractive_index; }

    QString matItemName() const { return m_name; }
    void setMatItemName(const QString& name);

    QString identifier() const { return m_id; }
    void createNewIdentifier();

    QColor color() const { return m_color; }
    void setColor(const QColor& color);

    bool isMagnetizatioEnabled() const { return m_magnetization_on; }
    void setMagnetizationEnabled(bool b);

    std::unique_ptr<Material> createMaterial() const;

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    //! Updates content from the other material.
    //!
    //! Does NOT change the identifier.
    //! emits dataChanged, if differences exist.
    void updateFrom(const MaterialItem& other);

private:
    //! Compares all contents. The inactive contents (e.g. SLD in case of refractive) are not taken
    //! into account. Only used by updateFrom.
    bool operator==(const MaterialItem& other) const;

    QString m_name;
    QString m_id;
    QColor m_color;
    VectorProperty m_magnetization;
    bool m_use_refractive_index = true;
    bool m_magnetization_on = false;
    DoubleProperty m_delta;
    DoubleProperty m_beta;
    DoubleProperty m_sld_re;
    DoubleProperty m_sld_im;

signals:
    void dataChanged() const;
};

#endif // BORNAGAIN_GUI_MODEL_MATERIAL_MATERIALITEM_H
