//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Material/MaterialsSet.h
//! @brief     Defines class MaterialsSet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_MATERIAL_MATERIALSSET_H
#define BORNAGAIN_GUI_MODEL_MATERIAL_MATERIALSSET_H

#include "Base/Type/VectorWC.h"
#include <QMap>
#include <QObject>
#include <QVector>
#include <QXmlStreamReader>

//! Materials created by default
enum class DefaultMaterials { Default, Vacuum, Particle, Core, Substrate };

extern const QMap<QString, DefaultMaterials> materialMap;

class MaterialItem;

class MaterialsSet : public QObject, public VectorWC<MaterialItem> {
    Q_OBJECT
public:
    MaterialsSet();
    ~MaterialsSet();

    //! Add the material and take ownership of it.
    MaterialItem* addMaterialItem(MaterialItem* materialItem);

    MaterialItem* addRefractiveMaterialItem(const QString& name, double delta, double beta);
    MaterialItem* addSLDMaterialItem(const QString& name, double sld, double abs_term);

    MaterialItem* materialItemFromName(const QString& name) const;
    MaterialItem* materialItemFromIdentifier(const QString& identifier) const;

    const MaterialItem* defaultMaterialItem() const;
    const MaterialItem* defaultCoreMaterialItem() const;
    const MaterialItem* defaultParticleMaterialItem() const;

    void removeMaterialItem(MaterialItem* materialItem);

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

signals:
    void materialAddedOrRemoved();
    void materialChanged();

private:
    //! Add the material and take ownership of it.
    //!
    //! In the given flag the signal "materialAddedOrRemoved" can be suppressed.
    MaterialItem* addMaterialItem(MaterialItem* material, bool signalAdding);

    MaterialItem* addMaterialItem();
};

#endif // BORNAGAIN_GUI_MODEL_MATERIAL_MATERIALSSET_H
