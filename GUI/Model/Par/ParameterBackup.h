//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Par/ParameterBackup.h
//! @brief     Defines class ParameterBackup.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_PAR_PARAMETERBACKUP_H
#define BORNAGAIN_GUI_MODEL_PAR_PARAMETERBACKUP_H

#include <QMap>
#include <QXmlStreamReader>

//! Contains set of backup parameters for a single snapshot of the parameter tree.

class ParameterBackup {
public:
    ParameterBackup(const QString& title = "");

    QString title() const { return m_title; }
    void setTitle(const QString& title) { m_title = title; }

    double value(const QString& link);
    void setValue(const QString& link, double d);

    bool contains(const QString& link);

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);
    void readValues(QXmlStreamReader* r);

private:
    QString m_title;
    QMap<QString, double> m_backup_values;
};

#endif // BORNAGAIN_GUI_MODEL_PAR_PARAMETERBACKUP_H
