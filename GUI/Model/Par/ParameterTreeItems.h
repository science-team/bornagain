//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Par/ParameterTreeItems.h
//! @brief     Defines classes ParameterLabelItem, ParameterItem, ParameterContainerItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_PAR_PARAMETERTREEITEMS_H
#define BORNAGAIN_GUI_MODEL_PAR_PARAMETERTREEITEMS_H

#include "Base/Type/OwningVector.h"
#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "GUI/Model/Par/ParameterBackup.h"
#include <QColor>
#include <QXmlStreamReader>

//! ParameterTreeItems is a collection of items necessary to form a tuning tree for
//! real time widget.

//! The ParameterLabelItem class represents a label (string without value, like 'Layer',
//! 'MultiLayer') in a parameter tuning tree.

class ParameterLabelItem : public QObject {
    Q_OBJECT
public:
    ParameterLabelItem(const QString& title, QObject* parent);

    QString title() const { return m_title; }
    void setTitle(const QString& title) { m_title = title; }

    QColor color() const { return m_color; }
    void setColor(const QColor& color) { m_color = color; }

    bool collapsed() const { return m_collapsed; }
    void setCollapsed(bool collapsed) { m_collapsed = collapsed; }

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

private:
    QString m_title;
    QColor m_color;
    bool m_collapsed = false;
};

//! The ParameterItem class represent a tuning value in a parameter tuning tree.

class ParameterItem : public QObject {
    Q_OBJECT
public:
    ParameterItem(QObject* parent);

    QString title() const { return m_title; }
    void setTitle(const QString& title) { m_title = title; }

    double valueOfLink() const;
    void propagateValueToLink(double newValue);

    //! Unique string to identify this ParameterItem.
    //!
    //! The link is arbitrary. It cannot be used for finding the linked item (therefore it does
    //! not have to be a model path). However, it is used for comparison, also across project
    //! load/save. Therefore the link is always the same, not e.g. an always generated UUID.
    //! This link is used for setting backup values and for finding this ParameterItem when
    //! referring from fit parameters.
    QString link() const;

    QString titleForFitItem() const;

    //! Links this item to the given value defined by a property.
    void linkToProperty(DoubleProperty& d);

    RealLimits limitsOfLink() const;
    int decimalsOfLink() const;

private:
    QString m_title;
    const DoubleProperty* m_d; // for the access to limits, uid, decimals
    std::function<double()> getPropertyValue = nullptr;
    std::function<void(double)> setPropertyValue = nullptr;
};

//! The ParameterContainerItem is a top item to hold all ParameterItem, represents an entry
//! point to parameter tuning tree. Part of JobItem.

class ParameterContainerItem {
public:
    ParameterContainerItem();

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    QStringList backupTitles() const;

    void addBackupValues(const QString& title);
    void restoreBackupValues(int index);
    void deleteBackupValues(int index);

    ParameterItem* findParameterItem(const QString& link) const;

    QObject* parameterTreeRoot();

    int currentIndex() const { return m_current_index; }
    void setCurrentIndex(int i) { m_current_index = i; }

private:
    void addBackupValue(QObject* item);
    void restoreBackupValue(QObject* item, int index);

    int m_current_index = 0;
    OwningVector<ParameterBackup> m_backup_values;
    std::unique_ptr<QObject> m_parameter_tree_root;
};

#endif // BORNAGAIN_GUI_MODEL_PAR_PARAMETERTREEITEMS_H
