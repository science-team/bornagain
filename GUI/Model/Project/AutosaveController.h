//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Project/AutosaveController.h
//! @brief     Defines class AutosaveController.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_PROJECT_AUTOSAVECONTROLLER_H
#define BORNAGAIN_GUI_MODEL_PROJECT_AUTOSAVECONTROLLER_H

#include <QObject>

class ProjectDocument;
class UpdateTimer;

//! Triggers autosave request after some accumulated ammount of document changes.

class AutosaveController : public QObject {
    Q_OBJECT
public:
    explicit AutosaveController(QObject* parent = nullptr, int update_interval_msec = 20000);

    void setDocument(ProjectDocument* document);

    //! Sets autosave time (in msec)
    void setAutosaveTime(int timerInterval);

    //! The complete path to the autosave dir (e.g. '/projects/Untitled2/autosave').
    QString autosaveDir() const;
    QString autosaveFullPath() const;

    //! remove auto save directory for given project and all its content
    void removeAutosaveDir() const;

private slots:
    void onTimerTimeout();
    void onDocumentDestroyed(QObject* object);
    void onDocumentModified();

private:
    void autosave();

    //! Tries to make sure that the directory for auto saving exists. Tries to create it if not
    //! existing so far. No creation, if project directory itself doesn't exist at all.
    //! Returns true, if the directory finally exists.
    bool assureAutoSaveDirExists() const;

    ProjectDocument* m_document;
    UpdateTimer* m_timer;
};

#endif // BORNAGAIN_GUI_MODEL_PROJECT_AUTOSAVECONTROLLER_H
