//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Project/DataSource.h
//! @brief     Defines class DataSource.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_PROJECT_DATASOURCE_H
#define BORNAGAIN_GUI_MODEL_PROJECT_DATASOURCE_H

#include <QObject>

class Data1DItem;
class Data2DItem;
class DataItem;
class JobItem;

//! The DataSource class is a base for widgets, working data, stored in JobItem or
//! DatafileItem. DataSource provides an access to SpecularDataItems and Data2DItems
//! separately and as a list of items that is required for simultaneous, synchronous work while
//! plotting and changing their properties.

class DataSource {
public:
    virtual ~DataSource();

    //... Access to concrete items:

    virtual Data1DItem* realData1DItem() const = 0;
    virtual Data2DItem* realData2DItem() const = 0;

    virtual Data1DItem* simuData1DItem() const { return nullptr; }
    virtual Data2DItem* simuData2DItem() const { return nullptr; }
    virtual Data1DItem* diffData1DItem() const { return nullptr; }
    virtual Data2DItem* diffData2DItem() const { return nullptr; }

    //... Access to lists of items:

    QVector<Data1DItem*> mainData1DItems() const;
    QVector<Data2DItem*> mainData2DItems() const;

    QVector<Data1DItem*> allData1DItems() const;
    QVector<Data2DItem*> allData2DItems() const;

    Data1DItem* currentData1DItem() const;
    Data2DItem* currentData2DItem() const;

    virtual bool isFromData() const { return false; }
    virtual bool isFromJob() const { return false; }
};

class DataFromData : public DataSource {
public:
    ~DataFromData() override;
    Data1DItem* realData1DItem() const override;
    Data2DItem* realData2DItem() const override;

    bool isFromData() const override { return true; }

private:
    DataItem* realDataItem() const;
};

class DataFromJob : public DataSource {
public:
    ~DataFromJob() override;

    Data1DItem* realData1DItem() const override;
    Data2DItem* realData2DItem() const override;

    Data1DItem* simuData1DItem() const override;
    Data2DItem* simuData2DItem() const override;
    Data1DItem* diffData1DItem() const override;
    Data2DItem* diffData2DItem() const override;

    bool isFromJob() const override { return true; }

private:
    JobItem* jobxItem() const;
};

#endif // BORNAGAIN_GUI_MODEL_PROJECT_DATASOURCE_H
