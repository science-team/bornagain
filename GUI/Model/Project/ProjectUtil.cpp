//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Project/ProjectUtil.cpp
//! @brief     Implements namespace GUI::Util::Project.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Project/ProjectUtil.h"
#include <QFileInfo>

QString GUI::Util::Project::projectName(const QString& projectFullPath)
{
    return QFileInfo(projectFullPath).baseName();
}

QString GUI::Util::Project::projectDir(const QString& projectFullPath)
{
    return QFileInfo(projectFullPath).path();
}

QString GUI::Util::Project::autosaveSubdir()
{
    return "autosave";
}

QString GUI::Util::Project::autosaveDir(const QString& projectFullPath)
{
    return projectDir(projectFullPath) + "/" + autosaveSubdir();
}

QString GUI::Util::Project::autosaveFullPath(const QString& projectFullPath)
{
    return autosaveDir(projectFullPath) + "/" + projectName(projectFullPath) + projectFileExtension;
}

bool GUI::Util::Project::hasAutosavedData(const QString& projectFullPath)
{
    return QFile::exists(projectFullPath) && QFile::exists(autosaveFullPath(projectFullPath));
}

bool GUI::Util::Project::isAutosave(const QString& projectFullPath)
{
    return projectFullPath.contains(autosaveSubdir());
}
