//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Project/ProjectUtil.h
//! @brief     Declares namespace GUI::Util::Project.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_PROJECT_PROJECTUTIL_H
#define BORNAGAIN_GUI_MODEL_PROJECT_PROJECTUTIL_H

#include <QString>

//! Defines convenience function for project manager and document.

namespace GUI::Util::Project {

inline constexpr const char* projectFileExtension{".ba"};

//! Returns project name deduced from project file name.
QString projectName(const QString& projectFullPath);

//! Returns project directory deduced from project file name.
QString projectDir(const QString& projectFullPath);

//! Returns fixed name for autosave sub-directory.
QString autosaveSubdir();

//! Returns name of autosave directory for project with given project file name.
//! E.g. from '/projects/Untitled2/Untitled2.ba' returns '/projects/Untitled2/autosave'
QString autosaveDir(const QString& projectFullPath);

//! Returns name of project for autoSave from given project file name.
//! E.g. from '/projects/Untitled2/Untitled2.ba' returns
//! '/projects/Untitled2/autosave/Untitled2.ba'
QString autosaveFullPath(const QString& projectFullPath);

//! Returns true if project with given projectFullPath contains autosaved data.
bool hasAutosavedData(const QString& projectFullPath);

//! Returns true if path leads to the autosave subdirectory.
bool isAutosave(const QString& projectFullPath);

} // namespace GUI::Util::Project

#endif // BORNAGAIN_GUI_MODEL_PROJECT_PROJECTUTIL_H
