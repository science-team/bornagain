//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/CompoundItem.cpp
//! @brief     Implements class CompoundItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/CompoundItem.h"
#include "Base/Util/Assert.h"
#include "Base/Util/Vec.h"
#include "GUI/Model/Sample/CoreAndShellItem.h"
#include "GUI/Model/Sample/MesocrystalItem.h"
#include "GUI/Model/Sample/ParticleCatalog.h"
#include "GUI/Model/Sample/ParticleItem.h"
#include "Sample/Particle/Compound.h"
#include "Sample/Particle/CoreAndShell.h"
#include "Sample/Particle/Mesocrystal.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"

namespace {
namespace Tag {

const QString Particle("Particle");
const QString BaseData("BaseData");
const QString ExpandCompoundGroupbox("ExpandCompoundGroupbox");

} // namespace Tag

const QString abundance_tooltip = "Proportion of this type of particles normalized to the \n"
                                  "total number of particles in the layout";

const QString position_tooltip = "Relative position of the particle's reference point \n"
                                 "in the coordinate system of the parent (nm)";

} // namespace


CompoundItem::CompoundItem(const MaterialsSet* materials)
    : ItemWithParticles(abundance_tooltip, position_tooltip)
    , m_materials(materials)
{
}

void CompoundItem::addItemWithParticleSelection(ItemWithParticles* particle)
{
    m_particles.push_back(particle);
}

void CompoundItem::removeItemWithParticle(ItemWithParticles* particle)
{
    m_particles.delete_element(particle);
}

void CompoundItem::writeTo(QXmlStreamWriter* w) const
{
    XML::writeBaseElement<ItemWithParticles>(w, XML::Tag::BaseData, this);
    for (auto* t : m_particles)
        XML::writeChosen<ItemWithParticles, ParticleCatalog>(t, w, Tag::Particle);
    XML::writeTaggedValue(w, Tag::ExpandCompoundGroupbox, expandCompound);
}

void CompoundItem::readFrom(QXmlStreamReader* r)
{
    m_particles.clear();
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::BaseData)
            XML::readBaseElement<ItemWithParticles>(r, tag, this);
        else if (tag == Tag::Particle)
            m_particles.push_back(
                XML::readChosen<ItemWithParticles, ParticleCatalog>(r, tag, m_materials));
        else if (tag == Tag::ExpandCompoundGroupbox)
            expandCompound = XML::readTaggedBool(r, tag);
        else
            r->skipCurrentElement();
    }
}

std::unique_ptr<Compound> CompoundItem::createCompound() const
{
    auto P_composition = std::make_unique<Compound>();
    P_composition->setAbundance(abundance().dVal());
    for (ItemWithParticles* t : m_particles) {
        if (auto* particleItem = dynamic_cast<ParticleItem*>(t)) {
            if (auto P_particle = particleItem->createParticle())
                P_composition->addComponent(*P_particle);
        } else if (auto* coreShellItem = dynamic_cast<CoreAndShellItem*>(t)) {
            if (auto P_particle_coreshell = coreShellItem->createCoreAndShell())
                P_composition->addComponent(*P_particle_coreshell);
        } else if (auto* compositionItem = dynamic_cast<CompoundItem*>(t)) {
            if (auto P_child_composition = compositionItem->createCompound())
                P_composition->addComponent(*P_child_composition);
        } else if (auto* mesocrystalItem = dynamic_cast<MesocrystalItem*>(t)) {
            if (auto P_meso = mesocrystalItem->createMesocrystal())
                P_composition->addComponent(*P_meso);
        }
    }

    if (const auto r = createRotation(); r && !r->isIdentity())
        P_composition->rotate(*r);
    P_composition->translate(position());

    return P_composition;
}

std::vector<ItemWithParticles*> CompoundItem::containedItemsWithParticles() const
{
    std::vector<ItemWithParticles*> result;
    for (auto* t : m_particles) {
        result.push_back(t);
        Vec::concat(result, t->containedItemsWithParticles());
    }
    return result;
}
