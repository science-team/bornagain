//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/CompoundItem.h
//! @brief     Defines class CompoundItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_COMPOUNDITEM_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_COMPOUNDITEM_H

#include "Base/Type/OwningVector.h"
#include "GUI/Model/Sample/ItemWithParticles.h"
#include <memory>

class Compound;
class MaterialsSet;

class CompoundItem : public ItemWithParticles {
public:
    CompoundItem(const MaterialsSet* materials);

    void addItemWithParticleSelection(ItemWithParticles* particle);
    void removeItemWithParticle(ItemWithParticles* particle);

    void readFrom(QXmlStreamReader* r) override;
    void writeTo(QXmlStreamWriter* w) const override;

    std::unique_ptr<Compound> createCompound() const;

    std::vector<ItemWithParticles*> itemsWithParticles() const { return m_particles.shared(); }
    std::vector<ItemWithParticles*> containedItemsWithParticles() const override;

    bool expandCompound = true;

private:
    OwningVector<ItemWithParticles> m_particles;
    const MaterialsSet* m_materials;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_COMPOUNDITEM_H
