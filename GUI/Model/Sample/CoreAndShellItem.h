//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/CoreAndShellItem.h
//! @brief     Defines class CoreAndShellItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_COREANDSHELLITEM_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_COREANDSHELLITEM_H

#include "GUI/Model/Sample/ItemWithParticles.h"
#include <memory>

class CoreAndShell;
class MaterialsSet;
class ParticleItem;

class CoreAndShellItem : public ItemWithParticles {
public:
    CoreAndShellItem(const MaterialsSet* materials);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    std::unique_ptr<CoreAndShell> createCoreAndShell() const;

    ParticleItem* coreItem() const;
    ParticleItem* createCoreItem(const MaterialsSet* materials);

    ParticleItem* shellItem() const;
    ParticleItem* createShellItem(const MaterialsSet* materials);

    std::vector<ItemWithParticles*> containedItemsWithParticles() const override;

    bool expandMain = true;
    bool expandCore = true;
    bool expandShell = true;

private:
    std::unique_ptr<ParticleItem> m_core;
    std::unique_ptr<ParticleItem> m_shell;
    const MaterialsSet* m_materials;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_COREANDSHELLITEM_H
