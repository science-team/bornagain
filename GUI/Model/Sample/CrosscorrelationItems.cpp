//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/CrosscorrelationItems.h
//! @brief     Implements classes CrosscorrelationItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/CrosscorrelationItems.h"
#include "GUI/Model/Util/UtilXML.h"
#include "Sample/Interface/CrosscorrelationModels.h"

namespace {
namespace Tag {

const QString CrosscorrelationDepth("CrosscorrelationDepth");
const QString BaseCrosscorrelationDepth("BaseCrosscorrelationDepth");
const QString BaseSpatialFrequency("BaseSpatialFrequency");
const QString Power("Power");

} // namespace Tag
} // namespace

SpatialFrequencyCrosscorrelationItem::SpatialFrequencyCrosscorrelationItem(
    double base_crosscorr_depth, double base_spatial_frequency, double power)
{
    m_base_crosscorr_depth.init("Base crosscorrelation depth", "nm",
                                "Vertical length for the correlation of roughness at different "
                                "interfaces for the given base spatial frequency",
                                base_crosscorr_depth, "baseCrossCorrDepth");

    m_base_spatial_frequency.init(
        "Base spatial frequency", "1/nm",
        "Spatial frequency of roughness for which the correlation depth "
        "at different interfaces is equal to the given base correlation depth",
        base_spatial_frequency, "baseSpatialFrequency");

    m_power.init("Exponent of spatial frequency", "",
                 "Power exponent, which defines the dependence of the correlation depth on spatial "
                 "frequency of roughness",
                 power, "power");
}

void SpatialFrequencyCrosscorrelationItem::writeTo(QXmlStreamWriter* w) const
{
    m_base_crosscorr_depth.writeTo2(w, Tag::BaseCrosscorrelationDepth);
    m_base_spatial_frequency.writeTo2(w, Tag::BaseSpatialFrequency);
    m_power.writeTo2(w, Tag::Power);
}

void SpatialFrequencyCrosscorrelationItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::BaseCrosscorrelationDepth)
            m_base_crosscorr_depth.readFrom2(r, tag);
        else if (tag == Tag::BaseSpatialFrequency)
            m_base_spatial_frequency.readFrom2(r, tag);
        else if (tag == Tag::Power)
            m_power.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

std::unique_ptr<CrosscorrelationModel> SpatialFrequencyCrosscorrelationItem::createModel() const
{
    return std::make_unique<SpatialFrequencyCrosscorrelation>(
        m_base_crosscorr_depth.dVal(), m_base_spatial_frequency.dVal(), m_power.dVal());
}

//-------------------------------------------------------------------------------------------------

CommonDepthCrosscorrelationItem::CommonDepthCrosscorrelationItem(double cross_corr_depth)
{
    m_crosscorrelation_depth.init(
        "Crosscorrelation depth", "nm",
        "Vertical length for the correlation of roughness at different interfaces",
        cross_corr_depth, "crossCorrDepth");
}

void CommonDepthCrosscorrelationItem::writeTo(QXmlStreamWriter* w) const
{
    m_crosscorrelation_depth.writeTo2(w, Tag::CrosscorrelationDepth);
}

void CommonDepthCrosscorrelationItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::CrosscorrelationDepth)
            m_crosscorrelation_depth.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

std::unique_ptr<CrosscorrelationModel> CommonDepthCrosscorrelationItem::createModel() const
{
    return std::make_unique<CommonDepthCrosscorrelation>(m_crosscorrelation_depth.dVal());
}
