//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/CrosscorrelationItems.h
//! @brief     Defines classes CrosscorrelationItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_CROSSCORRELATIONITEMS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_CROSSCORRELATIONITEMS_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include <QXmlStreamReader>

class CrosscorrelationModel;

class CrosscorrelationItem {
public:
    virtual ~CrosscorrelationItem() = default;

    virtual void writeTo(QXmlStreamWriter*) const = 0;
    virtual void readFrom(QXmlStreamReader*) = 0;
    virtual std::unique_ptr<CrosscorrelationModel> createModel() const = 0;
    virtual DoubleProperties crossCorrProperties() = 0;

protected:
    CrosscorrelationItem() = default;
};

class SpatialFrequencyCrosscorrelationItem : public CrosscorrelationItem {
public:
    SpatialFrequencyCrosscorrelationItem(double base_crosscorr_depth, double base_spatial_frequency,
                                         double power);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    std::unique_ptr<CrosscorrelationModel> createModel() const override;
    DoubleProperties crossCorrProperties() override
    {
        return {&m_base_crosscorr_depth, &m_base_spatial_frequency, &m_power};
    }

private:
    DoubleProperty m_base_crosscorr_depth;
    DoubleProperty m_base_spatial_frequency;
    DoubleProperty m_power;
};

class CommonDepthCrosscorrelationItem : public CrosscorrelationItem {
public:
    CommonDepthCrosscorrelationItem(double cross_corr_depth);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    std::unique_ptr<CrosscorrelationModel> createModel() const override;
    DoubleProperties crossCorrProperties() override { return {&m_crosscorrelation_depth}; }

private:
    DoubleProperty m_crosscorrelation_depth;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_CROSSCORRELATIONITEMS_H
