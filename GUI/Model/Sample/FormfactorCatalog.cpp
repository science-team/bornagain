//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/FormfactorCatalog.cpp
//! @brief     Implements class FormfactorCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/FormfactorCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/FormfactorItems.h"

FormfactorItem* FormfactorCatalog::create(Type type)
{
#define CREATE(t)                                                                                  \
    case Type::t:                                                                                  \
        return new t##Item

    switch (type) {
        CREATE(BarGauss);
        CREATE(BarLorentz);
        CREATE(Bipyramid4);
        CREATE(Box);
        CREATE(CantellatedCube);
        CREATE(Cone);
        CREATE(CosineRippleBox);
        CREATE(CosineRippleGauss);
        CREATE(CosineRippleLorentz);
        CREATE(Cylinder);
        CREATE(Dodecahedron);
        CREATE(EllipsoidalCylinder);
        CREATE(Sphere);
        CREATE(Spheroid);
        CREATE(HemiEllipsoid);
        CREATE(HorizontalCylinder);
        CREATE(Icosahedron);
        CREATE(PlatonicOctahedron);
        CREATE(PlatonicTetrahedron);
        CREATE(Prism3);
        CREATE(Prism6);
        CREATE(Pyramid2);
        CREATE(Pyramid3);
        CREATE(Pyramid4);
        CREATE(Pyramid6);
        CREATE(SawtoothRippleBox);
        CREATE(SawtoothRippleGauss);
        CREATE(SawtoothRippleLorentz);
        CREATE(TruncatedCube);
        CREATE(SphericalSegment);
        CREATE(SpheroidalSegment);
    default:
        ASSERT_NEVER;
    }

#undef CREATE
}

QVector<FormfactorCatalog::Type> FormfactorCatalog::types()
{
    return {Type::BarGauss,
            Type::BarLorentz,
            Type::Bipyramid4,
            Type::Box,
            Type::CantellatedCube,
            Type::Cone,
            Type::CosineRippleBox,
            Type::CosineRippleGauss,
            Type::CosineRippleLorentz,
            Type::Cylinder,
            Type::Dodecahedron,
            Type::EllipsoidalCylinder,
            Type::Sphere,
            Type::Spheroid,
            Type::HemiEllipsoid,
            Type::HorizontalCylinder,
            Type::Icosahedron,
            Type::PlatonicOctahedron,
            Type::PlatonicTetrahedron,
            Type::Prism3,
            Type::Prism6,
            Type::Pyramid2,
            Type::Pyramid3,
            Type::Pyramid4,
            Type::Pyramid6,
            Type::SawtoothRippleBox,
            Type::SawtoothRippleGauss,
            Type::SawtoothRippleLorentz,
            Type::TruncatedCube,
            Type::SphericalSegment,
            Type::SpheroidalSegment};
}

QVector<FormfactorCatalog::Type> FormfactorCatalog::hardParticleTypes()
{
    return {Type::Bipyramid4,
            Type::Box,
            Type::CantellatedCube,
            Type::Cone,
            Type::Cylinder,
            Type::Dodecahedron,
            Type::EllipsoidalCylinder,
            Type::Sphere,
            Type::Spheroid,
            Type::HemiEllipsoid,
            Type::HorizontalCylinder,
            Type::Icosahedron,
            Type::PlatonicOctahedron,
            Type::PlatonicTetrahedron,
            Type::Prism3,
            Type::Prism6,
            Type::Pyramid2,
            Type::Pyramid3,
            Type::Pyramid4,
            Type::Pyramid6,
            Type::TruncatedCube,
            Type::SphericalSegment,
            Type::SpheroidalSegment};
}


QVector<FormfactorCatalog::Type> FormfactorCatalog::rippleTypes()
{
    return {Type::BarGauss,          Type::BarLorentz,          Type::Box,
            Type::CosineRippleBox,   Type::CosineRippleGauss,   Type::CosineRippleLorentz,
            Type::SawtoothRippleBox, Type::SawtoothRippleGauss, Type::SawtoothRippleLorentz};
}

UiInfo FormfactorCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::Pyramid2:
        return {"Anisotropic pyramid", "Truncated pyramid with a rectangular base",
                ":/images/ff/Pyramid2_64x64.png"};

    case Type::BarGauss:
        return {"BarGauss", "Rectangular cuboid", ":/images/ff/Box_64x64.png"};

    case Type::BarLorentz:
        return {"BarLorentz", "Rectangular cuboid", ":/images/ff/Box_64x64.png"};

    case Type::Box:
        return {"Box", "Rectangular cuboid", ":/images/ff/Box_64x64.png"};

    case Type::Cone:
        return {"Cone", "Truncated cone with circular base", ":/images/ff/Cone_64x64.png"};

    case Type::Pyramid6:
        return {"Pyramid6", "A truncated pyramid, based on a regular hexagon",
                ":/images/ff/Pyramid6_64x64.png"};

    case Type::Bipyramid4:
        return {"Bipyramid4",
                "Compound of two truncated pyramids with a common square base "
                "and opposite orientations",
                ":/images/ff/Bipyramid4_64x64.png"};

    case Type::Dodecahedron:
        return {"Dodecahedron", "Dodecahedron", ":/images/ff/Dodecahedron_64x64.png"};

    case Type::Cylinder:
        return {"Cylinder", "Cylinder with a circular base", ":/images/ff/Cylinder_64x64.png"};

    case Type::EllipsoidalCylinder:
        return {"Ellipsoidal cylinder", "Cylinder with an ellipse cross section",
                ":/images/ff/EllipsoidalCylinder_64x64.png"};

    case Type::Sphere:
        return {"Full sphere", "Full sphere", ":/images/ff/Sphere_64x64.png"};

    case Type::Spheroid:
        return {"Full spheroid",
                "Full spheroid, generated by rotating an ellipse around the vertical axis",
                ":/images/ff/Spheroid_64x64.png"};

    case Type::HemiEllipsoid:
        return {"Hemi ellipsoid",
                "A horizontally oriented ellipsoid, truncated at the central plane",
                ":/images/ff/HemiEllipsoid_64x64.png"};

    case Type::Icosahedron:
        return {"Icosahedron", "Icosahedron", ":/images/ff/Icosahedron_64x64.png"};

    case Type::Prism3:
        return {"Prism3", "Prism with an equilateral triangle base",
                ":/images/ff/Prism3_64x64.png"};

    case Type::Prism6:
        return {"Prism6", "Prism with a regular hexagonal base", ":/images/ff/Prism6_64x64.png"};

    case Type::Pyramid4:
        return {"Pyramid4", "Truncated pyramid with a square base",
                ":/images/ff/Pyramid4_64x64.png"};

    case Type::CosineRippleBox:
        return {"CosineRippleBox", "Particle with a cosine profile and a rectangular base",
                ":/images/ff/CosineRipple_64x64.png"};

    case Type::CosineRippleGauss:
        return {"CosineRippleGauss", "Particle with a cosine profile and a rectangular base",
                ":/images/ff/CosineRipple_64x64.png"};

    case Type::CosineRippleLorentz:
        return {"CosineRippleLorentz", "Particle with a cosine profile and a rectangular base",
                ":/images/ff/CosineRipple_64x64.png"};

    case Type::SawtoothRippleBox:
        return {"SawtoothRippleBox",
                "Particle with an asymmetric triangle profile and a rectangular base",
                ":/images/ff/SawtoothRipple_64x64.png"};

    case Type::SawtoothRippleGauss:
        return {"SawtoothRippleGauss",
                "Particle with an asymmetric triangle profile and a rectangular base",
                ":/images/ff/SawtoothRipple_64x64.png"};

    case Type::SawtoothRippleLorentz:
        return {"SawtoothRippleLorentz",
                "Particle with an asymmetric triangle profile and a rectangular base",
                ":/images/ff/SawtoothRipple_64x64.png"};

    case Type::Pyramid3:
        return {"Pyramid3",
                "Truncated polyhedron with equilateral triangle base and cropped side faces",
                ":/images/ff/Pyramid3_64x64.png"};

    case Type::TruncatedCube:
        return {"Truncated cube", "A cube whose eight vertices have been removed",
                ":/images/ff/TruncatedCube_64x64.png"};

    case Type::SphericalSegment:
        return {"Truncated sphere", "Spherical dome", ":/images/ff/SphericalSegment_64x64.png"};

    case Type::SpheroidalSegment:
        return {"Truncated spheroid", "Spheroidal dome", ":/images/ff/SpheroidalSegment_64x64.png"};

    case Type::CantellatedCube:
        return {"Cantellated cube", "A cube with truncated edges and vertices",
                ":/images/ff/CantellatedCube_64x64.png"};

    case Type::HorizontalCylinder:
        return {"Horizontal cylinder", "Cylinder with a circular base, lying in x direction",
                ":/images/ff/HorizontalCylinder_64x64.png"};

    case Type::PlatonicOctahedron:
        return {"Platonic octahedron", "Regular octahedron",
                ":/images/ff/PlatonicOctahedron_64x64.png"};

    case Type::PlatonicTetrahedron:
        return {"Platonic tetrahedron", "Regular tetrahedron",
                ":/images/ff/PlatonicTetrahedron_64x64.png"};

    default:
        ASSERT_NEVER;
    }
}

FormfactorCatalog::Type FormfactorCatalog::type(const FormfactorItem* item)
{
    ASSERT(item);

#define RETURN_IF(type)                                                                            \
    if (dynamic_cast<const type##Item*>(item))                                                     \
    return Type::type

    RETURN_IF(BarGauss);
    RETURN_IF(BarLorentz);
    RETURN_IF(Bipyramid4);
    RETURN_IF(Box);
    RETURN_IF(CantellatedCube);
    RETURN_IF(Cone);
    RETURN_IF(CosineRippleBox);
    RETURN_IF(CosineRippleGauss);
    RETURN_IF(CosineRippleLorentz);
    RETURN_IF(Cylinder);
    RETURN_IF(Dodecahedron);
    RETURN_IF(EllipsoidalCylinder);
    RETURN_IF(Sphere);
    RETURN_IF(Spheroid);
    RETURN_IF(HemiEllipsoid);
    RETURN_IF(HorizontalCylinder);
    RETURN_IF(Icosahedron);
    RETURN_IF(PlatonicOctahedron);
    RETURN_IF(PlatonicTetrahedron);
    RETURN_IF(Prism3);
    RETURN_IF(Prism6);
    RETURN_IF(Pyramid2);
    RETURN_IF(Pyramid3);
    RETURN_IF(Pyramid4);
    RETURN_IF(Pyramid6);
    RETURN_IF(SawtoothRippleBox);
    RETURN_IF(SawtoothRippleGauss);
    RETURN_IF(SawtoothRippleLorentz);
    RETURN_IF(TruncatedCube);
    RETURN_IF(SphericalSegment);
    RETURN_IF(SpheroidalSegment);
#undef RETURN_IF
    ASSERT_NEVER;
}

QString FormfactorCatalog::menuEntry(const FormfactorItem* item)
{
    return uiInfo(type(item)).menuEntry;
}
