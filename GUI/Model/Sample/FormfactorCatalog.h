//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/FormfactorCatalog.h
//! @brief     Defines class FormfactorCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_FORMFACTORCATALOG_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_FORMFACTORCATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class FormfactorItem;

class FormfactorCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t {
        BarGauss = 1,
        BarLorentz = 2,
        Bipyramid4 = 3,
        Box = 4,
        CantellatedCube = 5,
        Cone = 6,
        CosineRippleBox = 7,
        CosineRippleGauss = 8,
        CosineRippleLorentz = 9,
        Cylinder = 10,
        Dodecahedron = 11,
        EllipsoidalCylinder = 12,
        Sphere = 13,
        Spheroid = 14,
        HemiEllipsoid = 15,
        HorizontalCylinder = 16,
        Icosahedron = 17,
        PlatonicOctahedron = 18,
        PlatonicTetrahedron = 19,
        Prism3 = 20,
        Prism6 = 21,
        Pyramid2 = 22,
        Pyramid3 = 23,
        Pyramid4 = 24,
        Pyramid6 = 25,
        SawtoothRippleBox = 26,
        SawtoothRippleGauss = 27,
        SawtoothRippleLorentz = 28,
        TruncatedCube = 29,
        SphericalSegment = 30,
        SpheroidalSegment = 31
    };

    //! Creates the item of the given type.
    static FormfactorItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    static QVector<Type> hardParticleTypes();
    static QVector<Type> rippleTypes();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const FormfactorItem* item);

    static QString menuEntry(const FormfactorItem* item);
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_FORMFACTORCATALOG_H
