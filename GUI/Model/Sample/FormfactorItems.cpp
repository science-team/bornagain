//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/FormfactorItems.cpp
//! @brief     Implements class FormfactorItemses.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/FormfactorItems.h"
#include "Base/Const/Units.h"
#include "GUI/Model/Util/UtilXML.h"
#include "Sample/HardParticle/HardParticles.h"

namespace {
namespace Tag {

const QString Alpha("Alpha");
const QString Asymmetry("Asymmetry");
const QString BaseEdge("BaseEdge");
const QString BaseHeight("BaseHeight");
const QString Edge("Edge");
const QString Height("Height");
const QString HeightFlattening("HeightFlattening");
const QString HeightRatio("HeightRatio");
const QString Length("Length");
const QString Radius("Radius");
const QString RadiusX("RadiusX");
const QString RadiusY("RadiusY");
const QString RadiusZ("RadiusZ");
const QString RadiusXY("RadiusXY");
const QString RemovedLength("RemovedLength");
const QString cutFromTop("cutFromTop");
const QString cutFromBottom("cutFromBottom");
const QString SliceBottom("SliceBottom");
const QString SliceTop("SliceTop");
const QString Width("Width");

} // namespace Tag
} // namespace

/* ------------------------------------------------ */

Pyramid2Item::Pyramid2Item()
{
    m_length.init("Length", "nm", "Length of the rectangular base", 16.0, "length");
    m_width.init("Width", "nm", "Width of the rectangular base", 16.0, "width");
    m_height.init("Height", "nm", "Height of pyramid", 16.0, "height");
    m_alpha.init("Alpha", "deg", "Dihedral angle between base and facet", 80.0, 2, 0.1,
                 RealLimits::limited(0.0, 90.0), "alpha");
}

std::unique_ptr<IFormfactor> Pyramid2Item::createFormfactor() const
{
    return std::make_unique<Pyramid2>(m_length.dVal(), m_width.dVal(), m_height.dVal(),
                                      m_alpha.dVal() * Units::deg);
}

void Pyramid2Item::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_width.writeTo2(w, Tag::Width);
    m_height.writeTo2(w, Tag::Height);
    m_alpha.writeTo2(w, Tag::Alpha);
}

void Pyramid2Item::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::Width)
            m_width.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else if (tag == Tag::Alpha)
            m_alpha.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

BarGaussItem::BarGaussItem()
{
    m_length.init("Length", "nm", "Length of the base", 16.0, "length");
    m_width.init("Width", "nm", "Width of the base", 16.0, "width");
    m_height.init("Height", "nm", "Height of the box", 16.0, "height");
}

std::unique_ptr<IFormfactor> BarGaussItem::createFormfactor() const
{
    return std::make_unique<BarGauss>(m_length.dVal(), m_width.dVal(), m_height.dVal());
}

void BarGaussItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_width.writeTo2(w, Tag::Width);
    m_height.writeTo2(w, Tag::Height);
}

void BarGaussItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::Width)
            m_width.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

BarLorentzItem::BarLorentzItem()
{
    m_length.init("Length", "nm", "Length of the base", 16.0, "length");
    m_width.init("Width", "nm", "Width of the base", 16.0, "width");
    m_height.init("Height", "nm", "Height of the box", 16.0, "height");
}

std::unique_ptr<IFormfactor> BarLorentzItem::createFormfactor() const
{
    return std::make_unique<BarLorentz>(m_length.dVal(), m_width.dVal(), m_height.dVal());
}

void BarLorentzItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_width.writeTo2(w, Tag::Width);
    m_height.writeTo2(w, Tag::Height);
}

void BarLorentzItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::Width)
            m_width.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

BoxItem::BoxItem()
{
    m_length.init("Length", "nm", "Length of the base", 16.0, "length");
    m_width.init("Width", "nm", "Width of the base", 16.0, "width");
    m_height.init("Height", "nm", "Height of the box", 16.0, "height");
}

std::unique_ptr<IFormfactor> BoxItem::createFormfactor() const
{
    return std::make_unique<Box>(m_length.dVal(), m_width.dVal(), m_height.dVal());
}

void BoxItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_width.writeTo2(w, Tag::Width);
    m_height.writeTo2(w, Tag::Height);
}

void BoxItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::Width)
            m_width.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

ConeItem::ConeItem()
{
    m_radius.init("Radius", "nm", "Radius of the base", 8.0, "radius");
    m_height.init("Height", "nm", "Height of the cone", 16.0, "height");
    m_alpha.init("Alpha", "deg", "Angle between the base and the side surface", 65.0, 2, 0.1,
                 RealLimits::limited(0.0, 90.0), "alpha");
}

std::unique_ptr<IFormfactor> ConeItem::createFormfactor() const
{
    return std::make_unique<Cone>(m_radius.dVal(), m_height.dVal(), m_alpha.dVal() * Units::deg);
}

void ConeItem::writeTo(QXmlStreamWriter* w) const
{
    m_radius.writeTo2(w, Tag::Radius);
    m_height.writeTo2(w, Tag::Height);
    m_alpha.writeTo2(w, Tag::Alpha);
}

void ConeItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Radius)
            m_radius.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else if (tag == Tag::Alpha)
            m_alpha.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

Pyramid6Item::Pyramid6Item()
{
    m_base_edge.init("Base edge", "nm", "Edge of the regular hexagonal base", 8.0, "baseEdge");
    m_height.init("Height", "nm", "Height of a truncated pyramid", 16.0, "height");
    m_alpha.init("Alpha", "deg", "Dihedral angle between base and facet", 70.0, 2, 0.1,
                 RealLimits::limited(0.0, 90.0), "alpha");
}

std::unique_ptr<IFormfactor> Pyramid6Item::createFormfactor() const
{
    return std::make_unique<Pyramid6>(m_base_edge.dVal(), m_height.dVal(),
                                      m_alpha.dVal() * Units::deg);
}

void Pyramid6Item::writeTo(QXmlStreamWriter* w) const
{
    m_base_edge.writeTo2(w, Tag::BaseEdge);
    m_height.writeTo2(w, Tag::Height);
    m_alpha.writeTo2(w, Tag::Alpha);
}

void Pyramid6Item::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::BaseEdge)
            m_base_edge.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else if (tag == Tag::Alpha)
            m_alpha.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

Bipyramid4Item::Bipyramid4Item()
{
    m_length.init("Length", "nm", "Side length of the common square base", 12.0, "length");

    m_base_height.init("Base height", "nm", "Height of the lower pyramid", 16.0, "base_height");

    m_height_ratio.init("Height ratio", "", "Ratio of heights of top to bottom pyramids", 0.7, 3,
                        RealLimits::lowerLimited(0.0), "heightRatio");

    m_alpha.init("Alpha", "deg", "Dihedral angle between base and facets", 70.0, 2, 0.1,
                 RealLimits::limited(0.0, 90.0), "alpha");
}

std::unique_ptr<IFormfactor> Bipyramid4Item::createFormfactor() const
{
    return std::make_unique<Bipyramid4>(m_length.dVal(), m_base_height.dVal(),
                                        m_height_ratio.dVal(), m_alpha.dVal() * Units::deg);
}

void Bipyramid4Item::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_base_height.writeTo2(w, Tag::BaseHeight);
    m_height_ratio.writeTo2(w, Tag::HeightRatio);
    m_alpha.writeTo2(w, Tag::Alpha);
}

void Bipyramid4Item::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::BaseHeight)
            m_base_height.readFrom2(r, tag);
        else if (tag == Tag::HeightRatio)
            m_height_ratio.readFrom2(r, tag);
        else if (tag == Tag::Alpha)
            m_alpha.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

CylinderItem::CylinderItem()
{
    m_radius.init("Radius", "nm", "Radius of the circular base", 8.0, "radius");
    m_height.init("Height", "nm", "Height of the cylinder", 16.0, "height");
}

std::unique_ptr<IFormfactor> CylinderItem::createFormfactor() const
{
    return std::make_unique<Cylinder>(m_radius.dVal(), m_height.dVal());
}

void CylinderItem::writeTo(QXmlStreamWriter* w) const
{
    m_radius.writeTo2(w, Tag::Radius);
    m_height.writeTo2(w, Tag::Height);
}

void CylinderItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Radius)
            m_radius.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

EllipsoidalCylinderItem::EllipsoidalCylinderItem()
{
    m_radiusX.init("Radius x", "nm", "Radius of the ellipse base in the x-direction", 5.0,
                   "radiusX");
    m_radiusY.init("Radius y", "nm", "Radius of the ellipse base in the y-direction", 8.0,
                   "radiusY");
    m_height.init("Height", "nm", "Height of the ellipsoidal cylinder", 16.0, "height");
}

std::unique_ptr<IFormfactor> EllipsoidalCylinderItem::createFormfactor() const
{
    return std::make_unique<EllipsoidalCylinder>(m_radiusX.dVal(), m_radiusY.dVal(),
                                                 m_height.dVal());
}

void EllipsoidalCylinderItem::writeTo(QXmlStreamWriter* w) const
{
    m_radiusX.writeTo2(w, Tag::RadiusX);
    m_radiusY.writeTo2(w, Tag::RadiusY);
    m_height.writeTo2(w, Tag::Height);
}

void EllipsoidalCylinderItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::RadiusX)
            m_radiusX.readFrom2(r, tag);
        else if (tag == Tag::RadiusY)
            m_radiusY.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

SphereItem::SphereItem()
{
    m_radius.init("Radius", "nm", "Radius of the sphere", 8.0, "radius");
}

std::unique_ptr<IFormfactor> SphereItem::createFormfactor() const
{
    return std::make_unique<Sphere>(m_radius.dVal());
}

void SphereItem::writeTo(QXmlStreamWriter* w) const
{
    m_radius.writeTo2(w, Tag::Radius);
}

void SphereItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Radius)
            m_radius.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

SpheroidItem::SpheroidItem()
{
    m_radius_xy.init("Radius XY", "nm", "Horizontal radius of the spheroid", 8.0, "radiusXY");
    m_radius_z.init("Radius Z", "nm", "Vertical radius of the spheroid", 5.0, "radiusZ");
}

std::unique_ptr<IFormfactor> SpheroidItem::createFormfactor() const
{
    return std::make_unique<Spheroid>(m_radius_xy.dVal(), m_radius_z.dVal());
}

void SpheroidItem::writeTo(QXmlStreamWriter* w) const
{
    m_radius_xy.writeTo2(w, Tag::RadiusXY);
    m_radius_z.writeTo2(w, Tag::RadiusZ);
}

void SpheroidItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::RadiusXY)
            m_radius_xy.readFrom2(r, tag);
        else if (tag == Tag::RadiusZ)
            m_radius_z.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

HemiEllipsoidItem::HemiEllipsoidItem()
{
    m_radiusX.init("Radius x", "nm", "Radius of the ellipse base in the x-direction", 8.0,
                   "radiusX");
    m_radiusY.init("Radius y", "nm", "Radius of the ellipse base in the y-direction", 6.0,
                   "radiusY");
    m_height.init("Height", "nm", "Height of the hemi ellipsoid", 16.0, "height");
}

std::unique_ptr<IFormfactor> HemiEllipsoidItem::createFormfactor() const
{
    return std::make_unique<HemiEllipsoid>(m_radiusX.dVal(), m_radiusY.dVal(), m_height.dVal());
}

void HemiEllipsoidItem::writeTo(QXmlStreamWriter* w) const
{
    m_radiusX.writeTo2(w, Tag::RadiusX);
    m_radiusY.writeTo2(w, Tag::RadiusY);
    m_height.writeTo2(w, Tag::Height);
}

void HemiEllipsoidItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::RadiusX)
            m_radiusX.readFrom2(r, tag);
        else if (tag == Tag::RadiusY)
            m_radiusY.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

Prism3Item::Prism3Item()
{
    m_base_edge.init("Base edge", "nm", "Length of the base edge", 14.0, "baseEdge");
    m_height.init("Height", "nm", "Height", 16.0, "height");
}

std::unique_ptr<IFormfactor> Prism3Item::createFormfactor() const
{
    return std::make_unique<Prism3>(m_base_edge.dVal(), m_height.dVal());
}

void Prism3Item::writeTo(QXmlStreamWriter* w) const
{
    m_base_edge.writeTo2(w, Tag::BaseEdge);
    m_height.writeTo2(w, Tag::Height);
}

void Prism3Item::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::BaseEdge)
            m_base_edge.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

Prism6Item::Prism6Item()
{
    m_base_edge.init("Base edge", "nm", "Length of the hexagonal base", 8.0, "baseEdge");
    m_height.init("Height", "nm", "Height", 16.0, "height");
}

std::unique_ptr<IFormfactor> Prism6Item::createFormfactor() const
{
    return std::make_unique<Prism6>(m_base_edge.dVal(), m_height.dVal());
}

void Prism6Item::writeTo(QXmlStreamWriter* w) const
{
    m_base_edge.writeTo2(w, Tag::BaseEdge);
    m_height.writeTo2(w, Tag::Height);
}

void Prism6Item::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::BaseEdge)
            m_base_edge.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

Pyramid4Item::Pyramid4Item()
{
    m_base_edge.init("Base edge", "nm", "Length of the square base", 16.0, "baseEdge");
    m_height.init("Height", "nm", "Height of the pyramid", 16.0, "height");
    m_alpha.init("Alpha", "deg", "Dihedral angle between the base and a side face", 65.0, 2, 0.1,
                 RealLimits::limited(0.0, 90.0), "alpha");
}

std::unique_ptr<IFormfactor> Pyramid4Item::createFormfactor() const
{
    return std::make_unique<Pyramid4>(m_base_edge.dVal(), m_height.dVal(),
                                      m_alpha.dVal() * Units::deg);
}

void Pyramid4Item::writeTo(QXmlStreamWriter* w) const
{
    m_base_edge.writeTo2(w, Tag::BaseEdge);
    m_height.writeTo2(w, Tag::Height);
    m_alpha.writeTo2(w, Tag::Alpha);
}

void Pyramid4Item::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::BaseEdge)
            m_base_edge.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else if (tag == Tag::Alpha)
            m_alpha.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

CosineRippleBoxItem::CosineRippleBoxItem()
{
    m_length.init("Length", "nm", "Length of the rectangular base", 16.0, "length");
    m_width.init("Width", "nm", "Width of the rectangular base", 16.0, "width");
    m_height.init("Height", "nm", "Height of the ripple", 16.0, "height");
}

std::unique_ptr<IFormfactor> CosineRippleBoxItem::createFormfactor() const
{
    return std::make_unique<CosineRippleBox>(m_length.dVal(), m_width.dVal(), m_height.dVal());
}

void CosineRippleBoxItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_width.writeTo2(w, Tag::Width);
    m_height.writeTo2(w, Tag::Height);
}

void CosineRippleBoxItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::Width)
            m_width.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

CosineRippleGaussItem::CosineRippleGaussItem()
{
    m_length.init("Length", "nm", "Length of the rectangular base", 16.0, "length");
    m_width.init("Width", "nm", "Width of the rectangular base", 16.0, "width");
    m_height.init("Height", "nm", "Height of the ripple", 16.0, "height");
}

std::unique_ptr<IFormfactor> CosineRippleGaussItem::createFormfactor() const
{
    return std::make_unique<CosineRippleGauss>(m_length.dVal(), m_width.dVal(), m_height.dVal());
}

void CosineRippleGaussItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_width.writeTo2(w, Tag::Width);
    m_height.writeTo2(w, Tag::Height);
}

void CosineRippleGaussItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::Width)
            m_width.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

CosineRippleLorentzItem::CosineRippleLorentzItem()
{
    m_length.init("Length", "nm", "Length of the rectangular base", 16.0, "length");
    m_width.init("Width", "nm", "Width of the rectangular base", 16.0, "width");
    m_height.init("Height", "nm", "Height of the ripple", 16.0, "height");
}

std::unique_ptr<IFormfactor> CosineRippleLorentzItem::createFormfactor() const
{
    return std::make_unique<CosineRippleLorentz>(m_length.dVal(), m_width.dVal(), m_height.dVal());
}

void CosineRippleLorentzItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_width.writeTo2(w, Tag::Width);
    m_height.writeTo2(w, Tag::Height);
}

void CosineRippleLorentzItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::Width)
            m_width.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

SawtoothRippleBoxItem::SawtoothRippleBoxItem()
{
    m_length.init("Length", "nm", "Length of the rectangular base", 16.0, "length");
    m_width.init("Width", "nm", "Width of the rectangular base", 16.0, "width");
    m_height.init("Height", "nm", "Height of the ripple", 16.0, "height");
    m_asymmetry.init("Asymmetry", "nm", "Asymmetry length of the triangular profile", 3.0,
                     "asymmetry");
}

std::unique_ptr<IFormfactor> SawtoothRippleBoxItem::createFormfactor() const
{
    return std::make_unique<SawtoothRippleBox>(m_length.dVal(), m_width.dVal(), m_height.dVal(),
                                               m_asymmetry.dVal());
}

void SawtoothRippleBoxItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_width.writeTo2(w, Tag::Width);
    m_height.writeTo2(w, Tag::Height);
    m_asymmetry.writeTo2(w, Tag::Asymmetry);
}

void SawtoothRippleBoxItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::Width)
            m_width.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else if (tag == Tag::Asymmetry)
            m_asymmetry.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

SawtoothRippleGaussItem::SawtoothRippleGaussItem()
{
    m_length.init("Length", "nm", "Length of the rectangular base", 16.0, "length");
    m_width.init("Width", "nm", "Width of the rectangular base", 16.0, "width");
    m_height.init("Height", "nm", "Height of the ripple", 16.0, "height");
    m_asymmetry.init("Asymmetry", "nm", "Asymmetry length of the triangular profile", 3.0,
                     "asymmetry");
}

std::unique_ptr<IFormfactor> SawtoothRippleGaussItem::createFormfactor() const
{
    return std::make_unique<SawtoothRippleGauss>(m_length.dVal(), m_width.dVal(), m_height.dVal(),
                                                 m_asymmetry.dVal());
}

void SawtoothRippleGaussItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_width.writeTo2(w, Tag::Width);
    m_height.writeTo2(w, Tag::Height);
    m_asymmetry.writeTo2(w, Tag::Asymmetry);
}

void SawtoothRippleGaussItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::Width)
            m_width.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else if (tag == Tag::Asymmetry)
            m_asymmetry.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

SawtoothRippleLorentzItem::SawtoothRippleLorentzItem()
{
    m_length.init("Length", "nm", "Length of the rectangular base", 16.0, "length");
    m_width.init("Width", "nm", "Width of the rectangular base", 16.0, "width");
    m_height.init("Height", "nm", "Height of the ripple", 16.0, "height");
    m_asymmetry.init("Asymmetry", "nm", "Asymmetry length of the triangular profile", 3.0,
                     "asymmetry");
}

std::unique_ptr<IFormfactor> SawtoothRippleLorentzItem::createFormfactor() const
{
    return std::make_unique<SawtoothRippleLorentz>(m_length.dVal(), m_width.dVal(), m_height.dVal(),
                                                   m_asymmetry.dVal());
}

void SawtoothRippleLorentzItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_width.writeTo2(w, Tag::Width);
    m_height.writeTo2(w, Tag::Height);
    m_asymmetry.writeTo2(w, Tag::Asymmetry);
}

void SawtoothRippleLorentzItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::Width)
            m_width.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else if (tag == Tag::Asymmetry)
            m_asymmetry.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

Pyramid3Item::Pyramid3Item()
{
    m_base_edge.init("Base edge", "nm", "Length of one edge of the equilateral triangular base",
                     14.0, "baseEdge");
    m_height.init("Height", "nm", "Height of the tetrahedron", 16.0, "height");
    m_alpha.init("Alpha", "deg", "Dihedral angle between base and facet", 80.0, 2, 0.1,
                 RealLimits::limited(0.0, 90.0), "alpha");
}

std::unique_ptr<IFormfactor> Pyramid3Item::createFormfactor() const
{
    return std::make_unique<Pyramid3>(m_base_edge.dVal(), m_height.dVal(),
                                      m_alpha.dVal() * Units::deg);
}

void Pyramid3Item::writeTo(QXmlStreamWriter* w) const
{
    m_base_edge.writeTo2(w, Tag::BaseEdge);
    m_height.writeTo2(w, Tag::Height);
    m_alpha.writeTo2(w, Tag::Alpha);
}

void Pyramid3Item::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::BaseEdge)
            m_base_edge.readFrom2(r, tag);
        else if (tag == Tag::Height)
            m_height.readFrom2(r, tag);
        else if (tag == Tag::Alpha)
            m_alpha.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

TruncatedCubeItem::TruncatedCubeItem()
{
    m_length.init("Length", "nm", "Length of the full cube's edge", 16.0, "length");
    m_removed_length.init("Removed length", "nm", "Removed length from each corner of the cube",
                          4.0, "removedLength");
}

std::unique_ptr<IFormfactor> TruncatedCubeItem::createFormfactor() const
{
    return std::make_unique<TruncatedCube>(m_length.dVal(), m_removed_length.dVal());
}

void TruncatedCubeItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_removed_length.writeTo2(w, Tag::RemovedLength);
}

void TruncatedCubeItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::RemovedLength)
            m_removed_length.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

SphericalSegmentItem::SphericalSegmentItem()
{
    m_radius.init("Radius", "nm", "Radius of the spherical segment", 8.0, "radius");
    m_removed_top.init("Removed top", "nm", "Height of the removed top cap", 0.0, "cutFromTop");
    m_removed_bottom.init("Removed bottom", "nm", "Height of the removed bottom cap", 0.0,
                          "cutFromBottom");
}

std::unique_ptr<IFormfactor> SphericalSegmentItem::createFormfactor() const
{
    return std::make_unique<SphericalSegment>(m_radius.dVal(), m_removed_top.dVal(),
                                              m_removed_bottom.dVal());
}

void SphericalSegmentItem::writeTo(QXmlStreamWriter* w) const
{
    m_radius.writeTo2(w, Tag::Radius);
    m_removed_top.writeTo2(w, Tag::cutFromTop);
    m_removed_bottom.writeTo2(w, Tag::cutFromBottom);
}

void SphericalSegmentItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Radius)
            m_radius.readFrom2(r, tag);
        else if (tag == Tag::cutFromTop)
            m_removed_top.readFrom2(r, tag);
        else if (tag == Tag::cutFromBottom)
            m_removed_bottom.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

SpheroidalSegmentItem::SpheroidalSegmentItem()
{
    m_radius_xy.init("Radius XY", "nm", "Horizontal radius of the truncated spheroid", 8.0,
                     "radiusXY");
    m_radius_z.init("Radius Z", "nm", "Vertical radius of the truncated spheroid", 10.0, "radiusZ");

    m_removed_top.init("Removed top", "nm", "Height of the removed top cap", 0.0, "cutFromTop");
    m_removed_bottom.init("Removed bottom", "nm", "Height of the removed bottom cap", 0.0,
                          "cutFromBottom");
}

std::unique_ptr<IFormfactor> SpheroidalSegmentItem::createFormfactor() const
{
    return std::make_unique<SpheroidalSegment>(m_radius_xy.dVal(), m_radius_z.dVal(),
                                               m_removed_top.dVal(), m_removed_bottom.dVal());
}

void SpheroidalSegmentItem::writeTo(QXmlStreamWriter* w) const
{
    m_radius_xy.writeTo2(w, Tag::RadiusXY);
    m_radius_z.writeTo2(w, Tag::RadiusZ);
    m_removed_top.writeTo2(w, Tag::cutFromTop);
    m_removed_bottom.writeTo2(w, Tag::cutFromBottom);
}

void SpheroidalSegmentItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::RadiusXY)
            m_radius_xy.readFrom2(r, tag);
        else if (tag == Tag::RadiusZ)
            m_radius_z.readFrom2(r, tag);
        else if (tag == Tag::cutFromTop)
            m_removed_top.readFrom2(r, tag);
        else if (tag == Tag::cutFromBottom)
            m_removed_bottom.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

CantellatedCubeItem::CantellatedCubeItem()
{
    m_length.init("Length", "nm", "Length of the full cube's edge", 16.0, "length");
    m_removed_length.init("Removed length", "nm",
                          "Removed length from each edge and vertice of the cube", 4.0,
                          "removedLength");
}

std::unique_ptr<IFormfactor> CantellatedCubeItem::createFormfactor() const
{
    return std::make_unique<CantellatedCube>(m_length.dVal(), m_removed_length.dVal());
}

void CantellatedCubeItem::writeTo(QXmlStreamWriter* w) const
{
    m_length.writeTo2(w, Tag::Length);
    m_removed_length.writeTo2(w, Tag::RemovedLength);
}

void CantellatedCubeItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::RemovedLength)
            m_removed_length.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

HorizontalCylinderItem::HorizontalCylinderItem()
{
    m_radius.init("Radius", "nm", "Radius of the horizontal cylinder", 8.0, "radius");
    m_length.init("Length", "nm", "Length of the horizontal cylinder", 16.0, "length");
    m_slice_bottom.init("Bottom boundary", "nm",
                        "Position of the lower boundary relative to the center", -4.1,
                        "sliceBottom");
    m_slice_top.init("Top boundary", "nm", "Position of the upper boundary relative to the center",
                     +5.2, "sliceTop");
}

std::unique_ptr<IFormfactor> HorizontalCylinderItem::createFormfactor() const
{
    return std::make_unique<HorizontalCylinder>(m_radius.dVal(), m_length.dVal(),
                                                m_slice_bottom.dVal(), m_slice_top.dVal());
}

void HorizontalCylinderItem::writeTo(QXmlStreamWriter* w) const
{
    m_radius.writeTo2(w, Tag::Radius);
    m_length.writeTo2(w, Tag::Length);
    m_slice_bottom.writeTo2(w, Tag::SliceBottom);
    m_slice_top.writeTo2(w, Tag::SliceTop);
}

void HorizontalCylinderItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Radius)
            m_radius.readFrom2(r, tag);
        else if (tag == Tag::Length)
            m_length.readFrom2(r, tag);
        else if (tag == Tag::SliceBottom)
            m_slice_bottom.readFrom2(r, tag);
        else if (tag == Tag::SliceTop)
            m_slice_top.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

/* ------------------------------------------------ */

void PlatonicItem::writeTo(QXmlStreamWriter* w) const
{
    m_edge.writeTo2(w, Tag::Edge);
}

void PlatonicItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Edge)
            m_edge.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

PlatonicOctahedronItem::PlatonicOctahedronItem()
{
    m_edge.init("Edge", "nm", "Length of the edge", 16.0, "edge");
}

std::unique_ptr<IFormfactor> PlatonicOctahedronItem::createFormfactor() const
{
    return std::make_unique<PlatonicOctahedron>(m_edge.dVal());
}

/* ------------------------------------------------ */

PlatonicTetrahedronItem::PlatonicTetrahedronItem()
{
    m_edge.init("Edge", "nm", "Length of the edge", 20.0, "edge");
}

std::unique_ptr<IFormfactor> PlatonicTetrahedronItem::createFormfactor() const
{
    return std::make_unique<PlatonicTetrahedron>(m_edge.dVal());
}

/* ------------------------------------------------ */

DodecahedronItem::DodecahedronItem()
{
    m_edge.init("Edge", "nm", "Length of the edge", 7.0, "edge");
}

std::unique_ptr<IFormfactor> DodecahedronItem::createFormfactor() const
{
    return std::make_unique<Dodecahedron>(m_edge.dVal());
}

/* ------------------------------------------------ */

IcosahedronItem::IcosahedronItem()
{
    m_edge.init("Edge", "nm", "Length of the edge", 10.0, "edge");
}

std::unique_ptr<IFormfactor> IcosahedronItem::createFormfactor() const
{
    return std::make_unique<Icosahedron>(m_edge.dVal());
}
