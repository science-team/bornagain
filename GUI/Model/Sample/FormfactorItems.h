//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/FormfactorItems.h
//! @brief     Defines class FormfactorItemses.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_FORMFACTORITEMS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_FORMFACTORITEMS_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include <memory>

class IFormfactor;

class FormfactorItem {
public:
    virtual ~FormfactorItem() = default;

public:
    virtual std::unique_ptr<IFormfactor> createFormfactor() const = 0;
    virtual DoubleProperties geometryProperties() = 0;

    virtual void writeTo(QXmlStreamWriter* w) const = 0;
    virtual void readFrom(QXmlStreamReader* r) = 0;
};

class Pyramid2Item : public FormfactorItem {
public:
    Pyramid2Item();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setWidth(double v) { m_width.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }
    void setAlpha(double v) { m_alpha.setDVal(v); }

    DoubleProperties geometryProperties() override
    {
        return {&m_length, &m_width, &m_height, &m_alpha};
    }
    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_width;
    DoubleProperty m_height;
    DoubleProperty m_alpha;
};

class BarGaussItem : public FormfactorItem {
public:
    BarGaussItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setWidth(double v) { m_width.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_length, &m_width, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_width;
    DoubleProperty m_height;
};

class BarLorentzItem : public FormfactorItem {
public:
    BarLorentzItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setWidth(double v) { m_width.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_length, &m_width, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_width;
    DoubleProperty m_height;
};

class BoxItem : public FormfactorItem {
public:
    BoxItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setWidth(double v) { m_width.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_length, &m_width, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_width;
    DoubleProperty m_height;
};

class ConeItem : public FormfactorItem {
public:
    ConeItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setRadius(double v) { m_radius.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }
    void setAlpha(double v) { m_alpha.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_radius, &m_height, &m_alpha}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_radius;
    DoubleProperty m_height;
    DoubleProperty m_alpha;
};

class Pyramid6Item : public FormfactorItem {
public:
    Pyramid6Item();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setBaseEdge(double v) { m_base_edge.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }
    void setAlpha(double v) { m_alpha.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_base_edge, &m_height, &m_alpha}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_base_edge;
    DoubleProperty m_height;
    DoubleProperty m_alpha;
};

class Bipyramid4Item : public FormfactorItem {
public:
    Bipyramid4Item();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setBaseHeight(double v) { m_base_height.setDVal(v); }
    void setHeightRatio(double v) { m_height_ratio.setDVal(v); }
    void setAlpha(double v) { m_alpha.setDVal(v); }

    DoubleProperties geometryProperties() override
    {
        return {&m_length, &m_base_height, &m_height_ratio, &m_alpha};
    }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_base_height;
    DoubleProperty m_height_ratio;
    DoubleProperty m_alpha;
};

class CylinderItem : public FormfactorItem {
public:
    CylinderItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setRadius(double v) { m_radius.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_radius, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_radius;
    DoubleProperty m_height;
};

class EllipsoidalCylinderItem : public FormfactorItem {
public:
    EllipsoidalCylinderItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setRadiusX(double v) { m_radiusX.setDVal(v); }
    void setRadiusY(double v) { m_radiusY.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_radiusX, &m_radiusY, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_radiusX;
    DoubleProperty m_radiusY;
    DoubleProperty m_height;
};

class SphereItem : public FormfactorItem {
public:
    SphereItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setRadius(double v) { m_radius.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_radius}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_radius;
};

class SpheroidItem : public FormfactorItem {
public:
    SpheroidItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setRadiusXY(double v) { m_radius_xy.setDVal(v); }
    void setRadiusZ(double v) { m_radius_z.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_radius_xy, &m_radius_z}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_radius_xy;
    DoubleProperty m_radius_z;
};

class HemiEllipsoidItem : public FormfactorItem {
public:
    HemiEllipsoidItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setRadiusX(double v) { m_radiusX.setDVal(v); }
    void setRadiusY(double v) { m_radiusY.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_radiusX, &m_radiusY, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_radiusX;
    DoubleProperty m_radiusY;
    DoubleProperty m_height;
};

class Prism3Item : public FormfactorItem {
public:
    Prism3Item();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setBaseEdge(double v) { m_base_edge.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_base_edge, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_base_edge;
    DoubleProperty m_height;
};

class Prism6Item : public FormfactorItem {
public:
    Prism6Item();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setBaseEdge(double v) { m_base_edge.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_base_edge, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_base_edge;
    DoubleProperty m_height;
};

class Pyramid4Item : public FormfactorItem {
public:
    Pyramid4Item();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setBaseEdge(double v) { m_base_edge.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }
    void setAlpha(double v) { m_alpha.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_base_edge, &m_height, &m_alpha}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_base_edge;
    DoubleProperty m_height;
    DoubleProperty m_alpha;
};

class CosineRippleBoxItem : public FormfactorItem {
public:
    CosineRippleBoxItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setWidth(double v) { m_width.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_length, &m_width, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_width;
    DoubleProperty m_height;
};

class CosineRippleGaussItem : public FormfactorItem {
public:
    CosineRippleGaussItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setWidth(double v) { m_width.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_length, &m_width, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_width;
    DoubleProperty m_height;
};

class CosineRippleLorentzItem : public FormfactorItem {
public:
    CosineRippleLorentzItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setWidth(double v) { m_width.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_length, &m_width, &m_height}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_width;
    DoubleProperty m_height;
};

class SawtoothRippleBoxItem : public FormfactorItem {
public:
    SawtoothRippleBoxItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setWidth(double v) { m_width.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }
    void setAsymmetry(double v) { m_asymmetry.setDVal(v); }

    DoubleProperties geometryProperties() override
    {
        return {&m_length, &m_width, &m_height, &m_asymmetry};
    }
    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_width;
    DoubleProperty m_height;
    DoubleProperty m_asymmetry;
};

class SawtoothRippleGaussItem : public FormfactorItem {
public:
    SawtoothRippleGaussItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setWidth(double v) { m_width.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }
    void setAsymmetry(double v) { m_asymmetry.setDVal(v); }

    DoubleProperties geometryProperties() override
    {
        return {&m_length, &m_width, &m_height, &m_asymmetry};
    }
    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_width;
    DoubleProperty m_height;
    DoubleProperty m_asymmetry;
};

class SawtoothRippleLorentzItem : public FormfactorItem {
public:
    SawtoothRippleLorentzItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setWidth(double v) { m_width.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }
    void setAsymmetry(double v) { m_asymmetry.setDVal(v); }

    DoubleProperties geometryProperties() override
    {
        return {&m_length, &m_width, &m_height, &m_asymmetry};
    }
    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_width;
    DoubleProperty m_height;
    DoubleProperty m_asymmetry;
};

class Pyramid3Item : public FormfactorItem {
public:
    Pyramid3Item();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setBaseEdge(double v) { m_base_edge.setDVal(v); }
    void setHeight(double v) { m_height.setDVal(v); }
    void setAlpha(double v) { m_alpha.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_base_edge, &m_height, &m_alpha}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_base_edge;
    DoubleProperty m_height;
    DoubleProperty m_alpha;
};

class TruncatedCubeItem : public FormfactorItem {
public:
    TruncatedCubeItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setRemovedLength(double v) { m_removed_length.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_length, &m_removed_length}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_removed_length;
};

class SphericalSegmentItem : public FormfactorItem {
public:
    SphericalSegmentItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setRadius(double v) { m_radius.setDVal(v); }
    void setcutFromTop(double v) { m_removed_top.setDVal(v); }
    void setcutFromBottom(double v) { m_removed_bottom.setDVal(v); }

    DoubleProperties geometryProperties() override
    {
        return {&m_radius, &m_removed_top, &m_removed_bottom};
    }
    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_radius;
    DoubleProperty m_removed_top;
    DoubleProperty m_removed_bottom;
};

class SpheroidalSegmentItem : public FormfactorItem {
public:
    SpheroidalSegmentItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setRadiusXY(double v) { m_radius_xy.setDVal(v); }
    void setRadiusZ(double v) { m_radius_z.setDVal(v); }
    void setcutFromTop(double v) { m_removed_top.setDVal(v); }
    void setcutFromBottom(double v) { m_removed_bottom.setDVal(v); }

    DoubleProperties geometryProperties() override
    {
        return {&m_radius_xy, &m_radius_z, &m_removed_top, &m_removed_bottom};
    }
    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_radius_xy;
    DoubleProperty m_radius_z;
    DoubleProperty m_removed_top;
    DoubleProperty m_removed_bottom;
};

class CantellatedCubeItem : public FormfactorItem {
public:
    CantellatedCubeItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setLength(double v) { m_length.setDVal(v); }
    void setRemovedLength(double v) { m_removed_length.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_length, &m_removed_length}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_length;
    DoubleProperty m_removed_length;
};

class HorizontalCylinderItem : public FormfactorItem {
public:
    HorizontalCylinderItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;

    void setRadius(double v) { m_radius.setDVal(v); }
    void setLength(double v) { m_length.setDVal(v); }
    void setSliceBottom(double v) { m_slice_bottom.setDVal(v); }
    void setSliceTop(double v) { m_slice_top.setDVal(v); }

    DoubleProperties geometryProperties() override
    {
        return {&m_radius, &m_length, &m_slice_bottom, &m_slice_top};
    }
    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_radius;
    DoubleProperty m_length;
    DoubleProperty m_slice_bottom;
    DoubleProperty m_slice_top;
};

// Platonic solids

class PlatonicItem : public FormfactorItem {
public:
    void setEdge(double v) { m_edge.setDVal(v); }

    DoubleProperties geometryProperties() override { return {&m_edge}; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    DoubleProperty m_edge;
};

class PlatonicOctahedronItem : public PlatonicItem {
public:
    PlatonicOctahedronItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;
};

class PlatonicTetrahedronItem : public PlatonicItem {
public:
    PlatonicTetrahedronItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;
};

class DodecahedronItem : public PlatonicItem {
public:
    DodecahedronItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;
};

class IcosahedronItem : public PlatonicItem {
public:
    IcosahedronItem();
    std::unique_ptr<IFormfactor> createFormfactor() const override;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_FORMFACTORITEMS_H
