//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/InterferenceCatalog.cpp
//! @brief     Implements class InterferenceCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/InterferenceCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/InterferenceItems.h"

InterferenceItem* InterferenceCatalog::create(Type type)
{
    switch (type) {
    case Type::Disorder:
        return nullptr;
    case Type::RadialParacrystalRadial:
        return new InterferenceRadialParacrystalItem;
    case Type::Paracrystal2D:
        return new Interference2DParacrystalItem;
    case Type::Lattice1D:
        return new Interference1DLatticeItem;
    case Type::Lattice2D:
        return new Interference2DLatticeItem;
    case Type::FiniteLattice2D:
        return new InterferenceFinite2DLatticeItem;
    case Type::HardDisk:
        return new InterferenceHardDiskItem;
    default:
        ASSERT_NEVER;
    }
}

QVector<InterferenceCatalog::Type> InterferenceCatalog::types()
{
    return {Type::Disorder,        Type::RadialParacrystalRadial, Type::Lattice1D, Type::Lattice2D,
            Type::FiniteLattice2D, Type::Paracrystal2D,           Type::HardDisk};
}

UiInfo InterferenceCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::Disorder:
        return {"Disorder", "", ""};
    case Type::RadialParacrystalRadial:
        return {"Radial paracrystal", "Interference function of radial paracrystal",
                ":/images/struct/Paracrystal1D.png"};
    case Type::Paracrystal2D:
        return {"2D paracrystal", "Interference function of two-dimensional paracrystal",
                ":/images/struct/Paracrystal2D.png"};
    case Type::Lattice1D:
        return {"1D lattice", "Interference function of 1D lattice",
                ":/images/struct/Lattice1D.png"};
    case Type::Lattice2D:
        return {"2D lattice", "Interference function of 2D lattice",
                ":/images/struct/Lattice2D.png"};
    case Type::FiniteLattice2D:
        return {"Finite 2D lattice", "Interference function of finite 2D lattice",
                ":/images/struct/Lattice2DFinite.png"};
    case Type::HardDisk:
        return {"Hard disk Percus-Yevick", "Interference function for hard disk Percus-Yevick",
                ":/images/struct/Lattice2D.png"};
    default:
        ASSERT_NEVER;
    }
}

InterferenceCatalog::Type InterferenceCatalog::type(const InterferenceItem* item)
{
    if (!item)
        return Type::Disorder;

    if (dynamic_cast<const InterferenceRadialParacrystalItem*>(item))
        return Type::RadialParacrystalRadial;
    if (dynamic_cast<const Interference2DParacrystalItem*>(item))
        return Type::Paracrystal2D;
    if (dynamic_cast<const Interference1DLatticeItem*>(item))
        return Type::Lattice1D;
    if (dynamic_cast<const Interference2DLatticeItem*>(item))
        return Type::Lattice2D;
    if (dynamic_cast<const InterferenceFinite2DLatticeItem*>(item))
        return Type::FiniteLattice2D;
    if (dynamic_cast<const InterferenceHardDiskItem*>(item))
        return Type::HardDisk;

    ASSERT_NEVER;
}
