//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/InterferenceCatalog.h
//! @brief     Defines class InterferenceCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_INTERFERENCECATALOG_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_INTERFERENCECATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class InterferenceItem;

class InterferenceCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t {
        Disorder = 0,
        RadialParacrystalRadial = 1,
        Paracrystal2D = 2,
        Lattice1D = 3,
        Lattice2D = 4,
        FiniteLattice2D = 5,
        HardDisk = 6
    };

    //! Creates the item of the given type.
    static InterferenceItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const InterferenceItem* item);
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_INTERFERENCECATALOG_H
