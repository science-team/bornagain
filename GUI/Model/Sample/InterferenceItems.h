//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/InterferenceItems.h
//! @brief     Defines class InterferenceItem and subclasses.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_INTERFERENCEITEMS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_INTERFERENCEITEMS_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/Model/Sample/Lattice2DCatalog.h"
#include "GUI/Model/Sample/Lattice2DItems.h"
#include "GUI/Model/Sample/ProfileCatalogs.h"
#include "GUI/Model/Sample/ProfileItems.h"
#include <memory>

class IInterference;

class InterferenceItem {
public:
    virtual ~InterferenceItem() = default;
    virtual std::unique_ptr<IInterference> createInterference() const = 0;

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);

    DoubleProperty& positionVariance() { return m_position_variance; }
    const DoubleProperty& positionVariance() const { return m_position_variance; }
    void setPositionVariance(double v) { m_position_variance.setDVal(v); }

protected:
    InterferenceItem();

    DoubleProperty m_position_variance;
};

// ------------------------------------------------------------------------------------------------

class Interference1DLatticeItem : public InterferenceItem {
public:
    Interference1DLatticeItem();

    std::unique_ptr<IInterference> createInterference() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    DoubleProperty& length() { return m_length; }
    const DoubleProperty& length() const { return m_length; }
    void setLength(double v) { m_length.setDVal(v); }

    DoubleProperty& rotationAngle() { return m_rotation_angle; }
    const DoubleProperty& rotationAngle() const { return m_rotation_angle; }
    void setRotationAngle(double v) { m_rotation_angle.setDVal(v); }

    PolyPtr<Profile1DItem, Profile1DCatalog>& decayFunctionSelection() { return m_decay_function; }
    void setDecayFunctionType(Profile1DItem* p) { m_decay_function.setCertainItem(p); }

private:
    DoubleProperty m_length;
    DoubleProperty m_rotation_angle;
    PolyPtr<Profile1DItem, Profile1DCatalog> m_decay_function;
};

// ------------------------------------------------------------------------------------------------

class Interference2DAbstractLatticeItem : public InterferenceItem {
public:
    Lattice2DItem* latticeTypeItem() const { return m_lattice_type.certainItem(); }
    PolyPtr<Lattice2DItem, Lattice2DCatalog>& latticeTypeSelection() { return m_lattice_type; }
    void setLatticeType(Lattice2DItem* p) { m_lattice_type.setCertainItem(p); }

    bool xiIntegration() const { return m_xi_integration; }
    void setXiIntegration(bool b) { m_xi_integration = b; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

protected:
    explicit Interference2DAbstractLatticeItem(bool xiIntegration);

    bool m_xi_integration;
    PolyPtr<Lattice2DItem, Lattice2DCatalog> m_lattice_type;
};

// ------------------------------------------------------------------------------------------------

class Interference2DLatticeItem : public Interference2DAbstractLatticeItem {
public:
    Interference2DLatticeItem();
    std::unique_ptr<IInterference> createInterference() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    PolyPtr<Profile2DItem, Profile2DCatalog>& decayFunctionSelection() { return m_decay_function; }
    void setDecayFunctionType(Profile2DItem* p) { m_decay_function.setCertainItem(p); }

protected:
    PolyPtr<Profile2DItem, Profile2DCatalog> m_decay_function;
};

// ------------------------------------------------------------------------------------------------

class Interference2DParacrystalItem : public Interference2DAbstractLatticeItem {
public:
    Interference2DParacrystalItem();
    std::unique_ptr<IInterference> createInterference() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    DoubleProperty& dampingLength() { return m_damping_length; }
    const DoubleProperty& dampingLength() const { return m_damping_length; }
    void setDampingLength(double dampingLength) { m_damping_length.setDVal(dampingLength); }

    DoubleProperty& domainSize1() { return m_domain_size1; }
    const DoubleProperty& domainSize1() const { return m_domain_size1; }
    void setDomainSize1(double size) { m_domain_size1.setDVal(size); }

    DoubleProperty& domainSize2() { return m_domain_size2; }
    const DoubleProperty& domainSize2() const { return m_domain_size2; }
    void setDomainSize2(double size) { m_domain_size2.setDVal(size); }

    PolyPtr<Profile2DItem, Profile2DCatalog>& probabilityDistributionSelection1() { return m_pdf1; }
    void setPDF1Type(Profile2DItem* p) { m_pdf1.setCertainItem(p); }

    PolyPtr<Profile2DItem, Profile2DCatalog>& probabilityDistributionSelection2() { return m_pdf2; }
    void setPDF2Type(Profile2DItem* p) { m_pdf2.setCertainItem(p); }

private:
    DoubleProperty m_damping_length;
    DoubleProperty m_domain_size1;
    DoubleProperty m_domain_size2;
    PolyPtr<Profile2DItem, Profile2DCatalog> m_pdf1;
    PolyPtr<Profile2DItem, Profile2DCatalog> m_pdf2;
};

// ------------------------------------------------------------------------------------------------

class InterferenceFinite2DLatticeItem : public Interference2DAbstractLatticeItem {
public:
    InterferenceFinite2DLatticeItem();
    std::unique_ptr<IInterference> createInterference() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    uint domainSize1() const { return m_domain_size1; }
    void setDomainSize1(uint v) { m_domain_size1 = v; }

    uint domainSize2() const { return m_domain_size2; }
    void setDomainSize2(uint v) { m_domain_size2 = v; }

private:
    uint m_domain_size1 = 100;
    uint m_domain_size2 = 100;
};

// ------------------------------------------------------------------------------------------------

class InterferenceHardDiskItem : public InterferenceItem {
public:
    InterferenceHardDiskItem();
    std::unique_ptr<IInterference> createInterference() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    DoubleProperty& radius() { return m_radius; }
    const DoubleProperty& radius() const { return m_radius; }
    void setRadius(double v) { m_radius.setDVal(v); }

    DoubleProperty& density() { return m_density; }
    const DoubleProperty& density() const { return m_density; }
    void setDensity(double v) { m_density.setDVal(v); }

private:
    DoubleProperty m_radius;
    DoubleProperty m_density;
};

// ------------------------------------------------------------------------------------------------

class InterferenceRadialParacrystalItem : public InterferenceItem {
public:
    InterferenceRadialParacrystalItem();
    std::unique_ptr<IInterference> createInterference() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    DoubleProperty& peakDistance() { return m_peak_distance; }
    const DoubleProperty& peakDistance() const { return m_peak_distance; }
    void setPeakDistance(double v) { m_peak_distance.setDVal(v); }

    DoubleProperty& dampingLength() { return m_damping_length; }
    const DoubleProperty& dampingLength() const { return m_damping_length; }
    void setDampingLength(double v) { m_damping_length.setDVal(v); }

    DoubleProperty& domainSize() { return m_domain_size; }
    const DoubleProperty& domainSize() const { return m_domain_size; }
    void setDomainSize(double v) { m_domain_size.setDVal(v); }

    DoubleProperty& kappa() { return m_kappa; }
    const DoubleProperty& kappa() const { return m_kappa; }
    void setKappa(double v) { m_kappa.setDVal(v); }

    PolyPtr<Profile1DItem, Profile1DCatalog>& probabilityDistributionSelection() { return m_pdf; }
    void setPDFType(Profile1DItem* p) { m_pdf.setCertainItem(p); }

private:
    DoubleProperty m_peak_distance;
    DoubleProperty m_damping_length;
    DoubleProperty m_domain_size;
    DoubleProperty m_kappa;
    PolyPtr<Profile1DItem, Profile1DCatalog> m_pdf;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_INTERFERENCEITEMS_H
