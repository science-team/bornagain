//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/InterlayerItem.h
//! @brief     Defines classes InterlayerItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_INTERLAYERITEMS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_INTERLAYERITEMS_H

#include "Sample/Interface/InterlayerModels.h"
#include <QXmlStreamReader>
#include <memory>

class InterlayerItem {
public:
    virtual ~InterlayerItem() = default;

    virtual void writeTo(QXmlStreamWriter*) const {}
    virtual void readFrom(QXmlStreamReader*) {}

    virtual std::unique_ptr<InterlayerModel> createModel() const = 0;

protected:
    InterlayerItem() = default;
};

class ErfInterlayerItem : public InterlayerItem {
public:
    ErfInterlayerItem() = default;
    std::unique_ptr<InterlayerModel> createModel() const
    {
        return std::make_unique<ErfInterlayer>();
    }
};

class TanhInterlayerItem : public InterlayerItem {
public:
    TanhInterlayerItem() = default;
    std::unique_ptr<InterlayerModel> createModel() const
    {
        return std::make_unique<TanhInterlayer>();
    }
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_INTERLAYERITEMS_H
