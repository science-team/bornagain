//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/Item3D.h
//! @brief     Defines and implements class Item3D.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_ITEM3D_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_ITEM3D_H

//! Base class of items that can be at the root of a realspace 3D representation
class Item3D {
public:
    virtual ~Item3D() = default;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_ITEM3D_H
