//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ItemWithLayers.cpp
//! @brief     Implements class ItemWithLayers.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/ItemWithLayers.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString Color("Color");
const QString ExpandGroupbox("ExpandGroupbox");

} // namespace Tag
} // namespace

void ItemWithLayers::writeTo(QXmlStreamWriter* w) const
{
    XML::writeTaggedValue(w, Tag::Color, m_color);
    XML::writeTaggedValue(w, Tag::ExpandGroupbox, expandGroupbox);
}

void ItemWithLayers::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::Color)
            m_color = XML::readTaggedColor(r, tag);
        else if (tag == Tag::ExpandGroupbox)
            expandGroupbox = XML::readTaggedBool(r, tag);
        else
            r->skipCurrentElement();
    }
}
