//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ItemWithLayers.h
//! @brief     Defines class ItemWithLayers.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_ITEMWITHLAYERS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_ITEMWITHLAYERS_H

#include "GUI/Model/Sample/Item3D.h"
#include <QColor>
#include <QXmlStreamReader>

class LayerItem;

class ItemWithLayers : public virtual Item3D {
public:
    ~ItemWithLayers() = default;

    QColor color() const { return m_color; }
    void setColor(const QColor& c) { m_color = c; }

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);

    bool expandGroupbox = true;

protected:
    QColor m_color;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_ITEMWITHLAYERS_H
