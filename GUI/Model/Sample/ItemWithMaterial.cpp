//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ItemWithMaterial.cpp
//! @brief     Implements class ItemWithMaterial.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/ItemWithMaterial.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Material/MaterialItem.h"
#include "GUI/Model/Material/MaterialsSet.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString MaterialId("MaterialId");

} // namespace Tag
} // namespace

ItemWithMaterial::ItemWithMaterial(const MaterialsSet* materialModel)
    : m_materials(materialModel)
{
    ASSERT(materialModel);
}

void ItemWithMaterial::setMaterial(const MaterialItem* materialItem)
{
    m_material_identifier = materialItem->identifier();
}

void ItemWithMaterial::setMaterial(const QString& materialIdentifier)
{
    m_material_identifier = materialIdentifier;
}


QColor ItemWithMaterial::materialColor() const
{
    ASSERT(materialItem());
    return materialItem()->color();
}

QString ItemWithMaterial::materialName() const
{
    ASSERT(materialItem());
    return materialItem()->matItemName();
}

MaterialItem* ItemWithMaterial::materialItem() const
{
    if (materialIdentifier().isEmpty())
        return nullptr;
    ASSERT(m_materials);
    return m_materials->materialItemFromIdentifier(materialIdentifier());
}

void ItemWithMaterial::writeTo(QXmlStreamWriter* w) const
{
    XML::writeTaggedValue(w, Tag::MaterialId, m_material_identifier);
}

void ItemWithMaterial::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::MaterialId)
            m_material_identifier = XML::readTaggedString(r, tag);
        else
            r->skipCurrentElement();
    }
}
