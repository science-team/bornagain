//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ItemWithMaterial.h
//! @brief     Defines class ItemWithMaterial.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_ITEMWITHMATERIAL_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_ITEMWITHMATERIAL_H

#include <QColor>
#include <QString>
#include <QXmlStreamReader>

class MaterialItem;
class MaterialsSet;

//! Base class for all sample components made of some material.

class ItemWithMaterial {
public:
    //! Overhand the material list where the current material has to be searched for.
    explicit ItemWithMaterial(const MaterialsSet* materialModel);

    //! Set the material this item shall use.
    //! Stores the identifier, not the pointer!
    void setMaterial(const MaterialItem* materialItem);

    //! Set the material this item shall use.
    //! Stores the given identifier, not a pointer to the material!
    void setMaterial(const QString& materialIdentifier);

    QColor materialColor() const;
    QString materialName() const;
    QString materialIdentifier() const { return m_material_identifier; }

    //! Returns the material item this item links to.
    MaterialItem* materialItem() const;

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);

protected:
    const MaterialsSet* m_materials;
    QString m_material_identifier;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_ITEMWITHMATERIAL_H
