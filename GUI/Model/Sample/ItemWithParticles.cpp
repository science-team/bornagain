//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ItemWithParticles.cpp
//! @brief     Implements class ItemWithParticles.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/ItemWithParticles.h"
#include "Sample/Particle/IParticle.h"
#include "Sample/Scattering/Rotations.h"

namespace {
namespace Tag {

const QString Abundance("Abundance");
const QString Position("Position");
const QString Rotation("Rotation");

} // namespace Tag
} // namespace

ItemWithParticles::ItemWithParticles(const QString& abundanceTooltip,
                                     const QString& positionTooltip)
{
    m_abundance.init("Abundance", "", abundanceTooltip, 1.0, 3, RealLimits::limited(0.0, 1.0),
                     "abundance");
    m_position.init("Position offset", "nm", positionTooltip, R3(0, 0, 0), 3, true, 0.1,
                    RealLimits::limitless(), "pos");
    m_rotation.simpleInit("Rotation", "", RotationCatalog::Type::None);
}

std::unique_ptr<IRotation> ItemWithParticles::createRotation() const
{
    if (!m_rotation.certainItem())
        return {};
    return m_rotation.certainItem()->createRotation();
}

void ItemWithParticles::writeTo(QXmlStreamWriter* w) const
{
    // abundance
    m_abundance.writeTo2(w, Tag::Abundance);
    XML::writeTaggedElement(w, Tag::Position, m_position);
    XML::writeTaggedElement(w, Tag::Rotation, m_rotation);
}

void ItemWithParticles::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::Abundance) {
            m_abundance.readFrom2(r, tag);
        } else if (tag == Tag::Position)
            XML::readTaggedElement(r, tag, m_position);
        else if (tag == Tag::Rotation)
            XML::readTaggedElement(r, tag, m_rotation);
        else
            r->skipCurrentElement();
    }
}
