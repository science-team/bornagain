//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ItemWithParticles.h
//! @brief     Defines abstract item with a material property.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_ITEMWITHPARTICLES_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_ITEMWITHPARTICLES_H

#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/Model/Descriptor/VectorProperty.h"
#include "GUI/Model/Sample/Item3D.h"
#include "GUI/Model/Sample/RotationCatalog.h"
#include "GUI/Model/Sample/RotationItems.h"
#include <memory>

class IRotation;

class ItemWithParticles : public virtual Item3D {
public:
    virtual ~ItemWithParticles() = default;

    DoubleProperty& abundance() { return m_abundance; }
    const DoubleProperty& abundance() const { return m_abundance; }
    void setAbundance(double v) { m_abundance.setDVal(v); }

    VectorProperty& position() { return m_position; }
    const VectorProperty& position() const { return m_position; }
    void setPosition(const R3& position) { m_position.setR3(position); }

    PolyPtr<RotationItem, RotationCatalog>& rotationSelection() { return m_rotation; }

    //! nullptr is allowed and sets to "no rotation"
    void setRotationType(RotationItem* p) { m_rotation.setCertainItem(p); }

    //! nullptr only if "no rotation". Can contain identity!
    std::unique_ptr<IRotation> createRotation() const;

    //! Returns particles contained at any sublevel (recursive).
    virtual std::vector<ItemWithParticles*> containedItemsWithParticles() const = 0;

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);

protected:
    ItemWithParticles(const QString& abundanceTooltip, const QString& positionTooltip);

    DoubleProperty m_abundance;
    VectorProperty m_position;
    PolyPtr<RotationItem, RotationCatalog> m_rotation;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_ITEMWITHPARTICLES_H
