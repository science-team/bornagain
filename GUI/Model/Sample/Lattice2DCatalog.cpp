//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/Lattice2DCatalog.cpp
//! @brief     Implements class Lattice2DCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/Lattice2DCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/Lattice2DItems.h"

Lattice2DItem* Lattice2DCatalog::create(Type type)
{
    switch (type) {
    case Type::Basic:
        return new BasicLattice2DItem;
    case Type::Square:
        return new SquareLattice2DItem;
    case Type::Hexagonal:
        return new HexagonalLattice2DItem;
    default:
        ASSERT_NEVER;
    }
}

QVector<Lattice2DCatalog::Type> Lattice2DCatalog::types()
{
    return {Type::Basic, Type::Square, Type::Hexagonal};
}

UiInfo Lattice2DCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::Basic:
        return {"Basic", "Two dimensional lattice", ""};
    case Type::Square:
        return {"Square", "", ""};
    case Type::Hexagonal:
        return {"Hexagonal", "", ""};
    default:
        ASSERT_NEVER;
    }
}

Lattice2DCatalog::Type Lattice2DCatalog::type(const Lattice2DItem* item)
{
    ASSERT(item);

    if (dynamic_cast<const BasicLattice2DItem*>(item))
        return Type::Basic;
    if (dynamic_cast<const SquareLattice2DItem*>(item))
        return Type::Square;
    if (dynamic_cast<const HexagonalLattice2DItem*>(item))
        return Type::Hexagonal;

    ASSERT_NEVER;
}
