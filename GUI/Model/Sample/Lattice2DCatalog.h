//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/Lattice2DCatalog.h
//! @brief     Defines class Lattice2DCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_LATTICE2DCATALOG_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_LATTICE2DCATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class Lattice2DItem;

class Lattice2DCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t { Basic = 1, Square = 2, Hexagonal };

    //! Creates the item of the given type.
    static Lattice2DItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const Lattice2DItem* item);
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_LATTICE2DCATALOG_H
