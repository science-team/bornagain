//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/Lattice2DItems.h
//! @brief     Defines classes Lattice2DItems.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_LATTICE2DITEMS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_LATTICE2DITEMS_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include <memory>

class Lattice2D;

class Lattice2DItem {
protected:
    Lattice2DItem();

public:
    virtual ~Lattice2DItem() = default;
    virtual std::unique_ptr<Lattice2D> createLattice() const = 0;
    virtual DoubleProperties geometryValues(bool withRotationAngle) = 0;

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);
    double unitCellArea() const;

    DoubleProperty& latticeRotationAngle() { return m_lattice_rotation_angle; }
    void setLatticeRotationAngle(double v) { m_lattice_rotation_angle.setDVal(v); }

protected:
    DoubleProperty m_lattice_rotation_angle;
};

// -------------------------------------------------------------------------------------

class BasicLattice2DItem : public Lattice2DItem {
public:
    BasicLattice2DItem();
    std::unique_ptr<Lattice2D> createLattice() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;
    DoubleProperties geometryValues(bool withRotationAngle) override
    {
        if (withRotationAngle)
            return {&m_length1, &m_length2, &m_angle, &m_lattice_rotation_angle};
        return {&m_length1, &m_length2, &m_angle};
    }

    void setLatticeLength1(double v) { m_length1.setDVal(v); }
    void setLatticeLength2(double v) { m_length2.setDVal(v); }
    void setLatticeAngle(double v) { m_angle.setDVal(v); }

private:
    DoubleProperty m_length1;
    DoubleProperty m_length2;
    DoubleProperty m_angle;
};

// -------------------------------------------------------------------------------------

class SquareLattice2DItem : public Lattice2DItem {
public:
    SquareLattice2DItem();
    std::unique_ptr<Lattice2D> createLattice() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;
    DoubleProperties geometryValues(bool withRotationAngle) override
    {
        if (withRotationAngle)
            return {&m_length, &m_lattice_rotation_angle};
        return {&m_length};
    }

    void setLatticeLength(double v) { m_length.setDVal(v); }

private:
    DoubleProperty m_length;
};

// -------------------------------------------------------------------------------------

class HexagonalLattice2DItem : public Lattice2DItem {
public:
    HexagonalLattice2DItem();
    std::unique_ptr<Lattice2D> createLattice() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;
    DoubleProperties geometryValues(bool withRotationAngle) override
    {
        if (withRotationAngle)
            return {&m_length, &m_lattice_rotation_angle};
        return {&m_length};
    }

    void setLatticeLength(double v) { m_length.setDVal(v); }

private:
    DoubleProperty m_length;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_LATTICE2DITEMS_H
