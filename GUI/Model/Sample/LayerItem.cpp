//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/LayerItem.cpp
//! @brief     Implements class LayerItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/LayerItem.h"
#include "Base/Util/Assert.h"
#include "Base/Util/Vec.h"
#include "GUI/Model/Sample/CompoundItem.h"
#include "GUI/Model/Sample/CoreAndShellItem.h"
#include "GUI/Model/Sample/MesocrystalItem.h"
#include "GUI/Model/Sample/ParticleItem.h"
#include "GUI/Model/Sample/ParticleLayoutItem.h"
#include "GUI/Model/Sample/SampleItem.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString BaseLayerData("BaseLayerData");
const QString MaterialData("MaterialData");
const QString Name("Name");
const QString NumSlices("NumSlices");
const QString Thickness("Thickness");
const QString Roughness("Roughness");
const QString Layout("Layout");
const QString ExpandRoughnessGroupbox("ExpandRoughnessGroupbox");

} // namespace Tag

std::vector<ItemWithMaterial*> layoutItemsWithMaterial(ParticleLayoutItem* layout)
{
    std::vector<ItemWithMaterial*> result;

    std::vector<ItemWithParticles*> itemsWithParticles = layout->itemsWithParticles();
    while (!itemsWithParticles.empty()) {
        auto* item = itemsWithParticles.front();
        itemsWithParticles.erase(itemsWithParticles.begin());
        if (!item)
            continue;

        if (auto* p = dynamic_cast<CompoundItem*>(item))
            Vec::concat(itemsWithParticles, p->itemsWithParticles());
        else if (auto* p = dynamic_cast<MesocrystalItem*>(item))
            itemsWithParticles.push_back(p->basisItem());
        else if (auto* p = dynamic_cast<ParticleItem*>(item))
            result.push_back(p);
        else if (auto* p = dynamic_cast<CoreAndShellItem*>(item)) {
            if (p->coreItem())
                result.push_back(p->coreItem());
            if (p->shellItem())
                result.push_back(p->shellItem());
        } else
            ASSERT_NEVER;
    }
    return result;
}

} // namespace

LayerItem::LayerItem(const MaterialsSet* materials)
    : ItemWithMaterial(materials)
{
    m_thickness.init("Thickness", "nm", "Thickness of the layer", 0.0, 3,
                     RealLimits::lowerLimited(0.0), "thickness");

    m_roughness.simpleInit("Top roughness", "Roughness of top interface",
                           RoughnessCatalog::Type::SelfAffineFractal);
}

LayerItem::~LayerItem() = default;

std::vector<ItemWithMaterial*> LayerItem::itemsWithMaterial()
{
    std::vector<ItemWithMaterial*> result;
    result.push_back(this);
    for (ParticleLayoutItem* layout : layoutItems())
        Vec::concat(result, layoutItemsWithMaterial(layout));
    return result;
}

std::vector<ItemWithParticles*> LayerItem::itemsWithParticles() const
{
    std::vector<ItemWithParticles*> result;
    for (auto* layout : layoutItems())
        Vec::concat(result, layout->containedItemsWithParticles());
    return result;
}

ParticleLayoutItem* LayerItem::addLayoutItem()
{
    m_layouts.push_back(new ParticleLayoutItem(m_materials));
    return m_layouts.back();
}

void LayerItem::removeLayoutItem(ParticleLayoutItem* layout)
{
    m_layouts.delete_element(layout);
}

void LayerItem::writeTo(QXmlStreamWriter* w) const
{
    XML::writeBaseElement<ItemWithMaterial>(w, Tag::MaterialData, this);
    XML::writeBaseElement<ItemWithLayers>(w, Tag::BaseLayerData, this);
    XML::writeTaggedValue(w, Tag::NumSlices, m_num_slices);
    m_thickness.writeTo2(w, Tag::Thickness);
    XML::writeTaggedElement(w, Tag::Roughness, m_roughness);
    for (const auto* layout : m_layouts)
        XML::writeTaggedElement(w, Tag::Layout, *layout);
    XML::writeTaggedValue(w, Tag::ExpandRoughnessGroupbox, expandRoughness);
}

void LayerItem::readFrom(QXmlStreamReader* r)
{
    m_layouts.clear();
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::MaterialData)
            XML::readBaseElement<ItemWithMaterial>(r, tag, this);
        else if (tag == Tag::BaseLayerData)
            XML::readBaseElement<ItemWithLayers>(r, tag, this);
        else if (tag == Tag::NumSlices)
            m_num_slices = XML::readTaggedUInt(r, tag);
        else if (tag == Tag::Thickness)
            m_thickness.readFrom2(r, tag);
        else if (tag == Tag::Roughness)
            XML::readTaggedElement(r, tag, m_roughness);
        else if (tag == Tag::Layout)
            XML::readTaggedElement(r, tag, *addLayoutItem());
        else if (tag == Tag::ExpandRoughnessGroupbox)
            expandRoughness = XML::readTaggedBool(r, tag);
        else
            r->skipCurrentElement();
    }
}
