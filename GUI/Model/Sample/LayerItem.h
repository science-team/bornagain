//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/LayerItem.h
//! @brief     Defines class LayerItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_LAYERITEM_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_LAYERITEM_H

#include "Base/Type/OwningVector.h"
#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/Model/Sample/Item3D.h"
#include "GUI/Model/Sample/ItemWithLayers.h"
#include "GUI/Model/Sample/ItemWithMaterial.h"
#include "GUI/Model/Sample/RoughnessCatalog.h"
#include "GUI/Model/Sample/RoughnessItems.h"
#include <QColor>

class ItemWithParticles;
class ParticleLayoutItem;

class LayerItem : public virtual ItemWithMaterial, public virtual ItemWithLayers {
public:
    explicit LayerItem(const MaterialsSet* materials);
    ~LayerItem();

    std::vector<ItemWithMaterial*> itemsWithMaterial();
    std::vector<ItemWithParticles*> itemsWithParticles() const;

    DoubleProperty& thickness() { return m_thickness; }
    const DoubleProperty& thickness() const { return m_thickness; }

    PolyPtr<RoughnessItem, RoughnessCatalog>& roughnessSelection() { return m_roughness; }
    RoughnessItem* certainRoughness() const { return m_roughness.certainItem(); }

    uint numSlices() const { return m_num_slices; }
    void setNumSlices(uint v) { m_num_slices = v; }

    const std::vector<ParticleLayoutItem*>& layoutItems() const { return m_layouts.shared(); }
    ParticleLayoutItem* addLayoutItem();
    void removeLayoutItem(ParticleLayoutItem* layout);

    // ambient is a top infinite or semi-infinite layer
    bool isAmbient() const { return m_is_ambient; }
    void setIsAmbient(bool b) { m_is_ambient = b; }

    // substrate is a bottom semi-infinite layer
    bool isSubstrate() const { return m_is_substrate; }
    void setIsSubstrate(bool b) { m_is_substrate = b; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    bool expandRoughness = false;

private:
    uint m_num_slices = 1;
    DoubleProperty m_thickness;
    bool m_is_ambient = false;
    bool m_is_substrate = false;
    OwningVector<ParticleLayoutItem> m_layouts;
    PolyPtr<RoughnessItem, RoughnessCatalog> m_roughness;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_LAYERITEM_H
