//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/LayerStackItem.h
//! @brief     Defines class LayerStackItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_LAYERSTACKITEM_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_LAYERSTACKITEM_H

#include "Base/Type/OwningVector.h"
#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/Model/Sample/ItemWithLayers.h"
#include "GUI/Model/Sample/LayeredComponentCatalog.h"

class MaterialsSet;

class LayerStackItem : public ItemWithLayers {
public:
    explicit LayerStackItem(const MaterialsSet* materials, uint n_periods = 1);

    std::vector<LayerItem*> uniqueLayerItems() const;
    std::vector<LayerItem*> unwrappedLayerItems() const;
    std::vector<ItemWithLayers*> componentItems() const;
    int indexOfComponent(const ItemWithLayers* item) const;
    LayerStackItem* parentOfComponent(const ItemWithLayers* searchedItem);

    LayerItem* createLayerItemAt(int index = -1);
    LayerStackItem* createLayerStackItemAt(int index = -1);

    void removeComponent(const ItemWithLayers* component);
    void moveComponent(ItemWithLayers* component, ItemWithLayers* aboveThisComponent);

    void setNumberOfPeriods(uint n) { m_n_periods = n; }
    uint numberOfPeriods() const { return m_n_periods; }

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    ItemWithLayers* addNewItemAt(ItemWithLayers* item, int index);

private:
    uint m_n_periods;
    OwningVector<PolyPtrWithContext<ItemWithLayers, LayeredComponentCatalog, MaterialsSet>>
        m_components;
    const MaterialsSet* m_materials;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_LAYERSTACKITEM_H
