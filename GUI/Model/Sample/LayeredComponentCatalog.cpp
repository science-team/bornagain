//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/LayeredComponentCatalog.h
//! @brief     Implements class LayeredComponentCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/LayeredComponentCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/LayerItem.h"
#include "GUI/Model/Sample/LayerStackItem.h"

ItemWithLayers* LayeredComponentCatalog::create(Type type, const MaterialsSet* materials)
{
    ASSERT(materials);

    switch (type) {
    case Type::Layer:
        return new LayerItem(materials);
    case Type::LayerStack:
        return new LayerStackItem(materials, 1);
    }
    ASSERT_NEVER;
}

QVector<LayeredComponentCatalog::Type> LayeredComponentCatalog::types()
{
    return {Type::Layer, Type::LayerStack};
}

UiInfo LayeredComponentCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::Layer:
        return {"Layer", "Single layer", ""};
    case Type::LayerStack:
        return {"Stack", "Periodic stack with given unit cell and number of repetitions", ""};
    }
    ASSERT_NEVER;
}

LayeredComponentCatalog::Type LayeredComponentCatalog::type(const ItemWithLayers* item)
{
    if (dynamic_cast<const LayerItem*>(item))
        return Type::Layer;
    if (dynamic_cast<const LayerStackItem*>(item))
        return Type::LayerStack;

    ASSERT_NEVER;
}
