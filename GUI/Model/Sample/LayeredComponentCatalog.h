//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/LayeredComponentCatalog.h
//! @brief     Defines class LayeredComponentCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_LAYEREDCOMPONENTCATALOG_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_LAYEREDCOMPONENTCATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class ItemWithLayers;
class MaterialsSet;

class LayeredComponentCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t { Layer = 0, LayerStack = 1 };

    //! Creates the item of the given type.
    static ItemWithLayers* create(Type type, const MaterialsSet* materials);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const ItemWithLayers* item);
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_LAYEREDCOMPONENTCATALOG_H
