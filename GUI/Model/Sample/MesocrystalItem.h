//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/MesocrystalItem.h
//! @brief     Defines class MesocrystalItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_MESOCRYSTALITEM_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_MESOCRYSTALITEM_H

#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/Model/Descriptor/VectorProperty.h"
#include "GUI/Model/Sample/FormfactorCatalog.h"
#include "GUI/Model/Sample/FormfactorItems.h"
#include "GUI/Model/Sample/ItemWithParticles.h"
#include "GUI/Model/Sample/ParticleCatalog.h"
#include "Sample/Lattice/Lattice3D.h"
#include <memory>

class IFormfactor;
class IParticle;
class MaterialsSet;
class Mesocrystal;

class MesocrystalItem : public ItemWithParticles {
public:
    explicit MesocrystalItem(const MaterialsSet* materials);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    std::unique_ptr<Mesocrystal> createMesocrystal() const;

    Lattice3D getLattice() const;
    std::unique_ptr<IParticle> getBasis() const;

    std::unique_ptr<IFormfactor> getOuterShape() const;
    template <typename T> T* setOuterShapeType();

    PolyPtr<FormfactorItem, FormfactorCatalog>& outerShapeSelection() { return m_outer_shape; }
    void setOuterShape(FormfactorItem* p) { m_outer_shape.setCertainItem(p); }

    ItemWithParticles* basisItem() const { return m_basis_particle.certainItem(); }
    void setBasisItem(ItemWithParticles* basis) { m_basis_particle.setCertainItem(basis); }

    VectorProperty& vectorA() { return m_vectorA; }
    const VectorProperty& vectorA() const { return m_vectorA; }
    void setVectorA(const R3& v) { m_vectorA.setR3(v); }

    VectorProperty& vectorB() { return m_vectorB; }
    const VectorProperty& vectorB() const { return m_vectorB; }
    void setVectorB(const R3& v) { m_vectorB.setR3(v); }

    VectorProperty& vectorC() { return m_vectorC; }
    const VectorProperty& vectorC() const { return m_vectorC; }
    void setVectorC(const R3& v) { m_vectorC.setR3(v); }

    std::vector<ItemWithParticles*> containedItemsWithParticles() const override;

    bool expandMesocrystal = true;

private:
    VectorProperty m_vectorA;
    VectorProperty m_vectorB;
    VectorProperty m_vectorC;
    PolyPtr<FormfactorItem, FormfactorCatalog> m_outer_shape;
    PolyPtrWithContext<ItemWithParticles, ParticleCatalog, MaterialsSet> m_basis_particle;
    const MaterialsSet* m_materials;
};

template <typename T> T* MesocrystalItem::setOuterShapeType()
{
    auto* p = new T;
    setOuterShape(p);
    return p;
}

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_MESOCRYSTALITEM_H
