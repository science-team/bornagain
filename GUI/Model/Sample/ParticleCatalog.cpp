//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ParticleCatalog.cpp
//! @brief     Implements class ParticleCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/ParticleCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/CompoundItem.h"
#include "GUI/Model/Sample/CoreAndShellItem.h"
#include "GUI/Model/Sample/MesocrystalItem.h"
#include "GUI/Model/Sample/ParticleItem.h"

ItemWithParticles* ParticleCatalog::create(Type type, const MaterialsSet* materials)
{
    ASSERT(materials);

    switch (type) {
    case Type::Particle:
        return new ParticleItem(materials);
    case Type::Composition:
        return new CompoundItem(materials);
    case Type::CoreShell:
        return new CoreAndShellItem(materials);
    case Type::Mesocrystal:
        return new MesocrystalItem(materials);
    default:
        ASSERT_NEVER;
    }
}

QVector<ParticleCatalog::Type> ParticleCatalog::types()
{
    return {Type::Particle, Type::Composition, Type::CoreShell, Type::Mesocrystal};
}

QVector<ParticleCatalog::Type> ParticleCatalog::assemblyTypes()
{
    return {Type::Composition, Type::CoreShell, Type::Mesocrystal};
}

UiInfo ParticleCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::Particle:
        return {"Particle", "", ""}; // particle is not on UI, only its form factor
    case Type::Composition:
        return {"Particle Composition", "Composition of particles with fixed positions",
                ":/images/struct/Compound_64x64.png"};
    case Type::CoreShell:
        return {"Core shell particle", "A particle with a core/shell geometry",
                ":/images/struct/CoreAndShell_64x64.png"};
    case Type::Mesocrystal:
        return {"Meso Crystal", "A 3D crystal structure of nanoparticles",
                ":/images/struct/Mesocrystal_64x64.png"};
    default:
        ASSERT_NEVER;
    }
}

ParticleCatalog::Type ParticleCatalog::type(const ItemWithParticles* item)
{
    ASSERT(item);

    if (dynamic_cast<const ParticleItem*>(item))
        return Type::Particle;
    if (dynamic_cast<const CompoundItem*>(item))
        return Type::Composition;
    if (dynamic_cast<const MesocrystalItem*>(item))
        return Type::Mesocrystal;
    if (dynamic_cast<const CoreAndShellItem*>(item))
        return Type::CoreShell;

    ASSERT_NEVER;
}
