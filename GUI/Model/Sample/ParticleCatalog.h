//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ParticleCatalog.h
//! @brief     Defines class ParticleCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_PARTICLECATALOG_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_PARTICLECATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class ItemWithParticles;
class MaterialsSet;

class ParticleCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t { Particle = 1, Composition = 2, CoreShell = 3, Mesocrystal = 4 };

    //! Creates the item of the given type.
    static ItemWithParticles* create(Type type, const MaterialsSet* materials);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    static QVector<Type> assemblyTypes();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const ItemWithParticles* item);
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_PARTICLECATALOG_H
