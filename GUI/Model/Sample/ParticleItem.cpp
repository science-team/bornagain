//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ParticleItem.cpp
//! @brief     Implements class ParticleItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/ParticleItem.h"
#include "GUI/Model/Material/MaterialItem.h"
#include "Sample/Particle/IFormfactor.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"

namespace {
namespace Tag {

const QString Formfactor("Formfactor");
const QString MaterialData("MaterialData");
const QString BaseData("BaseData");
const QString ExpandParticleGroupbox("ExpandParticleGroupbox");

} // namespace Tag

const QString abundance_tooltip = "Proportion of this type of particles normalized to the \n"
                                  "total number of particles in the layout";

const QString position_tooltip = "Relative position of the particle's reference point \n"
                                 "in the coordinate system of the parent (nm)";

} // namespace

ParticleItem::ParticleItem(const MaterialsSet* materials)
    : ItemWithMaterial(materials)
    , ItemWithParticles(abundance_tooltip, position_tooltip)
{
    m_form_factor.simpleInit("Form Factor", "", FormfactorCatalog::Type::Sphere);
    m_form_factor.setCertainItem(new CylinderItem);
}

void ParticleItem::writeTo(QXmlStreamWriter* w) const
{
    XML::writeBaseElement<ItemWithParticles>(w, XML::Tag::BaseData, this);
    w->writeStartElement(Tag::MaterialData);
    ItemWithMaterial::writeTo(w);
    w->writeEndElement();
    XML::writeTaggedElement(w, Tag::Formfactor, m_form_factor);
    XML::writeTaggedValue(w, Tag::ExpandParticleGroupbox, expandParticle);
}

void ParticleItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::BaseData)
            XML::readBaseElement<ItemWithParticles>(r, tag, this);
        else if (tag == Tag::MaterialData)
            XML::readBaseElement<ItemWithMaterial>(r, tag, this);
        else if (tag == Tag::Formfactor)
            XML::readTaggedElement(r, tag, m_form_factor);
        else if (tag == Tag::ExpandParticleGroupbox)
            expandParticle = XML::readTaggedBool(r, tag);
        else
            r->skipCurrentElement();
    }
}

std::unique_ptr<Particle> ParticleItem::createParticle() const
{
    auto domainMaterial = materialItem()->createMaterial();

    auto particle = std::make_unique<Particle>(*domainMaterial,
                                               *m_form_factor.certainItem()->createFormfactor());
    particle->setAbundance(abundance().dVal());
    if (auto r = createRotation(); r && !r->isIdentity())
        particle->rotate(*r);
    particle->translate(position());

    return particle;
}

void ParticleItem::setFormfactor(FormfactorItem* p)
{
    m_form_factor.setCertainItem(p);
}

FormfactorItem* ParticleItem::formFactorItem() const
{
    return m_form_factor.certainItem();
}
