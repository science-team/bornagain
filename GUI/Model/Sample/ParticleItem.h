//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ParticleItem.h
//! @brief     Defines class ParticleItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_PARTICLEITEM_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_PARTICLEITEM_H

#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/Model/Sample/FormfactorCatalog.h"
#include "GUI/Model/Sample/FormfactorItems.h"
#include "GUI/Model/Sample/ItemWithMaterial.h"
#include "GUI/Model/Sample/ItemWithParticles.h"
#include <memory>

class Particle;

class ParticleItem : public ItemWithMaterial, public ItemWithParticles {
public:
    ParticleItem(const MaterialsSet* materials);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    std::unique_ptr<Particle> createParticle() const;

    template <typename T> T* setFormfactorItemType();
    void setFormfactor(FormfactorItem* p);
    FormfactorItem* formFactorItem() const;
    std::vector<ItemWithParticles*> containedItemsWithParticles() const override { return {}; }

    bool expandParticle = true;

private:
    PolyPtr<FormfactorItem, FormfactorCatalog> m_form_factor;
};

template <typename T> T* ParticleItem::setFormfactorItemType()
{
    auto* p = new T;
    setFormfactor(p);
    return p;
}

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_PARTICLEITEM_H
