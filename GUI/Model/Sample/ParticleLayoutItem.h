//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ParticleLayoutItem.h
//! @brief     Defines class ParticleLayoutItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_PARTICLELAYOUTITEM_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_PARTICLELAYOUTITEM_H

#include "Base/Type/OwningVector.h"
#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/Model/Sample/InterferenceCatalog.h"
#include "GUI/Model/Sample/InterferenceItems.h"
#include "GUI/Model/Sample/Item3D.h"

class ItemWithParticles;
class MaterialsSet;

class ParticleLayoutItem : public Item3D {
public:
    ParticleLayoutItem(const MaterialsSet* materials);

    //! The density value which belonging only to the layout.
    //!
    //! This is the editable value. If an interference is present, this value may not be the one to
    //! be used for building the domain layout or to be presented. For the correct value-to-use,
    //! whether an interference is present or not, use totalDensityValue().
    DoubleProperty& ownDensity() { return m_own_density; }
    const DoubleProperty& ownDensity() const { return m_own_density; }
    void setOwnDensity(double v) { m_own_density.setDVal(v); }

    //! The real density.
    //!
    //! Returns ownDensity() value if the interference is not influencing the density. If the
    //! interference is defining the density, this is the interference-calculated density.
    double totalDensityValue() const;

    // Returns particles contained at top-level (non-recursive).
    std::vector<ItemWithParticles*> itemsWithParticles() const { return m_particles.shared(); }

    //! Return particles contained at any level (recursive).
    std::vector<ItemWithParticles*> containedItemsWithParticles() const;

    void addItemWithParticleSelection(ItemWithParticles* particle);
    void removeItemWithParticle(ItemWithParticles* particle);

    PolyPtr<InterferenceItem, InterferenceCatalog>& interferenceSelection()
    {
        return m_interference;
    }
    const PolyPtr<InterferenceItem, InterferenceCatalog>& interferenceSelection() const
    {
        return m_interference;
    }
    void setInterference(InterferenceItem* i) { m_interference.setCertainItem(i); }
    void removeInterference() { m_interference.setCertainItem(nullptr); }

    //! Returns whether total density is defined by the currently selected interference.
    //!
    //! Two dimensional interference calculates density automatically; in these cases the "own"
    //! total density value should not be edited but set by the one from the interference.
    bool totalDensityIsDefinedByInterference() const;

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    void updateSeed() const;

    bool expandParticleLayout = true;
    bool expandInterference = true;
    mutable int seed = -1;

private:
    DoubleProperty m_own_density;
    PolyPtr<InterferenceItem, InterferenceCatalog> m_interference;
    OwningVector<ItemWithParticles> m_particles;
    const MaterialsSet* m_materials;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_PARTICLELAYOUTITEM_H
