//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ProfileCatalogs.h
//! @brief     Defines classes Profile1DCatalog, Profile2DCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_PROFILECATALOGS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_PROFILECATALOGS_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class Profile1DItem;
class Profile2DItem;

class Profile1DCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t {
        Cauchy = 1,
        Gauss = 2,
        Gate = 3,
        Triangle = 4,
        Cosine = 5,
        Voigt = 6
    };

    //! Creates the item of the given type.
    static Profile1DItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const Profile1DItem* item);
};

class Profile2DCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t { Cauchy = 1, Gauss = 2, Gate = 3, Cone = 4, Voigt = 5 };

    //! Creates the item of the given type.
    static Profile2DItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const Profile2DItem* item);
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_PROFILECATALOGS_H
