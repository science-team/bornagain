//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/ProfileItems.h
//! @brief     Defines classes Profile1DItem, Profile2DItem, and subclasses.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_PROFILEITEMS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_PROFILEITEMS_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "Sample/Correlation/Profiles1D.h"
#include "Sample/Correlation/Profiles2D.h"
#include <memory>

class Profile1DItem {

public:
    virtual std::unique_ptr<IProfile1D> createProfile() const = 0;
    virtual ~Profile1DItem() = default;

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);

    double omega() const { return m_omega.dVal(); }
    void setOmega(double v) { m_omega.setDVal(v); }

    virtual DoubleProperties profileProperties();

protected:
    Profile1DItem();

    DoubleProperty m_omega;
};

class Profile1DCauchyItem : public Profile1DItem {
public:
    std::unique_ptr<IProfile1D> createProfile() const override;
};

class Profile1DGaussItem : public Profile1DItem {
public:
    std::unique_ptr<IProfile1D> createProfile() const override;
};

class Profile1DGateItem : public Profile1DItem {
public:
    std::unique_ptr<IProfile1D> createProfile() const override;
};

class Profile1DTriangleItem : public Profile1DItem {
public:
    std::unique_ptr<IProfile1D> createProfile() const override;
};

class Profile1DCosineItem : public Profile1DItem {
public:
    std::unique_ptr<IProfile1D> createProfile() const override;
};

class Profile1DVoigtItem : public Profile1DItem {
public:
    Profile1DVoigtItem();
    std::unique_ptr<IProfile1D> createProfile() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    void setEta(double v) { m_eta.setDVal(v); }

    DoubleProperties profileProperties() override;

private:
    DoubleProperty m_eta;
};

// --------------------------------------------------------------------------------------------- //

class Profile2DItem {
public:
    virtual ~Profile2DItem() = default;
    virtual std::unique_ptr<IProfile2D> createProfile() const = 0;

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);

    void setOmegaX(double v) { m_omegaX.setDVal(v); }
    void setOmegaY(double v) { m_omegaY.setDVal(v); }
    void setGamma(double v) { m_gamma.setDVal(v); }

    virtual DoubleProperties profileProperties();

protected:
    Profile2DItem();

    DoubleProperty m_omegaX;
    DoubleProperty m_omegaY;
    DoubleProperty m_gamma;
};

class Profile2DCauchyItem : public Profile2DItem {
public:
    std::unique_ptr<IProfile2D> createProfile() const override;
};

class Profile2DGaussItem : public Profile2DItem {
public:
    std::unique_ptr<IProfile2D> createProfile() const override;
};

class Profile2DGateItem : public Profile2DItem {
public:
    std::unique_ptr<IProfile2D> createProfile() const override;
};

class Profile2DConeItem : public Profile2DItem {
public:
    std::unique_ptr<IProfile2D> createProfile() const override;
};

class Profile2DVoigtItem : public Profile2DItem {
public:
    Profile2DVoigtItem();
    std::unique_ptr<IProfile2D> createProfile() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    void setEta(double v) { m_eta.setDVal(v); }

    DoubleProperties profileProperties() override;

private:
    DoubleProperty m_eta;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_PROFILEITEMS_H
