//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/RotationCatalog.cpp
//! @brief     Implements class RotationCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/RotationCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/RotationItems.h"

RotationItem* RotationCatalog::create(Type type)
{
    switch (type) {
    case Type::None:
        return nullptr;
    case Type::X:
        return new XRotationItem();
    case Type::Y:
        return new YRotationItem();
    case Type::Z:
        return new ZRotationItem();
    case Type::Euler:
        return new EulerRotationItem();
    }
    ASSERT_NEVER;
}

QVector<RotationCatalog::Type> RotationCatalog::types()
{
    return {Type::None, Type::X, Type::Y, Type::Z, Type::Euler};
}

UiInfo RotationCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::None:
        return {"None", "", ""};
    case Type::X:
        return {"X axis Rotation", "Particle rotation around x-axis", ""};
    case Type::Y:
        return {"Y axis Rotation", "Particle rotation around y-axis", ""};
    case Type::Z:
        return {"Z axis Rotation", "Particle rotation around z-axis", ""};
    case Type::Euler:
        return {"Euler Rotation",
                "Sequence of three rotations following Euler angles; notation z-x'-z'", ""};
    }
    ASSERT_NEVER;
}

RotationCatalog::Type RotationCatalog::type(const RotationItem* item)
{
    if (!item)
        return Type::None;

    if (dynamic_cast<const XRotationItem*>(item))
        return Type::X;
    if (dynamic_cast<const YRotationItem*>(item))
        return Type::Y;
    if (dynamic_cast<const ZRotationItem*>(item))
        return Type::Z;
    if (dynamic_cast<const EulerRotationItem*>(item))
        return Type::Euler;

    ASSERT_NEVER;
}
