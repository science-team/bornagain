//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/RotationItems.cpp
//! @brief     Implements class RotationItems.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/RotationItems.h"
#include "Base/Const/Units.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString Angle("Angle");
const QString Alpha("Alpha");
const QString Beta("Beta");
const QString Gamma("Gamma");

} // namespace Tag
} // namespace

using namespace Units;

// ----------------------------------------------------------------------------

void XYZRotationItem::writeTo(QXmlStreamWriter* w) const
{
    m_angle.writeTo2(w, Tag::Angle);
}

void XYZRotationItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Angle)
            m_angle.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

// ----------------------------------------------------------------------------

XRotationItem::XRotationItem()
{
    m_angle.init("Angle", "deg", "Rotation angle around x-axis", 0.0, 2, 1.0,
                 RealLimits::limited(0.0, 360.0), "angle");
}

unique_ptr<IRotation> XRotationItem::createRotation() const
{
    return std::make_unique<RotationX>(deg2rad(m_angle.dVal()));
}

// ----------------------------------------------------------------------------

YRotationItem::YRotationItem()
{
    m_angle.init("Angle", "deg", "Rotation angle around y-axis", 0.0, 2, 1.0,
                 RealLimits::limited(0.0, 360.0), "angle");
}

unique_ptr<IRotation> YRotationItem::createRotation() const
{
    return std::make_unique<RotationY>(deg2rad(m_angle.dVal()));
}

// ----------------------------------------------------------------------------

ZRotationItem::ZRotationItem()
{
    m_angle.init("Angle", "deg", "Rotation angle around z-axis", 0.0, 2, 1.0,
                 RealLimits::limited(0.0, 360.0), "angle");
}

unique_ptr<IRotation> ZRotationItem::createRotation() const
{
    return std::make_unique<RotationZ>(deg2rad(m_angle.dVal()));
}

// ----------------------------------------------------------------------------

EulerRotationItem::EulerRotationItem()
{
    m_alpha.init("Alpha", "deg", "First Euler angle in z-x'-z' sequence", 0.0, 2, 1.0,
                 RealLimits::limited(0.0, 360.0), "alpha");
    m_beta.init("Beta", "deg", "Second Euler angle in z-x'-z' sequence", 0.0, 2, 1.0,
                RealLimits::limited(0.0, 360.0), "beta");
    m_gamma.init("Gamma", "deg", "Third Euler angle in z-x'-z' sequence", 0.0, 2, 1.0,
                 RealLimits::limited(0.0, 360.0), "gamma");
}

void EulerRotationItem::writeTo(QXmlStreamWriter* w) const
{
    m_alpha.writeTo2(w, Tag::Alpha);
    m_beta.writeTo2(w, Tag::Beta);
    m_gamma.writeTo2(w, Tag::Gamma);
}

void EulerRotationItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();

        if (tag == Tag::Alpha)
            m_alpha.readFrom2(r, tag);
        else if (tag == Tag::Beta)
            m_beta.readFrom2(r, tag);
        else if (tag == Tag::Gamma)
            m_gamma.readFrom2(r, tag);
        else
            r->skipCurrentElement();
    }
}

unique_ptr<IRotation> EulerRotationItem::createRotation() const
{
    return std::make_unique<RotationEuler>(deg2rad(m_alpha.dVal()), deg2rad(m_beta.dVal()),
                                           deg2rad(m_gamma.dVal()));
}
