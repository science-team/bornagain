//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/RotationItems.h
//! @brief     Defines class RotationItems.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_ROTATIONITEMS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_ROTATIONITEMS_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "Sample/Scattering/Rotations.h"
#include <memory>

using std::unique_ptr;

class RotationItem {
public:
    virtual ~RotationItem() = default;
    virtual unique_ptr<IRotation> createRotation() const = 0;

    virtual void writeTo(QXmlStreamWriter* w) const = 0;
    virtual void readFrom(QXmlStreamReader* r) = 0;
    virtual DoubleProperties rotationProperties() = 0;

protected:
    RotationItem() {}
};

class XYZRotationItem : public RotationItem {
public:
    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;
    DoubleProperties rotationProperties() override { return {&m_angle}; }

    void setAngle(double v) { m_angle.setDVal(v); }

protected:
    DoubleProperty m_angle;
};

class XRotationItem : public XYZRotationItem {
public:
    XRotationItem();
    unique_ptr<IRotation> createRotation() const override;
};

class YRotationItem : public XYZRotationItem {
public:
    YRotationItem();
    unique_ptr<IRotation> createRotation() const override;
};

class ZRotationItem : public XYZRotationItem {
public:
    ZRotationItem();
    unique_ptr<IRotation> createRotation() const override;
};

class EulerRotationItem : public RotationItem {
public:
    EulerRotationItem();

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;
    DoubleProperties rotationProperties() override { return {&m_alpha, &m_beta, &m_gamma}; }

    void setAlpha(double v) { m_alpha.setDVal(v); }
    void setBeta(double v) { m_beta.setDVal(v); }
    void setGamma(double v) { m_gamma.setDVal(v); }

    unique_ptr<IRotation> createRotation() const override;

private:
    DoubleProperty m_alpha;
    DoubleProperty m_beta;
    DoubleProperty m_gamma;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_ROTATIONITEMS_H
