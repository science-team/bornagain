//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/RoughnessCatalog.cpp
//! @brief     Implements class RoughnessCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/RoughnessCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/CrosscorrelationItems.h"
#include "GUI/Model/Sample/RoughnessItems.h"
#include "GUI/Model/Sample/TransientItems.h"

RoughnessItem* RoughnessCatalog::create(Type type)
{
    switch (type) {
    case Type::None:
        return nullptr;
    case Type::SelfAffineFractal:
        return new SelfAffineFractalRoughnessItem(0., .7, 25., 0.5);
    case Type::LinearGrowth:
        return new LinearGrowthRoughnessItem(0, 0, 0, 0, 0, 0.5);
    }
    ASSERT_NEVER;
}

QVector<RoughnessCatalog::Type> RoughnessCatalog::types()
{
    return {Type::None, Type::SelfAffineFractal, Type::LinearGrowth};
}

UiInfo RoughnessCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::None:
        return {"None", "", ""};
    case Type::SelfAffineFractal:
        return {"K-correlation", "K-correlation model of lateral correlation function", ""};
    case Type::LinearGrowth:
        return {"Linear growth", "Model of growing rougness", ""};
    }
    ASSERT_NEVER;
}

RoughnessCatalog::Type RoughnessCatalog::type(const RoughnessItem* item)
{
    if (!item)
        return Type::None;

    if (dynamic_cast<const SelfAffineFractalRoughnessItem*>(item))
        return Type::SelfAffineFractal;

    if (dynamic_cast<const LinearGrowthRoughnessItem*>(item))
        return Type::LinearGrowth;

    ASSERT_NEVER;
}

//--------------------------------------------------------------------------

TransientItem* TransientCatalog::create(Type type)
{
    switch (type) {
    case Type::Erf:
        return new ErfTransientItem();
    case Type::Tanh:
        return new TanhTransientItem();
    }
    ASSERT_NEVER;
}

QVector<TransientCatalog::Type> TransientCatalog::types()
{
    return {Type::Erf, Type::Tanh};
}

UiInfo TransientCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::Erf:
        return {"Erf", "Interlayer transient is Erf function", ""};
    case Type::Tanh:
        return {"Tanh", "Interlayer transient is Tanh function", ""};
    }
    ASSERT_NEVER;
}

TransientCatalog::Type TransientCatalog::type(const TransientItem* model)
{
    if (dynamic_cast<const ErfTransientItem*>(model))
        return Type::Erf;

    if (dynamic_cast<const TanhTransientItem*>(model))
        return Type::Tanh;

    ASSERT_NEVER;
}

//--------------------------------------------------------------------------

CrosscorrelationItem* CrosscorrelationCatalog::create(Type type)
{
    switch (type) {
    case Type::None:
        return nullptr;
    case Type::CommonDepth:
        return new CommonDepthCrosscorrelationItem(0);
    case Type::SpatialFrequency:
        return new SpatialFrequencyCrosscorrelationItem(0, 1, 2);
    }
    ASSERT_NEVER;
}

QVector<CrosscorrelationCatalog::Type> CrosscorrelationCatalog::types()
{
    return {Type::None, Type::CommonDepth, Type::SpatialFrequency};
}

UiInfo CrosscorrelationCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::None:
        return {"None", "", ""};
    case Type::CommonDepth:
        return {"Common Depth", "Roughness crosscorrelation does not depend on spatial frequency",
                ""};
    case Type::SpatialFrequency:
        return {"Spatial Frequency", "Roughness crosscorrelation depends on spatial frequency", ""};
    }
    ASSERT_NEVER;
}

CrosscorrelationCatalog::Type CrosscorrelationCatalog::type(const CrosscorrelationItem* item)
{
    if (!item)
        return Type::None;

    if (dynamic_cast<const CommonDepthCrosscorrelationItem*>(item))
        return Type::CommonDepth;

    if (dynamic_cast<const SpatialFrequencyCrosscorrelationItem*>(item))
        return Type::SpatialFrequency;

    ASSERT_NEVER;
}
