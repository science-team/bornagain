//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/RoughnessCatalog.h
//! @brief     Defines class RoughnessCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_ROUGHNESSCATALOG_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_ROUGHNESSCATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class CrosscorrelationItem;
class RoughnessItem;
class TransientItem;

class RoughnessCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t { None = 0, SelfAffineFractal = 1, LinearGrowth = 2 };

    //! Creates the item of the given type.
    static RoughnessItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const RoughnessItem* item);
};

//--------------------------------------------------------------------------

class TransientCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t { Erf = 0, Tanh = 1 };

    //! Creates the item of the given type.
    static TransientItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const TransientItem* model);
};

//--------------------------------------------------------------------------

class CrosscorrelationCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t { None = 0, CommonDepth = 1, SpatialFrequency = 2 };

    //! Creates the item of the given type.
    static CrosscorrelationItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const CrosscorrelationItem* item);
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_ROUGHNESSCATALOG_H
