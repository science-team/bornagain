//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/RoughnessItems.h
//! @brief     Defines classes RoughnessItems.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_ROUGHNESSITEMS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_ROUGHNESSITEMS_H

#include "Base/Type/OwningVector.h"
#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/Model/Sample/CrosscorrelationItems.h"
#include "GUI/Model/Sample/RoughnessCatalog.h"
#include "GUI/Model/Sample/TransientItems.h"

class AutocorrelationModel;

class RoughnessItem {
public:
    virtual ~RoughnessItem() = default;

    virtual void writeTo(QXmlStreamWriter* w) const;
    virtual void readFrom(QXmlStreamReader* r);
    virtual std::unique_ptr<AutocorrelationModel> createModel() const = 0;
    virtual DoubleProperties roughnessProperties() { return {&m_max_spatial_frequency}; }

    PolyPtr<TransientItem, TransientCatalog>& transientSelection() { return m_transient; }
    TransientItem* certainTransientModel() const { return m_transient.certainItem(); }
    void setTransientModelType(TransientItem* p) { m_transient.setCertainItem(p); }

    PolyPtr<CrosscorrelationItem, CrosscorrelationCatalog>& crossrorrModelSelection()
    {
        return m_crosscorrelation;
    }
    CrosscorrelationItem* certainCrosscorrModel() const { return m_crosscorrelation.certainItem(); }
    void setCrosscorrModelType(CrosscorrelationItem* p) { m_crosscorrelation.setCertainItem(p); }

protected:
    RoughnessItem(double max_spat_frequency);
    PolyPtr<TransientItem, TransientCatalog> m_transient;
    PolyPtr<CrosscorrelationItem, CrosscorrelationCatalog> m_crosscorrelation;
    DoubleProperty m_max_spatial_frequency;
};

class SelfAffineFractalRoughnessItem : public RoughnessItem {
public:
    SelfAffineFractalRoughnessItem(double sigma, double hurst, double corr_length,
                                   double max_spat_frequency);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;
    std::unique_ptr<AutocorrelationModel> createModel() const override;

    DoubleProperties roughnessProperties() override;

private:
    DoubleProperty m_sigma;
    DoubleProperty m_hurst;
    DoubleProperty m_lateral_correlation_length;
};

class LinearGrowthRoughnessItem : public RoughnessItem {
public:
    LinearGrowthRoughnessItem(double cluster_volume, double damp1, double damp2, double damp3,
                              double damp4, double max_spat_frequency);

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;
    std::unique_ptr<AutocorrelationModel> createModel() const override;

    DoubleProperties roughnessProperties() override;

private:
    DoubleProperty m_cluster_volume;
    DoubleProperty m_damp1;
    DoubleProperty m_damp2;
    DoubleProperty m_damp3;
    DoubleProperty m_damp4;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_ROUGHNESSITEMS_H
