//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/SampleItem.h
//! @brief     Defines class SampleItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_SAMPLEITEM_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_SAMPLEITEM_H

#include "GUI/Model/Descriptor/VectorProperty.h"
#include "GUI/Model/Material/MaterialsSet.h"
#include "GUI/Model/Sample/Item3D.h"
#include "GUI/Model/Type/NamedItem.h"

class ItemWithLayers;
class ItemWithMaterial;
class LayerItem;
class LayerStackItem;

class SampleItem : public virtual Item3D, public NamedItem {
public:
    SampleItem();
    ~SampleItem();

    SampleItem* clone() const;

    std::vector<ItemWithMaterial*> itemsWithMaterial() const;

    void addStandardMaterials();

    VectorProperty& externalField() { return m_external_field; }
    const VectorProperty& externalField() const { return m_external_field; }
    void setExternalField(const R3& r) { m_external_field.setR3(r); }

    LayerStackItem& outerStackItem() const { return *m_outer_stack; }
    void setOuterStackItem(LayerStackItem* outer_stack);

    std::vector<LayerItem*> uniqueLayerItems() const;
    std::vector<ItemWithLayers*> componentItems() const;
    int indexOfComponent(const ItemWithLayers* item) const;
    LayerStackItem* parentOfComponent(const ItemWithLayers* searchedItem);

    //! Creates and inserts a layer at given index inside a stack.
    //!
    //! No properties etc. have been initialized; this has to be done by the caller.
    //! If index = -1, create a layer at the end of the list.
    LayerItem* createLayerItemAt(LayerStackItem& parentStack, int index = -1);
    LayerStackItem* createLayerStackItemAt(LayerStackItem& parentStack, int index = -1);

    void updateTopBottom();
    void removeComponent(const ItemWithLayers* component);
    void moveComponent(ItemWithLayers* component, ItemWithLayers* aboveThisComponent);

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    MaterialsSet& materialModel() { return m_materials; }
    const MaterialsSet& materialModel() const { return m_materials; }

    void updateDefaultLayerColors();
    void adjustLayerSeeds(bool update_all) const;
    void adjustLayoutSeeds() const;

    bool expandInfo = true;
    bool showRoughness = true;
    mutable std::vector<int> seeds;

private:
    VectorProperty m_external_field;
    std::unique_ptr<LayerStackItem> m_outer_stack;
    MaterialsSet m_materials;
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_SAMPLEITEM_H
