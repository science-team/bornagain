//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/SampleValidator.cpp
//! @brief     Implements class SampleValidator.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/SampleValidator.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/CompoundItem.h"
#include "GUI/Model/Sample/CoreAndShellItem.h"
#include "GUI/Model/Sample/InterferenceItems.h"
#include "GUI/Model/Sample/LayerItem.h"
#include "GUI/Model/Sample/ParticleLayoutItem.h"
#include "GUI/Model/Sample/SampleItem.h"

QString SampleValidator::isValidSample(const SampleItem* sample)
{
    QString messages;
    const auto append = [&](const QString& m) { messages.append("* " + m + "\n"); };

    std::vector<LayerItem*> layers = sample->uniqueLayerItems();

    if (layers.empty())
        append("Sample should contain at least one layer.");
    if (layers.size() == 1)
        if (layers.front()->layoutItems().empty())
            append("The single layer in your Sample should contain a particle layout.");

    for (const auto* layer : layers) {
        for (const auto* layout : layer->layoutItems()) {
            if (layout->itemsWithParticles().empty())
                append("Particle layout doesn't contain any particles.");
            for (const auto* particle : layout->containedItemsWithParticles()) {
                if (const auto* p = dynamic_cast<const CoreAndShellItem*>(particle)) {
                    if (!p->coreItem())
                        append("Sim/shell particle doesn't have core defined.");
                    if (!p->shellItem())
                        append("Sim/shell particle doesn't have shell defined.");
                } else if (const auto* p = dynamic_cast<const CompoundItem*>(particle))
                    if (p->itemsWithParticles().empty())
                        append("Particle composition doesn't have any particles.");
            }
        }
    }
    if (!messages.isEmpty())
        messages = "Cannot setup DWBA simulation for given Sample.\n" + messages;

    return messages;
}
