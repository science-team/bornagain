//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/SampleValidator.h
//! @brief     Defines class SampleValidator.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_SAMPLEVALIDATOR_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_SAMPLEVALIDATOR_H

#include <QString>

class SampleItem;

namespace SampleValidator {

QString isValidSample(const SampleItem* sample);

}

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_SAMPLEVALIDATOR_H
