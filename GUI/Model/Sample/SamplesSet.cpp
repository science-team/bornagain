//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/SamplesSet.cpp
//! @brief     Implements class SamplesSet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sample/SamplesSet.h"
#include "GUI/Model/Util/Backup.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString Sample("Sample");
const QString CurrentIndex("CurrentIndex");

} // namespace Tag
} // namespace

SamplesSet::SamplesSet() = default;
SamplesSet::~SamplesSet() = default;

void SamplesSet::writeTo(QXmlStreamWriter* w) const
{
    // samples
    for (const auto* t : *this) {
        XML::writeTaggedElement(w, Tag::Sample, *t);
    }
    XML::writeTaggedValue(w, Tag::CurrentIndex, (int)currentIndex());
}

void SamplesSet::readFrom(QXmlStreamReader* r)
{
    clear();
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::Sample) {
            auto* t = new SampleItem;
            t->readFrom(r);
            add_item(t);
            XML::gotoEndElementOfTag(r, tag);
        } else if (tag == Tag::CurrentIndex)
            setCurrentIndex(XML::readTaggedInt(r, tag));
        else
            r->skipCurrentElement();
    }
}
