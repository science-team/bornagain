//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/SamplesSet.h
//! @brief     Defines class SamplesSet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_SAMPLESSET_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_SAMPLESSET_H

#include "GUI/Model/Sample/SampleItem.h"
#include "GUI/Model/Type/SetWithModel.h"
#include <QXmlStreamReader>

class SampleItem;

//! Main model to hold sample items.

class SamplesSet : public SetWithModel<SampleItem> {
public:
    SamplesSet();
    virtual ~SamplesSet();

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_SAMPLESSET_H
