//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sample/TransientItems.h
//! @brief     Defines classes TransientItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SAMPLE_TRANSIENTITEMS_H
#define BORNAGAIN_GUI_MODEL_SAMPLE_TRANSIENTITEMS_H

#include "Sample/Interface/TransientModels.h"
#include <QXmlStreamReader>
#include <memory>

class TransientItem {
public:
    virtual ~TransientItem() = default;

    virtual void writeTo(QXmlStreamWriter*) const {}
    virtual void readFrom(QXmlStreamReader*) {}

    virtual std::unique_ptr<TransientModel> createModel() const = 0;

protected:
    TransientItem() = default;
};

class ErfTransientItem : public TransientItem {
public:
    ErfTransientItem() = default;
    std::unique_ptr<TransientModel> createModel() const { return std::make_unique<ErfTransient>(); }
};

class TanhTransientItem : public TransientItem {
public:
    TanhTransientItem() = default;
    std::unique_ptr<TransientModel> createModel() const
    {
        return std::make_unique<TanhTransient>();
    }
};

#endif // BORNAGAIN_GUI_MODEL_SAMPLE_TRANSIENTITEMS_H
