//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sim/BackgroundCatalog.cpp
//! @brief     Implements class BackgroundCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sim/BackgroundCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sim/BackgroundItems.h"

BackgroundItem* BackgroundCatalog::create(Type type)
{
    switch (type) {
    case Type::None:
        return new NoBackgroundItem();
    case Type::Constant:
        return new ConstantBackgroundItem();
    case Type::Poisson:
        return new PoissonBackgroundItem();
    }
    ASSERT_NEVER;
}

QVector<BackgroundCatalog::Type> BackgroundCatalog::types()
{
    return {Type::None, Type::Constant, Type::Poisson};
}

UiInfo BackgroundCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::None:
        return {"None", "", ""};
    case Type::Constant:
        return {"Constant background", "", ""};
    case Type::Poisson:
        return {"Poisson noise", "", ""};
    }
    ASSERT_NEVER;
}

BackgroundCatalog::Type BackgroundCatalog::type(const BackgroundItem* item)
{
    if (dynamic_cast<const NoBackgroundItem*>(item))
        return Type::None;
    if (dynamic_cast<const ConstantBackgroundItem*>(item))
        return Type::Constant;
    if (dynamic_cast<const PoissonBackgroundItem*>(item))
        return Type::Poisson;
    ASSERT_NEVER;
}
