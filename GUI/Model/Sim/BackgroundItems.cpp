//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sim/BackgroundItems.cpp
//! @brief     Implements class BackgroundItemes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sim/BackgroundItems.h"
#include "GUI/Model/Util/UtilXML.h"
#include "Sim/Background/ConstantBackground.h"
#include "Sim/Background/PoissonBackground.h"

namespace {
namespace Tag {

const QString BackgroundValue("BackgroundValue");

} // namespace Tag
} // namespace

ConstantBackgroundItem::ConstantBackgroundItem()
{
    m_background_value.init("Background flux", "", "Constant background value", 0.0, 3,
                            RealLimits::nonnegative(), "value");
}

std::unique_ptr<IBackground> ConstantBackgroundItem::createBackground() const
{
    return std::make_unique<ConstantBackground>(m_background_value.dVal());
}

void ConstantBackgroundItem::writeTo(QXmlStreamWriter* w) const
{
    // background value
    m_background_value.writeTo2(w, Tag::BackgroundValue);
}

void ConstantBackgroundItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::BackgroundValue) {
            m_background_value.readFrom2(r, tag);

        } else
            r->skipCurrentElement();
    }
}

std::unique_ptr<IBackground> PoissonBackgroundItem::createBackground() const
{
    return std::make_unique<PoissonBackground>();
}

std::unique_ptr<IBackground> NoBackgroundItem::createBackground() const
{
    return {};
}
