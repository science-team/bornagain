//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sim/BackgroundItems.h
//! @brief     Defines class BackgroundItemes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SIM_BACKGROUNDITEMS_H
#define BORNAGAIN_GUI_MODEL_SIM_BACKGROUNDITEMS_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include <memory>

class IBackground;

class BackgroundItem {
public:
    virtual ~BackgroundItem() = default;

    virtual std::unique_ptr<IBackground> createBackground() const = 0;
    virtual void writeTo(QXmlStreamWriter*) const {}
    virtual void readFrom(QXmlStreamReader*) {}
};

class NoBackgroundItem : public BackgroundItem {
public:
    std::unique_ptr<IBackground> createBackground() const override;
};

class ConstantBackgroundItem : public BackgroundItem {
public:
    ConstantBackgroundItem();
    std::unique_ptr<IBackground> createBackground() const override;

    void writeTo(QXmlStreamWriter* w) const override;
    void readFrom(QXmlStreamReader* r) override;

    DoubleProperty& backgroundValue() { return m_background_value; }
    const DoubleProperty& backgroundValue() const { return m_background_value; }
    void setBackgroundValue(double v) { m_background_value.setDVal(v); }

protected:
    DoubleProperty m_background_value;
};

class PoissonBackgroundItem : public BackgroundItem {
public:
    std::unique_ptr<IBackground> createBackground() const override;
};

#endif // BORNAGAIN_GUI_MODEL_SIM_BACKGROUNDITEMS_H
