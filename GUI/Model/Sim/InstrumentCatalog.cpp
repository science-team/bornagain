//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sim/InstrumentCatalog.cpp
//! @brief     Implements class InstrumentCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sim/InstrumentCatalog.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sim/InstrumentItems.h"

InstrumentItem* InstrumentCatalog::create(Type type)
{
    switch (type) {
    case Type::Scatter2D:
        return new Scatter2DInstrumentItem();
    case Type::Offspec:
        return new OffspecInstrumentItem();
    case Type::Specular:
        return new SpecularInstrumentItem();
    case Type::Depthprobe:
        return new DepthprobeInstrumentItem();
    }
    ASSERT_NEVER;
}

QVector<InstrumentCatalog::Type> InstrumentCatalog::types()
{
    return {Type::Scatter2D, Type::Offspec, Type::Specular, Type::Depthprobe};
}

UiInfo InstrumentCatalog::uiInfo(Type type)
{
    switch (type) {
    case Type::Scatter2D:
        return {"Scatter2D", "", ""};
    case Type::Offspec:
        return {"Offspec", "", ""};
    case Type::Specular:
        return {"Specular", "", ""};
    case Type::Depthprobe:
        return {"Depthprobe", "", ""};
    }
    ASSERT_NEVER;
}

InstrumentCatalog::Type InstrumentCatalog::type(const InstrumentItem* item)
{
    if (dynamic_cast<const Scatter2DInstrumentItem*>(item))
        return Type::Scatter2D;
    if (dynamic_cast<const OffspecInstrumentItem*>(item))
        return Type::Offspec;
    if (dynamic_cast<const SpecularInstrumentItem*>(item))
        return Type::Specular;
    if (dynamic_cast<const DepthprobeInstrumentItem*>(item))
        return Type::Depthprobe;

    ASSERT_NEVER;
}
