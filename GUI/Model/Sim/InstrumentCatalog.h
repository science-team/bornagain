//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sim/InstrumentCatalog.h
//! @brief     Defines class InstrumentCatalog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SIM_INSTRUMENTCATALOG_H
#define BORNAGAIN_GUI_MODEL_SIM_INSTRUMENTCATALOG_H

#include "GUI/Model/Type/UiInfo.h"
#include <QVector>

class InstrumentItem;

class InstrumentCatalog {
public:
    // Do not change the numbering! It is serialized!
    enum class Type : uint8_t { Scatter2D = 0, Offspec = 1, Specular = 2, Depthprobe = 3 };

    //! Creates the item of the given type.
    static InstrumentItem* create(Type type);

    //! List of available types, sorted as expected in the UI.
    static QVector<Type> types();

    //! UiInfo on the given type.
    static UiInfo uiInfo(Type t);

    //! Returns the enum type of the given item.
    static Type type(const InstrumentItem* item);
};

#endif // BORNAGAIN_GUI_MODEL_SIM_INSTRUMENTCATALOG_H
