//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sim/InstrumentXML.h
//! @brief     Defines class InstrumentXML.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SIM_INSTRUMENTXML_H
#define BORNAGAIN_GUI_MODEL_SIM_INSTRUMENTXML_H

#include <QString>

class InstrumentItem;

namespace InstrumentXML {

void save(const QString& fname, const InstrumentItem* t);
InstrumentItem* load(const QString& fname);

} // namespace InstrumentXML

#endif // BORNAGAIN_GUI_MODEL_SIM_INSTRUMENTXML_H
