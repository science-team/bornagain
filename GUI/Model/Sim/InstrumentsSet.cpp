//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sim/InstrumentsSet.cpp
//! @brief     Implement class InstrumentsSet
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sim/InstrumentsSet.h"
#include "GUI/Model/Sim/InstrumentCatalog.h"

namespace {
namespace Tag {

const QString Instrument("Instrument");
const QString CurrentIndex("CurrentIndex");

} // namespace Tag
} // namespace

InstrumentsSet::InstrumentsSet() = default;
InstrumentsSet::~InstrumentsSet() = default;

void InstrumentsSet::writeTo(QXmlStreamWriter* w) const
{
    for (const InstrumentItem* t : *this)
        XML::writeChosen<InstrumentItem, InstrumentCatalog>(t, w, Tag::Instrument);
    XML::writeTaggedValue(w, Tag::CurrentIndex, (int)currentIndex());
}

void InstrumentsSet::readFrom(QXmlStreamReader* r)
{
    clear();
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::Instrument)
            add_item(XML::readChosen<InstrumentItem, InstrumentCatalog>(r, tag));
        else if (tag == Tag::CurrentIndex)
            setCurrentIndex(XML::readTaggedInt(r, tag));
        else
            r->skipCurrentElement();
    }
}

InstrumentItem* InstrumentsSet::findInstrumentItemById(const QString& instrumentId) const
{
    for (InstrumentItem* t : *this)
        if (t->id() == instrumentId)
            return t;
    return nullptr;
}

bool InstrumentsSet::instrumentExists(const QString& instrumentId) const
{
    return findInstrumentItemById(instrumentId) != nullptr;
}
