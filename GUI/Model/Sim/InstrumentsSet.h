//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sim/InstrumentsSet.h
//! @brief     Defines class InstrumentsSet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SIM_INSTRUMENTSSET_H
#define BORNAGAIN_GUI_MODEL_SIM_INSTRUMENTSSET_H

#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/Model/Type/SetWithModel.h"
#include <QXmlStreamReader>

class InstrumentsSet : public SetWithModel<InstrumentItem> {
public:
    InstrumentsSet();
    virtual ~InstrumentsSet();

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    InstrumentItem* findInstrumentItemById(const QString& instrumentId) const;
    bool instrumentExists(const QString& instrumentId) const;
};

#endif // BORNAGAIN_GUI_MODEL_SIM_INSTRUMENTSSET_H
