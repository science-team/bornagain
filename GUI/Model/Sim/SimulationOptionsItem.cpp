//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sim/SimulationOptionsItem.cpp
//! @brief     Defines class SimulationOptionsItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Sim/SimulationOptionsItem.h"
#include "GUI/Model/Util/Backup.h"
#include "GUI/Model/Util/UtilXML.h"
#include <thread>

namespace {
namespace Tag {

const QString RunImmediately("RunImmediately");
const QString NumberOfThreads("NumberOfThreads");
const QString Analytical("Analytical");
const QString NumberOfMonteCarloPoints("NumberOfMonteCarloPoints");
const QString UseAverageMaterials("UseAverageMaterials");
const QString IncludeSpecularPeak("IncludeSpecularPeak");
const QString UseFastMesoAlgorithm("UseFastMesoAlgorithm");
const QString MesoCutoffRadius("MesoCutoffRadius");
const QString UseDataset("UseDataset");
const QString ExpandOptionsGroupbox("ExpandOptionsGroupbox");
const QString ExpandAdvancedOptionsGroupbox("ExpandAdvancedOptionsGroupbox");

} // namespace Tag
} // namespace


SimulationOptionsItem::SimulationOptionsItem()
    : m_number_of_threads(std::thread::hardware_concurrency())
{
    m_meso_radius_factor.init("Cutoff radius factor", "",
                              "Mesocrystal cutoff radius factor for Fourier sum", 2.1, 2, 0.1,
                              RealLimits::limited(0, 1000), "mesocutoff");
}

SimulationOptionsItem::SimulationOptionsItem(const SimulationOptionsItem& source)
{
    GUI::Util::copyContents(&source, this);
}

bool SimulationOptionsItem::useMonteCarloIntegration() const
{
    return !useAnalytical();
}

void SimulationOptionsItem::setUseMonteCarloIntegration(unsigned numberOfPoints)
{
    m_computation_method_analytical = false;
    m_number_of_monte_carlo_points = numberOfPoints;
}

void SimulationOptionsItem::writeTo(QXmlStreamWriter* w) const
{
    XML::writeTaggedValue(w, Tag::RunImmediately, m_run_immediately);
    XML::writeTaggedValue(w, Tag::NumberOfThreads, m_number_of_threads);
    XML::writeTaggedValue(w, Tag::Analytical, m_computation_method_analytical);
    XML::writeTaggedValue(w, Tag::NumberOfMonteCarloPoints, m_number_of_monte_carlo_points);
    XML::writeTaggedValue(w, Tag::UseAverageMaterials, m_use_average_materials);
    XML::writeTaggedValue(w, Tag::IncludeSpecularPeak, m_include_specular_peak);
    XML::writeTaggedValue(w, Tag::UseFastMesoAlgorithm, m_use_meso_reciprocal_sum);
    m_meso_radius_factor.writeTo2(w, Tag::MesoCutoffRadius);
    XML::writeTaggedValue(w, Tag::UseDataset, m_use_dataset);
    XML::writeTaggedValue(w, Tag::ExpandAdvancedOptionsGroupbox, expandAdvancedOptions);
    XML::writeTaggedValue(w, Tag::ExpandOptionsGroupbox, expandOptions);
}

void SimulationOptionsItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::RunImmediately)
            m_run_immediately = XML::readTaggedBool(r, tag);
        else if (tag == Tag::NumberOfThreads)
            m_number_of_threads = XML::readTaggedUInt(r, tag);
        else if (tag == Tag::Analytical)
            m_computation_method_analytical = XML::readTaggedBool(r, tag);
        else if (tag == Tag::NumberOfMonteCarloPoints)
            m_number_of_monte_carlo_points = XML::readTaggedUInt(r, tag);
        else if (tag == Tag::UseAverageMaterials)
            m_use_average_materials = XML::readTaggedBool(r, tag);
        else if (tag == Tag::IncludeSpecularPeak)
            m_include_specular_peak = XML::readTaggedBool(r, tag);
        else if (tag == Tag::UseFastMesoAlgorithm)
            m_use_meso_reciprocal_sum = XML::readTaggedBool(r, tag);
        else if (tag == Tag::MesoCutoffRadius)
            m_meso_radius_factor.readFrom2(r, tag);
        else if (tag == Tag::UseDataset)
            m_use_dataset = XML::readTaggedBool(r, tag);
        else if (tag == Tag::ExpandOptionsGroupbox)
            expandOptions = XML::readTaggedBool(r, tag);
        else if (tag == Tag::ExpandAdvancedOptionsGroupbox)
            expandAdvancedOptions = XML::readTaggedBool(r, tag);
        else
            r->skipCurrentElement();
    }
}
