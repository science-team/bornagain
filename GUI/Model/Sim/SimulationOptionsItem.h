//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Sim/SimulationOptionsItem.h
//! @brief     Defines class SimulationOptionsItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_SIM_SIMULATIONOPTIONSITEM_H
#define BORNAGAIN_GUI_MODEL_SIM_SIMULATIONOPTIONSITEM_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include <QXmlStreamReader>

//! The SimulationOptionsItem class holds simulation status (run policy, number of threads,
//! integration flag). Used in SimulationView to define job settings. When job is started,
//! item is copied to the job.

class SimulationOptionsItem {
public:
    SimulationOptionsItem();
    SimulationOptionsItem(const SimulationOptionsItem& source);

    void setNumberOfThreads(unsigned n) { m_number_of_threads = n; }
    unsigned numberOfThreads() const { return m_number_of_threads; }

    void setRunImmediately(bool b) { m_run_immediately = b; }
    bool runImmediately() const { return m_run_immediately; }

    bool useMonteCarloIntegration() const;
    void setUseMonteCarloIntegration(unsigned numberOfPoints);

    void setUseAnalytical() { m_computation_method_analytical = true; }
    bool useAnalytical() const { return m_computation_method_analytical; }

    unsigned numberOfMonteCarloPoints() const { return m_number_of_monte_carlo_points; }

    void setFastMesocrystalCalc(bool b) { m_use_meso_reciprocal_sum = b; }
    bool useFastMesocrystalCalc() const { return m_use_meso_reciprocal_sum; }

    void setMesocrystalCutoff(double d) { m_meso_radius_factor.setDVal(d); }
    DoubleProperty& mesocrystalCutoff() { return m_meso_radius_factor; }
    const DoubleProperty& mesocrystalCutoff() const { return m_meso_radius_factor; }

    void setUseAverageMaterials(bool b) { m_use_average_materials = b; }
    bool useAverageMaterials() const { return m_use_average_materials; }

    void setIncludeSpecularPeak(bool b) { m_include_specular_peak = b; }
    bool includeSpecularPeak() const { return m_include_specular_peak; }

    void setUseDataset(bool b) { m_use_dataset = b; }
    bool useDataset() const { return m_use_dataset; }

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

    bool expandOptions = true;
    bool expandAdvancedOptions = false;

private:
    bool m_run_immediately = true;
    unsigned m_number_of_threads;
    bool m_computation_method_analytical = true;
    unsigned m_number_of_monte_carlo_points = 100;
    bool m_use_average_materials = true;
    bool m_include_specular_peak = false;
    bool m_use_meso_reciprocal_sum = false;
    DoubleProperty m_meso_radius_factor;
    bool m_use_dataset = true;
};

#endif // BORNAGAIN_GUI_MODEL_SIM_SIMULATIONOPTIONSITEM_H
