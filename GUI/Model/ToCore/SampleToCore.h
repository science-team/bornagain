//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/ToCore/SampleToCore.h
//! @brief     Declares part of namespace GUI::ToCore.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_TOCORE_SAMPLETOCORE_H
#define BORNAGAIN_GUI_MODEL_TOCORE_SAMPLETOCORE_H

#include <memory>

class Sample;
class SampleItem;

namespace GUI::ToCore {

std::unique_ptr<Sample> itemToSample(const SampleItem& item);

} // namespace GUI::ToCore

#endif // BORNAGAIN_GUI_MODEL_TOCORE_SAMPLETOCORE_H
