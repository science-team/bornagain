//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/ToCore/SimulationToCore.cpp
//! @brief     Implements part of namespace GUI::ToCore.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/ToCore/SimulationToCore.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/SampleItem.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/Model/Sim/SimulationOptionsItem.h"
#include "GUI/Model/ToCore/SampleToCore.h"
#include "Resample/Option/SimulationOptions.h"
#include "Sample/Multilayer/Sample.h"
#include "Sim/Simulation/ISimulation.h"

void GUI::ToCore::setSimulationOptions(ISimulation* simulation, const SimulationOptionsItem& item)
{
    simulation->options().setNumberOfThreads(item.numberOfThreads());
    if (item.useMonteCarloIntegration())
        simulation->options().setMonteCarloIntegration(true, item.numberOfMonteCarloPoints());
    simulation->options().setUseAvgMaterials(item.useAverageMaterials());
    simulation->options().setIncludeSpecular(item.includeSpecularPeak());
    simulation->options().setMesoOptions(item.useFastMesocrystalCalc(),
                                         item.mesocrystalCutoff().dVal());
}

std::unique_ptr<ISimulation>
GUI::ToCore::itemsToSimulation(const SampleItem* sampleItem, const InstrumentItem* instrumentItem,
                               const SimulationOptionsItem* optionsItem)
{
    ASSERT(sampleItem && instrumentItem && optionsItem);
    std::unique_ptr<Sample> sample = GUI::ToCore::itemToSample(*sampleItem);
    std::unique_ptr<ISimulation> result(instrumentItem->createSimulation(*sample));

    setSimulationOptions(result.get(), *optionsItem);

    return result;
}
