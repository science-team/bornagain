//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/ToCore/SimulationToCore.h
//! @brief     Defines part of namespace GUI::ToCore.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_TOCORE_SIMULATIONTOCORE_H
#define BORNAGAIN_GUI_MODEL_TOCORE_SIMULATIONTOCORE_H

#include <memory>

class ISimulation;
class InstrumentItem;
class SampleItem;
class SimulationOptionsItem;

//! Contains functions to build the domain simulation from instrument and sample models.

namespace GUI::ToCore {

//! Creates domain simulation from sample and instrument items.
std::unique_ptr<ISimulation> itemsToSimulation(const SampleItem* sampleItem,
                                               const InstrumentItem* instrumentItem,
                                               const SimulationOptionsItem* optionsItem);

void setSimulationOptions(ISimulation* simulation, const SimulationOptionsItem& item);

} // namespace GUI::ToCore

#endif // BORNAGAIN_GUI_MODEL_TOCORE_SIMULATIONTOCORE_H
