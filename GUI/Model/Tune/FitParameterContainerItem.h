//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Tune/FitParameterContainerItem.h
//! @brief     Defines class FitParameterContainerItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_TUNE_FITPARAMETERCONTAINERITEM_H
#define BORNAGAIN_GUI_MODEL_TUNE_FITPARAMETERCONTAINERITEM_H

#include "GUI/Model/Tune/FitParameterItem.h"
#include "GUI/Model/Tune/FitParameterLinkItem.h"
#include <QObject>
#include <QXmlStreamReader>

namespace mumufit {
class Parameters;
}

class ParameterItem;

//! The FitParameterContainerItem class is a collection of all defined fit parameters in JobItem.

class FitParameterContainerItem : public QObject {
    Q_OBJECT
public:
    explicit FitParameterContainerItem(QObject* parent = nullptr);

    //! Creates fit parameter from given ParameterItem, sets starting value to the value
    //! of ParameterItem, copies link.
    void createFitParameter(ParameterItem* parameterItem);

    void removeFitParameter(FitParameterItem* fitPar);

    //! Adds given parameterItem to the existing fit parameter with display name fitParName.
    //! If parameterItem is already linked with another fitParameter, it will be relinked
    void addToFitParameter(ParameterItem* parameterItem, const QString& fitParName);

    //! get the fit parameter item whose link matches the given link.
    //!
    //! The link is a ParameterItem's path
    FitParameterItem* fitParameterItem(const QString& link) const;

    //! get the fit parameter item which links the given parameterItem.
    FitParameterItem* fitParameterItem(const ParameterItem* parameterItem) const;

    QVector<FitParameterItem*> fitParameterItems() const;

    //! Returns list of fit parameter display names
    QStringList fitParameterNames() const;

    void removeLink(const ParameterItem* parameterItem) const;

    FitParameterItem* createBareFitParameterItem();

    bool isEmpty() const;
    void setValuesInParameterContainer(const std::vector<double>& values,
                                       class ParameterContainerItem* parameterContainer) const;
    void pullValuesFromParameterContainer(class ParameterContainerItem* parameterContainer);

    mumufit::Parameters createParameters() const;

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

private:
    FitParameterItem* oldFitParameterItemToBeRemoved(ParameterItem* parameterItem) const;
    void updateFitParameterNames();

signals:
    void fitItemChanged();

private:
    OwningVector<FitParameterItem> m_fit_parameter_items;
};

#endif // BORNAGAIN_GUI_MODEL_TUNE_FITPARAMETERCONTAINERITEM_H
