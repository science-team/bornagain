//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Tune/FitParameterLinkItem.cpp
//! @brief     Implements class FitParameterLinkItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Tune/FitParameterLinkItem.h"
#include "GUI/Model/Util/UtilXML.h"

namespace {
namespace Tag {

const QString Link("Link");
const QString Title("Title");
const QString LinkItem("LinkItem");

} // namespace Tag
} // namespace


LinkItem::LinkItem(QObject* parent)
    : QObject(parent)
    , m_link("")
{
    setObjectName("Link");
}

void LinkItem::writeTo(QXmlStreamWriter* w) const
{
    XML::writeTaggedValue(w, Tag::Link, m_link);
}

void LinkItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::Link)
            m_link = XML::readTaggedString(r, tag);
        else
            r->skipCurrentElement();
    }
}

//------------------------------------------------------------------------------------------------

FitParameterLinkItem::FitParameterLinkItem(QObject* parent)
    : QObject(parent)
    , m_title("")
    , m_link_item(new LinkItem(this))
{
    setObjectName("FitParameterLink");
}

void FitParameterLinkItem::writeTo(QXmlStreamWriter* w) const
{
    XML::writeTaggedValue(w, Tag::Title, m_title);
    XML::writeTaggedElement(w, Tag::LinkItem, *m_link_item);
}

void FitParameterLinkItem::readFrom(QXmlStreamReader* r)
{
    while (r->readNextStartElement()) {
        QString tag = r->name().toString();
        if (tag == Tag::Title)
            m_title = XML::readTaggedString(r, tag);
        else if (tag == Tag::LinkItem)
            XML::readTaggedElement(r, tag, *m_link_item);
        else
            r->skipCurrentElement();
    }
}
