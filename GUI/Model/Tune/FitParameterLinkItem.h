//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Tune/FitParameterLinkItem.h
//! @brief     Defines class FitParameterLinkItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_TUNE_FITPARAMETERLINKITEM_H
#define BORNAGAIN_GUI_MODEL_TUNE_FITPARAMETERLINKITEM_H

#include <QObject>
#include <QXmlStreamReader>

//! Holds link value, accessible from FitparameterModel.
class LinkItem : public QObject {
    Q_OBJECT
public:
    explicit LinkItem(QObject* parent = nullptr);

    QString link() const { return m_link; }
    void setLink(QString link) { m_link = link; }

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

private:
    QString m_link;
};


//! Holds a link to ParameterItem in tuning tree.
class FitParameterLinkItem : public QObject {
    Q_OBJECT
public:
    explicit FitParameterLinkItem(QObject* parent = nullptr);

    QString link() const { return m_link_item->link(); }
    void setLink(const QString& l) { m_link_item->setLink(l); }

    QString title() const { return m_title; }
    void setTitle(const QString& t) { m_title = t; }
    LinkItem* linkItem() const { return m_link_item; }

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

private:
    QString m_title;
    LinkItem* m_link_item;
};

#endif // BORNAGAIN_GUI_MODEL_TUNE_FITPARAMETERLINKITEM_H
