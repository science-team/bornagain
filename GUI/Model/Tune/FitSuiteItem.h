//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Tune/FitSuiteItem.h
//! @brief     Defines class FitSuiteItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_TUNE_FITSUITEITEM_H
#define BORNAGAIN_GUI_MODEL_TUNE_FITSUITEITEM_H

#include <QXmlStreamReader>

class FitParameterContainerItem;
class MinimizerContainerItem;

class FitSuiteItem : public QObject {
    Q_OBJECT
public:
    FitSuiteItem();
    ~FitSuiteItem();

    FitParameterContainerItem* fitParameterContainerItem() { return m_fit_container.get(); }
    MinimizerContainerItem* minimizerContainerItem() { return m_minimizer_container.get(); }

    int updateInterval() const { return m_update_interval; }
    void setUpdateInterval(int interval);

    int iterationCount() const { return m_iter_count; }
    void setIterationCount(int count);

    double chi2() const { return m_chi2; }
    void setChi2(double chi2);

    void writeTo(QXmlStreamWriter* w) const;
    void readFrom(QXmlStreamReader* r);

signals:
    void iterationCountChanged(int count);
    void updateIntervalChanged(int interval);

private:
    int m_update_interval;
    int m_iter_count;
    double m_chi2;
    std::unique_ptr<FitParameterContainerItem> m_fit_container;
    std::unique_ptr<MinimizerContainerItem> m_minimizer_container;
};

#endif // BORNAGAIN_GUI_MODEL_TUNE_FITSUITEITEM_H
