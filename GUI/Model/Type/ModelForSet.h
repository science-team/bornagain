//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Type/ModelForSet.h
//! @brief     Defines and implements templated class ModelForSet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_TYPE_MODELFORSET_H
#define BORNAGAIN_GUI_MODEL_TYPE_MODELFORSET_H

#include "GUI/Model/Type/PredefinedColors.h"
#include <QAbstractListModel>

template <typename T> class SetWithModel;

//! Data model for a set of NamedItem%s.

template <typename T> class ModelForSet : public QAbstractListModel {
public:
    explicit ModelForSet(SetWithModel<T>* set)
        : m_set(set)
    {
    }

    int rowCount(const QModelIndex&) const override { return m_set->size(); }
    QVariant data(const QModelIndex& index, int role) const override
    {
        if (!index.isValid())
            return {};
        size_t row = index.row();
        if (row >= m_set->size())
            return {};
        const T* t = m_set->at(row);

        switch (role) {
        case Qt::DisplayRole:
            return t->name();
        case Qt::ToolTipRole:
            return t->description();
        case Qt::BackgroundRole:
            return GUI::Colors::listBackground(row == m_set->currentIndex());
        default:
            return {};
        }
    }

    friend SetWithModel<T>; // give access to protected fcts QAbstractListModel::beginInsertRows etc

private:
    SetWithModel<T>* m_set;
};

#endif // BORNAGAIN_GUI_MODEL_TYPE_MODELFORSET_H
