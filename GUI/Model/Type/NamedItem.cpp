//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Type/NamedItem.cpp
//! @brief     Implements class NamedItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Type/NamedItem.h"
#include "Base/Util/Assert.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>

namespace {

QStringList splitName(const QString& s)
{
    QRegularExpression pattern("(.*)_(\\d+)$");
    QRegularExpressionMatch match = pattern.match(s);
    if (match.hasMatch()) {
        ASSERT(match.lastCapturedIndex() == 2);
        return {match.captured(1), match.captured(2)};
    }
    return {}; // s does not contain '_\d+'
}

} // namespace


void NamedItem::renumber(const QStringList& extant_names)
{
    // Item name consists of a stem and an optional number
    QStringList ns = ::splitName(name());
    QString stem = ns.isEmpty() ? name() : ns[0];

    // Determine highest number for given stem in extant_items
    int imax = 0;
    for (const QString& tname : extant_names) {
        QStringList ts = ::splitName(tname);
        if (ts.isEmpty()) {
            if (tname == stem)
                imax = std::max(imax, 1);
        } else {
            if (ts[0] == stem)
                imax = std::max(imax, ts[1].toInt());
        }
    }

    if (imax != 0)
        setName(stem + "_" + QString::number(imax + 1));

    if (name().contains(' '))
        setName(name().replace(' ', '_'));
}
