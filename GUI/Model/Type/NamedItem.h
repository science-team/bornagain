//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Type/NamedItem.h
//! @brief     Defines class NamedItem.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_TYPE_NAMEDITEM_H
#define BORNAGAIN_GUI_MODEL_TYPE_NAMEDITEM_H

#include <QString>
#include <vector>

//! Base class of items that have a name and a description.

class NamedItem {
public:
    NamedItem(const QString& name = {})
        : m_name(name)
    {
    }
    virtual ~NamedItem() = default;

    void setName(const QString& name) { m_name = name; }
    QString name() const { return m_name; }

    QString description() const { return m_description; }
    void setDescription(const QString& description) { m_description = description; }

    //! Changes name of item to avoid duplication of any name from extant items.
    //! Also replaces blank by underscore characters.
    void renumber(const QStringList& extant_names);

private:
    QString m_name;
    QString m_description;
};

#endif // BORNAGAIN_GUI_MODEL_TYPE_NAMEDITEM_H
