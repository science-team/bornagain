//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Type/PredefinedColors.cpp
//! @brief     Implements namespace GUI::Colors.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Type/PredefinedColors.h"
#include <QWidget>

const QVector<QColor>& GUI::Colors::layerDefaults()
{
    static QVector<QColor> colors = {QColor(230, 255, 213), QColor(194, 252, 240),
                                     QColor(239, 228, 176), QColor(200, 191, 231),
                                     QColor(253, 205, 193), QColor(224, 193, 253)};

    return colors;
}

const QColor& GUI::Colors::listBackground(bool isCurrent)
{
    static QVector<QColor> values = {QWidget().palette().color(QPalette::Button),
                                     QColor(58, 147, 255)};
    return values.at(isCurrent);
}
