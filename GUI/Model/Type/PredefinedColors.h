//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Type/PredefinedColors.h
//! @brief     Defines namespace GUI::Colors.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_TYPE_PREDEFINEDCOLORS_H
#define BORNAGAIN_GUI_MODEL_TYPE_PREDEFINEDCOLORS_H

#include <QColor>
#include <QVector>

//! Utility functions to support layer oriented sample editor
namespace GUI::Colors {

const QVector<QColor>& layerDefaults();

const QColor& listBackground(bool isCurrent);

} // namespace GUI::Colors

#endif // BORNAGAIN_GUI_MODEL_TYPE_PREDEFINEDCOLORS_H
