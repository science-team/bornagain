//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Type/SetWithModel.cpp
//! @brief     Implements class AbstractSetModel, to enforce vtable generation.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Type/SetWithModel.h"

AbstractSetModel::AbstractSetModel() = default;

AbstractSetModel::~AbstractSetModel() = default;
