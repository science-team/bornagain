//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Type/UiInfo.h
//! @brief     Defines class UiInfo.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_TYPE_UIINFO_H
#define BORNAGAIN_GUI_MODEL_TYPE_UIINFO_H

#include <QString>

//! Defines structure to represent base information in UI about cataloged classes

struct UiInfo {
    QString menuEntry;
    QString description;
    QString iconPath;
};

#endif // BORNAGAIN_GUI_MODEL_TYPE_UIINFO_H
