//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Util/Path.h
//! @brief     Defines class Helpers functions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_UTIL_PATH_H
#define BORNAGAIN_GUI_MODEL_UTIL_PATH_H

#include <QModelIndex>
#include <QString>

namespace GUI::Path {

QString withTildeHomePath(const QString& path);

QString getBornAgainVersionString();

QString getValidFileName(const QString& proposed_name);

QString intensityDataFileName(const QString& itemName, const QString& prefix);

QString fileDir(const QString& fname);
QString baseName(const QString& fname);

bool parseVersion(const QString& version, int& major_num, int& minor_num);

int versionCode(const QString& version);

bool isVersionMatchMinimal(const QString& version, const QString& minimal_version);

} // namespace GUI::Path

#endif // BORNAGAIN_GUI_MODEL_UTIL_PATH_H
