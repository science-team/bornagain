//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Util/String.cpp
//! @brief     Implements functions from Utils namespace.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/Model/Util/String.h"
#include <QDir>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

QStringList GUI::Util::String::fromStdStrings(const std::vector<std::string>& container)
{
    QStringList result;
    for (const std::string& str : container)
        result.append(QString::fromStdString(str));
    return result;
}
