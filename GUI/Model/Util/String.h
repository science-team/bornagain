//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/Model/Util/String.h
//! @brief     Defines functions from namespace GUI::Util::String.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_MODEL_UTIL_STRING_H
#define BORNAGAIN_GUI_MODEL_UTIL_STRING_H

#include <QString>
#include <vector>

namespace GUI::Util::String {

QStringList fromStdStrings(const std::vector<std::string>& container);

} // namespace GUI::Util::String

#endif // BORNAGAIN_GUI_MODEL_UTIL_STRING_H
