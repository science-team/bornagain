//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/ActionFactory.cpp
//! @brief     Implements class ActionFactory.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Base/ActionFactory.h"

QAction* ActionFactory::createRemoveAction(QObject* parent, const QString& what,
                                           std::function<void()> slot)
{
    auto* removeAction = new QAction(parent);
    removeAction->setText("Remove");
    removeAction->setIcon(QIcon(":/images/delete.svg"));
    removeAction->setIconText("Remove");
    removeAction->setToolTip("Remove " + what);

    if (slot)
        QObject::connect(removeAction, &QAction::triggered, slot);

    return removeAction;
}

QAction* ActionFactory::createDuplicateAction(QObject* parent, const QString& what,
                                              std::function<void()> slot)
{
    auto* duplicateAction = new QAction(parent);
    duplicateAction->setText("Duplicate");
    duplicateAction->setIcon(QIcon(":/images/content-copy.svg"));
    duplicateAction->setIconText("Duplicate");
    duplicateAction->setToolTip("Duplicate " + what);

    if (slot)
        QObject::connect(duplicateAction, &QAction::triggered, slot);

    return duplicateAction;
}

QAction* ActionFactory::createShowInRealspaceAction(QObject* parent, const QString& what,
                                                    std::function<void()> slot)
{
    auto* action = new QAction(parent);
    action->setText("Show in Real Space (3D) view");
    action->setIcon(QIcon(":/images/rotate-3d.svg"));
    action->setIconText("3D");
    action->setToolTip("Show " + what + " in Real Space (3D) view");

    if (slot)
        QObject::connect(action, &QAction::triggered, slot);

    return action;
}
