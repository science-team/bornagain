//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/ActionFactory.h
//! @brief     Defines class ActionFactory.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_BASE_ACTIONFACTORY_H
#define BORNAGAIN_GUI_VIEW_BASE_ACTIONFACTORY_H

#include <QAction>
#include <QObject>
#include <QString>
#include <functional>

//! Factory to create commonly used actions
namespace ActionFactory {

//! Create "remove" action.
//!
//! The "what" text will be used in the tooltip, appended to "Remove ".
//! If a slot is given, it will be connected to the "triggered" signal.
QAction* createRemoveAction(QObject* parent, const QString& what,
                            std::function<void()> slot = nullptr);
//! Create "duplicate" action.
//!
//! The "what" text will be used in the tooltip, appended to "Duplicate ".
//! If a slot is given, it will be connected to the "triggered" signal.
QAction* createDuplicateAction(QObject* parent, const QString& what,
                               std::function<void()> slot = nullptr);

//! Create "show in Realspace" action.
//!
//! The "what" text will be used in the tooltip.
//! If a slot is given, it will be connected to the "triggered" signal.
QAction* createShowInRealspaceAction(QObject* parent, const QString& what,
                                     std::function<void()> slot = nullptr);

} // namespace ActionFactory

#endif // BORNAGAIN_GUI_VIEW_BASE_ACTIONFACTORY_H
