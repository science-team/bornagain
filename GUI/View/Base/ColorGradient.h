//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/ColorGradient.h
//! @brief     Defines namespace QCP_Color.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_BASE_COLORGRADIENT_H
#define BORNAGAIN_GUI_VIEW_BASE_COLORGRADIENT_H

class QCPColorGradient;

//! Provides colors for use in QCustomPlot.

namespace GUI::QCP_Color {

QCPColorGradient colorGradientInferno();

} // namespace GUI::QCP_Color

#endif // BORNAGAIN_GUI_VIEW_BASE_COLORGRADIENT_H
