//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/CustomEventFilters.h
//! @brief     Defines classes releted to event filtering.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_BASE_CUSTOMEVENTFILTERS_H
#define BORNAGAIN_GUI_VIEW_BASE_CUSTOMEVENTFILTERS_H

#include <QObject>

//! Filter out space bar key events, which is special case for dialog windows.

class SpaceKeyEater : public QObject {
    Q_OBJECT
public:
    SpaceKeyEater(QObject* parent = nullptr);

protected:
    bool eventFilter(QObject* obj, QEvent* event) override;
};

//! Event filter to install on combo boxes and spin boxes to not
//! to react on wheel events during scrolling of InstrumentComponentWidget.

class WheelEventEater : public QObject {
    Q_OBJECT
public:
    static void install(QObject* obj);
    WheelEventEater(QObject* parent = nullptr);

protected:
    bool eventFilter(QObject* obj, QEvent* event) override;
};

//! Lisens for press-del-key events

class DeleteEventFilter : public QObject {
    Q_OBJECT
public:
    DeleteEventFilter(QObject* parent = nullptr)
        : QObject(parent)
    {
    }

protected:
    bool eventFilter(QObject* dist, QEvent* event) override;

signals:
    void removeItem();
};

//! Event filter to prevent lost of focus by custom material editor.

class LostFocusFilter : public QObject {
    Q_OBJECT
public:
    LostFocusFilter(QObject* parent = nullptr);

protected:
    bool eventFilter(QObject* obj, QEvent* event) override;
};

//! Event filter for global tracking of shortcodes.

class ShortcodeFilter : public QObject {
    Q_OBJECT
public:
    ShortcodeFilter(const QString& shortcode, QObject* parent = nullptr);

signals:
    void found();

protected:
    bool eventFilter(QObject* obj, QEvent* event) override;
    QString m_shortcode;
    int m_index;
};

//! Filter out right mouse button events.

class RightMouseButtonEater : public QObject {
    Q_OBJECT
public:
    RightMouseButtonEater(QObject* parent = nullptr);

protected:
    bool eventFilter(QObject* obj, QEvent* event) override;
};

//! Propagate tab events from focusProxy to parent.

class TabFromFocusProxy : public QObject {
    Q_OBJECT
public:
    TabFromFocusProxy(QWidget* parent = nullptr);

protected:
    bool eventFilter(QObject* obj, QEvent* event) override;
    QWidget* m_parent;
};

#endif // BORNAGAIN_GUI_VIEW_BASE_CUSTOMEVENTFILTERS_H
