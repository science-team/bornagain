//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/Fontsize.cpp
//! @brief     Implements functions in namespace GUI::Style, concerned with font size.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Base/Fontsize.h"
#include "Base/Util/Assert.h"
#include <QApplication>

namespace {

//! Calculates size of letter `M` for current system font settings.

QSize FindSizeOfLetterM(const QWidget* widget)
{
    QFontMetrics fontMetric(widget->font());
    auto em = fontMetric.horizontalAdvance('M');
    auto fontAscent = fontMetric.ascent();

    return {em, fontAscent};
}

QSize DefaultSizeOfLetterM()
{
    QWidget widget;
    return FindSizeOfLetterM(&widget);
}

} // namespace


void GUI::Style::setResizable(QDialog*
#ifdef Q_OS_MAC
                                  dialog
#endif
)
{
#ifdef Q_OS_MAC
    dialog->setWindowFlags(Qt::WindowCloseButtonHint | Qt::CustomizeWindowHint
                           | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint
                           | Qt::Window);
#endif
}

QSize GUI::Style::SizeOfLetterM(const QWidget* widget)
{
    static QSize default_size = DefaultSizeOfLetterM();
    return widget ? FindSizeOfLetterM(widget) : default_size;
}

int GUI::Style::fontSizeRegular()
{
    return QApplication::font().pointSize();
}

int GUI::Style::fontSizeLarge()
{
    return fontSizeRegular() * 1.2;
}

int GUI::Style::fontSizeSmall()
{
    return fontSizeRegular() * 0.9;
}

int GUI::Style::fontSizeSmaller()
{
    return fontSizeRegular() * 0.7;
}
