//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/Fontsize.h
//! @brief     Declares functions in namespace GUI::Style, concerned with font size.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_BASE_FONTSIZE_H
#define BORNAGAIN_GUI_VIEW_BASE_FONTSIZE_H

#include <QDialog>
#include <QFont>
#include <QWidget>

//! Mostly concerned with fonts

namespace GUI::Style {

//! Make modal dialog resizable.
void setResizable(QDialog* dialog);

//... TODO if the above is removed, then rename the remaining namespace -> GUI::Font

//! Returns size of largest letter of default system font.
QSize SizeOfLetterM(const QWidget* widget = nullptr);

//! Returns size in points of default system font.
int fontSizeRegular();

int fontSizeLarge();
int fontSizeSmall();
int fontSizeSmaller();

} // namespace GUI::Style

#endif // BORNAGAIN_GUI_VIEW_BASE_FONTSIZE_H
