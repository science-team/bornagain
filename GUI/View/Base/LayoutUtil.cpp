//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/LayoutUtil.cpp
//! @brief     Implements namespace LayoutUtils.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Base/LayoutUtil.h"
#include <QLayoutItem>
#include <QWidget>

void GUI::Util::Layout::clearLayout(QLayout* layout, bool deleteWidgets)
{
    if (!layout)
        return;

    while (layout->count() > 0) {
        QLayoutItem* item = layout->takeAt(0);
        if (deleteWidgets)
            delete item->widget();
        if (QLayout* childLayout = item->layout())
            GUI::Util::Layout::clearLayout(childLayout, deleteWidgets);
        delete item;
    }
}
