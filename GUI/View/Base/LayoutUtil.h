//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/LayoutUtil.h
//! @brief     Defines namespace GUI::Util::Layout.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_BASE_LAYOUTUTIL_H
#define BORNAGAIN_GUI_VIEW_BASE_LAYOUTUTIL_H

#include <QLayout>

namespace GUI::Util::Layout {

//! Removes content from box layout.
void clearLayout(QLayout* layout, bool deleteWidgets = true);

} // namespace GUI::Util::Layout

#endif // BORNAGAIN_GUI_VIEW_BASE_LAYOUTUTIL_H
