//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/Stylesheet.cpp
//! @brief     Implements function in namespace GUI::Style, concerned with palette and stylesheet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Base/Stylesheet.h"
#include <QApplication>
#include <QFile>
#include <QPalette>

void GUI::Style::setInitialStyle()
{
    // The palette is used by the style sheet, but cannot be set there.
    // Therefore it is hard-coded here:
    QPalette p = QGuiApplication::palette();
    p.setColor(QPalette::Text, Qt::black);
    p.setColor(QPalette::WindowText, Qt::black);
    p.setColor(QPalette::Base, Qt::white);
    p.setColor(QPalette::AlternateBase, QColor(Qt::white).darker(110));
    p.setColor(QPalette::Light, QColor(Qt::white).darker(105));
    p.setColor(QPalette::Button, QColor(Qt::white).darker(115));
    p.setColor(QPalette::Mid, QColor(Qt::white).darker(125));
    p.setColor(QPalette::Dark, QColor(Qt::white).darker(135));
    p.setColor(QPalette::ToolTipBase, QColor(174, 248, 255));
    QApplication::setPalette(p);

    QFile base(":/styles/Base.stylesheet");
    base.open(QFile::ReadOnly);
    QString stylesheet = base.readAll();
    qApp->setStyleSheet(stylesheet);
}
