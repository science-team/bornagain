//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/Stylesheet.h
//! @brief     Defines function in namespace GUI::Style, concerned with palette and stylesheet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_BASE_STYLESHEET_H
#define BORNAGAIN_GUI_VIEW_BASE_STYLESHEET_H

namespace GUI::Style {

//! Sets the color palette, and loads the style sheet.
void setInitialStyle();

} // namespace GUI::Style

#endif // BORNAGAIN_GUI_VIEW_BASE_STYLESHEET_H
