//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Base/mainwindow_constants.h
//! @brief     Defines constants in namespace GUI::Style.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_BASE_MAINWINDOW_CONSTANTS_H
#define BORNAGAIN_GUI_VIEW_BASE_MAINWINDOW_CONSTANTS_H

#include <QString>

namespace GUI::Style {

// Settings groups
const char S_MAIN_WINDOW[] = "MainWindow";
const char S_JOB_MESSAGE_PANEL[] = "JobMessagePanel";
const char S_JOB_SELECTOR_PANEL[] = "JobsPanelPanel";
const char S_FIT_ACTIVITY_PANEL[] = "FitActivityPanel";
const char S_FIT_SESSION_WIDGET[] = "FitSessionWidget";
const char S_SAMPLE_3DVIEW_WIDGET[] = "Sample3DviewWidget";
const char S_PARAMETER_TUNING_WIDGET[] = "ParameterTuningWidget";
const char S_INSTRUMENT_LIBRARY_EDITOR[] = "InstrumentXMLDialog";

// Settings keys
const char S_WINDOW_SIZE[] = "size";
const char S_WINDOW_POSITION[] = "pos";

const char S_JOB_MESSAGE_PANEL_HEIHGT[] = "height";
const char S_JOB_SELECTOR_PANEL_WIDTH[] = "width";
const char S_JOB_SELECTOR_SPLITTER_SIZES[] = "splitter_sizes";
const char S_FIT_ACTIVITY_PANEL_SIZE[] = "size";
const char S_FIT_SESSION_WIDGET_CURRENT_TAB[] = "current_tab";
const char S_SAMPLE_3DVIEW_WIDGET_SIZE[] = "size";
const char S_PARAMETER_TUNING_WIDGET_COLUMN_WIDTH[] = "width";
const char S_INSTRUMENT_LIBRARY_EDITOR_TREE_WIDTH[] = "width";

// Hints

const unsigned int FIT_ACTIVITY_PANEL_WIDTH = 480;
const unsigned int FIT_ACTIVITY_PANEL_HEIGHT = 380;
const unsigned int JOB_MESSAGE_PANEL_HEIGHT = 400;
const unsigned int JOB_SELECTOR_PANEL_WIDTH = 231;
const unsigned int RUN_FIT_CONTROL_WIDGET_HEIGHT = 50;
const unsigned int SAMPLE_3DVIEW_WIDGET_WIDTH = 500;
const unsigned int SAMPLE_3DVIEW_WIDGET_HEIGHT = 400;
const unsigned int PARAMETER_TUNING_WIDGET_COLUMN_WIDTH = 220;
const unsigned int INSTRUMENT_LIBRARY_EDITOR_TREE_WIDTH = 250;

} // namespace GUI::Style

#endif // BORNAGAIN_GUI_VIEW_BASE_MAINWINDOW_CONSTANTS_H
