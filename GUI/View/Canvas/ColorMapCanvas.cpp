//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Canvas/ColorMapCanvas.cpp
//! @brief     Declares class ColorMapCanvas.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Canvas/ColorMapCanvas.h"
#include "GUI/Model/Data/Data2DItem.h"
#include "GUI/View/Plotter/ColorMap.h"
#include "GUI/View/Plotter/PlotStatusLabel.h"
#include <QVBoxLayout>

ColorMapCanvas::ColorMapCanvas(bool withStatusLabel)
    : m_plot(new ColorMap)
{
    auto* layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    layout->addWidget(m_plot);

    if (withStatusLabel) {
        auto status_label = new PlotStatusLabel({m_plot});
        layout->addWidget(status_label);
    }
}

void ColorMapCanvas::itemToCanvas(Data2DItem* item)
{
    m_plot->itemToMap(item);
}
