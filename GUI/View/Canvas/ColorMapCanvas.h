//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Canvas/ColorMapCanvas.h
//! @brief     Defines class ColorMapCanvas.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_CANVAS_COLORMAPCANVAS_H
#define BORNAGAIN_GUI_VIEW_CANVAS_COLORMAPCANVAS_H

#include <QWidget>

class ColorMap;
class Data2DItem;
class QCustomPlot;

//! Contains ColorMap for intensity data presentation, and provides control of font size.
//! Controls appearance of the status string.

class ColorMapCanvas : public QWidget {
    Q_OBJECT
public:
    ColorMapCanvas(bool withStatusLabel = true);

    void itemToCanvas(Data2DItem* item);

    ColorMap* colorMap() { return m_plot; }

private:
    ColorMap* m_plot;
};

#endif // BORNAGAIN_GUI_VIEW_CANVAS_COLORMAPCANVAS_H
