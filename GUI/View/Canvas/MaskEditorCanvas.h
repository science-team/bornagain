//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Canvas/MaskEditorCanvas.h
//! @brief     Defines class MaskEditorCanvas.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_CANVAS_MASKEDITORCANVAS_H
#define BORNAGAIN_GUI_VIEW_CANVAS_MASKEDITORCANVAS_H

#include "GUI/View/Setup/Canvas2DMode.h"
#include <QWidget>
#include <memory>

class Data2DItem;
class Datafield;
class LineItem;
class MaskGraphicsScene;
class MaskGraphicsView;
class PlotStatusLabel;

//! Painting widget for MaskEditor, contains graphics scene and graphics view

class MaskEditorCanvas : public QWidget {
    Q_OBJECT
public:
    MaskEditorCanvas();
    ~MaskEditorCanvas();

    void updateCanvas(Data2DItem* data_item);

    MaskGraphicsScene* scene() { return m_scene; }

signals:
    void changeActivityRequest(Canvas2DMode::Flag);
    void projectionMoved(Canvas2DMode::Flag);
    void marginsChanged(double left, double right);

public slots:
    void onResetViewRequest();
    void changeMaskDisplay(bool pixelized);
    void setCanvasMode(Canvas2DMode::Flag mode);

private slots:
    void onEnteringColorMap();
    void onLeavingColorMap();
    void onPositionChanged(double x, double y);
    void onDataDestroyed();

private:
    void setZoomToROI();

    MaskGraphicsScene* m_scene;
    MaskGraphicsView* m_view;
    PlotStatusLabel* m_status_label;
    Data2DItem* m_data_item;
    std::unique_ptr<Datafield> m_backup_data;
    bool m_backup_interpolated = false;
    std::unique_ptr<LineItem> m_live_projection; //!< temporary, matching mouse move
    Canvas2DMode::Flag m_current_activity;
};

#endif // BORNAGAIN_GUI_VIEW_CANVAS_MASKEDITORCANVAS_H
