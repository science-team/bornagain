//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Canvas/ProgressCanvas.cpp
//! @brief     Implement class ProgressCanvas
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Canvas/ProgressCanvas.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Job/JobsSet.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include "GUI/Model/Tune/FitSuiteItem.h"
#include "GUI/View/Plotter/HistogramPlot.h"
#include <QVBoxLayout>

ProgressCanvas::ProgressCanvas()
    : m_hist_plot(new HistogramPlot)
{
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    auto* layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(m_hist_plot);

    connect(gDoc->jobs(), &JobsSet::jobPlotContextChanged, this, &ProgressCanvas::setJobItem);
}

void ProgressCanvas::setJobItem()
{
    if (const JobItem* ji = gDoc->jobs()->currentItem()) {
        connect(ji->fitSuiteItem(), &FitSuiteItem::iterationCountChanged, this,
                &ProgressCanvas::onIterationCountChanged, Qt::UniqueConnection);
        show();
    } else
        hide();
}

void ProgressCanvas::onIterationCountChanged(int iter)
{
    const JobItem* ji = gDoc->jobs()->currentItem();
    ASSERT(ji);
    double chi = ji->fitSuiteItem()->chi2();
    if (iter == 1)
        m_hist_plot->clearData();
    m_hist_plot->addData(static_cast<double>(iter), chi);
}
