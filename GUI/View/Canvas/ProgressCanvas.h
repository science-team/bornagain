//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Canvas/ProgressCanvas.h
//! @brief     Defines class ProgressCanvas.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_CANVAS_PROGRESSCANVAS_H
#define BORNAGAIN_GUI_VIEW_CANVAS_PROGRESSCANVAS_H

#include <QWidget>

class HistogramPlot;

//! Intended for showing chi2 .vs interation count dependency.
//! The main goal is to fill vacant place in Fit2DFrame.

class ProgressCanvas : public QWidget {
    Q_OBJECT
public:
    ProgressCanvas();

private:
    void setJobItem();
    void onIterationCountChanged(int iter);

    HistogramPlot* m_hist_plot;
};

#endif // BORNAGAIN_GUI_VIEW_CANVAS_PROGRESSCANVAS_H
