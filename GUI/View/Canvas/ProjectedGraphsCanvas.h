//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Canvas/ProjectedGraphsCanvas.h
//! @brief     Defines class ProjectedGraphsCanvas.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_CANVAS_PROJECTEDGRAPHSCANVAS_H
#define BORNAGAIN_GUI_VIEW_CANVAS_PROJECTEDGRAPHSCANVAS_H

#include "GUI/View/Setup/Canvas2DMode.h"
#include <QTabWidget>

class Data2DItem;
class ProjectionsPlot;

//! Holds tabs of vertical and horizontal projections, located at the bottom of ProjectionsEditor.

class ProjectedGraphsCanvas : public QWidget {
    Q_OBJECT
public:
    ProjectedGraphsCanvas();

    void setData2DItem(Data2DItem* data_item);
    void disconnectItem();
    ProjectionsPlot* currentProjectionsPlot() const;

signals:
    void currentTabChanged(Canvas2DMode::Flag mode);

public slots:
    void onActivityChanged(Canvas2DMode::Flag mode);
    void onMarginsChanged(double left, double right);

private slots:
    void onTabChanged(int tab_index);

private:
    void setConnected(bool isConnected);

    ProjectionsPlot* m_x_projection;
    ProjectionsPlot* m_y_projection;
    QTabWidget* m_tab_widget;
};

#endif // BORNAGAIN_GUI_VIEW_CANVAS_PROJECTEDGRAPHSCANVAS_H
