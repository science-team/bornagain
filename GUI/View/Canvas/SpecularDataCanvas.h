//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Canvas/SpecularDataCanvas.h
//! @brief     Defines class SpecularDataCanvas.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_CANVAS_SPECULARDATACANVAS_H
#define BORNAGAIN_GUI_VIEW_CANVAS_SPECULARDATACANVAS_H

#include <QWidget>

class Data1DItem;
class DataSource;
class SpecularPlotCanvas;

class SpecularDataCanvas : public QWidget {
public:
    SpecularDataCanvas(const DataSource* data_source);

    void setDataItem();

    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;

public slots:
    void onSavePlotAction();
    void onMousePress(QMouseEvent* event);
    void onResetView();

private:
    const DataSource* m_data_source;
    SpecularPlotCanvas* m_plot_canvas;

    Data1DItem* dataItem();
};

#endif // BORNAGAIN_GUI_VIEW_CANVAS_SPECULARDATACANVAS_H
