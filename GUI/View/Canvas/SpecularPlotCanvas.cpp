//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Canvas/SpecularPlotCanvas.cpp
//! @brief     Declares class SpecularPlotCanvas.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Canvas/SpecularPlotCanvas.h"
#include "GUI/Model/Data/Data1DItem.h"
#include "GUI/View/Plotter/PlotStatusLabel.h"
#include "GUI/View/Plotter/SpecularPlot.h"

SpecularPlotCanvas::SpecularPlotCanvas()
    : m_plot(new SpecularPlot)
{
    auto* layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    layout->addWidget(m_plot);

    auto status_label = new PlotStatusLabel({m_plot});
    layout->addWidget(status_label);
}

void SpecularPlotCanvas::setData1DItems(const QVector<Data1DItem*>& data_items)
{
    m_plot->setData1DItems(data_items);
}
