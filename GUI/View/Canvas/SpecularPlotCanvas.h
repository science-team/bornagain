//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Canvas/SpecularPlotCanvas.h
//! @brief     Defines class SpecularPlotCanvas.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_CANVAS_SPECULARPLOTCANVAS_H
#define BORNAGAIN_GUI_VIEW_CANVAS_SPECULARPLOTCANVAS_H

#include <QWidget>

class Data1DItem;
class PlotStatusLabel;
class SpecularPlot;

//! Contains SpecularPlot for specular data presentation, and provides status string appearance.

class SpecularPlotCanvas : public QWidget {
public:
    SpecularPlotCanvas();

    void setData1DItems(const QVector<Data1DItem*>& data_items);

    SpecularPlot* specularPlot() { return m_plot; }

private:
    SpecularPlot* m_plot;
};

#endif // BORNAGAIN_GUI_VIEW_CANVAS_SPECULARPLOTCANVAS_H
