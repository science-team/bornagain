//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/AxisForm.h
//! @brief     Defines class AxisForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_AXISFORM_H
#define BORNAGAIN_GUI_VIEW_DEVICE_AXISFORM_H

#include "GUI/View/Widget/GroupBoxes.h"

class AxisProperty;

//! Use this to edit an AxisProperty.
//!
//! An AxisProperty handles values for a EquiDivision. The values will be already written into the
//! data element. For each change of values, a signal will be emitted. Use this e.g. to update
//! dependent values or to set the document to modified.
class AxisForm : public StaticGroupBox {
    Q_OBJECT
public:
    AxisForm(QWidget* parent, const QString& group_title, AxisProperty* axis_property,
             QString nbins_tooltip = "");
};


#endif // BORNAGAIN_GUI_VIEW_DEVICE_AXISFORM_H
