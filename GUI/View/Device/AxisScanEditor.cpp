//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/AlphaScanEditor.cpp
//! @brief     Defines class AlphaScanEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Device/AxisScanEditor.h"
#include "GUI/Model/Beam/BeamDistributionItem.h"
#include "GUI/Model/Beam/DistributionItems.h"
#include "GUI/Model/Beam/SourceItems.h"
#include "GUI/View/Device/DistributionPlot.h"
#include "GUI/View/Device/DistributionSelector.h"
#include "GUI/View/Device/ScanRangeForm.h"

AxisScanEditor::AxisScanEditor(QWidget* parent, ScanItem* item, QString title, QString units,
                               bool allow_distr)
    : StaticGroupBox(title + " (" + units + ")", parent)
    , m_item(item)
    , m_plot(new DistributionPlot(this))
{
    auto* layout = new QFormLayout(body());

    //... axis type combo
    auto* typeComboBox = new QComboBox;
    typeComboBox->addItem("Uniform axis");
    typeComboBox->addItem("Non-uniform axis"); // for use with experimental data ?
    const int idx = m_item->pointwiseAxisSelected() ? 1 : 0;
    layout->addRow("Axis type:", typeComboBox);

    //... axis parameters
    m_form = new ScanRangeForm(layout, units);

    //... beam distribution
    m_selector = new DistributionSelector(false, DistributionSelector::Category::Symmetric, this,
                                          m_item->scanDistributionItem(), allow_distr);
    connect(m_selector, &DistributionSelector::distributionChanged, this,
            &AxisScanEditor::dataChanged);
    connect(m_selector, &DistributionSelector::distributionChanged, this,
            &AxisScanEditor::updatePlot);
    layout->addRow(m_selector);

    //... update axis type combo (needs m_form)
    typeComboBox->setCurrentIndex(idx);
    onAxisTypeSelected(idx); // enable currently selected axis

    typeComboBox->setEnabled(m_item->pointwiseAxisDefined());
    connect(typeComboBox, &QComboBox::currentIndexChanged, this,
            &AxisScanEditor::onAxisTypeSelected);

    //... distribution plot

    m_plot->setFixedHeight(170);
    m_plot->setShowMouseCoords(false);
    layout->addRow(m_plot);
    setFixedWidth(300);

    updatePlot();
}

void AxisScanEditor::updateIndicators()
{
    m_form->updateData();
}

void AxisScanEditor::onAxisTypeSelected(int index)
{
    if (m_item) {
        if (index == 0 && m_item->pointwiseAxisSelected()) {
            m_item->selectUniformAxis();
            emit dataChanged();
        } else if (index == 1 && !m_item->pointwiseAxisSelected()) {
            m_item->selectListScan();
            emit dataChanged();
        }

        m_form->setAxisItem(m_item->currentAxisItem());
        m_form->setEnabled(index == 0);
    }
}

void AxisScanEditor::updatePlot()
{
    auto* d = m_selector->item()->distributionItem();
    m_plot->setVisible(!dynamic_cast<const DistributionNoneItem*>(d));
    m_plot->setDistItem(d);
    m_plot->plotItem();
}
