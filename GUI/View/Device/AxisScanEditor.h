//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/AxisScanEditor.h
//! @brief     Defines class AxisScanEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_AXISSCANEDITOR_H
#define BORNAGAIN_GUI_VIEW_DEVICE_AXISSCANEDITOR_H

#include "GUI/View/Widget/GroupBoxes.h"
#include <QFormLayout>

class DistributionPlot;
class DistributionSelector;
class ScanItem;
class ScanRangeForm;

//! Editor for scanning grazing angles or wavelengths

class AxisScanEditor : public StaticGroupBox {
    Q_OBJECT
public:
    AxisScanEditor(QWidget* parent, ScanItem* item, const QString& title, const QString& units,
                   bool allow_distr);

    void updateIndicators();

signals:
    void dataChanged();

private slots:
    void onAxisTypeSelected(int index);
    void updatePlot();

private:
    void updateSelectorWidget(bool allow_distr, const QString& distr_units);

    DistributionSelector* m_selector;
    ScanRangeForm* m_form;
    ScanItem* m_scan_item;
    DistributionPlot* m_plot;
    QFormLayout* m_selector_layout;
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_AXISSCANEDITOR_H
