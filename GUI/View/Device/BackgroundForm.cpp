//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/BackgroundForm.cpp
//! @brief     Implements class BackgroundForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Device/BackgroundForm.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sim/BackgroundItems.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/View/Numeric/ComboUtil.h"
#include "GUI/View/Numeric/DSpinBox.h"
#include "GUI/View/Numeric/NumWidgetUtil.h"

BackgroundForm::BackgroundForm(QWidget* parent, InstrumentItem* instrument)
    : CollapsibleGroupBox("Environment", parent, instrument->expandEnvironment)
    , m_instrument(instrument)
    , m_form_layout(new QFormLayout)
{
    m_form_layout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
    body()->setLayout(m_form_layout);

    auto* backgroundTypeCombo = GUI::Util::createComboBoxFromPolyPtr(
        instrument->backgroundSelection(), [this](int) { createBackgroundWidgets(); }, true);
    m_form_layout->addRow("Background type:", backgroundTypeCombo);

    createBackgroundWidgets();
}

void BackgroundForm::createBackgroundWidgets()
{
    while (m_form_layout->rowCount() > 1)
        m_form_layout->removeRow(1);

    auto* backgroundItem = m_instrument->backgroundItem();
    if (auto* p = dynamic_cast<ConstantBackgroundItem*>(backgroundItem)) {
        auto* sb = GUI::Util::addDoubleSpinBoxRow(m_form_layout, p->backgroundValue());
        sb->setMinimumWidth(150);
    }
}
