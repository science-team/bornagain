//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/BackgroundForm.h
//! @brief     Defines class BackgroundForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_BACKGROUNDFORM_H
#define BORNAGAIN_GUI_VIEW_DEVICE_BACKGROUNDFORM_H

#include "GUI/View/Widget/GroupBoxes.h"
#include <QFormLayout>

class InstrumentItem;

//! Environment editor (i.e. background) for instrument editors.
//! Operates on InstrumentItem.

class BackgroundForm : public CollapsibleGroupBox {
    Q_OBJECT
public:
    BackgroundForm(QWidget* parent, InstrumentItem* instrument);

private:
    void createBackgroundWidgets();

private:
    InstrumentItem* m_instrument;
    QFormLayout* m_form_layout;
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_BACKGROUNDFORM_H
