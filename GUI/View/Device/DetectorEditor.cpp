//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/DetectorEditor.cpp
//! @brief     Implements class DetectorEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Device/DetectorEditor.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Detector/DetectorItem.h"
#include "GUI/Model/Detector/ResolutionFunctionItems.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/View/Device/AxisForm.h"
#include "GUI/View/Numeric/ComboUtil.h"
#include "GUI/View/Numeric/DSpinBox.h"
#include "GUI/View/Numeric/NumWidgetUtil.h"
#include <QFormLayout>

DetectorEditor::DetectorEditor(QWidget* parent, Scatter2DInstrumentItem* instrItem)
    : CollapsibleGroupBox("Detector parameters", parent, instrItem->expandDetector)
{
    auto* layout = new QVBoxLayout;
    body()->setLayout(layout);

    DetectorItem* detectorItem = instrItem->detectorItem();
    ASSERT(detectorItem);

    auto* xyrow = new QHBoxLayout;
    layout->addLayout(xyrow);

    auto* phiForm = new AxisForm(this, u8"\u03c6 axis", &detectorItem->phiAxis(), "phi axis");
    xyrow->addWidget(phiForm);

    auto* alphaForm = new AxisForm(this, u8"\u03b1 axis", &detectorItem->alphaAxis(), "alpha axis");
    xyrow->addWidget(alphaForm);

    //... resolution controls

    auto* resolutionForm = new StaticGroupBox("Resolution function", this);
    xyrow->addWidget(resolutionForm);

    auto* resolutionLayout = new QFormLayout;
    resolutionForm->body()->setLayout(resolutionLayout);
    resolutionLayout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);

    auto updateResolutionForm = [layout = resolutionLayout, item = detectorItem]() -> void {
        while (layout->rowCount() > 1)
            layout->removeRow(1);

        auto* resFunction = item->resolutionFunctionSelection().certainItem();
        if (auto* p = dynamic_cast<ResolutionFunction2DGaussianItem*>(resFunction)) {
            auto* sigmaXSpinBox = GUI::Util::addDoubleSpinBoxRow(layout, p->sigmaX());
            auto* sigmaYSpinBox = GUI::Util::addDoubleSpinBoxRow(layout, p->sigmaY());

            connect(sigmaXSpinBox, &DSpinBox::valueChanged,
                    [p](double newValue) { p->setSigmaX(newValue); });

            connect(sigmaYSpinBox, &DSpinBox::valueChanged,
                    [p](double newValue) { p->setSigmaY(newValue); });
        }
    };

    auto* typeCombo = GUI::Util::createComboBoxFromPolyPtr(
        detectorItem->resolutionFunctionSelection(),
        [updateResolutionForm](int) { updateResolutionForm(); }, true);
    resolutionLayout->addRow("Type:", typeCombo);

    updateResolutionForm();
}
