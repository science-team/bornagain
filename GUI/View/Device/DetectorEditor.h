//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/DetectorEditor.h
//! @brief     Defines class DetectorEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_DETECTOREDITOR_H
#define BORNAGAIN_GUI_VIEW_DEVICE_DETECTOREDITOR_H

#include "GUI/View/Widget/GroupBoxes.h"

class Scatter2DInstrumentItem;

//! Detector section in GISAS instrument form.

class DetectorEditor : public CollapsibleGroupBox {
    Q_OBJECT
public:
    DetectorEditor(QWidget* parent, Scatter2DInstrumentItem* item);
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_DETECTOREDITOR_H
