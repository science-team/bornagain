//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/DistributionEditor.cpp
//! @brief     Implements class DistributionEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Device/DistributionEditor.h"
#include "GUI/Model/Beam/BeamDistributionItem.h"
#include "GUI/Model/Beam/DistributionItems.h"
#include "GUI/View/Device/DistributionPlot.h"

DistributionEditor::DistributionEditor(const QString& title, bool mean_config,
                                       DistributionSelector::Category distributions,
                                       QWidget* parent, BeamDistributionItem* item,
                                       bool allow_distr)
    : StaticGroupBox(title, parent)
    , m_selector(new DistributionSelector(mean_config, distributions, this, item, allow_distr))
    , m_plot(new DistributionPlot(this))
{
    auto* layout = new QVBoxLayout(body());

    layout->addWidget(m_selector);

    m_plot->setFixedHeight(170);
    m_plot->setShowMouseCoords(false);
    layout->addWidget(m_plot);
    layout->addStretch(1);
    setFixedWidth(300);

    connect(m_selector, &DistributionSelector::distributionChanged, this,
            &DistributionEditor::distributionChanged);
    connect(m_selector, &DistributionSelector::distributionChanged, this,
            &DistributionEditor::updatePlot);

    updatePlot();
}

void DistributionEditor::updateData()
{
    m_selector->refresh();
}

void DistributionEditor::updatePlot()
{
    auto* d = m_selector->item()->distributionItem();
    m_plot->setVisible(!dynamic_cast<const DistributionDeltaItem*>(d));
    m_plot->setDistItem(d);
    m_plot->plotItem();
}
