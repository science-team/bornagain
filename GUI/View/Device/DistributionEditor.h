//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/DistributionEditor.h
//! @brief     Defines class DistributionEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_DISTRIBUTIONEDITOR_H
#define BORNAGAIN_GUI_VIEW_DEVICE_DISTRIBUTIONEDITOR_H

#include "GUI/View/Device/DistributionSelector.h"
#include "GUI/View/Widget/GroupBoxes.h"
#include <optional>

class BeamDistributionItem;
class DistributionPlot;

//! DistributionSelector contained in a GroupBox with a title and a button
//! to open the distribution dialog

class DistributionEditor : public StaticGroupBox {
    Q_OBJECT
public:
    DistributionEditor(const QString& title, bool mean_config,
                       DistributionSelector::Category distributions, QWidget* parent,
                       BeamDistributionItem* item, bool allow_distr);

    //! Update UI from data
    void updateData();

signals:
    void distributionChanged();

private:
    void updatePlot();

    DistributionSelector* m_selector;
    DistributionPlot* m_plot;
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_DISTRIBUTIONEDITOR_H
