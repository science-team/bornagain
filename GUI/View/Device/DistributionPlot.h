//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/DistributionPlot.h
//! @brief     Defines class DistributionPlot.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_DISTRIBUTIONPLOT_H
#define BORNAGAIN_GUI_VIEW_DEVICE_DISTRIBUTIONPLOT_H

#include <QAction>
#include <QLabel>
#include <QWidget>
#include <qcustomplot.h>
#include <utility>

class CautionSign;
class DistributionItem;
class QCustomPlot;
class RealLimits;

//! The DistributionPlot class plots 1d functions corresponding to domain's Distribution1D
class DistributionPlot : public QWidget {
    Q_OBJECT
public:
    DistributionPlot(QWidget* parent = nullptr);

    void setDistItem(DistributionItem* distItem);
    void plotItem();
    void setXAxisName(const QString& xAxisName);
    void setShowMouseCoords(bool b);

public slots:
    void onMouseMove(QMouseEvent* event);
    void onMousePress(QMouseEvent* event);

private slots:
    void resetView();

private:
    void init_plot();
    void plot_distributions();
    void setPlotRange(const std::pair<double, double>& xRange);
    void plotVerticalLine(double xMin, double yMin, double xMax, double yMax,
                          const QColor& color = Qt::blue);
    QPoint positionForCautionSign();

    QCustomPlot* m_qcp;
    DistributionItem* m_dist_item;
    QLabel* m_label;
    QAction* m_reset_action;
    QCPRange m_x_range;
    CautionSign* m_caution_sign;
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_DISTRIBUTIONPLOT_H
