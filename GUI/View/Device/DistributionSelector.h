//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/DistributionSelector.h
//! @brief     Defines struct MeanConfig and class DistributionSelector.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_DISTRIBUTIONSELECTOR_H
#define BORNAGAIN_GUI_VIEW_DEVICE_DISTRIBUTIONSELECTOR_H

#include <QComboBox>
#include <QFormLayout>
#include <QWidget>
#include <optional>

class BeamDistributionItem;
class DSpinBox;
class DistributionItem;
class DoubleProperty;

/// Widget for selecting a distribution (combo box) and input of the corresponding values
/// with respect to the given distribution (e.g. mean and deviation for gauss distribution)

class DistributionSelector : public QWidget {
    Q_OBJECT
public:
    //! Which distributions should be available in the selector
    enum class Category { All, Symmetric };

    /// \param mean_config controls how the mean value is shown and can be input.
    ///                    If this parameter is \c nullopt then the mean value can not be input
    ///                    and only symmetric distributions are provided
    /// \pre ! mean_config && distributions == Distributions::Symmetric
    ///      (i.e. the combination of all distributions without mean input is currently not
    ///       supported)
    DistributionSelector(bool mean_config, Category distributions, QWidget* parent,
                         BeamDistributionItem* item, bool allow_distr);

    BeamDistributionItem* item() const { return m_item; }
    void refresh();

signals:
    void distributionChanged();

private:
    void createDistributionWidgets();
    void createNumSamplesSpinBox(DistributionItem* dist);
    void createSpinBox(DoubleProperty& d);
    void createMeanSpinBox(DoubleProperty& d);

    BeamDistributionItem* m_item;
    bool m_mean_config;
    Category m_distributions;
    QFormLayout* m_form_layout;
    QComboBox* m_distribution_combo;
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_DISTRIBUTIONSELECTOR_H
