//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/FootprintForm.cpp
//! @brief     Implements class FootprintForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Device/FootprintForm.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Beam/FootprintItems.h"
#include "GUI/Model/Beam/SourceItems.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/View/Numeric/ComboUtil.h"
#include "GUI/View/Numeric/DSpinBox.h"
#include "GUI/View/Numeric/NumWidgetUtil.h"

FootprintForm::FootprintForm(QWidget* parent, SourceItem* item)
    : StaticGroupBox("Footprint correction", parent)
    , m_form_layout(new QFormLayout(body()))
    , m_item(item)
{
    m_form_layout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
    ASSERT(item->footprintSelection().certainItem());
    auto* typeCombo = GUI::Util::createComboBoxFromPolyPtr(
        item->footprintSelection(), [this](int) { updateFootprintWidgets(); }, true);
    m_form_layout->addRow("Type:", typeCombo);

    updateFootprintWidgets();
}

void FootprintForm::updateFootprintWidgets()
{
    while (m_form_layout->rowCount() > 1)
        m_form_layout->removeRow(1);

    int sb_width = 134;
    auto* t = m_item->footprintSelection().certainItem();
    if (auto* tt = dynamic_cast<FootprintSquareItem*>(t)) {
        auto* sb = GUI::Util::addDoubleSpinBoxRow(m_form_layout, tt->squareFootprintValue());
        sb->setMinimumWidth(sb_width);
    } else if (auto* tt = dynamic_cast<FootprintGaussianItem*>(t)) {
        auto* sb = GUI::Util::addDoubleSpinBoxRow(m_form_layout, tt->gaussianFootprintValue());
        sb->setMinimumWidth(sb_width);
    }
}
