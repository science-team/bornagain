//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/FootprintForm.h
//! @brief     Defines class FootprintForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_FOOTPRINTFORM_H
#define BORNAGAIN_GUI_VIEW_DEVICE_FOOTPRINTFORM_H

#include "GUI/View/Widget/GroupBoxes.h"
#include <QFormLayout>

class SourceItem;

//! FootprintCorrection editor (i.e. background) for instrument editors.
//! Operates on InstrumentItem.

class FootprintForm : public StaticGroupBox {
    Q_OBJECT
public:
    FootprintForm(QWidget* parent, SourceItem* item);

private:
    void updateFootprintWidgets();

    QFormLayout* m_form_layout;
    SourceItem* m_item;
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_FOOTPRINTFORM_H
