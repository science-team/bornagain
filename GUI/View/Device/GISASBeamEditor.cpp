//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/GISASBeamEditor.cpp
//! @brief     Implements class GISASBeamEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Device/GISASBeamEditor.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Beam/BeamDistributionItem.h"
#include "GUI/Model/Beam/SourceItems.h"
#include "GUI/View/Device/DistributionEditor.h"
#include "GUI/View/Device/FootprintForm.h"
#include "GUI/View/Numeric/DSpinBox.h"
#include "GUI/View/Numeric/NumWidgetUtil.h"
#include <QFormLayout>
#include <QLineEdit>

GISASBeamEditor::GISASBeamEditor(QWidget* parent, BeamItem* item)
    : CollapsibleGroupBox("Beam parameters", parent, item->expandBeamParameters)
{
    auto* layout = new QVBoxLayout;
    layout->setAlignment(Qt::AlignLeft);
    body()->setLayout(layout);

    // Beam intensity row
    auto* form = new QFormLayout;
    layout->addLayout(form);
    form->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);

    auto* intensity_spinbox = GUI::Util::addDoubleSpinBoxRow(form, item->intensity());
    intensity_spinbox->setToolTip("Beam intensity in neutrons/photons per second.");

    // Main row: fixed values or distributions for wavelength and angles of incidence
    auto* wavelengthEditor =
        new DistributionEditor("Wavelength", true, DistributionSelector::Category::All, this,
                               item->wavelengthItem(), "nm");

    auto* grazingEditor =
        new DistributionEditor("Grazing angle", true, DistributionSelector::Category::All, this,
                               item->grazingAngleItem(), "deg");

    auto* azimuthalEditor =
        new DistributionEditor("Azimuthal angle", true, DistributionSelector::Category::All, this,
                               item->azimuthalAngleItem(), "deg");

    auto* mainrow = new QHBoxLayout;
    mainrow->addWidget(wavelengthEditor);
    mainrow->addWidget(grazingEditor);
    mainrow->addWidget(azimuthalEditor);
    layout->addLayout(mainrow);

    auto* footprintEditor = new FootprintForm(this, item);
    layout->addWidget(footprintEditor);
}
