//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/GISASBeamEditor.h
//! @brief     Defines class GISASBeamEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_GISASBEAMEDITOR_H
#define BORNAGAIN_GUI_VIEW_DEVICE_GISASBEAMEDITOR_H

#include "GUI/View/Widget/GroupBoxes.h"

class BeamItem;

//! GISAS beam editor. Operates on Scatter2DInstrumentItem.

class GISASBeamEditor : public CollapsibleGroupBox {
    Q_OBJECT
public:
    GISASBeamEditor(QWidget* parent, BeamItem* item);
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_GISASBEAMEDITOR_H
