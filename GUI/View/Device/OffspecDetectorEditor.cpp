//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/OffspecDetectorEditor.cpp
//! @brief     Implements class OffspecDetectorEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Device/OffspecDetectorEditor.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Detector/OffspecDetectorItem.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/View/Device/AxisForm.h"

OffspecDetectorEditor::OffspecDetectorEditor(QWidget* parent, OffspecInstrumentItem* instrument)
    : CollapsibleGroupBox("Detector parameters", parent, instrument->expandDetector)
    , m_instrument(instrument)
{
    OffspecDetectorItem* detector_item = m_instrument->detectorItem();

    auto* grid = new QGridLayout;
    grid->setColumnStretch(0, 1);
    grid->setColumnStretch(1, 1);
    grid->setColumnStretch(2, 1);

    auto* phiForm =
        new AxisForm(this, u8"\u03c6 axis", &detector_item->phiAxis(), "Number of phi-axis bins");
    grid->addWidget(phiForm, 1, 0);

    auto* alphaForm = new AxisForm(this, u8"\u03b1 axis", &detector_item->alphaAxis(),
                                   "Number of alpha-axis bins");
    grid->addWidget(alphaForm, 1, 1);
    grid->setRowStretch(2, 1);
    body()->setLayout(grid);
}
