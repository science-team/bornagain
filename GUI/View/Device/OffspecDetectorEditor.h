//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/OffspecDetectorEditor.h
//! @brief     Defines class OffspecDetectorEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_OFFSPECDETECTOREDITOR_H
#define BORNAGAIN_GUI_VIEW_DEVICE_OFFSPECDETECTOREDITOR_H

#include "GUI/View/Widget/GroupBoxes.h"
#include <QFormLayout>

class OffspecInstrumentItem;

class OffspecDetectorEditor : public CollapsibleGroupBox {
    Q_OBJECT
public:
    OffspecDetectorEditor(QWidget* parent, OffspecInstrumentItem* instrument);

private:
    OffspecInstrumentItem* m_instrument;
    QFormLayout* m_form_layout;
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_OFFSPECDETECTOREDITOR_H
