//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/PolarizationAnalysisEditor.cpp
//! @brief     Implements class PolarizationAnalysisEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Device/PolarizationAnalysisEditor.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/View/Numeric/DSpinBox.h"
#include "GUI/View/Numeric/NumWidgetUtil.h"
#include <QCheckBox>
#include <QLabel>

PolarizationAnalysisEditor::PolarizationAnalysisEditor(QWidget* parent, InstrumentItem* instrument)
    : CollapsibleGroupBox("Polarization analysis", parent, instrument->expandPolarizerAnalyzer)
    , m_instrument(instrument)
{
    auto* layout = new QVBoxLayout;
    body()->setLayout(layout);

    // polarizer
    {
        auto* polarizerCheckbox = new QCheckBox("Enable beam polarization");
        polarizerCheckbox->setChecked(m_instrument->withPolarizer());
        layout->addWidget(polarizerCheckbox);

        auto* polarizerWidget = new QWidget(this);
        auto* polarizerLayout = new QFormLayout(polarizerWidget);
        polarizerLayout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);

        addBlochRow(polarizerLayout, m_instrument->polarizerBlochVector());

        layout->addWidget(polarizerWidget);
        polarizerWidget->setVisible(m_instrument->withPolarizer());

        connect(polarizerCheckbox, &QCheckBox::toggled, [this, polarizerWidget](bool b) {
            polarizerWidget->setVisible(b);
            m_instrument->setWithPolarizer(b);
            gDoc->setModified();
        });
    }

    // analyzer
    {
        auto* analyzerCheckbox = new QCheckBox("Enable analyzer");
        analyzerCheckbox->setChecked(m_instrument->withAnalyzer());
        layout->addWidget(analyzerCheckbox);

        auto* analyzerWidget = new QWidget(this);
        auto* analyzerLayout = new QFormLayout(analyzerWidget);
        analyzerLayout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);

        addBlochRow(analyzerLayout, m_instrument->analyzerBlochVector());

        layout->addWidget(analyzerWidget);
        analyzerWidget->setVisible(m_instrument->withAnalyzer());

        connect(analyzerCheckbox, &QCheckBox::toggled, [this, analyzerWidget](bool b) {
            analyzerWidget->setVisible(b);
            m_instrument->setWithAnalyzer(b);
            gDoc->setModified();
        });
    }
}

void PolarizationAnalysisEditor::addBlochRow(QFormLayout* parentLayout, VectorProperty& v)
{
    auto* layout = new QHBoxLayout;

    const auto add = [layout](DoubleProperty& d) {
        layout->addWidget(new QLabel(d.label() + ":"));
        layout->addWidget(new DSpinBox(&d));
    };

    add(v.x());
    add(v.y());
    add(v.z());

    layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));

    parentLayout->addRow(v.label() + ":", layout);
}
