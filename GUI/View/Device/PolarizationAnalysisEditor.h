//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/PolarizationAnalysisEditor.h
//! @brief     Defines class PolarizationAnalysisEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_POLARIZATIONANALYSISEDITOR_H
#define BORNAGAIN_GUI_VIEW_DEVICE_POLARIZATIONANALYSISEDITOR_H

#include "GUI/View/Widget/GroupBoxes.h"
#include <QFormLayout>

class DSpinBox;
class DoubleProperty;
class InstrumentItem;
class VectorProperty;

//! Polarization analysis editor (beam polarization, analyzer properties) for instrument editors.
//! Operates on InstrumentItem.

class PolarizationAnalysisEditor : public CollapsibleGroupBox {
    Q_OBJECT
public:
    PolarizationAnalysisEditor(QWidget* parent, InstrumentItem* instrument);

private:
    void addBlochRow(QFormLayout* parentLayout, VectorProperty& v);

    InstrumentItem* m_instrument;
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_POLARIZATIONANALYSISEDITOR_H
