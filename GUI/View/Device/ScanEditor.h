//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/ScanEditor.h
//! @brief     Defines class ScanEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_SCANEDITOR_H
#define BORNAGAIN_GUI_VIEW_DEVICE_SCANEDITOR_H

#include "GUI/View/Widget/GroupBoxes.h"
#include <QFormLayout>

class InstrumentItem;
class ScanItem;

//! Specular beam editor. Operates on ScanItem.

class ScanEditor : public CollapsibleGroupBox {
    Q_OBJECT
public:
    ScanEditor(QWidget* parent, InstrumentItem* instr_item, ScanItem* item, bool allow_phi,
               bool allow_footprint, bool allow_distr);

private:
    void updateScanWidgets(bool allow_phi, bool allow_footprint, bool allow_distr);

    ScanItem* m_scan_item;
    InstrumentItem* m_instr_item;
    QHBoxLayout* m_beam_layout;
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_SCANEDITOR_H
