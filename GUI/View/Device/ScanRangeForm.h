//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Device/ScanRangeForm.h
//! @brief     Defines class ScanRangeForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DEVICE_SCANRANGEFORM_H
#define BORNAGAIN_GUI_VIEW_DEVICE_SCANRANGEFORM_H

#include <QFormLayout>
#include <QSpinBox>

class BasicAxisItem;
class DSpinBox;

//! The form for a spherical axis: contains the bare bone widgets for the user to enter
//! the number of bins and to select the range in degrees (minimum & maximum)
//! The input widgets will be inserted in the form given in the constructor

class ScanRangeForm : public QObject {
    Q_OBJECT
public:
    ScanRangeForm(QFormLayout* form, QString units);
    void setAxisItem(BasicAxisItem* axisItem);
    void setEnabled(bool enabled);

    //! Reload UI from data
    void updateData();

private slots:
    void onNbinsValueChanged(size_t value);
    void onMinValueChanged(double value);
    void onMaxValueChanged(double value);

private:
    QSpinBox* m_nbins_spin_box;
    DSpinBox* m_minimum_spin_box;
    DSpinBox* m_maximum_spin_box;
    BasicAxisItem* m_axis_item;
};

#endif // BORNAGAIN_GUI_VIEW_DEVICE_SCANRANGEFORM_H
