//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Dock/DockWidgetInfo.cpp
//! @brief     Implements class DockWidgetInfo.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Dock/DockWidgetInfo.h"

DockWidgetInfo::DockWidgetInfo()
    : m_dock(nullptr)
    , m_widget(nullptr)
    , m_area(Qt::NoDockWidgetArea)
{
}

DockWidgetInfo::DockWidgetInfo(QDockWidget* dock, QWidget* widget, Qt::DockWidgetArea area)
    : m_dock(dock)
    , m_widget(widget)
    , m_area(area)
{
}