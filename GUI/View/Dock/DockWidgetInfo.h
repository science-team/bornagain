//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Dock/DockWidgetInfo.h
//! @brief     Defines class DockWidgetInfo.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DOCK_DOCKWIDGETINFO_H
#define BORNAGAIN_GUI_VIEW_DOCK_DOCKWIDGETINFO_H

#include <QDockWidget>
#include <QWidget>
#include <qnamespace.h>

//! Holds information about the widget and its dock.

class DockWidgetInfo {
public:
    DockWidgetInfo();
    DockWidgetInfo(QDockWidget* dock, QWidget* widget, Qt::DockWidgetArea area);

    QDockWidget* dock() { return m_dock; }
    QWidget* widget() { return m_widget; }
    Qt::DockWidgetArea area() { return m_area; }

private:
    QDockWidget* m_dock;
    QWidget* m_widget;
    Qt::DockWidgetArea m_area;
};

#endif // BORNAGAIN_GUI_VIEW_DOCK_DOCKWIDGETINFO_H
