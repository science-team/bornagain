//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Dock/DocksController.h
//! @brief     Defines class DocksController.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_DOCK_DOCKSCONTROLLER_H
#define BORNAGAIN_GUI_VIEW_DOCK_DOCKSCONTROLLER_H

#include "GUI/View/Dock/DockWidgetInfo.h"
#include <QMainWindow>
#include <QMenu>
#include <QObject>
#include <QSize>
#include <map>

//! Handles creation and appearance of docked widgets in the context of QMainWindow.
//! Used in JobView. Previously also used for SampleView.

class DocksController : public QObject {
    Q_OBJECT
public:
    DocksController(QMainWindow* mainWindow);

    void addWidget(int id, QWidget* widget, Qt::DockWidgetArea area);

    void resetLayout();
    void toggleDock(int id);
    void setDockVisible(int id, bool visible = true);
    void setVisibleDocks(const std::vector<int>& visibleDocks);

    QDockWidget* findDock(int id);
    QDockWidget* findDock(QWidget* widget);
    QVector<QDockWidget*> dockWidgets() const;

    void addDockActionsToMenu(QMenu* menu);

    QHash<QString, QVariant> saveSettings() const;
    void restoreSettings(const QHash<QString, QVariant>& settings);

public slots:
    void setDockHeightForWidget(int height);
    void dockToMinMaxSizes();

private:
    struct DockSizeInfo {
        QDockWidget* m_dock = nullptr;
        QSize m_min_size;
        QSize m_max_size;
    };

    QDockWidget* addDockForWidget(QWidget* widget);
    void setTrackingEnabled(bool enabled);
    void handleWindowVisibilityChanged(bool visible);
    bool eventFilter(QObject*, QEvent* event) override;

    QMainWindow* m_main_window;
    std::map<int, DockWidgetInfo> m_docks;
    DockSizeInfo m_dock_info;
    bool m_handle_dock_visibility_changes = true;
};

#endif // BORNAGAIN_GUI_VIEW_DOCK_DOCKSCONTROLLER_H
