//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Fit/FitActivityPanel.h
//! @brief     Defines class FitActivityPanel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FIT_FITACTIVITYPANEL_H
#define BORNAGAIN_GUI_VIEW_FIT_FITACTIVITYPANEL_H

#include <QLabel>
#include <QStackedWidget>
#include <QWidget>

class FitLog;
class FitSessionManager;
class FitSessionWidget;
class JobItem;
class JobRealTimeWidget;

//! The FitActivityPanel class is a main widget to run the fitting.
//! Occupies bottom right corner of JobView, contains stack of FitSuiteWidgets for every
//! JobItem which is suitable for fitting.

class FitActivityPanel : public QWidget {
    Q_OBJECT
public:
    FitActivityPanel(QWidget* parent = nullptr);
    ~FitActivityPanel();

    void setRealTimeWidget(JobRealTimeWidget* realTimeWidget);

    void resizeEvent(QResizeEvent* event) override;

public slots:
    void setJobItem(JobItem* jobItem);

signals:
    void showLog(FitLog* log);

private:
    bool isValidJobItem(JobItem* item);

    void adjustWidthToRealTimeWidget(int w);
    void applySettings();
    void saveSettings();

    QStackedWidget* m_stack;
    QWidget* m_blank_widget;
    FitSessionWidget* m_fit_session_widget;
    QLabel* m_nofit_label;
    JobRealTimeWidget* m_real_time_widget;
    FitSessionManager* m_fit_session_manager;
};

#endif // BORNAGAIN_GUI_VIEW_FIT_FITACTIVITYPANEL_H
