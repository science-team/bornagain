//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Fit/FitParameterWidget.h
//! @brief     Defines class FitParameterWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FIT_FITPARAMETERWIDGET_H
#define BORNAGAIN_GUI_VIEW_FIT_FITPARAMETERWIDGET_H

#include <QItemSelection>
#include <QTreeView>
#include <QWidget>

class DeleteEventFilter;
class FitParameterContainerItem;
class FitParameterItem;
class FitparQModel;
class JobItem;
class OverlayLabelController;
class ParameterTuningWidget;

//! The FitParametersWidget class contains a tree view to set fit parameters (fix/release,
//! starting value, min/max bounds). It occupies bottom right corner of JobView.

class FitParameterWidget : public QWidget {
    Q_OBJECT
public:
    FitParameterWidget();

    void setJobItem(JobItem* job_item);

    void setParameterTuningWidget(ParameterTuningWidget* tuningWidget);

public slots:
    void onTuningWidgetContextMenu(const QPoint& point);
    void onFitParameterTreeContextMenu(const QPoint& point);
    void onFitParametersSelectionChanged(const QItemSelection& selection);
    void updateView();

private slots:
    void onCreateFitParAction();
    void onRemoveFromFitParAction();
    void onRemoveFitParAction();
    void onAddToFitParAction(int ipar);
    void onFitparQModelChange();
    void onJobDestroyed();
    void onTunerDestroyed();

private:
    void init_actions();
    void init_fit_model();
    FitParameterContainerItem* fitContainerItem() const;

    bool canCreateFitParameter();
    bool canRemoveFromFitParameters();

    void setActionsEnabled(bool value);

    QVector<FitParameterItem*> selectedFitParameterItems();
    QVector<FitParameterItem*> emptyFitParameterItems();
    QStringList selectedFitParameterLinks();

    void spanParameters();
    void updateInfoLabel();

    QTreeView* m_tree_view;
    ParameterTuningWidget* m_tuning_widget;
    QAction* m_create_fit_par_action;
    QAction* m_remove_from_fit_par_action;
    QAction* m_remove_fit_par_action;
    std::unique_ptr<FitparQModel> m_fit_parameter_model;
    DeleteEventFilter* m_keyboard_filter;
    OverlayLabelController* m_info_label;
    JobItem* m_job_item;
};

#endif // BORNAGAIN_GUI_VIEW_FIT_FITPARAMETERWIDGET_H
