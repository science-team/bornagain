//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Fit/FitSessionController.h
//! @brief     Defines class FitSessionController.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FIT_FITSESSIONCONTROLLER_H
#define BORNAGAIN_GUI_VIEW_FIT_FITSESSIONCONTROLLER_H

#include <QObject>
#include <memory>

class FitLog;
class FitObjectiveBuilder;
class FitProgressInfo;
class FitWorkerLauncher;
class GUIFitObserver;
class JobItem;

//! Controls all activity related to the single fitting task for JobItem.
//! Provides interaction between FitSessionWidget and fit observers.

class FitSessionController : public QObject {
    Q_OBJECT
public:
    FitSessionController(QObject* parent = nullptr);
    ~FitSessionController() override;

    void setJobItem(JobItem* jobItem);

    FitLog* fitLog();

signals:
    void fittingStarted();
    void fittingFinished();
    void fittingError(const QString& message);

public slots:
    void onStartFittingRequest();
    void onStopFittingRequest();
    void updateStartValuesFromTree();

private slots:
    void onObserverUpdate();
    void onFittingStarted();
    void onFittingFinished();
    void onFittingError(const QString& text);
    void updateIterationCount(const FitProgressInfo& info);
    void updateFitParameterValues(const FitProgressInfo& info);
    void updateLog(const FitProgressInfo& info);
    void onJobDestroyed();

private:
    JobItem* m_job_item;
    FitWorkerLauncher* m_run_fit_manager;
    std::shared_ptr<GUIFitObserver> m_observer;
    std::unique_ptr<FitLog> m_fitlog;
    std::shared_ptr<FitObjectiveBuilder> m_objective_builder;
    bool m_block_progress_update;
};

#endif // BORNAGAIN_GUI_VIEW_FIT_FITSESSIONCONTROLLER_H
