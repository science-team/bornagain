//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Fit/FitSessionManager.h
//! @brief     Defines class FitSessionManager.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FIT_FITSESSIONMANAGER_H
#define BORNAGAIN_GUI_VIEW_FIT_FITSESSIONMANAGER_H

#include <QMap>
#include <QObject>

class FitSessionController;
class JobItem;

//! Handles all activity related to the simultaneous running of fitting jobs.

class FitSessionManager : public QObject {
    Q_OBJECT
public:
    FitSessionManager(QObject* parent = nullptr);
    ~FitSessionManager();

    FitSessionController* sessionController(JobItem* jobItem);

private:
    FitSessionController* createController(JobItem* jobItem);
    void removeController(QObject* destroyed_obj);

    //!< Fit controller which is currently attached to jobMessagePanel
    FitSessionController* m_active_controller;
    QMap<JobItem*, FitSessionController*> m_item_to_controller;
};

#endif // BORNAGAIN_GUI_VIEW_FIT_FITSESSIONMANAGER_H
