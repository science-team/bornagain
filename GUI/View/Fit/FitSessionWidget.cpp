//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Fit/FitSessionWidget.cpp
//! @brief     Implements class FitSessionWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Fit/FitSessionWidget.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Job/JobItem.h"
#include "GUI/View/Base/mainwindow_constants.h"
#include "GUI/View/Fit/FitParameterWidget.h"
#include "GUI/View/Fit/FitSessionController.h"
#include "GUI/View/FitControl/FitEditor.h"
#include "GUI/View/FitControl/MinimizerEditor.h"
#include <QSettings>
#include <QVBoxLayout>

FitSessionWidget::FitSessionWidget(QWidget* parent)
    : QWidget(parent)
    , m_tab_widget(new QTabWidget(this))
    , m_control_widget(new FitEditor)
    , m_fit_parameters_widget(new FitParameterWidget)
    , m_minimizer_settings_widget(new MinimizerEditor)
    , m_session_controller(nullptr)
{
    auto* layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    m_tab_widget->addTab(m_fit_parameters_widget, "Fit Parameters");
    m_tab_widget->addTab(m_minimizer_settings_widget, "Minimizer");

    layout->addWidget(m_tab_widget);
    layout->addWidget(m_control_widget);

    connect(m_control_widget, &FitEditor::updFromTreePushed, m_fit_parameters_widget,
            &FitParameterWidget::updateView);

    applySettings();
}

FitSessionWidget::~FitSessionWidget()
{
    saveSettings();
}

void FitSessionWidget::setJobItem(JobItem* job_item)
{
    ASSERT(job_item);
    m_fit_parameters_widget->setJobItem(job_item);
    m_minimizer_settings_widget->setJobItem(job_item);
    m_control_widget->setJobItem(job_item);
}

void FitSessionWidget::setModelTuningWidget(ParameterTuningWidget* tuningWidget)
{
    ASSERT(m_fit_parameters_widget);
    ASSERT(tuningWidget);
    m_fit_parameters_widget->setParameterTuningWidget(tuningWidget);
}

void FitSessionWidget::setSessionController(FitSessionController* sessionController)
{
    m_session_controller = sessionController;

    if (!m_session_controller)
        return;

    connect(m_session_controller, &FitSessionController::fittingError, this,
            &FitSessionWidget::onFittingError, Qt::UniqueConnection);
    connect(m_session_controller, &QObject::destroyed, this,
            &FitSessionWidget::onControllerDestroyed, Qt::UniqueConnection);

    connect(m_control_widget, &FitEditor::startFittingPushed, m_session_controller,
            &FitSessionController::onStartFittingRequest, Qt::UniqueConnection);
    connect(m_control_widget, &FitEditor::stopFittingPushed, m_session_controller,
            &FitSessionController::onStopFittingRequest, Qt::UniqueConnection);
    connect(m_control_widget, &FitEditor::updFromTreePushed, m_session_controller,
            &FitSessionController::updateStartValuesFromTree, Qt::UniqueConnection);
}

void FitSessionWidget::onFittingError(const QString& text)
{
    m_control_widget->onFittingError(text);
}

void FitSessionWidget::applySettings()
{
    QSettings settings;
    if (settings.childGroups().contains(GUI::Style::S_FIT_SESSION_WIDGET)) {
        settings.beginGroup(GUI::Style::S_FIT_SESSION_WIDGET);
        m_tab_widget->setCurrentIndex(
            settings.value(GUI::Style::S_FIT_SESSION_WIDGET_CURRENT_TAB).toInt());
        settings.endGroup();
    }
}

void FitSessionWidget::saveSettings()
{
    QSettings settings;
    settings.beginGroup(GUI::Style::S_FIT_SESSION_WIDGET);
    settings.setValue(GUI::Style::S_FIT_SESSION_WIDGET_CURRENT_TAB, m_tab_widget->currentIndex());
    settings.endGroup();
    settings.sync();
}

void FitSessionWidget::onControllerDestroyed()
{
    m_session_controller = nullptr;
}
