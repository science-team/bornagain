//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Fit/FitSessionWidget.h
//! @brief     Defines class FitSessionWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FIT_FITSESSIONWIDGET_H
#define BORNAGAIN_GUI_VIEW_FIT_FITSESSIONWIDGET_H

#include <QTabWidget>
#include <QWidget>

class FitEditor;
class FitParameterWidget;
class FitSessionController;
class JobItem;
class MinimizerEditor;
class ParameterTuningWidget;

//! Contains all fit settings for given JobItem (fit parameters,
//! minimizer settings). Controlled by FitActivityPanel.

class FitSessionWidget : public QWidget {
    Q_OBJECT
public:
    FitSessionWidget(QWidget* parent = nullptr);
    ~FitSessionWidget();

    void setJobItem(JobItem* job_item);
    void setModelTuningWidget(ParameterTuningWidget* tuningWidget);
    void setSessionController(FitSessionController* sessionController);

private slots:
    void onControllerDestroyed();

private:
    void onFittingError(const QString& text);

    void applySettings();
    void saveSettings();

    QTabWidget* m_tab_widget;
    FitEditor* m_control_widget;
    FitParameterWidget* m_fit_parameters_widget;
    MinimizerEditor* m_minimizer_settings_widget;
    FitSessionController* m_session_controller;
};

#endif // BORNAGAIN_GUI_VIEW_FIT_FITSESSIONWIDGET_H
