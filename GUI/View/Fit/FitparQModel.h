//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Fit/FitparQModel.h
//! @brief     Defines class FitparQModel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FIT_FITPARQMODEL_H
#define BORNAGAIN_GUI_VIEW_FIT_FITPARQMODEL_H

#include <QAbstractItemModel>
#include <QStringList>

class FitParameterContainerItem;
class JobItem;

//! Model to show items from FitParameterContainer in 5 column tree view.
class FitparQModel : public QAbstractItemModel {
    Q_OBJECT
public:
    explicit FitparQModel(FitParameterContainerItem* fitParContainer, JobItem* jobItem);

    enum EColumn { COL_NAME, COL_TYPE, COL_VALUE, COL_MIN, COL_MAX, NUM_COLUMNS };

    Qt::ItemFlags flags(const QModelIndex& index) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent) const override;
    QModelIndex parent(const QModelIndex& child) const override;
    int rowCount(const QModelIndex& parent) const override;
    int columnCount(const QModelIndex& parent) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role) override;

    QStringList mimeTypes() const override;
    Qt::DropActions supportedDragActions() const override;
    Qt::DropActions supportedDropActions() const override;
    QMimeData* mimeData(const QModelIndexList& indexes) const override;
    bool canDropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column,
                         const QModelIndex& parent) const override;
    bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column,
                      const QModelIndex& parent) override;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    QModelIndex indexOfItem(QObject* item) const;
    QObject* itemForIndex(const QModelIndex& index) const;

    QVariant valueOfItem(QObject* item) const;
    void setValueOfItem(QObject* item, const QVariant& value);

private slots:
    void onFitItemChanged();

private:
    void addColumn(EColumn id, const QString& name, const QString& tooltip);

    FitParameterContainerItem* m_fit_parameter_container;
    JobItem* m_job_item;
    QMap<int, QString> m_column_names;
    QMap<int, QString> m_column_tool_tips;
};

inline Qt::DropActions FitparQModel::supportedDragActions() const
{
    return Qt::MoveAction | Qt::CopyAction;
}

inline Qt::DropActions FitparQModel::supportedDropActions() const
{
    return Qt::MoveAction | Qt::CopyAction;
}

#endif // BORNAGAIN_GUI_VIEW_FIT_FITPARQMODEL_H
