//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/FitControl/FitEditor.h
//! @brief     Defines class FitEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FITCONTROL_FITEDITOR_H
#define BORNAGAIN_GUI_VIEW_FITCONTROL_FITEDITOR_H

#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include <QWidget>

enum class JobStatus;

class CautionSign;
class FitSuiteItem;
class JobItem;

//! The FitEditor contains elements to start/stop fitting and to provide minimal
//! diagnostic. Part of FitSuiteWidget.

class FitEditor : public QWidget {
    Q_OBJECT
public:
    FitEditor();

    void setJobItem(JobItem* job_item);

signals:
    void startFittingPushed();
    void stopFittingPushed();
    void updFromTreePushed();

public slots:
    void onFittingError(const QString& what);

private slots:
    void onJobDestroyed();

private:
    void onSliderValueChanged(int value);
    int sliderUpdateInterval();
    int sliderValueToUpdateInterval(int value);
    int updateIntervalToSliderValue(int updInterval);
    void initializeSlider();

    void updateControlElements(JobStatus status);
    FitSuiteItem* fitSuiteItem();
    bool isValidJobItem();
    void updateIterationsCountLabel(int iter);

    QPushButton* m_start_button;
    QPushButton* m_stop_button;
    QPushButton* m_upd_button;
    QSlider* m_interval_slider;
    QLabel* m_update_interval_label;
    QLabel* m_iterations_count_label;
    CautionSign* m_caution_sign;
    JobItem* m_job_item;
};

#endif // BORNAGAIN_GUI_VIEW_FITCONTROL_FITEDITOR_H
