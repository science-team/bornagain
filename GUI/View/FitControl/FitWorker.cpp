//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/FitControl/FitWorker.cpp
//! @brief     Implements class FitWorker.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/FitControl/FitWorker.h"
#include "GUI/View/FitControl/FitObjectiveBuilder.h"

void FitWorker::startFit()
{
    emit fit_started();
    try {
        m_fit_objective->runFit();
    } catch (const std::exception& ex) {
        emit fit_error(QString::fromStdString(ex.what()));
    }
    emit fit_finished();
}

void FitWorker::interruptFitting()
{
    if (m_fit_objective)
        m_fit_objective->interruptFitting();
}
