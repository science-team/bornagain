//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/FitControl/MinimizerEditor.h
//! @brief     Defines class MinimizerEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FITCONTROL_MINIMIZEREDITOR_H
#define BORNAGAIN_GUI_VIEW_FITCONTROL_MINIMIZEREDITOR_H

#include <QFormLayout>
#include <QWidget>

class JobItem;
class MinimizerContainerItem;

//! The MinimizerEditor contains editor for all minimizer settings and related fit
//! options. Part of FitSuiteWidget.

class MinimizerEditor : public QWidget {
    Q_OBJECT
public:
    MinimizerEditor(QWidget* parent = nullptr);

public slots:
    void setJobItem(JobItem* job_item);
    void setMinContainerItem(MinimizerContainerItem* container_item);

private:
    void createGroupedAlgorithmsCombo();
    void createMimimizerEdits();
    void createMinuitEdits();
    void createGSLMultiMinEdits();
    void createTMVAGeneticEdits();
    void createGSLSimulatedAnnealingEdits();
    void createGSLLevMarEdits();

    void updateUIValues();

private:
    MinimizerContainerItem* m_container_item;
    QFormLayout* m_main_layout;
    QFormLayout* m_minimizer_layout;
    QVector<std::function<void()>> m_updaters;
};

#endif // BORNAGAIN_GUI_VIEW_FITCONTROL_MINIMIZEREDITOR_H
