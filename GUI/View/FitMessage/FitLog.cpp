//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/FitMessage/FitLog.cpp
//! @brief     Implements class FitLog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/FitMessage/FitLog.h"

FitLog::FitLog(QObject* parent)
    : QObject(parent)
{
}

void FitLog::append(const std::string& text, FitLogLevel level)
{
    Message m{text, level};
    m_messages.push_back(m);
    emit messageAppended(m);
}

void FitLog::clearLog()
{
    m_messages.clear();
    emit cleared();
}
