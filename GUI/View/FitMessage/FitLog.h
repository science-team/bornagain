//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/FitMessage/FitLog.h
//! @brief     Defines class FitLog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FITMESSAGE_FITLOG_H
#define BORNAGAIN_GUI_VIEW_FITMESSAGE_FITLOG_H

#include <QObject>
#include <string>
#include <vector>

//! enum class describing the log level of a message
enum class FitLogLevel { Default, Success, Highlight, Warning, Error };

//! the collected messages of a fitting session
class FitLog : public QObject {
    Q_OBJECT
public:
    struct Message {
        std::string text;
        FitLogLevel level;
    };

    FitLog(QObject* parent = nullptr);

    void append(const std::string& text, FitLogLevel level);
    void clearLog();
    const std::vector<Message>& messages() const { return m_messages; }

signals:
    void cleared();
    void messageAppended(const Message& message);

private:
    std::vector<Message> m_messages;
};

#endif // BORNAGAIN_GUI_VIEW_FITMESSAGE_FITLOG_H
