//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/FitMessage/JobMessagePanel.cpp
//! @brief     Implements class JobMessagePanel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/FitMessage/JobMessagePanel.h"
#include "GUI/View/Base/mainwindow_constants.h"
#include <QScrollBar>
#include <QSettings>

namespace {

//! text color for the given log level
QColor color(FitLogLevel level)
{
    switch (level) {
    case FitLogLevel::Default:
        return Qt::black;
    case FitLogLevel::Success:
        return Qt::darkBlue;
    case FitLogLevel::Highlight:
        return Qt::darkGreen;
    case FitLogLevel::Warning:
        return Qt::darkYellow;
    case FitLogLevel::Error:
        return Qt::darkRed;
    default:
        return Qt::red;
    }
}

} // namespace


JobMessagePanel::JobMessagePanel(QWidget* parent)
    : QTextEdit(parent)
    , m_log(nullptr)
{
    setWindowTitle("Message Panel");
    setReadOnly(true);
    setFont(QFont("Courier"));

    setFixedHeight(GUI::Style::JOB_MESSAGE_PANEL_HEIGHT);
    applySettings();
}

JobMessagePanel::~JobMessagePanel()
{
    saveSettings();
}

void JobMessagePanel::resizeEvent(QResizeEvent* event)
{
    QTextEdit::resizeEvent(event);
    setMinimumSize(0, 0);
    setMaximumSize(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX);
}

void JobMessagePanel::appendMessage(const FitLog::Message& message)
{
    QScrollBar* scrollbar = verticalScrollBar();
    bool autoscroll = scrollbar->value() == scrollbar->maximum();
    setTextColor(color(message.level));
    append(QString::fromStdString(message.text));
    if (autoscroll) {
        QTextCursor c = textCursor();
        c.movePosition(QTextCursor::End);
        setTextCursor(c);
    }
}

void JobMessagePanel::setLog(FitLog* log)
{
    m_log = log;
    clear();
    if (m_log) {
        for (const auto& record : m_log->messages())
            appendMessage(record);
        connect(m_log, &FitLog::cleared, this, &JobMessagePanel::clear, Qt::UniqueConnection);
        connect(m_log, &FitLog::messageAppended, this, &JobMessagePanel::appendMessage,
                Qt::UniqueConnection);
    }
}

void JobMessagePanel::applySettings()
{
    QSettings settings;
    if (settings.childGroups().contains(GUI::Style::S_JOB_MESSAGE_PANEL)) {
        settings.beginGroup(GUI::Style::S_JOB_MESSAGE_PANEL);
        setFixedHeight(settings.value(GUI::Style::S_JOB_MESSAGE_PANEL_HEIHGT).toInt());
        settings.endGroup();
    }
}

void JobMessagePanel::saveSettings()
{
    QSettings settings;
    settings.beginGroup(GUI::Style::S_JOB_MESSAGE_PANEL);
    settings.setValue(GUI::Style::S_JOB_MESSAGE_PANEL_HEIHGT, height());
    settings.endGroup();
    settings.sync();
}
