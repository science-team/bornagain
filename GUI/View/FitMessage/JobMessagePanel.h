//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/FitMessage/JobMessagePanel.h
//! @brief     Defines class JobMessagePanel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FITMESSAGE_JOBMESSAGEPANEL_H
#define BORNAGAIN_GUI_VIEW_FITMESSAGE_JOBMESSAGEPANEL_H

#include "GUI/View/FitMessage/FitLog.h"
#include <QTextEdit>

//! Shows log messages from FitActivityPanel at the bottom part of JobView.

class JobMessagePanel : public QTextEdit {
    Q_OBJECT
public:
    JobMessagePanel(QWidget* parent = nullptr);
    ~JobMessagePanel();

    void appendMessage(const FitLog::Message& message);
    void setLog(FitLog* log);

    void resizeEvent(QResizeEvent* event) override;

private:
    void applySettings();
    void saveSettings();

    FitLog* m_log;
};

#endif // BORNAGAIN_GUI_VIEW_FITMESSAGE_JOBMESSAGEPANEL_H
