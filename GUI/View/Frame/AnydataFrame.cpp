//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Frame/AnydataFrame.cpp
//! @brief     Implements class AnydataFrame.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Frame/AnydataFrame.h"
#include "GUI/Model/Project/DataSource.h"

AnydataFrame::AnydataFrame(std::unique_ptr<DataSource>&& data_source)
    : m_data_source(std::move(data_source))
{
}
