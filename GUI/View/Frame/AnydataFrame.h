//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Frame/AnydataFrame.h
//! @brief     Defines class AnydataFrame.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FRAME_ANYDATAFRAME_H
#define BORNAGAIN_GUI_VIEW_FRAME_ANYDATAFRAME_H

#include <QWidget>

class DataSource;

//! Widget to represent any kind of data from given source.

class AnydataFrame : public QWidget {
public:
    AnydataFrame(std::unique_ptr<DataSource>&& data_source);

    const DataSource* dataSource() const { return m_data_source.get(); }

protected:
    virtual bool hasProperContext() const = 0;

    std::unique_ptr<DataSource> m_data_source;
};

#endif // BORNAGAIN_GUI_VIEW_FRAME_ANYDATAFRAME_H
