//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Frame/Fit1DFrame.h
//! @brief     Defines class Fit1DFrame.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FRAME_FIT1DFRAME_H
#define BORNAGAIN_GUI_VIEW_FRAME_FIT1DFRAME_H

#include "GUI/View/Frame/AnydataFrame.h"
#include <QAction>

class Data1DItem;
class PlotStatusLabel;
class SpecularPlotCanvas;

//! Plots real data and simulated data.

class Fit1DFrame : public AnydataFrame {
    Q_OBJECT
public:
    Fit1DFrame();
    ~Fit1DFrame();

private:
    void updateFrame();
    void updateDiffData();
    void updateRealFrame();
    void connectItems();
    void onResetViewAction();

    bool hasProperContext() const override;

    Data1DItem* realItem() const;
    Data1DItem* simuItem() const;
    Data1DItem* diffItem() const;

    SpecularPlotCanvas* m_data_canvas;
    SpecularPlotCanvas* m_diff_canvas;
    PlotStatusLabel* m_status_label;
};

#endif // BORNAGAIN_GUI_VIEW_FRAME_FIT1DFRAME_H
