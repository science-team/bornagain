//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Frame/Fit2DFrame.h
//! @brief     Defines class Fit2DFrame.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FRAME_FIT2DFRAME_H
#define BORNAGAIN_GUI_VIEW_FRAME_FIT2DFRAME_H

#include "GUI/View/Frame/AnydataFrame.h"
#include <QAction>

class ColorMapCanvas;
class Data2DItem;
class PlotStatusLabel;

//! Plots realdata, simulated data and relative difference map during the course of the fit.

class Fit2DFrame : public AnydataFrame {
    Q_OBJECT
public:
    Fit2DFrame();
    ~Fit2DFrame();

private:
    void updateFrame();
    void updateDiffData();
    void updateCommonDataRange();
    void connectItems();
    void onResetViewAction();

    bool hasProperContext() const override;

    Data2DItem* realItem() const;
    Data2DItem* simuItem() const;
    Data2DItem* diffItem() const;

    ColorMapCanvas* m_real_canvas;
    ColorMapCanvas* m_simu_canvas;
    ColorMapCanvas* m_diff_canvas;
    PlotStatusLabel* m_status_label;
};

#endif // BORNAGAIN_GUI_VIEW_FRAME_FIT2DFRAME_H
