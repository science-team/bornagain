//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Frame/Plot1DFrame.cpp
//! @brief     Implements class Plot1DFrame.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Frame/Plot1DFrame.h"
#include "GUI/Model/File/DatafilesSet.h"
#include "GUI/Model/Job/JobsSet.h"
#include "GUI/Model/Project/DataSource.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include "GUI/View/Canvas/SpecularDataCanvas.h"
#include "GUI/View/Plotter/SavePlot.h"
#include "GUI/View/Setup/AxisPanel.h"
#include "GUI/View/Setup/Data1DToolbar.h"
#include "GUI/View/Setup/FrameActions.h"
#include <QBoxLayout>
#include <QMenu>

namespace {

void execContextMenu(const QPoint& point, FrameActions* actions)
{
    QMenu menu;
    menu.addAction(actions->reset_view);
    menu.addAction(actions->save_plot);
    menu.addAction(actions->toggle_properties_panel);
    menu.exec(point);
}

} // namespace


Plot1DFrame::Plot1DFrame(std::unique_ptr<DataSource>&& data_source)
    : AnydataFrame(std::move(data_source))
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    auto* layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    m_specular_canvas = new SpecularDataCanvas(dataSource());
    layout->addWidget(m_specular_canvas);

    auto* axis_panel = new AxisPanel(dataSource());
    layout->addWidget(axis_panel);
    axis_panel->hide();

    auto* toolbar = new Data1DToolbar;
    layout->addWidget(toolbar);

    updateFrame();


    //... Connect signals from toolbar

    connect(toolbar->actions()->toggle_properties_panel, &QAction::triggered, axis_panel,
            &QWidget::setVisible);

    connect(toolbar->actions()->reset_view, &QAction::triggered, m_specular_canvas,
            &SpecularDataCanvas::onResetView);

    connect(toolbar->actions()->save_plot, &QAction::triggered, m_specular_canvas,
            &SpecularDataCanvas::onSavePlotAction);


    //... Connect signals from canvas

    connect(m_specular_canvas, &SpecularDataCanvas::customContextMenuRequested,
            [toolbar](const QPoint& point) { execContextMenu(point, toolbar->actions()); });


    //... Connect model, depending on the context

    if (m_data_source->isFromData())
        connect(gDoc->datafiles(), &DatafilesSet::setChanged, this, &Plot1DFrame::updateFrame);
    else
        connect(gDoc->jobs(), &JobsSet::jobPlotContextChanged, this, &Plot1DFrame::updateFrame);
}

void Plot1DFrame::updateFrame()
{
    setVisible(hasProperContext());
    m_specular_canvas->setDataItem();
}

//  ************************************************************************************************
//  shortcuts
//  ************************************************************************************************

bool Plot1DFrame::hasProperContext() const
{
    return bool(dataSource()->realData1DItem() && !dataSource()->diffData1DItem());
}
