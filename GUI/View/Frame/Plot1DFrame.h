//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Frame/Plot1DFrame.h
//! @brief     Defines class Plot1DFrame.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FRAME_PLOT1DFRAME_H
#define BORNAGAIN_GUI_VIEW_FRAME_PLOT1DFRAME_H

#include "GUI/View/Frame/AnydataFrame.h"

class DataSource;
class SpecularDataCanvas;

class Plot1DFrame : public AnydataFrame {
public:
    Plot1DFrame(std::unique_ptr<DataSource>&& data_source);

private:
    void updateFrame();

    bool hasProperContext() const override;

    SpecularDataCanvas* m_specular_canvas;
};

#endif // BORNAGAIN_GUI_VIEW_FRAME_PLOT1DFRAME_H
