//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Frame/Plot2DFrame.h
//! @brief     Defines class Plot2DFrame.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FRAME_PLOT2DFRAME_H
#define BORNAGAIN_GUI_VIEW_FRAME_PLOT2DFRAME_H

#include "GUI/View/Frame/AnydataFrame.h"
#include "GUI/View/Setup/Canvas2DMode.h"

class Data2DItem;
class MaskEditorCanvas;
class MasksSet;
class ProjectedGraphsCanvas;
class QTabWidget;

//! Main widget to embed projections editor for Data2DItem.

class Plot2DFrame : public AnydataFrame {
public:
    Plot2DFrame(std::unique_ptr<DataSource>&& data_source);

private:
    void onActivityChanged(Canvas2DMode::Flag mode);
    void raiseMaskInStack(int relative_position);
    void updateFrame();
    void autoSwitchPanel(int to_panel);

    bool hasProperContext() const override;
    Data2DItem* data2DItem();
    MasksSet* maskSet();

    MaskEditorCanvas* m_canvas2D;      //!< canvas with color map at the top
    ProjectedGraphsCanvas* m_canvas1D; //!< bottom widget to draw projections plot
    QTabWidget* m_panels;
};

#endif // BORNAGAIN_GUI_VIEW_FRAME_PLOT2DFRAME_H
