//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Frame/StackedFrames.cpp
//! @brief     Implements class StackedFrames and children
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Frame/StackedFrames.h"
#include "GUI/Model/File/DatafilesSet.h"
#include "GUI/Model/Job/JobsSet.h"
#include "GUI/Model/Project/DataSource.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include "GUI/View/Frame/Fit1DFrame.h"
#include "GUI/View/Frame/Fit2DFrame.h"
#include "GUI/View/Frame/Plot1DFrame.h"
#include "GUI/View/Frame/Plot2DFrame.h"

StackedDataFrames::StackedDataFrames()
{
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    setMinimumSize(500, 600);

    addWidget(new Plot1DFrame(std::make_unique<DataFromData>()));
    addWidget(new Plot2DFrame(std::make_unique<DataFromData>()));
    setCurrentIndex(0);

    connect(gDoc->datafiles(), &DatafilesSet::setChanged, this,
            &StackedDataFrames::showCurrentFrame);
    showCurrentFrame();
}

void StackedDataFrames::showCurrentFrame()
{
    if (const DatafileItem* dfi = gDoc->datafiles()->currentItem())
        setCurrentIndex(dfi->rank() - 1);
    else
        setCurrentIndex(-1);
}

//  ************************************************************************************************

StackedJobFrames::StackedJobFrames()
{
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    setMinimumSize(500, 600);

    addWidget(new Plot1DFrame(std::make_unique<DataFromJob>()));
    addWidget(new Plot2DFrame(std::make_unique<DataFromJob>()));
    addWidget(new Fit1DFrame);
    addWidget(new Fit2DFrame);
    setCurrentIndex(0);

    showCurrentFrame();
}

void StackedJobFrames::showCurrentFrame()
{
    if (const JobItem* ji = gDoc->jobs()->currentItem()) {
        const int offset = ji->isValidForFitting() ? 2 : 0;
        setCurrentIndex(offset + ji->rank() - 1);
    } else
        setCurrentIndex(-1);
}
