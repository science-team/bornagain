//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Frame/StackedFrames.h
//! @brief     Defines class StackedFrames and children.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_FRAME_STACKEDFRAMES_H
#define BORNAGAIN_GUI_VIEW_FRAME_STACKEDFRAMES_H

#include <QStackedWidget>

class StackedDataFrames : public QStackedWidget {
public:
    StackedDataFrames();

private:
    void showCurrentFrame();
};

class StackedJobFrames : public QStackedWidget {
public:
    StackedJobFrames();
    void showCurrentFrame();
};

#endif // BORNAGAIN_GUI_VIEW_FRAME_STACKEDFRAMES_H
