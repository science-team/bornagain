//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/IO/ComponentRW.h
//! @brief     Defines functions for serialization in namespace IO.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_IO_COMPONENTRW_H
#define BORNAGAIN_GUI_VIEW_IO_COMPONENTRW_H

#include <QString>

namespace IO {

template <typename T> void saveComponentToXML(const QString& type, const T* t);
template <typename T> T* loadComponentFromXML(const QString& type);

} // namespace IO

#endif // BORNAGAIN_GUI_VIEW_IO_COMPONENTRW_H
