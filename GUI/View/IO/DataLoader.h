//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/IO/DataLoader.h
//! @brief     Defines functions for data import in namespace IO.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_IO_DATALOADER_H
#define BORNAGAIN_GUI_VIEW_IO_DATALOADER_H

#include <vector>

class DatafileItem;

namespace IO {

std::vector<DatafileItem*> importData1D();
std::vector<DatafileItem*> importData2D();

} // namespace IO

#endif // BORNAGAIN_GUI_VIEW_IO_DATALOADER_H
