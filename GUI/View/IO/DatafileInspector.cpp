//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/IO/DatafileInspector.cpp
//! @brief     Implements class DatafileInspector.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/IO/DatafileInspector.h"
#include "Base/Util/PathUtil.h"
#include "Device/IO/ZipUtil.h"
#include <QFile>
#include <QPlainTextEdit>
#include <QTabWidget>
#include <QVBoxLayout>

namespace {

QString text_from_file(const QString& fname)
{
    std::stringstream ss = ZipUtil::file2stream(fname.toStdString());
    return QString::fromStdString(ss.str());
}

} // namespace


DatafileInspector::DatafileInspector(QWidget* parent, const QString& fname)
    : QDialog(parent)
{
    setMinimumSize(900, 450);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    setWindowTitle(fname);

    auto* viewer = new QPlainTextEdit(::text_from_file(fname), parent);
    viewer->setLineWrapMode(QPlainTextEdit::NoWrap);

    auto* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(viewer);

    setLayout(mainLayout);
}
