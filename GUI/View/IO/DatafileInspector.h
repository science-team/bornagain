//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/IO/DatafileInspector.h
//! @brief     Defines class DatafileInspector.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_IO_DATAFILEINSPECTOR_H
#define BORNAGAIN_GUI_VIEW_IO_DATAFILEINSPECTOR_H

#include <QDialog>

class DatafileInspector : public QDialog {
    Q_OBJECT
public:
    DatafileInspector(QWidget* parent, const QString& fname);
};

#endif // BORNAGAIN_GUI_VIEW_IO_DATAFILEINSPECTOR_H
