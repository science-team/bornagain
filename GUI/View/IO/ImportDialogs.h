//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/IO/ImportDialogs.h
//! @brief     Defines class ImportDialog and its children.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_IO_IMPORTDIALOGS_H
#define BORNAGAIN_GUI_VIEW_IO_IMPORTDIALOGS_H

#include "Device/IO/ImportSettings.h"
#include <QDialog>

//! Base class for interactive loaders

class ImportDialog : public QDialog {
    Q_OBJECT
public:
    ImportDialog(QWidget* parent, QString fname);
};

//! Properties widget for the QREDataLoader

class Import1dDialog : public ImportDialog {
    Q_OBJECT
public:
    Import1dDialog(QWidget* parent, QString fname);

    static ImportSettings1D Msettings;
};

//! Properties widget for the 2D csv loader

class Import2dDialog : public ImportDialog {
    Q_OBJECT
public:
    Import2dDialog(QWidget* parent, QString fname);

    static ImportSettings2D Msettings;
};
#endif // BORNAGAIN_GUI_VIEW_IO_IMPORTDIALOGS_H
