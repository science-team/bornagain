//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/IO/ProjectionsSaver.h
//! @brief     Defines class ProjectionsSaver.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_IO_PROJECTIONSSAVER_H
#define BORNAGAIN_GUI_VIEW_IO_PROJECTIONSSAVER_H

class Data2DItem;

namespace IO {

//! Opens dialog, saves all projections into ASCII file.
void saveProjections(const Data2DItem* data_item);

} // namespace IO

#endif // BORNAGAIN_GUI_VIEW_IO_PROJECTIONSSAVER_H
