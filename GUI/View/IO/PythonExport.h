//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/IO/PythonExport.h
//! @brief     Declares Python export function in namespace IO.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_IO_PYTHONEXPORT_H
#define BORNAGAIN_GUI_VIEW_IO_PYTHONEXPORT_H

class InstrumentItem;
class SampleItem;
class SimulationOptionsItem;

//! The PythonScriptWidget displays a python script which represents full simulation.
//! Part of SimulationSetupWidget

namespace IO::Py {

void saveScript(const SampleItem* sampleItem, const InstrumentItem* instrumentItem,
                const SimulationOptionsItem* optionItem);
};

#endif // BORNAGAIN_GUI_VIEW_IO_PYTHONEXPORT_H
