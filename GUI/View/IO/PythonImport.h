//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/IO/PythonImport.h
//! @brief     Declares Python import functions in namespace IO.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_IO_PYTHONIMPORT_H
#define BORNAGAIN_GUI_VIEW_IO_PYTHONIMPORT_H

#ifdef BORNAGAIN_PYTHON

class SampleItem;

//! Assists in importing Python object to GUI models.

namespace IO::Py {

//! Show select-file dialog, try to import via python, return created sample.
//!
//! If sth. went wrong, a dialog has presented already and nullptr is returned.
SampleItem* importSample();

} // namespace IO::Py

#endif // BORNAGAIN_PYTHON

#endif // BORNAGAIN_GUI_VIEW_IO_PYTHONIMPORT_H
