//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/CautionSign.cpp
//! @brief     Implements class CautionSign.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Info/CautionSign.h"
#include "Base/Util/Assert.h"
#include "GUI/View/Info/CautionSignWidget.h"
#include <QAbstractScrollArea>
#include <QEvent>
#include <QScrollBar>
#include <QTimer>

namespace {

const int xpos_offset = 40;
const int ypos_offset = 40;

} // namespace

CautionSign::CautionSign(QWidget* parent)
    : QObject(parent)
    , m_caution_widget(nullptr)
    , m_area(nullptr)
    , m_clear_just_had_happened(false)
{
    setArea(parent);
}

//! Clears caution message;

void CautionSign::clear()
{
    delete m_caution_widget;
    m_caution_widget = nullptr;
    m_caution_message.clear();

    m_clear_just_had_happened = true;
    QTimer::singleShot(10, this, [this] { m_clear_just_had_happened = false; });
}

//! Shows caution sign on the screen. If clear of previous caution sign had happened just
//! few msec ago, make a small delay, to stress its reapearance.

void CautionSign::setCautionMessage(const QString& cautionMessage)
{
    ASSERT(m_area);

    if (m_clear_just_had_happened) {
        m_clear_just_had_happened = false;
        QTimer::singleShot(50, this, [this, cautionMessage] { setCautionMessage(cautionMessage); });
    } else {
        m_caution_message = cautionMessage;

        if (!m_caution_widget)
            m_caution_widget = new CautionSignWidget(m_area);

        m_caution_widget->setCautionMessage(m_caution_message);
        updateLabelGeometry();
        m_caution_widget->show();
    }
}

void CautionSign::setArea(QWidget* area)
{
    m_area = area;
    m_area->installEventFilter(this);
}

bool CautionSign::eventFilter(QObject* obj, QEvent* event)
{
    if (event->type() == QEvent::Resize)
        updateLabelGeometry();

    return QObject::eventFilter(obj, event);
}

void CautionSign::updateLabelGeometry()
{
    if (!m_caution_widget || !m_area)
        return;

    QPoint pos = positionForCautionSign();
    m_caution_widget->setPosition(pos.x(), pos.y());
}

QPoint CautionSign::positionForCautionSign() const
{
    ASSERT(m_area);

    int x = m_area->width() - xpos_offset;
    int y = m_area->height() - ypos_offset;

    if (auto* scrollArea = dynamic_cast<QAbstractScrollArea*>(m_area)) {
        if (QScrollBar* horizontal = scrollArea->horizontalScrollBar()) {
            if (horizontal->isVisible())
                y -= horizontal->height();
        }

        if (QScrollBar* vertical = scrollArea->verticalScrollBar()) {
            if (vertical->isVisible())
                x -= vertical->width();
        }
    }

    return {x, y};
}
