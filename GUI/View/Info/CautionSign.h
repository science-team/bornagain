//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/CautionSign.h
//! @brief     Defines class CautionSign.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INFO_CAUTIONSIGN_H
#define BORNAGAIN_GUI_VIEW_INFO_CAUTIONSIGN_H

#include <QObject>
#include <QWidget>

class CautionSignWidget;

//! The CautionSign controls appearance of CautionSignWidget on top of parent widget.

class CautionSign : public QObject {
    Q_OBJECT
public:
    CautionSign(QWidget* parent);

    void clear();

    void setCautionMessage(const QString& cautionMessage);

    void setArea(QWidget* area);

protected:
    bool eventFilter(QObject* obj, QEvent* event) override;

private:
    void updateLabelGeometry();
    QPoint positionForCautionSign() const;

    QString m_caution_message;
    CautionSignWidget* m_caution_widget;
    QWidget* m_area;
    bool m_clear_just_had_happened;
};

#endif // BORNAGAIN_GUI_VIEW_INFO_CAUTIONSIGN_H
