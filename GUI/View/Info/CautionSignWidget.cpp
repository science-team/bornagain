//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/CautionSignWidget.cpp
//! @brief     Implements class CautionSignWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Info/CautionSignWidget.h"
#include <QMessageBox>
#include <QPainter>

CautionSignWidget::CautionSignWidget(QWidget* parent)
    : QWidget(parent)
    , m_pixmap(":/images/warning@2x.png")
    , m_caution_header("BornAgain warning")
{
    setAttribute(Qt::WA_NoSystemBackground);
    setToolTip(m_caution_header + "\nClick to see details.");
}

void CautionSignWidget::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    QRect target(m_pixmap.rect());
    painter.drawPixmap(target, m_pixmap);
}

void CautionSignWidget::mousePressEvent(QMouseEvent* event)
{
    Q_UNUSED(event);
    QMessageBox box;
    box.setWindowTitle(m_caution_header);
    box.setInformativeText(m_caution_message);
    box.setStandardButtons(QMessageBox::Ok);
    box.setDefaultButton(QMessageBox::Ok);
    box.exec();
}

//! set geometry of widget around center point
void CautionSignWidget::setPosition(int x, int y)
{
    setGeometry(x, y, m_pixmap.width(), m_pixmap.height());
}
