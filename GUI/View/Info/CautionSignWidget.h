//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/CautionSignWidget.h
//! @brief     Defines class CautionSignWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INFO_CAUTIONSIGNWIDGET_H
#define BORNAGAIN_GUI_VIEW_INFO_CAUTIONSIGNWIDGET_H

#include <QPixmap>
#include <QString>
#include <QWidget>

//! The CautionSignWidget is an transparent widget with caution sign pixmap intended to be
//! overlayed onto other widget at some arbitrary position.
class CautionSignWidget : public QWidget {
    Q_OBJECT
public:
    CautionSignWidget(QWidget* parent = nullptr);

    void setPosition(int x, int y);

    void setCautionMessage(const QString& message) { m_caution_message = message; }

private:
    void paintEvent(QPaintEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;

    QPixmap m_pixmap;
    QString m_caution_header;
    QString m_caution_message;
};

#endif // BORNAGAIN_GUI_VIEW_INFO_CAUTIONSIGNWIDGET_H
