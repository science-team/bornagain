//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/ComboSelectorDialog.cpp
//! @brief     Implements class ComboSelectorDialog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Info/ComboSelectorDialog.h"
#include <QApplication> // qApp
#include <QPushButton>
#include <QVBoxLayout>

ComboSelectorDialog::ComboSelectorDialog(QWidget* parent)
    : QDialog(parent)
    , m_top_label(new QLabel)
    , m_combo_selector(new QComboBox)
    , m_bottom_label(new QLabel)
{
    QColor bgColor(240, 240, 240, 255);
    QPalette palette;
    palette.setColor(QPalette::Window, bgColor);
    setAutoFillBackground(true);
    setPalette(palette);

    setFixedSize(500, 250);
    setWindowTitle("Please make a selection");
    setWindowFlags(Qt::Dialog);

    auto* topLayout = new QHBoxLayout;
    topLayout->addLayout(createLogoLayout(), 0);
    topLayout->addLayout(createInfoLayout(), 1);

    auto* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(createButtonLayout());

    setLayout(mainLayout);
}

void ComboSelectorDialog::addItems(const QStringList& selection, const QString& currentItem)
{
    m_combo_selector->addItems(selection);

    if (selection.contains(currentItem))
        m_combo_selector->setCurrentIndex(selection.indexOf(currentItem));
}

void ComboSelectorDialog::setTextTop(const QString& text)
{
    m_top_label->setText(text);
}

void ComboSelectorDialog::setTextBottom(const QString& text)
{
    m_bottom_label->setText(text);
}

QString ComboSelectorDialog::currentText() const
{
    return m_combo_selector->currentText();
}

//! Returns layout with icon for left part of the widget.

QBoxLayout* ComboSelectorDialog::createLogoLayout()
{
    auto* result = new QVBoxLayout;
    result->setContentsMargins(0, 5, 0, 5);

    QIcon icon = qApp->style()->standardIcon(QStyle::SP_MessageBoxQuestion);

    auto* label = new QLabel;
    label->setPixmap(icon.pixmap(100));

    result->addWidget(label);
    result->addStretch(1);

    return result;
}

//! Creates right layout with text and QComboBox selection.

QBoxLayout* ComboSelectorDialog::createInfoLayout()
{
    auto* result = new QVBoxLayout;
    result->setContentsMargins(0, 5, 5, 5);

    m_top_label->setWordWrap(true);
    m_bottom_label->setWordWrap(true);

    result->addWidget(m_top_label);
    result->addStretch(1);
    result->addWidget(m_combo_selector);
    result->addStretch(1);
    result->addWidget(m_bottom_label);
    result->addStretch(1);

    return result;
}

//! Creates button layout with buttons.

QBoxLayout* ComboSelectorDialog::createButtonLayout()
{
    auto* result = new QHBoxLayout;

    auto* cancelButton = new QPushButton("Cancel");
    connect(cancelButton, &QPushButton::clicked, this, &ComboSelectorDialog::reject);

    auto* okButton = new QPushButton("Try current selection");
    connect(okButton, &QPushButton::clicked, this, &ComboSelectorDialog::accept);

    result->addStretch(1);
    result->addWidget(okButton);
    result->addWidget(cancelButton);

    return result;
}
