//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/ComboSelectorDialog.h
//! @brief     Defines class ComboSelectorDialog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INFO_COMBOSELECTORDIALOG_H
#define BORNAGAIN_GUI_VIEW_INFO_COMBOSELECTORDIALOG_H

#include <QBoxLayout>
#include <QComboBox>
#include <QDialog>
#include <QLabel>
#include <QString>

//! A dialog similar to standard QMessageBox with combo box selector.

class ComboSelectorDialog : public QDialog {
    Q_OBJECT
public:
    ComboSelectorDialog(QWidget* parent = nullptr);

    void addItems(const QStringList& selection, const QString& currentItem = "");
    void setTextTop(const QString& text);
    void setTextBottom(const QString& text);

    QString currentText() const;

private:
    QBoxLayout* createLogoLayout();
    QBoxLayout* createInfoLayout();
    QBoxLayout* createButtonLayout();

    QLabel* m_top_label;
    QComboBox* m_combo_selector;
    QLabel* m_bottom_label;
};

#endif // BORNAGAIN_GUI_VIEW_INFO_COMBOSELECTORDIALOG_H
