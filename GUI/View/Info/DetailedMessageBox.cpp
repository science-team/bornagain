//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/DetailedMessageBox.cpp
//! @brief     Implements class DetailedMessageBox.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Info/DetailedMessageBox.h"
#include <QApplication>
#include <QPushButton>
#include <QStyle>
#include <QVBoxLayout>

namespace {

const QSize default_dialog_size(512, 300);
} // namespace

DetailedMessageBox::DetailedMessageBox(QWidget* parent, const QString& title, const QString& text,
                                       const QString& details)
    : QDialog(parent)
    , m_top_label(new QLabel)
    , m_text_edit(new QTextEdit)
{
    setWindowTitle(title);
    m_top_label->setText(text);
    m_text_edit->setText(details);
    m_text_edit->setReadOnly(true);
    m_text_edit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setWindowFlags(Qt::Dialog);

    resize(default_dialog_size);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QColor bgColor(240, 240, 240, 255);
    QPalette palette;
    palette.setColor(QPalette::Window, bgColor);
    setAutoFillBackground(true);
    setPalette(palette);

    auto* topLayout = new QHBoxLayout;
    topLayout->addLayout(createLogoLayout());
    topLayout->addLayout(createInfoLayout());
    topLayout->addStretch(1);

    auto* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(topLayout);
    mainLayout->addWidget(m_text_edit);
    mainLayout->addLayout(createButtonLayout());

    setLayout(mainLayout);

    setSizeGripEnabled(true);
}

//! Returns layout with icon for left part of the widget.

QBoxLayout* DetailedMessageBox::createLogoLayout() const
{
    auto* result = new QVBoxLayout;
    result->setContentsMargins(5, 5, 5, 5);

    QIcon icon = QApplication::style()->standardIcon(QStyle::SP_MessageBoxWarning);

    auto* label = new QLabel;
    label->setPixmap(icon.pixmap(128));

    result->addWidget(label);

    return result;
}

//! Creates right layout with text and QComboBox selection.

QBoxLayout* DetailedMessageBox::createInfoLayout() const
{
    m_top_label->setWordWrap(true);

    auto* result = new QVBoxLayout;
    result->setContentsMargins(5, 5, 5, 5);
    result->addWidget(m_top_label);
    return result;
}

//! Creates button layout with buttons.

QBoxLayout* DetailedMessageBox::createButtonLayout() const
{
    auto* result = new QHBoxLayout;

    auto* okButton = new QPushButton("Ok");
    connect(okButton, &QPushButton::clicked, this, &DetailedMessageBox::reject);

    result->addStretch(1);
    result->addWidget(okButton);

    return result;
}
