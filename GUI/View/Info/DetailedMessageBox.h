//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/DetailedMessageBox.h
//! @brief     Defines class DetailedMessageBox.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INFO_DETAILEDMESSAGEBOX_H
#define BORNAGAIN_GUI_VIEW_INFO_DETAILEDMESSAGEBOX_H

#include <QBoxLayout>
#include <QDialog>
#include <QLabel>
#include <QString>
#include <QTextEdit>

//! A dialog similar to standard QMessageBox intended for detailed warning messages.
//! On the contrary to QMessageBox, the dialog has size grip and visible text editor.

class DetailedMessageBox : public QDialog {
    Q_OBJECT
public:
    DetailedMessageBox(QWidget* parent, const QString& title, const QString& text,
                       const QString& details);

private:
    QBoxLayout* createLogoLayout() const;
    QBoxLayout* createInfoLayout() const;
    QBoxLayout* createButtonLayout() const;

    QLabel* m_top_label;
    QTextEdit* m_text_edit;
};

#endif // BORNAGAIN_GUI_VIEW_INFO_DETAILEDMESSAGEBOX_H
