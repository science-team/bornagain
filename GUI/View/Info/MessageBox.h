//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/MessageBox.h
//! @brief     Declares namespace GUI::Message.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INFO_MESSAGEBOX_H
#define BORNAGAIN_GUI_VIEW_INFO_MESSAGEBOX_H

#include <QWidget>

namespace GUI::Message {

void information(const QString& title, const QString& text, const QString& detailedText = "");

void warning(const QString& title, const QString& text, const QString& detailedText = "");

bool question(const QString& title, const QString& text, const QString& detailedText = "",
              const QString& yesText = "&Yes", const QString& noText = "&No");

} // namespace GUI::Message

#endif // BORNAGAIN_GUI_VIEW_INFO_MESSAGEBOX_H
