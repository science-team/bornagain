//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/OverlayLabelController.h
//! @brief     Defines class OverlayLabelController.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INFO_OVERLAYLABELCONTROLLER_H
#define BORNAGAIN_GUI_VIEW_INFO_OVERLAYLABELCONTROLLER_H

#include <QAbstractScrollArea>
#include <QObject>
#include <QString>

class OverlayLabelWidget;

//! Controlls appearance of InfoLabelWidget (position, show/hide) on top of some scroll area.

class OverlayLabelController : public QObject {
    Q_OBJECT
public:
    OverlayLabelController(QObject* parent = nullptr);

    void setText(const QString& text);
    void setArea(QAbstractScrollArea* area);
    void setShown(bool shown);

private:
    bool eventFilter(QObject* obj, QEvent* event) override;
    void updateLabelGeometry();

    OverlayLabelWidget* m_label;
    QAbstractScrollArea* m_area;
    QString m_text;
};

#endif // BORNAGAIN_GUI_VIEW_INFO_OVERLAYLABELCONTROLLER_H
