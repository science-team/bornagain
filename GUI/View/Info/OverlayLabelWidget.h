//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/OverlayLabelWidget.h
//! @brief     Defines class OverlayLabelWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INFO_OVERLAYLABELWIDGET_H
#define BORNAGAIN_GUI_VIEW_INFO_OVERLAYLABELWIDGET_H

#include <QRect>
#include <QString>
#include <QWidget>

//! A semi-transparent overlay label to place on top of other widgets outside of any layout context.

class OverlayLabelWidget : public QWidget {
    Q_OBJECT
public:
    OverlayLabelWidget(QWidget* parent = nullptr);

    void setRectangle(const QRect& rect);
    void setPosition(int x, int y);

    void setText(const QString& text) { m_text = text; }

private:
    void paintEvent(QPaintEvent* event) override;

    QString m_text;
    QRect m_bounding_rect;
};

#endif // BORNAGAIN_GUI_VIEW_INFO_OVERLAYLABELWIDGET_H
