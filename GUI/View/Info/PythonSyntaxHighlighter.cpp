//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/PythonSyntaxHighlighter.cpp
//! @brief     Implements class PythonSyntaxHighlighter.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

/*
This is a C++ port of the following PyQt example
http://diotavelli.net/PyQtWiki/Python%20syntax%20highlighting
C++ port by Frankie Simon (docklight.de, www.fuh-edv.de)

The following free software license applies for this file ("X11 license"):

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "GUI/View/Info/PythonSyntaxHighlighter.h"

PythonSyntaxHighlighter::PythonSyntaxHighlighter(QTextDocument* parent)
    : QSyntaxHighlighter(parent)
{
    keywords = QStringList() << "and"
                             << "assert"
                             << "break"
                             << "class"
                             << "continue"
                             << "def"
                             << "del"
                             << "elif"
                             << "else"
                             << "except"
                             << "exec"
                             << "finally"
                             << "for"
                             << "from"
                             << "global"
                             << "if"
                             << "import"
                             << "in"
                             << "is"
                             << "lambda"
                             << "not"
                             << "or"
                             << "pass"
                             << "print"
                             << "raise"
                             << "return"
                             << "try"
                             << "while"
                             << "yield"
                             << "None"
                             << "True"
                             << "False";

    operators = QStringList() << "=" <<
                // Comparison
                "==" << "!="
                              << "<"
                              << "<="
                              << ">"
                              << ">=" <<
                // Arithmetic
                "\\+" << "-"
                              << "\\*"
                              << "/"
                              << "//"
                              << "%"
                              << "\\*\\*" <<
                // In-place
                "\\+=" << "-="
                              << "\\*="
                              << "/="
                              << "%=" <<
                // Bitwise
                "\\^" << "\\|"
                              << "&"
                              << "~"
                              << ">>"
                              << "<<";

    braces = QStringList() << "{"
                           << "}"
                           << "\\("
                           << "\\)"
                           << "\\["
                           << "]";

    basicStyles.insert("keyword", getTextCharFormat("blue"));
    basicStyles.insert("operator", getTextCharFormat("red"));
    basicStyles.insert("brace", getTextCharFormat("darkGray"));
    basicStyles.insert("defclass", getTextCharFormat("black", "bold"));
    basicStyles.insert("brace", getTextCharFormat("black"));
    basicStyles.insert("string", getTextCharFormat("magenta"));
    basicStyles.insert("string2", getTextCharFormat("darkMagenta"));
    basicStyles.insert("comment", getTextCharFormat("darkGreen", "italic"));
    basicStyles.insert("self", getTextCharFormat("black", "italic"));
    basicStyles.insert("numbers", getTextCharFormat("brown"));

    triSingleQuote.setPattern("'''");
    triDoubleQuote.setPattern(R"(""")");

    initializeRules();
}

void PythonSyntaxHighlighter::initializeRules()
{
    for (const QString& currKeyword : keywords) {
        rules.append(HighlightingRule(QString("\\b%1\\b").arg(currKeyword), 0,
                                      basicStyles.value("keyword")));
    }
    for (const QString& currOperator : operators) {
        rules.append(
            HighlightingRule(QString("%1").arg(currOperator), 0, basicStyles.value("operator")));
    }
    for (const QString& currBrace : braces)
        rules.append(HighlightingRule(QString("%1").arg(currBrace), 0, basicStyles.value("brace")));
    // 'self'
    rules.append(HighlightingRule("\\bself\\b", 0, basicStyles.value("self")));

    // Double-quoted string, possibly containing escape sequences
    // FF: originally in python : r'"[^"\\]*(\\.[^"\\]*)*"'
    rules.append(HighlightingRule(R"("[^"\\]*(\\.[^"\\]*)*")", 0, basicStyles.value("string")));
    // Single-quoted string, possibly containing escape sequences
    // FF: originally in python : r"'[^'\\]*(\\.[^'\\]*)*'"
    rules.append(HighlightingRule(R"('[^'\\]*(\\.[^'\\]*)*')", 0, basicStyles.value("string")));

    // 'def' followed by an identifier
    // FF: originally: r'\bdef\b\s*(\w+)'
    rules.append(HighlightingRule(R"(\bdef\b\s*(\w+))", 1, basicStyles.value("defclass")));
    //  'class' followed by an identifier
    // FF: originally: r'\bclass\b\s*(\w+)'
    rules.append(HighlightingRule(R"(\bclass\b\s*(\w+))", 1, basicStyles.value("defclass")));

    // From '#' until a newline
    // FF: originally: r'#[^\\n]*'
    rules.append(HighlightingRule("#[^\\n]*", 0, basicStyles.value("comment")));

    // Numeric literals
    rules.append(HighlightingRule("\\b[+-]?[0-9]+[lL]?\\b", 0,
                                  basicStyles.value("numbers"))); // r'\b[+-]?[0-9]+[lL]?\b'
    rules.append(
        HighlightingRule("\\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\\b", 0,
                         basicStyles.value("numbers"))); // r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b'
    rules.append(HighlightingRule(
        R"(\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b)", 0,
        basicStyles.value("numbers"))); // r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b'
}

void PythonSyntaxHighlighter::highlightBlock(const QString& text)
{
    for (const HighlightingRule& currRule : rules) {
        QRegularExpressionMatch matched{currRule.pattern.match(text)};
        int idx = matched.capturedStart(0);
        while (idx >= 0) {
            // Get index of Nth match
            idx = matched.capturedStart(currRule.nth);
            const int length = matched.capturedLength(currRule.nth);
            setFormat(idx, length, currRule.format);
            matched = currRule.pattern.match(text, idx + length);
            idx = matched.capturedStart(0);
        }
    }
    setCurrentBlockState(0);

    // Do multi-line strings
    if (!matchMultiline(text, triSingleQuote, 1, basicStyles.value("string2")))
        matchMultiline(text, triDoubleQuote, 2, basicStyles.value("string2"));
}

bool PythonSyntaxHighlighter::matchMultiline(const QString& text,
                                             const QRegularExpression& delimiter, const int inState,
                                             const QTextCharFormat& style)
{
    int start = -1;
    int add = -1;
    int end = -1;
    int length = 0;

    // If inside triple-single quotes, start at 0
    if (previousBlockState() == inState) {
        start = 0;
        add = 0;
    }
    // Otherwise, look for the delimiter on this line
    else {
        QRegularExpressionMatch matched{delimiter.match(text)};
        // TODO: What if match fails?
        start = matched.capturedStart(0);
        // Move past this match
        add = matched.capturedLength(0);
    }
    // As long as there's a delimiter match on this line...
    while (start >= 0) {
        // Look for the ending delimiter
        // TODO: What if match fails?
        QRegularExpressionMatch matched{delimiter.match(text, start + add)};
        end = matched.capturedStart(0);
        // Ending delimiter on this line?
        if (end >= add) {
            length = end - start + add + matched.capturedLength(0);
            setCurrentBlockState(0);
        }
        // No; multi-line string
        else {
            setCurrentBlockState(inState);
            length = text.length() - start + add;
        }
        // Apply formatting and look for next
        setFormat(start, length, style);
        matched = delimiter.match(text, start + length);
        // TODO: What if match fails?
        start = matched.capturedStart(0);
    }
    // Return True if still inside a multi-line string, False otherwise
    return currentBlockState() == inState;
}

QTextCharFormat PythonSyntaxHighlighter::getTextCharFormat(const QString& colorName,
                                                           const QString& style)
{
    QTextCharFormat charFormat;
    QColor color(colorName);
    charFormat.setForeground(color);
    if (style.contains("bold", Qt::CaseInsensitive))
        charFormat.setFontWeight(QFont::Bold);
    if (style.contains("italic", Qt::CaseInsensitive))
        charFormat.setFontItalic(true);
    return charFormat;
}
