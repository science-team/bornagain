//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Info/PythonSyntaxHighlighter.h
//! @brief     Defines class PythonSyntaxHighlighter.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

/*
This is a C++ port of the following PyQt example
http://diotavelli.net/PyQtWiki/Python%20syntax%20highlighting
C++ port by Frankie Simon (docklight.de, www.fuh-edv.de)

The following free software license applies for this file ("X11 license"):

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef BORNAGAIN_GUI_VIEW_INFO_PYTHONSYNTAXHIGHLIGHTER_H
#define BORNAGAIN_GUI_VIEW_INFO_PYTHONSYNTAXHIGHLIGHTER_H

#include <QRegularExpression>
#include <QSyntaxHighlighter>

//! Container to describe a highlighting rule. Based on a regular expression, a relevant match # and
//! the format.
class HighlightingRule {
public:
    HighlightingRule(const QString& patternStr, int n, const QTextCharFormat& matchingFormat)
        : originalRuleStr{patternStr}
        , pattern{patternStr}
        , nth{n}
        , format{matchingFormat}
    {
    }

public:
    QString originalRuleStr;
    QRegularExpression pattern;
    int nth;
    QTextCharFormat format;
};

//! Implementation of highlighting for Python code.
class PythonSyntaxHighlighter : public QSyntaxHighlighter {
    Q_OBJECT
public:
    PythonSyntaxHighlighter(QTextDocument* parent = nullptr);

private:
    void highlightBlock(const QString& text) override;

    QStringList keywords;
    QStringList operators;
    QStringList braces;

    QHash<QString, QTextCharFormat> basicStyles;

    void initializeRules();

    //! Highlighst multi-line strings, returns true if after processing we are still within the
    // multi-line section.
    bool matchMultiline(const QString& text, const QRegularExpression& delimiter, int inState,
                        const QTextCharFormat& style);
    QTextCharFormat getTextCharFormat(const QString& colorName, const QString& style = "");

    QVector<HighlightingRule> rules;
    QRegularExpression triSingleQuote;
    QRegularExpression triDoubleQuote;
};

#endif // BORNAGAIN_GUI_VIEW_INFO_PYTHONSYNTAXHIGHLIGHTER_H
