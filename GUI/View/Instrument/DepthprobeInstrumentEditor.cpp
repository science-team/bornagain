//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Instrument/DepthprobeInstrumentEditor.cpp
//! @brief     Implements class DepthprobeInstrumentEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Instrument/DepthprobeInstrumentEditor.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/View/Device/AxisForm.h"
#include "GUI/View/Device/ScanEditor.h"
#include <QVBoxLayout>

DepthprobeInstrumentEditor::DepthprobeInstrumentEditor(DepthprobeInstrumentItem* instrument)
{
    ASSERT(instrument);
    auto* layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto* scanEditor = new ScanEditor(this, instrument, instrument->scanItem(), false, false, true);
    layout->addWidget(scanEditor);

    auto* depthAxisEditor = new AxisForm(this, "Depth axis", &instrument->zAxis(),
                                         "Number of points in scan across sample bulk");
    layout->addWidget(depthAxisEditor);

    layout->addStretch(1);
}
