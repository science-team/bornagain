//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Instrument/DepthprobeInstrumentEditor.h
//! @brief     Defines class DepthprobeInstrumentEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INSTRUMENT_DEPTHPROBEINSTRUMENTEDITOR_H
#define BORNAGAIN_GUI_VIEW_INSTRUMENT_DEPTHPROBEINSTRUMENTEDITOR_H

#include "GUI/View/Widget/IComponentEditor.h"

class DepthprobeInstrumentItem;

//! Depthprobe instrument editor, for use in main scroll area of InstrumentView.

class DepthprobeInstrumentEditor : public IComponentEditor {
    Q_OBJECT
public:
    DepthprobeInstrumentEditor(DepthprobeInstrumentItem* instrument);
};

#endif // BORNAGAIN_GUI_VIEW_INSTRUMENT_DEPTHPROBEINSTRUMENTEDITOR_H
