//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Instrument/OffspecInstrumentEditor.cpp
//! @brief     Implements class OffspecInstrumentEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Instrument/OffspecInstrumentEditor.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/View/Device/BackgroundForm.h"
#include "GUI/View/Device/OffspecDetectorEditor.h"
#include "GUI/View/Device/PolarizationAnalysisEditor.h"
#include "GUI/View/Device/ScanEditor.h"

OffspecInstrumentEditor::OffspecInstrumentEditor(OffspecInstrumentItem* instrument)
{
    ASSERT(instrument);
    auto* layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto* scanEditor = new ScanEditor(this, instrument, instrument->scanItem(), true, true, true);
    layout->addWidget(scanEditor);

    auto* detectorEditor = new OffspecDetectorEditor(this, instrument);
    layout->addWidget(detectorEditor);

    auto* polMatricesAnalysisEditor = new PolarizationAnalysisEditor(this, instrument);
    layout->addWidget(polMatricesAnalysisEditor);

    auto* backgroundForm = new BackgroundForm(this, instrument);
    layout->addWidget(backgroundForm);

    layout->addStretch(1);
}
