//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Instrument/OffspecInstrumentEditor.h
//! @brief     Defines class OffspecInstrumentEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INSTRUMENT_OFFSPECINSTRUMENTEDITOR_H
#define BORNAGAIN_GUI_VIEW_INSTRUMENT_OFFSPECINSTRUMENTEDITOR_H

#include "GUI/View/Widget/IComponentEditor.h"

class OffspecInstrumentItem;

//! Offspec instrument editor, for use in main scroll area of InstrumentView.

class OffspecInstrumentEditor : public IComponentEditor {
    Q_OBJECT
public:
    OffspecInstrumentEditor(OffspecInstrumentItem* instrument);
};

#endif // BORNAGAIN_GUI_VIEW_INSTRUMENT_OFFSPECINSTRUMENTEDITOR_H
