//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Instrument/Scatter2DInstrumentEditor.cpp
//! @brief     Implements class Scatter2DInstrumentEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Instrument/Scatter2DInstrumentEditor.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/View/Device/BackgroundForm.h"
#include "GUI/View/Device/DetectorEditor.h"
#include "GUI/View/Device/GISASBeamEditor.h"
#include "GUI/View/Device/PolarizationAnalysisEditor.h"

Scatter2DInstrumentEditor::Scatter2DInstrumentEditor(Scatter2DInstrumentItem* instrument)
{
    ASSERT(instrument);
    auto* layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto* beamEditor = new GISASBeamEditor(this, instrument->beamItem());
    layout->addWidget(beamEditor);

    auto* detectorEditor = new DetectorEditor(this, instrument);
    layout->addWidget(detectorEditor);

    auto* polMatricesAnalysisEditor = new PolarizationAnalysisEditor(this, instrument);
    layout->addWidget(polMatricesAnalysisEditor);

    auto* backgroundForm = new BackgroundForm(this, instrument);
    layout->addWidget(backgroundForm);

    layout->addStretch();
}
