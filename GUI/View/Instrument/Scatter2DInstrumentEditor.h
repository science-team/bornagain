//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Instrument/Scatter2DInstrumentEditor.h
//! @brief     Defines class Scatter2DInstrumentEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INSTRUMENT_SCATTER2DINSTRUMENTEDITOR_H
#define BORNAGAIN_GUI_VIEW_INSTRUMENT_SCATTER2DINSTRUMENTEDITOR_H

#include "GUI/View/Widget/IComponentEditor.h"

class Scatter2DInstrumentItem;

//! Editor for GISAS instruments, for use in main scroll area of InstrumentView.

class Scatter2DInstrumentEditor : public IComponentEditor {
    Q_OBJECT
public:
    Scatter2DInstrumentEditor(Scatter2DInstrumentItem* instrument);
};

#endif // BORNAGAIN_GUI_VIEW_INSTRUMENT_SCATTER2DINSTRUMENTEDITOR_H
