//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Instrument/SpecularInstrumentEditor.cpp
//! @brief     Implements class SpecularInstrumentEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Instrument/SpecularInstrumentEditor.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/View/Device/BackgroundForm.h"
#include "GUI/View/Device/PolarizationAnalysisEditor.h"
#include "GUI/View/Device/ScanEditor.h"

SpecularInstrumentEditor::SpecularInstrumentEditor(SpecularInstrumentItem* instrument)
{
    ASSERT(instrument);
    auto* layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto* scanEditor = new ScanEditor(this, instrument, instrument->scanItem(), false, true, true);
    layout->addWidget(scanEditor);

    auto* polMatricesAnalysisEditor = new PolarizationAnalysisEditor(this, instrument);
    layout->addWidget(polMatricesAnalysisEditor);

    auto* backgroundForm = new BackgroundForm(this, instrument);
    layout->addWidget(backgroundForm);

    layout->addStretch(1);
}
