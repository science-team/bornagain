//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Instrument/SpecularInstrumentEditor.h
//! @brief     Defines class SpecularInstrumentEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_INSTRUMENT_SPECULARINSTRUMENTEDITOR_H
#define BORNAGAIN_GUI_VIEW_INSTRUMENT_SPECULARINSTRUMENTEDITOR_H

#include "GUI/View/Widget/IComponentEditor.h"

class SpecularInstrumentItem;

//! Specular instrument editor, for use in main scroll area of InstrumentView.

class SpecularInstrumentEditor : public IComponentEditor {
    Q_OBJECT
public:
    SpecularInstrumentEditor(SpecularInstrumentItem* instrument);
};

#endif // BORNAGAIN_GUI_VIEW_INSTRUMENT_SPECULARINSTRUMENTEDITOR_H
