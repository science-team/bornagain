//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/JobMessagesDisplay.h
//! @brief     Defines class JobMessagesDisplay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_JOB_JOBMESSAGESDISPLAY_H
#define BORNAGAIN_GUI_VIEW_JOB_JOBMESSAGESDISPLAY_H

#include <QTextEdit>

class JobItem;

enum class JobStatus;

//! Holds tabs for properties view and comments editor.

class JobMessagesDisplay : public QWidget {
    Q_OBJECT
public:
    explicit JobMessagesDisplay(QWidget* parent = nullptr, Qt::WindowFlags f = {});
    void setJobItem(JobItem* jobItem);

private slots:
    void onCommentsEdited();
    void onJobCommentsChanged(const QString& comments);
    void onJobStatusChanged(JobStatus status);
    void onJobDestroyed();

private:
    QTextEdit* m_comments_editor;
    JobItem* m_job_item;
};

#endif // BORNAGAIN_GUI_VIEW_JOB_JOBMESSAGESDISPLAY_H
