//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/JobProgressDelegate.h
//! @brief     Defines class JobProgressDelegate.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_JOB_JOBPROGRESSDELEGATE_H
#define BORNAGAIN_GUI_VIEW_JOB_JOBPROGRESSDELEGATE_H

#include <QItemDelegate>
#include <QMap>
#include <QRect>

enum class JobStatus;

class JobItem;

//! Used in JobsListing to show progress bar.

class JobProgressDelegate : public QItemDelegate {
    Q_OBJECT
public:
    JobProgressDelegate(QWidget* parent);

    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const override;

    bool editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option,
                     const QModelIndex& index) override;

signals:
    void cancelButtonClicked(const QModelIndexList indices);

private:
    QStyle::State m_button_state;
    void drawCustomProjectBar(const JobItem* item, QPainter* painter,
                              const QStyleOptionViewItem& option) const;

    QRect getTextRect(QRect optionRect) const;
    QRect getProgressBarRect(QRect optionRect) const;
    QRect getButtonRect(QRect optionRect) const;

    QMap<JobStatus, QColor> m_status_to_color;
};

#endif // BORNAGAIN_GUI_VIEW_JOB_JOBPROGRESSDELEGATE_H
