//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/JobViewActivities.cpp
//! @brief     Implements class JobViewActivities.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Job/JobViewActivities.h"
#include "Base/Util/Assert.h"
#include <QMap>

namespace {

const QMap<JobViewActivity, QVector<JobViewFlags::Dock>> activityToDocks = {
    {JobViewActivity::JobView, {JobViewFlags::JOB_LIST_DOCK}},
    {JobViewActivity::RealTime, {JobViewFlags::JOB_LIST_DOCK, JobViewFlags::REAL_TIME_DOCK}},
    {JobViewActivity::Fitting,
     {JobViewFlags::JOB_LIST_DOCK, JobViewFlags::REAL_TIME_DOCK, JobViewFlags::FIT_PANEL_DOCK,
      JobViewFlags::JOB_MESSAGE_DOCK}}};

const QMap<JobViewActivity, QString> activityNames = {
    {JobViewActivity::JobView, "Job View Activity"},
    {JobViewActivity::RealTime, "Real Time Activity"},
    {JobViewActivity::Fitting, "Fitting Activity"}};

} // namespace


//! Returns list of all available activities.

QVector<JobViewActivity> JobViewActivities::all()
{
    return activityNames.keys();
}

//! Returns name of activity.

QString JobViewActivities::nameFromActivity(JobViewActivity activity)
{
    QMap<JobViewActivity, QString>::const_iterator it = activityNames.find(activity);
    ASSERT(it != activityNames.end());
    return it.value();
}

//! Returns activity by its name.

JobViewActivity JobViewActivities::activityFromName(QString name)
{
    ASSERT(activityNames.values().contains(name));
    return activityNames.key(name);
}

//! Returns vector of JobView's dockId which have to be shown for given activity.

QVector<JobViewFlags::Dock> JobViewActivities::activeDocks(JobViewActivity activity)
{
    QMap<JobViewActivity, QVector<JobViewFlags::Dock>>::const_iterator it =
        activityToDocks.find(activity);
    ASSERT(it != activityToDocks.end());
    return it.value();
}
