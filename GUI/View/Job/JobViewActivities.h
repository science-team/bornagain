//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/JobViewActivities.h
//! @brief     Defines class JobViewActivities.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_JOB_JOBVIEWACTIVITIES_H
#define BORNAGAIN_GUI_VIEW_JOB_JOBVIEWACTIVITIES_H

#include <QFlags>
#include <QString>
#include <QVector>

//! The JobViewFlags class is a namespace for various flags used in JobView.

class JobViewFlags {
public:
    enum EDocksId {
        JOB_LIST_DOCK,
        REAL_TIME_DOCK,
        FIT_PANEL_DOCK,
        JOB_MESSAGE_DOCK,
    };
    Q_DECLARE_FLAGS(Dock, EDocksId)
};

Q_DECLARE_OPERATORS_FOR_FLAGS(JobViewFlags::Dock)

enum class JobViewActivity { JobView, RealTime, Fitting };

//! The JobViewActivities namespace is a helper to get info related to JobView activities

namespace JobViewActivities {

QVector<JobViewActivity> all();

QString nameFromActivity(JobViewActivity activity);
JobViewActivity activityFromName(QString name);

QVector<JobViewFlags::Dock> activeDocks(JobViewActivity activity);

} // namespace JobViewActivities

#endif // BORNAGAIN_GUI_VIEW_JOB_JOBVIEWACTIVITIES_H
