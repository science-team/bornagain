//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/JobparQModel.h
//! @brief     Defines class JobparQModel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_JOB_JOBPARQMODEL_H
#define BORNAGAIN_GUI_VIEW_JOB_JOBPARQMODEL_H

#include <QAbstractTableModel>

class JobItem;

//! A table model for the properties of a job except for the comment.
//! The name of the job is editable, all other fields are read only.

class JobparQModel : public QAbstractTableModel {
    Q_OBJECT
public:
    explicit JobparQModel(QObject* parent = nullptr);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
};

#endif // BORNAGAIN_GUI_VIEW_JOB_JOBPARQMODEL_H
