//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/JobsListing.h
//! @brief     Defines class JobsListing.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_JOB_JOBSLISTING_H
#define BORNAGAIN_GUI_VIEW_JOB_JOBSLISTING_H

#include "GUI/Model/Job/JobItem.h"
#include <QAction>
#include <QListView>
#include <QMenu>
#include <QWidget>

class JobItem;
class JobProgressDelegate;
class JobsQModel;

//! List of jobs on the top left side of JobView.

class JobsListing : public QWidget {
    Q_OBJECT
public:
    JobsListing(QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    QVector<JobItem*> selectedJobItems() const;
    void selectNewJob(JobItem* job);

signals:
    void selectedJobsChanged(const QVector<JobItem*>& jobs);

private slots:
    void onItemSelectionChanged();
    void onJobsDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight);
    void onRun();
    void onCancel(const QModelIndexList indexes);
    void onCopy();
    void onRemove();
    void showContextMenu(const QPoint& pos);

private:
    void updateActions();
    void restoreSelection();
    void selectAlternativeItem(int lastSelectedRow = -1);
    void selectAndSetCurrent(const QModelIndex& index);

    QListView* m_list_view;
    JobProgressDelegate* m_progress_delegate;
    JobsQModel* m_model;
    QAction* m_run_action;
    QAction* m_cancel_action;
    QAction* m_cp_action;
    QAction* m_remove_action;
};

#endif // BORNAGAIN_GUI_VIEW_JOB_JOBSLISTING_H
