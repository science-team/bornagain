//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/JobsPanel.h
//! @brief     Defines class JobsPanel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_JOB_JOBSPANEL_H
#define BORNAGAIN_GUI_VIEW_JOB_JOBSPANEL_H

#include <QSplitter>
#include <QTreeView>

class JobItem;
class JobMessagesDisplay;
class JobparQModel;
class JobsListing;

//! A panel with a JobsListing on top and a JobMessagesDisplay an bottom.

class JobsPanel : public QWidget {
    Q_OBJECT
public:
    explicit JobsPanel(QWidget* parent = nullptr);
    ~JobsPanel();

    void resizeEvent(QResizeEvent* event) override;

    void makeNewJobItemSelected(JobItem*);

    QVector<JobItem*> selectedJobItems() const;

signals:
    void selectedJobsChanged();

private slots:
    void onSelectedJobsChanged(const QVector<JobItem*>& jobs);

private:
    void applySettings();
    void saveSettings();

    QSplitter* m_splitter;

    JobsListing* m_listing;
    QTreeView* m_properties_view;
    JobparQModel* m_properties_model;
    JobMessagesDisplay* m_messages_display;
};

#endif // BORNAGAIN_GUI_VIEW_JOB_JOBSPANEL_H
