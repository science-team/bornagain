//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/JobsQModel.cpp
//! @brief     Implements class JobsQModel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Job/JobsQModel.h"
#include "GUI/Model/Job/BatchInfo.h"
#include "GUI/Model/Job/JobItem.h"
#include "GUI/Model/Job/JobsSet.h"
#include "GUI/Model/Project/ProjectDocument.h"

//--------------------------------------------------------------------------------------------------
// public member functions
//--------------------------------------------------------------------------------------------------

JobsQModel::JobsQModel(QObject* parent)
    : QAbstractListModel(parent)
{
    connect(gDoc->jobsRW(), &JobsSet::jobAdded, this, &JobsQModel::onJobAdded);
}

JobsQModel::~JobsQModel() = default;

int JobsQModel::rowCount(const QModelIndex&) const
{
    return gDoc->jobs()->size();
}

QVariant JobsQModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid() || (size_t)index.row() >= gDoc->jobs()->size())
        return {};

    const JobItem* item = gDoc->jobs()->at(index.row());
    if (role == Qt::DisplayRole)
        return item->batchInfo()->jobName();

    return {};
}

JobItem* JobsQModel::jobItemForIndex(const QModelIndex& index) const
{
    if (index.row() >= 0 && index.row() < (int)gDoc->jobs()->size())
        return gDoc->jobsRW()->at(index.row());
    return nullptr;
}

QModelIndex JobsQModel::indexForJob(JobItem* job)
{
    int idx = gDoc->jobs()->index_of(job);
    if (idx != -1)
        return index(idx, 0);
    return {};
}

void JobsQModel::removeJob(const QModelIndex& index)
{
    beginRemoveRows(QModelIndex(), index.row(), index.row());
    JobItem* job = jobItemForIndex(index);
    job->batchInfo()->disconnect();
    gDoc->jobsRW()->removeJob(job);
    endRemoveRows();
}

void JobsQModel::cancelJob(const QModelIndex& index)
{
    gDoc->jobsRW()->cancelJob(jobItemForIndex(index));
}

//--------------------------------------------------------------------------------------------------
// private slots
//--------------------------------------------------------------------------------------------------

void JobsQModel::emitJobsQModelChanged(JobItem* job)
{
    const int i = gDoc->jobs()->index_of(job);
    if (i != -1) {
        const QModelIndex idx = index(i, 0);
        emit dataChanged(idx, idx);
    }
    gDoc->setModified();
}

void JobsQModel::onJobAdded(JobItem* job)
{
    connect(job->batchInfo(), &BatchInfo::jobStatusChanged,
            [this, job] { emitJobsQModelChanged(job); });
    connect(job->batchInfo(), &BatchInfo::jobNameChanged,
            [this, job] { emitJobsQModelChanged(job); });
    connect(job->batchInfo(), &BatchInfo::jobProgressChanged,
            [this, job] { emitJobsQModelChanged(job); });
    emitJobsQModelChanged(job);
}
