//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/JobsQModel.h
//! @brief     Defines class JobsQModel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_JOB_JOBSQMODEL_H
#define BORNAGAIN_GUI_VIEW_JOB_JOBSQMODEL_H

#include <QAbstractListModel>

class JobItem;

class JobsQModel : public QAbstractListModel {
    Q_OBJECT
public:
    JobsQModel(QObject* parent = nullptr);
    ~JobsQModel() override;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    JobItem* jobItemForIndex(const QModelIndex& index) const;
    QModelIndex indexForJob(JobItem* job);

    void removeJob(const QModelIndex& index);
    void cancelJob(const QModelIndex& index);

private slots:
    void emitJobsQModelChanged(JobItem* job);
    void onJobAdded(JobItem* job);
};

#endif // BORNAGAIN_GUI_VIEW_JOB_JOBSQMODEL_H
