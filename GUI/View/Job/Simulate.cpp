//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/Simulate.cpp
//! @brief     Implements function GUI::Sim::simulate.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Job/Simulate.h"
#include "GUI/Model/Job/JobItem.h"
#include "GUI/Model/Job/JobsSet.h"
#include "GUI/View/Info/MessageBox.h"

bool GUI::Sim::simulate(JobItem* job_item, JobsSet* jobs)
{
    try {
        jobs->runJob(job_item);
    } catch (const std::exception& ex) {
        GUI::Message::warning("BornAgain: job failed", ex.what());
        return false;
    }
    return true;
}
