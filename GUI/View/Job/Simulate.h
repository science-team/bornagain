//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Job/Simulate.h
//! @brief     Defines function GUI::Sim::simulate.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_JOB_SIMULATE_H
#define BORNAGAIN_GUI_VIEW_JOB_SIMULATE_H

class JobItem;
class JobsSet;

namespace GUI::Sim {

//! Runs job, pops up a warning box upon error, and returns true if no error occured.
bool simulate(JobItem*, JobsSet*);

} // namespace GUI::Sim

#endif // BORNAGAIN_GUI_VIEW_JOB_SIMULATE_H
