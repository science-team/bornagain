//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Main/AboutDialog.cpp
//! @brief     Implements class AboutDialog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Main/AboutDialog.h"
#include "GUI/Model/Util/Path.h"
#include "GUI/View/Base/CustomEventFilters.h"
#include "GUI/View/Base/Fontsize.h"
#include <QDate>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

namespace {

QLabel* createLinkLabel(const QString& text)
{
    auto* result = new QLabel;
    result->setTextFormat(Qt::RichText);
    result->setTextInteractionFlags(Qt::TextBrowserInteraction);
    result->setText(text);
    result->setOpenExternalLinks(true);
    return result;
}

QLabel* createLogoLabel()
{
    QPixmap logo(":/images/about_icon.awk", "JPG");
    auto* result = new QLabel;
    result->setPixmap(logo.scaled(656, 674, Qt::KeepAspectRatio));
    return result;
}

QBoxLayout* createLogoLayout()
{
    auto* result = new QVBoxLayout;
    result->setContentsMargins(5, 5, 5, 5);

    QPixmap logo(":/images/about_icon.png");
    auto* label = new QLabel;
    label->setPixmap(logo.scaled(120, 120, Qt::KeepAspectRatio));

    result->addWidget(label);
    result->addStretch(1);

    return result;
}

QBoxLayout* createTextLayout()
{
    auto* result = new QVBoxLayout;

    QFont titleFont;
    titleFont.setPointSize(GUI::Style::fontSizeSmall() + 2);
    titleFont.setBold(true);

    QFont normalFont;
    normalFont.setPointSize(GUI::Style::fontSizeSmall() + 2);
    normalFont.setBold(false);

    // title
    auto* aboutTitleLabel =
        new QLabel("BornAgain version " + GUI::Path::getBornAgainVersionString());
    aboutTitleLabel->setFont(titleFont);

    // description
    QString description = "Open-source research software to simulate and fit neutron and x-ray"
                          " reflectometry and grazing-incidence small-angle scattering.";
    auto* descriptionLabel = new QLabel(description);
    descriptionLabel->setFont(normalFont);
    descriptionLabel->setWordWrap(true);

    // copyright
    auto* copyrightLabel = new QLabel("Copyright: Forschungszentrum Jülich GmbH 2012-"
                                      + QDate::currentDate().toString("yyyy") + ".");
    copyrightLabel->setFont(normalFont);

    // home page
    auto* homepageLabel = createLinkLabel(
        "Homepage: "
        " <a href=\"https://www.bornagainproject.org\">www.bornagainproject.org</a>.");
    homepageLabel->setFont(normalFont);

    // mailing list
    auto* mailingLabel = createLinkLabel(
        "For updates, subscribe to <a "
        "href = \"https://lists.fz-juelich.de/mailman/listinfo/BornAgain-announcements\">"
        "bornagain-announcements</a>.");
    mailingLabel->setFont(normalFont);
    mailingLabel->setWordWrap(true);

    result->addWidget(aboutTitleLabel);
    result->addStretch(1);
    result->addWidget(descriptionLabel);
    result->addStretch(1);
    result->addWidget(copyrightLabel);
    result->addStretch(1);
    result->addWidget(homepageLabel);
    result->addStretch(1);
    result->addWidget(mailingLabel);
    result->addStretch(1);

    return result;
}

QBoxLayout* createButtonLayout(AboutDialog* parent)
{
    auto* result = new QHBoxLayout;

    auto* closeButton = new QPushButton("Close");
    QObject::connect(closeButton, &QPushButton::clicked, parent, &QDialog::reject);

    result->addStretch(1);
    result->addWidget(closeButton);

    return result;
}

} // namespace

AboutDialog::AboutDialog(QWidget* parent)
    : QDialog(parent)
{
    QColor bgColor(240, 240, 240, 255);
    QPalette palette;
    palette.setColor(QPalette::Window, bgColor);
    setAutoFillBackground(true);
    setPalette(palette);

    setWindowTitle("About BornAgain");
    setWindowFlags(Qt::Dialog);

    auto* detailsLayout = new QHBoxLayout;
    detailsLayout->addLayout(createLogoLayout());
    detailsLayout->addLayout(createTextLayout());

    auto* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(detailsLayout);
    mainLayout->addLayout(createButtonLayout(this));

    setLayout(mainLayout);

    static const char mydata[] = {0x64, 0x65, 0x76, 0x73};
    QByteArray b = QByteArray::fromRawData(mydata, sizeof(mydata));
    auto* f = new ShortcodeFilter(b, this);
    connect(f, &ShortcodeFilter::found, [this] { layout()->addWidget(createLogoLabel()); });
    installEventFilter(f);
}
