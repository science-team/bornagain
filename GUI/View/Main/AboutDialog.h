//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Main/AboutDialog.h
//! @brief     Defines class AboutDialog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_MAIN_ABOUTDIALOG_H
#define BORNAGAIN_GUI_VIEW_MAIN_ABOUTDIALOG_H

#include <QDialog>

//! About BornAgain dialog.

class AboutDialog : public QDialog {
    Q_OBJECT
public:
    AboutDialog(QWidget* parent = nullptr);
};

#endif // BORNAGAIN_GUI_VIEW_MAIN_ABOUTDIALOG_H
