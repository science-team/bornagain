//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Main/ActionManager.h
//! @brief     Defines class ActionManager.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_MAIN_ACTIONMANAGER_H
#define BORNAGAIN_GUI_VIEW_MAIN_ACTIONMANAGER_H

#include <QAction>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QObject>
#include <QShortcut>

class ProjectManager;

//! Class to handle MainWindow's menu and corresponding actions

class ActionManager : public QObject {
    Q_OBJECT
public:
    ActionManager(QMainWindow* parent, ProjectManager* pm);

    void updateActionEnabling();

public slots:
    void onCurrentViewChanged();

private slots:
    void onAboutToShowFileMenu();
    void onAboutToShowViewMenu();
    void onAboutApplication();

private:
    QMainWindow* m_main_window;
    ProjectManager* m_project_manager;

    QAction* m_new_action = nullptr;
    QAction* m_open_action = nullptr;
    QAction* m_save_action = nullptr;
    QAction* m_save_as_action = nullptr;
    QAction* m_exit_action = nullptr;
    QAction* m_webdoc_action = nullptr;
    QAction* m_about_action = nullptr;

    QMenuBar* m_menu_bar = nullptr;
    QMenu* m_file_menu = nullptr;
    QMenu* m_settings_menu = nullptr;
    QMenu* m_view_menu = nullptr;
    QMenu* m_recent_projects_menu = nullptr;
    QMenu* m_help_menu = nullptr;

    QShortcut* m_simulate_shortcut = nullptr;

    void createActions();
    void createMenus();
};

#endif // BORNAGAIN_GUI_VIEW_MAIN_ACTIONMANAGER_H
