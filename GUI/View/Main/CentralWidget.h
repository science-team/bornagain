//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Main/CentralWidget.h
//! @brief     Defines class CentralWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_MAIN_CENTRALWIDGET_H
#define BORNAGAIN_GUI_VIEW_MAIN_CENTRALWIDGET_H

#include <QButtonGroup>
#include <QProgressBar>
#include <QStackedLayout>
#include <QToolButton>
#include <QVBoxLayout>

class DataView;
class InstrumentView;
class JobView;
class ProjectsView;
class SampleView;
class SimulationView;

class CentralWidget : public QWidget {
    Q_OBJECT
public:
    CentralWidget();
    ~CentralWidget() override;

    QWidget* currentView() const;

    void updateViews();

    void raiseView(int viewId);
    void restoreView(int viewId);

signals:
    void currentViewChanged();

private:
    enum ViewId { Datafile, Instrument, Sample, Simulation, Job };
    bool checkViewId(int viewId) const;

    void addButton(ViewId id, const QIcon& icon, const QString& title, const QString& tooltip);
    QToolButton* createViewSelectionButton() const;

    //! Recalculate the size of the view selection buttons to show complete button text
    void updateViewSelectionButtonsGeometry() const;

    QProgressBar* m_progress_bar;
    QButtonGroup* m_view_selection_buttons;
    QStackedLayout* m_views_stack;
    QVBoxLayout* m_view_selection_buttons_layout;

    ProjectsView* m_projects_view;
    InstrumentView* m_instrument_view;
    SampleView* m_sample_view;
    DataView* m_data_view;
    SimulationView* m_simulation_view;
    JobView* m_job_view;
};

#endif // BORNAGAIN_GUI_VIEW_MAIN_CENTRALWIDGET_H
