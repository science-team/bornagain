//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Main/MainWindow.h
//! @brief     Defines class MainWindow.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_MAIN_MAINWINDOW_H
#define BORNAGAIN_GUI_VIEW_MAIN_MAINWINDOW_H

#include <QCloseEvent>
#include <QMainWindow>

class ActionManager;
class CentralWidget;
class ProjectManager;

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow() override;

    void updateTitle();
    void loadProject(QString projectPath);

private:
    void closeEvent(QCloseEvent* event) override;
    void initApplication();

    void loadSettings();
    void saveSettings();

    //! Recalculate the size of the view selection buttons to show complete button text
    void onDocumentOpened();
    void onDocumentModified();

    ProjectManager* m_project_manager;
    ActionManager* m_action_manager;
    CentralWidget* m_central_widget;
};

#endif // BORNAGAIN_GUI_VIEW_MAIN_MAINWINDOW_H
