//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Main/ProjectManager.h
//! @brief     Defines class ProjectManager.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_MAIN_PROJECTMANAGER_H
#define BORNAGAIN_GUI_VIEW_MAIN_PROJECTMANAGER_H

#include <QObject>
#include <QStringList>
#include <memory>

class AutosaveController;

//! Handles activity related to opening/save projects.

class ProjectManager : public QObject {
    Q_OBJECT
public:
    ProjectManager(QObject* parent);
    ~ProjectManager() override;

    void loadSettings();
    void saveSettings();

    bool saveOnQuit();

    QStringList recentProjects();

public slots:
    void clearRecentProjects();
    void newProject();
    bool saveProject(QString projectPullPath = "");
    bool saveProjectAs();
    void openProject(QString projectPullPath = "");

private:
    void createNewProject();
    void loadProject(const QString& fullPathAndName);

    QString acquireProjectPullPath();
    void addToRecentProjects();

    QString untitledProjectName();

    bool restoreProjectDialog(const QString& projectFileName, QString autosaveName);

    QString m_working_directory;
    QStringList m_recent_projects;
    std::unique_ptr<AutosaveController> m_autosaver;
};

#endif // BORNAGAIN_GUI_VIEW_MAIN_PROJECTMANAGER_H
