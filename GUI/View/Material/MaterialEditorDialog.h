//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Material/MaterialEditorDialog.h
//! @brief     Defines class MaterialEditorDialog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_MATERIAL_MATERIALEDITORDIALOG_H
#define BORNAGAIN_GUI_VIEW_MATERIAL_MATERIALEDITORDIALOG_H

#include <QString>

class SampleItem;

namespace GUI {

//! Use this to choose a material. identifierOfPreviousMaterial is the material which should be
//! selected when opening the dialog. Returns the identifier of the newly selected material.
//! Returns an empty string, if the dialog is cancelled.
QString chooseMaterial(SampleItem* sample, const QString& identifierOfPreviousMaterial);

} // namespace GUI

#endif // BORNAGAIN_GUI_VIEW_MATERIAL_MATERIALEDITORDIALOG_H
