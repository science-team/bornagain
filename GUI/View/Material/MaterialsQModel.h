//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Material/MaterialsQModel.h
//! @brief     Defines class MaterialsQModel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_MATERIAL_MATERIALSQMODEL_H
#define BORNAGAIN_GUI_VIEW_MATERIAL_MATERIALSQMODEL_H

#include <QAbstractItemModel>

class MaterialItem;
class MaterialsSet;

//! Model for list of materials, used in MaterialEditorDialog.
//!
//! This model is also used for changing values of a material, therefore the list can be
//! updated accordingly.
class MaterialsQModel : public QAbstractTableModel {
    Q_OBJECT
public:
    MaterialsQModel(MaterialsSet& model);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    void setMaterialItemName(const QModelIndex& index, const QString& name);
    void setColor(const QModelIndex& index, const QColor& color);
    void setX(const QModelIndex& index, double value);
    void setY(const QModelIndex& index, double value);
    void setZ(const QModelIndex& index, double value);
    void setDelta(const QModelIndex& index, double value);
    void setBeta(const QModelIndex& index, double value);
    void setRe(const QModelIndex& index, double value);
    void setIm(const QModelIndex& index, double value);

    MaterialItem* materialItemFromIndex(const QModelIndex& index) const;
    QModelIndex indexFromMaterial(const MaterialItem* m) const;
    QModelIndex indexFromMaterial(const QString& identifier) const;

    QModelIndex first() const;

    MaterialItem* addRefractiveMaterialItem(const QString& name, double delta, double beta);
    MaterialItem* addSLDMaterialItem(const QString& name, double sld, double abs_term);
    void removeMaterial(const QModelIndex& index);

private:
    //! The columns in the header. PARAMETERS contains delta/beta or Re/Im.
    enum Column { NAME, TYPE, PARAMETERS, MAGNETIZATION, NUM_COLUMNS };

    MaterialsSet& m_model;
};


#endif // BORNAGAIN_GUI_VIEW_MATERIAL_MATERIALSQMODEL_H
