//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Modelview/SetView.cpp
//! @brief     Implements class SetView.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Modelview/SetView.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Type/SetWithModel.h"
#include "GUI/View/Widget/ListItemDelegate.h"

SetView::SetView(AbstractSetModel* set, int minimum_width, QSizePolicy policy)
{
    setMinimumWidth(minimum_width);
    setSizePolicy(policy);
    setSelectionMode(QAbstractItemView::NoSelection);
    setItemDelegate(new ListItemDelegate(this));
    setCurrentIndex({}); // make "current" invalid, hence invisible
    setSet(set);
}

void SetView::setSet(AbstractSetModel* set)
{
    disconnect(this, &QListView::clicked, nullptr, nullptr);
    if (set) {
        QListView::setModel(set->model());
        connect(this, &QListView::clicked,
                [set](const QModelIndex& qi) { set->setCurrentIndex(qi.row()); });
        connect(set, &AbstractSetModel::indexSet, [this, set]() {
            QModelIndex idx = model()->index(set->currentIndex(), 0, QModelIndex());
            setCurrentIndex(idx); // keep "current" the same as selected
        });
    } else {
        // It seems to be a stable though undocumented feature of QAbstractItemView::setModel
        // that the nullptr argument is supported: https://stackoverflow.com/a/46630417/1017348.
        QListView::setModel(nullptr);
    }
}

void SetView::setModel(QAbstractItemModel*)
{
    ASSERT_NEVER; // always use setSet
}
