//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Modelview/SetView.h
//! @brief     Defines class SetView.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_MODELVIEW_SETVIEW_H
#define BORNAGAIN_GUI_VIEW_MODELVIEW_SETVIEW_H

#include <QListView>

class AbstractSetModel;

//! Instrument selector on the left side of InstrumentView.

class SetView : public QListView {
public:
    SetView(AbstractSetModel* set, int minimum_width = 200,
            QSizePolicy policy = {QSizePolicy::Preferred, QSizePolicy::Expanding});
    void setSet(AbstractSetModel* set);
    void setModel(QAbstractItemModel*) override;
};

#endif // BORNAGAIN_GUI_VIEW_MODELVIEW_SETVIEW_H
