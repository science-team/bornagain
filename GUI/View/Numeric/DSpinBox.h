//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Numeric/DSpinBox.h
//! @brief     Defines class DSpinBox.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_NUMERIC_DSPINBOX_H
#define BORNAGAIN_GUI_VIEW_NUMERIC_DSPINBOX_H

#include <QAbstractSpinBox>

class DoubleProperty;

//! SpinBox for DoubleProperties, supporting units.
class DSpinBox : public QAbstractSpinBox {
    Q_OBJECT
public:
    //! Create a DSpinBox with the information found in a DoubleProperty.
    //!
    //! The spin box will be fully initialized (tooltip, limits, unit, current value, size
    //! policy). Furthermore, the spin box will prohibit accidental changes by the mouse wheel.
    //! Otherwise it would be dangerous if the spin box is on a scrollable form - unintended and
    //! unnoticed changes would take place when just scrolling through the form.
    explicit DSpinBox(DoubleProperty* d);

    void replaceProperty(DoubleProperty* d);

    //! Update the shown value to the one contained in the value descriptor.
    //!
    //! No signal will be emitted if the new value has changed.
    void updateValue();

signals:
    //! Emitted whenever the value changes.
    void valueChanged(double value);

private:
    double fromDisplay() const;
    QAbstractSpinBox::StepEnabled stepEnabled() const override;
    void setValue(double val);
    void wheelEvent(QWheelEvent* event) override;
    void stepBy(int steps) override;

    DoubleProperty* m_property = nullptr;
};

#endif // BORNAGAIN_GUI_VIEW_NUMERIC_DSPINBOX_H
