//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Numeric/NumWidgetUtil.cpp
//! @brief     Implements widget functions in namespace GUI::Util.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Numeric/NumWidgetUtil.h"
#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "GUI/View/Base/CustomEventFilters.h"
#include "GUI/View/Numeric/DSpinBox.h"
#include <QLabel>
#include <QLineEdit>

DSpinBox* GUI::Util::addDoubleSpinBoxRow(QFormLayout* parentLayout, DoubleProperty& d)
{
    auto* result = new DSpinBox(&d);
    parentLayout->addRow(d.label() + ":", result);
    return result;
}

QCheckBox* GUI::Util::createCheckBox(const QString& title, std::function<bool()> getter,
                                     std::function<void(bool)> setter, const QString& tooltip)
{
    auto* result = new QCheckBox(title);
    result->setChecked(getter());
    result->setToolTip(tooltip);
    QObject::connect(result, &QCheckBox::stateChanged, [=] { setter(result->isChecked()); });
    return result;
}

QSpinBox* GUI::Util::createIntSpinBox(std::function<int()> getter, std::function<void(int)> slot,
                                      const RealLimits& limits, QString tooltip,
                                      QVector<std::function<void()>>* updaters, bool easyScrollable)
{
    auto* result = new QSpinBox;
    result->setFocusPolicy(Qt::StrongFocus);

    result->setMinimum(limits.hasLowerLimit() ? limits.min() : -std::numeric_limits<int>::max());
    result->setMaximum(limits.hasUpperLimit() ? limits.max() : std::numeric_limits<int>::max());
    result->setToolTip(tooltip);
    result->setValue(getter());

    if (!easyScrollable)
        WheelEventEater::install(result);

    QObject::connect(result, &QSpinBox::valueChanged, [=] { slot(result->value()); });

    if (updaters)
        (*updaters) << [=] {
            QSignalBlocker b(result);
            result->setValue(getter());
        };

    return result;
}
