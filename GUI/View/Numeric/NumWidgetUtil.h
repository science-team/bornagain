//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Numeric/NumWidgetUtil.h
//! @brief     Declares widget functions in namespace GUI::Util.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_NUMERIC_NUMWIDGETUTIL_H
#define BORNAGAIN_GUI_VIEW_NUMERIC_NUMWIDGETUTIL_H

#include "Fit/Param/RealLimits.h"
#include <QCheckBox>
#include <QFormLayout>
#include <QSpinBox>
#include <QString>

class DSpinBox;
class DoubleProperty;
class RealLimits;

namespace GUI::Util {

//! Create a label and a spin box with the information found in a DoubleProperty and place them in
//! a row in a form layout.
//!
//! The spin box will be fully initialized (tooltip, limits, current value, unit, size policy).
//! Furthermore, the spin box will prohibit accidental changes by the mouse wheel. Otherwise it
//! would be dangerous if the spin box is on a scrollable form - unintended and unnoticed changes
//! would take place when just scrolling through the form.
//!
//! No connections to update the property will be established! Therefore changes in the spin box
//! will *not* be notified to the property.
DSpinBox* addDoubleSpinBoxRow(QFormLayout* parentLayout, DoubleProperty& d);

//! Creates an updatable checkbox
QCheckBox* createCheckBox(const QString& title, std::function<bool()> getter,
                          std::function<void(bool)> setter, const QString& tooltip = "");

//! Creates configurable spinBox
//!
//! The spin box will be initialized by tooltip, limits, current value. Getter and setter are passed
//! as functional pointers.
//!
//! Furthermore, the spin box will prohibit accidental changes by the mouse wheel if
//! "easyScrollable" flag is false. Otherwise it would be dangerous if the spin box is on a
//! scrollable form - unintended and unnoticed changes would take place when just scrolling through
//! the form.
QSpinBox* createIntSpinBox(std::function<int()> getter, std::function<void(int)> slot,
                           const RealLimits& limits = {}, QString tooltip = "",
                           QVector<std::function<void()>>* updaters = nullptr,
                           bool easyScrollable = false);
} // namespace GUI::Util

#endif // BORNAGAIN_GUI_VIEW_NUMERIC_NUMWIDGETUTIL_H
