//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/EllipseOverlay.h
//! @brief     Defines class EllipseOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_ELLIPSEOVERLAY_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_ELLIPSEOVERLAY_H

#include "GUI/View/Overlay/IRectangularOverlay.h"

class EllipseItem;

//! This is a View of ellipse mask (represented by EllipseItem) on GraphicsScene.
//! Given view follows standard QGraphicsScene notations: (x,y) is top left corner.

class EllipseOverlay : public IRectangularOverlay {
    Q_OBJECT
public:
    explicit EllipseOverlay(EllipseItem* item, ColorMap* plot);

    OverlayItem* parameterizedItem() const override;

public slots:
    void resizeX(double xl, double yl) override;
    void resizeY(double yl, double yh) override;

private slots:
    void onChangedX() override;
    void onChangedY() override;
    void onPropertyChange() override;

private:
    QPainterPath shape() const override;

    void updatePosition() override;
    QRectF maskRectangle() override;

    qreal width() const override;
    qreal height() const override;

    EllipseItem* m_item;
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_ELLIPSEOVERLAY_H
