//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/FullframeOverlay.cpp
//! @brief     Implements class FullframeOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Overlay/FullframeOverlay.h"
#include "GUI/Model/Mask/MaskItems.h"
#include "GUI/View/Overlay/Viewport.h"
#include "GUI/View/Plotter/ColorMap.h"
#include <QBrush>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

FullframeOverlay::FullframeOverlay(FullframeItem* item, ColorMap* plot)
    : IOverlay(plot)
    , m_item(item)
{
    setFlag(QGraphicsItem::ItemIsSelectable);
}

OverlayItem* FullframeOverlay::parameterizedItem() const
{
    return m_item;
}

void FullframeOverlay::update_view()
{
    //    prepareGeometryChange();
    m_bounding_rect = GUI::Util::viewportRectangle(m_plot);
    update();
}

void FullframeOverlay::paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    //    painter->setRenderHints(QPainter::Antialiasing);
    QColor color(250, 250, 240, 150);
    painter->setBrush(color);
    painter->drawRect(m_bounding_rect);

    if (isSelected()) {
        QPen pen;
        pen.setStyle(Qt::DashLine);
        painter->setPen(pen);
        painter->setBrush(Qt::NoBrush);
        painter->drawRect(m_bounding_rect);
    }
}
