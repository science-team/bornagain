//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/FullframeOverlay.h
//! @brief     Defines class FullframeOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_FULLFRAMEOVERLAY_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_FULLFRAMEOVERLAY_H

#include "GUI/View/Overlay/IOverlay.h"

class FullframeItem;

//! This is a view of FullframeItem which covers whole detector plane with mask value=true.

class FullframeOverlay : public IOverlay {
    Q_OBJECT
public:
    explicit FullframeOverlay(FullframeItem* item, ColorMap* plot);

    OverlayItem* parameterizedItem() const override;

private slots:
    void update_view() override;

private:
    void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*) override;

    FullframeItem* m_item;
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_FULLFRAMEOVERLAY_H
