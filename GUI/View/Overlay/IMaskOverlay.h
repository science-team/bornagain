//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/IMaskOverlay.h
//! @brief     Defines interfaces IMaskOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_IMASKOVERLAY_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_IMASKOVERLAY_H

#include "GUI/View/Overlay/IOverlay.h"
#include <QPainter>

class IMaskOverlay : public IOverlay {
    Q_OBJECT
public:
    explicit IMaskOverlay(ColorMap* plot);

public slots:
    void onVisibilityChange();

protected:
    void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*) override;

private:
    //! Returns the shape with all masking already applied.
    QPainterPath maskedShape() const;
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_IMASKOVERLAY_H
