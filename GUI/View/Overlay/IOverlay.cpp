//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/IOverlay.cpp
//! @brief     Implements interfaces IOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Overlay/IOverlay.h"
#include "GUI/Model/Mask/MaskItems.h"
#include "GUI/View/Plotter/ColorMap.h"

IOverlay::IOverlay(ColorMap* plot)
    : m_plot(plot)
    , m_block_on_property_change(false)
{
    connect(this, &QGraphicsObject::xChanged, this, &IOverlay::onChangedX);
    connect(this, &QGraphicsObject::yChanged, this, &IOverlay::onChangedY);
}

void IOverlay::setBlockOnProperty(bool value)
{
    m_block_on_property_change = value;
}

void IOverlay::onGeometryChange()
{
    if (m_block_on_property_change)
        return;

    m_block_on_property_change = true;
    onPropertyChange();
    m_block_on_property_change = false;
}

double IOverlay::x2pix(double val) const
{
    return m_plot->xAxis->coordToPixel(val);
}

double IOverlay::y2pix(double val) const
{
    return m_plot->yAxis->coordToPixel(val);
}

double IOverlay::x2coo(double val) const
{
    return m_plot->xAxis->pixelToCoord(val);
}

double IOverlay::y2coo(double val) const
{
    return m_plot->yAxis->pixelToCoord(val);
}
