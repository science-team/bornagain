//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/IOverlay.h
//! @brief     Defines interfaces IOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_IOVERLAY_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_IOVERLAY_H

#include <QGraphicsObject>

class ColorMap;
class OverlayItem;

//! Main interface class for views representing MaskItems, Projections on graphics scene.

class IOverlay : public QGraphicsObject {
    Q_OBJECT
public:
    explicit IOverlay(ColorMap* plot);

    QRectF boundingRect() const override { return m_bounding_rect; }

    virtual OverlayItem* parameterizedItem() const = 0;

public slots:
    void onGeometryChange();
    virtual void update_view() = 0; //!< update visual appearance of view (triggered by event)

protected slots:
    virtual void onChangedX() {}
    virtual void onChangedY() {}
    virtual void onPropertyChange() {}

protected:
    void setBlockOnProperty(bool value);

    double x2pix(double val) const;
    double y2pix(double val) const;
    double x2coo(double val) const;
    double y2coo(double val) const;

    ColorMap* m_plot;
    QRectF m_bounding_rect;
    bool m_block_on_property_change;
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_IOVERLAY_H
