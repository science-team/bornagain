//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/IRectangularOverlay.h
//! @brief     Defines class IRectangularOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_IRECTANGULAROVERLAY_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_IRECTANGULAROVERLAY_H

#include "GUI/View/Overlay/IMaskOverlay.h"
#include "GUI/View/Overlay/SizeHandle.h"
#include <array>
#include <memory>

//! Base view for all rectangular-like masks.

class IRectangularOverlay : public IMaskOverlay {
    Q_OBJECT
public:
    explicit IRectangularOverlay(ColorMap* plot);

public slots:
    virtual void resizeX(double xl, double yl) = 0;
    virtual void resizeY(double yl, double yh) = 0;

protected:
    void update_view() override;

    virtual void updatePosition() = 0;
    virtual QRectF maskRectangle() = 0;

    //... Item properties to scene coordinates
    virtual qreal width() const = 0;
    virtual qreal height() const = 0;

    QRectF m_mask_rect; //!< mask rectangle in scene coordinates
    //!< coordinates of corner opposite to the grip corner at the moment it first clicked
    //!< in scene coordinates

    SizeHandle* m_active_handle;

    virtual bool is_true_mask() const { return true; } // overridden in ROIOverlay

private slots:
    void setToBeResized(bool on);

private:
    QVariant itemChange(GraphicsItemChange change, const QVariant& value) override;
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
    void update_bounding_rect();
    void enactResize(int i, QPointF pos);

    std::array<std::unique_ptr<SizeHandle>, 8> m_resize_handles;
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_IRECTANGULAROVERLAY_H
