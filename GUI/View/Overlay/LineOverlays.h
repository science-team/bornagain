//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/LineOverlays.h
//! @brief     Defines classes VerticalLineOverlay and HorizontalLineOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_LINEOVERLAYS_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_LINEOVERLAYS_H

#include "GUI/View/Overlay/IMaskOverlay.h"

class LineItem;

class LineOverlay : public IMaskOverlay {
protected:
    explicit LineOverlay(LineItem* item, ColorMap* plot, bool is_projection);

    virtual QRectF rectangle() const = 0;

    LineItem* m_item;
    bool m_is_projection;

private:
    void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*) override;
    QPainterPath shape() const override;
    OverlayItem* parameterizedItem() const override;
};

//! This is a view of HorizontalLineItem mask

class HorizontalLineOverlay : public LineOverlay {
    Q_OBJECT
public:
    explicit HorizontalLineOverlay(LineItem* item, ColorMap* plot, bool is_projection);

private slots:
    void update_view() override;
    void onChangedY() override;
    void onPropertyChange() override;

private:
    QRectF rectangle() const override;
    QVariant itemChange(GraphicsItemChange change, const QVariant& value) override;
};

//! This is a view of VerticalLineItem mask

class VerticalLineOverlay : public LineOverlay {
    Q_OBJECT
public:
    explicit VerticalLineOverlay(LineItem* item, ColorMap* plot, bool is_projection);

private slots:
    void update_view() override;
    void onChangedX() override;
    void onPropertyChange() override;

private:
    QRectF rectangle() const override;
    QVariant itemChange(GraphicsItemChange change, const QVariant& value) override;
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_LINEOVERLAYS_H
