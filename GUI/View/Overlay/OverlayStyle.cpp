//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/OverlayStyle.cpp
//! @brief     Implements class GUI::Overlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Overlay/OverlayStyle.h"
#include <QColor>

QBrush GUI::Overlay::getSelectionMarkerBrush()
{
    QBrush result;
    result.setStyle(Qt::SolidPattern);
    result.setColor(QColor(226, 235, 244));
    return result;
}

QPen GUI::Overlay::getSelectionMarkerPen()
{
    return {QColor(99, 162, 217)};
}

QBrush GUI::Overlay::getMaskBrush(bool mask_value)
{
    if (!mask_value)
        return Qt::NoBrush;
    return {QColor(0, 0, 80)}; // deep blue
}

QPen GUI::Overlay::getMaskPen(bool mask_value)
{
    if (mask_value)
        return {QColor(165, 80, 76)}; // dark red
    return {QColor(0, 140, 70)};      // dark green
}

QBrush GUI::Overlay::getProjectionBrush()
{
    return {QColor(0, 191, 172)}; // dark cyan
}

QPen GUI::Overlay::getProjectionPen()
{
    return getProjectionBrush().color();
}
