//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/OverlayStyle.h
//! @brief     Defines class GUI::Overlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_OVERLAYSTYLE_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_OVERLAYSTYLE_H

#include <QBrush>
#include <QPen>

//! Provides MaskEditor with common settings (colors, gradients, etc)

namespace GUI::Overlay {

QBrush getSelectionMarkerBrush();
QPen getSelectionMarkerPen();
QBrush getMaskBrush(bool mask_value);
QPen getMaskPen(bool mask_value);
QBrush getProjectionBrush();
QPen getProjectionPen();

}; // namespace GUI::Overlay

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_OVERLAYSTYLE_H
