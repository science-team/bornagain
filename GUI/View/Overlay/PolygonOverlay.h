//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/PolygonOverlay.h
//! @brief     Defines class PolygonOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_POLYGONOVERLAY_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_POLYGONOVERLAY_H

#include "GUI/View/Overlay/IMaskOverlay.h"
#include <QPolygonF>

class PolygonItem;

//! This is a View of polygon mask (represented by PolygonItem) on GraphicsScene.

class PolygonOverlay : public IMaskOverlay {
    Q_OBJECT
public:
    explicit PolygonOverlay(PolygonItem* item, ColorMap* plot);

    OverlayItem* parameterizedItem() const override;

    void addOverlay(IOverlay* child);

    bool isClosedPolygon();
    QPointF lastAddedPoint() const;
    QPainterPath shape() const override;

public slots:
    bool closePolygonIfNecessary();
    void onClosePolygonRequest(bool value);

private slots:
    void update_view() override;

private:
    void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*) override;
    QVariant itemChange(GraphicsItemChange change, const QVariant& value) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;

    void update_polygon();
    void update_points();
    void setChildrenVisible(bool value);
    bool makePolygonClosed();

    PolygonItem* m_item;
    QPolygonF m_polygon;
    bool m_block_on_point_update;
    bool m_close_polygon_request;
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_POLYGONOVERLAY_H
