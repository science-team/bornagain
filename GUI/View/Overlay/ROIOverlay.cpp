//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/ROIOverlay.cpp
//! @brief     Defines class ROIOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Overlay/ROIOverlay.h"
#include "GUI/Model/Mask/MaskItems.h"
#include "GUI/View/Overlay/OverlayStyle.h"
#include "GUI/View/Overlay/Viewport.h"
#include "GUI/View/Plotter/ColorMap.h"
#include <QPainter>

ROIOverlay::ROIOverlay(RegionOfInterestItem* item, ColorMap* plot)
    : RectangleOverlay(item, plot)
{
}

QRectF ROIOverlay::boundingRect() const
{
    return mapFromScene(GUI::Util::viewportRectangle(m_plot)).boundingRect();
}

void ROIOverlay::update_view()
{
    RectangleOverlay::update_view();
    m_bounding_rect = GUI::Util::viewportRectangle(m_plot);
    update();
}

void ROIOverlay::paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    QPainterPath outerRect;
    outerRect.addRect(boundingRect());

    QPainterPath innerRect;
    innerRect.addRect(m_mask_rect);

    painter->setBrush(GUI::Overlay::getMaskBrush(true));
    painter->drawPath(outerRect.subtracted(innerRect));
}
