//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/ROIOverlay.h
//! @brief     Defines class ROIOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_ROIOVERLAY_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_ROIOVERLAY_H

#include "GUI/View/Overlay/RectangleOverlay.h"

class RegionOfInterestItem;

//! The RegionOfInterest class represent view of RegionOfInterestItem on graphics scene.

class ROIOverlay : public RectangleOverlay {
    Q_OBJECT
public:
    explicit ROIOverlay(RegionOfInterestItem* item, ColorMap* plot);

    QRectF boundingRect() const override;

private slots:
    void update_view() override;

private:
    void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*) override;
    bool is_true_mask() const override { return false; }
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_ROIOVERLAY_H
