//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/RectangleOverlay.cpp
//! @brief     Implements class RectangleOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Overlay/RectangleOverlay.h"
#include "GUI/Model/Mask/MaskItems.h"
#include "GUI/View/Plotter/ColorMap.h"

RectangleOverlay::RectangleOverlay(RectangleItem* item, ColorMap* plot)
    : IRectangularOverlay(plot)
    , m_item(item)
{
    m_item->maskVisibilityChanged();
}

OverlayItem* RectangleOverlay::parameterizedItem() const
{
    return m_item;
}

void RectangleOverlay::onChangedX()
{
    setBlockOnProperty(true);
    m_item->setXLow(x2coo(this->x()));
    m_item->setXHig(x2coo(this->x() + m_mask_rect.width()));
    setBlockOnProperty(false);
}

void RectangleOverlay::onChangedY()
{
    setBlockOnProperty(true);
    m_item->setYLow(y2coo(this->y() + m_mask_rect.height()));
    m_item->setYHig(y2coo(this->y()));
    setBlockOnProperty(false);
}

void RectangleOverlay::onPropertyChange()
{
    update_view();
}

QPainterPath RectangleOverlay::shape() const
{
    QPainterPath path;
    path.addRect(m_mask_rect);
    return path;
}

void RectangleOverlay::resizeX(double xl, double xh)
{
    m_item->setXLow(x2coo(xl));
    m_item->setXHig(x2coo(xh));
}

void RectangleOverlay::resizeY(double yl, double yh)
{
    m_item->setYLow(y2coo(yh));
    m_item->setYHig(y2coo(yl));
}

//! updates position of view using item properties
void RectangleOverlay::updatePosition()
{
    setX(x2pix(m_item->xLow().dVal()));
    setY(y2pix(m_item->yUp().dVal()));
}

QRectF RectangleOverlay::maskRectangle()
{
    return {0, 0, width(), height()};
}

qreal RectangleOverlay::width() const
{
    return x2pix(m_item->xUp().dVal()) - x2pix(m_item->xLow().dVal());
}

qreal RectangleOverlay::height() const
{
    return y2pix(m_item->yLow().dVal()) - y2pix(m_item->yUp().dVal());
}
