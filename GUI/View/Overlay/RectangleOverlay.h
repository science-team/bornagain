//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/RectangleOverlay.h
//! @brief     Defines class RectangleOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_RECTANGLEOVERLAY_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_RECTANGLEOVERLAY_H

#include "GUI/View/Overlay/IRectangularOverlay.h"

class RectangleItem;

//! This is a View of rectangular mask (represented by RectangleItem) on GraphicsScene.
//! Given view follows standard QGraphicsScene notations: (x,y) is top left corner.

class RectangleOverlay : public IRectangularOverlay {
    Q_OBJECT
public:
    explicit RectangleOverlay(RectangleItem* item, ColorMap* plot);

    OverlayItem* parameterizedItem() const override;

public slots:
    void resizeX(double xl, double xh) override;
    void resizeY(double yl, double yh) override;

private slots:
    void onChangedX() override;
    void onChangedY() override;
    void onPropertyChange() override;

private:
    QPainterPath shape() const override;

    void updatePosition() override;
    QRectF maskRectangle() override;

    qreal width() const override;
    qreal height() const override;

    RectangleItem* m_item;
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_RECTANGLEOVERLAY_H
