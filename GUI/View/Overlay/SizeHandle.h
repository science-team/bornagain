//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/SizeHandle.h
//! @brief     Defines class SizeHandle.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_SIZEHANDLE_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_SIZEHANDLE_H

#include <QGraphicsObject>

//! Size handle on top of RectangleOverlay represented as small circle or small rectangle.
//! Placed either in corners on in the middle of the edge.

class SizeHandle : public QGraphicsObject {
    Q_OBJECT
public:
    SizeHandle(int i, QGraphicsObject* parent = nullptr);

    void updateHandlePosition(const QRectF& rect);

    QRectF boundingRect() const override;

public slots:
    virtual void update_view();

signals:
    void requestResizing(bool going_to_resize);
    void requestEnactResize(int i, QPointF pos);

private:
    void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*) override;
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;

    int m_i; //!< Ordinal number. Handlers are numbered clockwise from top left.
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_SIZEHANDLE_H
