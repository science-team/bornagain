//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/VertexOverlay.h
//! @brief     Defines class VertexOverlay.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_VERTEXOVERLAY_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_VERTEXOVERLAY_H

#include "GUI/View/Overlay/IOverlay.h"

class PointItem;

//! Represents a vertex of a polygon.

class VertexOverlay : public IOverlay {
    Q_OBJECT
public:
    explicit VertexOverlay(PointItem* item, ColorMap* plot);

    QRectF boundingRect() const override;

    OverlayItem* parameterizedItem() const override;

    void updateParameterizedItem(const QPointF& pos);

signals:
    void closePolygonRequest(bool);
    void propertyChanged();

private slots:
    void update_view() override;
    void onPropertyChange() override;

private:
    void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    void hoverEnterEvent(QGraphicsSceneHoverEvent* event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent* event) override;

    PointItem* m_item;
    bool m_on_hover;
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_VERTEXOVERLAY_H
