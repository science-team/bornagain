//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/Viewport.cpp
//! @brief     Implements function GUI::Util::viewportRectangle.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Overlay/Viewport.h"
#include <qcustomplot.h>

QRectF GUI::Util::viewportRectangle(const QCustomPlot* qcp)
{
    const QCPRange& xrange = qcp->xAxis->range();
    const QCPRange& yrange = qcp->yAxis->range();
    const double left = qcp->xAxis->coordToPixel(xrange.lower);
    const double right = qcp->xAxis->coordToPixel(xrange.upper);
    const double top = qcp->yAxis->coordToPixel(yrange.upper);
    const double bottom = qcp->yAxis->coordToPixel(yrange.lower);
    return {left, top, right - left, bottom - top};
}

Viewport::Viewport(const QCustomPlot* qcp)
    : m_plot(qcp)
{
    // the key flag to not to draw children going outside of given shape
    setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
}

void Viewport::addChildGraphics(QGraphicsItem* overlay)
{
    if (!childItems().contains(overlay))
        overlay->setParentItem(this);
}

QRectF Viewport::boundingRect() const
{
    return GUI::Util::viewportRectangle(m_plot);
}
