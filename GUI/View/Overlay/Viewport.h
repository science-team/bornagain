//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Overlay/Viewport.h
//! @brief     Declares function GUI::Util::viewportRectangle.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_OVERLAY_VIEWPORT_H
#define BORNAGAIN_GUI_VIEW_OVERLAY_VIEWPORT_H

#include <QGraphicsObject>

class QCustomPlot;

namespace GUI::Util {

QRectF viewportRectangle(const QCustomPlot* qcp);

} // namespace GUI::Util

//! This is nothing more than just transparent rectangle to cover axes area
//! of color map plot. The goal of this rectangle is to hide all masks
//! if they are going outside of color map plot.
//!
//! The size of the rectangle always matches axes viewport at any zoom level.
//! All visible mask objects are added to Viewport as children.

class Viewport : public QGraphicsObject {
public:
    explicit Viewport(const QCustomPlot* qcp);
    void addChildGraphics(QGraphicsItem* overlay);
    QRectF boundingRect() const override;
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override {}

private:
    const QCustomPlot* m_plot;
};

#endif // BORNAGAIN_GUI_VIEW_OVERLAY_VIEWPORT_H
