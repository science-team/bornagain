//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/ParEdit/CustomEditors.h
//! @brief     Defines classes CustomEditor, ComboPropertyEditor, ParSpinBoxEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PAREDIT_CUSTOMEDITORS_H
#define BORNAGAIN_GUI_VIEW_PAREDIT_CUSTOMEDITORS_H

#include <QComboBox>
#include <QLabel>
#include <QVariant>
#include <QWidget>

class LostFocusFilter;
class RealLimits;

//! Base class for all custom variants editors.

class CustomEditor : public QWidget {
    Q_OBJECT
public:
    explicit CustomEditor(QWidget* parent = nullptr)
        : QWidget(parent)
    {
    }

    QVariant editorData() { return m_data; }

public slots:
    void setData(const QVariant& data);

signals:
    //! Signal emit then user changed the data through the editor
    void dataChanged(const QVariant& data);

protected:
    virtual void initEditor();
    void setDataIntern(const QVariant& data);
    QVariant m_data;
};

//! Editor for ComboProperty variant.

class ComboPropertyEditor : public CustomEditor {
    Q_OBJECT
public:
    explicit ComboPropertyEditor(QWidget* parent = nullptr);

    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;

protected slots:
    virtual void onIndexChanged(int index);

protected:
    void initEditor() override;
    virtual QStringList internLabels();
    virtual int internIndex();
    void setConnected(bool isConnected);

    QComboBox* m_box;
};

//! Editor for Double variant using ParSpinBox.

class ParSpinBoxEditor : public CustomEditor {
    Q_OBJECT
public:
    ParSpinBoxEditor(QWidget* parent = nullptr);

    void setLimits(const RealLimits& limits);
    void setDecimals(int decimals);
    void setSingleStep(double step);

private slots:
    void onEditingFinished();

protected:
    void initEditor() override;

private:
    class ParSpinBox* m_double_editor;
};

#endif // BORNAGAIN_GUI_VIEW_PAREDIT_CUSTOMEDITORS_H
