//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/ParEdit/FitParameterDelegate.h
//! @brief     Defines class FitParameterDelegate.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PAREDIT_FITPARAMETERDELEGATE_H
#define BORNAGAIN_GUI_VIEW_PAREDIT_FITPARAMETERDELEGATE_H

#include <QStyledItemDelegate>

//! The FitParameterDelegate class presents the content of items in
//! standard QTreeView. Extents base QItemDelegate with possibility to show/edit
//! our custom QVariant's.

class FitParameterDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    FitParameterDelegate(QObject* parent);

    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const override;

    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
                          const QModelIndex& index) const override;

    void setModelData(QWidget* editor, QAbstractItemModel* model,
                      const QModelIndex& index) const override;

    void setEditorData(QWidget* editor, const QModelIndex& index) const override;

    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;

    void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option,
                              const QModelIndex& index) const override;

public slots:
    void onCustomEditorDataChanged(const QVariant&);

private:
    void paintCustomLabel(QPainter* painter, const QStyleOptionViewItem& option,
                          const QModelIndex& index, const QString& text) const;
};

#endif // BORNAGAIN_GUI_VIEW_PAREDIT_FITPARAMETERDELEGATE_H
