//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/ParEdit/ParSpinBox.h
//! @brief     Defines class ParSpinBox.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PAREDIT_PARSPINBOX_H
#define BORNAGAIN_GUI_VIEW_PAREDIT_PARSPINBOX_H

#include <QAbstractSpinBox>

//! This class has similar scope as GUI/View/Numeric/DSpinBox.
//! Ideally, we shall merge both classes (#869).
//! For the time being, however, we keep ParSpinBox for exclusive use in parameter views.

class ParSpinBox : public QAbstractSpinBox {
    Q_OBJECT
    Q_PROPERTY(double value MEMBER m_value READ value WRITE setValue NOTIFY valueChanged USER true)
public:
    ParSpinBox(QWidget* parent = nullptr, bool easyScrollable = false);
    ~ParSpinBox() override;

    double value() const { return m_value; }
    void setValue(double val);

    double singleStep() const { return m_step; }
    void setSingleStep(double step);

    void setMinimum(double min);
    void setMaximum(double max);

    void setDecimals(int);

    void stepBy(int steps) override;
    QValidator::State validate(QString&, int&) const override { return QValidator::Acceptable; }
    void fixup(QString&) const override {}

    static QString toString(double val, int decimal_points);
    static double toDouble(QString text, const QDoubleValidator& validator, double min, double max,
                           double default_value);
    static double round(double val, int decimals);

signals:
    void valueChanged(double value);

private:
    QAbstractSpinBox::StepEnabled stepEnabled() const override;
    void wheelEvent(QWheelEvent* event) override;

    void updateValue();
    void updateText();
    bool inRange(double val) const;

    double m_value, m_min, m_max;
    double m_step;
    int m_decimals;
    bool m_easy_scrollable;
    QDoubleValidator m_validator;
};

#endif // BORNAGAIN_GUI_VIEW_PAREDIT_PARSPINBOX_H
