//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/ColorMap.h
//! @brief     Defines class ColorMap.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PLOTTER_COLORMAP_H
#define BORNAGAIN_GUI_VIEW_PLOTTER_COLORMAP_H

#include "GUI/View/Plotter/MousyPlot.h"

class Data2DItem;
class QCPColorMap;
class QCPColorMapData;
class QCPColorScale;
class QCPLayoutGrid;
class QCPRange;

//! Presents 2D intensity data from Data2DItem as color map.

//! Provides a minimal functionality for data plotting and axes interaction.
//! Is used as a component of more complicated plotting widgets.

class ColorMap : public MousyPlot {
    Q_OBJECT
public:
    ColorMap();

    void itemToMap(Data2DItem* item);

    QString infoString(double x, double y) const override;

signals:
    void marginsChanged(double left, double right);

private slots:
    void onDataDestroyed();

private:
    Data2DItem* data2DItem() const { return m_data_item; }
    QCPColorMapData* cmData() const;

    void onIntensityModified();
    void onUnitsChanged();
    void setGradient();
    void setInterpolation();
    void setLogZ();

    void onDataRangeChanged(QCPRange range);
    void onXaxisRangeChanged(QCPRange range);
    void onYaxisRangeChanged(QCPRange range);
    void marginsChangedNotify();

    void setAxesRangeConnected(bool isConnected);
    void setDataRangeConnected(bool isConnected);

    void setAxesRangeFromItem();
    void setAxesZoomFromItem();
    void setAxesLabelsFromItem();
    void setDataFromItem();
    void setColorScaleAppearanceFromItem();
    void setDataRangeFromItem();
    void setColorScaleVisible();

    Data2DItem* m_data_item = nullptr;
    QCPColorMap* m_qcp_map;
    QCPColorScale* m_color_scale;
    QCPLayoutGrid* m_color_bar_layout;
};

#endif // BORNAGAIN_GUI_VIEW_PLOTTER_COLORMAP_H
