//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/HistogramPlot.cpp
//! @brief     Implements class HistogramPlot.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Plotter/HistogramPlot.h"
#include "GUI/View/Base/Fontsize.h"

HistogramPlot::HistogramPlot()
{
    setAttribute(Qt::WA_NoMousePropagation, false);

    initGraph();

    const QFont font1(QFont().family(), GUI::Style::fontSizeSmall());
    xAxis->setTickLabelFont(font1);
    yAxis->setTickLabelFont(font1);

    yAxis->setScaleType(QCPAxis::stLogarithmic);
    yAxis->setNumberFormat("eb");
    yAxis->setNumberPrecision(0);

    xAxis->setLabel("iteration");
    yAxis->setLabel("chi2");

    const QFont font2(QFont().family(), GUI::Style::fontSizeRegular());
    xAxis->setLabelFont(font2);
    yAxis->setLabelFont(font2);
}

void HistogramPlot::addData(double x, double y)
{
    graph()->addData(x, y);
    graph()->rescaleAxes();
    replot();
}

void HistogramPlot::addData(const QVector<double>& x, const QVector<double>& y)
{
    graph()->addData(x, y);
    graph()->rescaleAxes();
    replot();
}

void HistogramPlot::setData(const QVector<double>& x, const QVector<double>& y)
{
    graph()->setData(x, y);
    graph()->rescaleAxes();
    replot();
}

void HistogramPlot::clearData()
{
    removeGraph(graph());
    initGraph();
    replot();
}

void HistogramPlot::initGraph()
{
    addGraph();

    QPen pen(QColor(0, 0, 255, 200));
    graph()->setLineStyle(QCPGraph::lsLine);
    graph()->setPen(pen);
    graph()->setBrush(QBrush(QColor(255 / 4, 160, 50, 150)));

    auto base_size = GUI::Style::SizeOfLetterM();
    auto* axisRectangle = axisRect();
    axisRectangle->setAutoMargins(QCP::msTop | QCP::msBottom);
    axisRectangle->setMargins(QMargins(base_size.width() * 4, base_size.height() * 2,
                                       base_size.width() * 2, base_size.height() * 2));
}
