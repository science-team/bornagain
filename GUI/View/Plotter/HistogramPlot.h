//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/HistogramPlot.h
//! @brief     Defines class HistogramPlot.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PLOTTER_HISTOGRAMPLOT_H
#define BORNAGAIN_GUI_VIEW_PLOTTER_HISTOGRAMPLOT_H

#include <qcustomplot.h>

class HistogramPlot : public QCustomPlot {
    Q_OBJECT
public:
    HistogramPlot();

public slots:
    void addData(double x, double y);
    void addData(const QVector<double>& x, const QVector<double>& y);
    void setData(const QVector<double>& x, const QVector<double>& y);
    void clearData();

private:
    void initGraph();
};

#endif // BORNAGAIN_GUI_VIEW_PLOTTER_HISTOGRAMPLOT_H
