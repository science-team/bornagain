//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/MousyPlot.h
//! @brief     Declares class MousyPlot.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PLOTTER_MOUSYPLOT_H
#define BORNAGAIN_GUI_VIEW_PLOTTER_MOUSYPLOT_H

#include <qcustomplot.h>

//! A plot widget with support for mouse tracking. Inherits QCustomPlot, which inherits QWidget.

class MousyPlot : public QCustomPlot {
    Q_OBJECT
public:
    MousyPlot();
    ~MousyPlot() override;

    QSize sizeHint() const override { return {500, 400}; }

    //! Tracks move events (used when showing profile histograms and printing status string)
    void setMouseTrackingEnabled(bool enable);

    virtual QString infoString(double x, double y) const = 0;

signals:
    void enteringPlot();
    void leavingPlot();
    void positionChanged(double x, double y);

protected:
    void replot();

private slots:
    void onCustomMouseMove(QMouseEvent* event);

private:
    bool m_was_inside = false;
};

#endif // BORNAGAIN_GUI_VIEW_PLOTTER_MOUSYPLOT_H
