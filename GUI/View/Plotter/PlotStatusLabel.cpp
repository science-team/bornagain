//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/PlotStatusLabel.cpp
//! @brief     Implements class PlotStatusLabel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Plotter/PlotStatusLabel.h"
#include "GUI/View/Base/Fontsize.h"
#include "GUI/View/Plotter/MousyPlot.h"
#include <QColor>
#include <QFont>
#include <QPainter>

namespace {

int default_text_size()
{
    return GUI::Style::fontSizeRegular();
}
int default_label_height()
{
    return GUI::Style::SizeOfLetterM().height() * 1.75;
}

} // namespace


PlotStatusLabel::PlotStatusLabel(const std::vector<MousyPlot*>& plots)
    : m_font("Monospace", default_text_size(), QFont::Normal, false)
    , m_plots(plots)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    setFixedHeight(default_label_height());

    for (auto* plot : m_plots) {
        // Enables/disables showing of label for given plot.
        // Connects with colorMap's status string signal.

        plot->setMouseTrackingEnabled(true);

        connect(plot, &MousyPlot::positionChanged,
                [this, plot](double x, double y) { setLabelText(plot, x, y); });
        connect(plot, &MousyPlot::leavingPlot, this, &PlotStatusLabel::clearLabelText);
        connect(plot, &QObject::destroyed, this, &PlotStatusLabel::onPlotDestroyed);
    }
}

void PlotStatusLabel::setLabelText(const MousyPlot* plot, double x, double y)
{
    m_text = plot->infoString(x, y);
    update();
}

void PlotStatusLabel::clearLabelText()
{
    m_text = "";
    update();
}

void PlotStatusLabel::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);

    QPainter painter(this);
    painter.setBrush(QColor(Qt::black));
    painter.setPen(QColor(Qt::black));
    painter.setFont(m_font);

    QRect textRect(0, 0, geometry().width(), geometry().height());
    painter.fillRect(textRect, QColor(Qt::white));
    painter.drawText(textRect, m_alignment, m_text);
}

void PlotStatusLabel::onPlotDestroyed(QObject* obj)
{
    if (auto* old_plot = dynamic_cast<MousyPlot*>(obj)) {
        for (auto it = m_plots.begin(); it != m_plots.end(); ++it)
            if (*it == old_plot)
                m_plots.erase(it);
    }
}
