//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/PlotStatusLabel.h
//! @brief     Defines class PlotStatusLabel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PLOTTER_PLOTSTATUSLABEL_H
#define BORNAGAIN_GUI_VIEW_PLOTTER_PLOTSTATUSLABEL_H

#include <QFrame>
#include <QPaintEvent>
#include <vector>

class MousyPlot;

//! A frame that displays information about associated plots.

class PlotStatusLabel : public QFrame {
    Q_OBJECT
public:
    PlotStatusLabel(const std::vector<MousyPlot*>& plots);

private slots:
    void onPlotDestroyed(QObject* obj);

private:
    void paintEvent(QPaintEvent* event) override;
    void setLabelText(const MousyPlot* plot, double x, double y);
    void clearLabelText();

    QString m_text;
    Qt::Alignment m_alignment;
    QFont m_font;

    std::vector<MousyPlot*> m_plots;
};

#endif // BORNAGAIN_GUI_VIEW_PLOTTER_PLOTSTATUSLABEL_H
