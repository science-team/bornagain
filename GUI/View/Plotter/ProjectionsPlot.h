//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/ProjectionsPlot.h
//! @brief     Defines class ProjectionsPlot.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PLOTTER_PROJECTIONSPLOT_H
#define BORNAGAIN_GUI_VIEW_PLOTTER_PROJECTIONSPLOT_H

#include <QMap>
#include <qcustomplot.h>

class Data2DItem;
class LineItem;
class QCPGraph;

//! A customplot based widget to display projections of Data2DItem on X,Y axes.

class ProjectionsPlot : public QCustomPlot {
    Q_OBJECT
public:
    ProjectionsPlot(Qt::Orientation orientation);

    QSize sizeHint() const override { return {500, 200}; }

    void setData2DItem(Data2DItem* item);

    void connectItems();
    void disconnectItems();

public slots:
    void onMarginsChanged(double left, double right);
    void exportPlot();

private slots:
    void onDataDestroyed();

private:
    QCPGraph* graphForItem(const LineItem* item);

    void updateProjectionsData();
    void updateProjections();
    void onProjectionPropertyChanged(const LineItem* item);
    void updateAxesRange();
    void updateAxesTitle();
    void clearProjection(const LineItem* item);
    void clearAll();

    void setGraphFromItem(const LineItem* item);

    void setInterpolate(bool isInterpolated);
    void setLogZ(bool isLogz);

    void replot();

    Data2DItem* m_data_item = nullptr;
    const Qt::Orientation m_orientation;
    QMap<const LineItem*, QCPGraph*> m_item_to_graph;
};

#endif // BORNAGAIN_GUI_VIEW_PLOTTER_PROJECTIONSPLOT_H
