//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/RangeUtil.cpp
//! @brief     Implements namespace QCP_RangeUtil.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Plotter/RangeUtil.h"
#include <qcustomplot.h>

void GUI::QCP_RangeUtil::setLogZ(QCPColorScale* scale, bool isLogz)
{
    if (isLogz && scale->dataScaleType() != QCPAxis::stLogarithmic)
        scale->setDataScaleType(QCPAxis::stLogarithmic);

    else if (!isLogz && scale->dataScaleType() != QCPAxis::stLinear)
        scale->setDataScaleType(QCPAxis::stLinear);

    setLogAmplitudeAxis(scale->axis(), isLogz);
}

void GUI::QCP_RangeUtil::setLogAmplitudeAxis(QCPAxis* axis, bool isLog)
{
    if (isLog) {
        axis->setNumberFormat("e");
        axis->setNumberPrecision(0);
        axis->setScaleType(QCPAxis::stLogarithmic);
        QSharedPointer<QCPAxisTicker> ticker(new QCPAxisTickerLog);
        axis->setTicker(ticker);
    } else {
        axis->setNumberFormat("g");
        axis->setScaleType(QCPAxis::stLinear);
        QSharedPointer<QCPAxisTicker> ticker(new QCPAxisTicker);
        axis->setTicker(ticker);
    }
}

void GUI::QCP_RangeUtil::setLogArgumentAxis(QCPAxis* axis, bool isLog)
{
    axis->setNumberFormat("g");
    if (isLog) {
        axis->setScaleType(QCPAxis::stLogarithmic);
        QSharedPointer<QCPAxisTicker> ticker(new QCPAxisTickerLog);
        axis->setTicker(ticker);
    } else {
        axis->setScaleType(QCPAxis::stLinear);
        QSharedPointer<QCPAxisTicker> ticker(new QCPAxisTicker);
        axis->setTicker(ticker);
    }
}
