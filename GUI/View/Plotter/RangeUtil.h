//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/RangeUtil.h
//! @brief     Defines namespace QCP_RangeUtil.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PLOTTER_RANGEUTIL_H
#define BORNAGAIN_GUI_VIEW_PLOTTER_RANGEUTIL_H

class QCPAxis;
class QCPColorScale;

//! Provides few helper functions for ColorMapPlot.

namespace GUI::QCP_RangeUtil {

void setLogZ(QCPColorScale* scale, bool isLogz);

void setLogAmplitudeAxis(QCPAxis* axis, bool isLog);

void setLogArgumentAxis(QCPAxis* axis, bool isLog);

} // namespace GUI::QCP_RangeUtil

#endif // BORNAGAIN_GUI_VIEW_PLOTTER_RANGEUTIL_H
