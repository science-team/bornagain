//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/SavePlot.h
//! @brief     Declares function savePlot in namespace GUI::Plot.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PLOTTER_SAVEPLOT_H
#define BORNAGAIN_GUI_VIEW_PLOTTER_SAVEPLOT_H

#include <QString>

class Datafield;
class QCustomPlot;

namespace GUI::Plot {

void savePlot(QCustomPlot* plot, const Datafield* output_data);

};

#endif // BORNAGAIN_GUI_VIEW_PLOTTER_SAVEPLOT_H
