//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Plotter/SpecularPlot.h
//! @brief     Defines class SpecularPlot.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_PLOTTER_SPECULARPLOT_H
#define BORNAGAIN_GUI_VIEW_PLOTTER_SPECULARPLOT_H

#include "GUI/View/Plotter/MousyPlot.h"
#include <QMap>

class Data1DItem;
class QCPErrorBars;
class QCPGraph;
class QCPRange;

//! The SpecularPlot class presents 1D intensity data from Data1DItem.

//! Provides minimal functionality for data plotting and axes interaction. Should be a component
//! for more complicated plotting widgets. Corresponds to ColorMap for 2D intensity data.

class SpecularPlot : public MousyPlot {
    Q_OBJECT
public:
    SpecularPlot();

    void setData1DItems(const QVector<Data1DItem*>& items);

    QString infoString(double x, double y) const override;

    Data1DItem* currentData1DItem() const;

private slots:
    void onDataDestroyed(QObject* destroyed_obj);

private:
    //! sets logarithmic scale
    void applyLogXstate();
    void applyLogYstate();

    QVector<Data1DItem*> data_items() const;

    //! Propagate xmin, xmax back to Data1DItem
    void onXaxisRangeChanged(QCPRange range) const;

    //! Propagate ymin, ymax back to Data1DItem
    void onYaxisRangeChanged(QCPRange range) const;

    void connectItems();

    //! Creates and initializes the color map
    void initPlot();

    void setConnected(bool isConnected);

    //! Connects/disconnects signals related to SpecularPlot's X,Y axes rectangle change.
    void setAxesRangeConnected(bool isConnected);

    //! Sets initial state of SpecularPlot for all items.
    void setPlot();

    //! Sets axes ranges and logarithmic scale on y-axis if necessary.
    void setAxes();

    //! Sets (xmin,xmax) and (ymin,ymax) of SpecularPlot from the first item.
    void setAxesRanges();

    //! Sets X,Y axes labels from the first item
    void setAxesLabels();

    //! Sets the data values to SpecularPlot.
    void setDataFromItem(Data1DItem* item);

    QVector<Data1DItem*> m_data_items;

    QMap<Data1DItem*, QCPGraph*> m_graph_map;
    QMap<Data1DItem*, QCPErrorBars*> m_errorbar_map;
};

#endif // BORNAGAIN_GUI_VIEW_PLOTTER_SPECULARPLOT_H
