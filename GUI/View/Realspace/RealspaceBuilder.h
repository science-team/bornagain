//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Realspace/RealspaceBuilder.h
//! @brief     Defines class RealspaceBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_REALSPACE_REALSPACEBUILDER_H
#define BORNAGAIN_GUI_VIEW_REALSPACE_REALSPACEBUILDER_H

#include "Base/Type/Field2D.h"
#include "Img3D/View/Camera.h"

class Item3D;
class ItemWithParticles;
class LayerItem;
class LayerStackItem;
class ParticleLayoutItem;
class SampleItem;
struct SceneGeometry;

namespace Img3D {
class BuilderUtils;
class Model;
class Particle3DContainer;
} // namespace Img3D

class RealspaceBuilder {
public:
    RealspaceBuilder(std::function<QColor(const QString&)> fnColorFromMaterialName);
    ~RealspaceBuilder();

    void populate(Img3D::Model* model, const Item3D* item, const SampleItem* sampleItem,
                  const SceneGeometry& sceneGeometry, unsigned int& numParticles) const;

private:
    std::unique_ptr<Img3D::BuilderUtils> m_builder_utils;

    std::vector<Img3D::Particle3DContainer>
    particle3DContainerVector(const ParticleLayoutItem& layoutItem,
                              const Img3D::F3& origin = {}) const;

    void populateSample(Img3D::Model* model, const SampleItem& sampleItem,
                        const SceneGeometry& sceneGeometry, unsigned int& numParticles) const;

    void populateStack(Img3D::Model* model, const LayerStackItem& stackItem,
                       const SampleItem& sampleItem, const SceneGeometry& sceneGeometry,
                       unsigned int& numParticles) const;

    void populateLayer(Img3D::Model* model, const LayerItem& layerItem,
                       const SampleItem& sampleItem, const SceneGeometry& sceneGeometry,
                       unsigned int& numParticles, const Img3D::F3& origin = {},
                       const double2d_t* topRoughMap = nullptr,
                       const double2d_t* bottomRoughMap = nullptr, bool drawBottom = true,
                       bool independentLayer = true) const;

    void populateLayout(Img3D::Model* model, const ParticleLayoutItem& layoutItem,
                        const SceneGeometry& sceneGeometry, unsigned int& numParticles,
                        const Img3D::F3& origin = {}) const;

    Img3D::Particle3DContainer particlesFromItem(const ItemWithParticles& particleItem,
                                                 double total_abundance = 1,
                                                 const Img3D::F3& origin = {}) const;

    void translateContainer(Img3D::Model* model,
                            const Img3D::Particle3DContainer& particle3DContainer,
                            unsigned& numParticles, const Img3D::F3& lattice_position = {}) const;

    void populateParticlesInLattice(
        const double2d_t& lattice_positions,
        const std::vector<Img3D::Particle3DContainer>& particle3DContainer_vector,
        Img3D::Model* model, const SceneGeometry& sceneGeometry, unsigned& numParticles,
        int seed) const;
};

#endif // BORNAGAIN_GUI_VIEW_REALSPACE_REALSPACEBUILDER_H
