//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Realspace/RealspacePanel.cpp
//! @brief     Implements class RealspacePanel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Realspace/RealspacePanel.h"
#include "GUI/View/Realspace/RealspaceWidget.h"
#include "GUI/View/Widget/StyledToolbar.h"
#include <QCheckBox>
#include <QVBoxLayout>

RealspacePanel::RealspacePanel(QWidget* parent)
    : QWidget(parent)
    , m_widget(new RealspaceWidget)
{
    setWindowTitle("Real Space");

    auto* tb_top = new StyledToolbar(this);
    auto* tb_bottom = new StyledToolbar(this);

    const auto createAction = [&](QToolBar* tb, const QString& text,
                                  const QString& tooltip) -> QAction* {
        auto* action = new QAction(text, this);
        action->setToolTip(tooltip);
        if (!tb->actions().empty())
            tb->addSeparator();
        tb->addAction(action);
        return action;
    };

    // Top row

    auto* action = createAction(tb_top, "Save picture", "Save 3D real space view as .png file");
    connect(action, &QAction::triggered, m_widget, &RealspaceWidget::savePicture);

    action = createAction(tb_top, "Default view", "Reset view and zoom level to default");
    connect(action, &QAction::triggered, m_widget, &RealspaceWidget::defaultView);

    action = createAction(tb_top, "Side view", "View sample from the side at current zoom level");
    connect(action, &QAction::triggered, m_widget, &RealspaceWidget::sideView);

    action = createAction(tb_top, "Top view", "View sample from the top at current zoom level");
    connect(action, &QAction::triggered, m_widget, &RealspaceWidget::topView);

    action = createAction(tb_top, "Enlarge", "Increase layer size");
    connect(action, &QAction::triggered, [this] { m_widget->changeLayerSize(1.25); });

    action = createAction(tb_top, "Reduce", "Decrease layer size");
    connect(action, &QAction::triggered, [this] { m_widget->changeLayerSize(0.8); });

    // Bottom row

    auto* showRoughness = new QCheckBox("Show roughness");
    m_widget->setShowRoughnessCheckbox(showRoughness);
    tb_bottom->addWidget(showRoughness);

    action = createAction(tb_bottom, "Regenerate roughness",
                          "Regenerate random roughness on interfaces");
    connect(action, &QAction::triggered, m_widget, &RealspaceWidget::regenerateRoughness);

    action = createAction(tb_bottom, "Regenerate particle positions",
                          "Regenerate random particle positions in layouts");
    connect(action, &QAction::triggered, m_widget, &RealspaceWidget::regenerateParticlePositions);


    auto* mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(tb_top);
    mainLayout->addWidget(tb_bottom);
    mainLayout->addWidget(m_widget);
}

QSize RealspacePanel::sizeHint() const
{
    return {300, 300};
}
