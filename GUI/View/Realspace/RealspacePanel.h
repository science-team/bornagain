//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Realspace/RealspacePanel.h
//! @brief     Defines class RealspacePanel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_REALSPACE_REALSPACEPANEL_H
#define BORNAGAIN_GUI_VIEW_REALSPACE_REALSPACEPANEL_H

#include <QWidget>

class RealspaceWidget;

//! Panel to show 3D view of sample. Contains toolbar and RealspaceWidget

class RealspacePanel : public QWidget {
    Q_OBJECT
public:
    RealspacePanel(QWidget* parent);

    QSize sizeHint() const override;
    RealspaceWidget* widget() { return m_widget; }

private:
    RealspaceWidget* m_widget;
};

#endif // BORNAGAIN_GUI_VIEW_REALSPACE_REALSPACEPANEL_H
