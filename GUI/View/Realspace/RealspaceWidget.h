//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Realspace/RealspaceWidget.h
//! @brief     Defines class RealspaceWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_REALSPACE_REALSPACEWIDGET_H
#define BORNAGAIN_GUI_VIEW_REALSPACE_REALSPACEWIDGET_H

#include "Img3D/Type/SceneGeometry.h"
#include <QCheckBox>
#include <QLabel>
#include <memory>

class CautionSign;
class Item3D;
class SampleItem;

namespace Img3D {
class Canvas;
class Model;
} // namespace Img3D

//! Provides 3D object generation for Realspace presentation.
class RealspaceWidget : public QWidget {
    Q_OBJECT
public:
    RealspaceWidget(QWidget* parent = nullptr);
    ~RealspaceWidget() override;

    void setDisplayedItem(SampleItem* containingSample, Item3D* item);
    void clearDisplay() { setDisplayedItem(nullptr, nullptr); }
    void defaultView();
    void sideView();
    void topView();
    void regenerateParticlePositions();
    void regenerateRoughness();
    void changeLayerSize(double layerSizeChangeScale);
    void savePicture();
    void resetRealScene();
    void updateRealScene();
    Item3D* displayedItem() { return m_displayed_item; }

    void setShowRoughnessCheckbox(QCheckBox* cb) { m_showRoughnessCheckbox = cb; }

protected:
    void showEvent(QShowEvent*) override;

private:
    void connectShowRoughnessCheckbox();

    Img3D::Canvas* m_canvas;
    std::unique_ptr<Img3D::Model> m_realspace_model;
    SceneGeometry m_scene_geometry;
    CautionSign* m_caution_sign;
    QLabel* m_caution_label;
    Item3D* m_displayed_item;
    SampleItem* m_containing_sample;
    QCheckBox* m_showRoughnessCheckbox;
    bool m_first_view;
};

#endif // BORNAGAIN_GUI_VIEW_REALSPACE_REALSPACEWIDGET_H
