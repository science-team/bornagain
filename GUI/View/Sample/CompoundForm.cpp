//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/CompoundForm.cpp
//! @brief     Implements class CompoundForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Sample/CompoundForm.h"
#include "Base/Util/Vec.h"
#include "GUI/Model/Sample/CompoundItem.h"
#include "GUI/View/Base/ActionFactory.h"
#include "GUI/View/Sample/HeinzFormLayout.h"
#include "GUI/View/Sample/LayerEditorUtil.h"
#include <QAction>
#include <QPushButton>

CompoundForm::CompoundForm(QWidget* parent, CompoundItem* compoundItem, SampleEditorController* ec,
                           bool allowRemove)
    : CollapsibleGroupBox("Compound", parent, compoundItem->expandCompound)
    , m_layout(new HeinzFormLayout(ec))
    , m_composition_item(compoundItem)
    , m_ec(ec)
{
    body()->setLayout(m_layout);
    m_layout->setContentsMargins(30, 6, 0, 0);
    m_layout->addVector(compoundItem->position(), false);
    m_layout->addSelection(compoundItem->rotationSelection());
    m_layout->addValue(compoundItem->abundance());

    for (auto* particle : compoundItem->itemsWithParticles())
        m_layout->addRow(
            GUI::Util::Layer::createWidgetForItemWithParticles(this, particle, false, ec));

    auto* btn = GUI::Util::Layer::createAddParticleButton(
        this, [=](FormfactorCatalog::Type type) { ec->addCompoundItem(compoundItem, type); },
        [=](ParticleCatalog::Type type) { ec->addCompoundItem(compoundItem, type); });

    m_layout->addStructureEditingRow(btn);

    // top right corner actions
    // show in real space
    {
        auto* showInRealspaceAction = ActionFactory::createShowInRealspaceAction(
            this, "particle composition",
            [ec, compoundItem] { ec->requestViewInRealspace(compoundItem); });
        addTitleAction(showInRealspaceAction);
    }
    // duplicate
    {
        m_duplicate_action =
            ActionFactory::createDuplicateAction(this, "particle composition", [ec, compoundItem] {
                ec->duplicateItemWithParticles(compoundItem);
            });
        addTitleAction(m_duplicate_action);
    }
    // remove
    {
        m_remove_action = ActionFactory::createRemoveAction(
            this, "particle composition", [ec, compoundItem] { ec->removeParticle(compoundItem); });
        if (allowRemove)
            addTitleAction(m_remove_action);
    }
}

CompoundForm::~CompoundForm() = default;
void CompoundForm::onParticleAdded(ItemWithParticles* item)
{
    int index = Vec::indexOfPtr(item, m_composition_item->itemsWithParticles());
    const int rowInLayout = m_layout->rowCount() - 1
                            - (m_composition_item->itemsWithParticles().size() - 1)
                            + index; // -1: btn

    m_layout->insertRow(
        rowInLayout, GUI::Util::Layer::createWidgetForItemWithParticles(this, item, false, m_ec));
}

void CompoundForm::onAboutToRemoveParticle(ItemWithParticles* item)
{
    int index = Vec::indexOfPtr(item, m_composition_item->itemsWithParticles());
    const int rowInLayout = m_layout->rowCount() - m_composition_item->itemsWithParticles().size()
                            - 1 + index; // -1: btn

    m_layout->removeRow(rowInLayout);
}
