//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/CompoundForm.h
//! @brief     Defines class CompoundForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_COMPOUNDFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_COMPOUNDFORM_H

#include "GUI/View/Widget/GroupBoxes.h"

class CompoundItem;
class HeinzFormLayout;
class ItemWithParticles;
class SampleEditorController;

//! Form for editing a particle composition
class CompoundForm : public CollapsibleGroupBox {
    Q_OBJECT
public:
    CompoundForm(QWidget* parent, CompoundItem* compoundItem, SampleEditorController* ec,
                 bool allowRemove = true);
    ~CompoundForm();

    CompoundItem* compositionItem() const { return m_composition_item; }
    void onParticleAdded(ItemWithParticles* item);
    void onAboutToRemoveParticle(ItemWithParticles* item);

private:
    HeinzFormLayout* m_layout;
    CompoundItem* m_composition_item;
    QAction* m_remove_action = nullptr;
    QAction* m_duplicate_action = nullptr;
    SampleEditorController* m_ec;
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_COMPOUNDFORM_H
