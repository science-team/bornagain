//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/CoreAndShellForm.h
//! @brief     Defines class CoreAndShellForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_COREANDSHELLFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_COREANDSHELLFORM_H

#include "GUI/View/Widget/GroupBoxes.h"
#include <QComboBox>
#include <memory>

class CoreAndShellItem;
class HeinzFormLayout;
class SampleEditorController;

//! Form for editing a core/shell particle
class CoreAndShellForm : public CollapsibleGroupBox {
    Q_OBJECT
public:
    CoreAndShellForm(QWidget* parent, CoreAndShellItem* coreShellItem, SampleEditorController* ec,
                     bool allowRemove = true);

    CoreAndShellItem* coreShellItem() const { return m_item; }

    void createCoreWidgets();
    void createShellWidgets();

private:
    void onCoreComboChanged();
    void onShellComboChanged();

    void showCoreInRealspace();
    void showShellInRealspace();

    CoreAndShellItem* m_item;
    QAction* m_remove_action = nullptr;
    QAction* m_duplicate_action = nullptr;
    SampleEditorController* m_ec;
    struct Location {
        HeinzFormLayout* layout;
        QComboBox* formfactorCombo = nullptr;
    };
    Location core;
    Location shell;
};


#endif // BORNAGAIN_GUI_VIEW_SAMPLE_COREANDSHELLFORM_H
