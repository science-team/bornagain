//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/ISelectionForm.cpp
//! @brief     Implements class ISelectionForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Sample/ISelectionForm.h"
#include "GUI/View/Base/LayoutUtil.h"

ISelectionForm::ISelectionForm(QWidget* parent, SampleEditorController* ec)
    : QWidget(parent)
    , m_ec(ec)
{
}

void ISelectionForm::initUI(PolyBase& d)
{
    m_grid_layout = new QGridLayout(this);
    m_grid_layout->setContentsMargins(0, 0, 0, 0);
    m_grid_layout->setSpacing(6);

    m_combo = new QComboBox;
    WheelEventEater::install(m_combo);
    m_combo->addItems(d.menuEntries());
    m_combo->setCurrentIndex(d.certainIndex());
    m_combo->setMaxVisibleItems(m_combo->count());

    QObject::connect(m_combo, &QComboBox::currentIndexChanged, [this, &d](int current) {
        clear();
        d.setCertainIndex(current);
        createContent();
        emit gDoc->sampleChanged();
    });

    m_grid_layout->addWidget(m_combo, 1, 0);
    createContent();
}

void ISelectionForm::clear()
{
    auto* layoutItemOfComboBox = m_grid_layout->itemAtPosition(1, 0);
    m_grid_layout->takeAt(m_grid_layout->indexOf(layoutItemOfComboBox));
    GUI::Util::Layout::clearLayout(m_grid_layout, true);
    m_grid_layout->addWidget(layoutItemOfComboBox->widget(), 1, 0);
}
