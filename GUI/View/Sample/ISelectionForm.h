//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/ISelectionForm.h
//! @brief     Defines class ISelectionForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_ISELECTIONFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_ISELECTIONFORM_H

#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include "GUI/View/Base/CustomEventFilters.h"
#include "GUI/View/Sample/SampleEditorController.h"
#include <QComboBox>
#include <QGridLayout>

//! Abstract widget class that provides a combo box for selecting a polymorphous class.

class ISelectionForm : public QWidget {
public:
    virtual void createContent() = 0;

protected:
    ISelectionForm(QWidget* parent, SampleEditorController* ec);

    void initUI(PolyBase& d);

    QGridLayout* m_grid_layout;
    QComboBox* m_combo;
    SampleEditorController* m_ec;

private:
    //! Remove all properties from the layout
    void clear();
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_ISELECTIONFORM_H
