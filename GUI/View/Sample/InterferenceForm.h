//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/InterferenceForm.h
//! @brief     Defines class InterferenceForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_INTERFERENCEFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_INTERFERENCEFORM_H

#include "GUI/View/Widget/GroupBoxes.h"
#include <QComboBox>

class HeinzFormLayout;
class ParticleLayoutItem;
class SampleEditorController;

//! Form for editing interference functions
class InterferenceForm : public CollapsibleGroupBox {
    Q_OBJECT
public:
    InterferenceForm(QWidget* parent, ParticleLayoutItem* layoutItem, SampleEditorController* ec);

    ParticleLayoutItem* layoutItem() const { return m_layout_item; }
    void onInterferenceTypeChanged();

private:
    void createInterferenceWidgets();
    void updateTitle();

    HeinzFormLayout* m_layout;
    QComboBox* m_cb;
    ParticleLayoutItem* m_layout_item;
    SampleEditorController* m_ec;
};


#endif // BORNAGAIN_GUI_VIEW_SAMPLE_INTERFERENCEFORM_H
