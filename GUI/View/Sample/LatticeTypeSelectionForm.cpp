//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/LatticeTypeSelectionForm.cpp
//! @brief     Implements class LatticeTypeSelectionForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Sample/LatticeTypeSelectionForm.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/Lattice2DItems.h"

LatticeTypeSelectionForm::LatticeTypeSelectionForm(
    QWidget* parent, Interference2DAbstractLatticeItem* interferenceItem,
    SampleEditorController* ec)
    : ISelectionForm(parent, ec)
    , m_interference_item(interferenceItem)
{
    initUI(interferenceItem->latticeTypeSelection());
}

void LatticeTypeSelectionForm::createContent()
{
    auto* currentLatticeType = m_interference_item->latticeTypeSelection().certainItem();
    const auto valueProperties = currentLatticeType->geometryValues(false);
    const bool vertical = valueProperties.size() > 2;

    const auto onValueChange = [this] { m_ec->setDensityRelatedValue(m_interference_item); };
    GUI::Util::Layer::addMultiPropertyToGrid(m_grid_layout, 1, valueProperties, vertical, false,
                                             onValueChange);

    m_integrate_over_xi_check_box = new QCheckBox("Integrate over Xi", this);
    m_integrate_over_xi_check_box->setChecked(m_interference_item->xiIntegration());
    m_grid_layout->addWidget(m_integrate_over_xi_check_box, 1, m_grid_layout->columnCount());
    connect(m_integrate_over_xi_check_box, &QCheckBox::stateChanged, [this] {
        interferenceItem()->setXiIntegration(m_integrate_over_xi_check_box->isChecked());
        onIntegrateOverXiChanged();
        emit gDoc->sampleChanged();
    });

    const int colOfXiLabel = m_grid_layout->columnCount();
    GUI::Util::Layer::addMultiPropertyToGrid(m_grid_layout, m_grid_layout->columnCount(),
                                             {&currentLatticeType->latticeRotationAngle()},
                                             vertical, true, onValueChange);
    m_xi_label = qobject_cast<QLabel*>(
        m_grid_layout->itemAtPosition(vertical ? 0 : 1, colOfXiLabel)->widget());
    ASSERT(m_xi_label);

    updateXiVisibility();
}

void LatticeTypeSelectionForm::onIntegrateOverXiChanged()
{
    QSignalBlocker b(m_integrate_over_xi_check_box);
    m_integrate_over_xi_check_box->setChecked(m_interference_item->xiIntegration());
    updateXiVisibility();
}

void LatticeTypeSelectionForm::updateXiVisibility()
{
    m_xi_label->setVisible(!m_interference_item->xiIntegration());
    m_xi_label->buddy()->setVisible(!m_interference_item->xiIntegration());
}
