//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/LatticeTypeSelectionForm.h
//! @brief     Defines class LatticeTypeSelectionForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_LATTICETYPESELECTIONFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_LATTICETYPESELECTIONFORM_H

#include "GUI/Model/Sample/InterferenceItems.h"
#include "GUI/View/Sample/PolyForm.h"
#include <QCheckBox>
#include <QLabel>

class Interference2DAbstractLatticeItem;
class SampleEditorController;

//! Form for editing lattice type values
class LatticeTypeSelectionForm : public ISelectionForm {
public:
    LatticeTypeSelectionForm(QWidget* parent, Interference2DAbstractLatticeItem* interferenceItem,
                             SampleEditorController* ec);

    virtual void createContent() override;
    void onIntegrateOverXiChanged();
    void updateXiVisibility();

    Interference2DAbstractLatticeItem* interferenceItem() const { return m_interference_item; }

private:
    Interference2DAbstractLatticeItem* m_interference_item;
    QLabel* m_xi_label;
    QCheckBox* m_integrate_over_xi_check_box;
};


#endif // BORNAGAIN_GUI_VIEW_SAMPLE_LATTICETYPESELECTIONFORM_H
