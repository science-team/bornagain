//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/LayerContainerForm.cpp
//! @brief     Implements class LayerContainerForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Sample/LayerContainerForm.h"
#include "Base/Util/Assert.h"
#include "Base/Util/Vec.h"
#include "GUI/Model/Sample/ItemWithLayers.h"
#include "GUI/Model/Sample/LayerStackItem.h"
#include "GUI/Model/Sample/SampleItem.h"
#include "GUI/Model/Type/PredefinedColors.h"
#include "GUI/View/Base/ActionFactory.h"
#include "GUI/View/Sample/HeinzFormLayout.h"
#include "GUI/View/Sample/SampleEditorController.h"
#include "GUI/View/Widget/WidgetMoverButton.h"
#include <QMenu>

LayerContainerForm::LayerContainerForm(QWidget* parent, ItemWithLayers* item,
                                       SampleEditorController* ec, const QString& what)
    : CollapsibleGroupBox(parent, item->expandGroupbox)
    , m_layout(new HeinzFormLayout(ec))
    , m_item(item)
    , m_ec(ec)
{
    ASSERT(parent);
    setContentsMargins(5, 5, 5, 5);

    body()->setLayout(m_layout);

    //... top right corner actions

    // choose color action
    auto* chooseColorAction = new QAction(this);
    chooseColorAction->setText("Choose color");
    chooseColorAction->setIcon(QIcon(":/images/palette.svg"));
    chooseColorAction->setIconText("Choose color");
    chooseColorAction->setToolTip("Choose a color for this layer");
    auto* menu = new QMenu(this);
    chooseColorAction->setMenu(menu);
    addTitleAction(chooseColorAction);

    for (const auto& color : GUI::Colors::layerDefaults()) {
        QPixmap p(64, 64);
        p.fill(color);
        auto* ca = menu->addAction(QIcon(p), "");
        connect(ca, &QAction::triggered, [this, color] {
            m_item->setColor(color);
            updateColor();
            gDoc->setModified();
        });
    }

    // move component action
    m_move_button = new WidgetMoverButton(this, this, 0);
    m_move_button->setToolTip("Move " + what + " up/down");
    connect(m_move_button, &WidgetMoverButton::finishedMoving, ec,
            &SampleEditorController::onStoppedToMoveComponent);
    addTitleWidget(m_move_button);

    // show in real space action
    auto* showInRealspaceAction = ActionFactory::createShowInRealspaceAction(
        this, what, [ec, item] { ec->requestViewInRealspace(item); });
    addTitleAction(showInRealspaceAction);

    // duplicate action
    auto* duplicate_action = ActionFactory::createDuplicateAction(
        this, what, [ec, item] { ec->duplicateItemWithLayers(item); });
    addTitleAction(duplicate_action);

    // remove action
    auto* remove_action = ActionFactory::createRemoveAction(
        this, what, [ec, item] { ec->removeItemWithLayers(item); });
    addTitleAction(remove_action);

    updateColor();
}

void LayerContainerForm::expand()
{
    setExpanded(true);
}

void LayerContainerForm::updatePositionDependentElements()
{
    LayerStackItem* parentStack = m_ec->sampleItem()->parentOfComponent(m_item);
    m_move_button->setVisible(parentStack && parentStack->componentItems().size() > 1);
}

void LayerContainerForm::updateColor()
{
    if (isOuterStack())
        return;

    QColor bckgrCol = m_item->color();
    setStyleSheet("LayerContainerForm {background-color: " + bckgrCol.name(QColor::HexRgb) + "}");
}

bool LayerContainerForm::isOuterStack() const
{
    return (m_item == &(m_ec->sampleItem()->outerStackItem()));
}
