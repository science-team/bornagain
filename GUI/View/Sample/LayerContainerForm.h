//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/LayerContainerForm.h
//! @brief     Defines class LayerContainerForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_LAYERCONTAINERFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_LAYERCONTAINERFORM_H

#include "GUI/View/Widget/GroupBoxes.h"

class HeinzFormLayout;
class ItemWithLayers;
class SampleEditorController;
class WidgetMoverButton;

//! Base form for editing layers and stacks

class LayerContainerForm : public CollapsibleGroupBox {
    Q_OBJECT
public:
    LayerContainerForm(QWidget* parent, ItemWithLayers* item, SampleEditorController* ec,
                       const QString& what);
    ItemWithLayers* item() const { return m_item; }
    void expand();

    virtual void updatePositionDependentElements();

protected:
    void updateColor();
    bool isOuterStack() const;

    HeinzFormLayout* m_layout;
    ItemWithLayers* m_item;
    SampleEditorController* m_ec;
    WidgetMoverButton* m_move_button;
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_LAYERCONTAINERFORM_H
