//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/LayerEditorUtil.h
//! @brief     Defines class GUI::Util::Layer.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_LAYEREDITORUTIL_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_LAYEREDITORUTIL_H

#include "GUI/Model/Descriptor/DoubleProperty.h"
#include "GUI/Model/Sample/FormfactorCatalog.h"
#include "GUI/Model/Sample/ParticleCatalog.h"
#include <QGridLayout>
#include <QPushButton>
#include <functional>

class ItemWithParticles;
class SampleEditorController;

//! Utility functions to support layer oriented sample editor
namespace GUI::Util::Layer {

//! Create spin boxes for the DoubleProperties and connect them to the given setNewValue()
QVector<QWidget*> addMultiPropertyToGrid(QGridLayout* m_grid_layout, int firstCol,
                                         const DoubleProperties& valueProperties, bool vertically,
                                         bool addSpacer,
                                         std::function<void()> setNewValue = nullptr);

QVector<QWidget*> addMultiPropertyToGrid(QGridLayout* m_grid_layout, int firstCol,
                                         const DoubleProperties& valueProperties, bool addSpacer);

QWidget* createWidgetForItemWithParticles(QWidget* parentWidget,
                                          ItemWithParticles* itemWithParticles, bool allowAbundance,
                                          SampleEditorController* ec, bool allowRemove = true);

QPushButton*
createAddParticleButton(QWidget* parentWidget,
                        std::function<void(FormfactorCatalog::Type t)> slotAddFormfactor,
                        std::function<void(ParticleCatalog::Type t)> slotAddParticle);

QVector<QColor> predefinedLayerColors();

} // namespace GUI::Util::Layer

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_LAYEREDITORUTIL_H
