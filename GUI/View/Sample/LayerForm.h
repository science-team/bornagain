//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/LayerForm.h
//! @brief     Defines class LayerForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_LAYERFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_LAYERFORM_H

#include "GUI/View/Sample/LayerContainerForm.h"

class LayerItem;
class ParticleLayoutItem;
class RoughnessForm;
class SampleEditorController;

//! Form for editing a layer
class LayerForm : public LayerContainerForm {
    Q_OBJECT
public:
    LayerForm(QWidget* parent, LayerItem* layer, SampleEditorController* ec);

    void updatePositionDependentElements() override;
    void onLayoutAdded(ParticleLayoutItem* t);
    void onAboutToRemoveLayout(ParticleLayoutItem* t);
    LayerItem* layerItem() const;
    void updateTitle();

private:
    int m_thickness_row;
    int m_roughness_row;
    RoughnessForm* m_roughnessForm;
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_LAYERFORM_H
