//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/LayerStackForm.h
//! @brief     Defines class LayerStackForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_LAYERSTACKFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_LAYERSTACKFORM_H

#include "GUI/View/Sample/LayerContainerForm.h"
#include <QBoxLayout>

class LayerStackItem;

class LayerStackForm : public LayerContainerForm {
    Q_OBJECT
public:
    LayerStackForm(QWidget* parent, LayerStackItem* stack, SampleEditorController* ec);
    LayerStackItem& stackItem() const;

    //! Create widgets for the new layer or stack.
    void addComponentForm(ItemWithLayers* componentItem);

    //! Call this when a component has been moved to a different position.
    //!
    //! This updates the item's position in the layout.
    void onComponentMoved(const ItemWithLayers* componentItem);

    //! Call this before removing (deleting) a component.
    //!
    //! Any widgets related to the item will be deleted or scheduled for later deletion.
    void removeComponentForm(ItemWithLayers* componentItem);

    //! Search for the next LayerContainerForm, starting from the given widget.
    //!
    //! The search starts with the given widget itself. If it is a LayerContainerForm, it is
    //! returned. If no following LayerContainerForm is found, nullptr is returned.
    LayerContainerForm* findNextComponentForm(QWidget* w);

    void updatePositionDependentElements() override;

private:
    int rowInLayout(const ItemWithLayers* componentItem) const;
    void updateTitle();

    QVBoxLayout* m_components_layout;
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_LAYERSTACKFORM_H
