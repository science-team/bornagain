//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/MaterialInplaceForm.h
//! @brief     Defines class MaterialInplaceForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_MATERIALINPLACEFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_MATERIALINPLACEFORM_H

#include <QGridLayout>
#include <QLabel>

class ItemWithMaterial;
class SampleEditorController;

//! Form to select a material and to edit it in-place
class MaterialInplaceForm : public QWidget {
    Q_OBJECT
public:
    MaterialInplaceForm(ItemWithMaterial* item, SampleEditorController* ec);

    ItemWithMaterial* itemWithMaterial() const { return m_item; }
    void updateValues();

private:
    void selectMaterial();
    void createWidgets();

    ItemWithMaterial* m_item;
    SampleEditorController* m_ec;
    QGridLayout* m_layout;
    QVector<QWidget*> m_magnetic_widgets;
    QLabel* m_material_label;
};


#endif // BORNAGAIN_GUI_VIEW_SAMPLE_MATERIALINPLACEFORM_H
