//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/MesocrystalForm.cpp
//! @brief     Implements class MesocrystalForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Sample/MesocrystalForm.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Sample/FormfactorItems.h"
#include "GUI/Model/Sample/MesocrystalItem.h"
#include "GUI/Model/Sample/ParticleItem.h"
#include "GUI/View/Base/ActionFactory.h"
#include "GUI/View/Sample/HeinzFormLayout.h"
#include <QAction>

MesocrystalForm::MesocrystalForm(QWidget* parent, MesocrystalItem* mesocrystalItem,
                                 SampleEditorController* ec, bool allowRemove)
    : CollapsibleGroupBox("Mesocrystal", parent, mesocrystalItem->expandMesocrystal)
    , m_layout(new HeinzFormLayout(ec))
    , m_item(mesocrystalItem)
    , m_ec(ec)
    , m_basis_combo(createBasisCombo(this, mesocrystalItem->basisItem()))
{

    body()->setLayout(m_layout);
    m_layout->setContentsMargins(30, 6, 0, 0);
    m_layout->addVector(mesocrystalItem->position(), false);
    m_layout->addSelection(mesocrystalItem->rotationSelection());
    m_layout->addValue(mesocrystalItem->abundance());
    m_layout->addVector(mesocrystalItem->vectorA(), false);
    m_layout->addVector(mesocrystalItem->vectorB(), false);
    m_layout->addVector(mesocrystalItem->vectorC(), false);

    m_layout->addSelection(mesocrystalItem->outerShapeSelection());


    connect(m_basis_combo, &QComboBox::currentIndexChanged, this,
            &MesocrystalForm::onBasisComboChanged);
    m_layout->addBoldRow("Basis type", m_basis_combo);
    m_row_of_basis_type_combo = m_layout->rowCount() - 1;
    createBasisWidgets();

    //... top right corner actions

    // show in real space
    auto* showInRealspaceAction =
        ActionFactory::createShowInRealspaceAction(this, "meso crystal", [ec, mesocrystalItem] {
            ec->requestViewInRealspace(mesocrystalItem);
        });
    addTitleAction(showInRealspaceAction);

    // duplicate
    m_duplicate_action =
        ActionFactory::createDuplicateAction(this, "meso crystal", [ec, mesocrystalItem] {
            ec->duplicateItemWithParticles(mesocrystalItem);
        });
    addTitleAction(m_duplicate_action);

    // remove
    m_remove_action = ActionFactory::createRemoveAction(
        this, "meso crystal", [ec, mesocrystalItem] { ec->removeParticle(mesocrystalItem); });
    if (allowRemove)
        addTitleAction(m_remove_action);
}

QComboBox* MesocrystalForm::createBasisCombo(QWidget* parent, ItemWithParticles* current)
{
    auto* combo = new QComboBox(parent);
    WheelEventEater::install(combo);
    combo->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);

    uint32_t currentData = 0;
    for (auto type : FormfactorCatalog::types()) {
        const auto ui = FormfactorCatalog::uiInfo(type);
        combo->addItem(QIcon(ui.iconPath), ui.menuEntry, static_cast<uint32_t>(type));
        if (auto* p = dynamic_cast<ParticleItem*>(current))
            if (FormfactorCatalog::type(p->formFactorItem()) == type)
                currentData = static_cast<uint32_t>(type);
    }
    for (auto type : ParticleCatalog::assemblyTypes()) {
        const auto ui = ParticleCatalog::uiInfo(type);
        combo->addItem(QIcon(ui.iconPath), ui.menuEntry, 1000 + static_cast<uint32_t>(type));
        if (ParticleCatalog::type(current) == type)
            currentData = 1000 + static_cast<uint32_t>(type);
    }

    combo->setMaxVisibleItems(combo->count());

    const auto currentIndex = combo->findData(currentData);
    ASSERT(currentIndex >= 0);
    combo->setCurrentIndex(currentIndex);

    return combo;
}

void MesocrystalForm::onBasisComboChanged()
{
    while (m_layout->rowCount() > m_row_of_basis_type_combo + 1)
        m_layout->removeRow(m_row_of_basis_type_combo + 1);

    const auto currentData = m_basis_combo->currentData().toUInt();
    if (currentData < 1000)
        m_ec->setMesocrystalBasis(this, static_cast<FormfactorCatalog::Type>(currentData));
    else
        m_ec->setMesocrystalBasis(this, static_cast<ParticleCatalog::Type>(currentData - 1000));
}

void MesocrystalForm::createBasisWidgets()
{
    if (!m_item->basisItem())
        return;

    m_layout->addRow(GUI::Util::Layer::createWidgetForItemWithParticles(this, m_item->basisItem(),
                                                                        false, m_ec, false));
}
