//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/MesocrystalForm.h
//! @brief     Defines class MesocrystalForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_MESOCRYSTALFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_MESOCRYSTALFORM_H

#include "GUI/View/Widget/GroupBoxes.h"
#include <QComboBox>

class HeinzFormLayout;
class ItemWithParticles;
class MesocrystalItem;
class SampleEditorController;

//! Form for editing a mesocrystal
class MesocrystalForm : public CollapsibleGroupBox {
    Q_OBJECT
public:
    MesocrystalForm(QWidget* parent, MesocrystalItem* mesocrystalItem, SampleEditorController* ec,
                    bool allowRemove = true);

    MesocrystalItem* mesocrystalItem() const { return m_item; }
    void createBasisWidgets();

private:
    QComboBox* createBasisCombo(QWidget* parent, ItemWithParticles* current);

    void onBasisComboChanged();

    HeinzFormLayout* m_layout;
    MesocrystalItem* m_item;
    QAction* m_remove_action = nullptr;
    QAction* m_duplicate_action = nullptr;
    SampleEditorController* m_ec;
    QComboBox* m_basis_combo;
    int m_row_of_basis_type_combo;
};


#endif // BORNAGAIN_GUI_VIEW_SAMPLE_MESOCRYSTALFORM_H
