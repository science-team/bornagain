//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/ParticleForm.cpp
//! @brief     Implements class ParticleForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Sample/ParticleForm.h"
#include "GUI/Model/Sample/FormfactorCatalog.h"
#include "GUI/Model/Sample/FormfactorItems.h"
#include "GUI/Model/Sample/ParticleItem.h"
#include "GUI/View/Base/ActionFactory.h"
#include "GUI/View/Sample/HeinzFormLayout.h"
#include "GUI/View/Sample/MaterialInplaceForm.h"
#include <QAction>

namespace {

QString titleOfItem(const ParticleItem* particleItem)
{
    return "Particle (" + FormfactorCatalog::menuEntry(particleItem->formFactorItem()) + ")";
}

} // namespace

ParticleForm::ParticleForm(QWidget* parent, ParticleItem* particleItem, bool allowAbundance,
                           SampleEditorController* ec, bool allowRemove)
    : CollapsibleGroupBox(::titleOfItem(particleItem), parent, particleItem->expandParticle)
{
    auto* layout = new HeinzFormLayout(ec);
    body()->setLayout(layout);

    layout->addBoldRow("Material", new MaterialInplaceForm(particleItem, ec));
    layout->addGroupOfValues("Geometry", particleItem->formFactorItem()->geometryProperties());
    layout->addVector(particleItem->position(), false);
    layout->addSelection(particleItem->rotationSelection());
    if (allowAbundance)
        layout->addValue(particleItem->abundance());

    // top right corner actions
    // show in real space
    {
        auto* showInRealspaceAction = ActionFactory::createShowInRealspaceAction(
            this, "particle", [ec, particleItem] { ec->requestViewInRealspace(particleItem); });
        addTitleAction(showInRealspaceAction);
    }
    // duplicate
    {
        m_duplicate_action = ActionFactory::createDuplicateAction(
            this, "particle", [ec, particleItem] { ec->duplicateItemWithParticles(particleItem); });
        addTitleAction(m_duplicate_action);
    }
    // remove
    {
        m_remove_action = ActionFactory::createRemoveAction(
            this, "particle", [ec, particleItem] { ec->removeParticle(particleItem); });
        if (allowRemove)
            addTitleAction(m_remove_action);
    }
}
