//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/ParticleForm.h
//! @brief     Defines class ParticleForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_PARTICLEFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_PARTICLEFORM_H

#include "GUI/View/Widget/GroupBoxes.h"

class ParticleItem;
class SampleEditorController;

//! Form for editing a particle
class ParticleForm : public CollapsibleGroupBox {
    Q_OBJECT
public:
    ParticleForm(QWidget* parent, ParticleItem* particleItem, bool allowAbundance,
                 SampleEditorController* ec, bool allowRemove = true);

private:
    QAction* m_remove_action = nullptr;
    QAction* m_duplicate_action = nullptr;
};


#endif // BORNAGAIN_GUI_VIEW_SAMPLE_PARTICLEFORM_H
