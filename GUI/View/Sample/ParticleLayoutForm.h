//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/ParticleLayoutForm.h
//! @brief     Defines class ParticleLayoutForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_PARTICLELAYOUTFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_PARTICLELAYOUTFORM_H

#include "GUI/View/Widget/GroupBoxes.h"
#include <QVector>

class DSpinBox;
class HeinzFormLayout;
class ItemWithParticles;
class LayerForm;
class LayerItem;
class ParticleLayoutItem;
class SampleEditorController;

//! Form for editing a particle layout
class ParticleLayoutForm : public CollapsibleGroupBox {
    Q_OBJECT
public:
    ParticleLayoutForm(LayerForm* form, ParticleLayoutItem* t, SampleEditorController* ec);

    ParticleLayoutItem* layoutItem() const { return m_layout_item; }
    void onParticleAdded(ItemWithParticles* t);
    void onAboutToRemoveParticle(ItemWithParticles* t);

    //! Disable/enable total density property depending on type of interference function.
    //!
    //! E.g. 2-dimensional interference calculates density automatically, so
    //! the property shown in the particle layout shall be disabled.
    void updateDensityEnabling();

    //! Update the shown density value.
    //!
    //! This does not update any internal values, it only refreshes the shown value.
    void updateDensityValue();

    void updateTitle(const LayerItem* layerItem);

private:
    HeinzFormLayout* m_layout;
    ParticleLayoutItem* m_layout_item;
    QAction* m_remove_action;
    QAction* m_duplicate_action;
    SampleEditorController* m_ec;
    DSpinBox* m_total_density_spin_box;
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_PARTICLELAYOUTFORM_H
