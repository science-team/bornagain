//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/PolyForm.cpp
//! @brief     Implements class PolyForm, except for templated functions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Sample/PolyForm.h"
#include "GUI/Model/Sample/FormfactorItems.h"
#include "GUI/Model/Sample/ProfileItems.h"
#include "GUI/Model/Sample/RotationItems.h"
#include "GUI/Model/Sample/RoughnessItems.h"
#include "GUI/View/Base/LayoutUtil.h"

void PolyForm::createContent()
{
    if (currentValues != nullptr)
        GUI::Util::Layer::addMultiPropertyToGrid(m_grid_layout, 1, currentValues(), m_ec, true);
}

DoubleProperties PolyForm::doublePropertiesOfItem(RotationItem* item)
{
    if (!item)
        return {};
    return item->rotationProperties();
}

DoubleProperties PolyForm::doublePropertiesOfItem(Profile2DItem* item)
{
    if (!item)
        return {};
    return item->profileProperties();
}

DoubleProperties PolyForm::doublePropertiesOfItem(Profile1DItem* item)
{
    if (!item)
        return {};
    return item->profileProperties();
}

DoubleProperties PolyForm::doublePropertiesOfItem(FormfactorItem* item)
{
    if (!item)
        return {};
    return item->geometryProperties();
}

DoubleProperties PolyForm::doublePropertiesOfItem(TransientItem*)
{
    return {};
}

DoubleProperties PolyForm::doublePropertiesOfItem(CrosscorrelationItem* item)
{
    if (!item)
        return {};
    return item->crossCorrProperties();
}
