//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/PolyForm.h
//! @brief     Defines class PolyForm, and implements templated functions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_POLYFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_POLYFORM_H

#include "GUI/View/Sample/ISelectionForm.h"
#include "GUI/View/Sample/LayerEditorUtil.h"

//! A widget to contain a selection, defined by a PolyItem.
//!
//! This PolyForm is limited to contain the selection combo box and a list of double
//! values represented by DoubleProperties. The list of DoubleProperties is queried by calling
//! GUI::Util::Layer::doublePropertiesOfItem(). To have the correct DoubleProperties on this form,
//! you may have to overload this method according to your class and your needs. The overload will
//! expect the template type you defined for PolyItem.
//!
//! Example:
//! PolyItem<XCatalog>  =>
//! GUI::Util::Layer::doublePropertiesOfItem(XItem*)
//!
//! The connection from selection combo -> PolyItem is made via
//! SampleEditorController::setCurrentIndex(), where a command can be used for undo purposes.
//!
//! For each DoubleProperty, a unit-aware DSpinBox is created. The connection from each
//! spinbox to the DoubleProperty is made via SampleEditorController::setDouble(), where a command
//! can be used for undo purposes.
//!
//! If a more complex selection shall be realized (e.g. with
//! sub-selections or different value types), this class is not sufficient.
class PolyForm : public ISelectionForm {
public:
    template <typename B, typename C>
    PolyForm(QWidget* parent, PolyPtr<B, C>& d, SampleEditorController* ec)
        : ISelectionForm(parent, ec)
    {
        currentValues = [&d] { return doublePropertiesOfItem(d.certainItem()); };
        initUI(d);
    }

    virtual void createContent();

private:
    static DoubleProperties doublePropertiesOfItem(class CrosscorrelationItem* item);
    static DoubleProperties doublePropertiesOfItem(class TransientItem* item);
    static DoubleProperties doublePropertiesOfItem(class FormfactorItem* item);
    static DoubleProperties doublePropertiesOfItem(class Profile1DItem* item);
    static DoubleProperties doublePropertiesOfItem(class Profile2DItem* item);
    static DoubleProperties doublePropertiesOfItem(class RotationItem* item);

    std::function<DoubleProperties()> currentValues = nullptr;
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_POLYFORM_H
