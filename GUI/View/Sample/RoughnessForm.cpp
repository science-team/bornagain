//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/RoughnessForm.cpp
//! @brief     Implements class RoughnessForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Sample/RoughnessForm.h"
#include "GUI/Model/Sample/RoughnessCatalog.h"
#include "GUI/Model/Sample/RoughnessItems.h"
#include "GUI/View/Sample/HeinzFormLayout.h"
#include <QLabel>

RoughnessForm::RoughnessForm(QWidget* parent,
                             PolyPtr<RoughnessItem, RoughnessCatalog>& roughnessSelection,
                             bool& expandRoughness, SampleEditorController* ec)
    : CollapsibleGroupBox(parent, expandRoughness)
    , m_cb(new QComboBox(this))
    , m_rs(roughnessSelection)
{
    m_layout = new HeinzFormLayout(ec);
    body()->setLayout(m_layout);

    WheelEventEater::install(m_cb);
    m_cb->addItems(m_rs.menuEntries());
    m_cb->setCurrentIndex(m_rs.certainIndex());
    m_cb->setMaxVisibleItems(m_cb->count());
    m_cb->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    m_layout->addBoldRow("Type:", m_cb);

    createRoughnessWidgets();
    updateTitle();

    connect(m_cb, &QComboBox::currentIndexChanged, [this](int newIndex) {
        m_rs.setCertainIndex(newIndex);
        onRoughnessTypeChanged();
        emit gDoc->sampleChanged();
    });
}

void RoughnessForm::updateTitle()
{
    setTitle("Roughness (" + m_cb->currentText() + ")");
}

void RoughnessForm::recreateWidgets()
{
    while (m_layout->rowCount() > 1)
        m_layout->removeRow(1);

    createRoughnessWidgets();
}

void RoughnessForm::markLayerAsSubstrate(bool is_substrate)
{
    m_is_substrate = is_substrate;
    recreateWidgets();
}

void RoughnessForm::onRoughnessTypeChanged()
{
    recreateWidgets();
    updateTitle();
}

void RoughnessForm::createRoughnessWidgets()
{
    RoughnessItem* roughness = m_rs.certainItem();

    if (auto* rsi = dynamic_cast<SelfAffineFractalRoughnessItem*>(roughness)) {
        m_layout->addGroupOfValues("Parameters", rsi->roughnessProperties());
        m_layout->addSelection(roughness->transientSelection());
        if (!m_is_substrate)
            m_layout->addSelection(roughness->crossrorrModelSelection());
    } else if (auto* rsi = dynamic_cast<LinearGrowthRoughnessItem*>(roughness)) {
        if (m_is_substrate) {
            m_layout->addRow(new QLabel(
                "The growth model cannot be used for the substrate. Please select another model."));
        } else {
            m_layout->addGroupOfValues("Parameters", rsi->roughnessProperties());
            m_layout->addSelection(roughness->transientSelection());
        }
    }
}
