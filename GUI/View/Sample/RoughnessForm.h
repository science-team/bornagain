//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/RoughnessForm.h
//! @brief     Defines class RoughnessForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_ROUGHNESSFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_ROUGHNESSFORM_H

#include "GUI/Model/Descriptor/PolyPtr.h"
#include "GUI/View/Widget/GroupBoxes.h"
#include <QComboBox>

class HeinzFormLayout;
class RoughnessCatalog;
class RoughnessItem;
class SampleEditorController;

class RoughnessForm : public CollapsibleGroupBox {
public:
    RoughnessForm(QWidget* parent, PolyPtr<RoughnessItem, RoughnessCatalog>& roughnessSelection,
                  bool& expandRoughness, SampleEditorController* ec);

    void onRoughnessTypeChanged();
    void recreateWidgets();
    void markLayerAsSubstrate(bool is_substrate);

private:
    void updateTitle();
    void createRoughnessWidgets();

    HeinzFormLayout* m_layout;
    QComboBox* m_cb;
    PolyPtr<RoughnessItem, RoughnessCatalog>& m_rs;
    bool m_is_substrate = false;
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_ROUGHNESSFORM_H
