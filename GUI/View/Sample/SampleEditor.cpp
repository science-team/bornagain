//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/SampleEditor.cpp
//! @brief     Implements class SampleEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Sample/SampleEditor.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include "GUI/Model/Sample/SampleItem.h"
#include "GUI/View/Sample/SampleEditorController.h"
#include "GUI/View/Sample/SampleForm.h"

SampleEditor::SampleEditor()
{
    QScrollArea::setWidgetResizable(true);
    QScrollArea::setWidget(new QWidget);
    setMinimumWidth(1000);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

SampleEditor::~SampleEditor() = default;

void SampleEditor::setCurrentSample(SampleItem* t)
{
    if (t == nullptr) {
        QScrollArea::setWidget(new QWidget);
        return;
    }

    m_edit_controller = std::make_unique<SampleEditorController>(t);
    auto* ec = m_edit_controller.get();
    connect(ec, &SampleEditorController::requestViewInRealspace, this,
            &SampleEditor::requestViewInRealspace);
    connect(ec, &SampleEditorController::aboutToRemoveItem, this, &SampleEditor::aboutToRemoveItem);

    t->updateDefaultLayerColors();

    auto* f = new SampleForm(t, ec);
    ec->setSampleForm(f);
    f->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    QScrollArea::setWidget(f);
}
