//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/SampleEditor.h
//! @brief     Defines class SampleEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_SAMPLEEDITOR_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_SAMPLEEDITOR_H

#include <QScrollArea>
#include <memory>

class Item3D;
class SampleEditorController;
class SampleItem;

//! Sample editor with layer oriented presentation of a sample
class SampleEditor : public QScrollArea {
    Q_OBJECT
public:
    SampleEditor();
    ~SampleEditor() override;

    void setCurrentSample(SampleItem* t);

signals:
    void requestViewInRealspace(Item3D* itemToShow);
    void aboutToRemoveItem(Item3D* item);

private:
    std::unique_ptr<SampleEditorController> m_edit_controller;
};


#endif // BORNAGAIN_GUI_VIEW_SAMPLE_SAMPLEEDITOR_H
