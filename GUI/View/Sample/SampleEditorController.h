//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/SampleEditorController.h
//! @brief     Defines class SampleEditorController.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_SAMPLEEDITORCONTROLLER_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_SAMPLEEDITORCONTROLLER_H

#include "GUI/Model/Sample/FormfactorCatalog.h"
#include "GUI/Model/Sample/ParticleCatalog.h"
#include <QObject>

class CompoundItem;
class CoreAndShellForm;
class InterferenceForm;
class InterferenceItem;
class Item3D;
class ItemWithLayers;
class ItemWithMaterial;
class ItemWithParticles;
class LayerForm;
class LayerStackForm;
class MaterialsSet;
class MesocrystalForm;
class ParticleLayoutItem;
class SampleForm;
class SampleItem;

//! Class to modify a sample from the layer oriented sample editor.
//!
//! Use this class to modify the sample model. It takes care of notifications.
//! It operates on one SampleItem.

class SampleEditorController : public QObject {
    Q_OBJECT
public:
    SampleEditorController(SampleItem* multi);

    //! Set the current form.
    //!
    //! The form can change e.g. when a different sample gets the current one
    //! in the layer editor. Also nullptr is allowed.
    void setSampleForm(SampleForm* view);

    //! The item on which this controller operates.
    SampleItem* sampleItem() const { return m_sample_item; }

    //! The materials of the current document
    MaterialsSet* materialModel() const;

    void addLayerItem(LayerStackForm& parentStackForm, const ItemWithLayers* before);
    void addLayerStackItem(LayerStackForm& parentStackForm, const ItemWithLayers* before);
    void duplicateItemWithLayers(const ItemWithLayers* component);
    void removeItemWithLayers(ItemWithLayers* component);
    void onStoppedToMoveComponent(QWidget* widgetToMove, QWidget* moveAboveThisWidget);

    void onParticleLayoutAdded(ParticleLayoutItem* layout, ItemWithParticles* newItem);
    void addParticleLayoutItem(ParticleLayoutItem* layout, ParticleCatalog::Type type);
    void addParticleLayoutItem(ParticleLayoutItem* layout, FormfactorCatalog::Type type);
    ParticleLayoutItem* parentLayoutItem(ItemWithParticles* item);

    void onParticleCompoundAdded(CompoundItem* composition, ItemWithParticles* newItem);
    void addCompoundItem(CompoundItem* composition, ParticleCatalog::Type type);
    void addCompoundItem(CompoundItem* composition, FormfactorCatalog::Type type);
    CompoundItem* parentCompoundItem(ItemWithParticles* item);

    void duplicateItemWithParticles(ItemWithParticles* item);
    void removeParticle(ItemWithParticles* item);

    void setCoreFormfactor(CoreAndShellForm* widget, FormfactorCatalog::Type type);
    void setShellFormfactor(CoreAndShellForm* widget, FormfactorCatalog::Type type);

    void setMesocrystalBasis(MesocrystalForm* widget, ParticleCatalog::Type type);
    void setMesocrystalBasis(MesocrystalForm* widget, FormfactorCatalog::Type type);
    void selectInterference(InterferenceForm* widget, int newIndex);

    void selectMaterial(ItemWithMaterial* item, const QString& newMaterialIdentifier);
    void setMaterialValue(ItemWithMaterial* item);

    void setDensityRelatedValue(InterferenceItem* interferenceItem);

    static void addLayoutItem(LayerForm* layerForm);
    static void duplicateLayoutItem(LayerForm* layerForm, ParticleLayoutItem* layout);
    void removeLayoutItem(LayerForm* layerForm, ParticleLayoutItem* layout);

signals:
    void requestViewInRealspace(Item3D* item);
    void aboutToRemoveItem(Item3D* item);

private:
    static void onLayoutAdded(LayerForm* layerForm, ParticleLayoutItem* layout);
    void onComponentAdded(ItemWithLayers* component);
    QColor findColor(size_t atIndex);

    ItemWithParticles* createAndInitItem(FormfactorCatalog::Type formFactorType) const;
    ItemWithParticles* createAndInitItem(ParticleCatalog::Type itemType) const;

    SampleItem* m_sample_item;
    SampleForm* m_sample_form;
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_SAMPLEEDITORCONTROLLER_H
