//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/SampleForm.cpp
//! @brief     Implements class SampleForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Sample/SampleForm.h"
#include "Base/Util/Vec.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include "GUI/Model/Sample/LayerItem.h"
#include "GUI/Model/Sample/LayerStackItem.h"
#include "GUI/Model/Sample/SamplesSet.h"
#include "GUI/View/Base/ActionFactory.h"
#include "GUI/View/Base/LayoutUtil.h"
#include "GUI/View/Sample/CompoundForm.h"
#include "GUI/View/Sample/CoreAndShellForm.h"
#include "GUI/View/Sample/HeinzFormLayout.h"
#include "GUI/View/Sample/LayerForm.h"
#include "GUI/View/Sample/LayerStackForm.h"
#include "GUI/View/Sample/MesocrystalForm.h"
#include "GUI/View/Sample/ParticleForm.h"
#include "GUI/View/Sample/ParticleLayoutForm.h"
#include "GUI/View/Sample/SampleEditorController.h"
#include "GUI/View/Widget/AppConfig.h"
#include "GUI/View/Widget/GroupBoxes.h"
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>

SampleForm::SampleForm(SampleItem* sampleItem, SampleEditorController* ec)
    : m_layout(new QVBoxLayout(this))
    , m_sample_item(sampleItem)
    , m_ec(ec)
{
    setAttribute(Qt::WA_StyledBackground, true);

    m_layout->setAlignment(Qt::AlignTop);

    auto* summary = new CollapsibleGroupBox("Summary", this, sampleItem->expandInfo);
    m_layout->addWidget(summary, 0, Qt::AlignTop);
    summary->setObjectName("SampleSummary");

    auto* gLayout = new HeinzFormLayout(ec);
    summary->body()->setLayout(gLayout);

    gLayout->setFieldGrowthPolicy(QFormLayout::ExpandingFieldsGrow);

    auto* nameEdit = new QLineEdit(sampleItem->name());
    gLayout->addBoldRow("Name:", nameEdit);

    nameEdit->setMaximumWidth(600);
    connect(nameEdit, &QLineEdit::textEdited, [](const QString& s) {
        gDoc->samplesRW()->setCurrentName(s);
        gDoc->setModified();
    });

    auto* descriptionEdit = new QTextEdit;
    gLayout->addBoldRow("Description:", descriptionEdit);
    descriptionEdit->setMaximumWidth(600);
    descriptionEdit->setFixedHeight(60);
    descriptionEdit->setAcceptRichText(false);
    descriptionEdit->setTabChangesFocus(true);
    descriptionEdit->setPlainText(sampleItem->description());
    connect(descriptionEdit, &QTextEdit::textChanged, [descriptionEdit] {
        gDoc->samplesRW()->setCurrentDescription(descriptionEdit->toPlainText());
        gDoc->setModified();
    });

    // Processing external field is not implemented yet, so temporary disable it (see issue #654)
    // m_layout->addVector(sampleItem->externalField(), false);

    auto* showInRealspaceAction = ActionFactory::createShowInRealspaceAction(
        this, "sample", [this] { m_ec->requestViewInRealspace(m_sample_item); });

    summary->addTitleAction(showInRealspaceAction);

    auto* outer_stack_form = new LayerStackForm(this, &m_sample_item->outerStackItem(), m_ec);
    outer_stack_form->setObjectName("OuterLayerStackForm"); // for control from stylesheet
    m_layout->addWidget(outer_stack_form, 0, Qt::AlignTop);
}

void SampleForm::updateRowVisibilities()
{
    for (auto* c : findChildren<LayerContainerForm*>())
        c->updatePositionDependentElements();
}

LayerStackForm* SampleForm::formOfStackItem(const LayerStackItem* searchedStackItem)
{
    for (auto* sf : findChildren<LayerStackForm*>())
        if (&sf->stackItem() == searchedStackItem)
            return sf;
    return nullptr;
}
