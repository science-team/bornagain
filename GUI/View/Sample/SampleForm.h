//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/SampleForm.h
//! @brief     Defines class SampleForm.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_SAMPLEFORM_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_SAMPLEFORM_H

#include <QWidget>

class LayerContainerForm;
class LayerStackForm;
class LayerStackItem;
class QVBoxLayout;
class SampleEditorController;
class SampleItem;

//! Form to present/edit a sample
class SampleForm : public QWidget {
    Q_OBJECT
public:
    SampleForm(SampleItem* sampleItem, SampleEditorController* ec);

    void updateRowVisibilities();
    LayerStackForm* formOfStackItem(const LayerStackItem* searchedStackItem);

private:
    QVBoxLayout* m_layout;
    SampleItem* m_sample_item;    //!< Ptr is borrowed, don't delete
    SampleEditorController* m_ec; //!< Ptr is borrowed, don't delete
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_SAMPLEFORM_H
