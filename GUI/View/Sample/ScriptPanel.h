//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Sample/ScriptPanel.h
//! @brief     Defines class ScriptPanel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SAMPLE_SCRIPTPANEL_H
#define BORNAGAIN_GUI_VIEW_SAMPLE_SCRIPTPANEL_H

#include <QHideEvent>
#include <QModelIndex>
#include <QShowEvent>
#include <QTextEdit>
#include <QWidget>

class CautionSign;
class PythonSyntaxHighlighter;
class SampleItem;
class UpdateTimer;

//! Resides at the bottom of SampleView and displays a Python script.

class ScriptPanel : public QWidget {
    Q_OBJECT
public:
    explicit ScriptPanel(QWidget* parent);

    void setCurrentSample(SampleItem* sampleItem);
    void onSampleModified();

private:
    void showEvent(QShowEvent*) override;
    void hideEvent(QHideEvent*) override;

    //! generates string representing code snippet for all multi layers in the model
    QString generateCodeSnippet();

    //! Update the editor with the script content
    void updateEditor();

    QTextEdit* m_text_edit;
    PythonSyntaxHighlighter* m_highlighter;
    UpdateTimer* m_update_timer;
    CautionSign* m_caution_sign;
    SampleItem* m_current_sample;
};

#endif // BORNAGAIN_GUI_VIEW_SAMPLE_SCRIPTPANEL_H
