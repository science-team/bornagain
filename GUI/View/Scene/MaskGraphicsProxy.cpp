//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Scene/MaskGraphicsProxy.cpp
//! @brief     Implements class MaskGraphicsProxy.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Scene/MaskGraphicsProxy.h"
#include <QGraphicsSceneMouseEvent>

MaskGraphicsProxy::MaskGraphicsProxy()
{
    setZooming(false);
}

MaskGraphicsProxy::~MaskGraphicsProxy() = default;

//! Sets widget to zoom mode, when signals (zoom wheel, mouse clicks) are send down to
//! ColorMap plot. In non-zoom mode, widget doesn't react on clicks.
void MaskGraphicsProxy::setZooming(bool zooming)
{
    m_zooming = zooming;
    setAcceptedMouseButtons(m_zooming ? Qt::LeftButton : Qt::NoButton);
}

void MaskGraphicsProxy::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if (!m_zooming)
        return;
    QGraphicsProxyWidget::mousePressEvent(event);
    event->accept(); // prevents propagation to parent widget
}

void MaskGraphicsProxy::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    if (!m_zooming)
        return;
    QGraphicsProxyWidget::mouseReleaseEvent(event);
}

void MaskGraphicsProxy::wheelEvent(QGraphicsSceneWheelEvent* event)
{
    if (!m_zooming)
        return;
    QGraphicsProxyWidget::wheelEvent(event);
}

void MaskGraphicsProxy::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    if (!m_zooming)
        return;
    QGraphicsProxyWidget::mouseMoveEvent(event);
}
