//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Scene/MaskGraphicsProxy.h
//! @brief     Defines class MaskGraphicsProxy.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SCENE_MASKGRAPHICSPROXY_H
#define BORNAGAIN_GUI_VIEW_SCENE_MASKGRAPHICSPROXY_H

#include <QGraphicsProxyWidget>

//! Used by MaskGraphicsScene to embed the ColorMap.
//!
//! A QGraphicsProxyWidget embeds a QWidget-based widget into a QGraphicsScene.
//!
//! The hierarchy MaskGraphicsScene > MaskGraphicsProxy > ColorMap is constructed
//! by calls of QGraphicsProxyWidget functions setParent and setWidget.
//!
//! In zooming mode, mouse events are send to the ColorMap widget.
//! In non-zooming mode, they are ignored (so that they can be processed elsewhere).

class MaskGraphicsProxy : public QGraphicsProxyWidget {
    Q_OBJECT
public:
    MaskGraphicsProxy();
    ~MaskGraphicsProxy() override;

    void setZooming(bool zooming);

private:
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
    void wheelEvent(QGraphicsSceneWheelEvent* event) override;

    bool m_zooming;
};

#endif // BORNAGAIN_GUI_VIEW_SCENE_MASKGRAPHICSPROXY_H
