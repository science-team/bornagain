//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Scene/MaskGraphicsView.cpp
//! @brief     Implements class MaskGraphicsView.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Scene/MaskGraphicsView.h"
#include "GUI/View/Scene/MaskGraphicsScene.h"
#include <QScrollBar>
#include <QTransform>

namespace {

const double min_zoom_value = 1.0;
const double max_zoom_value = 5.0;
const double zoom_step = 0.05;

} // namespace


MaskGraphicsView::MaskGraphicsView(MaskGraphicsScene* scene, QWidget* parent)
    : QGraphicsView(scene, parent)
    , m_scene(scene)
    , m_current_zoom_value(1.0)
{
    setObjectName("MaskGraphicsView");
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    setMouseTracking(true);
}

//! Reset given view to original zoom state. Also asks graphics scene to do the same with color map.
void MaskGraphicsView::onResetViewRequest()
{
    setZoomValue(1.0);
}

void MaskGraphicsView::wheelEvent(QWheelEvent* event)
{
    // hold control button
    if (isControlButtonIsPressed(event)) {
        centerOn(mapToScene(event->position().toPoint()));
        if (event->angleDelta().y() > 0) {
            // Zoom in
            increaseZoomValue();
        } else {
            // Zooming out
            if (horizontalScrollBar()->isVisible() || verticalScrollBar()->isVisible())
                decreaseZoomValue();
        }
    } else {
        QGraphicsView::wheelEvent(event);
    }
}

//! On resize event changes scene size and MaskGraphicsProxy so they would get the size of viewport
void MaskGraphicsView::resizeEvent(QResizeEvent* event)
{
    QWidget::resizeEvent(event);
    m_scene->updateSize(event->size());
}

void MaskGraphicsView::keyPressEvent(QKeyEvent* event)
{
    switch (event->key()) {
    case Qt::Key_Left:
        break;
    case Qt::Key_Space:
        if (!event->isAutoRepeat())
            emit changeActivityRequest(Canvas2DMode::PAN_ZOOM);
        break;
    case Qt::Key_Escape:
        m_scene->cancelCurrentDrawing();
        break;
    case Qt::Key_Delete:
    case Qt::Key_Backspace:
        emit deleteCurrentRequest();
        break;
    default:
        QWidget::keyPressEvent(event);
    }
}

bool MaskGraphicsView::isControlButtonIsPressed(QWheelEvent* event)
{
    return event->modifiers().testFlag(Qt::ControlModifier);
}

void MaskGraphicsView::setZoomValue(double zoom_value)
{
    if (zoom_value == m_current_zoom_value)
        return;
    const QTransform oldMatrix = transform();
    resetTransform();
    translate(oldMatrix.dx(), oldMatrix.dy());
    scale(zoom_value, zoom_value);
    m_current_zoom_value = zoom_value;
}

void MaskGraphicsView::decreaseZoomValue()
{
    double zoom_value = m_current_zoom_value - zoom_step;
    if (zoom_value < min_zoom_value)
        zoom_value = min_zoom_value;
    setZoomValue(zoom_value);
}

void MaskGraphicsView::increaseZoomValue()
{
    double zoom_value = m_current_zoom_value + zoom_step;
    if (zoom_value > max_zoom_value)
        zoom_value = max_zoom_value;
    setZoomValue(zoom_value);
}
