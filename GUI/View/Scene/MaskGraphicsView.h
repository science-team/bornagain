//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Scene/MaskGraphicsView.h
//! @brief     Defines class MaskGraphicsView.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SCENE_MASKGRAPHICSVIEW_H
#define BORNAGAIN_GUI_VIEW_SCENE_MASKGRAPHICSVIEW_H

#include "GUI/View/Setup/Canvas2DMode.h"
#include <QGraphicsView>
#include <QWheelEvent>

class MaskGraphicsScene;

//! Graphics view for MaskEditorCanvas

class MaskGraphicsView : public QGraphicsView {
    Q_OBJECT
public:
    MaskGraphicsView(MaskGraphicsScene* scene, QWidget* parent = nullptr);
    QSize sizeHint() const override { return {512, 512}; }

signals:
    void changeActivityRequest(Canvas2DMode::Flag);
    void deleteCurrentRequest();

public slots:
    void onResetViewRequest();

private:
    void wheelEvent(QWheelEvent* event) override;
    void resizeEvent(QResizeEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;

    bool isControlButtonIsPressed(QWheelEvent* event);

    void setZoomValue(double zoom_value);
    void decreaseZoomValue();
    void increaseZoomValue();

    MaskGraphicsScene* m_scene;
    double m_current_zoom_value;
};

#endif // BORNAGAIN_GUI_VIEW_SCENE_MASKGRAPHICSVIEW_H
