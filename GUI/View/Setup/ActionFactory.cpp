//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/ActionFactory.cpp
//! @brief     Implement namespace ActionFactory.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Setup/ActionFactory.h"

QAction* ActionFactory::createCopyAction(const QString& type, QObject* parent)
{
    auto* result = new QAction("Copy", parent);
    result->setIcon(QIcon(":/images/content-copy.svg"));
    result->setToolTip("Clone current " + type);
    return result;
}

QAction* ActionFactory::createRemoveAction(const QString& type, QObject* parent)
{
    auto* result = new QAction("Remove", parent);
    result->setIcon(QIcon(":/images/delete.svg"));
    result->setToolTip("Remove current " + type);
    return result;
}
