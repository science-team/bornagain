//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/ActionFactory.h
//! @brief     Defines namespace ActionFactory.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SETUP_FRAMEACTIONS_H
#define BORNAGAIN_GUI_VIEW_SETUP_FRAMEACTIONS_H

#include <QAction>
#include <QString>

namespace ActionFactory {

QAction* createCopyAction(const QString& type, QObject* parent = nullptr);
QAction* createRemoveAction(const QString& type, QObject* parent = nullptr);

} // namespace ActionFactory

#endif // BORNAGAIN_GUI_VIEW_SETUP_FRAMEINSTRUMENTS_H
