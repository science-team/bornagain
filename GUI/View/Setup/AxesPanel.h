//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/AxesPanel.h
//! @brief     Defines class AxesPanel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SETUP_AXESPANEL_H
#define BORNAGAIN_GUI_VIEW_SETUP_AXESPANEL_H

#include <QFormLayout>
#include <QWidget>

class Data2DItem;
class DataSource;

//! Widget to edit properties of a Data2DItem.

class AxesPanel : public QWidget {
public:
    AxesPanel(const DataSource*);
    ~AxesPanel();

private:
    void updatePanel();
    void updateAxesProperties();

    Data2DItem* d2Item();

    const DataSource* m_data_source;
    QFormLayout* m_editor_layout;
};

#endif // BORNAGAIN_GUI_VIEW_SETUP_AXESPANEL_H
