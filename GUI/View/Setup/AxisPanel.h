//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/AxisPanel.h
//! @brief     Defines class AxisPanel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SETUP_AXISPANEL_H
#define BORNAGAIN_GUI_VIEW_SETUP_AXISPANEL_H

#include <QFormLayout>
#include <QWidget>

class Data1DItem;
class DataItem;
class DataSource;

//! Widget to edit properties of a Data1DItem.

class AxisPanel : public QWidget {
public:
    AxisPanel(const DataSource*);
    ~AxisPanel();

private:
    void updatePanel();
    void updateAxesProperties();

    Data1DItem* d1Item();

    const DataSource* m_data_source;
    QFormLayout* m_editor_layout;
};

#endif // BORNAGAIN_GUI_VIEW_SETUP_AXISPANEL_H
