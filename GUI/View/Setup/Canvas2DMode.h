//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/Canvas2DMode.h
//! @brief     Defines namespace Canvas2DMode.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SETUP_CANVAS2DMODE_H
#define BORNAGAIN_GUI_VIEW_SETUP_CANVAS2DMODE_H

//! Various flags for MaskEditor operation

namespace Canvas2DMode {

enum Flag {
    SELECTION,
    PAN_ZOOM,
    RECTANGLE,
    POLYGON,
    VERTICAL_PRJN,
    HORIZONTAL_PRJN,
    VERTICAL_MASK,
    HORIZONTAL_MASK,
    ELLIPSE,
    ROI,
    MASKALL,
};

inline bool basedOnRectangle(Flag m)
{
    return m == RECTANGLE || m == ELLIPSE || m == ROI;
}

inline bool isMask(Flag m)
{
    return m == RECTANGLE || m == POLYGON || m == VERTICAL_MASK || m == HORIZONTAL_MASK
           || m == ELLIPSE || m == ROI || m == MASKALL;
}

inline bool isLineMask(Flag m)
{
    return m == VERTICAL_MASK || m == HORIZONTAL_MASK;
}

inline bool isPrjn(Flag m)
{
    return m == VERTICAL_PRJN || m == HORIZONTAL_PRJN;
}

inline bool isHorizontalLine(Flag m)
{
    return m == HORIZONTAL_PRJN || m == HORIZONTAL_MASK;
}

inline bool isVerticalLine(Flag m)
{
    return m == VERTICAL_PRJN || m == VERTICAL_MASK;
}

inline bool isLineMode(Flag m)
{
    return isHorizontalLine(m) || isVerticalLine(m);
}


} // namespace Canvas2DMode

#endif // BORNAGAIN_GUI_VIEW_SETUP_CANVAS2DMODE_H
