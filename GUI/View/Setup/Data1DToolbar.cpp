//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/Data1DToolbar.cpp
//! @brief     Implements class Data1DToolbar.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Setup/Data1DToolbar.h"
#include "GUI/View/Base/mainwindow_constants.h"
#include "GUI/View/Setup/FrameActions.h"
#include <QToolButton>

Data1DToolbar::Data1DToolbar()
{
    //... Frame tools

    auto* panelButton = new QToolButton;
    panelButton->setDefaultAction(actions()->toggle_properties_panel);
    addWidget(panelButton);

    addSeparator();

    //... Zoom mode

    // Zoom mode is always on; therefore we only provide a "reset" button
    auto* resetButton = new QToolButton;
    resetButton->setDefaultAction(actions()->reset_view);
    addWidget(resetButton);

    addSeparator();

    //... Export

    auto* saveButton = new QToolButton;
    saveButton->setDefaultAction(actions()->save_plot);
    addWidget(saveButton);
}
