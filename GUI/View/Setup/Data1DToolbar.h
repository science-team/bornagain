//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/Data1DToolbar.h
//! @brief     Defines class Data1DToolbar.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SETUP_DATA1DTOOLBAR_H
#define BORNAGAIN_GUI_VIEW_SETUP_DATA1DTOOLBAR_H

#include "GUI/View/Setup/DataToolbar.h"

//! Toolbar with icons of tools for manipulating 1D plots.

class Data1DToolbar : public DataToolbar {
    Q_OBJECT
public:
    Data1DToolbar();
};

#endif // BORNAGAIN_GUI_VIEW_SETUP_DATA1DTOOLBAR_H
