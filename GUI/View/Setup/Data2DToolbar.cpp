//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/Data2DToolbar.cpp
//! @brief     Implements class Data2DToolbar.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Setup/Data2DToolbar.h"
#include "GUI/View/Base/mainwindow_constants.h"
#include "GUI/View/Setup/FrameActions.h"
#include <QLabel>
#include <QToolButton>

Data2DToolbar::Data2DToolbar(bool mask_tools, bool proj_tools)
    : m_activity_button_group(new QButtonGroup)
{
    //... Frame tools

    auto* propertyPanel_button = new QToolButton;
    propertyPanel_button->setDefaultAction(actions()->toggle_properties_panel);
    addWidget(propertyPanel_button);

    addSeparator();

    if (mask_tools || proj_tools) {
        //... Selection / zoom mode

        auto* selection_button = new QToolButton;
        selection_button->setIcon(QIcon(":/images/mask/arrow.svg"));
        selection_button->setToolTip("Select/modify mask");
        selection_button->setCheckable(true);
        selection_button->setChecked(true);
        addWidget(selection_button);
        m_activity_button_group->addButton(selection_button, Canvas2DMode::SELECTION);

        auto* pan_button = new QToolButton;
        pan_button->setIcon(QIcon(":/images/hand-right.svg"));
        pan_button->setToolTip("Pan/zoom mode (space)\n"
                               "Drag axes with the mouse, use mouse wheel to zoom in/out");
        pan_button->setCheckable(true);
        addWidget(pan_button);
        m_activity_button_group->addButton(pan_button, Canvas2DMode::PAN_ZOOM);
    }

    auto* resetView_button = new QToolButton;
    resetView_button->setDefaultAction(actions()->reset_view);
    addWidget(resetView_button);

    addSeparator();

    if (proj_tools) {
        //... Projection tools

        auto* horizontal_prjn_button = new QToolButton;
        horizontal_prjn_button->setIcon(QIcon(":/images/mask/horizontalprojection.svg"));
        horizontal_prjn_button->setToolTip("Create horizontal projection");
        horizontal_prjn_button->setCheckable(true);
        m_activity_button_group->addButton(horizontal_prjn_button, Canvas2DMode::HORIZONTAL_PRJN);
        addWidget(horizontal_prjn_button);

        auto* vertical_prjn_button = new QToolButton;
        vertical_prjn_button->setIcon(QIcon(":/images/mask/verticalprojection.svg"));
        vertical_prjn_button->setToolTip("Create vertical projection");
        vertical_prjn_button->setCheckable(true);
        m_activity_button_group->addButton(vertical_prjn_button, Canvas2DMode::VERTICAL_PRJN);
        addWidget(vertical_prjn_button);

        addSeparator();
    }
    if (mask_tools) {
        //... Shape creation tools

        auto* roi_button = new QToolButton;
        roi_button->setIcon(QIcon(":/images/mask/roi.svg"));
        roi_button->setToolTip("Create region of interest");
        roi_button->setCheckable(true);
        m_activity_button_group->addButton(roi_button, Canvas2DMode::ROI);
        addWidget(roi_button);

        //    auto* maskAll_button = new QToolButton;
        //    maskAll_button->setIcon(QIcon(":/images/mask/maskall.svg"));
        //    maskAll_button->setToolTip("Create masked area covering whole detector plane\n"
        //                              "Will be placed beneath all masks. Only one instance is
        //                              allowed.");
        //    maskAll_button->setCheckable(true);
        //    m_activity_button_group->addButton(maskAll_button, Canvas2DMode::MASKALL);
        //    addWidget(maskAll_button);

        auto* rectangle_button = new QToolButton;
        rectangle_button->setIcon(QIcon(":/images/mask/rectangle.svg"));
        rectangle_button->setToolTip("Create rectangle mask");
        rectangle_button->setCheckable(true);
        m_activity_button_group->addButton(rectangle_button, Canvas2DMode::RECTANGLE);
        addWidget(rectangle_button);

        auto* polygon_button = new QToolButton;
        polygon_button->setIcon(QIcon(":/images/mask/polygon.svg"));
        polygon_button->setToolTip("Create polygon mask");
        m_activity_button_group->addButton(polygon_button, Canvas2DMode::POLYGON);
        polygon_button->setCheckable(true);
        addWidget(polygon_button);

        auto* ellipse_button = new QToolButton;
        ellipse_button->setIcon(QIcon(":/images/mask/ellipse.svg"));
        ellipse_button->setToolTip("Create ellipse mask");
        ellipse_button->setCheckable(true);
        m_activity_button_group->addButton(ellipse_button, Canvas2DMode::ELLIPSE);
        addWidget(ellipse_button);

        auto* horizontal_mask_button = new QToolButton;
        horizontal_mask_button->setIcon(QIcon(":/images/mask/horizontalmask.svg"));
        horizontal_mask_button->setToolTip("Create horizontal line mask");
        horizontal_mask_button->setCheckable(true);
        m_activity_button_group->addButton(horizontal_mask_button, Canvas2DMode::HORIZONTAL_MASK);
        addWidget(horizontal_mask_button);

        auto* vertical_mask_button = new QToolButton;
        vertical_mask_button->setIcon(QIcon(":/images/mask/verticalmask.svg"));
        vertical_mask_button->setToolTip("Create vertical line mask");
        vertical_mask_button->setCheckable(true);
        m_activity_button_group->addButton(vertical_mask_button, Canvas2DMode::VERTICAL_MASK);
        addWidget(vertical_mask_button);

        addSeparator();

        //... Modifier and inspector tools

        auto* raise_button = new QToolButton;
        addWidget(raise_button);
        raise_button->setDefaultAction(actions()->raise_mask);

        auto* lower_button = new QToolButton;
        addWidget(lower_button);
        lower_button->setDefaultAction(actions()->lower_mask);

        addSeparator();

        auto* mask_display_button = new QToolButton;
        mask_display_button->setIcon(QIcon(":/images/mask/lightbulb.svg"));
        mask_display_button->setToolTip("Press and hold to see mask results.");
        addWidget(mask_display_button);

        connect(mask_display_button, &QToolButton::pressed,
                [this] { emit requestMaskDisplay(true); });
        connect(mask_display_button, &QToolButton::released,
                [this] { emit requestMaskDisplay(false); });

        addSeparator();
    }
    connect(m_activity_button_group, &QButtonGroup::idClicked,
            [this] { emit activityChanged(currentActivity()); });

    auto* save_2D_button = new QToolButton;
    save_2D_button->setDefaultAction(actions()->save_plot);
    addWidget(save_2D_button);

    if (proj_tools) {
        auto* save_proj_button = new QToolButton;
        save_proj_button->setDefaultAction(actions()->save_projections);
        addWidget(save_proj_button);
    }
}

void Data2DToolbar::onChangeActivityRequest(Canvas2DMode::Flag mode)
{
    m_activity_button_group->button(mode)->setChecked(true);
    emit activityChanged(currentActivity());
}

//! Change activity only if current activity is one of drawing mode (horizontal, vertical
//! projections drawing).
void Data2DToolbar::onProjectionTabChange(Canvas2DMode::Flag mode)
{
    if (currentActivity() == Canvas2DMode::HORIZONTAL_PRJN
        || currentActivity() == Canvas2DMode::VERTICAL_PRJN)
        onChangeActivityRequest(mode);
}

Canvas2DMode::Flag Data2DToolbar::currentActivity() const
{
    return Canvas2DMode::Flag(m_activity_button_group->checkedId());
}
