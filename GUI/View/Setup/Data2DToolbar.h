//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/Data2DToolbar.h
//! @brief     Defines class Data2DToolbar.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SETUP_DATA2DTOOLBAR_H
#define BORNAGAIN_GUI_VIEW_SETUP_DATA2DTOOLBAR_H

#include "GUI/View/Setup/Canvas2DMode.h"
#include "GUI/View/Setup/DataToolbar.h"
#include <QButtonGroup>

class FrameActions;

//! Toolbar with icons of tools for manipulating 2D plots with masks and projections.

class Data2DToolbar : public DataToolbar {
    Q_OBJECT
public:
    Data2DToolbar(bool mask_tools, bool proj_tools);

public slots:
    void onChangeActivityRequest(Canvas2DMode::Flag mode);
    void onProjectionTabChange(Canvas2DMode::Flag mode);

signals:
    void activityChanged(Canvas2DMode::Flag);
    void requestMaskDisplay(bool);

private:
    Canvas2DMode::Flag currentActivity() const;

    QButtonGroup* m_activity_button_group;
};

#endif // BORNAGAIN_GUI_VIEW_SETUP_DATA2DTOOLBAR_H
