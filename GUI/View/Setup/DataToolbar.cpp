//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/DataToolbar.cpp
//! @brief     Implements class DataToolbar.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Setup/DataToolbar.h"
#include "GUI/View/Setup/FrameActions.h"

DataToolbar::DataToolbar()
    : m_actions(std::make_unique<FrameActions>())
{
    setOrientation(Qt::Vertical);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
    setStyleSheet("QToolBar{border-left:1px solid rgb(180,180,180);} "
                  "::separator{background:lightgray;height:0px;"
                  "margin-top:25px;margin-bottom:25px;};");
    setIconSize(QSize(32, 32));
}

DataToolbar::~DataToolbar() = default;
