//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/DataToolbar.h
//! @brief     Defines class DataToolbar.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SETUP_DATATOOLBAR_H
#define BORNAGAIN_GUI_VIEW_SETUP_DATATOOLBAR_H

#include <QToolBar>
#include <memory>

class FrameActions;

//! Iconic representation of tools for 1D plots

class DataToolbar : public QToolBar {
    Q_OBJECT
public:
    DataToolbar();
    virtual ~DataToolbar();

    FrameActions* actions() const { return m_actions.get(); }

private:
    std::unique_ptr<FrameActions> m_actions;
};

#endif // BORNAGAIN_GUI_VIEW_SETUP_DATATOOLBAR_H
