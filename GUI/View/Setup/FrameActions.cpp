//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/FrameActions.cpp
//! @brief     Implements class FrameActions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Setup/FrameActions.h"

FrameActions::FrameActions()
{
    raise_mask = new QAction("Rise mask up");
    raise_mask->setIcon(QIcon(":/images/mask/bringtofront.svg"));
    raise_mask->setToolTip("Rise selected mask one level up (PgUp)");
    raise_mask->setShortcuts(QKeySequence::MoveToPreviousPage);

    lower_mask = new QAction("Lower mask down");
    lower_mask->setIcon(QIcon(":/images/mask/sendtoback.svg"));
    lower_mask->setToolTip("Lower selected mask one level down (PgDown)");
    lower_mask->setShortcuts(QKeySequence::MoveToNextPage);

    reset_view = new QAction("Reset view");
    reset_view->setIcon(QIcon(":/images/camera-metering-center.svg"));
    reset_view->setToolTip("Reset view: all axes will be set to default");

    save_plot = new QAction("Save");
    save_plot->setIcon(QIcon(":/images/content-save-outline.svg"));
    save_plot->setToolTip("Save Plot");

    save_projections = new QAction("Save projections");
    save_projections->setIcon(QIcon(":/images/table-export.svg"));
    save_projections->setToolTip("Save projections plot");

    toggle_properties_panel = new QAction("Properties");
    toggle_properties_panel->setIcon(QIcon(":/images/dock-right.svg"));
    toggle_properties_panel->setToolTip("Toggle properties panel");
    toggle_properties_panel->setCheckable(true);

    delete_mask = new QAction("Delete mask");
    delete_mask->setIcon(QIcon(":/images/mask/maskall.svg"));
    delete_mask->setToolTip("Delete selected mask (Del/Backspace)");
    delete_mask->setShortcuts(QKeySequence::Delete);
}
