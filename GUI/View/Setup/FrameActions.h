//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/FrameActions.h
//! @brief     Defines class FrameActions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SETUP_FRAMEACTIONS_H
#define BORNAGAIN_GUI_VIEW_SETUP_FRAMEACTIONS_H

#include <QAction>

class FrameActions {
public:
    FrameActions();

    QAction* raise_mask;
    QAction* lower_mask;
    QAction* reset_view;
    QAction* save_plot;
    QAction* save_projections;
    QAction* toggle_properties_panel;
    QAction* delete_mask;
};

#endif // BORNAGAIN_GUI_VIEW_SETUP_FRAMEINSTRUMENTS_H
