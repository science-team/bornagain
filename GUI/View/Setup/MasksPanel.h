//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Setup/MasksPanel.h
//! @brief     Defines class MasksPanel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_SETUP_MASKSPANEL_H
#define BORNAGAIN_GUI_VIEW_SETUP_MASKSPANEL_H

#include <QFormLayout>
#include <QWidget>
#include <functional>

class MaskItem;
class MasksSet;
class SetView;

//! Panel with list of masks and parameter editor for one selected mask.

class MasksPanel : public QWidget {
    Q_OBJECT
public:
    MasksPanel(const std::function<MasksSet*()>& set_source, bool data_not_job, bool mask_not_prjn);

    void updateMasksPanel();

signals:
    void deleteCurrentRequest();

private:
    //! Set the current mask and creates the UI to edit the mask's properties
    void updateMaskEditor();
    void updateMaskPresentationProperties(MaskItem* t);

    std::function<MasksSet*()> m_set_source;
    SetView* m_set_view;
    QFormLayout* m_editor_layout;
    bool m_data_not_job;
    bool m_mask_not_prjn;
};

#endif // BORNAGAIN_GUI_VIEW_SETUP_MASKSPANEL_H
