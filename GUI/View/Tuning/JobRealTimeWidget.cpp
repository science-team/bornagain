//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Tuning/JobRealTimeWidget.cpp
//! @brief     Implements class JobRealTimeWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Tuning/JobRealTimeWidget.h"
#include "GUI/Model/Job/BatchInfo.h"
#include "GUI/Model/Job/JobItem.h"
#include "GUI/Model/Job/JobStatus.h"
#include "GUI/Model/Job/JobsSet.h"
#include "GUI/View/Base/mainwindow_constants.h"
#include "GUI/View/Tuning/ParameterTuningStackPresenter.h"
#include "GUI/View/Tuning/ParameterTuningWidget.h"
#include <QSettings>
#include <QVBoxLayout>

JobRealTimeWidget::JobRealTimeWidget(JobsSet* jobs, QWidget* parent)
    : QWidget(parent)
    , m_stacked_widget(new ParameterTuningStackPresenter)
    , m_jobs(jobs)
{
    setWindowTitle("Job Real Time");
    setObjectName("JobRealTimeWidget");
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

    auto* mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(m_stacked_widget);

    setFixedWidth(GUI::Style::FIT_ACTIVITY_PANEL_WIDTH);
    applySettings();
}

void JobRealTimeWidget::resizeEvent(QResizeEvent* event)
{
    QWidget::resizeEvent(event);
    emit widthChanged(width());
    setMinimumWidth(0);
    setMaximumWidth(QWIDGETSIZE_MAX);
}

ParameterTuningWidget* JobRealTimeWidget::parameterTuningWidget()
{
    return m_stacked_widget->itemWidget();
}

void JobRealTimeWidget::setJobItem(JobItem* jobItem)
{
    if (!isValidJobItem(jobItem)) {
        m_stacked_widget->hide();
        return;
    }
    m_stacked_widget->setItem(jobItem, m_jobs);
}

//! Returns true if JobItem is valid for real time simulation.

bool JobRealTimeWidget::isValidJobItem(JobItem* item)
{
    return item && isOver(item->batchInfo()->status());
}

void JobRealTimeWidget::applySettings()
{
    QSettings settings;
    if (settings.childGroups().contains(GUI::Style::S_FIT_ACTIVITY_PANEL)) {
        settings.beginGroup(GUI::Style::S_FIT_ACTIVITY_PANEL);
        setFixedWidth(settings.value(GUI::Style::S_FIT_ACTIVITY_PANEL_SIZE).toSize().width());
        settings.endGroup();
    }
}
