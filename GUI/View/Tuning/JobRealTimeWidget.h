//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Tuning/JobRealTimeWidget.h
//! @brief     Defines class JobRealTimeWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_TUNING_JOBREALTIMEWIDGET_H
#define BORNAGAIN_GUI_VIEW_TUNING_JOBREALTIMEWIDGET_H

#include <QWidget>

class JobItem;
class JobsSet;
class ParameterTuningStackPresenter;
class ParameterTuningWidget;

//! The JobRealTimeWidget class provides tuning of sample parameters in real time.
//! Located on the right side of JobView and is visible when realtime activity is selected.

class JobRealTimeWidget : public QWidget {
    Q_OBJECT
public:
    JobRealTimeWidget(JobsSet* jobs, QWidget* parent = nullptr);

    ParameterTuningWidget* parameterTuningWidget();

    void resizeEvent(QResizeEvent* event) override;
    void setJobItem(JobItem* jobItem);

signals:
    void widthChanged(int width);

private:
    bool isValidJobItem(JobItem* item);
    void applySettings();

    ParameterTuningStackPresenter* m_stacked_widget;
    JobsSet* m_jobs;
};

#endif // BORNAGAIN_GUI_VIEW_TUNING_JOBREALTIMEWIDGET_H
