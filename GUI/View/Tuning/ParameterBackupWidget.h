//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Tuning/ParameterBackupWidget.h
//! @brief     Defines class ParameterBackupWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_TUNING_PARAMETERBACKUPWIDGET_H
#define BORNAGAIN_GUI_VIEW_TUNING_PARAMETERBACKUPWIDGET_H

#include <QComboBox>
#include <QPushButton>
#include <QWidget>

class ParameterContainerItem;

//! Provides tools to create, remove and apply parameter tree backups.

class ParameterBackupWidget : public QWidget {
    Q_OBJECT
public:
    explicit ParameterBackupWidget(QWidget* parent = nullptr);

    void setParameterContainer(ParameterContainerItem* container);

signals:
    void backupSwitched(int index);

private slots:
    void onComboChange(int index);

private:
    void fillCombo();

    QPushButton* m_create;
    QComboBox* m_combo;
    QPushButton* m_remove;
    QPushButton* m_reset;
    ParameterContainerItem* m_container;
};

#endif // BORNAGAIN_GUI_VIEW_TUNING_PARAMETERBACKUPWIDGET_H
