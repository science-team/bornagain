//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Tuning/ParameterTuningStackPresenter.cpp
//! @brief     Implements class ParameterTuningStackPresenter.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Tuning/ParameterTuningStackPresenter.h"
#include "GUI/View/Tuning/ParameterTuningWidget.h"

void ParameterTuningStackPresenter::setItem(JobItem* item, QObject* model)
{
    if (!item) {
        hide();
        return;
    }

    ParameterTuningWidget* widget;
    auto it = m_item_to_widget.find(item);
    if (it != m_item_to_widget.end()) {
        widget = it.value();
    } else {
        widget = new ParameterTuningWidget;
        addWidget(widget);
        m_item_to_widget[item] = widget;
    }

    setCurrentWidget(widget);
    widget->show();
    show();

    widget->setModel(model);
    widget->setJobItem(item);
}

ParameterTuningWidget* ParameterTuningStackPresenter::itemWidget()
{
    if (m_item_to_widget.empty())
        return nullptr;
    return dynamic_cast<ParameterTuningWidget*>(currentWidget());
}
