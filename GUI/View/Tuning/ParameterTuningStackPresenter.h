//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Tuning/ParameterTuningStackPresenter.h
//! @brief     Defines class ParameterTuningStackPresenter.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_TUNING_PARAMETERTUNINGSTACKPRESENTER_H
#define BORNAGAIN_GUI_VIEW_TUNING_PARAMETERTUNINGSTACKPRESENTER_H

#include <QMap>
#include <QStackedWidget>

class JobItem;
class ParameterTuningWidget;

class ParameterTuningStackPresenter : public QStackedWidget {
public:
    //! Shows the widget for given item (and hides previous one).
    //! If no widget yet exists, it will be created.
    void setItem(JobItem* item, QObject* model = nullptr);

    ParameterTuningWidget* itemWidget();

private:
    QMap<JobItem*, ParameterTuningWidget*> m_item_to_widget;
};

#endif // BORNAGAIN_GUI_VIEW_TUNING_PARAMETERTUNINGSTACKPRESENTER_H
