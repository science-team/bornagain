//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Tuning/ParameterTuningWidget.h
//! @brief     Defines class ParameterTuningWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_TUNING_PARAMETERTUNINGWIDGET_H
#define BORNAGAIN_GUI_VIEW_TUNING_PARAMETERTUNINGWIDGET_H

#include <QItemSelectionModel>
#include <QTreeView>
#include <QWidget>

class CautionSign;
class JobItem;
class JobsSet;
class ParameterBackupWidget;
class ParameterItem;
class ParameterTuningDelegate;
class PartunerQModel;
class SliderEditor;

enum class JobStatus;

//! Main widget for real time parameter tuning.
//! Contains a tree for parameter tuning and the model to provide drag-and-drop in FitActivityPanel.

class ParameterTuningWidget : public QWidget {
    Q_OBJECT
public:
    ParameterTuningWidget();
    ~ParameterTuningWidget();

    void setJobItem(JobItem* job_item);
    void setModel(QObject* jobs);
    QItemSelectionModel* selectionModel();

    QVector<ParameterItem*> selectedParameterItems();

signals:
    void itemContextMenuRequest(const QPoint& point);

public slots:
    void onCurrentLinkChanged(ParameterItem* item);
    void onSliderRangeChanged(int value);
    void onLockArgValueChanged(bool value);
    void onLockValValueChanged(bool value);
    void restoreModelsOfCurrentJobItem(int index);
    void makeSelected(ParameterItem* item);
    void updateView();

private slots:
    void updateParameterModel();
    void onCustomContextMenuRequested(const QPoint& point);
    void onJobDestroyed();
    void onSectionResized(int i, int, int n);

private:
    void contextMenuEvent(QContextMenuEvent*) override;
    void updateDragAndDropSettings();
    void setTuningDelegateEnabled(bool enabled);
    void closeActiveEditors();
    void updateJobStatus(const JobStatus status);
    void saveSettings();
    void applySettings();

    JobItem* m_job_item;
    JobsSet* m_jobs;
    std::unique_ptr<PartunerQModel> m_parameter_tuning_model;
    ParameterBackupWidget* m_backup_widget;
    SliderEditor* m_slider_settings_widget;
    QTreeView* m_tree_view;
    ParameterTuningDelegate* m_delegate;
    CautionSign* m_caution_sign;
};

#endif // BORNAGAIN_GUI_VIEW_TUNING_PARAMETERTUNINGWIDGET_H
