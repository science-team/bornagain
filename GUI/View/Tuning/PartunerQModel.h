//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Tuning/PartunerQModel.h
//! @brief     Defines class PartunerQModel.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_TUNING_PARTUNERQMODEL_H
#define BORNAGAIN_GUI_VIEW_TUNING_PARTUNERQMODEL_H

class ParameterItem;
class ParameterLabelItem;

#include <QAbstractItemModel>

//! Represents parameters which can be tuned in real time in ParameterTuningWidget.
//! In the fitting activity context handles dragging of ParameterItem's to the FitParametersWidget.

class PartunerQModel : public QAbstractItemModel {
    Q_OBJECT
public:
    PartunerQModel(QObject* rootObject, QObject* parent = nullptr);

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role /* = Qt::DisplayRole */) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    QMimeData* mimeData(const QModelIndexList& indexes) const override;
    Qt::DropActions supportedDragActions() const override;
    Qt::DropActions supportedDropActions() const override;

    //! Returns ParameterItem from given index
    ParameterItem* getParameterItem(const QModelIndex& index) const;
    QModelIndex indexForItem(ParameterItem* item) const;

    static ParameterItem* toParameterItem(const QModelIndex& index);
    static ParameterLabelItem* toParameterLabelItem(const QModelIndex& index);

    void setExpanded(const QModelIndex& index) const;
    void setCollapsed(const QModelIndex& index) const;

private:
    QVariant parentColor(const QModelIndex& index) const;
    QObject* m_root_object;
};

#endif // BORNAGAIN_GUI_VIEW_TUNING_PARTUNERQMODEL_H
