//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Tuning/SliderEditor.cpp
//! @brief     Implements class SliderEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Tuning/SliderEditor.h"
#include "Base/Util/Assert.h"
#include "GUI/Model/Data/Data1DItem.h"
#include "GUI/Model/Data/Data2DItem.h"
#include "GUI/Model/Job/JobItem.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include <QLabel>
#include <QVBoxLayout>

SliderEditor::SliderEditor()
{
    // tuning selectors
    QString tooltip("Allows to tune sample parameters within +/- of given range \nwith the help of "
                    "the slider.");

    auto* label = new QLabel("Tuning:");
    label->setToolTip(tooltip);

    m_radio1 = new QRadioButton("10%");
    m_radio1->setAutoExclusive(true);
    m_radio1->setToolTip(tooltip);
    connect(m_radio1, &QRadioButton::clicked, this, &SliderEditor::rangeChanged);

    m_radio2 = new QRadioButton("100%");
    m_radio2->setChecked(true);
    m_radio2->setAutoExclusive(true);
    m_radio2->setToolTip(tooltip);
    connect(m_radio2, &QRadioButton::clicked, this, &SliderEditor::rangeChanged);

    m_radio3 = new QRadioButton("1000%");
    m_radio3->setAutoExclusive(true);
    m_radio3->setToolTip(tooltip);
    connect(m_radio3, &QRadioButton::clicked, this, &SliderEditor::rangeChanged);

    // Fix argument axes
    m_lock_arg_check_box = new QCheckBox("Lock arg range");
    m_lock_arg_check_box->setToolTip(
        "Preserve (min, max) ranges of view area during parameter tuning.");
    connect(m_lock_arg_check_box, &QCheckBox::stateChanged, this, &SliderEditor::lockArgChanged);

    // Fix value axis
    m_lock_val_check_box = new QCheckBox("Lock val range");
    m_lock_val_check_box->setToolTip(
        "Preserve (min, max) range of intensity axis during parameter tuning.");
    connect(m_lock_val_check_box, &QCheckBox::stateChanged, this, &SliderEditor::lockValChanged);

    auto main_layout = new QVBoxLayout(this);

    auto* hbox1 = new QHBoxLayout;
    hbox1->setAlignment(Qt::AlignLeft);
    main_layout->addLayout(hbox1);

    hbox1->addWidget(label);
    hbox1->addWidget(m_radio1);
    hbox1->addWidget(m_radio2);
    hbox1->addWidget(m_radio3);

    auto* hbox2 = new QHBoxLayout;
    hbox2->setAlignment(Qt::AlignLeft);
    main_layout->addLayout(hbox2);

    hbox2->addWidget(m_lock_arg_check_box);
    hbox2->addWidget(m_lock_val_check_box);
}

void SliderEditor::onJobDestroyed()
{
    m_job_item = nullptr;
}

void SliderEditor::setJobItem(JobItem* job_item)
{
    m_job_item = job_item;
    if (!m_job_item)
        return;

    connect(m_job_item, &QObject::destroyed, this, &SliderEditor::onJobDestroyed,
            Qt::UniqueConnection);

    initSlider();
    initLockers();
}

void SliderEditor::rangeChanged()
{
    if (!m_job_item)
        return;

    if (m_radio1->isChecked())
        m_slider_range = 10;
    else if (m_radio2->isChecked())
        m_slider_range = 100;
    else if (m_radio3->isChecked())
        m_slider_range = 1000;

    emit sliderRangeFactorChanged(m_slider_range);
    gDoc->setModified();
}

void SliderEditor::initSlider()
{
    ASSERT(m_job_item);

    QSignalBlocker b1(m_radio1);
    QSignalBlocker b2(m_radio2);
    QSignalBlocker b3(m_radio3);

    if (m_slider_range == 10)
        m_radio1->setChecked(true);
    else if (m_slider_range == 1000)
        m_radio3->setChecked(true);
    else {
        m_slider_range = 100;
        m_radio2->setChecked(true);
    }
    emit sliderRangeFactorChanged(m_slider_range);
}

void SliderEditor::initLockers()
{
    ASSERT(m_job_item);
    DataItem* data_item = m_job_item->simulatedDataItem();
    ASSERT(data_item);

    QSignalBlocker b1(m_lock_arg_check_box);
    m_lock_arg_check_box->setChecked(data_item->isArgRangeLocked());

    QSignalBlocker b2(m_lock_val_check_box);
    m_lock_val_check_box->setChecked(data_item->isValAxisLocked());
}
