//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Tuning/SliderEditor.h
//! @brief     Defines class SliderEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_TUNING_SLIDEREDITOR_H
#define BORNAGAIN_GUI_VIEW_TUNING_SLIDEREDITOR_H

#include <QCheckBox>
#include <QRadioButton>
#include <QWidget>

class JobItem;

class SliderEditor : public QWidget {
    Q_OBJECT
public:
    SliderEditor();

    void setJobItem(JobItem* job_item);

signals:
    void sliderRangeFactorChanged(int value);
    void lockValChanged(bool value);
    void lockArgChanged(bool value);

private slots:
    void rangeChanged();
    void onJobDestroyed();

private:
    void initSlider();
    void initLockers();

private:
    QRadioButton* m_radio1;
    QRadioButton* m_radio2;
    QRadioButton* m_radio3;
    QCheckBox* m_lock_val_check_box;
    QCheckBox* m_lock_arg_check_box;
    JobItem* m_job_item;
    int m_slider_range = 100;
};

#endif // BORNAGAIN_GUI_VIEW_TUNING_SLIDEREDITOR_H
