//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/View/DataView.cpp
//! @brief     Implements class DataView.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/View/DataView.h"
#include "GUI/View/Frame/StackedFrames.h"
#include "GUI/View/View/DatafilesSelector.h"
#include <QSplitter>
#include <QVBoxLayout>

DataView::DataView()
{
    auto* stacked_frames = new StackedDataFrames;
    auto* selector = new DatafilesSelector;

    auto* splitter = new QSplitter;
    splitter->addWidget(selector);
    splitter->addWidget(stacked_frames);
    splitter->setCollapsible(0, false);
    splitter->setCollapsible(1, false);

    auto* layout = new QVBoxLayout(this);
    layout->addWidget(splitter);
}
