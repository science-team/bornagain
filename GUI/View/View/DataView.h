//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/View/DataView.h
//! @brief     Defines class DataView.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_VIEW_DATAVIEW_H
#define BORNAGAIN_GUI_VIEW_VIEW_DATAVIEW_H

#include <QWidget>

//! The DataView class is a main view for importing experimental data.

class DataView : public QWidget {
    Q_OBJECT
public:
    DataView();
};

#endif // BORNAGAIN_GUI_VIEW_VIEW_DATAVIEW_H
