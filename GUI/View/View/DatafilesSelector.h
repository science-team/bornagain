//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/View/DatafilesSelector.h
//! @brief     Defines class DatafilesSelector.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_VIEW_DATAFILESSELECTOR_H
#define BORNAGAIN_GUI_VIEW_VIEW_DATAFILESSELECTOR_H

#include <QWidget>

class DatafilesSet;
class QToolBar;
class SetView;

//! A list of loaded datafiles, shown as left panel in DataView.

class DatafilesSelector : public QWidget {
    Q_OBJECT
public:
    DatafilesSelector();

private:
    void createActions(QToolBar* toolbar);
    void updateActions();

    DatafilesSet* m_set;
    SetView* m_qlistview;

    QAction* m_rm_action;
};

#endif // BORNAGAIN_GUI_VIEW_VIEW_DATAFILESSELECTOR_H
