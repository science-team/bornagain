//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/View/InstrumentView.h
//! @brief     Defines class InstrumentView.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_VIEW_INSTRUMENTVIEW_H
#define BORNAGAIN_GUI_VIEW_VIEW_INSTRUMENTVIEW_H

#include <QAction>
#include <QScrollArea>
#include <QWidget>

class InstrumentItem;
class InstrumentsSet;
class QToolBar;
class SetView;

class InstrumentView : public QWidget {
    Q_OBJECT
public:
    InstrumentView();

private:
    void createActions(QToolBar* toolbar);
    void updateActions();
    void updateEditor();
    QWidget* createEditor(InstrumentItem*);

    InstrumentsSet* m_set;
    SetView* m_qlistview;
    QScrollArea* m_scroll_area;
    InstrumentItem* m_displayed_item = nullptr;

    QAction* m_rm_action;
    QAction* m_cp_action;
    QAction* m_load_action;
    QAction* m_adapt_action;
    QAction* m_save_action;
};

#endif // BORNAGAIN_GUI_VIEW_VIEW_INSTRUMENTVIEW_H
