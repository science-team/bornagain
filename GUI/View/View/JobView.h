//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/View/JobView.h
//! @brief     Defines class JobView.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_VIEW_JOBVIEW_H
#define BORNAGAIN_GUI_VIEW_VIEW_JOBVIEW_H

#include <QActionGroup>
#include <QMainWindow>
#include <QProgressBar>

enum class JobViewActivity;

class DocksController;
class FitActivityPanel;
class JobItem;
class JobMessagePanel;
class JobRealTimeWidget;
class JobView;
class JobsPanel;
class StackedJobFrames;

//! The JobView class is a main view to show list of jobs, job results and widgets for real time
//! and fitting activities.

class JobView : public QMainWindow {
    Q_OBJECT
public:
    JobView(QProgressBar* progressBar);

    void fillViewMenu(QMenu* menu);

signals:
    void requestSwitchToJobView();

public slots:
    void switchWidgetsToJob();

private:
    JobItem* selectedJobItem();
    void setActivity(JobViewActivity activity);
    void switchToNewJob(JobItem* job_item);

    void resetLayout();

    DocksController* m_docks;

    JobsPanel* m_jobs_panel;
    StackedJobFrames* m_data_frames;
    JobRealTimeWidget* m_job_real_time_widget;
    FitActivityPanel* m_fit_activity_panel;
    JobMessagePanel* m_job_message_panel;

    QActionGroup m_activity_actions;
};

#endif // BORNAGAIN_GUI_VIEW_VIEW_JOBVIEW_H
