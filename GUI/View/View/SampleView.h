//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/View/SampleView.h
//! @brief     Defines class SampleView.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_VIEW_SAMPLEVIEW_H
#define BORNAGAIN_GUI_VIEW_VIEW_SAMPLEVIEW_H

#include <QAction>
#include <QWidget>

class Item3D;
class QToolBar;
class RealspacePanel;
class SamplesSet;
class SetView;

class SampleView : public QWidget {
    Q_OBJECT
public:
    SampleView();
    ~SampleView();

private:
    void createActions(QToolBar* toolbar);
    void updateActions();
    void onRequestViewInRealspace(Item3D* item);

    void loadExample(const QString& example_name);
    void onAboutToRemoveItem(Item3D* item);

    void applySplitterPos();
    void saveSplitterPos();

    SamplesSet* m_set;
    SetView* m_qlistview;
    RealspacePanel* m_realspace_panel;

    QAction* m_rm_action;
    QAction* m_cp_action;
};

#endif // BORNAGAIN_GUI_VIEW_VIEW_SAMPLEVIEW_H
