//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/View/SimulationView.h
//! @brief     Defines class SimulationView.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_VIEW_SIMULATIONVIEW_H
#define BORNAGAIN_GUI_VIEW_VIEW_SIMULATIONVIEW_H

#include <QCheckBox>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>
#include <QSpinBox>

class CollapsibleGroupBox;
class DSpinBox;
class DatafileItem;
class InstrumentItem;
class SampleItem;
class SimulationOptionsItem;

//! Widget to define a simulation.
//! Contains:
//! * Elements to select instrument, sample and real data.
//! * Elements to set options like number of threads.
//! * Buttons to run simulation or to export as a Python script
class SimulationView : public QWidget {
    Q_OBJECT
public:
    SimulationView();

    void simulate();

private:
    void exportPythonScript();

    void updateOptions();

    //! Update enabling of elements depending on other elements.
    void updateEnabling();

    QLabel* m_instrument_label;

    CollapsibleGroupBox* m_settings;

    CollapsibleGroupBox* m_advanced_settings;

    QRadioButton* m_analytical_radio;
    QRadioButton* m_monte_carlo_radio;
    QSpinBox* m_number_of_monte_carlo_points;

    QRadioButton* m_real_sum;
    QRadioButton* m_fourier_sum;
    DSpinBox* m_meso_radius_factor;

    QCheckBox* m_use_avge_material_check;

    QCheckBox* m_include_specular_check;

    QRadioButton* m_run_policy_immediately_radio;
    QRadioButton* m_run_policy_background_radio;
    QComboBox* m_number_of_threads_combo;

    QLabel* m_current_instrument_name;
    QLabel* m_current_sample_name;
    QLabel* m_current_data_name;
    QCheckBox* m_use_dataset;

    QPushButton* m_simulate_button;
    QPushButton* m_export_to_py_script_button;

    // Convenience method for easier access
    SimulationOptionsItem* optionsItem() const;
};

#endif // BORNAGAIN_GUI_VIEW_VIEW_SIMULATIONVIEW_H
