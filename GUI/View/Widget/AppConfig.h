//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/AppConfig.h
//! @brief     Defines class AppConfig.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_WIDGET_APPCONFIG_H
#define BORNAGAIN_GUI_VIEW_WIDGET_APPCONFIG_H

#include "Wrap/WinDllMacros.h"
#include <QObject>
#include <QString>

class ComboProperty;
class QCPColorGradient;
class Widget;

//! Application-wide settings.

class AppConfig : public QObject {
    Q_OBJECT
public:
    AppConfig();
    ~AppConfig();

    QWidget* mainWindow; //!< Pointer to _the_ MainWindow

    std::unique_ptr<ComboProperty> color_gradient_combo;

    bool autosave_enabled;

    QString import_filter_1D; //!< Last used import filter for 1D files
    QString import_filter_2D; //!< Last used import filter for 2D files

    QString artifact_export_dir; //!< Last used directory to save image, script, ...
    QString data_import_dir;     //!< Last data were imported from this directory
    QString script_export_dir;   //!< Last script export was to this directory
    QString script_import_dir;   //!< Last script import was from this directory
    QString xml_dir;             //!< Last directory for component IO

    void saveSettings();

    QCPColorGradient currentColorGradient() const;

signals:
    void gradientChanged();

private:
    void loadSettings();
};

BA_GUI_API_ extern std::unique_ptr<AppConfig> gApp; //!< global pointer to _the_ instance

#endif // BORNAGAIN_GUI_VIEW_WIDGET_APPCONFIG_H
