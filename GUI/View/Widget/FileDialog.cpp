//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/FileDialog.cpp
//! @brief     Implements functions in namespace GUI::FileDialog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Widget/FileDialog.h"
#include "GUI/View/Widget/AppConfig.h"
#include <QFileDialog>
#include <QStandardPaths>

QString GUI::FileDialog::w1_1f(const QString& caption, QString& dirname, const QString& name_filter,
                               const QString& suggested_name)
{
    if (dirname.isEmpty())
        dirname = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    QString default_name = dirname + "/" + suggested_name;

    QString fname =
        QFileDialog::getSaveFileName(gApp->mainWindow, caption, default_name, name_filter);
    if (fname.isEmpty())
        return "";

    dirname = QDir(std::filesystem::path(fname.toStdString()).parent_path()).absolutePath();

    return fname;
}
