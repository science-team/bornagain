//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/FileDialog.h
//! @brief     Defines functions in namespace GUI::FileDialog.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_WIDGET_FILEDIALOG_H
#define BORNAGAIN_GUI_VIEW_WIDGET_FILEDIALOG_H

#include <QString>

namespace GUI::FileDialog {

//! Choose one file for writing.
//! File is either new, or will be overwritten.
//! A fixed name filter is given.
//! Returns file name; may modify the default directory.
QString w1_1f(const QString& caption, QString& dirname, const QString& name_filter,
              const QString& suggested_name = {});

} // namespace GUI::FileDialog

#endif // BORNAGAIN_GUI_VIEW_WIDGET_FILEDIALOG_H
