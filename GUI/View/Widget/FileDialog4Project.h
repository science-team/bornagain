//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/FileDialog4Project.h
//! @brief     Defines class FileDialog4Project.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_WIDGET_FILEDIALOG4PROJECT_H
#define BORNAGAIN_GUI_VIEW_WIDGET_FILEDIALOG4PROJECT_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>

//! new project dialog window
class FileDialog4Project : public QDialog {
    Q_OBJECT
public:
    FileDialog4Project(QWidget* parent, const QString& workingDirectory = "",
                       const QString& projectName = "");

    QString getWorkingDirectory() const;
    void setWorkingDirectory(const QString& text);

    QString getProjectFileName() const;

private slots:
    void onBrowseDirectory();
    void checkIfProjectPathIsValid(const QString& dirname);
    void checkIfProjectNameIsValid(const QString& projectName);
    void createProjectDir();

private:
    QString getProjectName() const { return m_project_name_edit->text(); }

    void setValidProjectName(bool status);
    void setValidProjectPath(bool status);
    void updateWarningStatus();

    QLineEdit* m_project_name_edit;
    QLineEdit* m_work_dir_edit;
    QPushButton* m_browse_button;
    QLabel* m_warning_label;
    QPushButton* m_cancel_button;
    QPushButton* m_create_button;

    bool m_valid_project_name;
    bool m_valid_project_path;
};

#endif // BORNAGAIN_GUI_VIEW_WIDGET_FILEDIALOG4PROJECT_H
