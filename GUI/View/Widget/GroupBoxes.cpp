//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/GroupBoxes.cpp
//! @brief     Implements class GroupBoxCollapser.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Widget/GroupBoxes.h"
#include <QBoxLayout>
#include <QLabel>
#include <QMenu>

//  ************************************************************************************************
//  class StaticGroupBox
//  ************************************************************************************************

StaticGroupBox::StaticGroupBox(const QString& title, QWidget* parent)
    : QssWidget(parent)
    , m_body(new QWidget)
{
    auto* layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    auto* label = new QLabel;
    layout->setMenuBar(label);
    label->setObjectName("GroupBoxToggler");
    label->setText(title);
    label->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

    layout->addWidget(m_body);
}

//  ************************************************************************************************
//  class CollapsibleGroupBox
//  ************************************************************************************************

CollapsibleGroupBox::CollapsibleGroupBox(QWidget* parent, bool& expanded)
    : QssWidget(parent)
    , m_title_widget(new QWidget)
    , m_title_layout(new QHBoxLayout(m_title_widget))
    , m_toggle_button(new QToolButton)
    , m_body(new QWidget(this))
{
    auto* layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    //... Header widget

    layout->setMenuBar(m_title_widget);
    m_title_widget->setObjectName("GroupBoxTitleWidget");
    m_title_widget->setAttribute(Qt::WA_StyledBackground, true);

    m_title_layout->setContentsMargins(1, 1, 1, 1);
    m_title_layout->setSpacing(3);
    m_title_layout->setAlignment(Qt::AlignVCenter);

    m_title_layout->addWidget(m_toggle_button);
    m_toggle_button->setObjectName("GroupBoxToggler");
    m_toggle_button->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    m_toggle_button->setCheckable(true);
    m_toggle_button->setArrowType(Qt::ArrowType::DownArrow);
    m_toggle_button->setChecked(expanded);
    m_toggle_button->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

    //... Body

    layout->addWidget(m_body);
    m_body->setVisible(expanded);
    m_toggle_button->setArrowType(expanded ? Qt::ArrowType::DownArrow : Qt::ArrowType::RightArrow);

    connect(m_toggle_button, &QAbstractButton::clicked,
            [tb = m_toggle_button, bo = m_body, &expanded](bool checked) {
                tb->setArrowType(checked ? Qt::ArrowType::DownArrow : Qt::ArrowType::RightArrow);
                bo->setVisible(checked);
                expanded = checked;
            });
}

CollapsibleGroupBox::CollapsibleGroupBox(const QString& title, QWidget* parent, bool& expanded)
    : CollapsibleGroupBox(parent, expanded)
{
    setTitle(title);
}

void CollapsibleGroupBox::setTitle(const QString& title)
{
    m_toggle_button->setText(title);
}

void CollapsibleGroupBox::addTitleAction(QAction* action)
{
    auto* btn = new QToolButton;
    m_title_layout->addWidget(btn);

    btn->setToolButtonStyle(Qt::ToolButtonIconOnly);
    btn->setDefaultAction(action);
    if (action->menu() != nullptr)
        btn->setPopupMode(QToolButton::InstantPopup);

    connect(action, &QAction::changed, [=] { btn->setVisible(action->isVisible()); });
}

void CollapsibleGroupBox::addTitleWidget(QWidget* widget)
{
    m_title_layout->addWidget(widget);
}

void CollapsibleGroupBox::setExpanded(bool expanded)
{
    if (m_toggle_button->isChecked() != expanded)
        m_toggle_button->click();
}
