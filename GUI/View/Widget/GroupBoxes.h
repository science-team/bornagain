//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/GroupBoxes.h
//! @brief     Defines class GroupBoxCollapser.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_WIDGET_GROUPBOXES_H
#define BORNAGAIN_GUI_VIEW_WIDGET_GROUPBOXES_H

#include "GUI/View/Widget/QssWidget.h"
#include <QAction>
#include <QToolButton>

class QHBoxLayout;
class QssWidget;

//! A group box with given title and layout, and some standard styling.

class StaticGroupBox : public QssWidget {
    Q_OBJECT
public:
    StaticGroupBox(const QString& title, QWidget* parent);
    QWidget* body() { return m_body; }

private:
    QWidget* m_body;
};

//! A group box that can be expanded or collapsed, according to external boolean flag 'expanded'.

class CollapsibleGroupBox : public QssWidget {
    Q_OBJECT
public:
    CollapsibleGroupBox(QWidget* parent, bool& expanded);
    CollapsibleGroupBox(const QString& title, QWidget* parent, bool& expanded);
    QWidget* body() { return m_body; }
    void setTitle(const QString& title);
    void addTitleAction(QAction* action);
    void addTitleWidget(QWidget* widget);
    void setExpanded(bool expanded);

protected:
    QWidget* m_title_widget; //!< widget used to present the new groupbox title
private:
    QHBoxLayout* m_title_layout;  //!< layout in the title widget
    QToolButton* m_toggle_button; //!< button to toggle between collapsed/expanded
    QWidget* m_body;
};

#endif // BORNAGAIN_GUI_VIEW_WIDGET_GROUPBOXES_H
