//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/IComponentEditor.h
//! @brief     Defines class IComponentEditor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_WIDGET_ICOMPONENTEDITOR_H
#define BORNAGAIN_GUI_VIEW_WIDGET_ICOMPONENTEDITOR_H

#include <QWidget>

//! Base class for model component editors

class IComponentEditor : public QWidget {
    Q_OBJECT
public:
    IComponentEditor();
};

#endif // BORNAGAIN_GUI_VIEW_WIDGET_ICOMPONENTEDITOR_H
