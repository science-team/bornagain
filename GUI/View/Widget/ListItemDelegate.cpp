//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/ListItemDelegate.cpp
//! @brief     Implements class ListItemDelegate.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Widget/ListItemDelegate.h"
#include "Base/Util/Assert.h"
#include <QPainter>
#include <QTextDocument>

namespace {

QSize mySizeHint(const QString& text)
{
    QTextDocument doc;
    doc.setHtml(text);
    doc.setTextWidth(10000 /*options.rect.width()*/);
    QSize size = QSize(doc.idealWidth(), doc.size().height());
    return size;
}

} // namespace


ListItemDelegate::ListItemDelegate(QObject* parent)
    : QStyledItemDelegate(parent)
{
}

void ListItemDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option,
                             const QModelIndex& index) const
{
    QStyleOptionViewItem options = option;
    initStyleOption(&options, index);

    QStyledItemDelegate::paint(painter, option, index);

    painter->save();
    painter->setPen(QPen(Qt::lightGray, 1));
    painter->drawLine(options.rect.left(), options.rect.bottom(), options.rect.right(),
                      options.rect.bottom());
    painter->restore();
}

QSize ListItemDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    ASSERT(index.isValid());
    int h = QStyledItemDelegate::sizeHint(option, index).height();

    QStyleOptionViewItem options = option;
    initStyleOption(&options, index);
    auto s2 = mySizeHint(options.text);

    h = std::max(h, 32);
    h = std::max(h, s2.height() + 10);

    return {s2.width() + h, h};
}
