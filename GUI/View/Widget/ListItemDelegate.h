//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/ListItemDelegate.h
//! @brief     Defines class ListItemDelegate.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_WIDGET_LISTITEMDELEGATE_H
#define BORNAGAIN_GUI_VIEW_WIDGET_LISTITEMDELEGATE_H

#include <QStyledItemDelegate>

//! Styling for items in list.
//!
//! We need this not least because a long-standing bug [1] in Qt prevents us from
//! using the style sheet to fine-tune the item layout.
//! Specifically, in a style sheet stance like QAbstractItemView::item,
//! the padding-left property is ignored unless there is also a border property.
//! However, as soon as there is a border, the style sheet takes control over colors
//! so that the BackgroundRole in the model's data() function is ignored.
//!
//! [1] reported anew as https://bugreports.qt.io/browse/QTBUG-122698

class ListItemDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    ListItemDelegate(QObject* parent);

protected:
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const override;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

#endif // BORNAGAIN_GUI_VIEW_WIDGET_LISTITEMDELEGATE_H
