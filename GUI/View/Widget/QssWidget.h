//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/QssWidget.h
//! @brief     Defines class QssWidget.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_WIDGET_QSSWIDGET_H
#define BORNAGAIN_GUI_VIEW_WIDGET_QSSWIDGET_H

#include <QWidget>

//! Base class to create custom widgets customizable via stylesheets.

class QssWidget : public QWidget {
    Q_OBJECT
public:
    explicit QssWidget(QWidget* parent = nullptr);

private:
    void paintEvent(QPaintEvent*) override;
};

#endif // BORNAGAIN_GUI_VIEW_WIDGET_QSSWIDGET_H
