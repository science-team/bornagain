//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/StyledToolbar.cpp
//! @brief     Implements class StyledToolbar.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Widget/StyledToolbar.h"
#include <QLabel>
#include <QStyle>

StyledToolbar::StyledToolbar(QWidget* parent)
    : QToolBar(parent)
{
    setMovable(false);
    const int size = style()->pixelMetric(QStyle::PM_ToolBarIconSize);
    setIconSize(QSize(size, size));
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
}

void StyledToolbar::contextMenuEvent(QContextMenuEvent*)
{
    // Context menu reimplemented to suppress the default one
}
void StyledToolbar::paintEvent(QPaintEvent*)
{
    // Paint event reimplemented to suppress drawing the MacOs Menu
}
