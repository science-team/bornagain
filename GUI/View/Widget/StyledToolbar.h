//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/StyledToolbar.h
//! @brief     Defines class StyledToolbar.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_WIDGET_STYLEDTOOLBAR_H
#define BORNAGAIN_GUI_VIEW_WIDGET_STYLEDTOOLBAR_H

#include <QToolBar>

//! The StyledToolbar class represents our standard narrow toolbar with the height 24 pixels.

class StyledToolbar : public QToolBar {
    Q_OBJECT
public:
    explicit StyledToolbar(QWidget* parent = nullptr);

protected:
    void paintEvent(QPaintEvent* event) override;
    void contextMenuEvent(QContextMenuEvent*) override;
};

#endif // BORNAGAIN_GUI_VIEW_WIDGET_STYLEDTOOLBAR_H
