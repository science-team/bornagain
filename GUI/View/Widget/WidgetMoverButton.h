//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/WidgetMoverButton.h
//! @brief     Defines class WidgetMoverButton.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_WIDGET_WIDGETMOVERBUTTON_H
#define BORNAGAIN_GUI_VIEW_WIDGET_WIDGETMOVERBUTTON_H

#include <QMap>
#include <QPropertyAnimation>
#include <QScrollArea>
#include <QTimer>
#include <QToolButton>

//! Button to move a widget vertically in a layout.
//!
//! Used for the "move layer" feature in the sample editor.
//!
//! When the button is pressed, the parent layout of the widget is deactivated, and the widget is
//! moved to the position where the pressed mouse cursor is moved.
//! When the mouse is released, the layout is reactivated and signal finishedMoving() is
//! emitted.
//! The caller remains responsible for reordering the widgets - no reordering is done in here.

class WidgetMoverButton : public QToolButton {
    Q_OBJECT
public:
    //! Create a widget mover button.
    //!
    //! If the widget shall not be able to be dragged on the top position, this can be defined by
    //! ignoreOnTop (the number of widgets at the top of the layout which shall not be affected by
    //! the reordering). In the case of the layer moving, the topmost Form (the sample
    //! properties) shall not be part of reordering.
    WidgetMoverButton(QWidget* parent, QWidget* widgetToMove, int ignoreOnTop = 0);

protected:
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;

signals:
    void startingToMove();
    void finishedMoving(QWidget* widgetToMove, QWidget* moveAboveThisWidget);

private:
    void scrollParent(bool up);

    bool m_pressed = false;
    bool m_started = false;
    int m_global_mouse_downY;
    QLayout* m_layout_to_deactivate = nullptr;
    QWidget* m_widget_to_move;
    QWidget* m_drop_above_this_widget;
    int m_original_widgetY;
    int m_y_of_first_relevant_widget;
    int m_ignore_on_top;
    QTimer m_drag_scroll_timer;
    QScrollArea* m_scroll_area;
    QPoint m_hot_spot; //!< The mouse-down coordinates in the widget to move
    QMap<QWidget*, QPropertyAnimation*> m_animations;
};


#endif // BORNAGAIN_GUI_VIEW_WIDGET_WIDGETMOVERBUTTON_H
