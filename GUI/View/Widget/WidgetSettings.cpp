//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/WidgetSettings.cpp
//! @brief     Implements namespace WidgetSettings
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/View/Widget/WidgetSettings.h"
#include "Base/Util/Assert.h"
#include <QSettings>

namespace {

const QString S_SIZE = "Size";
const QString S_POS = "Pos";

} // namespace

void GUI::WidgetSettings::save(const QWidget* w)
{
    ASSERT(!w->objectName().isEmpty());
    QSettings settings;
    settings.setValue(S_SIZE + "/" + w->objectName(), w->size());
    settings.setValue(S_POS + "/" + w->objectName(), w->pos());
}

void GUI::WidgetSettings::load(QWidget* w)
{
    ASSERT(!w->objectName().isEmpty());

    QSettings settings;
    const QSize size = settings.value(S_SIZE + "/" + w->objectName(), QSize()).toSize();
    if (size.isValid())
        w->resize(size);
    if (settings.contains(S_POS + "/" + w->objectName()))
        w->move(settings.value(S_POS + "/" + w->objectName()).toPoint());
}
