//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/View/Widget/WidgetSettings.h
//! @brief     Defines namespace WidgetSettings
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_VIEW_WIDGET_WIDGETSETTINGS_H
#define BORNAGAIN_GUI_VIEW_WIDGET_WIDGETSETTINGS_H

#include <QWidget>

namespace GUI::WidgetSettings {

void save(const QWidget* w);
void load(QWidget* w);

} // namespace GUI::WidgetSettings

#endif // BORNAGAIN_GUI_VIEW_WIDGET_WIDGETSETTINGS_H
