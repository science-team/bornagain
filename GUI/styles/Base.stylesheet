/* Base.stylesheet serves as the base for all other stylesheets.
   It defines the shapes and colors for all controls.
   The other stylesheets are used for minor color adjustments that are
   specific to the theme and cannot be applied by the theme color palette. */

/* Set text and background color of all widgets. */
*
{
    color: palette(text);
    background-color: palette(base);
}

/* ------------------------------------------------------------------------------- */
QWidget#GroupBoxToggler
{
    background-color: transparent;
}
QWidget#GroupBoxTitleWidget,
QWidget#GroupBoxToggler
{
    background-color: palette(light);
    border-radius: 3px;
}
QWidget#GroupBoxTitleWidget:hover
{
    background-color: palette(mid);
}
QWidget#SampleSummary
{
    border: 0px;
}

/* ------------------------------------------------------------------------------- */
QToolTip
{
    background-color: palette(tooltipbase);
}

/* ------------------------------------------------------------------------------- */
QLabel
{
    background-color: transparent;
    padding: 2px;
}

/* Set highlight color for pseudo states of all buttons. */
QAbstractButton::hover
{
    background-color: rgba(100, 100, 100, 35%);
}

QAbstractButton::pressed
{
    background-color: rgba(100, 100, 100, 55%);
}

/* ------------------------------------------------------------------------------- */
QPushButton
{
    padding: 4px;
    border-radius: 3px;
    border: 1px solid;
    color: palette(text);
    background-color: palette(button);
    border-color: black;
}

/* ------------------------------------------------------------------------------- */
QToolButton
{
    border: none;
}
QToolButton::menu-arrow
{
    width: 0px;
}
QToolButton::menu-indicator
{
    width: 0px;
}
QToolButton#ViewSelectionButton
{
    color: palette(base);
    background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0, stop : 0 #153b4c, stop : 1 #347a9c);
}
QToolButton#ViewSelectionButton:pressed
{
    color: palette(base);
    background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #97a8b0, stop: 1 #dae7ed);
}
QToolButton#ViewSelectionButton:hover
{
    color: palette(base);
    background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #254b5c, stop: 1 #448aac);
}
QToolButton#ViewSelectionButton:checked
{
    color: palette(base);
    background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0, stop : 0 #2b899c, stop : 1 #2abdda);
}
QToolButton#ViewSelectionButton:disabled
{
    color: dimgray;
    background-color: transparent;
    border-right: 1px solid rgba(100, 100, 100, 25%);
}

QToolButton:checked
{
    background-color: palette(text);
}

/* ------------------------------------------------------------------------------- */
QRadioButton
{
    background-color: transparent;
    spacing: 5px;
    padding: 0px;
}
QRadioButton::focus
{
    border: 0px solid transparent;
}
QRadioButton::hover
{
    background-color: transparent;
}
QRadioButton::pressed
{
    background-color: transparent;
}
QRadioButton::indicator
{
    width: 13px;
    height: 13px;
    border: 1px solid transparent;
    border-radius: 7px;
    background-color: rgba(100, 100, 100, 15%);
}
QRadioButton::indicator:checked
{
    image: url(:/images/radio-checked.svg);
}
QRadioButton::indicator:hover
{
    background-color: palette(light);
}

/* ------------------------------------------------------------------------------- */
QCheckBox
{
    background-color: transparent;
    spacing: 5px;
    padding: 0px;
}
QCheckBox::focus
{
    border: 0px solid transparent;
}
QCheckBox::hover
{
    background-color: transparent;
}
QCheckBox::pressed
{
    background-color: transparent;
}
QCheckBox::indicator
{
    width: 13px;
    height: 13px;
    border: 1px solid transparent;
    background-color: rgba(100, 100, 100, 15%);
}
QCheckBox::indicator:checked
{
    image: url(:/images/checkbox-checked.svg);
}
QCheckBox::indicator:hover
{
    background-color: palette(light);
}

/* ------------------------------------------------------------------------------- */
QTabWidget
{
    background-color: palette(light)
}
QTabWidget::pane
{
    border: 1px solid;
    border-color: rgba(100, 100, 100, 25%);
    padding: 5px;
    margin: 0px;
}
QTabWidget::tab-bar
{
    left: 5px;
}

/* ------------------------------------------------------------------------------- */
QMenu
{
    background-color: palette(base);
    border: 1px solid;
    border-color: rgba(100, 100, 100, 25%);
    color: palette(text);
}
QMenu::separator
{
    height: 1px;
    background-color: rgba(100, 100, 100, 25%); /* same as menu border color */
}
/* To highlight selected item use darker color*/
QMenu::selected,
QMenu::item:selected
{
    background-color: rgba(0, 0, 0, 15%);
}

QMenu QRadioButton
{
    padding: 4px;
    margin 4px;
}
QMenu QRadioButton::indicator
{
    width: 13px;
    height: 13px;
    border: 1px solid transparent;
    border-radius: 7px;
    margin-left: 4px;
    background-color: rgba(100, 100, 100, 15%);
}

QMenu QCheckBox
{
    padding: 4px;
    margin 4px;
}
QMenu QCheckBox::indicator
{
    width: 10px;
    height: 10px;
    border: 0px solid;
    margin-left: 4px;
    padding: 2px;
    background-color: rgba(100, 100, 100, 15%);
}

/* ------------------------------------------------------------------------------- */
QMenuBar
{
    border: 0px;
}
/* To highlight selected item use darker color*/
QMenuBar::item:selected
{
    background-color: rgba(0, 0, 0, 15%);
}

/* ------------------------------------------------------------------------------- */
/* Set alternate background color, e.g, for QTableWidgets alternate row coloring.
QAbstractItemView
{
    alternate-background-color:palette(alternate-base);
}
*/

/* ------------------------------------------------------------------------------- */
/* QListView
{
    background-color: palette(light)
}
*/

/* ------------------------------------------------------------------------------- */
QDockWidget
{
    font: bold 15px;
    margin-left: 10px;
    margin-bottom: 20px;
    border: none;
    text-align: left;
    padding-top: 10px;
    color:palette(text);
    background-color: palette(light);
    titlebar-close-icon: url(:images/dock-close.svg);
    titlebar-normal-icon: url(:images/dock-float.svg);
}
QDockWidget::title
{
    border: none;
    text-align: left;
}
QDockWidget::close-button,
QDockWidget::float-button
{
    border: 1px solid transparent;
    border-radius: 0px;
    background: transparent;
    padding: 0px;
}
QDockWidget::close-button:hover,
QDockWidget::float-button:hover
{
    background: rgba(100, 100, 100, 35%);
}
QDockWidget::close-button:pressed,
QDockWidget::float-button:pressed
{
    background: rgba(100, 100, 100, 55%);
}

/* ------------------------------------------------------------------------------- */
QTabBar::tab
{
    color: palette(text);
    border: 1px solid transparent;
    padding: 5px;
    margin-right: 2px;
    background-color: palette(light);
}
QTabBar::tab:selected
{
    background-color: palette(dark);
    color: palette(text);
}
QTabBar::tab:hover
{
    background-color: palette(mid);
    color: palette(text);
}
QTabBar::tab:top
{
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}
QTabBar::tab:bottom
{
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
}

/* ------------------------------------------------------------------------------- */
QTableView QHeaderView::section
{
    background-color: palette(alternate-base);
    color: palette(text);
    padding: 2px;
    font: 12px;
    spacing: 2px;
    background-clip: padding;
}
QTableView QHeaderView::section:horizontal
{
    border: 1px solid transparent;
    border-right: 1px solid rgba(100, 100, 100, 25%);
}
QTableView QHeaderView::section:vertical
{
    border: 1px solid transparent;
    border-bottom: 1px solid rgba(100, 100, 100, 25%);
}
QTableView QHeaderView::section:checked
{
    background-color: palette(alternate-base);
    color: palette(text);
    padding: 4px;
    font: 12px;
    spacing: 2px;
    background-clip: padding;
}
QTableView QTableCornerButton::section
{
    background-color: palette(alternate-base);
    border: 1px solid transparent;
    color: palette(text);
    font: 12px;
    background-clip: padding;
}

/* ------------------------------------------------------------------------------- */
QScrollBar::add-page:horizontal,
QScrollBar::sub-page:horizontal,
QScrollBar::add-page:vertical,
QScrollBar::sub-page:vertical
{
    background: none;
}
QScrollBar:vertical,
QScrollBar:horizontal
{
    border: 1px solid;
    border-color: rgba(100, 100, 100, 25%);
    background: palette(alternate-base);
    margin: 0px 0px 0px 0px;
}
QScrollBar:vertical{width:10px}
QScrollBar:horizontal{height:10px}
QScrollBar::handle:vertical,
QScrollBar::handle:horizontal
{
    background: palette(dark);
    min-height: 0px;
    min-width: 0px;
}
QScrollBar::add-line:vertical,
QScrollBar::sub-line:vertical,
QScrollBar::add-line:horizontal,
QScrollBar::sub-line:horizontal
{
    border: 1px solid;
    border-color: rgba(100, 100, 100, 25%);
    background: palette(alternate-base);
    height: 0px;
    width: 0px;
}
QScrollBar::add-line:vertical
{
    subcontrol-position: bottom;
    subcontrol-origin: margin;
}
QScrollBar::sub-line:vertical
{
    subcontrol-position: top;
    subcontrol-origin: margin;
}
QScrollBar::add-line:horizontal
{
    subcontrol-position: right;
    subcontrol-origin: margin;
}
QScrollBar::sub-line:horizontal
{
    subcontrol-position: left;
    subcontrol-origin: margin;
}

/* ------------------------------------------------------------------------------- */
QScrollArea .QWidget
{
    background-color: transparent;
}

/* ------------------------------------------------------------------------------- */
QStackedWidget
{
    margin: 0px;
    spacing: 0px;
}

/* ------------------------------------------------------------------------------- */
QToolBar
{
    border: 0px;
}

/* ------------------------------------------------------------------------------- */
StaticGroupBox,
CollapsibleGroupBox
{
    background-color: transparent;
    border: 1px solid;
    border-radius: 2px;
    border-color: palette(light);
}

/* ------------------------------------------------------------------------------- */
LayerStackForm
{
    border: 1px solid;
    border-color: black;
}

/* ------------------------------------------------------------------------------- */
LayerStackForm#OuterLayerStackForm
{
    border: 0px;
}

/* ------------------------------------------------------------------------------- */
LayerForm
{
    margin-top:0px;
    border: 0px;
}

/* ------------------------------------------------------------------------------- */
/* Common style for all input controls. */
QLineEdit,
QTextEdit,
QSpinBox,
DSpinBox,
ParSpinBox,
QComboBox
{
    padding: 4px;
    border-radius: 3px;
    border: 1px solid;
    min-width: 95px;
    color:palette(text);
    background-color: palette(button);
    border-color: rgba(100, 100, 100, 25%);
}

/* Highlight border of focused controls with system highlight color. */
QLineEdit::focus,
QTextEdit::focus,
QSpinBox::focus,
DSpinBox::focus,
ParSpinBox::focus
{
    border: 2px solid palette(highlight);
}

/* ------------------------------------------------------------------------------- */
QComboBox::drop-down
{
    subcontrol-origin: padding;
    subcontrol-position: top right;
    width: 15px;
    border-top-right-radius: 3px; /* same radius as the QComboBox */
    border-bottom-right-radius: 3px;
    image: url(:/images/caret-down.svg);
}
QComboBox::drop-down:hover
{
    background-color: palette(light);
}
QComboBox::drop-down:pressed
{
    background-color: palette(mid);
}

/* ------------------------------------------------------------------------------- */
QSpinBox::up-button,
DSpinBox::up-button,
ParSpinBox::up-button
{
    subcontrol-origin: border;
    subcontrol-position: top right;
    width: 15px;
    border-top-right-radius: 3px; /* same radius as the QComboBox */
    border-bottom-right-radius: 3px;
    image: url(:/images/caret-up.svg);
}
QSpinBox::up-button:hover,
DSpinBox::up-button:hover,
ParSpinBox::up-button:hover
{
    background-color: palette(light);
}
QSpinBox::up-button:pressed,
DSpinBox::up-button:pressed,
ParSpinBox::up-button:pressed
{
    background-color: palette(mid);
}
QSpinBox::down-button,
DSpinBox::down-button,
ParSpinBox::down-button
{
    subcontrol-origin: border;
    subcontrol-position: bottom right;
    width: 15px;
    border-top-right-radius: 3px; /* same radius as the QComboBox */
    border-bottom-right-radius: 3px;
    image: url(:/images/caret-down.svg);
}
QSpinBox::down-button:hover,
DSpinBox::down-button:hover,
ParSpinBox::down-button:hover
{
    background-color: palette(light);
}
QSpinBox::down-button:pressed,
DSpinBox::down-button:pressed,
ParSpinBox::down-button:pressed
{
    background-color: palette(mid);
}


QWidget{
    color: palette(text);
}

QTextEdit, QLineEdit, QSpinBox, ParSpinBox, QDoubleSpinBox, DSpinBox, QComboBox, QTabWidget,
QDockWidget, QTableView, QTableWidget, QHeaderView, QProgressBar, QSlider, QScrollBar,
QToolBar, QPushButton, QScrollArea {
    background-color: palette(base);
}

::disabled{
    color: rgb(200, 200, 200);
}

SampleView LayerOrientedSampleEditor QScrollArea LayerForm QLabel{
    color: palette(text);
}

SampleView LayerOrientedSampleEditor QScrollArea LayerForm QCheckBox{
    color: palette(text);
}

SampleView LayerOrientedSampleEditor QScrollArea LayerForm QToolButton{
    color: palette(text);
    background-color: "transparent";
}

SampleView LayerOrientedSampleEditor QScrollArea LayerForm QPushButton{
    color: palette(text);
}
