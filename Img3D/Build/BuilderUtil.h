//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Build/BuilderUtil.h
//! @brief     Defines Img3D::BuilderUtils namespace.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_BUILD_BUILDERUTIL_H
#define BORNAGAIN_IMG3D_BUILD_BUILDERUTIL_H

#include "Img3D/Type/FloatVector3D.h"
#include <QColor>
#include <functional>
#include <memory>

class Compound;
class CoreAndShell;
class Mesocrystal;
class Particle;

namespace Img3D {

class Particle3DContainer;
class PlotParticle;

class BuilderUtils {
public:
    BuilderUtils(std::function<QColor(const QString&)> fnColorFromMaterialName);

    Particle3DContainer singleParticle3DContainer(const Particle& particle,
                                                  double total_abundance = 1.0,
                                                  const Img3D::F3& origin = {});

    Particle3DContainer particleCoreShell3DContainer(const CoreAndShell& particleCoreShell,
                                                     double total_abundance = 1.0,
                                                     const Img3D::F3& origin = {});

    Particle3DContainer
    particleComposition3DContainer(const Compound& particleComposition3DContainer,
                                   double total_abundance = 1.0, const Img3D::F3& origin = {});

    Particle3DContainer mesocrystal3DContainer(Mesocrystal* const mesocrystal,
                                               double total_abundance = 1.0,
                                               const Img3D::F3& origin = {});

private:
    std::function<QColor(const QString&)> m_fn_color_from_material_name;

    //! Apply color to a 3D particle
    void applyParticleColor(const Particle& particle, Img3D::PlotParticle& particle3D,
                            double alpha = 1);
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_BUILD_BUILDERUTIL_H
