//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Build/ParacrystalLatticePositions.h
//! @brief     Declares function Paracrystal::latticePositions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_BUILD_PARACRYSTALLATTICEPOSITIONS_H
#define BORNAGAIN_IMG3D_BUILD_PARACRYSTALLATTICEPOSITIONS_H

#include "Base/Type/Field2D.h"
#include <vector>

class Interference2DParacrystal;

namespace Paracrystal {

double2d_t latticePositions(const Interference2DParacrystal*, double layer_size, int seed);
}

#endif // BORNAGAIN_IMG3D_BUILD_PARACRYSTALLATTICEPOSITIONS_H
