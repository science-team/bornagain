//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Build/Particle3DContainer.cpp
//! @brief     Implements namespace GUI::View::TransformTo3D.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Img3D/Build/Particle3DContainer.h"
#include "Img3D/Model/Particles.h"

namespace Img3D {

Particle3DContainer::~Particle3DContainer() = default;

void Particle3DContainer::addParticle3D(Img3D::PlotParticle* particle3D)
{
    m_container_particles.push_back(particle3D);
}

void Particle3DContainer::setCumulativeAbundance(double cumulativeAbundance)
{
    m_cumulative_abundance = cumulativeAbundance;
}

const PlotParticle* Particle3DContainer::particleAt(const size_t& index) const
{
    return m_container_particles.at(index);
}

std::unique_ptr<Img3D::PlotParticle> Particle3DContainer::createParticle(const size_t& index) const
{
    return std::make_unique<Img3D::PlotParticle>(*m_container_particles.at(index));
}

} // namespace Img3D
