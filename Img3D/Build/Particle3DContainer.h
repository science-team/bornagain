//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Build/Particle3DContainer.h
//! @brief     Implements namespace GUI::View::TransformTo3D.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_BUILD_PARTICLE3DCONTAINER_H
#define BORNAGAIN_IMG3D_BUILD_PARTICLE3DCONTAINER_H

#include "Base/Type/OwningVector.h"
#include <memory>

namespace Img3D {

class PlotParticle;

//! Contains the constituents of a 3D Particle (e.g. core and shell of a CoreAndShell)
class Particle3DContainer {
public:
    Particle3DContainer()
        : m_cumulative_abundance(0)
    {
    }
    Particle3DContainer(const Particle3DContainer& other) = delete;
    Particle3DContainer& operator=(const Particle3DContainer& rhs) = delete;
    ~Particle3DContainer();

    Particle3DContainer(Particle3DContainer&& other) = default;
    Particle3DContainer& operator=(Particle3DContainer&& rhs) = default;

    size_t containerSize() const { return m_container_particles.size(); }
    double cumulativeAbundance() const { return m_cumulative_abundance; }

    void addParticle3D(Img3D::PlotParticle* particle3D);
    void setCumulativeAbundance(double cumulativeAbundance);

    const Img3D::PlotParticle* particleAt(const size_t& index) const;
    std::unique_ptr<Img3D::PlotParticle> createParticle(const size_t& index) const;

private:
    OwningVector<Img3D::PlotParticle> m_container_particles;
    double m_cumulative_abundance;
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_BUILD_PARTICLE3DCONTAINER_H
