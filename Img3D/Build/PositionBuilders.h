//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Build/PositionBuilders.h
//! @brief     Declares interface IPositionBuilder and subclasses.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_BUILD_POSITIONBUILDERS_H
#define BORNAGAIN_IMG3D_BUILD_POSITIONBUILDERS_H

#include "Base/Type/Field2D.h"
#include <memory>
#include <vector>

class Interference1DLattice;
class Interference2DLattice;
class Interference2DParacrystal;
class InterferenceFinite2DLattice;
class InterferenceRadialParacrystal;

class IPositionBuilder {
public:
    virtual ~IPositionBuilder();

    double2d_t generatePositions(double layer_size, double density, int seed) const;

private:
    virtual double2d_t generatePositionsImpl(double layer_size, double density, int seed) const = 0;
    virtual double positionVariance() const = 0;
};


class RandomPositionBuilder : public IPositionBuilder {
public:
    RandomPositionBuilder();
    ~RandomPositionBuilder() override;

private:
    double2d_t generatePositionsImpl(double layer_size, double density, int seed) const override;
    double positionVariance() const override;
};


class Lattice1DPositionBuilder : public IPositionBuilder {
public:
    Lattice1DPositionBuilder(const Interference1DLattice* p_iff);
    ~Lattice1DPositionBuilder() override;

private:
    double2d_t generatePositionsImpl(double layer_size, double density, int seed) const override;
    double positionVariance() const override;
    std::unique_ptr<Interference1DLattice> m_iff;
};


class Lattice2DPositionBuilder : public IPositionBuilder {
public:
    Lattice2DPositionBuilder(const Interference2DLattice* p_iff);
    ~Lattice2DPositionBuilder() override;

private:
    double2d_t generatePositionsImpl(double layer_size, double density, int seed) const override;
    double positionVariance() const override;
    std::unique_ptr<Interference2DLattice> m_iff;
};


class Paracrystal2DPositionBuilder : public IPositionBuilder {
public:
    Paracrystal2DPositionBuilder(const Interference2DParacrystal* p_iff);
    ~Paracrystal2DPositionBuilder() override;

private:
    double2d_t generatePositionsImpl(double layer_size, double density, int seed) const override;
    double positionVariance() const override;
    std::unique_ptr<Interference2DParacrystal> m_iff;
};


class Finite2DLatticePositionBuilder : public IPositionBuilder {
public:
    Finite2DLatticePositionBuilder(const InterferenceFinite2DLattice* p_iff);
    ~Finite2DLatticePositionBuilder() override;

private:
    double2d_t generatePositionsImpl(double layer_size, double density, int seed) const override;
    double positionVariance() const override;
    std::unique_ptr<InterferenceFinite2DLattice> m_iff;
};


class RadialParacrystalPositionBuilder : public IPositionBuilder {
public:
    RadialParacrystalPositionBuilder(const InterferenceRadialParacrystal* p_iff);
    ~RadialParacrystalPositionBuilder() override;

private:
    double2d_t generatePositionsImpl(double layer_size, double density, int seed) const override;
    double positionVariance() const override;
    std::unique_ptr<InterferenceRadialParacrystal> m_iff;
};

#endif // BORNAGAIN_IMG3D_BUILD_POSITIONBUILDERS_H
