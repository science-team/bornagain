//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Mesh/Ripple.cpp
//! @brief     Implements utility functions in ba3d namespace.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Const/PhysicalConstants.h"
#include "Base/Util/Assert.h"
#include "Img3D/Model/Geometry.h"

using PhysConsts::pi;

namespace Img3D {

Geometry::Mesh Geometry::meshRipple(float numSides, float ratio_asymmetry_W)
{
    int const sides = qRound(numSides);
    bool const smooth = (0 == sides); // sides = 0 implies smooth -> e.g. cosine ripple
    int const slices = smooth ? 4 * SLICES : sides;

    // Values are chosen such that length, width and height are 1
    float const R = .5f;
    float const H = 2 * R;                             // L = W = H = 2*R
    float const asymmetry = ratio_asymmetry_W * 2 * R; // for CosineRipple asymmetry is inherently 0

    Vertices vfront(slices), vback(slices);

    if (numSides == 3) {
        // SawtoothRipple: saw-tooth (3 rectangular sides and 2 triangular front and back)
        vfront[0] = F3(-R, -R, 0);
        vfront[1] = F3(-R, asymmetry, H);
        vfront[2] = F3(-R, R, 0);

        vback[0] = F3(R, -R, 0);
        vback[1] = F3(R, asymmetry, H);
        vback[2] = F3(R, R, 0);
    } else if (numSides == 0) {
        // CosineRipple: cosine ripple
        for (int s = 0; s < slices; ++s) {
            auto th = static_cast<float>(pi * s / (slices + 1));
            float y = -R * cosf(th);
            float z = R * (1.0f + cosf(2 * static_cast<float>(pi) * y / (2 * R)));
            vfront[s] = F3(-R, y, z);
            vback[s] = F3(R, y, z);
        }
    }

    int const nv = (3 + 3 + 6) * slices; // 3 for part of front face, 3 for part of back face
                                         // 6 for side face

    Vertices vs;
    vs.reserve(nv);

    for (int s = 0; s < slices; ++s) {
        int s1 = s, s2 = (s + 1) % slices;

        // clockwise ordering of vertices
        vs.addTriangle(vfront.at(s1), vfront.at(s2), F3(-R, asymmetry, H / 2)); // front

        // counter-clockwise ordering of vertices
        vs.addTriangle(vback.at(s1), F3(R, asymmetry, H / 2), vback.at(s2)); // back

        // counter-clockwise ordering of vertices
        vs.addQuad(vfront.at(s2), vfront.at(s1), vback.at(s1), vback.at(s2)); // side
    }

    ASSERT(vs.count() == nv);

    return makeMesh(vs, nullptr); // normals not calculated here and left for makeMesh to calculate
}

} // namespace Img3D
