//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Model/GeometryStore.cpp
//! @brief     Implements class GeometryStore.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Img3D/Model/GeometryStore.h"
#include "Img3D/Model/Geometry.h"

namespace Img3D {

std::shared_ptr<Geometry> GeometryStore::getGeometry(GeometricID::Key key)
{
    auto it = m_geometries.find(key);
    if (m_geometries.end() != it) {
        if (auto g = it->second.lock())
            return g;
    }
    std::shared_ptr<Geometry> g(new Geometry(key));
    m_geometries[key] = GeometryRef(g);
    return g;
}

void GeometryStore::geometryDeleted(Geometry const& g, GeometricID::Key key)
{
    emit deletingGeometry(&g);
    m_geometries.erase(key);
}

GeometryStore& geometryStore()
{
    static GeometryStore gs;
    return gs;
}

std::size_t GeometryStore::KeyHash::operator()(const GeometricID::Key& key) const noexcept
{
    {
        size_t h1 = std::hash<int>{}(static_cast<int>(key.id));
        size_t h2 = std::hash<float>{}(key.p1);
        size_t h3 = std::hash<float>{}(key.p2);
        size_t h4 = std::hash<float>{}(key.p3);
        return h1 ^ (h2 ^ (h3 ^ h4));
    }
}


} // namespace Img3D
