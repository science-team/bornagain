//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Model/GeometryStore.h
//! @brief     Defines class GeometryStore.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_MODEL_GEOMETRYSTORE_H
#define BORNAGAIN_IMG3D_MODEL_GEOMETRYSTORE_H

#include "Img3D/Model/Geometry_inc.h"
#include <QObject>
#include <unordered_map>

namespace Img3D {

class Geometry;

//! A single instance of this store keeps existing geometries for sharing

class GeometryStore : public QObject {
    Q_OBJECT
    using GeometryRef = std::weak_ptr<Geometry>;

public:
    std::shared_ptr<Geometry> getGeometry(GeometricID::Key);
    void geometryDeleted(Geometry const&, GeometricID::Key key); // ~Geometry() calls this

signals:
    void deletingGeometry(Geometry const*);

private:
    struct KeyHash {
        std::size_t operator()(const GeometricID::Key& key) const noexcept;
    };
    std::unordered_map<GeometricID::Key, GeometryRef, GeometryStore::KeyHash> m_geometries;
    //! Hash functor for Key objects
};

GeometryStore& geometryStore(); // simpleton

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_MODEL_GEOMETRYSTORE_H
