//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Model/Geometry_inc.cpp
//! @brief     Implements namespace geometry.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Img3D/Model/Geometry_inc.h"

namespace Img3D {

// Useful constants:
const float GoldenRatio = (1.f + std::sqrt(5.f)) / 2.f;
const float IcosahedronL2R = 4.f / (10.f + 2.f * std::sqrt(5.f));
const float DodecahedronL2R = 4.f / std::sqrt(3.f) / (1.f + std::sqrt(5.f));

// Keys and hash:
GeometricID::Key::Key(BaseShape id_, float p1_, float p2_, float p3_)
    : id(id_)
    , p1(p1_)
    , p2(p2_)
    , p3(p3_)
{
}

bool GeometricID::Key::operator==(Key const& other) const
{
    return id == other.id && p1 == other.p1 && p2 == other.p2;
}

} // namespace Img3D
