//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Model/Geometry_inc.h
//! @brief     Defines namespace Img3D::GeometricID.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_MODEL_GEOMETRY_INC_H
#define BORNAGAIN_IMG3D_MODEL_GEOMETRY_INC_H

#include "Img3D/Type/FloatVector3D.h"
#include <memory>

// include to use geometry basics, without details

namespace Img3D {

// some useful constants:
extern const float GoldenRatio;
extern const float IcosahedronL2R; // L/R conversion
extern const float DodecahedronL2R;

namespace GeometricID {

//! Enum id for basic shapes
enum class BaseShape {
    Plane,
    Box,
    Sphere,
    Column,
    Icosahedron,
    Dodecahedron,
    TruncatedBox,
    Bipyramid4,
    Ripple
};

//! Real shapes will be parameterized by BaseShape enum and possibly two floats
struct Key {
    Key(BaseShape, float = 0.0f, float = 0.0f, float = 0.0f);

    bool operator==(Key const&) const;

    BaseShape id;
    float p1, p2, p3; // semantics depends on mesh type
};

} // namespace GeometricID

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_MODEL_GEOMETRY_INC_H
