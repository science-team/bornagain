//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Model/Layer.cpp
//! @brief     Implements class Layer.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Img3D/Model/Layer.h"
#include "GUI/Model/Sample/RoughnessItems.h"

namespace Img3D {

Layer::Layer(F3Range d, const double2d_t* top, const double2d_t* bottom, bool drawBottom)
    : PlottableBody(GeometricID::Key(GeometricID::BaseShape::Box), top, bottom, drawBottom)
{
    transform(d.size(), F3(0, 0, 0), d.mid());
}

} // namespace Img3D
