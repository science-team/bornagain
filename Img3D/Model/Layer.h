//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Model/Layer.h
//! @brief     Defines class Layer.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_MODEL_LAYER_H
#define BORNAGAIN_IMG3D_MODEL_LAYER_H

#include "Img3D/Model/PlottableBody.h"

namespace Img3D {

class RoughnessItem;

//! Particle layer: a transparent box
class Layer : public PlottableBody {
public:
    Layer(F3Range, const double2d_t* top = nullptr, const double2d_t* bottom = nullptr,
          bool drawBottom = false);
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_MODEL_LAYER_H
