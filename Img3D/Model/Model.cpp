//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Model/Model.cpp
//! @brief     Implements class Model.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Img3D/Model/Model.h"
#include "Base/Util/Assert.h"

namespace Img3D {

Model::Model()
    : defaultCameraPosition(F3(1, 1, 1), F3(0, 0, 0), F3(0, 0, 1))
{
}

Model::~Model() = default;

void Model::emplaceSolidBody(PlottableBody* o)
{
    ASSERT(o);
    m_objects.push_back(o);
}

void Model::emplaceTransparentBody(PlottableBody* o)
{
    ASSERT(o);
    m_transparent_objects.push_back(o);
}

bool Model::modelIsEmpty() const
{
    return m_objects.empty() && m_transparent_objects.empty();
}

} // namespace Img3D
