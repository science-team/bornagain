//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Model/Model.h
//! @brief     Defines class Model.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_MODEL_MODEL_H
#define BORNAGAIN_IMG3D_MODEL_MODEL_H

#include "Base/Type/OwningVector.h"
#include "Img3D/Model/Particles.h"
#include "Img3D/Type/CameraParams.h"
#include <QObject>

namespace Img3D {

class Camera;
class PlottableBody;

class Model : public QObject {
    Q_OBJECT
public:
    Model();
    ~Model() override;

    void emplaceSolidBody(PlottableBody*);       //!< adds an opaque object, takes ownership
    void emplaceTransparentBody(PlottableBody*); //!< adds a transparent object, takes ownership

    bool modelIsEmpty() const;

    CameraParams defaultCameraPosition; //!< default camera params

    const OwningVector<PlottableBody>& objects() const { return m_objects; }
    const OwningVector<PlottableBody>& transparentObjects() const { return m_transparent_objects; }

private:
    OwningVector<PlottableBody> m_objects;
    OwningVector<PlottableBody> m_transparent_objects;
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_MODEL_MODEL_H
