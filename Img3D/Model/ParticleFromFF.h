//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Model/ParticleFromFF.h
//! @brief     Defines namespace GUI::View::TransformTo3D.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_MODEL_PARTICLEFROMFF_H
#define BORNAGAIN_IMG3D_MODEL_PARTICLEFROMFF_H

#include <memory>

class IFormfactor;

namespace Img3D {

class PlotParticle;

std::unique_ptr<Img3D::PlotParticle> particle3DfromFF(const IFormfactor* ff);

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_MODEL_PARTICLEFROMFF_H
