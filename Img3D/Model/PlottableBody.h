//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Model/PlottableBody.h
//! @brief     Defines class PlottableBody.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_MODEL_PLOTTABLEBODY_H
#define BORNAGAIN_IMG3D_MODEL_PLOTTABLEBODY_H

#include "Base/Type/Field2D.h"
#include "Img3D/Model/Geometry_inc.h"
#include "Img3D/Type/FloatVector3D.h"
#include <QColor>
#include <QMatrix4x4>

namespace Img3D {

class Canvas;
class Geometry;
class Model;
class RoughnessItem;

//! A geometric object. Base class for Particle and Layer.
class PlottableBody {
public:
    PlottableBody(GeometricID::Key, const double2d_t* top = nullptr,
                  const double2d_t* bottom = nullptr, bool drawBottom = false);
    virtual ~PlottableBody();

    const QColor& color() const { return m_color; }
    bool isTransparent() const;

    void transform(F3 scale, F3 rotate, F3 translate);
    void transform(F3 scale, QQuaternion, F3 translate);
    void transform(F3 turn, F3 scale, F3 rotate, F3 translate);

    void addExtrinsicRotation(F3 turn, F3 scale, F3& rotate, F3 rotateExtrinsic, F3& translate);
    void setColor(QColor color) { m_color = color; }

    bool valid() const { return !isNull; }
    const Geometry& geo() const;
    const QMatrix4x4& matrix() const { return m_matrix; }

    void releaseGeometry(); // can be released whenever

protected:
    bool isNull;

private:
    QColor m_color;
    GeometricID::Key gky;
    mutable std::shared_ptr<Geometry> m_geo; // retrieved on demand
    QMatrix4x4 m_matrix;
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_MODEL_PLOTTABLEBODY_H
