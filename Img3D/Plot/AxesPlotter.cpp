//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Plot/AxesPlotter.cpp
//! @brief     Implements class AxesPlotter.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Img3D/Plot/AxesPlotter.h"

namespace {

const float cx = 120; // multiplication scale for controlling how long the axes shall be in xy
const float cz = 100; // multiplication scale for controlling how long the axes shall be in z

} // namespace

namespace Img3D {

AxesPlotter::AxesPlotter()
    : m_vertex_count3DAxes(30)
{
    initializeOpenGLFunctions();

    QOpenGLVertexArrayObject::Binder dummy(&m_vao3DAxes);

    // vertices (xyz) and colors (rgb) for drawing each line (also arrows) in the 3D axes
    const GLfloat vertices3DAxes[] = {
        0.00f,       0.00f,       0.00f,       1.0f,       0.0f,        0.0f, // x-axis
        cx * 1.00f,  0.00f,       0.00f,       1.0f,       0.0f,        0.0f,        cx * 1.00f,
        0.00f,       0.00f,       1.0f,        0.0f,       0.0f,        cx * 0.95f,  cz * 0.05f,
        cz * 0.05f,  1.0f,        0.0f,        0.0f,       cx * 1.00f,  0.00f,       0.00f,
        1.0f,        0.0f,        0.0f,        cx * 0.95f, cz * 0.05f,  cz * -0.05f, 1.0f,
        0.0f,        0.0f,        cx * 1.00f,  0.00f,      0.00f,       1.0f,        0.0f,
        0.0f,        cx * 0.95f,  cz * -0.05f, cz * 0.05f, 1.0f,        0.0f,        0.0f,
        cx * 1.00f,  0.00f,       0.00f,       1.0f,       0.0f,        0.0f,        cx * 0.95f,
        cz * -0.05f, cz * -0.05f, 1.0f,        0.0f,       0.0f,

        0.00f,       0.00f,       0.00f,       0.0f,       1.0f,        0.0f, // y-axis
        0.00f,       cx * 1.00f,  0.00f,       0.0f,       1.0f,        0.0f,        0.00f,
        cx * 1.00f,  0.00f,       0.0f,        1.0f,       0.0f,        cz * 0.05f,  cx * 0.95f,
        cz * 0.05f,  0.0f,        1.0f,        0.0f,       0.00f,       cx * 1.00f,  0.00f,
        0.0f,        1.0f,        0.0f,        cz * 0.05f, cx * 0.95f,  cz * -0.05f, 0.0f,
        1.0f,        0.0f,        0.00f,       cx * 1.00f, 0.00f,       0.0f,        1.0f,
        0.0f,        cz * -0.05f, cx * 0.95f,  cz * 0.05f, 0.0f,        1.0f,        0.0f,
        0.00f,       cx * 1.00f,  0.00f,       0.0f,       1.0f,        0.0f,        cz * -0.05f,
        cx * 0.95f,  cz * -0.05f, 0.0f,        1.0f,       0.0f,

        0.00f,       0.00f,       0.00f,       0.0f,       0.0f,        1.0f, // z-axis
        0.00f,       0.00f,       cz * 1.00f,  0.0f,       0.0f,        1.0f,        0.00f,
        0.00f,       cz * 1.00f,  0.0f,        0.0f,       1.0f,        cz * 0.05f,  cz * 0.05f,
        cz * 0.95f,  0.0f,        0.0f,        1.0f,       0.00f,       0.00f,       cz * 1.00f,
        0.0f,        0.0f,        1.0f,        cz * 0.05f, cz * -0.05f, cz * 0.95f,  0.0f,
        0.0f,        1.0f,        0.00f,       0.00f,      cz * 1.00f,  0.0f,        0.0f,
        1.0f,        cz * -0.05f, cz * 0.05f,  cz * 0.95f, 0.0f,        0.0f,        1.0f,
        0.00f,       0.00f,       cz * 1.00f,  0.0f,       0.0f,        1.0f,        cz * -0.05f,
        cz * -0.05f, cz * 0.95f,  0.0f,        0.0f,       1.0f,
    };

    m_gl_axes_plotter.create();
    m_gl_axes_plotter.bind();
    m_gl_axes_plotter.allocate(vertices3DAxes, int(sizeof(vertices3DAxes)));

    glEnableVertexAttribArray(0); // 3D axes vertices
    glEnableVertexAttribArray(2); // 3D axes colors

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), nullptr);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
                          reinterpret_cast<void*>(3 * sizeof(float)));
}

void AxesPlotter::draw3DAxes()
{
    QOpenGLVertexArrayObject::Binder dummy(&m_vao3DAxes);
    glLineWidth(1.4f);
    glDrawArrays(GL_LINES, 0, m_vertex_count3DAxes);
}

} // namespace Img3D
