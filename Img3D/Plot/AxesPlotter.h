//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Plot/AxesPlotter.h
//! @brief     Defines class AxesPlotter.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_PLOT_AXESPLOTTER_H
#define BORNAGAIN_IMG3D_PLOT_AXESPLOTTER_H

#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>

namespace Img3D {

//! Buffer for drawing 3D Coordinate Axes on canvas
class AxesPlotter final : protected QOpenGLFunctions {
public:
    AxesPlotter();
    void draw3DAxes();

private:
    int m_vertex_count3DAxes;
    QOpenGLVertexArrayObject m_vao3DAxes;
    QOpenGLBuffer m_gl_axes_plotter;
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_PLOT_AXESPLOTTER_H
