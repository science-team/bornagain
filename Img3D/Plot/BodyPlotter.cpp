//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Plot/BodyPlotter.cpp
//! @brief     Implements class BodyPlotter.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Img3D/Plot/BodyPlotter.h"
#include "Img3D/Model/Geometry.h"

namespace Img3D {

BodyPlotter::BodyPlotter(Geometry const& geometry)
{
    initializeOpenGLFunctions();

    const QVector<Geometry::VertexAndNormal>& mesh = geometry.mesh();
    m_vertex_count = mesh.count();

    QOpenGLVertexArrayObject::Binder dummy(&m_vao);

    m_gl_buffer.create();
    m_gl_buffer.bind();
    m_gl_buffer.allocate(mesh.constData(), m_vertex_count * int(sizeof(Geometry::VertexAndNormal)));

    glEnableVertexAttribArray(0); // vertices
    glEnableVertexAttribArray(1); // normals

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(F3), nullptr);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(F3),
                          reinterpret_cast<void*>(sizeof(F3)));
}

void BodyPlotter::draw()
{
    QOpenGLVertexArrayObject::Binder dummy(&m_vao);
    glDrawArrays(GL_TRIANGLES, 0, m_vertex_count);
}

} // namespace Img3D
