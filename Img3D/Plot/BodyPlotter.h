//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Plot/BodyPlotter.h
//! @brief     Defines class BodyPlotter.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_PLOT_BODYPLOTTER_H
#define BORNAGAIN_IMG3D_PLOT_BODYPLOTTER_H

#include "Img3D/Type/FloatVector3D.h"
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>

namespace Img3D {

class Geometry;

//! GL buffer
class BodyPlotter final : protected QOpenGLFunctions {
public:
    BodyPlotter(Geometry const&);
    void draw();

private:
    int m_vertex_count;
    QOpenGLVertexArrayObject m_vao;
    QOpenGLBuffer m_gl_buffer;
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_PLOT_BODYPLOTTER_H
