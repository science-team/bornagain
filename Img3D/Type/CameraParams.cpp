//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Type/CameraParams.cpp
//! @brief     Implements struct CameraParams.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Img3D/Type/CameraParams.h"

Img3D::CameraParams::CameraParams(const F3& eye_, const F3& ctr_, const F3& up_,
                                  QQuaternion const& rot_)
    : eye(eye_)
    , ctr(ctr_)
    , up(up_)
    , rot(rot_)
{
}
