//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Type/CameraParams.h
//! @brief     Defines struct CameraParams.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_TYPE_CAMERAPARAMS_H
#define BORNAGAIN_IMG3D_TYPE_CAMERAPARAMS_H

#include "Img3D/Type/FloatVector3D.h"
#include <QQuaternion>

namespace Img3D {

struct CameraParams {
    CameraParams();
    CameraParams(const F3& eye, const F3& ctr, const F3& up, const QQuaternion& = {});

    F3 eye;
    F3 ctr; // center
    F3 up;
    QQuaternion rot;
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_TYPE_CAMERAPARAMS_H
