//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Type/FloatVector3D.cpp
//! @brief     Definitions
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Img3D/Type/FloatVector3D.h"

namespace Img3D {

F3 F3fromR3(const R3& o)
{
    return {static_cast<float>(o.x()), static_cast<float>(o.y()), static_cast<float>(o.z())};
}

FloatRange::FloatRange(float r1, float r2)
    : min(qMin(r1, r2))
    , max(qMax(r1, r2))
{
}

float FloatRange::size() const
{
    return max - min;
}

float FloatRange::mid() const
{
    return (min + max) / 2;
}


F3Range::F3Range(FloatRange x_, FloatRange y_, FloatRange z_)
    : x(x_)
    , y(y_)
    , z(z_)
{
}

F3Range::F3Range(F3 _1, F3 _2)
    : x(FloatRange(_1.x(), _2.x()))
    , y(FloatRange(_1.y(), _2.y()))
    , z(FloatRange(_1.z(), _2.z()))
{
}

F3 F3Range::size() const
{
    return {x.size(), y.size(), z.size()};
}

F3 F3Range::mid() const
{
    return {x.mid(), y.mid(), z.mid()};
}

} // namespace Img3D
