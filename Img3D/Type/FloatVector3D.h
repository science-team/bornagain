//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Type/FloatVector3D.h
//! @brief     Definitions in namespace Img3D
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_TYPE_FLOATVECTOR3D_H
#define BORNAGAIN_IMG3D_TYPE_FLOATVECTOR3D_H

#if defined(__GNUC__) && !defined(__APPLE__)
#pragma GCC diagnostic ignored "-Wswitch-enum"
#pragma GCC diagnostic ignored "-Wpadded"
#pragma GCC diagnostic ignored "-Wfloat-equal"
#endif

#include <QVector3D>
#include <heinz/Vectors3D.h>

namespace Img3D {

using F3 = QVector3D;

F3 F3fromR3(const R3&);

//! FloatRange of float
struct FloatRange {
    float min, max;
    FloatRange(float, float);

    float size() const;
    float mid() const;
};

//! FloatRange of coordinates
struct F3Range {
    FloatRange x, y, z;
    F3Range(FloatRange, FloatRange, FloatRange);
    F3Range(F3, F3);

    F3 size() const;
    F3 mid() const;
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_TYPE_FLOATVECTOR3D_H
