//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/Type/SceneGeometry.h
//! @brief     Defines class SceneGeometry.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_TYPE_SCENEGEOMETRY_H
#define BORNAGAIN_IMG3D_TYPE_SCENEGEOMETRY_H

//! Size and thickness information of layers
struct SceneGeometry {
    double layerSize = 100.0;
    double layerBorderWidth = 5.0;
    double topOrBottomLayerThickness = 25.0;
    double layerMinimumThickness = 2.0;
    unsigned numRoughnessPointsAlongAxis = 256;
    unsigned maxNumberOfParticlesToShow = 20000;
};

#endif // BORNAGAIN_IMG3D_TYPE_SCENEGEOMETRY_H
