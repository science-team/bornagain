//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/View/Camera.h
//! @brief     Defines class Camera.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_VIEW_CAMERA_H
#define BORNAGAIN_IMG3D_VIEW_CAMERA_H

#include "Img3D/Type/CameraParams.h"
#include "Img3D/Type/FloatVector3D.h"
#include <QColor>
#include <QMatrix4x4>
#include <QObject>
#include <QQuaternion>

namespace Img3D {

class Canvas;

class Camera : public QObject {
    Q_OBJECT
public:
    Camera();

    void lookAt(const CameraParams&);       //!< camera position for 3D object
    void lookAt3DAxes(const CameraParams&); //!< camera position for 3D axes

    const CameraParams& getPos() const { return pos; }
    const CameraParams& getPos3DAxes() const { return pos3DAxes; }

    void set();
    void setAspectRatio(float);
    void zoomBy(float);
    void endTransform(bool keep);

    const QMatrix4x4& matModel() const { return m_mat_model; }
    const QMatrix4x4& matProj() const { return m_mat_proj; }
    const QMatrix4x4& matAxes() const { return m_mat_model3DAxes; }

    const F3 lightPosRotated() const { return lightPosRotated1; }

    // Flying Camera implementation (similar to Blender's camera system)
    void horizontalTurn(float theta);
    void verticalTurn(float theta);

signals:
    void updated(Camera const&);

private:
    // camera setup
    CameraParams pos;
    CameraParams pos3DAxes;

    float zoom;
    float vertAngle, nearPlane, farPlane;

    // light
    F3 lightPos1, lightPosRotated1;

    // transformation
    QQuaternion addRot; // rotation, additional rotation
    QMatrix4x4 m_mat_model;
    QMatrix4x4 m_mat_proj;
    QMatrix4x4 m_mat_model3DAxes;
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_VIEW_CAMERA_H
