//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Img3D/View/Shader.h
//! @brief     Defines class Shader.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_IMG3D_VIEW_SHADER_H
#define BORNAGAIN_IMG3D_VIEW_SHADER_H

#include "Img3D/Type/FloatVector3D.h"
#include <QOpenGLShaderProgram>

namespace Img3D {

class Camera;
class Canvas;

class Shader : private QOpenGLShaderProgram {
public:
    Shader();

    void needsInit(); //<! marks as needing init
    void init();      //<! inits, if needed
    void bind() { QOpenGLShaderProgram::bind(); }
    void release() { QOpenGLShaderProgram::release(); }
    void setCamera(Camera const&);
    void setColor(QColor const&);
    void setMatModel(QMatrix4x4 const&);
    void setAxis(bool const&);
    void setMatObject(QMatrix4x4 const&);

private:
    bool doInit;
    int locMatProj, locMatModel, locMatObject;
    int locLightPos1, locColor, ambient, eye;
    int locAxis;
};

} // namespace Img3D

#endif // BORNAGAIN_IMG3D_VIEW_SHADER_H
