//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Param/Distrib/DistributionHandler.h
//! @brief     Defines class DistributionHandler.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_PARAM_DISTRIB_DISTRIBUTIONHANDLER_H
#define BORNAGAIN_PARAM_DISTRIB_DISTRIBUTIONHANDLER_H

#include <functional>
#include <map>
#include <vector>

class ParameterDistribution;
struct ParameterSample;

//! Provides the functionality to average over parameter distributions with weights.
class DistributionHandler {
public:
    DistributionHandler();
    ~DistributionHandler();

    //... Setters:

    void addDistribution(const ParameterDistribution& par_distr);

    //! Sets the parameter values of the simulation object to a specific
    //! combination of values, determined by the index (which must be smaller
    //! than the total number of combinations) and returns the weight
    //! associated with this combination of parameter values.
    double setParameterValues(size_t index);

    void defineCallbackForDistribution(const ParameterDistribution* distribution,
                                       std::function<void(double)> fn);

    //... Getters:

    //! Returns the total number of parameter value combinations (product of distribution sizes).
    size_t nParamSamples() const { return m_n_combinations; }

    const std::vector<ParameterDistribution>& paramDistributions() const { return m_distributions; }

private:
    size_t m_n_combinations;
    std::vector<ParameterDistribution> m_distributions;
    std::map<const ParameterDistribution*, std::function<void(double)>> m_set_value_functions;
    std::vector<std::vector<ParameterSample>> m_cached_samples;
};

#endif // BORNAGAIN_PARAM_DISTRIB_DISTRIBUTIONHANDLER_H
