//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Param/Distrib/ParameterDistribution.cpp
//! @brief     Implements class ParameterDistribution.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Param/Distrib/ParameterDistribution.h"
#include "Base/Util/Assert.h"
#include "Param/Distrib/Distributions.h"
#include "Param/Distrib/ParameterSample.h"

ParameterDistribution::ParameterDistribution(WhichParameter whichParameter,
                                             const IDistribution1D& distribution)
    : m_which_parameter(whichParameter)
{
    m_distribution.reset(distribution.clone());
}

ParameterDistribution::ParameterDistribution(const ParameterDistribution& other)
    : m_which_parameter(other.m_which_parameter)
{
    m_distribution.reset(other.m_distribution->clone());
}

ParameterDistribution::~ParameterDistribution() = default;

std::string ParameterDistribution::whichParameterAsPyEnum() const
{
    switch (whichParameter()) {
    case ParameterDistribution::BeamWavelength:
        return "ParameterDistribution.BeamWavelength";
    case ParameterDistribution::BeamGrazingAngle:
        return "ParameterDistribution.BeamGrazingAngle";
    case ParameterDistribution::BeamAzimuthalAngle:
        return "ParameterDistribution.BeamAzimuthalAngle";
    default:
        ASSERT_NEVER;
    }
}

size_t ParameterDistribution::nDraws() const
{
    ASSERT(m_distribution);
    return m_distribution->nSamples();
}

double ParameterDistribution::relSamplingWidth() const
{
    return m_distribution->relSamplingWidth();
}

std::vector<ParameterSample> ParameterDistribution::generateSamples() const
{
    return m_distribution->distributionSamples();
}

std::string ParameterDistribution::unitOfParameter() const
{
    switch (m_which_parameter) {
    case BeamWavelength:
        return "nm";
    case BeamAzimuthalAngle:
    case BeamGrazingAngle:
        return "rad";
    default:
        ASSERT_NEVER;
    }
}
