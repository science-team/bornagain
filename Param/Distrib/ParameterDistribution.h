//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Param/Distrib/ParameterDistribution.h
//! @brief     Defines class ParameterDistribution.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_PARAM_DISTRIB_PARAMETERDISTRIBUTION_H
#define BORNAGAIN_PARAM_DISTRIB_PARAMETERDISTRIBUTION_H

#include <memory>
#include <string>
#include <vector>

class IDistribution1D;
struct ParameterSample;

//! A parametric distribution function, for use with any model parameter.

class ParameterDistribution {
public:
    enum WhichParameter {
        None, // #baPool ++ really necessary?? Try to refactor the relevant place
        BeamWavelength,
        BeamGrazingAngle,
        BeamAzimuthalAngle
    };

    ParameterDistribution(WhichParameter whichParameter, const IDistribution1D& distribution);
    ParameterDistribution(const ParameterDistribution& other);

    virtual ~ParameterDistribution();

#ifndef SWIG
    //... Getters:

    WhichParameter whichParameter() const { return m_which_parameter; }
    const IDistribution1D* getDistribution() const { return m_distribution.get(); }

    //! get number of samples for this distribution
    size_t nDraws() const;
    double relSamplingWidth() const;

    std::string whichParameterAsPyEnum() const;

    //! generate list of sampled values with their weight
    std::vector<ParameterSample> generateSamples() const;

    std::string unitOfParameter() const;

private:
    WhichParameter m_which_parameter;
    std::unique_ptr<IDistribution1D> m_distribution;
#endif // SWIG
};

#endif // BORNAGAIN_PARAM_DISTRIB_PARAMETERDISTRIBUTION_H
