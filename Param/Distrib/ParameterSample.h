//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Param/Distrib/ParameterSample.h
//! @brief     Defines struct ParameterSample.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_PARAM_DISTRIB_PARAMETERSAMPLE_H
#define BORNAGAIN_PARAM_DISTRIB_PARAMETERSAMPLE_H

//! A parameter value with a weight, as obtained when sampling from a distribution.

struct ParameterSample {
    double value;
    double weight;
};

#endif // BORNAGAIN_PARAM_DISTRIB_PARAMETERSAMPLE_H
