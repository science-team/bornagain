//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Param/Node/INode.cpp
//! @brief     Implements interface INode.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Param/Node/INode.h"
#include "Base/Util/Assert.h"
#include "Base/Util/StringUtil.h"
#include <algorithm>
#include <iostream>

INode::INode(const std::vector<double>& PValues)
    : m_P(PValues)
{
}

std::vector<const INode*> INode::nodeChildren() const
{
    return {};
}

std::vector<const INode*> INode::nodeOffspring() const
{
    std::vector<const INode*> result;
    result.push_back(this);
    for (const auto* child : nodeChildren()) {
        for (const auto* p : child->nodeOffspring())
            result.push_back(p);
    }
    return result;
}

void INode::requestGt0(std::vector<std::string>& errs, const double& val, const std::string& name)
{
    if (val <= 0)
        errs.push_back("nonpositive " + name + "=" + std::to_string(val));
}

void INode::requestGe0(std::vector<std::string>& errs, const double& val, const std::string& name)
{
    if (val < 0)
        errs.push_back("negative " + name + "=" + std::to_string(val));
}

void INode::requestIn(std::vector<std::string>& errs, const double& val, const std::string& name,
                      double min, double max)
{
    if (val < min || val > max)
        errs.push_back("parameter " + name + "=" + std::to_string(val) + " not in ["
                       + std::to_string(min) + ", " + std::to_string(max) + "]");
}

std::string INode::jointError(const std::vector<std::string> errs) const
{
    return "{ " + className() + ": [ " + Base::String::join(errs, ", ") + " ] }";
}

void INode::validateOrThrow() const
{
    const std::string err = validate();
    if (!err.empty())
        throw std::runtime_error(err);
}
