//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Param/Node/NodeUtil.h
//! @brief     Defines collection of utility functions for INode.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_PARAM_NODE_NODEUTIL_H
#define BORNAGAIN_PARAM_NODE_NODEUTIL_H

#include "Base/Util/Assert.h"
#include "Param/Node/INode.h"
#include <string>
#include <vector>

namespace NodeUtil {

template <typename T> std::vector<const T*> ChildNodesOfType(const INode& node)
{
    std::vector<const T*> result;
    for (const auto& p_child : node.nodeChildren()) {
        if (const auto* t = dynamic_cast<const T*>(p_child))
            result.push_back(t);
    }
    return result;
}

template <typename T> const T* OnlyChildOfType(const INode& node)
{
    const auto list = ChildNodesOfType<T>(node);
    if (list.size() != 1)
        return nullptr;
    return list.front();
}

template <typename T> std::vector<const T*> AllDescendantsOfType(const INode& node)
{
    std::vector<const T*> result;
    for (const auto* child : node.nodeChildren()) {
        ASSERT(child);
        // Since this function is used to define objects in the generated Python script, the order
        // of the elements is important. The parent may call its children => the children must be
        // defined before. So first add all children/descendants to the output vector.
        for (const auto* t : AllDescendantsOfType<T>(*child))
            result.push_back(t);
        if (const auto* t = dynamic_cast<const T*>(child))
            result.push_back(t);
    }
    return result;
}

} // namespace NodeUtil

#endif // BORNAGAIN_PARAM_NODE_NODEUTIL_H
