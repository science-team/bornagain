## BornAgain project

BornAgain is a software to simulate and fit neutron and x-ray reflectometry
and scattering at grazing incidence, using distorted-wave Born approximation (DWBA).

Full documentation at https://www.bornagainproject.org.
