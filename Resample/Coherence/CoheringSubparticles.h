//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Coherence/CoheringSubparticles.h
//! @brief     Defines class CoheringSubparticles.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_COHERENCE_COHERINGSUBPARTICLES_H
#define BORNAGAIN_RESAMPLE_COHERENCE_COHERINGSUBPARTICLES_H

#include "Base/Type/OwningVector.h"
#include <heinz/Complex.h>
#include <vector>

class DiffuseElement;
class IReParticle;
class SpinMatrix;
struct SubparticlePlacements;

//! A set of particle slices that have fixed positions relative to each other.
//! Accordingly, the collective form factor is computed by coherent superposition.

class CoheringSubparticles {
public:
    CoheringSubparticles(double abundance, OwningVector<IReParticle>&& terms);
    ~CoheringSubparticles();

    complex_t summedFF(const DiffuseElement& ele) const;
    SpinMatrix summedPolFF(const DiffuseElement& ele) const;

    double relativeAbundance() const { return m_abundance; }
    double radialExtension() const;

    const OwningVector<IReParticle>& terms() const { return m_subparticles; }

private:
    //! Leaves only the unique subparticles.
    void findUniqueSubparticles();

    const double m_abundance;
    const OwningVector<IReParticle> m_subparticles;
    std::vector<SubparticlePlacements> m_unique_subparticles;
};

#endif // BORNAGAIN_RESAMPLE_COHERENCE_COHERINGSUBPARTICLES_H
