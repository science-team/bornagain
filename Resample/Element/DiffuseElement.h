//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Element/DiffuseElement.h
//! @brief     Defines class DiffuseElement.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_ELEMENT_DIFFUSEELEMENT_H
#define BORNAGAIN_RESAMPLE_ELEMENT_DIFFUSEELEMENT_H

#include "Resample/Element/IElement.h"
#include <heinz/Vectors3D.h>
#include <memory>
#include <vector>

class Fluxes;
class IFlux;
class Pixel;
class WavevectorInfo;

//! Data stucture containing both input and output of a single detector cell.

class DiffuseElement : public IElement {
public:
    DiffuseElement(double wavelength, double alpha_i, double phi_i, const Pixel* const pixel,
                   const SpinMatrix& polarizer, const SpinMatrix& analyzer, bool isSpecular_,
                   const Fluxes* fluxes_in = nullptr, const Fluxes* fluxes_out = nullptr);
    DiffuseElement(const DiffuseElement&) = delete;
    DiffuseElement(DiffuseElement&&) noexcept = default;

    void setFluxes(const Fluxes* fluxes_in, const Fluxes* fluxes_out);
    const IFlux* fluxIn(size_t i_layer) const;
    const IFlux* fluxOut(size_t i_layer) const;

    //! Returns copy of this DiffuseElement with k_f given by in-pixel coordinate x,y.
    DiffuseElement pointElement(double x, double y) const;

    double wavelength() const { return m_wavelength; }
    double alphaI() const { return m_alpha_i; }
    double alphaMean() const { return alpha(0.5, 0.5); }
    R3 getKi() const { return m_k_i; }
    R3 meanKf() const { return m_mean_kf; }
    R3 meanQ() const;

    double integrationFactor(double x, double y) const;

    double solidAngle() const;

    WavevectorInfo wavevectorInfo() const;

    //! Tells if simulation element corresponds to a specular peak
    bool isSpecular() const { return m_is_specular; }

private:
    double alpha(double x, double y) const;
    R3 getKf(double x, double y) const;

    const double m_wavelength; //!< wavelength of beam
    const double m_alpha_i;    //!< incident grazing angle
    const double m_phi_i;      //!< incident angle in xy plane
    const R3 m_k_i;            //!< cached value of k_i
    const R3 m_mean_kf;        //!< cached value of mean_kf
    const Pixel* m_pixel;
    const bool m_is_specular;
    const Fluxes* m_fluxes_in;
    const Fluxes* m_fluxes_out;
};

#endif // BORNAGAIN_RESAMPLE_ELEMENT_DIFFUSEELEMENT_H
