//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Element/IElement.h
//! @brief     Defines class IElement.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_ELEMENT_IELEMENT_H
#define BORNAGAIN_RESAMPLE_ELEMENT_IELEMENT_H

#include "Base/Spin/SpinMatrix.h"

class IElement {
public:
    IElement() = default;

    IElement(const SpinMatrix& polarizer, const SpinMatrix& analyzer)
        : m_polarizer(polarizer)
        , m_analyzer(analyzer)
    {
    }

    const SpinMatrix& polarizer() const { return m_polarizer; }
    const SpinMatrix& analyzer() const { return m_analyzer; }

protected:
    const SpinMatrix m_polarizer{SpinMatrix::One() / 2.0};
    const SpinMatrix m_analyzer{SpinMatrix::One()};
};

#endif // BORNAGAIN_RESAMPLE_ELEMENT_IELEMENT_H
