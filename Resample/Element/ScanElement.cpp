//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Element/ScanElement.cpp
//! @brief     Implements class the ScanElement.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Resample/Element/ScanElement.h"

ScanElement::ScanElement(size_t i_out, bool computable, double weight, double intensity,
                         double footprint, const SpinMatrix& polarizer, const SpinMatrix& analyzer,
                         double lambda, double alpha, double phi, R3 k)
    : IElement(polarizer, analyzer)
    , m_i_out(i_out)
    , m_computable(computable)
    , m_weight(weight)
    , m_beam_intensity(intensity)
    , m_footprint(footprint)
    , m_lambda(lambda)
    , m_alpha(alpha)
    , m_phi(phi)
    , m_k(k)
{
}
