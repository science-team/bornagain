//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Element/ScanElement.h
//! @brief     Declares class the ScanElement.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_ELEMENT_SCANELEMENT_H
#define BORNAGAIN_RESAMPLE_ELEMENT_SCANELEMENT_H

#include "Resample/Element/IElement.h"
#include <functional>
#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>
#include <memory>
#include <vector>

class SliceStack;

//! Data stucture containing input of a single computation point for scan-based simulations.

class ScanElement : public IElement {
public:
    ScanElement(size_t i_out, bool computable, double weight, double intensity, double footprint,
                const SpinMatrix& polarizer, const SpinMatrix& analyzer, double lambda,
                double alpha, double phi, R3 k);

    ScanElement(const ScanElement& other) = delete;
    ScanElement(ScanElement&& other) noexcept = default;

    size_t i_out() const { return m_i_out; }
    double weight() const { return m_weight; }
    double beamIntensity() const { return m_beam_intensity; }
    double footprint() const { return m_footprint; }
    double lambda() const { return m_lambda; }
    double alpha() const { return m_alpha; }
    double phi() const { return m_phi; }

    //! Returns calculation flag (if it's false, zero intensity is assigned to the element)
    bool isCalculated() const { return m_computable; }

    R3 k() { return m_k; }

private:
    const size_t m_i_out; //!< index in scan -> the channel to which this element contributes
    const bool m_computable;
    const double m_weight;
    const double m_beam_intensity;
    const double m_footprint;
    const double m_lambda;
    const double m_alpha;
    const double m_phi;
    const R3 m_k;
};

#endif // BORNAGAIN_RESAMPLE_ELEMENT_SCANELEMENT_H
