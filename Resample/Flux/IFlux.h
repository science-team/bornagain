//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Flux/IFlux.h
//! @brief     Defines and implements class IFlux.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_FLUX_IFLUX_H
#define BORNAGAIN_RESAMPLE_FLUX_IFLUX_H

#include "Base/Spin/Spinor.h"
#include "Base/Type/OwningVector.h"

//! Interface to access reflection/transmission coefficients.
//! Realized by ScalarFlux and MatrixFlux.

class IFlux {
public:
    virtual ~IFlux() = default;

    //! The following functions return the transmitted and reflected amplitudes
    //! for different incoming beam polarizations and eigenmodes
    virtual Spinor T1plus() const = 0;
    virtual Spinor R1plus() const = 0;
    virtual Spinor T2plus() const = 0;
    virtual Spinor R2plus() const = 0;
    virtual Spinor T1min() const = 0;
    virtual Spinor R1min() const = 0;
    virtual Spinor T2min() const = 0;
    virtual Spinor R2min() const = 0;
    //! Returns z-part of the two wavevector eigenmodes
    virtual Spinor getKz() const = 0;
};

class Fluxes : public OwningVector<IFlux> {};

#endif // BORNAGAIN_RESAMPLE_FLUX_IFLUX_H
