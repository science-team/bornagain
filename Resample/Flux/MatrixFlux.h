//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file     Resample/Flux/MatrixFlux.h
//! @brief    Defines class MatrixFlux.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2020
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_FLUX_MATRIXFLUX_H
#define BORNAGAIN_RESAMPLE_FLUX_MATRIXFLUX_H

#include "Base/Spin/SpinMatrix.h"
#include "Resample/Flux/IFlux.h"
#include <heinz/Vectors3D.h>

//! Specular reflection and transmission coefficients in a layer in case
//! of magnetic interactions between the scattered particle and the layer.

class MatrixFlux : public IFlux {
public:
    MatrixFlux(double kz_sign, const Spinor& k_eigen, const R3& b, double magnetic_SLD);

    //! The following functions return the transmitted and reflected amplitudes
    //! for different incoming beam polarizations and eigenmodes
    Spinor T1plus() const override;
    Spinor R1plus() const override;
    Spinor T2plus() const override;
    Spinor R2plus() const override;
    Spinor T1min() const override;
    Spinor R1min() const override;
    Spinor T2min() const override;
    Spinor R2min() const override;
    //! Returns z-part of the two wavevector eigenmodes
    Spinor getKz() const override;
    double magneticSLD() const { return m_magnetic_SLD; }

    SpinMatrix computeKappa() const;
    SpinMatrix computeInverseKappa() const;
    SpinMatrix eigenToMatrix(const Spinor& diagonal) const;

    SpinMatrix computeDeltaMatrix(double thickness) const;

    SpinMatrix getReflectionMatrix() const { return m_R; };

    complex_t k_eigen_up() const { return m_k_eigen.u; }
    complex_t k_eigen_dn() const { return m_k_eigen.v; }
    const R3& field() const { return m_b; }

private:
    Spinor m_k_eigen; //!< eigenvalues for wave propagation

public: // TODO privatize
    SpinMatrix m_T;
    SpinMatrix m_R;

private:
    R3 m_b;           //!< unit magnetic field vector
    double m_kz_sign; //! wave propagation direction (-1 for direct one, 1 for time reverse)
    double m_magnetic_SLD;

    // helper functions to compute DWBA compatible amplitudes used in the T1plus() etc. functions
    SpinMatrix T1Matrix() const;
    SpinMatrix T2Matrix() const;
};

#endif // BORNAGAIN_RESAMPLE_FLUX_MATRIXFLUX_H
