//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Flux/ScalarFlux.cpp
//! @brief     Implements class ScalarFlux.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Resample/Flux/ScalarFlux.h"

ScalarFlux::ScalarFlux(complex_t kz, Spinor TR)
    : m_kz(kz)
    , m_TR(TR)
    , m_plus(1, 0)
    , m_min(0, 1)
{
}

Spinor ScalarFlux::T1plus() const
{
    return Spinor::Zero();
}

Spinor ScalarFlux::R1plus() const
{
    return Spinor::Zero();
}

Spinor ScalarFlux::T2plus() const
{
    return m_plus * getScalarT();
}

Spinor ScalarFlux::R2plus() const
{
    return m_plus * getScalarR();
}

Spinor ScalarFlux::T1min() const
{
    return m_min * getScalarT();
}

Spinor ScalarFlux::R1min() const
{
    return m_min * getScalarR();
}

Spinor ScalarFlux::T2min() const
{
    return Spinor::Zero();
}

Spinor ScalarFlux::R2min() const
{
    return Spinor::Zero();
}

Spinor ScalarFlux::getKz() const
{
    return (m_plus + m_min) * m_kz;
}

complex_t ScalarFlux::getScalarR() const
{
    return m_TR.v;
}

complex_t ScalarFlux::getScalarT() const
{
    return m_TR.u;
}
