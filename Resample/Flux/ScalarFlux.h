//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Flux/ScalarFlux.h
//! @brief     Defines class ScalarFlux.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_FLUX_SCALARFLUX_H
#define BORNAGAIN_RESAMPLE_FLUX_SCALARFLUX_H

#include "Resample/Flux/IFlux.h"

//! Specular reflection and transmission coefficients in a layer in case
//! of scalar interactions between the layers and the scattered particle.

class ScalarFlux : public IFlux {
public:
    ScalarFlux(complex_t kz, Spinor TR);

    // The following functions return the transmitted and reflected amplitudes
    // for different incoming beam polarizations and eigenmodes
    Spinor T1plus() const override;
    Spinor R1plus() const override;
    Spinor T2plus() const override;
    Spinor R2plus() const override;
    Spinor T1min() const override;
    Spinor R1min() const override;
    Spinor T2min() const override;
    Spinor R2min() const override;

    //! Returns z-part of the two wavevector eigenmodes
    Spinor getKz() const override;

    complex_t getScalarT() const;
    complex_t getScalarR() const;
    complex_t getScalarKz() const { return m_kz; }

private:
    const complex_t m_kz; //!< Signed vertical wavevector component k_z

    //! Transmission and reflection coefficient
    //!
    //! In the manual called A^{-} = m_TR(0) and A^{+} = m_TR(1).
    //! Values of the transmitted/reflected (=down/up propagating) wavefunction
    //! at top boundary of the layer (resp. at the bottom of the top air/vacuum layer).
    const Spinor m_TR;

    const Spinor m_plus;
    const Spinor m_min;
};

#endif // BORNAGAIN_RESAMPLE_FLUX_SCALARFLUX_H
