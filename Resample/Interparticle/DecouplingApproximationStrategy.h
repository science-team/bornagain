//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Interparticle/DecouplingApproximationStrategy.h
//! @brief     Defines class DecouplingApproximationStrategy.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_INTERPARTICLE_DECOUPLINGAPPROXIMATIONSTRATEGY_H
#define BORNAGAIN_RESAMPLE_INTERPARTICLE_DECOUPLINGAPPROXIMATIONSTRATEGY_H

#include "Resample/Interparticle/IInterparticleStrategy.h"

class DiffuseElement;

//! Strategy class to compute the total scattering from a particle layout
//! in the decoupling approximation.

class DecouplingApproximationStrategy : public IInterparticleStrategy {
public:
    DecouplingApproximationStrategy(
        const OwningVector<const CoheringSubparticles>& weighted_formfactors,
        const IInterference* iff, SimulationOptions options, bool polarized);

private:
    //! Returns the total scalar incoherent and coherent scattering intensity
    //! for given kf and for one particle layout (implied by the given particle form factors).
    double scalarCalculation(const DiffuseElement& ele) const override;
    //! Returns the total polarized incoherent and coherent scattering intensity
    //! for given kf and for one particle layout (implied by the given particle form factors).
    double polarizedCalculation(const DiffuseElement& ele) const override;

    const std::unique_ptr<IInterference> m_iff;
};

#endif // BORNAGAIN_RESAMPLE_INTERPARTICLE_DECOUPLINGAPPROXIMATIONSTRATEGY_H
