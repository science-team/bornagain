//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Interparticle/IInterparticleStrategy.cpp
//! @brief     Implements default behaviour of IInterparticleStrategy.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Resample/Interparticle/IInterparticleStrategy.h"
#include "Base/Math/IntegratorMCMiser.h"
#include "Base/Util/Assert.h"
#include "Resample/Coherence/CoheringSubparticles.h"
#include "Resample/Element/DiffuseElement.h"

IInterparticleStrategy::IInterparticleStrategy(
    const OwningVector<const CoheringSubparticles>& weighted_formfactors,
    const SimulationOptions& options, bool polarized)
    : m_weighted_formfactors(weighted_formfactors)
    , m_options(options)
    , m_polarized(polarized)
    , m_integrator(
          make_integrator_miser(this, &IInterparticleStrategy::evaluate_for_fixed_angles, 2))
{
    ASSERT(!m_weighted_formfactors.empty());
}

IInterparticleStrategy::IInterparticleStrategy(IInterparticleStrategy&&) = default;

IInterparticleStrategy::~IInterparticleStrategy() = default;

double IInterparticleStrategy::evaluate(const DiffuseElement& ele) const
{
    if (m_options.isIntegrate() && (ele.solidAngle() > 0.0))
        return MCIntegratedEvaluate(ele);
    return evaluateSinglePoint(ele);
}

double IInterparticleStrategy::evaluateSinglePoint(const DiffuseElement& ele) const
{
    // functions (scalar|polarized)Calculation are implemented in child classes
    if (!m_polarized)
        return scalarCalculation(ele);
    return polarizedCalculation(ele);
}

//! Performs a Monte Carlo integration over the bin for the evaluation of the intensity.
double IInterparticleStrategy::MCIntegratedEvaluate(const DiffuseElement& ele) const
{
    double min_array[] = {0.0, 0.0};
    double max_array[] = {1.0, 1.0};
    return m_integrator->integrate(min_array, max_array, (void*)&ele, m_options.getMcPoints());
}

double IInterparticleStrategy::evaluate_for_fixed_angles(const double* fractions, size_t,
                                                         const void* params) const
{
    double par0 = fractions[0];
    double par1 = fractions[1];

    const auto* pars = static_cast<const DiffuseElement*>(params);

    DiffuseElement ele = pars->pointElement(par0, par1);
    return pars->integrationFactor(par0, par1) * evaluateSinglePoint(ele);
}
