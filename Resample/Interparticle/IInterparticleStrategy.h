//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Interparticle/IInterparticleStrategy.h
//! @brief     Defines interface IInterparticleStrategy.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_INTERPARTICLE_IINTERPARTICLESTRATEGY_H
#define BORNAGAIN_RESAMPLE_INTERPARTICLE_IINTERPARTICLESTRATEGY_H

#include "Base/Type/OwningVector.h"
#include "Resample/Option/SimulationOptions.h"
#include <heinz/Complex.h>
#include <memory>
#include <vector>

template <typename T> class IntegratorMCMiser;

class CoheringSubparticles;
class DiffuseElement;
class IInterference;

//! Abstract base class of DecouplingApproximationStrategy, SSCAStrategy.
//! Provides function 'evaluate' that computes the scattering from a decorated layer.

class IInterparticleStrategy {
public:
    IInterparticleStrategy(const OwningVector<const CoheringSubparticles>& weighted_formfactors,
                           const SimulationOptions& options, bool polarized);
    IInterparticleStrategy(IInterparticleStrategy&&);
    virtual ~IInterparticleStrategy();

    //! Calculates the intensity for scalar particles/interactions
    double evaluate(const DiffuseElement& ele) const;

protected:
    const OwningVector<const CoheringSubparticles>& m_weighted_formfactors;
    const SimulationOptions m_options;

private:
    double evaluateSinglePoint(const DiffuseElement& ele) const;
    double MCIntegratedEvaluate(const DiffuseElement& ele) const;
    double evaluate_for_fixed_angles(const double* fractions, size_t dim, const void* params) const;
    //! Evaluates the intensity in the scalar case
    virtual double scalarCalculation(const DiffuseElement& ele) const = 0;
    //! Evaluates the intensity in the polarized case
    virtual double polarizedCalculation(const DiffuseElement& ele) const = 0;

    const bool m_polarized;

    std::unique_ptr<IntegratorMCMiser<IInterparticleStrategy>> m_integrator;
};

#endif // BORNAGAIN_RESAMPLE_INTERPARTICLE_IINTERPARTICLESTRATEGY_H
