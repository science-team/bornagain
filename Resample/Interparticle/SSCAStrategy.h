//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Interparticle/SSCAStrategy.h
//! @brief     Defines class SSCAStrategy.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_INTERPARTICLE_SSCASTRATEGY_H
#define BORNAGAIN_RESAMPLE_INTERPARTICLE_SSCASTRATEGY_H

#include "Resample/Interparticle/IInterparticleStrategy.h"

class DiffuseElement;
class InterferenceRadialParacrystal;

//! Strategy class to compute the total scattering from a particle layout
//! in the size-spacing correlation approximation.

class SSCAStrategy : public IInterparticleStrategy {
public:
    SSCAStrategy(const OwningVector<const CoheringSubparticles>& weighted_formfactors,
                 const InterferenceRadialParacrystal* iff, SimulationOptions options,
                 bool polarized, double kappa);

private:
    double scalarCalculation(const DiffuseElement& ele) const override;
    double polarizedCalculation(const DiffuseElement& ele) const override;

    complex_t getCharacteristicSizeCoupling(
        double qp, const OwningVector<const CoheringSubparticles>& ff_wrappers) const;
    complex_t calculatePositionOffsetPhase(double qp, double radial_extension) const;

    const std::unique_ptr<InterferenceRadialParacrystal> m_iff;
    const double m_kappa;
    const double m_mean_radius;
};

#endif // BORNAGAIN_RESAMPLE_INTERPARTICLE_SSCASTRATEGY_H
