//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Particle/IReParticle.h
//! @brief     Defines and implements interface IReParticle.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_PARTICLE_IREPARTICLE_H
#define BORNAGAIN_RESAMPLE_PARTICLE_IREPARTICLE_H

#include "Base/Spin/SpinMatrix.h"
#include "Base/Type/ICloneable.h"
#include "Base/Vector/WavevectorInfo.h"
#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>
#include <memory>
#include <optional>
#include <vector>

class DiffuseElement; // used by all children
class IRotation;
class Material;
class Span;
struct OneAdmixture;

//! Auxilary struct to store one polarized DWBA component.

struct PolarizedTerm {
    WavevectorInfo wavevectors;
    SpinMatrix dwba_term;
};

//! Auxilary struct to store one scalar DWBA component.

struct ScalarTerm {
    WavevectorInfo wavevectors;
    complex_t dwba_term;
};

//! Abstract base class for reprocessed particles.
//!
//! Reprocessing is necessary to handle particles that cross material layers.
//! These particles are divided into several.

class IReParticle : public ICloneable {
protected:
    IReParticle();
    IReParticle(const std::optional<size_t>& i_layer);

public:
    ~IReParticle() override;

#ifndef SWIG
    IReParticle* clone() const override = 0;
#endif // SWIG

    //! Passes the material in which this particle is embedded.
    virtual void setAmbientMaterial(const Material&) {}

    //! Returns scattering amplitude for complex wavevectors ki, kf.
    virtual complex_t theFF(const WavevectorInfo& wavevectors) const = 0;

    //! Returns scattering amplitude for matrix interactions
    virtual SpinMatrix thePolFF(const WavevectorInfo& wavevectors) const = 0;

    //! Returns the (approximate in some cases) radial size of the particle of this
    //! form factor's shape. This is used for SSCA calculations
    virtual double radialExtension() const = 0;

    virtual Span zSpan() const = 0;

    const std::optional<size_t>& i_layer() const { return m_i_layer; }
    size_t i_layer_or_0() const { return m_i_layer ? m_i_layer.value() : 0; }
    void setLayerIndex(size_t i) { m_i_layer = i; }

    //! Returns the coherent sum of the DWBA terms for scalar scattering.
    complex_t coherentFF(const std::vector<ScalarTerm>& components, const R3& shift) const;

    //! Returns the DWBA terms for scalar scattering.
    std::vector<ScalarTerm> calcCoherentComponents(const DiffuseElement& ele) const;

    //! Returns the coherent sum of the DWBA terms for polarized scattering.
    SpinMatrix coherentPolFF(const std::vector<PolarizedTerm>& components, const R3& shift) const;

    //! Returns the DWBA terms for polarized scattering.
    std::vector<PolarizedTerm> calcCoherentPolComponents(const DiffuseElement& ele) const;

    OneAdmixture admixed() const;
    double admixedFraction() const { return m_admixed_fraction; }
    void setAdmixedFraction(double fraction);
    void setAdmixedMaterial(const Material& material);

    //! Checks 'equality' of processed particles for the purpose of FF computation.
    virtual bool consideredEqualTo(const IReParticle& ire) const;

    //! Returns a parameter understood as 'position' for different particle types.
    virtual const R3* position() const = 0;

    //! Util to compare particle positions.
    static R3 posDiff(const R3* a, const R3* b);

    //! Returns phase factor due to particle position.
    static complex_t phaseFactor(const WavevectorInfo& wavevectors, const R3* position);

private:
    std::optional<size_t> m_i_layer;
    std::unique_ptr<Material> m_admixed_material;
    double m_admixed_fraction = 0;
};

#endif // BORNAGAIN_RESAMPLE_PARTICLE_IREPARTICLE_H
