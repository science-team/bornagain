//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Particle/ReCompound.cpp
//! @brief     Implements class ReCompound.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Resample/Particle/ReCompound.h"
#include "Base/Type/Span.h"
#include "Base/Util/Assert.h"
#include "Base/Vector/WavevectorInfo.h" // debug

ReCompound::ReCompound(const std::optional<size_t>& i_layer)
    : IReParticle(i_layer)
{
}

ReCompound::~ReCompound() = default;

ReCompound* ReCompound::clone() const
{
    auto* result = new ReCompound(i_layer());
    for (auto* m_component : m_components)
        result->addFormfactor(*m_component);
    return result;
}

double ReCompound::radialExtension() const
{
    double result{0.0};
    for (auto* m_component : m_components)
        result += m_component->radialExtension();
    return result;
}

Span ReCompound::zSpan() const
{
    ASSERT(!m_components.empty());
    Span result = m_components[0]->zSpan();
    for (size_t i = 1; i < m_components.size(); ++i)
        result = Span::unite(result, m_components[i]->zSpan());
    return result;
}

void ReCompound::addFormfactor(const IReParticle& formfactor)
{
    m_components.push_back(formfactor.clone());
}

void ReCompound::setAmbientMaterial(const Material& material)
{
    for (auto* m_component : m_components)
        m_component->setAmbientMaterial(material);
}

complex_t ReCompound::theFF(const WavevectorInfo& wavevectors) const
{
    complex_t result(0.0, 0.0);
    for (auto* m_component : m_components)
        result += m_component->theFF(wavevectors);
    return result;
}

SpinMatrix ReCompound::thePolFF(const WavevectorInfo& wavevectors) const
{
    SpinMatrix result;
    for (auto* m_component : m_components)
        result += m_component->thePolFF(wavevectors);
    return result;
}

std::vector<const IReParticle*> ReCompound::components() const
{
    std::vector<const IReParticle*> result;
    for (const auto& comp : m_components)
        result.push_back(comp);
    return result;
}

bool ReCompound::consideredEqualTo(const IReParticle& ire) const
{
    if (const auto* re = dynamic_cast<const ReCompound*>(&ire)) {
        const auto& other = re->components();

        if (m_components.size() != other.size())
            return false;

        for (size_t i = 0; i < m_components.size(); i++)
            if (!m_components[i]->consideredEqualTo(*other[i]))
                return false;

        if (!m_components.empty()) {
            const R3 shift = posDiff(m_components.front()->position(), other.front()->position());
            for (size_t i = 1; i < m_components.size(); i++)
                if (shift != posDiff(m_components[i]->position(), other[i]->position()))
                    return false;
        }

        return IReParticle::consideredEqualTo(ire);
    }
    return false;
}

const R3* ReCompound::position() const
{
    if (m_components.empty())
        return nullptr;

    return m_components.front()->position();
}
