//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Particle/ReCompound.h
//! @brief     Defines class ReCompound.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_PARTICLE_RECOMPOUND_H
#define BORNAGAIN_RESAMPLE_PARTICLE_RECOMPOUND_H

#include "Base/Spin/SpinMatrix.h"
#include "Base/Type/OwningVector.h"
#include "Resample/Particle/IReParticle.h"
#include <vector>

//! A reprocessed Compound.

class ReCompound : public IReParticle {
public:
    ReCompound() = default;
    ReCompound(const std::optional<size_t>& i_layer);

    ~ReCompound() override;

#ifndef SWIG
    ReCompound* clone() const override;

    std::vector<const IReParticle*> components() const;
#endif // SWIG

    double radialExtension() const override;

    Span zSpan() const override;

    void addFormfactor(const IReParticle& formfactor);

    void setAmbientMaterial(const Material& material) override;

    complex_t theFF(const WavevectorInfo& wavevectors) const override;

    //! Calculates and returns a polarized form factor calculation in DWBA
    SpinMatrix thePolFF(const WavevectorInfo& wavevectors) const override;

    bool consideredEqualTo(const IReParticle& ire) const override;
    const R3* position() const override;

protected:
    OwningVector<IReParticle> m_components;
};

#endif // BORNAGAIN_RESAMPLE_PARTICLE_RECOMPOUND_H
