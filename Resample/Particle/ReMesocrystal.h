//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Particle/ReMesocrystal.h
//! @brief     Defines class ReMesocrystal.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_PARTICLE_REMESOCRYSTAL_H
#define BORNAGAIN_RESAMPLE_PARTICLE_REMESOCRYSTAL_H

#include "Resample/Option/SimulationOptions.h"
#include "Resample/Particle/IReParticle.h"
#include "Sample/Lattice/Lattice3D.h"
#include "Sample/Particle/Mesocrystal.h"
#include <functional>

class ReParticle;

//! A reprocessed Mesocrystal.

class ReMesocrystal : public IReParticle {
public:
    ReMesocrystal(const std::optional<size_t>& i_layer, const Lattice3D& lattice,
                  const IReParticle& basis, const ReParticle& outer_shape,
                  const MesoOptions& meso_options, double position_variance = 0.0);
    ~ReMesocrystal() override;

#ifndef SWIG
    ReMesocrystal* clone() const override;
#endif // SWIG

    void setAmbientMaterial(const Material& material) override
    {
        m_basis->setAmbientMaterial(material);
    }

    double radialExtension() const override;

    Span zSpan() const override;

    complex_t theFF(const WavevectorInfo& wavevectors) const override;
    SpinMatrix thePolFF(const WavevectorInfo& wavevectors) const override;

    void setBasisIndexes(const ShapeIndexes& shape_indexes);

    const Lattice3D& lattice() const { return m_lattice; }

    const IReParticle* basis() const { return m_basis.get(); }

    const ReParticle* outerShape() const { return m_outer_shape.get(); }

    double positionVariance() const { return m_position_variance; }

    bool consideredEqualTo(const IReParticle& ire) const override;
    const R3* position() const override;

private:
    complex_t structureFactor(const WavevectorInfo& wavevectors) const;

    complex_t realSpaceSum(const WavevectorInfo& wavevectors) const;
    SpinMatrix realSpaceSumPol(const WavevectorInfo& wavevectors) const;

    complex_t reciprocalSpaceSum(const WavevectorInfo& wavevectors) const;
    SpinMatrix reciprocalSpaceSumPol(const WavevectorInfo& wavevectors) const;

    void calculateLargestReciprocalDistance();
    complex_t debyeWallerFactor(const R3& q_i) const;

    std::function<complex_t(const WavevectorInfo&)> m_compute_FF = nullptr;
    std::function<SpinMatrix(const WavevectorInfo&)> m_compute_FF_pol = nullptr;

    Lattice3D m_lattice;
    std::unique_ptr<IReParticle> m_basis;
    std::unique_ptr<ReParticle> m_outer_shape; //!< The outer shape of this mesocrystal
    double m_position_variance;
    MesoOptions m_meso_options;
    double m_max_rec_length;
    ShapeIndexes m_shape_indexes; //!< Info about the lattice points inside the outer shape
};

#endif // BORNAGAIN_RESAMPLE_PARTICLE_REMESOCRYSTAL_H
