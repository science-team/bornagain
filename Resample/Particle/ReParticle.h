//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Particle/ReParticle.h
//! @brief     Defines class interface ReParticle.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_PARTICLE_REPARTICLE_H
#define BORNAGAIN_RESAMPLE_PARTICLE_REPARTICLE_H

#include "Base/Vector/RotMatrix.h"
#include "Resample/Particle/IReParticle.h"
#include <heinz/Vectors3D.h>
#include <memory>

class IFormfactor;
class Material;

//! A reprocessed simple particle, with shape m_ff.

class ReParticle : public IReParticle {
public:
    ReParticle(const IFormfactor* ff, const R3* position, const RotMatrix* rot);
    ~ReParticle() override;

#ifndef SWIG
    ReParticle* clone() const override;
#endif // SWIG

    //! Sets the material of the scatterer
    void setMaterial(const Material& material);

    //! Sets the ambient material
    void setAmbientMaterial(const Material& ambient_material) override;

    complex_t theFF(const WavevectorInfo& wavevectors) const override;
    SpinMatrix thePolFF(const WavevectorInfo& wavevectors) const override;

    double volume() const;

    double radialExtension() const override;

    Span zSpan() const override;

    bool contains(const R3& position) const;

    bool consideredEqualTo(const IReParticle& ire) const override;

    // TODO: should these pointer-returning functions be exposed to the Python interface
    const IFormfactor* ff() const { return m_ff.get(); }

    const Material* material() const { return m_material.get(); }

    const Material* ambientMaterial() const { return m_ambient_material.get(); }

    const RotMatrix* rotMatrix() const { return m_rot_matrix.get(); }

    const R3* position() const override { return m_position.get(); }

private:
    const std::unique_ptr<const IFormfactor> m_ff;
    std::unique_ptr<const Material> m_material;
    std::unique_ptr<const Material> m_ambient_material;
    const std::unique_ptr<const R3> m_position;
    const std::unique_ptr<const RotMatrix> m_rot_matrix;

    ReParticle(const std::optional<size_t>& i_layer, const IFormfactor* ff,
               const Material* material, const Material* ambient_material, const R3* position,
               const RotMatrix* rotMatrix);
};

#endif // BORNAGAIN_RESAMPLE_PARTICLE_REPARTICLE_H
