//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Processed/ReLayout.cpp
//! @brief     Implements class ReLayout.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Resample/Processed/ReLayout.h"
#include "Resample/Coherence/CoheringSubparticles.h"
#include "Resample/Interparticle/DecouplingApproximationStrategy.h"
#include "Resample/Interparticle/IInterparticleStrategy.h"
#include "Resample/Interparticle/SSCAStrategy.h"
#include "Sample/Aggregate/IInterference.h"
#include "Sample/Aggregate/InterferenceRadialParacrystal.h"

namespace {

std::unique_ptr<IInterparticleStrategy>
processedInterference(const OwningVector<const CoheringSubparticles>& particles,
                      const IInterference* iff, const SimulationOptions& options, bool polarized)
{
    if (const auto* radial_para = dynamic_cast<const InterferenceRadialParacrystal*>(iff))
        if (double kappa = radial_para->kappa(); kappa > 0.0)
            return std::make_unique<SSCAStrategy>(particles, radial_para, options, polarized,
                                                  kappa);

    return std::make_unique<DecouplingApproximationStrategy>(particles, iff, options, polarized);
}

} // namespace


ReLayout::ReLayout(double surface_density, OwningVector<const CoheringSubparticles>&& particles,
                   const IInterference* iff, const SimulationOptions& options, bool polarized)
    : m_surface_density(surface_density)
    , m_particles(std::move(particles))
    , m_iff(iff)
    , m_interparticle_strategy(processedInterference(m_particles, iff, options, polarized))
{
}

ReLayout::~ReLayout() = default;
