//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Processed/ReLayout.h
//! @brief     Defines class ReLayout.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_PROCESSED_RELAYOUT_H
#define BORNAGAIN_RESAMPLE_PROCESSED_RELAYOUT_H

#include "Base/Type/OwningVector.h"
#include <memory>

class CoheringSubparticles;
class IInterference;
class IInterparticleStrategy;
class SimulationOptions;

//! Data structure that contains preprocessed data for a single layout.
//!
//! It is set by the preprocessor makeReLayout in ReSample.cpp.

class ReLayout {
public:
    ReLayout(double surface_density, OwningVector<const CoheringSubparticles>&& particles,
             const IInterference* iff, const SimulationOptions& options, bool polarized);

    ~ReLayout();

    double surfaceDensity() const { return m_surface_density; }
    const OwningVector<const CoheringSubparticles>& subparticles() const { return m_particles; }
    const IInterference* interferenceFunction() const { return m_iff.get(); }
    const IInterparticleStrategy* interparticle_strategy() const
    {
        return m_interparticle_strategy.get();
    }

private:
    const double m_surface_density;
    OwningVector<const CoheringSubparticles> m_particles;
    std::unique_ptr<const IInterference> m_iff;
    std::unique_ptr<const IInterparticleStrategy> m_interparticle_strategy;
};

#endif // BORNAGAIN_RESAMPLE_PROCESSED_RELAYOUT_H
