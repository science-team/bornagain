//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Processed/ReSample.h
//! @brief     Defines class ReSample.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_PROCESSED_RESAMPLE_H
#define BORNAGAIN_RESAMPLE_PROCESSED_RESAMPLE_H

#include "Base/Type/OwningVector.h"
#include "Resample/Slice/SliceStack.h"
#include <heinz/Vectors3D.h>

class Fluxes;
class Material;
class ReLayout;
class Sample;
class SimulationOptions;

//! Data structure that contains all the necessary data for scattering calculations.
//!
//! If the usage of average materials is requested, layers and particles are sliced into multiple
//! slices and the average material is calculated for each slice.

class ReSample {
public:
    //! Factory method that wraps the private constructor.
    static ReSample make(const Sample& sample, const SimulationOptions& options,
                         bool forcePolarized = false);
    static ReSample make(const Sample& sample); // used by tests only?

    ReSample(const ReSample&) = delete;
    ~ReSample();

    size_t numberOfSlices() const { return m_stack.size(); }
    const SliceStack& averageSlices() const { return m_stack; }
    const Slice& avgeSlice(size_t i) const { return m_stack.at(i); }
    const OwningVector<const ReLayout>& relayouts() const { return m_relayouts; }
    bool polarizing() const; //!< Contains magnetic material, or nonzero magnetic field
    bool hasRoughness() const;
    const Sample& sample() const { return m_sample; }

    Fluxes fluxesIn(const R3& k) const;
    Fluxes fluxesOut(const R3& k) const;

private:
    ReSample(const Sample& sample, bool polarized, OwningVector<const ReLayout>&& layouts,
             const SliceStack& refined_stack);
    const Sample& m_sample;
    const bool m_polarized;
    const OwningVector<const ReLayout> m_relayouts;
    const SliceStack m_stack;
};

#endif // BORNAGAIN_RESAMPLE_PROCESSED_RESAMPLE_H
