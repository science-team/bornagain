//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Processed/Slicer.h
//! @brief     Defines function Compute::Slicing::sliceFormfactor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG

#ifndef BORNAGAIN_RESAMPLE_PROCESSED_SLICER_H
#define BORNAGAIN_RESAMPLE_PROCESSED_SLICER_H

#include "Base/Type/OwningVector.h"
#include "Resample/Option/SimulationOptions.h"

class IParticle;
class IReParticle;
class Material;
class Span;
class ZLimits;

namespace Compute::Slicing {

OwningVector<IReParticle> particlesInSlice(const IParticle* particle, const ZLimits&,
                                           const Material& ambientMat,
                                           const MesoOptions& meso_options);

} // namespace Compute::Slicing

#endif // BORNAGAIN_RESAMPLE_PROCESSED_SLICER_H
