//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Slice/KzComputation.h
//! @brief     Declares functions in namespace ComputateKz.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_SLICE_KZCOMPUTATION_H
#define BORNAGAIN_RESAMPLE_SLICE_KZCOMPUTATION_H

#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>
#include <vector>

class SliceStack;

namespace Compute::Kz {

//! Computes kz values from known k vector and slices with the following assumptions:
//! - the beam penetrates fronting medium from a side
//! - the wavelength is known for a distant point in vacuum (ref. index = 1)
//! - the incident angle is known for the sample surface
//!
//! This function is used in GISAS and off-spec computations mainly for back-compatibility
//! reasons and should be replaced with computeKzFromRefIndices.

std::vector<complex_t> computeReducedKz(const SliceStack& slices, R3 k);

//! Computes kz values from kz of the incoming beam known at a distant point in vacuum.
//! It is assumed, that the beam penetrates fronting medium from a side.

std::vector<complex_t> computeKzFromSLDs(const SliceStack& slices, double kz);

//! Computes kz values from k-vector of the incoming beam known at a distant point in vacuum.
//! It is assumed, that the beam penetrates fronting medium from a side.

std::vector<complex_t> computeKzFromRefIndices(const SliceStack& slices, R3 k);

} // namespace Compute::Kz

#endif // BORNAGAIN_RESAMPLE_SLICE_KZCOMPUTATION_H
