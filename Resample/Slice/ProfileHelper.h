//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Slice/ProfileHelper.h
//! @brief     Defines class ProfileHelper.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_SLICE_PROFILEHELPER_H
#define BORNAGAIN_RESAMPLE_SLICE_PROFILEHELPER_H

#include "Resample/Slice/SliceStack.h"
#include <utility>
#include <vector>

//! Object that can generate the material profile of a sample as a function of depth.
//!
//! The generated profile contains the complex SLD for SLD materials and the parameters
//! delta and beta for refractive index materials

class ProfileHelper {
public:
    ProfileHelper(const SliceStack& stack);
    ~ProfileHelper() = default;

    std::vector<complex_t> calculateSLDProfile(const std::vector<double>& z_values) const;
    std::vector<double> calculateMagnetizationProfile(const std::vector<double>& z_values,
                                                      std::string xyz) const;
    std::pair<double, double> defaultLimits() const;

private:
    std::vector<complex_t> profile(const std::vector<double>& z_values,
                                   std::string component) const;

    const SliceStack m_stack; // copy
};

#endif // BORNAGAIN_RESAMPLE_SLICE_PROFILEHELPER_H
