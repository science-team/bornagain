//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Slice/Slice.h
//! @brief     Defines class Slice.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_SLICE_SLICE_H
#define BORNAGAIN_RESAMPLE_SLICE_SLICE_H

#include "Resample/Slice/ZLimits.h"
#include "Sample/Material/Material.h"
#include <memory>

class Roughness;

//! Data structure containing the data of a single slice, for calculating the Fresnel coefficients.

class Slice {
public:
    Slice(const ZLimits& zRange, const Material& material, const R3& B_field,
          const Roughness* roughness, double rms);
    ~Slice();

    void setMaterial(const Material& material) { m_material = material; }
    const Material& material() const { return m_material; }

    double low() const;
    double hig() const;
    double higOr0() const;
    const ZLimits& span() const { return m_z_range; }
    double thicknessOr0() const;
    const Roughness* topRoughness() const { return m_top_roughness; }
    double topRMS() const { return m_top_rms; }

    //! Return the potential term that is used in the one-dimensional Fresnel calculations
    complex_t scalarReducedPotential(R3 k, double n_ref) const;

    //! Return the potential term that is used in the one-dimensional Fresnel calculations
    //! in the presence of magnetization
    SpinMatrix polarizedReducedPotential(R3 k, double n_ref) const;

    //! Initializes the magnetic B field from a given ambient field strength H
    void initBField(R3 h_field, double h_z);
    R3 bField() const { return m_B_field; }

    void invertBField();

private:
    const ZLimits m_z_range;
    Material m_material;
    R3 m_B_field; //!< cached value of magnetic induction
    const Roughness* const m_top_roughness;
    double m_top_rms;
};

#endif // BORNAGAIN_RESAMPLE_SLICE_SLICE_H
