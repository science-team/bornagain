//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Slice/SliceStack.h
//! @brief     Defines class SliceStack.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_SLICE_SLICESTACK_H
#define BORNAGAIN_RESAMPLE_SLICE_SLICESTACK_H

#include "Resample/Slice/Slice.h" // TODO move back to cpp file
#include <heinz/Vectors3D.h>
#include <vector>

class Material;
class Roughness;
class Slice;

//! A stack of Slice%s.

class SliceStack : public std::vector<Slice> {
public:
    SliceStack() = default;

    void addTopSlice(double zbottom, const Material& material);
    void addSlice(double thickness, const Material& material, const Roughness* roughness = nullptr,
                  double rms = 0);
    void addNSlices(size_t n, double thickness, const Material& material,
                    const Roughness* roughness = nullptr, double rms = 0);

    SliceStack setBField(const R3& externalField);

    // Top roughness of the slice below
    const Roughness* bottomRoughness(size_t i_slice) const;
    double bottomRMS(size_t i_slice) const;
};

#endif // BORNAGAIN_RESAMPLE_SLICE_SLICESTACK_H
