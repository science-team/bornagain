//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Slice/ZLimits.cpp
//! @brief     Defines class ZLimits.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Resample/Slice/ZLimits.h"
#include "Base/Util/Assert.h"
#include <algorithm>

bool ZLimits::isFinite() const
{
    return !std::isinf(low()) && !std::isinf(hig());
}

ZLimits ZLimits::unite(const ZLimits& left, const ZLimits& right)
{
    return {std::min(left.low(), right.low()), std::max(left.hig(), right.hig())};
}

bool operator==(const ZLimits& left, const ZLimits& right)
{
    return (left.low() == right.low() && left.hig() == right.hig());
}

bool operator!=(const ZLimits& left, const ZLimits& right)
{
    return !(left == right);
}
