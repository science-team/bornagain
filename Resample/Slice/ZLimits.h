//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Slice/ZLimits.h
//! @brief     Defines class ZLimits.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_SLICE_ZLIMITS_H
#define BORNAGAIN_RESAMPLE_SLICE_ZLIMITS_H

#include "Base/Type/Span.h"
#include <cmath>
#include <iostream>
#include <limits>

//! An interval. Limits are of type double, and may be infinite.
//! Used for the z-coordinate, especially when slicing form factors.
//!

class ZLimits : public Span {
public:
    static constexpr double inf = std::numeric_limits<double>::infinity();

    ZLimits()
        : Span(-inf, inf)
    {
    }
    ZLimits(double low, double hig)
        : Span(low, hig)
    {
    }

    bool isFinite() const;

    double higOr0() const { return std::isfinite(hig()) ? hig() : 0; }
    double thicknessOr0() const { return isFinite() ? hig() - low() : 0; }

    //! Returns the union of two ZLimits (the minimal interval that contains both input intervals).
    static ZLimits unite(const ZLimits& left, const ZLimits& right);
};

// used in ZLimitsTest:
bool operator==(const ZLimits& left, const ZLimits& right);
bool operator!=(const ZLimits& left, const ZLimits& right);

#endif // BORNAGAIN_RESAMPLE_SLICE_ZLIMITS_H
