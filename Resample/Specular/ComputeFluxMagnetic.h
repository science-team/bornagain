//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Specular/ComputeFluxMagnetic.h
//! @brief     Defines functions to compute polarized fluxes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2020
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_SPECULAR_COMPUTEFLUXMAGNETIC_H
#define BORNAGAIN_RESAMPLE_SPECULAR_COMPUTEFLUXMAGNETIC_H

#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>
#include <vector>

class Fluxes;
class SliceStack;
class SpinMatrix;

//! Methods to compute polarized propagation directions and fluxes as function of slice.
//!
//! Implements the transfer matrix formalism for the calculation of wave
//! amplitudes of the coherent wave solution in a sample with magnetization.
//! For a description, see internal
//! document "Polarized Implementation of the Transfer Matrix Method"

namespace Compute {

//! Computes refraction angle reflection/transmission coefficients
//! for given sliced sample and wavevector k
Fluxes polarizedFluxes(const SliceStack& slices, const R3& k, bool forward);

//! Computes the Fresnel R coefficient for the top layer only
//! Introduced in order to speed up pure reflectivity computations
SpinMatrix polarizedReflectivity(const SliceStack& slices, const std::vector<complex_t>& kzs,
                                 bool forward);

} // namespace Compute

#endif // BORNAGAIN_RESAMPLE_SPECULAR_COMPUTEFLUXMAGNETIC_H
