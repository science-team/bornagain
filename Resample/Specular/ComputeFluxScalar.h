//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Specular/ComputeFluxScalar.h
//! @brief     Defines functions to compute scalar fluxes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_RESAMPLE_SPECULAR_COMPUTEFLUXSCALAR_H
#define BORNAGAIN_RESAMPLE_SPECULAR_COMPUTEFLUXSCALAR_H

#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>
#include <vector>

class Fluxes;
class SliceStack;

//! Methods to compute scalar propagation directions and fluxes as function of slice.

namespace Compute {

//! Computes refraction angles and transmission/reflection coefficients
//! for given coherent wave propagation in a sample.
Fluxes scalarFluxes(const SliceStack& slices, const R3& k);

//! Computes the Fresnel R coefficient for the top layer only.
//! Introduced in order to speed up pure reflectivity computations.
complex_t scalarReflectivity(const SliceStack& slices, const std::vector<complex_t>& kz);

} // namespace Compute

#endif // BORNAGAIN_RESAMPLE_SPECULAR_COMPUTEFLUXSCALAR_H
