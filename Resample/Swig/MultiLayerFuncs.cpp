//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Swig/MultiLayerFuncs.cpp
//! @brief     Global functions related to MultiLayers.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Resample/Swig/MultiLayerFuncs.h"
#include "Resample/Option/SimulationOptions.h"
#include "Resample/Processed/ReSample.h"
#include "Resample/Slice/ProfileHelper.h"

namespace {

ProfileHelper helper(const Sample& sample)
{
    SimulationOptions options;
    options.setUseAvgMaterials(true);
    const ReSample resample = ReSample::make(sample, options);
    return {resample.averageSlices()};
}

} // namespace

std::vector<double> swigAPI::generateZValues(int n_points, double z_min, double z_max)
{
    std::vector<double> result;
    if (n_points < 1)
        return result;
    double step = n_points > 1 ? (z_max - z_min) / (n_points - 1) : 0.0;
    for (int i = 0; i < n_points; ++i)
        result.push_back(z_min + i * step);
    return result;
}

std::vector<complex_t> swigAPI::materialProfileSLD(const Sample& sample, int n_points, double z_min,
                                                   double z_max)
{
    std::vector<double> z_values = generateZValues(n_points, z_min, z_max);
    return ::helper(sample).calculateSLDProfile(z_values);
}

std::vector<double> swigAPI::magnetizationProfile(const Sample& sample, std::string xyz,
                                                  int n_points, double z_min, double z_max)
{
    std::vector<double> z_values = generateZValues(n_points, z_min, z_max);
    return ::helper(sample).calculateMagnetizationProfile(z_values, xyz);
}

std::pair<double, double> swigAPI::defaultMaterialProfileLimits(const Sample& sample)
{
    SimulationOptions options;
    options.setUseAvgMaterials(true);
    const ReSample resample = ReSample::make(sample, options);
    ProfileHelper helper(resample.averageSlices());
    return helper.defaultLimits();
}
