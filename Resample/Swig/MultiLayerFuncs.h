//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Resample/Swig/MultiLayerFuncs.h
//! @brief     Global functions related to MultiLayers.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_RESAMPLE_SWIG_MULTILAYERFUNCS_H
#define BORNAGAIN_RESAMPLE_SWIG_MULTILAYERFUNCS_H

#include <heinz/Complex.h>
#include <utility>
#include <vector>

class Sample;

//! Functions that are only used in the swig *.i files
namespace swigAPI {

std::vector<double> generateZValues(int n_points, double z_min, double z_max);

//! Calculate average material profile for given sample
std::vector<complex_t> materialProfileSLD(const Sample& sample, int n_points, double z_min,
                                          double z_max);

//! Calculate average magnetization profile for given sample
std::vector<double> magnetizationProfile(const Sample& sample, std::string xyz, int n_points,
                                         double z_min, double z_max);

//! Get default z limits for generating a material profile
std::pair<double, double> defaultMaterialProfileLimits(const Sample& sample);

} // namespace swigAPI

#endif // BORNAGAIN_RESAMPLE_SWIG_MULTILAYERFUNCS_H
