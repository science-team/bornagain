//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/IInterference.cpp
//! @brief     Implements the interface class IInterference.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Aggregate/IInterference.h"
#include <algorithm>
#include <stdexcept>

IInterference::IInterference(const std::vector<double>& PValues)
    : INode(PValues)
{
}

IInterference::IInterference(double position_var)
    : m_position_var(position_var)
{
}

// Default implementation of evaluate assumes no inner structure
// It is only to be overriden in case of the presence of such inner structure. See for example
// Interference2DSuperLattice for such a case.
double IInterference::structureFactor(const R3& q, double outer_iff) const
{
    return iff_no_inner(q, outer_iff);
}

void IInterference::setPositionVariance(double var)
{
    if (var < 0.0)
        throw std::runtime_error(
            "IInterference::setPositionVariance called with negative argument");
    m_position_var = var;
}

double IInterference::DWfactor(R3 q) const
{
    // remove z component for two-dimensional interference functions:
    if (supportsMultilayer())
        q.setZ(0.0);
    return std::exp(-q.mag2() * m_position_var);
}

double IInterference::iff_no_inner(const R3& q, double outer_iff) const
{
    return DWfactor(q) * (iff_without_dw(q) * outer_iff - 1.0) + 1.0;
}
