//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/IInterference.h
//! @brief     Defines and implements the interface class IInterference.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_AGGREGATE_IINTERFERENCE_H
#define BORNAGAIN_SAMPLE_AGGREGATE_IINTERFERENCE_H

#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"
#include <heinz/Vectors3D.h>

//! Abstract base class of interference functions.

class IInterference : public ICloneable, public INode {
protected:
    IInterference(const std::vector<double>& PValues);
    IInterference(double position_var);

public:
#ifndef SWIG
    IInterference* clone() const override = 0;
#endif // SWIG

    //! The interference function for a given wavevector transfer
    virtual double structureFactor(const R3& q, double outer_iff = 1.0) const;

    //! Sets the variance of the position for the calculation of the DW factor
    //! It is defined as the variance in each relevant dimension
    void setPositionVariance(double var);

    //! Returns the position variance
    double positionVariance() const { return m_position_var; }

    //! If defined by this interference function's parameters, returns the particle density (per
    //!  area). Otherwise, returns zero or a user-defined value
    virtual double particleDensity() const { return 0.0; }

    //! Indicates if this interference function can be used with a sample (DWBA mode)
    virtual bool supportsMultilayer() const { return true; }

    //! structureFactors the Debye-Waller factor for a given wavevector transfer
    double DWfactor(R3 q) const;

protected:
    double m_position_var;

    //! Calculates the structure factor in the absence of extra inner structure
    double iff_no_inner(const R3& q, double outer_iff) const;

    //! Calculates the structure factor without Debye-Waller factor
    virtual double iff_without_dw(const R3& q) const = 0;
};

#endif // BORNAGAIN_SAMPLE_AGGREGATE_IINTERFERENCE_H
