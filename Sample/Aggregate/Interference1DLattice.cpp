//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/Interference1DLattice.cpp
//! @brief     Implements class Interference1DLattice.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Aggregate/Interference1DLattice.h"
#include "Base/Util/Assert.h"
#include "Sample/Correlation/Profiles1D.h"
#include "Sample/Correlation/Profiles2D.h"
#include <algorithm>

namespace {

// maximum value for qx*Lambdax
const int nmax = 20;
// minimum number of neighboring reciprocal lattice points to use
const int min_points = 4;

} // namespace

//! Constructor of interference function of one-dimensional lattice.
//! @param length: lattice constant in nanometers
//! @param xi: rotation of lattice with respect to x-axis in radians
Interference1DLattice::Interference1DLattice(double length, double xi)
    : IInterference(0)
    , m_length(length)
    , m_xi(xi)
    , m_na{0}
{
    validateOrThrow();
}

Interference1DLattice::~Interference1DLattice() = default;

Interference1DLattice* Interference1DLattice::clone() const
{
    auto* result = new Interference1DLattice(m_length, m_xi);
    result->setPositionVariance(m_position_var);
    result->m_na = m_na;
    if (m_decay)
        result->setDecayFunction(*m_decay);
    return result;
}

//! Sets one-dimensional decay function.
//! @param decay: one-dimensional decay function in reciprocal space
void Interference1DLattice::setDecayFunction(const IProfile1D& decay)
{
    m_decay.reset(decay.clone());
    double decay_length = m_decay->decayLength();
    double qa_max = m_length * nmax / decay_length / (2 * pi);
    m_na = static_cast<int>(std::lround(std::abs(qa_max) + 0.5));
    m_na = std::max(m_na, min_points);
}

std::vector<const INode*> Interference1DLattice::nodeChildren() const
{
    return std::vector<const INode*>() << m_decay;
}

double Interference1DLattice::iff_without_dw(const R3& q) const
{
    ASSERT(m_decay);
    double result = 0.0;
    double qxr = q.x();
    double qyr = q.y();
    double qx_frac;
    double xi = m_xi;
    double a = m_length;
    double a_rec = (2 * pi) / a;

    // rotate the q vector to xi angle
    // so that qx_prime is along the a axis of lattice
    double qx_prime = qxr * std::cos(xi) + qyr * std::sin(xi);

    // calculate reciprocal vector fraction
    int qa_int = static_cast<int>(qx_prime / a_rec);
    qx_frac = qx_prime - qa_int * a_rec;

    for (int i = -m_na; i < m_na + 1; ++i) {
        double qx = qx_frac + i * a_rec;
        result += m_decay->decayFT(qx);
    }
    return result / a;
}

std::string Interference1DLattice::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_length, "latticeConstant");
    if (!errs.empty())
        return jointError(errs);
    m_validated = true;
    return "";
}
