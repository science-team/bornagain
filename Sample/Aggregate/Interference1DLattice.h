//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/Interference1DLattice.h
//! @brief     Defines class Interference1DLattice.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE1DLATTICE_H
#define BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE1DLATTICE_H

#include "Sample/Aggregate/IInterference.h"

class IProfile1D;

//! Interference function of a 1D lattice.

class Interference1DLattice : public IInterference {
public:
    Interference1DLattice(double length, double xi);
    ~Interference1DLattice() override;

#ifndef SWIG
    Interference1DLattice* clone() const override;

    std::vector<const INode*> nodeChildren() const override;
#endif // SWIG

    std::string className() const final { return "Interference1DLattice"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Length", "nm"}, {"Xi", "rad"}}; }

    void setDecayFunction(const IProfile1D& decay);

    double length() const { return m_length; }
    double xi() const { return m_xi; }

    std::string validate() const override;

private:
    double iff_without_dw(const R3& q) const override;

    double m_length;
    double m_xi;
    std::unique_ptr<IProfile1D> m_decay;
    int m_na; //!< determines the number of reciprocal lattice points to use
};

#endif // BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE1DLATTICE_H
