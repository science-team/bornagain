//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/Interference2DLattice.h
//! @brief     Defines class Interference2DLattice.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE2DLATTICE_H
#define BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE2DLATTICE_H

#include "Sample/Aggregate/IInterference.h"
#include "Sample/Lattice/Lattice2D.h"

class IProfile2D;

//! Interference function of a 2D lattice.

class Interference2DLattice : public IInterference {
public:
    Interference2DLattice(const Lattice2D& lattice);
    ~Interference2DLattice() override;

#ifndef SWIG
    Interference2DLattice* clone() const override;

    std::vector<const INode*> nodeChildren() const override;
#endif // SWIG

    std::string className() const final { return "Interference2DLattice"; }

    void setDecayFunction(const IProfile2D& decay);

    void setIntegrationOverXi(bool integrate_xi);
    bool integrationOverXi() const { return m_integrate_xi; }

    const Lattice2D& lattice() const;

    //! Returns the particle density associated with this 2d lattice
    double particleDensity() const override;

private:
    double iff_without_dw(const R3& q) const override;

    double interferenceForXi(double xi, double qx, double qy) const;

    //! Returns interference from a single reciprocal lattice vector
    double interferenceAtOneRecLatticePoint(double qx, double qy) const;

    //! Returns reciprocal coordinates in the coordinate system rotated by the angle gamma
    std::pair<double, double> rotateOrthonormal(double qx, double qy, double gamma) const;

    //! Returns qx,qy coordinates of q - qint, where qint is a reciprocal lattice vector
    //! bounding the reciprocal unit cell to which q belongs
    std::pair<double, double> calculateReciprocalVectorFraction(double qx, double qy,
                                                                double xi) const;

    bool m_integrate_xi; //!< Integrate over the orientation xi
    std::unique_ptr<IProfile2D> m_decay;
    std::unique_ptr<Lattice2D> m_lattice;
    ReciprocalBases m_sbase; //!< reciprocal lattice is stored without xi
    int m_na, m_nb;          //!< determines the number of reciprocal lattice points to use
};

#endif // BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE2DLATTICE_H
