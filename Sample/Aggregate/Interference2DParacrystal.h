//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/Interference2DParacrystal.h
//! @brief     Defines class Interference2DParacrystal.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE2DPARACRYSTAL_H
#define BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE2DPARACRYSTAL_H

#include "Sample/Aggregate/IInterference.h"
#include "Sample/Correlation/Profiles2D.h"
#include "Sample/Lattice/Lattice2D.h"
#include <array>
#include <heinz/Complex.h>
#include <memory>

class IProfile2D;

//! Interference function of a 2D paracrystal.

class Interference2DParacrystal : public IInterference {
public:
    Interference2DParacrystal(const Lattice2D& lattice, double damping_length, double domain_size_1,
                              double domain_size_2);

    ~Interference2DParacrystal() override;

#ifndef SWIG
    Interference2DParacrystal* clone() const override;

    std::vector<const INode*> nodeChildren() const override;
#endif // SWIG

    std::string className() const final { return "Interference2DParacrystal"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"DampingLength", "nm"}, {"DomainSize 1", "nm"}, {"DomainSize 2", "nm"}};
    }

    void setDomainSizes(double size_1, double size_2);

    void setProbabilityDistributions(const IProfile2D& pdf_1, const IProfile2D& pdf_2);

    void setDampingLength(double damping_length);

    std::vector<double> domainSizes() const;

    void setIntegrationOverXi(bool integrate_xi);
    bool integrationOverXi() const { return m_integrate_xi; }
    double dampingLength() const { return m_damping_length; }

    const Lattice2D& lattice() const;

    double particleDensity() const override;

    const IProfile2D* pdf1() const { return m_pdf1.get(); }

    const IProfile2D* pdf2() const { return m_pdf2.get(); }

    std::string validate() const override;

private:
    double iff_without_dw(const R3& q) const override;

    double interferenceForXi(double xi, double qx, double qy) const;
    double interference1D(double qx, double qy, double xi, size_t index) const;
    complex_t FTPDF(double qx, double qy, double xi, size_t index) const;
    void transformToPrincipalAxes(double qx, double qy, double gamma, double delta, double& q_pa_1,
                                  double& q_pa_2) const;

    bool m_integrate_xi; //!< Integrate over the orientation xi
    std::unique_ptr<IProfile2D> m_pdf1, m_pdf2;
    std::unique_ptr<Lattice2D> m_lattice; // TODO ASAP name as in other i-fcts
    double m_damping_length; //!< Damping length for removing delta function singularity at q=0.
    std::array<double, 2> m_domain_sizes; //!< Coherence domain sizes
};

#endif // BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE2DPARACRYSTAL_H
