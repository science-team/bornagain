//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/Interference2DSuperLattice.cpp
//! @brief     Implements class Interference2DSuperLattice.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Aggregate/Interference2DSuperLattice.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Functions.h"
#include "Base/Math/IntegratorGK.h"
#include "Base/Util/Assert.h"
#include "Sample/Aggregate/InterferenceNone.h"
#include <limits>

using PhysConsts::pi;

Interference2DSuperLattice::Interference2DSuperLattice(const Lattice2D& lattice, unsigned size_1,
                                                       unsigned size_2)
    : IInterference(0)
    , m_integrate_xi(false)
    , m_substructure(nullptr)
    , m_size_1(size_1)
    , m_size_2(size_2)
{
    m_lattice.reset(lattice.clone());
    setSubstructureIFF(InterferenceNone());
}

//! Constructor of two-dimensional interference function.
//! @param length_1: length of first lattice vector in nanometers
//! @param length_2: length of second lattice vector  in nanometers
//! @param alpha: angle between lattice vectors in radians
//! @param xi: rotation of lattice with respect to x-axis (beam direction) in radians
//! @param size_1: correlation length in direction 1
//! @param size_2: correlation length in direction 2
Interference2DSuperLattice::Interference2DSuperLattice(double length_1, double length_2,
                                                       double alpha, double xi, unsigned size_1,
                                                       unsigned size_2)
    : Interference2DSuperLattice(BasicLattice2D(length_1, length_2, alpha, xi), size_1, size_2)
{
}

Interference2DSuperLattice::~Interference2DSuperLattice() = default;

Interference2DSuperLattice* Interference2DSuperLattice::clone() const
{
    auto* result = new Interference2DSuperLattice(*m_lattice, m_size_1, m_size_2);
    result->setPositionVariance(m_position_var);
    result->setSubstructureIFF(*m_substructure);
    result->setIntegrationOverXi(integrationOverXi());
    return result;
}

void Interference2DSuperLattice::setSubstructureIFF(const IInterference& sub_iff)
{
    m_substructure.reset(sub_iff.clone());
}

const IInterference& Interference2DSuperLattice::substructureIFF() const
{
    return *m_substructure;
}

double Interference2DSuperLattice::structureFactor(const R3& q, double outer_iff) const
{
    if (!m_integrate_xi)
        return interferenceForXi(m_lattice->rotationAngle(), q.x(), q.y(), outer_iff);
    return RealIntegrator().integrate(
               [&](double xi) -> double { return interferenceForXi(xi, q.x(), q.y(), outer_iff); },
               0.0, (2 * pi))
           / (2 * pi);
}

void Interference2DSuperLattice::setIntegrationOverXi(bool integrate_xi)
{
    m_integrate_xi = integrate_xi;
    m_lattice->setRotationEnabled(!m_integrate_xi); // deregister Xi in the case of integration
}

const Lattice2D& Interference2DSuperLattice::lattice() const
{
    ASSERT(m_lattice);
    return *m_lattice;
}

std::vector<const INode*> Interference2DSuperLattice::nodeChildren() const
{
    return std::vector<const INode*>() << m_lattice << m_substructure;
}

double Interference2DSuperLattice::iff_without_dw(const R3&) const
{
    ASSERT_NEVER;
}

double Interference2DSuperLattice::iff_without_dw(const R3& q, double xi) const
{
    using Math::Laue;

    const double a = m_lattice->length1();
    const double b = m_lattice->length2();
    const double xialpha = xi + m_lattice->latticeAngle();

    const double qadiv2 = (q.x() * a * std::cos(xi) + q.y() * a * std::sin(xi)) / 2.0;
    const double qbdiv2 = (q.x() * b * std::cos(xialpha) + q.y() * b * std::sin(xialpha)) / 2.0;
    const double ampl = Laue(qadiv2, m_size_1) * Laue(qbdiv2, m_size_2);
    return ampl * ampl / (m_size_1 * m_size_2);
}

double Interference2DSuperLattice::interferenceForXi(double xi, double qx, double qy,
                                                     double outer_iff) const
{
    const R3 q = R3(qx, qy, 0.0);
    const double sub_iff = DWfactor(q) * (iff_without_dw(q, xi) * outer_iff - 1.0) + 1.0;
    const double delta_xi = xi - m_lattice->rotationAngle();
    return m_substructure->structureFactor(q.rotatedZ(-delta_xi), sub_iff);
}
