//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/Interference2DSuperLattice.h
//! @brief     Defines class Interference2DSuperLattice.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE2DSUPERLATTICE_H
#define BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE2DSUPERLATTICE_H

#include "Sample/Aggregate/IInterference.h"
#include "Sample/Lattice/Lattice2D.h"

//! Interference function of a 2D superlattice with a configurable interference function for
//! each lattice site.

class Interference2DSuperLattice : public IInterference {
public:
    Interference2DSuperLattice(const Lattice2D& lattice, unsigned size_1, unsigned size_2);
    Interference2DSuperLattice(double length_1, double length_2, double alpha, double xi,
                               unsigned size_1, unsigned size_2);
    ~Interference2DSuperLattice() override;

#ifndef SWIG
    Interference2DSuperLattice* clone() const override;

    std::vector<const INode*> nodeChildren() const override;
#endif // SWIG

    std::string className() const final { return "Interference2DSuperLattice"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length1", "nm"}, {"Length2", "nm"}, {"Alpha", "rad"}, {"Xi", "rad"}};
    }

    void setSubstructureIFF(const IInterference& sub_iff);
    const IInterference& substructureIFF() const;

    double structureFactor(const R3& q, double outer_iff = 1.0) const override;
    unsigned domainSize1() const { return m_size_1; }
    unsigned domainSize2() const { return m_size_2; }

    void setIntegrationOverXi(bool integrate_xi);
    bool integrationOverXi() const { return m_integrate_xi; }

    const Lattice2D& lattice() const;

private:
    double iff_without_dw(const R3& q) const override;
    double iff_without_dw(const R3& q, double xi) const;

    double interferenceForXi(double xi, double qx, double qy, double outer_iff) const;

    bool m_integrate_xi; //!< Integrate over the orientation xi
    std::unique_ptr<Lattice2D> m_lattice;
    std::unique_ptr<IInterference> m_substructure; //!< IFF of substructure
    unsigned m_size_1, m_size_2;                   //!< Size of the finite lattice in lattice units
};

#endif // BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCE2DSUPERLATTICE_H
