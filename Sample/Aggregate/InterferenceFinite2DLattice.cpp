//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/InterferenceFinite2DLattice.cpp
//! @brief     Implements class InterferenceFinite2DLattice.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Aggregate/InterferenceFinite2DLattice.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Functions.h"
#include "Base/Math/IntegratorGK.h"
#include "Base/Util/Assert.h"
#include <limits>

using Math::Laue;
using PhysConsts::pi;

//! Constructor of two-dimensional finite lattice interference function.
//! @param lattice: object specifying a 2d lattice structure
//! @param N_1: number of lattice cells in the first lattice direction
//! @param N_2: number of lattice cells in the second lattice direction
InterferenceFinite2DLattice::InterferenceFinite2DLattice(const Lattice2D& lattice, unsigned N_1,
                                                         unsigned N_2)
    : IInterference(0)
    , m_integrate_xi(false)
    , m_N_1(N_1)
    , m_N_2(N_2)
{
    m_lattice.reset(lattice.clone());
}

InterferenceFinite2DLattice::~InterferenceFinite2DLattice() = default;

InterferenceFinite2DLattice* InterferenceFinite2DLattice::clone() const
{
    auto* result = new InterferenceFinite2DLattice(*m_lattice, m_N_1, m_N_2);
    result->setPositionVariance(m_position_var);
    result->setIntegrationOverXi(integrationOverXi());
    return result;
}

void InterferenceFinite2DLattice::setIntegrationOverXi(bool integrate_xi)
{
    m_integrate_xi = integrate_xi;
    m_lattice->setRotationEnabled(!m_integrate_xi); // deregister Xi in the case of integration
}

const Lattice2D& InterferenceFinite2DLattice::lattice() const
{
    ASSERT(m_lattice);
    return *m_lattice;
}

double InterferenceFinite2DLattice::particleDensity() const
{
    double area = m_lattice->unitCellArea();
    return area == 0.0 ? 0.0 : 1.0 / area;
}

std::vector<const INode*> InterferenceFinite2DLattice::nodeChildren() const
{
    return std::vector<const INode*>() << m_lattice;
}

double InterferenceFinite2DLattice::iff_without_dw(const R3& q) const
{
    if (!m_integrate_xi)
        return interferenceForXi(m_lattice->rotationAngle(), q.x(), q.y());
    double range = pi;
    return RealIntegrator().integrate(
               [&](double xi) -> double { return interferenceForXi(xi, q.x(), q.y()); }, 0.0, range)
           / range;
}

double InterferenceFinite2DLattice::interferenceForXi(double xi, double qx, double qy) const
{
    double a = m_lattice->length1();
    double b = m_lattice->length2();
    double xialpha = xi + m_lattice->latticeAngle();

    double qadiv2 = (qx * a * std::cos(xi) + qy * a * std::sin(xi)) / 2.0;
    double qbdiv2 = (qx * b * std::cos(xialpha) + qy * b * std::sin(xialpha)) / 2.0;
    double ampl = Laue(qadiv2, m_N_1) * Laue(qbdiv2, m_N_2);
    double lattice_factor = ampl * ampl / (m_N_1 * m_N_2);

    return lattice_factor;
}
