//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/InterferenceFinite2DLattice.h
//! @brief     Defines class InterferenceFinite2DLattice.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCEFINITE2DLATTICE_H
#define BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCEFINITE2DLATTICE_H

#include "Sample/Aggregate/IInterference.h"
#include "Sample/Lattice/Lattice2D.h"

//! Interference function of a finite 2D lattice.

class InterferenceFinite2DLattice : public IInterference {
public:
    InterferenceFinite2DLattice(const Lattice2D& lattice, unsigned N_1, unsigned N_2);
    ~InterferenceFinite2DLattice() override;

#ifndef SWIG
    InterferenceFinite2DLattice* clone() const override;

    std::vector<const INode*> nodeChildren() const override;
#endif // SWIG

    std::string className() const final { return "InterferenceFinite2DLattice"; }

    unsigned numberUnitCells1() const { return m_N_1; }
    unsigned numberUnitCells2() const { return m_N_2; }

    void setIntegrationOverXi(bool integrate_xi);
    bool integrationOverXi() const { return m_integrate_xi; }

    const Lattice2D& lattice() const;

    //! Returns the particle density associated with this 2d lattice
    double particleDensity() const override;

private:
    double iff_without_dw(const R3& q) const override;

    double interferenceForXi(double xi, double qx, double qy) const;

    bool m_integrate_xi; //!< Integrate over the orientation xi
    std::unique_ptr<Lattice2D> m_lattice;
    unsigned m_N_1, m_N_2; //!< Size of the finite lattice in lattice units
};

#endif // BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCEFINITE2DLATTICE_H
