//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/InterferenceHardDisk.h
//! @brief     Defines class InterferenceHardDisk.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCEHARDDISK_H
#define BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCEHARDDISK_H

#include "Sample/Aggregate/IInterference.h"

//! Percus-Yevick hard disk interference function.
//!
//! M.S. Ripoll & C.F. Tejero (1995) Approximate analytical expression for the direct correlation
//! function of hard discs within the Percus-Yevick equation, Molecular Physics, 85:2, 423-428,
//! DOI: 10.1080/00268979500101211

class InterferenceHardDisk : public IInterference {
public:
    InterferenceHardDisk(double radius, double density, double position_var = 0);
    ~InterferenceHardDisk() override = default;
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Radius", "nm"}, {"Density", "nm^-2"}, {"Variance", "nm?"}};
    }

#ifndef SWIG
    InterferenceHardDisk* clone() const override;
#endif // SWIG

    std::string className() const final { return "InterferenceHardDisk"; }

    double particleDensity() const override { return m_density; }

    double radius() const { return m_radius; }
    double density() const { return m_density; }

    std::string validate() const override;

private:
    double iff_without_dw(const R3& q) const override;
    double packingRatio() const;

    double m_radius;
    double m_density;
};

#endif // BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCEHARDDISK_H
