//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/InterferenceNone.cpp
//! @brief     Implements class InterferenceNone.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Aggregate/InterferenceNone.h"

InterferenceNone::InterferenceNone()
    : IInterference(0)
{
}

InterferenceNone* InterferenceNone::clone() const
{
    auto* result = new InterferenceNone;
    result->setPositionVariance(m_position_var);
    return result;
}

double InterferenceNone::iff_without_dw(const R3&) const
{
    return 1.0;
}
