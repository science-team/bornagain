//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/InterferenceNone.h
//! @brief     Defines class InterferenceNone.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCENONE_H
#define BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCENONE_H

#include "Sample/Aggregate/IInterference.h"

//! Default interference function (i.e. absence of any interference).

class InterferenceNone : public IInterference {
public:
    InterferenceNone();

#ifndef SWIG
    InterferenceNone* clone() const override;
#endif // SWIG

    std::string className() const final { return "InterferenceNone"; }

private:
    double iff_without_dw(const R3& q) const override;
};

#endif // BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCENONE_H
