//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/InterferenceRadialParacrystal.h
//! @brief     Defines class InterferenceRadialParacrystal.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCERADIALPARACRYSTAL_H
#define BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCERADIALPARACRYSTAL_H

#include "Sample/Aggregate/IInterference.h"
#include "Sample/Correlation/Profiles1D.h"
#include <heinz/Complex.h>
#include <memory>

//! Interference function of radial paracrystal.

class InterferenceRadialParacrystal : public IInterference {
public:
    InterferenceRadialParacrystal(double peak_distance, double damping_length);

#ifndef SWIG
    InterferenceRadialParacrystal* clone() const override;

    std::vector<const INode*> nodeChildren() const override;
#endif // SWIG

    std::string className() const final { return "InterferenceRadialParacrystal"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"PeakDistance", "nm"}, {"DampingLength", "nm"}};
    }

    void setKappa(double kappa);
    double kappa() const { return m_kappa; }

    void setDomainSize(double size);
    double domainSize() const { return m_domain_size; }

    complex_t FTPDF(double qpar) const;

    void setProbabilityDistribution(const IProfile1D& pdf);

    double peakDistance() const { return m_peak_distance; }

    double dampingLength() const { return m_damping_length; }

    double randomSample(int seed) const { return m_pdf->createSampler()->randomSample(seed); }

    std::string validate() const override;

private:
    double iff_without_dw(const R3& q) const override;
    void init_parameters();

    double m_peak_distance;  //!< the distance to the first neighbor peak
    double m_damping_length; //!< damping length of paracrystal
    //! Fourier transformed probability distribution of the nearest particle
    std::unique_ptr<IProfile1D> m_pdf;
    double m_kappa;       //!< Size-spacing coupling parameter
    double m_domain_size; //!< Size of coherence domain
};

#endif // BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCERADIALPARACRYSTAL_H
