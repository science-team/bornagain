//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/Interferences.h
//! @brief     Includes all interference function definitions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCES_H
#define BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCES_H

#include "Sample/Aggregate/Interference1DLattice.h"
#include "Sample/Aggregate/Interference2DLattice.h"
#include "Sample/Aggregate/Interference2DParacrystal.h"
#include "Sample/Aggregate/Interference2DSuperLattice.h"
#include "Sample/Aggregate/InterferenceFinite2DLattice.h"
#include "Sample/Aggregate/InterferenceHardDisk.h"
#include "Sample/Aggregate/InterferenceNone.h"
#include "Sample/Aggregate/InterferenceRadialParacrystal.h"

#endif // BORNAGAIN_SAMPLE_AGGREGATE_INTERFERENCES_H
