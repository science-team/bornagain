//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/ParticleLayout.cpp
//! @brief     Implements class ParticleLayout.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Aggregate/ParticleLayout.h"
#include "Base/Util/StringUtil.h"
#include "Sample/Aggregate/InterferenceNone.h"
#include "Sample/Particle/Particle.h"

ParticleLayout::ParticleLayout() = default;

ParticleLayout::ParticleLayout(const IParticle& particle)
{
    addParticle(particle, 1.0);
}

ParticleLayout::~ParticleLayout() = default; // needs member class definitions => don't move to .h

ParticleLayout* ParticleLayout::clone() const
{
    auto* result = new ParticleLayout;

    result->m_particles = m_particles;
    if (m_interparticle)
        result->m_interparticle.reset(m_interparticle->clone());
    result->setTotalParticleSurfaceDensity(totalParticleSurfaceDensity());
    result->setAbsoluteWeight(m_abs_weight);

    return result;
}

std::vector<const INode*> ParticleLayout::nodeChildren() const
{
    std::vector<const INode*> result;
    for (const IParticle* p : m_particles)
        result.push_back(p);
    result << m_interparticle;
    return result;
}

//! Adds particle to the layout with abundance, position and the rotation defined.
//! @param particle to be added
//! @param abundance Particle abundance
void ParticleLayout::addParticle(const IParticle& particle, double abundance)
{
    IParticle* p = particle.clone();
    if (abundance >= 0.0)
        p->setAbundance(abundance);
    m_particles.push_back(p);
}

std::vector<const IParticle*> ParticleLayout::particles() const
{
    std::vector<const IParticle*> result;
    for (const IParticle* p : m_particles)
        result.push_back(p);
    return result;
}

const IInterference* ParticleLayout::interferenceFunction() const
{
    return m_interparticle.get();
}

double ParticleLayout::totalAbundance() const
{
    double result = 0.0;
    for (const auto& particle : m_particles)
        result += particle->abundance();
    return result;
}

//! Adds interference functions
void ParticleLayout::setInterference(const IInterference& interparticle)
{
    m_interparticle.reset(interparticle.clone());
}

double ParticleLayout::totalParticleSurfaceDensity() const
{
    double iff_density = m_interparticle ? m_interparticle->particleDensity() : 0.0;
    return iff_density > 0.0 ? iff_density : m_total_particle_density;
}

//! Sets total particle surface density.
//! @param particle_density: number of particles per square nanometer
void ParticleLayout::setTotalParticleSurfaceDensity(double particle_density)
{
    m_total_particle_density = particle_density;
}

std::string ParticleLayout::validate() const
{
    std::vector<std::string> errs;
    for (size_t i = 0; i < m_particles.size(); ++i) {
        std::string err = m_particles[i]->validate();
        if (!err.empty())
            errs.push_back("{ particle " + std::to_string(i) + ": " + err + " }");
    }

    if (!errs.empty())
        return "[ " + Base::String::join(errs, ", ") + " ]";
    return "";
}
