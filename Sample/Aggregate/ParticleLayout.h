//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Aggregate/ParticleLayout.h
//! @brief     Defines class ParticleLayout.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_AGGREGATE_PARTICLELAYOUT_H
#define BORNAGAIN_SAMPLE_AGGREGATE_PARTICLELAYOUT_H

#include "Base/Type/CloneableVector.h"
#include "Sample/Scattering/ISampleNode.h"
#include <memory>

class IInterference;
class IParticle;

//! Decorator class that adds particles to ISampleNode objects.

class ParticleLayout : public ISampleNode {
public:
    ParticleLayout();
    ParticleLayout(const IParticle& particle);
    ~ParticleLayout() override;

    std::string className() const final { return "ParticleLayout"; }

    void addParticle(const IParticle& particle, double abundance = -1.0);
    void setInterference(const IInterference& interparticle);
    void setTotalParticleSurfaceDensity(double particle_density);
    void setAbsoluteWeight(double weight) { m_abs_weight = weight; }

#ifndef SWIG
    ParticleLayout* clone() const override;

    std::string validate() const override;

    std::vector<const INode*> nodeChildren() const override;

    std::vector<ParaMeta> parDefs() const final { return {{"Abundance", ""}}; }

    double totalAbundance() const;
    double totalParticleSurfaceDensity() const;
    double absoluteWeight() const { return m_abs_weight; }

    std::vector<const IParticle*> particles() const;
    const IInterference* interferenceFunction() const;

private:
    double m_abs_weight = 1; //!< Weight factor, independent of particle density
    double m_total_particle_density{.01};
    CloneableVector<IParticle> m_particles; //!< Vector of particle types
    std::unique_ptr<IInterference> m_interparticle;
#endif // SWIG
};

#endif // BORNAGAIN_SAMPLE_AGGREGATE_PARTICLELAYOUT_H
