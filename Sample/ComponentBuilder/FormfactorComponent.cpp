//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/ComponentBuilder/FormfactorComponent.cpp
//! @brief     Implements sample components for complex sample builders.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/ComponentBuilder/FormfactorComponent.h"
#include "Base/Const/Units.h"

//  ************************************************************************************************
FormfactorComponent::FormfactorComponent()
//  ************************************************************************************************
{
    add("Pyramid2", new Pyramid2(10.0, 20.0, 5.0, Units::deg2rad(54.73)));

    add("Box", new Box(10.0, 20.0, 5.0));

    add("CantellatedCube", new CantellatedCube(15.0, 6.0));

    add("Cone", new Cone(5.0, 6.0, Units::deg2rad(54.73)));

    add("Pyramid6", new Pyramid6(2. / sqrt(3.) * 5.0, 5.0, Units::deg2rad(54.73)));

    add("Bipyramid4", new Bipyramid4(10.0, 5.0, 1.0, Units::deg2rad(54.73)));

    add("Cylinder", new Cylinder(5.0, 10.0));

    add("HorizontalCylinder", new HorizontalCylinder(5.0, 10.0));

    add("Dodecahedron", new Dodecahedron(5.0));

    add("EllipsoidalCylinder", new EllipsoidalCylinder(5.0, 10.0, 15.0));

    add("Sphere", new Sphere(5.0));

    add("Spheroid", new Spheroid(5.0, 5.0));

    add("HemiEllipsoid", new HemiEllipsoid(5.0, 10.0, 15.0));

    add("Icosahedron", new Icosahedron(10.0));

    add("PlatonicOctahedron", new PlatonicOctahedron(10.0));

    add("PlatonicTetrahedron", new PlatonicTetrahedron(10.0));

    add("Prism3", new Prism3(10.0, 5.0));

    add("Prism6", new Prism6(2. / sqrt(3.) * 5.0, 5.0));

    add("Pyramid4", new Pyramid4(10.0, 5.0, Units::deg2rad(54.73)));

    add("CosineRippleBox", new CosineRippleBox(100.0, 20.0, 4.0));

    add("SawtoothRippleBox", new SawtoothRippleBox(100.0, 20.0, 4.0, 0.0));

    add("Pyramid3", new Pyramid3(10.0, 4.0, Units::deg2rad(54.73)));

    add("TruncatedCube", new TruncatedCube(15.0, 6.0));

    add("SphericalSegment", new SphericalSegment(5.0, 0, 3.0));

    add("SpheroidalSegment", new SpheroidalSegment(5.0, 5.0, 0, 3.0));
}
