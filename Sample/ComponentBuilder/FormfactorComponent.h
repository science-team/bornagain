//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/ComponentBuilder/FormfactorComponent.h
//! @brief     Defines sample components for complex sample builders.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_COMPONENTBUILDER_FORMFACTORCOMPONENT_H
#define BORNAGAIN_SAMPLE_COMPONENTBUILDER_FORMFACTORCOMPONENT_H

#include "Sample/ComponentBuilder/IRegistry.h"
#include "Sample/HardParticle/HardParticles.h"

//! Predefined form factors for functional tests.

class FormfactorComponent : public IRegistry<IFormfactor> {
public:
    FormfactorComponent();
};

#endif // BORNAGAIN_SAMPLE_COMPONENTBUILDER_FORMFACTORCOMPONENT_H
