//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/ComponentBuilder/IRegistry.h
//! @brief     Defines templated registry for ICloneable objects.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_COMPONENTBUILDER_IREGISTRY_H
#define BORNAGAIN_SAMPLE_COMPONENTBUILDER_IREGISTRY_H

#include "Base/Util/Assert.h"
#include <map>
#include <memory>
#include <string>
#include <vector>

//! Templated object registry.

template <typename T> class IRegistry {
public:
    const T* getItem(const std::string& key) const
    {
        auto it = m_data.find(key);
        ASSERT(it != m_data.end());
        return it->second.get();
    }

    std::vector<std::string> keys() const
    {
        std::vector<std::string> result;
        for (const auto& it : m_data)
            result.push_back(it.first);
        return result;
    }

    size_t size() const { return m_data.size(); }

protected:
    void add(const std::string& key, T* item)
    {
        ASSERT(m_data.find(key) == m_data.end());
        m_data[key] = std::unique_ptr<T>(item);
    }

private:
    std::map<std::string, std::unique_ptr<T>> m_data;
};

#endif // BORNAGAIN_SAMPLE_COMPONENTBUILDER_IREGISTRY_H
