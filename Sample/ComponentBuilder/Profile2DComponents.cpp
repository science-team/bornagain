//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/ComponentBuilder/Profile2DComponents.cpp
//! @brief     Implements sample components for complex sample builders.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/ComponentBuilder/Profile2DComponents.h"

//  ************************************************************************************************
Profile2DComponents::Profile2DComponents()
//  ************************************************************************************************
{
    add("Profile2DCauchy", new Profile2DCauchy(0.5, 1.0, 0));
    add("Profile2DGauss", new Profile2DGauss(0.5, 1.0, 0));
    add("Profile2DGate", new Profile2DGate(0.5, 1.0, 0));
    add("Profile2DCone", new Profile2DCone(0.5, 1.0, 0));
    add("Profile2DVoigt", new Profile2DVoigt(0.5, 1.0, 0, 0.2));
}
