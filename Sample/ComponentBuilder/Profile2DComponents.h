//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/ComponentBuilder/Profile2DComponents.h
//! @brief     Defines sample components for complex sample builders.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_COMPONENTBUILDER_PROFILE2DCOMPONENTS_H
#define BORNAGAIN_SAMPLE_COMPONENTBUILDER_PROFILE2DCOMPONENTS_H

#include "Sample/ComponentBuilder/IRegistry.h"
#include "Sample/Correlation/Profiles2D.h"

//! Predefined Fourier transformed distributions for functional tests.

class Profile2DComponents : public IRegistry<IProfile2D> {
public:
    Profile2DComponents();
};

#endif // BORNAGAIN_SAMPLE_COMPONENTBUILDER_PROFILE2DCOMPONENTS_H
