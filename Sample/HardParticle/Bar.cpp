//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Bar.cpp
//! @brief     Implements classes Bar*.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/Bar.h"
#include "Base/Util/Assert.h"
#include "Sample/HardParticle/Ripples.h"

//  ************************************************************************************************
//  class BarGauss
//  ************************************************************************************************

BarGauss::BarGauss(const std::vector<double> P)
    : IProfileRectangularRipple(P)
{
    validateOrThrow();
}

BarGauss::BarGauss(double length, double width, double height)
    : BarGauss(std::vector<double>{length, width, height})
{
}

BarGauss* BarGauss::clone() const
{
    return new BarGauss(m_length, m_width, m_height);
}

bool BarGauss::contains(const R3&) const
{
    throw std::runtime_error("BarGauss cannot be used as mesocrystal outer shape");
}

complex_t BarGauss::factor_x(complex_t qx) const
{
    ASSERT(m_validated);
    return ripples::factor_x_Gauss(qx, m_length);
}

//  ************************************************************************************************
//  class BarLorentz
//  ************************************************************************************************

BarLorentz::BarLorentz(const std::vector<double> P)
    : IProfileRectangularRipple(P)
{
    validateOrThrow();
}

BarLorentz::BarLorentz(double length, double width, double height)
    : BarLorentz(std::vector<double>{length, width, height})
{
}

BarLorentz* BarLorentz::clone() const
{
    return new BarLorentz(m_length, m_width, m_height);
}

bool BarLorentz::contains(const R3&) const
{
    throw std::runtime_error("BarLorentz cannot be used as mesocrystal outer shape");
}

complex_t BarLorentz::factor_x(complex_t qx) const
{
    ASSERT(m_validated);
    return ripples::factor_x_Lorentz(qx, m_length);
}
