//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Bar.h
//! @brief     Defines classes Bar*.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_BAR_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_BAR_H

#include "Sample/HardParticle/IProfileRipple.h"

//! The form factor of an elongated bar, with Gaussian profile in elongation direction.
class BarGauss : public IProfileRectangularRipple {
public:
    BarGauss(double length, double width, double height);
    BarGauss(std::vector<double> P);

#ifndef SWIG
    BarGauss* clone() const override;
#endif // SWIG

    std::string className() const final { return "BarGauss"; }

    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}};
    }

    bool contains(const R3& position) const override;

private:
    complex_t factor_x(complex_t qx) const override;
};

//! The form factor of an elongated, with Lorentz form factor in elongation direction.
class BarLorentz : public IProfileRectangularRipple {
public:
    BarLorentz(double length, double width, double height);
    BarLorentz(std::vector<double> P);

#ifndef SWIG
    BarLorentz* clone() const override;
#endif // SWIG

    std::string className() const final { return "BarLorentz"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}};
    }

    bool contains(const R3& position) const override;

private:
    complex_t factor_x(complex_t qx) const override;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_BAR_H
