//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Cone.cpp
//! @brief     Implements class Cone.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/Cone.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Bessel.h"
#include "Base/Math/Functions.h"
#include "Base/Math/IntegratorGK.h"
#include "Base/Util/Assert.h"
#include "Sample/Shape/DoubleEllipse.h"
#include <limits>

using PhysConsts::pi;

Cone::Cone(const std::vector<double> P)
    : IFormfactor(P)
    , m_radius(m_P[0])
    , m_height(m_P[1])
    , m_alpha(m_P[2])
{
    validateOrThrow();
}

Cone::Cone(double radius, double height, double alpha)
    : Cone(std::vector<double>{radius, height, alpha})
{
}

complex_t Cone::formfactor(C3 q) const
{
    ASSERT(m_validated);
    const double R = m_radius;
    const double H = m_height;

    if (std::abs(q.mag()) < std::numeric_limits<double>::epsilon()) {
        if (m_cot_alpha == 0.0)
            return pi * R * R * H; // cylinder case
        double R2 = R - H * m_cot_alpha;
        double apex_height = R / m_cot_alpha;
        return pi / 3. * (R * R * H + (R * R - R2 * R2) * (apex_height - H));
    }

    const complex_t q_p = std::sqrt(q.x() * q.x() + q.y() * q.y());

    return (2 * pi)
           * ComplexIntegrator().integrate(
               [&](double Z) {
                   const double Rz = R - Z * m_cot_alpha;
                   return Rz * Rz * Math::Bessel::J1c(q_p * Rz) * exp_I(q.z() * Z);
               },
               0., H);
}

std::string Cone::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_radius, "radius");
    requestGt0(errs, m_height, "height");
    m_cot_alpha = Math::cot(m_alpha);
    if (m_alpha <= 0 || !std::isfinite(m_cot_alpha) || m_cot_alpha < 0)
        errs.push_back("cone angle alpha " + std::to_string(m_alpha) + " out of bounds");
    if (!errs.empty())
        return jointError(errs);

    if (m_cot_alpha * m_height > m_radius) {
        std::ostringstream ostr;
        ostr << "Cone -> Error in class initialization ";
        ostr << "with parameters radius:" << m_radius;
        ostr << " m_height:" << m_height;
        ostr << " alpha[rad]:" << m_alpha << "\n\n";
        ostr << "Check for 'height <= radius*tan(alpha)' failed.";
        return jointError({"parameters violate condition height <= radius*tan(alpha)"});
    }

    m_cot_alpha = Math::cot(m_alpha);
    double radius2 = m_radius - m_height * m_cot_alpha;
    m_shape3D = std::make_unique<DoubleEllipseZ>(m_radius, m_radius, m_height, radius2, radius2);

    m_validated = true;
    return "";
}

bool Cone::contains(const R3& position) const
{
    if (std::abs(position.x()) > radius() || std::abs(position.y()) > radius() || position.z() < 0
        || position.z() > height())
        return false;

    double R_z = radius() - position.z() / std::tan(alpha());
    if (pow(position.x() / R_z, 2) + pow(position.y() / R_z, 2) <= 1)
        return true;

    return false;
}
