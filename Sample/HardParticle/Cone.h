//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Cone.h
//! @brief     Defines class Cone.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_CONE_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_CONE_H

#include "Sample/Particle/IFormfactor.h"

//! A conical frustum (cone truncated parallel to the base) with circular base.

class Cone : public IFormfactor {
public:
    Cone(double radius, double height, double alpha);
    Cone(std::vector<double> P);

#ifndef SWIG
    Cone* clone() const override { return new Cone(m_radius, m_height, m_alpha); }
#endif // SWIG

    std::string className() const final { return "Cone"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Radius", "nm"}, {"Height", "nm"}, {"Alpha", "rad"}};
    }

    double radius() const { return m_radius; }
    double height() const { return m_height; }
    double alpha() const { return m_alpha; }

    double radialExtension() const override { return m_radius; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius;
    const double& m_height;
    const double& m_alpha;
    mutable double m_cot_alpha;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_CONE_H
