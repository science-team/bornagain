//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/CosineRipple.cpp
//! @brief     Implements classes CosineRipple*.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/CosineRipple.h"
#include "Sample/HardParticle/Ripples.h"

//  ************************************************************************************************
//  class CosineRippleBox
//  ************************************************************************************************

CosineRippleBox::CosineRippleBox(const std::vector<double> P)
    : ICosineRipple(P)
{
    validateOrThrow();
}

CosineRippleBox::CosineRippleBox(double length, double width, double height)
    : CosineRippleBox(std::vector<double>{length, width, height})
{
}

CosineRippleBox* CosineRippleBox::clone() const
{
    return new CosineRippleBox(m_length, m_width, m_height);
}

bool CosineRippleBox::contains(const R3&) const
{
    // TODO: Implement CosineRippleBox
    std::ostringstream ostr;
    ostr << "Outer shape CosineRippleBox not yet implemented for Mesocrystal";
    ostr << "\n\nStay tuned!";
    throw std::runtime_error(ostr.str());
}

complex_t CosineRippleBox::factor_x(complex_t qx) const
{
    return ripples::factor_x_box(qx, m_length);
}

//  ************************************************************************************************
//  class CosineRippleGauss
//  ************************************************************************************************

CosineRippleGauss::CosineRippleGauss(const std::vector<double> P)
    : ICosineRipple(P)
{
    validateOrThrow();
}

CosineRippleGauss::CosineRippleGauss(double length, double width, double height)
    : CosineRippleGauss(std::vector<double>{length, width, height})
{
}

CosineRippleGauss* CosineRippleGauss::clone() const
{
    return new CosineRippleGauss(m_length, m_width, m_height);
}

bool CosineRippleGauss::contains(const R3&) const
{
    throw std::runtime_error("CosineRippleGauss cannot be used as mesocrystal outer shape");
}

complex_t CosineRippleGauss::factor_x(complex_t qx) const
{
    return ripples::factor_x_Gauss(qx, m_length);
}

//  ************************************************************************************************
//  class CosineRippleLorentz
//  ************************************************************************************************

CosineRippleLorentz::CosineRippleLorentz(const std::vector<double> P)
    : ICosineRipple(P)
{
    validateOrThrow();
}

CosineRippleLorentz::CosineRippleLorentz(double length, double width, double height)
    : CosineRippleLorentz(std::vector<double>{length, width, height})
{
}

CosineRippleLorentz* CosineRippleLorentz::clone() const
{
    return new CosineRippleLorentz(m_length, m_width, m_height);
}

bool CosineRippleLorentz::contains(const R3&) const
{
    throw std::runtime_error("CosineRippleLorentz cannot be used as mesocrystal outer shape");
}

complex_t CosineRippleLorentz::factor_x(complex_t qx) const
{
    return ripples::factor_x_Lorentz(qx, m_length);
}
