//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/CosineRipple.h
//! @brief     Defines classes CosineRipple*.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_COSINERIPPLE_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_COSINERIPPLE_H

#include "Sample/HardParticle/IProfileRipple.h"

//! The form factor for a cosine ripple, with box profile in elongation direction.
class CosineRippleBox : public ICosineRipple {
public:
    CosineRippleBox(double length, double width, double height);
    CosineRippleBox(std::vector<double> P);

#ifndef SWIG
    CosineRippleBox* clone() const override;
#endif // SWIG

    std::string className() const final { return "CosineRippleBox"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}};
    }

    bool contains(const R3& position) const override;

private:
    complex_t factor_x(complex_t qx) const override;
};

//! The form factor for a cosine ripple, with Gaussian profile in elongation direction.
class CosineRippleGauss : public ICosineRipple {
public:
    CosineRippleGauss(double length, double width, double height);
    CosineRippleGauss(std::vector<double> P);

#ifndef SWIG
    CosineRippleGauss* clone() const override;
#endif // SWIG

    std::string className() const final { return "CosineRippleGauss"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}};
    }

    bool contains(const R3& position) const override;

private:
    complex_t factor_x(complex_t qx) const override;
};

//! The form factor for a cosine ripple, with Lorentz form factor in elongation direction.
class CosineRippleLorentz : public ICosineRipple {
public:
    CosineRippleLorentz(double length, double width, double height);
    CosineRippleLorentz(std::vector<double> P);

#ifndef SWIG
    CosineRippleLorentz* clone() const override;
#endif // SWIG

    std::string className() const final { return "CosineRippleLorentz"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}};
    }

    bool contains(const R3& position) const override;

private:
    complex_t factor_x(complex_t qx) const override;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_COSINERIPPLE_H
