//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Cylinder.cpp
//! @brief     Implements class Cylinder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/Cylinder.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Bessel.h"
#include "Base/Math/Functions.h"
#include "Base/Util/Assert.h"
#include "Sample/Shape/DoubleEllipse.h"

using PhysConsts::pi;

Cylinder::Cylinder(const std::vector<double> P)
    : IFormfactor(P)
    , m_radius(m_P[0])
    , m_height(m_P[1])
{
    validateOrThrow();
}

Cylinder::Cylinder(double radius, double height)
    : Cylinder(std::vector<double>{radius, height})
{
}

complex_t Cylinder::formfactor(C3 q) const
{
    ASSERT(m_validated);
    const double R = m_radius;
    const double H = m_height;

    const complex_t qH2 = q.z() * H / 2.;
    const complex_t qR = std::sqrt(q.x() * q.x() + q.y() * q.y()) * R;

    const complex_t axial_part = H * Math::sinc(qH2);
    const complex_t radial_part = (2 * pi) * R * R * Math::Bessel::J1c(qR);

    return radial_part * axial_part * exp_I(qH2);
}

std::string Cylinder::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_radius, "radius");
    requestGt0(errs, m_height, "height");
    if (!errs.empty())
        return jointError(errs);

    // TODO improve!
    m_shape3D = std::make_unique<DoubleEllipseZ>(m_radius, m_radius, m_height, m_radius, m_radius);

    m_validated = true;
    return "";
}

bool Cylinder::contains(const R3& position) const
{
    double R = radius();
    double H = height();

    if (std::abs(position.x()) > R || std::abs(position.y()) > R || position.z() < 0
        || position.z() > H)
        return false;

    if (std::pow(position.x() / R, 2) + std::pow(position.y() / R, 2) <= 1)
        return true;

    return false;
}
