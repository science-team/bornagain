//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Cylinder.h
//! @brief     Defines class Cylinder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_CYLINDER_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_CYLINDER_H

#include "Sample/Particle/IFormfactor.h"

//! A circular cylinder.

class Cylinder : public IFormfactor {
public:
    Cylinder(double radius, double height);
    Cylinder(std::vector<double> P);

#ifndef SWIG
    Cylinder* clone() const override { return new Cylinder(m_radius, m_height); }
#endif // SWIG

    std::string className() const final { return "Cylinder"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Radius", "nm"}, {"Height", "nm"}}; }

    double height() const { return m_height; }
    double radius() const { return m_radius; }

    double radialExtension() const override { return m_radius; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius;
    const double& m_height;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_CYLINDER_H
