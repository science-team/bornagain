//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/EllipsoidalCylinder.h
//! @brief     Defines class EllipsoidalCylinder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_ELLIPSOIDALCYLINDER_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_ELLIPSOIDALCYLINDER_H

#include "Sample/Particle/IFormfactor.h"

//! A cylinder with elliptical base.

class EllipsoidalCylinder : public IFormfactor {
public:
    EllipsoidalCylinder(double radius_x, double radius_y, double height);
    EllipsoidalCylinder(std::vector<double> P);

#ifndef SWIG
    EllipsoidalCylinder* clone() const override
    {
        return new EllipsoidalCylinder(m_radius_x, m_radius_y, m_height);
    }
#endif // SWIG

    std::string className() const final { return "EllipsoidalCylinder"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"RadiusX", "nm"}, {"RadiusY", "nm"}, {"Height", "nm"}};
    }

    double radiusX() const { return m_radius_x; }
    double radiusY() const { return m_radius_y; }
    double height() const { return m_height; }

    double radialExtension() const override;

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius_x;
    const double& m_radius_y;
    const double& m_height;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_ELLIPSOIDALCYLINDER_H
