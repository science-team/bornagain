//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/HardParticles.h
//! @brief     Includes all particle-shape form-factor definitions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_HARDPARTICLES_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_HARDPARTICLES_H

#include "Sample/HardParticle/Bar.h"
#include "Sample/HardParticle/Cone.h"
#include "Sample/HardParticle/CosineRipple.h"
#include "Sample/HardParticle/Cylinder.h"
#include "Sample/HardParticle/EllipsoidalCylinder.h"
#include "Sample/HardParticle/HemiEllipsoid.h"
#include "Sample/HardParticle/HorizontalCylinder.h"
#include "Sample/HardParticle/LongBoxGauss.h"
#include "Sample/HardParticle/LongBoxLorentz.h"
#include "Sample/HardParticle/Polyhedra.h"
#include "Sample/HardParticle/SawtoothRipple.h"
#include "Sample/HardParticle/Sphere.h"
#include "Sample/HardParticle/SphericalSegment.h"
#include "Sample/HardParticle/Spheroid.h"
#include "Sample/HardParticle/SpheroidalSegment.h"

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_HARDPARTICLES_H
