//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/HemiEllipsoid.h
//! @brief     Defines class HemiEllipsoid.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_HEMIELLIPSOID_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_HEMIELLIPSOID_H

#include "Sample/Particle/IFormfactor.h"

//! An hemi ellipsoid,
//!   obtained by truncating a full ellipsoid in the middle plane spanned by two principal axes.

class HemiEllipsoid : public IFormfactor {
public:
    HemiEllipsoid(double radius_x, double radius_y, double radius_z);
    HemiEllipsoid(std::vector<double> P);
    ~HemiEllipsoid() override = default;

#ifndef SWIG
    HemiEllipsoid* clone() const override
    {
        return new HemiEllipsoid(m_radius_x, m_radius_y, m_radius_z);
    }
#endif // SWIG

    std::string className() const final { return "HemiEllipsoid"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"RadiusX", "nm"}, {"RadiusY", "nm"}, {"Height", "nm"}};
    }

    double radiusX() const { return m_radius_x; }
    double radiusY() const { return m_radius_y; }
    double radiusZ() const { return m_radius_z; }

    double radialExtension() const override;

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius_x;
    const double& m_radius_y;
    const double& m_radius_z; //!< radius in +z direction
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_HEMIELLIPSOID_H
