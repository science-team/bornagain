//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/HorizontalCylinder.cpp
//! @brief     Implements class HorizontalCylinder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/HorizontalCylinder.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Bessel.h"
#include "Base/Math/Functions.h"
#include "Base/Math/IntegratorGK.h"
#include "Base/Util/Assert.h"
#include "Sample/Shape/DoubleEllipse.h"

using PhysConsts::pi;

HorizontalCylinder::HorizontalCylinder(const std::vector<double> P)
    : IFormfactor(P)
    , m_radius(m_P[0])
    , m_length(m_P[1])
    , m_slice_bottom(m_P[2])
    , m_slice_top(m_P[3])
{
    validateOrThrow();
}

HorizontalCylinder::HorizontalCylinder(double radius, double length, double slice_bottom,
                                       double slice_top)
    : HorizontalCylinder(std::vector<double>{radius, length, slice_bottom, slice_top})
{
}

HorizontalCylinder::HorizontalCylinder(double radius, double length)
    : HorizontalCylinder(radius, length, -radius, +radius)
{
}

complex_t HorizontalCylinder::formfactor(C3 q) const
{
    ASSERT(m_validated);
    const double R = m_radius;
    const double L = m_length;

    const complex_t qL2 = q.x() * L / 2.;
    const complex_t qR = std::sqrt(q.y() * q.y() + q.z() * q.z()) * R;

    const complex_t axial_part = L * Math::sinc(qL2);
    complex_t radial_part;

    if (m_slice_bottom == -R && m_slice_top == +R)
        radial_part = (2 * pi) * R * R * Math::Bessel::J1c(qR) * exp_I(q.z() * R);

    else
        // integration variable substituted as z = R * sin(t)
        radial_part = 2. * pow(R, 2) * exp_I(q.z() * (-m_slice_bottom))
                      * ComplexIntegrator().integrate(
                          [=](double t) {
                              return pow(cos(t), 2) * Math::sinc(q.y() * R * cos(t))
                                     * exp_I(q.z() * R * sin(t));
                          },
                          asin(m_slice_bottom / R), asin(m_slice_top / R));

    return radial_part * axial_part;
}

std::string HorizontalCylinder::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_radius, "radius");
    requestGt0(errs, m_length, "length");
    if (m_slice_bottom < -m_radius)
        errs.push_back("slice_bottom=" + std::to_string(m_slice_bottom) + "<-R");
    if (m_slice_top > m_radius)
        errs.push_back("slice_top=" + std::to_string(m_slice_top) + "<R");
    if (m_slice_bottom >= m_slice_top)
        errs.emplace_back("parameters violate condition bottom<top");
    if (!errs.empty())
        return jointError(errs);

    // TODO improve!
    m_shape3D = std::make_unique<DoubleEllipseX>(-m_length / 2, m_radius, m_radius, m_slice_bottom,
                                                 m_slice_top, m_length / 2, m_radius, m_radius,
                                                 m_slice_bottom, m_slice_top);

    m_validated = true;
    return "";
}

bool HorizontalCylinder::contains(const R3&) const
{
    // TODO: Implement Horizontal cylinder
    std::ostringstream ostr;
    ostr << "Outer shape Horizontal cylinder not yet implemented for Mesocrystal";
    ostr << "\n\nStay tuned!";
    throw std::runtime_error(ostr.str());
}
