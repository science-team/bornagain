//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/HorizontalCylinder.h
//! @brief     Defines class Cylinder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_HORIZONTALCYLINDER_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_HORIZONTALCYLINDER_H

#include "Sample/Particle/IFormfactor.h"

//! A circular cylinder.

class HorizontalCylinder : public IFormfactor {
public:
    HorizontalCylinder(double radius, double length, double slice_bottom, double slice_top);
    HorizontalCylinder(double radius, double length);
    HorizontalCylinder(std::vector<double> P);

#ifndef SWIG
    HorizontalCylinder* clone() const override
    {
        return new HorizontalCylinder(m_radius, m_length, m_slice_bottom, m_slice_top);
    }
#endif // SWIG

    std::string className() const final { return "HorizontalCylinder"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Radius", "nm"}, {"Length", "nm"}, {"Slice_bottom", "nm"}, {"Slice_top", "nm"}};
    }

    double length() const { return m_length; }
    double radius() const { return m_radius; }
    double slice_bottom() const { return m_slice_bottom; }
    double slice_top() const { return m_slice_top; }
    double height() const { return 2 * m_radius; }

    double radialExtension() const override { return m_radius; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius;
    const double& m_length;
    const double& m_slice_bottom;
    const double& m_slice_top;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_HORIZONTALCYLINDER_H
