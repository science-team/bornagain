//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/IFormfactorPolyhedron.cpp
//! @brief     Implements interface IFormfactorPolyhedron.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

//! The mathematics implemented here is described in full detail in a paper
//! by Joachim Wuttke, entitled
//! "Form factor (Fourier shape transform) of polygon and polyhedron."

#include "Sample/HardParticle/IFormfactorPolyhedron.h"
#include "Base/Type/Span.h"
#include "Base/Util/Assert.h"
#include "Sample/Particle/PolyhedralUtil.h"
#include <ff/Polyhedron.h>

IFormfactorPolyhedron::IFormfactorPolyhedron(const std::vector<double>& PValues)
    : IFormfactor(PValues)
{
}

IFormfactorPolyhedron::~IFormfactorPolyhedron() = default;

//! Called by child classes to set faces and other internal variables.

void IFormfactorPolyhedron::setPolyhedron(const ff::Topology& topology, double z_bottom,
                                          const std::vector<R3>& vertices) const
{
    pimpl = std::make_unique<ff::Polyhedron>(topology, vertices, R3(0, 0, -z_bottom));
}

double IFormfactorPolyhedron::volume() const
{
    ASSERT(m_validated);
    return pimpl->volume();
}

double IFormfactorPolyhedron::radialExtension() const
{
    ASSERT(m_validated);
    return pimpl->radius();
}

Span IFormfactorPolyhedron::spanZ(const IRotation* rotation) const
{
    ASSERT(m_validated);
    return PolyhedralUtil::spanZ(pimpl->vertices(), rotation);
}

complex_t IFormfactorPolyhedron::formfactor(C3 q) const
{
    ASSERT(m_validated);
    return pimpl->formfactor(q);
}
