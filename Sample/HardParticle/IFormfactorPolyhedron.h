//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/IFormfactorPolyhedron.h
//! @brief     Defines interface IFormfactorPolyhedron.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_IFORMFACTORPOLYHEDRON_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_IFORMFACTORPOLYHEDRON_H

#include "Sample/Particle/IFormfactor.h"
#include <ff/Topology.h> // needed by all child classes
#include <memory>

namespace ff {
class IBody;
}

//! A polyhedron, for form factor computation.

class IFormfactorPolyhedron : public IFormfactor {
public:
    IFormfactorPolyhedron(const std::vector<double>& PValues);
    ~IFormfactorPolyhedron() override;

    double volume() const override;
    double radialExtension() const override;

    complex_t formfactor(C3 q) const override;
    Span spanZ(const IRotation* rotation) const override;

protected:
    void setPolyhedron(const ff::Topology& topology, double z_bottom,
                       const std::vector<R3>& vertices) const;

    mutable std::unique_ptr<ff::IBody> pimpl;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_IFORMFACTORPOLYHEDRON_H
