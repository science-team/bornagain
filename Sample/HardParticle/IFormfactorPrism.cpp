//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/IFormfactorPrism.cpp
//! @brief     Implements interface IFormfactorPrism.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/IFormfactorPrism.h"
#include "Base/Util/Assert.h"
#include "Sample/Particle/PolyhedralUtil.h"
#include <ff/Prism.h>

IFormfactorPrism::IFormfactorPrism(const std::vector<double>& PValues)
    : IFormfactorPolyhedron(PValues)
{
}

IFormfactorPrism::~IFormfactorPrism() = default;
