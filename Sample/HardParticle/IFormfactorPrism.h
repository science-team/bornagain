//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/IFormfactorPrism.h
//! @brief     Defines interface IFormfactorPrism.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_IFORMFACTORPRISM_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_IFORMFACTORPRISM_H

#include "Sample/HardParticle/IFormfactorPolyhedron.h"

//! A prism with a polygonal base, for form factor computation.

class IFormfactorPrism : public IFormfactorPolyhedron {
public:
    IFormfactorPrism(const std::vector<double>& PValues);
    ~IFormfactorPrism() override;

    virtual double height() const = 0;

    // Each child must have a parameter m_height. We would like to move it here,
    // but this seems incompatible with the current initialization of m_P and
    // with the order of child constructor arguments, especially Box(l, w, h).
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_IFORMFACTORPRISM_H
