//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/IProfileRipple.cpp
//! @brief     Implements interface ISawtoothRipple.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/IProfileRipple.h"
#include "Sample/HardParticle/Ripples.h"
#include "Sample/Shape/BoxNet.h"
#include "Sample/Shape/RippleCosineNet.h"
#include "Sample/Shape/RippleSawtoothNet.h"

//  ************************************************************************************************
//  interface IProfileRipple
//  ************************************************************************************************

IProfileRipple::IProfileRipple(const std::vector<double>& PValues)
    : IFormfactor(PValues)
    , m_length(m_P[0])
    , m_width(m_P[1])
    , m_height(m_P[2])
{
}

double IProfileRipple::radialExtension() const
{
    return (m_width + m_length) / 4.0;
}

complex_t IProfileRipple::formfactor(C3 q) const
{
    return factor_x(q.x()) * factor_yz(q.y(), q.z());
}

std::string IProfileRipple::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_length, "length");
    requestGt0(errs, m_width, "width");
    requestGt0(errs, m_height, "height");
    if (!errs.empty())
        return jointError(errs);
    m_validated = true;
    return "";
}

//  ************************************************************************************************
//  interface IProfileRectangularRipple
//  ************************************************************************************************

IProfileRectangularRipple::IProfileRectangularRipple(const std::vector<double>& PValues)
    : IProfileRipple(PValues)
{
    m_shape3D = std::make_unique<BoxNet>(m_length, m_width, m_height);
}

//! Complex form factor.
complex_t IProfileRectangularRipple::factor_yz(complex_t qy, complex_t qz) const
{
    return ripples::profile_yz_bar(qy, qz, m_width, m_height);
}

//  ************************************************************************************************
//  interface ICosineRipple
//  ************************************************************************************************

ICosineRipple::ICosineRipple(const std::vector<double>& PValues)
    : IProfileRipple(PValues)
{
    m_shape3D = std::make_unique<RippleCosineNet>(m_length, m_width, m_height);
}

//! Complex form factor.
complex_t ICosineRipple::factor_yz(complex_t qy, complex_t qz) const
{
    return ripples::profile_yz_cosine(qy, qz, m_width, m_height);
}

//  ************************************************************************************************
//  interface ISawtoothRipple
//  ************************************************************************************************

ISawtoothRipple::ISawtoothRipple(const std::vector<double>& PValues)
    : IProfileRipple(PValues)
    , m_asymmetry(m_P[3])
{
    m_shape3D = std::make_unique<RippleSawtoothNet>(m_length, m_width, m_height, m_asymmetry);
}

//! Complex form factor.
complex_t ISawtoothRipple::factor_yz(complex_t qy, complex_t qz) const
{
    return ripples::profile_yz_triangular(qy, qz, m_width, m_height, m_asymmetry);
}
