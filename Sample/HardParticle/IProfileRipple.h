//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/IProfileRipple.h
//! @brief     Defines class interfacees IProfileRipple, ICosineRipple, ISawtoothRipple.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_IPROFILERIPPLE_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_IPROFILERIPPLE_H

#include "Sample/Particle/IFormfactor.h"

//! Base class for form factors with a cosine ripple profile in the yz plane.

class IProfileRipple : public IFormfactor {
public:
    IProfileRipple(const std::vector<double>& PValues);

    double length() const { return m_length; }
    double height() const { return m_height; }
    double width() const { return m_width; }

    double radialExtension() const override;

    complex_t formfactor(C3 q) const override;
    std::string validate() const override;

protected:
    const double& m_length;
    const double& m_width;
    const double& m_height;

    virtual complex_t factor_x(complex_t qx) const = 0;
    virtual complex_t factor_yz(complex_t qy, complex_t qz) const = 0;
};

//! Base class for form factors with a rectangular ripple (bar) profile in the yz plane.

class IProfileRectangularRipple : public IProfileRipple {
public:
    IProfileRectangularRipple(const std::vector<double>& PValues);

private:
    complex_t factor_yz(complex_t qy, complex_t qz) const override;
};

//! Base class for form factors with a cosine ripple profile in the yz plane.

class ICosineRipple : public IProfileRipple {
public:
    ICosineRipple(const std::vector<double>& PValues);

private:
    complex_t factor_yz(complex_t qy, complex_t qz) const override;
};

//! Base class for form factors with a triangular ripple profile in the yz plane.

class ISawtoothRipple : public IProfileRipple {
public:
    ISawtoothRipple(const std::vector<double>& PValues);

    double asymmetry() const { return m_asymmetry; }

protected:
    const double& m_asymmetry;

private:
    complex_t factor_yz(complex_t qy, complex_t qz) const override;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_IPROFILERIPPLE_H
