//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/LongBoxGauss.cpp
//! @brief     Implements class LongBoxGauss.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/LongBoxGauss.h"
#include "Base/Math/Functions.h"
#include "Sample/Shape/BoxNet.h"

LongBoxGauss::LongBoxGauss(const std::vector<double> P)
    : IFormfactor(P)
    , m_length(m_P[0])
    , m_width(m_P[1])
    , m_height(m_P[2])
{
    validateOrThrow();
}

LongBoxGauss::LongBoxGauss(double length, double width, double height)
    : LongBoxGauss(std::vector<double>{length, width, height})
{
}

complex_t LongBoxGauss::formfactor(C3 q) const
{
    complex_t qxL2 = std::pow(m_length * q.x(), 2) / 2.0;
    complex_t qyWdiv2 = m_width * q.y() / 2.0;
    complex_t qzHdiv2 = m_height * q.z() / 2.0;

    return m_height * m_length * m_width * exp_I(qzHdiv2) * std::exp(-qxL2) * Math::sinc(qyWdiv2)
           * Math::sinc(qzHdiv2);
}


std::string LongBoxGauss::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_length, "length");
    requestGt0(errs, m_width, "width");
    requestGt0(errs, m_height, "height");
    if (!errs.empty())
        return jointError(errs);

    m_shape3D = std::make_unique<BoxNet>(m_length, m_width, m_height);

    m_validated = true;
    return "";
}

bool LongBoxGauss::contains(const R3&) const
{
    throw std::runtime_error("LongBoxGauss cannot be used as mesocrystal outer shape");
}
