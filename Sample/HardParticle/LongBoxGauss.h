//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/LongBoxGauss.h
//! @brief     Defines class LongBoxGauss.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_LONGBOXGAUSS_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_LONGBOXGAUSS_H

#include "Sample/Particle/IFormfactor.h"

//! The form factor for a long rectangular box.

class LongBoxGauss : public IFormfactor {
public:
    LongBoxGauss(double length, double width, double height);
    LongBoxGauss(std::vector<double> P);

#ifndef SWIG
    LongBoxGauss* clone() const override { return new LongBoxGauss(m_length, m_width, m_height); }
#endif // SWIG

    std::string className() const final { return "LongBoxGauss"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}};
    }

    double length() const { return m_length; }
    double height() const { return m_height; }
    double width() const { return m_width; }
    double radialExtension() const override { return m_length / 2.0; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_length;
    const double& m_width;
    const double& m_height;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_LONGBOXGAUSS_H
