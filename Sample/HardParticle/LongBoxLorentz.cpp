//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/LongBoxLorentz.cpp
//! @brief     Implements class LongBoxLorentz.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/LongBoxLorentz.h"
#include "Base/Math/Functions.h"
#include "Sample/Shape/BoxNet.h"

LongBoxLorentz::LongBoxLorentz(const std::vector<double> P)
    : IFormfactor(P)
    , m_length(m_P[0])
    , m_width(m_P[1])
    , m_height(m_P[2])
{
    validateOrThrow();
}

LongBoxLorentz::LongBoxLorentz(double length, double width, double height)
    : LongBoxLorentz(std::vector<double>{length, width, height})
{
}

complex_t LongBoxLorentz::formfactor(C3 q) const
{
    complex_t qxL2 = 2.5 * std::pow(m_length * q.x(), 2);
    complex_t qyWdiv2 = m_width * q.y() / 2.0;
    complex_t qzHdiv2 = m_height * q.z() / 2.0;

    return m_height * m_length * m_width * std::exp(complex_t(0., 1.) * qzHdiv2) / (1.0 + qxL2)
           * Math::sinc(qyWdiv2) * Math::sinc(qzHdiv2);
}

std::string LongBoxLorentz::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_length, "length");
    requestGt0(errs, m_width, "width");
    requestGt0(errs, m_height, "height");
    if (!errs.empty())
        return jointError(errs);

    m_shape3D = std::make_unique<BoxNet>(m_length, m_width, m_height);

    m_validated = true;
    return "";
}

bool LongBoxLorentz::contains(const R3&) const
{
    throw std::runtime_error("LongBoxLorentz cannot be used as mesocrystal outer shape");
}
