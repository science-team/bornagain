//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Polyhedra.h
//! @brief     Defines class Box.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_POLYHEDRA_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_POLYHEDRA_H

#include "Sample/HardParticle/IFormfactorPolyhedron.h"
#include "Sample/HardParticle/IFormfactorPrism.h"

//  ************************************************************************************************
//  Prisms
//  ************************************************************************************************

//! A rectangular prism (parallelepiped).

class Box : public IFormfactorPrism {
public:
    Box(double length, double width, double height);
    Box(std::vector<double> P);

#ifndef SWIG
    Box* clone() const override { return new Box(m_length, m_width, m_height); }
#endif // SWIG

    std::string className() const final { return "Box"; }

    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}};
    }

    double length() const { return m_length; }
    double width() const { return m_width; }
    double height() const override { return m_height; }

    bool contains(const R3& position) const override;

private:
    const double& m_length;
    const double& m_width;
    const double& m_height;
};

//! A prism based on an equilateral triangle.

class Prism3 : public IFormfactorPrism {
public:
    Prism3(double base_edge, double height);
    Prism3(std::vector<double> P);

#ifndef SWIG
    Prism3* clone() const override { return new Prism3(m_base_edge, m_height); }
#endif // SWIG

    std::string className() const final { return "Prism3"; }
    std::vector<ParaMeta> parDefs() const final { return {{"BaseEdge", "nm"}, {"Height", "nm"}}; }

    double baseEdge() const { return m_base_edge; }
    double height() const override { return m_height; }

    bool contains(const R3& position) const override;

private:
    const double& m_base_edge;
    const double& m_height;
};

//! A prism based on a regular hexagonal.

class Prism6 : public IFormfactorPrism {
public:
    Prism6(double base_edge, double height);
    Prism6(std::vector<double> P);

#ifndef SWIG
    Prism6* clone() const override { return new Prism6(m_base_edge, m_height); }
#endif // SWIG

    std::string className() const final { return "Prism6"; }
    std::vector<ParaMeta> parDefs() const final { return {{"BaseEdge", "nm"}, {"Height", "nm"}}; }

    double baseEdge() const { return m_base_edge; }
    double height() const override { return m_height; }

    bool contains(const R3& position) const override;

private:
    const double& m_base_edge;
    const double& m_height;
};

//  ************************************************************************************************
//  Platonic solids
//  ************************************************************************************************

//! A frustum with equilateral trigonal base.

class PlatonicTetrahedron : public IFormfactorPolyhedron {
public:
    PlatonicTetrahedron(double edge);
    PlatonicTetrahedron(std::vector<double> P);

#ifndef SWIG
    PlatonicTetrahedron* clone() const override { return new PlatonicTetrahedron(m_edge); }
#endif // SWIG

    std::string className() const final { return "PlatonicTetrahedron"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Edge", "nm"}}; }

    double edge() const { return m_edge; }
    double height() const { return sqrt(2. / 3) * m_edge; }

    bool contains(const R3& position) const override;

private:
    const double& m_edge;
};

//! A truncated bifrustum with quadratic base.

class PlatonicOctahedron : public IFormfactorPolyhedron {
public:
    PlatonicOctahedron(double edge);
    PlatonicOctahedron(std::vector<double> P);

#ifndef SWIG
    PlatonicOctahedron* clone() const override { return new PlatonicOctahedron(m_edge); }
#endif // SWIG

    std::string className() const final { return "PlatonicOctahedron"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Edge", "nm"}}; }

    double edge() const { return m_edge; }
    double height() const { return sqrt(1 / 2.) * m_edge; }

    bool contains(const R3& position) const override;

private:
    const double& m_edge;
};

//! A regular dodecahedron.

class Dodecahedron : public IFormfactorPolyhedron {
public:
    Dodecahedron(double edge);
    Dodecahedron(std::vector<double> P);

#ifndef SWIG
    Dodecahedron* clone() const override { return new Dodecahedron(m_edge); }
#endif // SWIG

    std::string className() const final { return "Dodecahedron"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Edge", "nm"}}; }

    double edge() const { return m_edge; }

    bool contains(const R3& position) const override;

private:
    const double& m_edge;
};

//! A regular icosahedron.

class Icosahedron : public IFormfactorPolyhedron {
public:
    Icosahedron(double edge);
    Icosahedron(std::vector<double> P);

#ifndef SWIG
    Icosahedron* clone() const override { return new Icosahedron(m_edge); }
#endif // SWIG

    std::string className() const final { return "Icosahedron"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Edge", "nm"}}; }

    double edge() const { return m_edge; }

    bool contains(const R3& position) const override;

private:
    const double& m_edge;
};

//  ************************************************************************************************
//  Pyramids
//  ************************************************************************************************

//! A frustum (truncated pyramid) with rectangular base.

class Pyramid2 : public IFormfactorPolyhedron {
public:
    Pyramid2(double length, double width, double height, double alpha);
    Pyramid2(std::vector<double> P);

#ifndef SWIG
    Pyramid2* clone() const override { return new Pyramid2(m_length, m_width, m_height, m_alpha); }
#endif // SWIG

    std::string className() const final { return "Pyramid2"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}, {"Alpha", "rad"}};
    }

    double length() const { return m_length; }
    double width() const { return m_width; }
    double height() const { return m_height; }
    double alpha() const { return m_alpha; }

    bool contains(const R3& position) const override;

private:
    const double& m_length;
    const double& m_width;
    const double& m_height;
    const double& m_alpha;
};

//! A frustum with equilateral trigonal base.

class Pyramid3 : public IFormfactorPolyhedron {
public:
    Pyramid3(double base_edge, double height, double alpha);
    Pyramid3(std::vector<double> P);

#ifndef SWIG
    Pyramid3* clone() const override { return new Pyramid3(m_base_edge, m_height, m_alpha); }
#endif // SWIG

    std::string className() const final { return "Pyramid3"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"BaseEdge", "nm"}, {"Height", "nm"}, {"Alpha", "rad"}};
    }

    double baseEdge() const { return m_base_edge; }
    double height() const { return m_height; }
    double alpha() const { return m_alpha; }

    bool contains(const R3& position) const override;

private:
    const double& m_base_edge;
    const double& m_height;
    const double& m_alpha;
};

//! A frustum with a quadratic base.

class Pyramid4 : public IFormfactorPolyhedron {
public:
    Pyramid4(double base_edge, double height, double alpha);
    Pyramid4(std::vector<double> P);

#ifndef SWIG
    Pyramid4* clone() const override { return new Pyramid4(m_base_edge, m_height, m_alpha); }
#endif // SWIG

    std::string className() const final { return "Pyramid4"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"BaseEdge", "nm"}, {"Height", "nm"}, {"Alpha", "rad"}};
    }

    double height() const { return m_height; }
    double baseEdge() const { return m_base_edge; }
    double alpha() const { return m_alpha; }

    bool contains(const R3& position) const override;

private:
    const double& m_base_edge;
    const double& m_height;
    const double& m_alpha;
};

//! A frustum (truncated pyramid) with regular hexagonal base.

class Pyramid6 : public IFormfactorPolyhedron {
public:
    Pyramid6(double base_edge, double height, double alpha);
    Pyramid6(std::vector<double> P);

#ifndef SWIG
    Pyramid6* clone() const override { return new Pyramid6(m_base_edge, m_height, m_alpha); }
#endif // SWIG

    std::string className() const final { return "Pyramid6"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"BaseEdge", "nm"}, {"Height", "nm"}, {"Alpha", "rad"}};
    }

    double baseEdge() const { return m_base_edge; }
    double height() const { return m_height; }
    double alpha() const { return m_alpha; }

    bool contains(const R3& position) const override;

private:
    const double& m_base_edge;
    const double& m_height;
    const double& m_alpha;
};

//! A truncated bifrustum with quadratic base.

class Bipyramid4 : public IFormfactorPolyhedron {
public:
    Bipyramid4(double length, double base_height, double height_ratio, double alpha);
    Bipyramid4(std::vector<double> P);

#ifndef SWIG
    Bipyramid4* clone() const override
    {
        return new Bipyramid4(m_length, m_base_height, m_height_ratio, m_alpha);
    }
#endif // SWIG

    std::string className() const final { return "Bipyramid4"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Height", "nm"}, {"HeightRatio", ""}, {"Alpha", "rad"}};
    }
    double length() const { return m_length; }
    double base_height() const { return m_base_height; }
    double heightRatio() const { return m_height_ratio; }
    double alpha() const { return m_alpha; }
    double height() const { return (1 + m_height_ratio) * m_base_height; }

    bool contains(const R3& position) const override;

private:
    const double& m_length;
    const double& m_base_height;
    const double& m_height_ratio;
    const double& m_alpha;
};

//  ************************************************************************************************
//  Others
//  ************************************************************************************************

//! A cube, with truncation of all edges and corners, as in Croset (2017) Fig 7

class CantellatedCube : public IFormfactorPolyhedron {
public:
    CantellatedCube(double length, double removed_length);
    CantellatedCube(std::vector<double> P);

#ifndef SWIG
    CantellatedCube* clone() const override
    {
        return new CantellatedCube(m_length, m_removed_length);
    }
#endif // SWIG

    std::string className() const final { return "CantellatedCube"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"RemovedLength", "nm"}};
    }

    double length() const { return m_length; }
    double removedLength() const { return m_removed_length; }

    bool contains(const R3& position) const override;

private:
    const double& m_length;
    const double& m_removed_length;
};

//! A cube, with tetrahedral truncation of all corners

class TruncatedCube : public IFormfactorPolyhedron {
public:
    TruncatedCube(double length, double removed_length);
    TruncatedCube(std::vector<double> P);

#ifndef SWIG
    TruncatedCube* clone() const override { return new TruncatedCube(m_length, m_removed_length); }
#endif // SWIG

    std::string className() const final { return "TruncatedCube"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"RemovedLength", "nm"}};
    }

    double length() const { return m_length; }
    double removedLength() const { return m_removed_length; }

    bool contains(const R3& position) const override;

private:
    const double& m_length;
    const double& m_removed_length;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_POLYHEDRA_H
