//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/SawtoothRipple.cpp
//! @brief     Implements classes SawtoothRipple*.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/SawtoothRipple.h"
#include "Sample/HardParticle/Ripples.h"

//  ************************************************************************************************
//  class SawtoothRippleBox
//  ************************************************************************************************

SawtoothRippleBox::SawtoothRippleBox(const std::vector<double> P)
    : ISawtoothRipple(P)
{
    validateOrThrow();
}

SawtoothRippleBox::SawtoothRippleBox(double length, double width, double height, double asymmetry)
    : SawtoothRippleBox(std::vector<double>{length, width, height, asymmetry})
{
}

SawtoothRippleBox* SawtoothRippleBox::clone() const
{
    return new SawtoothRippleBox(m_length, m_width, m_height, m_asymmetry);
}

bool SawtoothRippleBox::contains(const R3&) const
{
    // TODO: Implement SawtoothRippleBox
    std::ostringstream ostr;
    ostr << "Outer shape SawtoothRippleBox not yet implemented for Mesocrystal";
    ostr << "\n\nStay tuned!";
    throw std::runtime_error(ostr.str());
}

complex_t SawtoothRippleBox::factor_x(complex_t qx) const
{
    return ripples::factor_x_box(qx, m_length);
}

//  ************************************************************************************************
//  class SawtoothRippleGauss
//  ************************************************************************************************

SawtoothRippleGauss::SawtoothRippleGauss(const std::vector<double> P)
    : ISawtoothRipple(P)
{
    validateOrThrow();
}

SawtoothRippleGauss::SawtoothRippleGauss(double length, double width, double height,
                                         double asymmetry)
    : SawtoothRippleGauss(std::vector<double>{length, width, height, asymmetry})
{
}

SawtoothRippleGauss* SawtoothRippleGauss::clone() const
{
    return new SawtoothRippleGauss(m_length, m_width, m_height, m_asymmetry);
}

bool SawtoothRippleGauss::contains(const R3&) const
{
    throw std::runtime_error("SawtoothRippleGauss cannot be used as mesocrystal outer shape");
}

complex_t SawtoothRippleGauss::factor_x(complex_t qx) const
{
    return ripples::factor_x_Gauss(qx, m_length);
}

//  ************************************************************************************************
//  class SawtoothRippleLorentz
//  ************************************************************************************************

SawtoothRippleLorentz::SawtoothRippleLorentz(const std::vector<double> P)
    : ISawtoothRipple(P)
{
    validateOrThrow();
}

SawtoothRippleLorentz::SawtoothRippleLorentz(double length, double width, double height,
                                             double asymmetry)
    : SawtoothRippleLorentz(std::vector<double>{length, width, height, asymmetry})
{
}

SawtoothRippleLorentz* SawtoothRippleLorentz::clone() const
{
    return new SawtoothRippleLorentz(m_length, m_width, m_height, m_asymmetry);
}

bool SawtoothRippleLorentz::contains(const R3&) const
{
    throw std::runtime_error("SawtoothRippleLorentz cannot be used as mesocrystal outer shape");
}

complex_t SawtoothRippleLorentz::factor_x(complex_t qx) const
{
    return ripples::factor_x_Lorentz(qx, m_length);
}
