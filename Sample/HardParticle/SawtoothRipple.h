//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/SawtoothRipple.h
//! @brief     Defines classes SawtoothRipple*.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_SAWTOOTHRIPPLE_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_SAWTOOTHRIPPLE_H

#include "Sample/HardParticle/IProfileRipple.h"

//! The form factor for a cosine ripple, with box profile in elongation direction.
class SawtoothRippleBox : public ISawtoothRipple {
public:
    SawtoothRippleBox(double length, double width, double height, double asymmetry);
    SawtoothRippleBox(std::vector<double> P);

#ifndef SWIG
    SawtoothRippleBox* clone() const override;
#endif // SWIG

    std::string className() const final { return "SawtoothRippleBox"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}, {"AsymmetryLength", "nm"}};
    }

    bool contains(const R3& position) const override;

private:
    complex_t factor_x(complex_t qx) const override;
};

//! The form factor for a cosine ripple, with Gaussian profile in elongation direction.
class SawtoothRippleGauss : public ISawtoothRipple {
public:
    SawtoothRippleGauss(double length, double width, double height, double asymmetry);
    SawtoothRippleGauss(std::vector<double> P);

#ifndef SWIG
    SawtoothRippleGauss* clone() const override;
#endif // SWIG

    std::string className() const final { return "SawtoothRippleGauss"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}, {"AsymmetryLength", "nm"}};
    }

    bool contains(const R3& position) const override;

private:
    complex_t factor_x(complex_t qx) const override;
};

//! The form factor for a cosine ripple, with Lorentz form factor in elongation direction.
class SawtoothRippleLorentz : public ISawtoothRipple {
public:
    SawtoothRippleLorentz(double length, double width, double height, double asymmetry);
    SawtoothRippleLorentz(std::vector<double> P);

#ifndef SWIG
    SawtoothRippleLorentz* clone() const override;
#endif // SWIG

    std::string className() const final { return "SawtoothRippleLorentz"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Length", "nm"}, {"Width", "nm"}, {"Height", "nm"}, {"AsymmetryLength", "nm"}};
    }

    bool contains(const R3& position) const override;

private:
    complex_t factor_x(complex_t qx) const override;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_SAWTOOTHRIPPLE_H
