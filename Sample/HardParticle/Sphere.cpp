//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Sphere.cpp
//! @brief     Implements class Sphere.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/Sphere.h"
#include "Base/Type/Span.h"
#include "Base/Util/Assert.h"
#include "Sample/HardParticle/SphericalSegment.h"
#include "Sample/LibFF/SomeFormfactor.h"
#include "Sample/Scattering/Rotations.h"

Sphere::Sphere(const std::vector<double> P, bool position_at_center)
    : IFormfactor(P)
    , m_radius(m_P[0])
    , m_position_at_center(position_at_center)
{
    validateOrThrow();
}

Sphere::Sphere(double radius, bool position_at_center)
    : Sphere(std::vector<double>{radius}, position_at_center)
{
}

complex_t Sphere::formfactor(C3 q) const
{
    ASSERT(m_validated);
    complex_t result = SampleUtil::someff::ffSphere(q, m_radius);
    if (!m_position_at_center)
        result *= exp_I(q.z() * m_radius);
    return result;
}

Span Sphere::spanZ(const IRotation* rotation) const
{
    if (m_position_at_center)
        return {-m_radius, +m_radius};
    if (!rotation)
        return {0, 2 * m_radius};
    R3 centre(0.0, 0.0, m_radius);
    R3 new_centre = rotation->transformed(centre);
    return {new_centre.z() - m_radius, new_centre.z() + m_radius};
}

std::string Sphere::validate() const
{
    if (m_radius <= 0)
        return jointError({"nonpositive value of radius=" + std::to_string(m_radius)});
    m_validated = true;
    return "";
}

bool Sphere::contains(const R3& position) const
{
    double R = radius();

    if (std::abs(position.x()) > R || std::abs(position.y()) > R || position.z() < 0
        || position.z() > 2 * R)
        return false;

    if (std::pow(position.x() / R, 2) + std::pow(position.y() / R, 2)
            + std::pow((position.z() - R) / R, 2)
        <= 1)
        return true;

    return false;
}
