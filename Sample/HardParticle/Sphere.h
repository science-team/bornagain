//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Sphere.h
//! @brief     Defines class Sphere.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_SPHERE_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_SPHERE_H

#include "Sample/Particle/IFormfactor.h"

//! A full sphere.

class Sphere : public IFormfactor {
public:
    Sphere(double radius, bool position_at_center = false);
    Sphere(std::vector<double> P, bool position_at_center = false);

#ifndef SWIG
    Sphere* clone() const override { return new Sphere(m_radius, m_position_at_center); }
#endif // SWIG

    std::string className() const final { return "Sphere"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Radius", "nm"}}; }

    double radius() const { return m_radius; }
    double height() const { return 2 * m_radius; }

    double radialExtension() const override { return m_radius; }

    complex_t formfactor(C3 q) const override;
    Span spanZ(const IRotation* rotation) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

protected:
    bool canSliceAnalytically(const IRotation*) const override { return true; }

private:
    const double& m_radius;
    bool m_position_at_center;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_SPHERE_H
