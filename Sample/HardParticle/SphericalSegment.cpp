//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/SphericalSegment.cpp
//! @brief     Implements class SphericalSegment.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/SphericalSegment.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Bessel.h"
#include "Base/Math/IntegratorGK.h"
#include "Base/Util/Assert.h"
#include "Sample/Shape/TruncatedEllipsoidNet.h"
#include <limits>

using PhysConsts::pi;

SphericalSegment::SphericalSegment(const std::vector<double> P)
    : IFormfactor(P)
    , m_radius(m_P[0])
    , m_rm_top(m_P[1])
    , m_rm_bottom(m_P[2])
{
    validateOrThrow();
}

SphericalSegment::SphericalSegment(double radius, double top_cut, double bottom_cut)
    : SphericalSegment(std::vector<double>{radius, top_cut, bottom_cut})
{
}

//! Complex form factor.
complex_t SphericalSegment::formfactor(C3 q) const
{
    ASSERT(m_validated);

    double untruncated_height = 2 * m_radius - m_rm_bottom;

    if (std::abs(q.mag()) < std::numeric_limits<double>::epsilon())
        return pi / 3.
               * (untruncated_height * untruncated_height * (3. * m_radius - untruncated_height)
                  - m_rm_top * m_rm_top * (3. * m_radius - m_rm_top));

    const complex_t Q2 = std::sqrt(q.x() * q.x() + q.y() * q.y()); // NOT the modulus!
    const double R2 = m_radius * m_radius;

    complex_t integral = ComplexIntegrator().integrate(
        [=](double Z) {
            const double R2Z2 = R2 - Z * Z;
            return R2Z2 * Math::Bessel::J1c(Q2 * sqrt(R2Z2)) * exp_I(q.z() * Z);
        },
        m_radius - untruncated_height, m_radius - m_rm_top);
    return (2 * pi) * integral * exp_I(q.z() * (untruncated_height - m_radius));
}

std::string SphericalSegment::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_radius, "radius");
    requestGe0(errs, m_rm_top, "removed_top");
    requestGe0(errs, m_rm_bottom, "removed_bottom");
    if (m_rm_top > (2 * m_radius - m_rm_bottom))
        errs.emplace_back("parameters violate condition rm_top<=H");
    if (m_rm_bottom > (2 * m_radius - m_rm_top))
        errs.emplace_back("parameters violate condition rm_bottom<=H");
    if (!errs.empty())
        return jointError(errs);

    double untruncated_height = 2 * m_radius - m_rm_bottom;
    m_shape3D = std::make_unique<TruncatedEllipsoidNet>(m_radius, m_radius, m_radius,
                                                        untruncated_height, m_rm_top);

    m_validated = true;
    return "";
}

bool SphericalSegment::contains(const R3& position) const
{
    double R = radius();
    double H = 2 * R - cutFromBottom();
    double deltaH = cutFromTop();

    if (std::abs(position.x()) > R || std::abs(position.y()) > R || position.z() < 0
        || position.z() > (H - deltaH))
        return false;

    if (std::pow(position.x() / R, 2) + std::pow(position.y() / R, 2)
            + std::pow((position.z() - (H - R)) / R, 2)
        <= 1)
        return true;

    return false;
}
