//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/SphericalSegment.h
//! @brief     Defines class SphericalSegment.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_SPHERICALSEGMENT_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_SPHERICALSEGMENT_H

#include "Sample/Particle/IFormfactor.h"

//! A truncated Sphere.

class SphericalSegment : public IFormfactor {
public:
    SphericalSegment(double radius, double top_cut, double bottom_cut);
    SphericalSegment(std::vector<double> P);

#ifndef SWIG
    SphericalSegment* clone() const override
    {
        return new SphericalSegment(m_radius, m_rm_top, m_rm_bottom);
    }
#endif // SWIG

    std::string className() const final { return "SphericalSegment"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Radius", "nm"}, {"Top cut", "nm"}, {"Bottom cut", "nm"}};
    }

    double radius() const { return m_radius; }
    double cutFromTop() const { return m_rm_top; }
    double cutFromBottom() const { return m_rm_bottom; }

    double radialExtension() const override { return m_radius; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius;
    const double& m_rm_top;    //!< height of removed top cap
    const double& m_rm_bottom; //!< height of removed bottom cap
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_SPHERICALSEGMENT_H
