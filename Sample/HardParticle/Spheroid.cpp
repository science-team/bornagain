//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Spheroid.cpp
//! @brief     Implements class Spheroid.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/Spheroid.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Functions.h"
#include "Base/Util/Assert.h"
#include "Sample/HardParticle/SpheroidalSegment.h"
#include "Sample/Shape/TruncatedEllipsoidNet.h"
#include <limits>

using PhysConsts::pi;

Spheroid::Spheroid(const std::vector<double> P)
    : IFormfactor(P)
    , m_radius_xy(m_P[0])
    , m_radius_z(m_P[1])
{
    validateOrThrow();
    m_shape3D = std::make_unique<TruncatedEllipsoidNet>(m_radius_xy, m_radius_xy, m_radius_z,
                                                        2 * m_radius_z, 0.0);
}

Spheroid::Spheroid(double radius_xy, double radius_z)
    : Spheroid(std::vector<double>{radius_xy, radius_z})
{
}

complex_t Spheroid::formfactor(C3 q) const
{
    ASSERT(m_validated);
    double h = m_radius_z;
    double R = m_radius_xy;

    // complex length of q (not a sesquilinear dot product!),
    // xy components multiplied with R, z component multiplied with h
    complex_t qR = sqrt(R * R * (q.x() * q.x() + q.y() * q.y()) + h * h * q.z() * q.z());

    complex_t zFactor = exp_I(h * q.z());

    if (std::abs(qR) < 1e-4)
        // expand sin(qR)-qR*cos(qR) up to qR^5
        return 4 * pi / 3 * R * R * h * (1. - 0.1 * pow(qR, 2)) * zFactor;

    return 4 * pi / pow(qR, 3) * R * R * h * (sin(qR) - qR * cos(qR)) * zFactor;
}

std::string Spheroid::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_radius_xy, "radius_x");
    requestGt0(errs, m_radius_z, "radius_z");
    if (!errs.empty())
        return jointError(errs);
    m_validated = true;
    return "";
}

bool Spheroid::contains(const R3& position) const
{
    double rx = radiusXY(); // semi-axis length along x and y
    double rz = radiusZ();  // semi-axis length along z
    double H = 2 * rz;

    if (std::abs(position.x()) > rx || std::abs(position.y()) > rx || position.z() < 0
        || position.z() > H)
        return false;

    if (std::pow(position.x() / rx, 2) + std::pow(position.y() / rx, 2)
            + std::pow((position.z() - rz) / rz, 2)
        <= 1)
        return true;

    return false;
}
