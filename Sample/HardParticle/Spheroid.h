//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/Spheroid.h
//! @brief     Defines class Spheroid.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_SPHEROID_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_SPHEROID_H

#include "Sample/Particle/IFormfactor.h"

//! A full spheroid (an ellipsoid with two equal axes, hence with circular cross section)

class Spheroid : public IFormfactor {
public:
    Spheroid(double radius, double height);
    Spheroid(std::vector<double> P);

#ifndef SWIG
    Spheroid* clone() const override { return new Spheroid(m_radius, m_height); }
#endif // SWIG

    std::string className() const final { return "Spheroid"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Radius", "nm"}, {"Height", "nm"}}; }

    double height() const { return m_height; }
    double radius() const { return m_radius; }

    double radialExtension() const override { return m_radius; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius;
    const double& m_height;
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_SPHEROID_H
