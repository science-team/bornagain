//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/SpheroidalSegment.cpp
//! @brief     Implements class SpheroidalSegment.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/SpheroidalSegment.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Bessel.h"
#include "Base/Math/IntegratorGK.h"
#include "Base/Util/Assert.h"
#include "Sample/Shape/TruncatedEllipsoidNet.h"
#include <limits>

using PhysConsts::pi;

SpheroidalSegment::SpheroidalSegment(const std::vector<double> P)
    : IFormfactor(P)
    , m_radius_xy(m_P[0])
    , m_radius_z(m_P[1])
    , m_rm_top(m_P[2])
    , m_rm_bottom(m_P[3])
{
    validateOrThrow();
}

SpheroidalSegment::SpheroidalSegment(double radius_xy, double radius_z, double rm_top,
                                     double rm_bottom)
    : SpheroidalSegment(std::vector<double>{radius_xy, radius_z, rm_top, rm_bottom})
{
}

complex_t SpheroidalSegment::formfactor(C3 q) const
{
    ASSERT(m_validated);
    const double H = 2 * m_radius_z - m_rm_bottom;
    const double R = m_radius_xy;
    const double fp = m_radius_z / m_radius_xy;
    const double dh = m_rm_top;
    const double R2 = R * R;
    const complex_t Qp = std::sqrt(q.x() * q.x() + q.y() * q.y());
    const complex_t Qz = q.z() * fp;

    if (std::abs(q.mag()) <= std::numeric_limits<double>::epsilon())
        return pi / 3. / fp * (H * H * (3. * R - H / fp) - dh * dh * (3. * R - dh / fp));

    return (2 * pi) * exp_I(q.z() * (H - fp * R)) * fp
           * ComplexIntegrator().integrate(
               [=](double Z) {
                   const double R2Z2 = R2 - Z * Z;
                   return R2Z2 * Math::Bessel::J1c(Qp * std::sqrt(R2Z2)) * exp_I(Qz * Z);
               },
               R - H / fp, R - dh / fp);
}

std::string SpheroidalSegment::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_radius_xy, "radius_xy");
    requestGt0(errs, m_radius_z, "radius_z");
    requestGe0(errs, m_rm_top, "removed_top");
    requestGe0(errs, m_rm_bottom, "removed_bottom");
    if (m_rm_top > (2 * m_radius_z - m_rm_bottom))
        errs.emplace_back("parameters violate condition rm_top<=H");
    if (m_rm_bottom > (2 * m_radius_z - m_rm_top))
        errs.emplace_back("parameters violate condition rm_bottom<=H");
    if (!errs.empty())
        return jointError(errs);

    double untruncated_height = 2 * m_radius_z - m_rm_bottom;
    m_shape3D = std::make_unique<TruncatedEllipsoidNet>(m_radius_xy, m_radius_xy, m_radius_z,
                                                        untruncated_height, m_rm_top);
    m_validated = true;
    return "";
}

bool SpheroidalSegment::contains(const R3&) const
{
    // TODO: Implement Truncated spheroid
    std::ostringstream ostr;
    ostr << "Outer shape Truncated spheroid not yet implemented for Mesocrystal";
    ostr << "\n\nStay tuned!";
    throw std::runtime_error(ostr.str());
}
