//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/SpheroidalSegment.h
//! @brief     Defines class SpheroidalSegment.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_SPHEROIDALSEGMENT_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_SPHEROIDALSEGMENT_H

#include "Sample/Particle/IFormfactor.h"

//! A truncated spheroid.
//! An ellipsoid with two equal axis, truncated by a plane perpendicular to the third axis.

class SpheroidalSegment : public IFormfactor {
public:
    SpheroidalSegment(double radius_xy, double radius_z, double rm_top, double rm_bottom);
    SpheroidalSegment(std::vector<double> P);

#ifndef SWIG
    SpheroidalSegment* clone() const override
    {
        return new SpheroidalSegment(m_radius_xy, m_radius_z, m_rm_top, m_rm_bottom);
    }
#endif // SWIG

    std::string className() const final { return "SpheroidalSegment"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Radius XY", "nm"}, {"Radius Z", "nm"}, {"Top cut", "nm"}, {"Bottom cut", "nm"}};
    }

    double radiusXY() const { return m_radius_xy; }
    double radiusZ() const { return m_radius_z; }
    double cutFromTop() const { return m_rm_top; }
    double cutFromBottom() const { return m_rm_bottom; }

    double height() const { return 2 * m_radius_z - m_rm_top - m_rm_bottom; }
    double radialExtension() const override { return m_radius_xy; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius_xy;
    const double& m_radius_z;  //!< untruncated_height before removal of cap
    const double& m_rm_top;    //!< ratio of vertical to horizontal radius
    const double& m_rm_bottom; //!< untruncated_height of removed cap
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_SPHEROIDALSEGMENT_H
