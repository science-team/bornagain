//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/TruncatedSphere.cpp
//! @brief     Implements class TruncatedSphere.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/TruncatedSphere.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Bessel.h"
#include "Base/Math/IntegratorGK.h"
#include "Base/Util/Assert.h"
#include "Sample/Shape/TruncatedEllipsoidNet.h"
#include <limits>

using PhysConsts::pi;

TruncatedSphere::TruncatedSphere(const std::vector<double> P)
    : IFormfactor(P)
    , m_radius(m_P[0])
    , m_untruncated_height(m_P[1])
    , m_dh(m_P[2])
{
    validateOrThrow();
}

TruncatedSphere::TruncatedSphere(double radius, double untruncated_height, double dh)
    : TruncatedSphere(std::vector<double>{radius, untruncated_height, dh})
{
}

//! Complex form factor.
complex_t TruncatedSphere::formfactor(C3 q) const
{
    ASSERT(m_validated);
    if (std::abs(q.mag()) < std::numeric_limits<double>::epsilon())
        return pi / 3.
               * (m_untruncated_height * m_untruncated_height
                      * (3. * m_radius - m_untruncated_height)
                  - m_dh * m_dh * (3. * m_radius - m_dh));

    const complex_t Q2 = std::sqrt(q.x() * q.x() + q.y() * q.y()); // NOT the modulus!
    const double R2 = m_radius * m_radius;

    complex_t integral = ComplexIntegrator().integrate(
        [=](double Z) {
            const double R2Z2 = R2 - Z * Z;
            return R2Z2 * Math::Bessel::J1c(Q2 * sqrt(R2Z2)) * exp_I(q.z() * Z);
        },
        m_radius - m_untruncated_height, m_radius - m_dh);
    return (2 * pi) * integral * exp_I(q.z() * (m_untruncated_height - m_radius));
}

std::string TruncatedSphere::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_radius, "radius");
    requestGt0(errs, m_untruncated_height, "untruncated_height");
    requestGe0(errs, m_dh, "dh");
    if (m_untruncated_height > 2. * m_radius)
        errs.emplace_back("parameters violate condition H>2R");
    if (m_dh > m_untruncated_height)
        errs.emplace_back("parameters violate condition dH<=H");
    if (!errs.empty())
        return jointError(errs);

    m_shape3D = std::make_unique<TruncatedEllipsoidNet>(m_radius, m_radius, m_radius,
                                                        m_untruncated_height, m_dh);

    m_validated = true;
    return "";
}

bool TruncatedSphere::contains(const R3& position) const
{
    double R = radius();
    double H = untruncated_height();
    double deltaH = removedTop();

    if (std::abs(position.x()) > R || std::abs(position.y()) > R || position.z() < 0
        || position.z() > (H - deltaH))
        return false;

    if (std::pow(position.x() / R, 2) + std::pow(position.y() / R, 2)
            + std::pow((position.z() - (H - R)) / R, 2)
        <= 1)
        return true;

    return false;
}
