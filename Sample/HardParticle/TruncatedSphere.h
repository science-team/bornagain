//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/TruncatedSphere.h
//! @brief     Defines class TruncatedSphere.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_TRUNCATEDSPHERE_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_TRUNCATEDSPHERE_H

#include "Sample/Particle/IFormfactor.h"

//! A truncated Sphere.

class TruncatedSphere : public IFormfactor {
public:
    TruncatedSphere(double radius, double untruncated_height, double dh);
    TruncatedSphere(std::vector<double> P);

#ifndef SWIG
    TruncatedSphere* clone() const override
    {
        return new TruncatedSphere(m_radius, m_untruncated_height, m_dh);
    }
#endif // SWIG

    std::string className() const final { return "TruncatedSphere"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Radius", "nm"}, {"Height", "nm"}, {"DeltaHeight", "nm"}};
    }

    double untruncated_height() const { return m_untruncated_height; }
    double radius() const { return m_radius; }
    double removedTop() const { return m_dh; }

    double height() const { return m_untruncated_height - m_dh; }
    double radialExtension() const override { return m_radius; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius;
    const double& m_untruncated_height; //!< height before removal of cap
    const double& m_dh;                 //!< height of removed cap
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_TRUNCATEDSPHERE_H
