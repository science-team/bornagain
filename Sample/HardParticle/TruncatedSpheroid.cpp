//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/TruncatedSpheroid.cpp
//! @brief     Implements class TruncatedSpheroid.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/HardParticle/TruncatedSpheroid.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Bessel.h"
#include "Base/Math/IntegratorGK.h"
#include "Base/Util/Assert.h"
#include "Sample/Shape/TruncatedEllipsoidNet.h"
#include <limits>

using PhysConsts::pi;

TruncatedSpheroid::TruncatedSpheroid(const std::vector<double> P)
    : IFormfactor(P)
    , m_radius(m_P[0])
    , m_untruncated_height(m_P[1])
    , m_fp(m_P[2])
    , m_dh(m_P[3])
{
    validateOrThrow();
}

TruncatedSpheroid::TruncatedSpheroid(double radius, double untruncated_height,
                                     double height_flattening, double dh)
    : TruncatedSpheroid(std::vector<double>{radius, untruncated_height, height_flattening, dh})
{
}

complex_t TruncatedSpheroid::formfactor(C3 q) const
{
    ASSERT(m_validated);
    const double H = m_untruncated_height;
    const double R = m_radius;
    const double fp = m_fp;
    const double R2 = R * R;
    const complex_t Qp = std::sqrt(q.x() * q.x() + q.y() * q.y());
    const complex_t Qz = q.z() * fp;

    if (std::abs(q.mag()) <= std::numeric_limits<double>::epsilon())
        return pi / 3. / fp * (H * H * (3. * R - H / fp) - m_dh * m_dh * (3. * R - m_dh / fp));

    return (2 * pi) * exp_I(q.z() * (H - fp * R)) * fp
           * ComplexIntegrator().integrate(
               [=](double Z) {
                   const double R2Z2 = R2 - Z * Z;
                   return R2Z2 * Math::Bessel::J1c(Qp * std::sqrt(R2Z2)) * exp_I(Qz * Z);
               },
               R - H / fp, R - m_dh / fp);
}

std::string TruncatedSpheroid::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_radius, "radius");
    requestGt0(errs, m_untruncated_height, "untruncated_height");
    requestGt0(errs, m_fp, "height_flattening");
    requestGe0(errs, m_dh, "dh");
    if (m_untruncated_height > 2 * m_radius * m_fp)
        errs.emplace_back("parameters violate condition H>2R*flattening");
    if (m_dh > m_untruncated_height)
        errs.emplace_back("parameters violate condition dH<=H");
    if (!errs.empty())
        return jointError(errs);

    m_shape3D = std::make_unique<TruncatedEllipsoidNet>(m_radius, m_radius, m_fp * m_radius,
                                                        m_untruncated_height, m_dh);

    m_validated = true;
    return "";
}

bool TruncatedSpheroid::contains(const R3&) const
{
    // TODO: Implement Truncated spheroid
    std::ostringstream ostr;
    ostr << "Outer shape Truncated spheroid not yet implemented for Mesocrystal";
    ostr << "\n\nStay tuned!";
    throw std::runtime_error(ostr.str());
}
