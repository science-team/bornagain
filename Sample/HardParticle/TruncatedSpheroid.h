//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/HardParticle/TruncatedSpheroid.h
//! @brief     Defines class TruncatedSpheroid.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_HARDPARTICLE_TRUNCATEDSPHEROID_H
#define BORNAGAIN_SAMPLE_HARDPARTICLE_TRUNCATEDSPHEROID_H

#include "Sample/Particle/IFormfactor.h"

//! A truncated spheroid.
//! An ellipsoid with two equal axis, truncated by a plane perpendicular to the third axis.

class TruncatedSpheroid : public IFormfactor {
public:
    TruncatedSpheroid(double radius, double untruncated_height,
                      double untruncated_height_flattening, double dh);
    TruncatedSpheroid(std::vector<double> P);

#ifndef SWIG
    TruncatedSpheroid* clone() const override
    {
        return new TruncatedSpheroid(m_radius, m_untruncated_height, m_fp, m_dh);
    }
#endif // SWIG

    std::string className() const final { return "TruncatedSpheroid"; }
    std::vector<ParaMeta> parDefs() const final
    {
        return {{"Radius", "nm"},
                {"UntruncatedHeight", "nm"},
                {"HeightFlattening", ""},
                {"DeltaHeight", "nm"}};
    }

    double radius() const { return m_radius; }
    double untruncated_height() const { return m_untruncated_height; }
    double heightFlattening() const { return m_fp; }
    double removedTop() const { return m_dh; }

    double height() const { return m_untruncated_height - m_dh; }
    double radialExtension() const override { return m_radius; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius;
    const double& m_untruncated_height; //!< untruncated_height before removal of cap
    const double& m_fp;                 //!< ratio of vertical to horizontal radius
    const double& m_dh;                 //!< untruncated_height of removed cap
};

#endif // BORNAGAIN_SAMPLE_HARDPARTICLE_TRUNCATEDSPHEROID_H
