//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Interface/AutocorrelationModels.h
//! @brief     Define AutocorrelationModel classes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_INTERFACE_AUTOCORRELATIONMODELS_H
#define BORNAGAIN_SAMPLE_INTERFACE_AUTOCORRELATIONMODELS_H

#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"

//! Base class for autocorrelation function models.
class AutocorrelationModel : public ICloneable, public INode {
public:
    void setMaxSpatialFrequency(double val) { m_max_spatial_frequency = val; }
    double maxSpatialFrequency() const { return m_max_spatial_frequency; }

    std::vector<ParaMeta> parDefs() const override { return {{"MaxSpatialFrequency", "1/nm"}}; }

#ifndef SWIG
    AutocorrelationModel* clone() const override = 0;

    //! Used to create the Python constructor of this class
    virtual std::string pythonArguments() const;

protected:
    std::vector<std::string> validationErrs() const;
    AutocorrelationModel(double maxSpatFrequency);
    double m_max_spatial_frequency; //! upper limit for spatial frequency of roughness (nm^-1)
#endif
};

//! The model is described e.g. in the article "Roughness spectrum and surface width of self-affine
//! fractal surfaces via the K-correlation model" by G. Palasantzas, Physical Review B 48,
//! 14472-14478 (1993)
class SelfAffineFractalModel : public AutocorrelationModel {
public:
    SelfAffineFractalModel(double sigma, double hurst, double lateralCorrLength,
                           double maxSpatFrequency = 0.5);
    std::string className() const override { return "SelfAffineFractalModel"; }
    std::vector<ParaMeta> parDefs() const final;
    std::string validate() const override;

    //! Returns power spectral density of the surface roughness
    double spectralFunction(double spatial_f) const;

    //! Returns rms of roughness
    double rms() const;

    void setSigma(double sigma) { m_sigma = sigma; }
    double sigma() const { return m_sigma; }

    void setHurstParameter(double hurstParameter) { m_hurst_parameter = hurstParameter; }
    double hurst() const { return m_hurst_parameter; }

    void setLatteralCorrLength(double lateralCorrLen) { m_lateral_corr_length = lateralCorrLen; }
    double lateralCorrLength() const { return m_lateral_corr_length; }

#ifndef SWIG
    SelfAffineFractalModel* clone() const override;

    std::string pythonArguments() const override;

private:
    double m_sigma;           //!< height scale of the roughness
    double m_hurst_parameter; //!< Hurst parameter which describes how jagged the interface, 0<H<=1
    double m_lateral_corr_length; //!< lateral correlation length of the roughness
#endif
};

//! The model is described e.g. in the article "Stochastic model for thin film growth and erosion"
//! by D. G. Stearns, Applied Physics Letters 62, 1745–1747 (1993) and extended in e.g.
//! Stearns, D. G. & Gullikson, E. M. "Nonspecular scattering from extreme ultraviolet multilayer
//! coatings", Physica B: Condensed Matter 283, 1–3, p. 84-91 (2000)
class LinearGrowthModel : public AutocorrelationModel {
public:
    LinearGrowthModel(double particle_volume, double damp1, double damp2, double damp3,
                      double damp4, double maxSpatFrequency = 0.5);
    std::string className() const override { return "LinearGrowthModel"; }
    std::vector<ParaMeta> parDefs() const final;
    std::string validate() const override;

    //! Returns power spectral density of the surface roughness
    double spectralFunction(double spectrum_below, double thickness, double spatial_f) const;

    double crosscorrSpectrum(double spectrum_below, double thickness, double spatial_f) const;

    void setClusterVolume(double particle_volume) { m_cluster_volume = particle_volume; }
    double clusterVolume() const { return m_cluster_volume; }

    void setDamp1(double damp1) { m_damp1 = damp1; }
    double damp1() const { return m_damp1; }

    void setDamp2(double damp2) { m_damp2 = damp2; }
    double damp2() const { return m_damp2; }

    void setDamp3(double damp3) { m_damp3 = damp3; }
    double damp3() const { return m_damp3; }

    void setDamp4(double damp4) { m_damp4 = damp4; }
    double damp4() const { return m_damp4; }

#ifndef SWIG
    LinearGrowthModel* clone() const override;

    std::string pythonArguments() const override;

private:
    double damping(double spatial_f) const;

    double m_cluster_volume; //!< volume of particles, incoming to the surface during growth
    double m_damp1;          //!< damping exponent for spatial frequency raised to the power of 1
    double m_damp2;          //!< damping exponent for spatial frequency raised to the power of 2
    double m_damp3;          //!< damping exponent for spatial frequency raised to the power of 3
    double m_damp4;          //!< damping exponent for spatial frequency raised to the power of 4
#endif
};

#endif // BORNAGAIN_SAMPLE_INTERFACE_AUTOCORRELATIONMODELS_H
