//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Interface/CrosscorrelationModels.cpp
//! @brief     Implement CrossCorrModel classes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Interface/CrosscorrelationModels.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Py/PyFmt.h"
#include "Base/Util/Assert.h"
#include <cmath>

using PhysConsts::pi;

SpatialFrequencyCrosscorrelation::SpatialFrequencyCrosscorrelation(double base_crosscorr_depth,
                                                                   double base_frequency,
                                                                   double power)
    : m_base_crosscorr_depth(base_crosscorr_depth)
    , m_base_spatial_frequency(base_frequency)
    , m_power(power)
{
    validateOrThrow();
}

SpatialFrequencyCrosscorrelation* SpatialFrequencyCrosscorrelation::clone() const
{
    return new SpatialFrequencyCrosscorrelation(m_base_crosscorr_depth, m_base_spatial_frequency,
                                                m_power);
}

std::string SpatialFrequencyCrosscorrelation::pythonArguments() const
{
    return Py::Fmt::printArguments({{m_base_crosscorr_depth, parDefs()[0].unit},
                                    {m_base_spatial_frequency, parDefs()[1].unit},
                                    {m_power, parDefs()[2].unit}});
}

std::string SpatialFrequencyCrosscorrelation::validate() const
{
    std::vector<std::string> errs;
    requestGe0(errs, m_base_crosscorr_depth, parDefs()[0].name);
    requestGt0(errs, m_base_spatial_frequency, parDefs()[1].name);
    requestGe0(errs, m_power, parDefs()[2].name);
    if (!errs.empty())
        return jointError(errs);
    m_validated = true;
    return "";
}

double SpatialFrequencyCrosscorrelation::crosscorrSpectrum(double spectrum_up, double spectrum_low,
                                                           double thickness, double spatial_f) const
{
    ASSERT(thickness >= 0);

    if (m_base_crosscorr_depth == 0 || spectrum_up == 0 || spectrum_low == 0)
        return 0;

    const double frequency_factor = std::pow(spatial_f / m_base_spatial_frequency, m_power);
    const double base_cross_spectrum = std::sqrt(spectrum_up * spectrum_low);
    return base_cross_spectrum * std::exp(-thickness / m_base_crosscorr_depth * frequency_factor);
}

//-------------------------------------------------------------------------------------------------

CommonDepthCrosscorrelation::CommonDepthCrosscorrelation(double cross_corr_depth)
    : SpatialFrequencyCrosscorrelation(cross_corr_depth, 1, 0)
{
}

CommonDepthCrosscorrelation* CommonDepthCrosscorrelation::clone() const
{
    return new CommonDepthCrosscorrelation(m_base_crosscorr_depth);
}

std::string CommonDepthCrosscorrelation::pythonArguments() const
{
    return Py::Fmt::printArguments({{m_base_crosscorr_depth, parDefs()[0].unit}});
}
