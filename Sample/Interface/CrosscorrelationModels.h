//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Interface/CrosscorrelationModels.h
//! @brief     Define CrosscorrelationModel classes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_INTERFACE_CROSSCORRELATIONMODELS_H
#define BORNAGAIN_SAMPLE_INTERFACE_CROSSCORRELATIONMODELS_H

#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"
#include <heinz/Vectors3D.h>

//! Base class for cross-correlation function models.
class CrosscorrelationModel : public ICloneable, public INode {
public:
#ifndef SWIG
    CrosscorrelationModel* clone() const override = 0;

    //! Used to create the Python constructor of this class
    virtual std::string pythonArguments() const = 0;
#endif
};

//! The crosscorrelation factor depends both on the distance between interfaces and the spatial
//! frequency of roughness.
class SpatialFrequencyCrosscorrelation : public CrosscorrelationModel {
public:
    SpatialFrequencyCrosscorrelation(double base_crosscorr_depth, double base_frequency,
                                     double power);
    std::string className() const override { return "SpatialFrequencyCrosscorrelation"; }
    std::string validate() const override;
    double crosscorrSpectrum(double spectrum_up, double spectrum_low, double thickness,
                             double spatial_f) const;
    std::vector<ParaMeta> parDefs() const override
    {
        return {{"BaseCrosscorrDepth", "nm"}, {"BaseSpatialFrequency", "1/nm"}, {"Power", ""}};
    }

#ifndef SWIG
    SpatialFrequencyCrosscorrelation* clone() const override;

    std::string pythonArguments() const override;
#endif

    double baseCrossCorrDepth() const { return m_base_crosscorr_depth; }
    double baseSpatialFrequency() const { return m_base_spatial_frequency; }
    double power() const { return m_power; }

protected:
    double m_base_crosscorr_depth;
    double m_base_spatial_frequency;
    double m_power;
};

//! The crosscorrelation factor depends on the distance between interfaces and does not depend
//! on the spatial frequency of roughness.
class CommonDepthCrosscorrelation : public SpatialFrequencyCrosscorrelation {
public:
    CommonDepthCrosscorrelation(double cross_corr_depth);
    std::string className() const override { return "CommonDepthCrosscorrelation"; }
    std::vector<ParaMeta> parDefs() const override { return {{"CrosscorrDepth", "nm"}}; }

#ifndef SWIG
    CommonDepthCrosscorrelation* clone() const override;

    std::string pythonArguments() const override;
#endif

    double crossCorrDepth() const { return m_base_crosscorr_depth; }
};

#endif // BORNAGAIN_SAMPLE_INTERFACE_CROSSCORRELATIONMODELS_H
