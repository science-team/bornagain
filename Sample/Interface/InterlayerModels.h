//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Interface/InterlayerModels.h
//! @brief     Define InterlayerModel classes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_INTERFACE_INTERLAYERMODELS_H
#define BORNAGAIN_SAMPLE_INTERFACE_INTERLAYERMODELS_H

#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"

class InterlayerModel : public ICloneable, public INode {
public:
#ifndef SWIG
    InterlayerModel* clone() const override = 0;
#endif // SWIG

    virtual double transient(double x, double sigma) const = 0;
    virtual double distribution(double x, double sigma) const = 0;
    virtual double sigmaRange() const = 0;
};

class ErfInterlayer : public InterlayerModel {
public:
    ErfInterlayer();
    double transient(double x, double sigma) const override;
    double distribution(double x, double sigma) const override;
    double sigmaRange() const override;
    std::string className() const override { return "ErfInterlayer"; }

#ifndef SWIG
    ErfInterlayer* clone() const override;
#endif // SWIG
};

class TanhInterlayer : public InterlayerModel {
public:
    TanhInterlayer();
    double transient(double x, double sigma) const override;
    double distribution(double x, double sigma) const override;
    double sigmaRange() const override;
    std::string className() const override { return "TanhInterlayer"; }

#ifndef SWIG
    TanhInterlayer* clone() const override;
#endif // SWIG
};

#endif // BORNAGAIN_SAMPLE_INTERFACE_INTERLAYERMODELS_H
