//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Interface/LayerRoughness.cpp
//! @brief     Implements class LayerRoughness.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Interface/LayerRoughness.h"
#include "Base/Util/Assert.h"

//! Constructor of layer roughness.
//! @param autoCorrModel: autocorrelation function and spectrum
//! @param interlayerModel: shape of the interfacial transition region
LayerRoughness::LayerRoughness(const AutocorrelationModel* autocorrelation,
                               const InterlayerModel* interlayer,
                               const CrosscorrelationModel* crosscorrelation)
    : m_autocorrelation_model(autocorrelation ? autocorrelation->clone() : nullptr)
    , m_interlayer_model(interlayer ? interlayer->clone() : nullptr)
    , m_crosscorrelation_model(crosscorrelation ? crosscorrelation->clone() : nullptr)
{
    ASSERT(m_autocorrelation_model);
    ASSERT(m_interlayer_model);
    if (dynamic_cast<LinearGrowthModel*>(m_autocorrelation_model.get())
        && m_crosscorrelation_model.get())
        throw std::runtime_error("Linear growth model already has the crosscorrelation model");
}

LayerRoughness* LayerRoughness::clone() const
{
    return new LayerRoughness(m_autocorrelation_model.get(), m_interlayer_model.get(),
                              m_crosscorrelation_model.get());
}

std::vector<const INode*> LayerRoughness::nodeChildren() const
{
    return std::vector<const INode*>()
           << m_autocorrelation_model << m_interlayer_model << m_crosscorrelation_model;
}

bool LayerRoughness::showInScriptOrGui() const
{
    if (auto autocorr = dynamic_cast<const K_CorrelationModel*>(m_autocorrelation_model.get()))
        if (autocorr->sigma() == 0)
            return false;

    return true;
}
