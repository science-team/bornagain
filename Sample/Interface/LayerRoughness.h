//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Interface/LayerRoughness.h
//! @brief     Defines class LayerRoughness.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_INTERFACE_LAYERROUGHNESS_H
#define BORNAGAIN_SAMPLE_INTERFACE_LAYERROUGHNESS_H

#include "Sample/Interface/AutocorrelationModels.h"
#include "Sample/Interface/CrosscorrelationModels.h"
#include "Sample/Interface/InterlayerModels.h"
#include "Sample/Scattering/ISampleNode.h"
#include <heinz/Vectors3D.h>

//! A roughness of interface between two layers.
//!
//! Based on the article "X-ray reflection and transmission by rough surfaces"
//! by D. K. G. de Boer, Physical Review B 51, 5297 (1995)

class LayerRoughness : public ISampleNode {
public:
    LayerRoughness(const AutocorrelationModel* autocorrelation, const InterlayerModel* interlayer,
                   const CrosscorrelationModel* crosscorrelation = nullptr);

    std::string className() const final { return "LayerRoughness"; }

    const AutocorrelationModel* autocorrelationModel() const
    {
        return m_autocorrelation_model.get();
    }

    const InterlayerModel* interlayerModel() const { return m_interlayer_model.get(); }

    const CrosscorrelationModel* crosscorrelationModel() const
    {
        return m_crosscorrelation_model.get();
    }

#ifndef SWIG
    LayerRoughness* clone() const override;

    std::vector<const INode*> nodeChildren() const override;

    bool showInScriptOrGui() const;
#endif

private:
    std::unique_ptr<AutocorrelationModel> m_autocorrelation_model; // never nullptr
    std::unique_ptr<InterlayerModel> m_interlayer_model;           // never nullptr
    std::unique_ptr<CrosscorrelationModel> m_crosscorrelation_model;
};

#endif // BORNAGAIN_SAMPLE_INTERFACE_LAYERROUGHNESS_H
