//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Interface/Roughness.cpp
//! @brief     Implements class Roughness.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Interface/Roughness.h"
#include "Base/Util/Assert.h"

//! Constructor of layer roughness.
//! @param autoCorrModel: autocorrelation function and spectrum
//! @param transient: shape of the interfacial transition region
Roughness::Roughness(const AutocorrelationModel* autocorrelation, const TransientModel* transient,
                     const CrosscorrelationModel* crosscorrelation)
    : m_autocorrelation(autocorrelation ? autocorrelation->clone() : nullptr)
    , m_transient(transient ? transient->clone() : nullptr)
    , m_crosscorrelation_model(crosscorrelation ? crosscorrelation->clone() : nullptr)
{
    ASSERT(m_autocorrelation);
    ASSERT(m_transient);
    if (dynamic_cast<LinearGrowthModel*>(m_autocorrelation.get()) && m_crosscorrelation_model.get())
        throw std::runtime_error("Linear growth model already has the crosscorrelation model");
}

Roughness* Roughness::clone() const
{
    return new Roughness(m_autocorrelation.get(), m_transient.get(),
                         m_crosscorrelation_model.get());
}

std::vector<const INode*> Roughness::nodeChildren() const
{
    return std::vector<const INode*>()
           << m_autocorrelation << m_transient << m_crosscorrelation_model;
}

bool Roughness::showInScriptOrGui() const
{
    if (auto autocorr = dynamic_cast<const SelfAffineFractalModel*>(m_autocorrelation.get()))
        if (autocorr->sigma() == 0)
            return false;

    return true;
}
