//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Interface/Roughness.h
//! @brief     Defines class Roughness.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_INTERFACE_ROUGHNESS_H
#define BORNAGAIN_SAMPLE_INTERFACE_ROUGHNESS_H

#include "Sample/Interface/AutocorrelationModels.h"
#include "Sample/Interface/CrosscorrelationModels.h"
#include "Sample/Interface/TransientModels.h"
#include "Sample/Scattering/ISampleNode.h"
#include <heinz/Vectors3D.h>

//! A roughness of interface between two layers.
//!
//! Based on the article "X-ray reflection and transmission by rough surfaces"
//! by D. K. G. de Boer, Physical Review B 51, 5297 (1995)

class Roughness : public ISampleNode {
public:
    Roughness(const AutocorrelationModel* autocorrelation, const TransientModel* transient,
              const CrosscorrelationModel* crosscorrelation = nullptr);

    std::string className() const final { return "Roughness"; }

    const AutocorrelationModel* autocorrelationModel() const { return m_autocorrelation.get(); }

    const TransientModel* transient() const { return m_transient.get(); }

    const CrosscorrelationModel* crosscorrelationModel() const
    {
        return m_crosscorrelation_model.get();
    }

#ifndef SWIG
    Roughness* clone() const override;

    std::vector<const INode*> nodeChildren() const override;

    bool showInScriptOrGui() const;
#endif

private:
    std::unique_ptr<AutocorrelationModel> m_autocorrelation; // never nullptr
    std::unique_ptr<TransientModel> m_transient;             // never nullptr
    std::unique_ptr<CrosscorrelationModel> m_crosscorrelation_model;
};

#endif // BORNAGAIN_SAMPLE_INTERFACE_ROUGHNESS_H
