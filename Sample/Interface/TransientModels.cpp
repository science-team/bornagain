//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Interface/TransientModels.cpp
//! @brief     Implements TransientModel classes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Interface/TransientModels.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Math/Functions.h"
#include "Base/Util/Assert.h"
#include <cmath>

using PhysConsts::pi;

namespace {
const double tanh_prefactor = pi / 2. / std::sqrt(3);
const double erf_prefactor = std::sqrt(1.0 / 2.0);
const double pi_2 = std::sqrt(pi);
} // namespace


ErfTransient::ErfTransient()
    : TransientModel()
{
}

double ErfTransient::transient(double x, double sigma) const
{
    ASSERT(sigma >= 0);
    if (sigma == 0.0)
        return Math::Heaviside(x);

    return (1.0 + std::erf(erf_prefactor * x / sigma)) / 2.0;
}

double ErfTransient::distribution(double x, double sigma) const
{
    ASSERT(sigma > 0);
    return erf_prefactor / pi_2 / sigma * std::exp(-pow(erf_prefactor * x / sigma, 2));
}

double ErfTransient::sigmaRange() const
{
    return 6.;
}

ErfTransient* ErfTransient::clone() const
{
    return new ErfTransient;
}

//-------------------------------------------------------------------------------------------------

TanhTransient::TanhTransient()
    : TransientModel()
{
}

double TanhTransient::transient(double x, double sigma) const
{
    ASSERT(sigma >= 0);
    if (sigma == 0.0)
        return Math::Heaviside(x);

    return (1.0 + std::tanh(tanh_prefactor * x / sigma)) / 2.0;
}

double TanhTransient::distribution(double x, double sigma) const
{
    ASSERT(sigma > 0);
    return tanh_prefactor / 2 / sigma / pow(std::cosh(tanh_prefactor * x / sigma), 2);
}

double TanhTransient::sigmaRange() const
{
    return 11.;
}

TanhTransient* TanhTransient::clone() const
{
    return new TanhTransient;
}
