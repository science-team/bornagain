//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Interface/TransientModels.h
//! @brief     Define TransientModel classes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_INTERFACE_TRANSIENTMODELS_H
#define BORNAGAIN_SAMPLE_INTERFACE_TRANSIENTMODELS_H

#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"

class TransientModel : public ICloneable, public INode {
public:
#ifndef SWIG
    TransientModel* clone() const override = 0;
#endif // SWIG

    virtual double transient(double x, double sigma) const = 0;
    virtual double distribution(double x, double sigma) const = 0;
    virtual double sigmaRange() const = 0;
};

class ErfTransient : public TransientModel {
public:
    ErfTransient();
    double transient(double x, double sigma) const override;
    double distribution(double x, double sigma) const override;
    double sigmaRange() const override;
    std::string className() const override { return "ErfTransient"; }

#ifndef SWIG
    ErfTransient* clone() const override;
#endif // SWIG
};

class TanhTransient : public TransientModel {
public:
    TanhTransient();
    double transient(double x, double sigma) const override;
    double distribution(double x, double sigma) const override;
    double sigmaRange() const override;
    std::string className() const override { return "TanhTransient"; }

#ifndef SWIG
    TanhTransient* clone() const override;
#endif // SWIG
};

#endif // BORNAGAIN_SAMPLE_INTERFACE_TRANSIENTMODELS_H
