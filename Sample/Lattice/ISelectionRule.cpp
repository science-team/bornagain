//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Lattice/ISelectionRule.cpp
//! @brief     Implements classes ISelectionRule, SimpleSelectionRule.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Lattice/ISelectionRule.h"

SimpleSelectionRule::SimpleSelectionRule(int a, int b, int c, int modulus)
    : m_a(a)
    , m_b(b)
    , m_c(c)
    , m_mod(modulus)
{
}

SimpleSelectionRule* SimpleSelectionRule::clone() const
{
    return new SimpleSelectionRule(m_a, m_b, m_c, m_mod);
}

bool SimpleSelectionRule::coordinateSelected(const I3& coordinate) const
{
    return (m_a * coordinate.x() + m_b * coordinate.y() + m_c * coordinate.z()) % m_mod == 0;
}

bool SimpleSelectionRule::isEqualTo(const ISelectionRule& isr) const
{
    if (const auto* sr = dynamic_cast<const SimpleSelectionRule*>(&isr))
        return *this == *sr;
    return false;
}

bool SimpleSelectionRule::operator==(const SimpleSelectionRule& other) const
{
    return (m_a == other.m_a && m_b == other.m_b && m_c == other.m_c && m_mod == other.m_mod);
}
