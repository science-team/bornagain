//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Lattice/ISelectionRule.h
//! @brief     Defines classes ISelectionRule, SimpleSelectionRule.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_LATTICE_ISELECTIONRULE_H
#define BORNAGAIN_SAMPLE_LATTICE_ISELECTIONRULE_H

#include <heinz/Vectors3D.h>

//! Abstract base class for selection rules.

class ISelectionRule {
public:
    virtual ~ISelectionRule() = default;

#ifndef SWIG
    virtual ISelectionRule* clone() const = 0;
#endif // SWIG

    virtual bool coordinateSelected(const I3& coordinate) const = 0;

    virtual bool isEqualTo(const ISelectionRule& isr) const = 0;

    bool operator==(const ISelectionRule&) const { return false; }
};

//! Selection rule (v*q)%modulus!=0, defined by vector v(a,b,c) and modulus.

class SimpleSelectionRule : public ISelectionRule {
public:
    SimpleSelectionRule(int a, int b, int c, int modulus);
    ~SimpleSelectionRule() override = default;

#ifndef SWIG
    SimpleSelectionRule* clone() const override;
#endif // SWIG

    bool coordinateSelected(const I3& coordinate) const override;

    bool isEqualTo(const ISelectionRule& isr) const override;

    bool operator==(const SimpleSelectionRule& other) const;

private:
    int m_a, m_b, m_c;
    int m_mod;
};

#endif // BORNAGAIN_SAMPLE_LATTICE_ISELECTIONRULE_H
