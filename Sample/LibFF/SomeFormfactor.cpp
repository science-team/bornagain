//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/LibFF/SomeFormfactor.cpp
//! @brief     Implements namespace someff with some form factor functions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/LibFF/SomeFormfactor.h"
#include "Base/Const/PhysicalConstants.h"

using PhysConsts::pi;

//! Returns the form factor of a sphere of radius R.
//!
//! Used by the hard sphere and by several soft sphere classes.

complex_t SampleUtil::someff::ffSphere(C3 q, double R)
{
    complex_t q1 =
        sqrt(q.x() * q.x() + q.y() * q.y() + q.z() * q.z()); // NO sesquilinear dot product!
    complex_t qR = q1 * R;

    if (std::abs(qR) < 1e-4) { // relative error is O(qR^4) with small prefactor
        // expand sin(qR)-qR*cos(qR) up to qR^5
        return 4 * pi / 3 * pow(R, 3) * (1. - 0.1 * pow(qR, 2));
    }
    return 4 * pi * pow(q1, -3) * (sin(qR) - qR * cos(qR));
}
