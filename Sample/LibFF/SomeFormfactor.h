//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/LibFF/SomeFormfactor.h
//! @brief     Declares namespace someff with some form factor functions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_LIBFF_SOMEFORMFACTOR_H
#define BORNAGAIN_SAMPLE_LIBFF_SOMEFORMFACTOR_H

#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>

namespace SampleUtil {

//! Some form factor functions.

namespace someff {

complex_t ffSphere(C3 q, double R);

} // namespace someff

} // namespace SampleUtil

#endif // BORNAGAIN_SAMPLE_LIBFF_SOMEFORMFACTOR_H
