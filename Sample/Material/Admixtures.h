//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Material/Admixtures.h
//! @brief     Defines struct OneAdmixture, and declares fct createAveragedMaterial.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_MATERIAL_ADMIXTURES_H
#define BORNAGAIN_SAMPLE_MATERIAL_ADMIXTURES_H

#include "Sample/Material/Material.h"
#include <vector>

//! An admixture to a slice, consisting of a certain volume fraction of a homogeneous material.
//!

struct OneAdmixture {
    double fraction; //!< Volume fraction
    Material material;
};

//! The list of material admixtures to a slice.
//!
//! Needed for calculating the average of a material, which is needed in the Fresnel calculation.

class Admixtures : public std::vector<OneAdmixture> {};

#endif // BORNAGAIN_SAMPLE_MATERIAL_ADMIXTURES_H
