//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Material/IMaterialImpl.h
//! @brief     Defines magnetic material base implementation.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_MATERIAL_IMATERIALIMPL_H
#define BORNAGAIN_SAMPLE_MATERIAL_IMATERIALIMPL_H

#include "Base/Vector/RotMatrix.h"
#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>

class SpinMatrix;
class WavevectorInfo;

enum class MATERIAL_TYPES { InvalidMaterialType = -1, RefractiveMaterial = 0, MaterialBySLD };

//! Basic implementation for magnetized material.
//! Inherited by RefractiveMaterialImpl and MaterialBySLDImpl.
//! Incorporates data and methods required to handle material magnetization.

class IMaterialImpl {
public:
    //! Constructs basic material with name and magnetization
    IMaterialImpl(const std::string& name, const R3& magnetization);

    virtual ~IMaterialImpl() = default;

#ifndef SWIG
    //! Returns pointer to a copy of material
    virtual IMaterialImpl* clone() const = 0;
#endif // SWIG

    //! Constructs a material with inverted magnetization
    virtual IMaterialImpl* inverted() const;

    //! Returns refractive index.
    virtual complex_t refractiveIndex(double wavelength) const = 0;

    //! Returns squared refractive index.
    virtual complex_t refractiveIndex2(double wavelength) const = 0;

    //! Returns underlying SLD or refractive index value
    virtual complex_t refractiveIndex_or_SLD() const = 0;

    //! Returns type of material implementation
    virtual MATERIAL_TYPES typeID() const = 0;

    //! Returns (\f$ \pi/\lambda^2 \f$ - sld), sld being the scattering length density
    virtual complex_t scalarSubtrSLD(double lambda0) const = 0;

    //! Indicates whether the interaction with the material is scalar.
    //! This means that different polarization states will be diffracted equally
    virtual bool isScalarMaterial() const;

    virtual bool isMagneticMaterial() const;

    //! Returns the magnetization (in A/m)
    virtual R3 magnetization() const { return m_magnetization; }

    //! Returns (\f$ \pi/\lambda^2 \f$ - sld) matrix with magnetization corrections
    virtual SpinMatrix polarizedSubtrSLD(const WavevectorInfo& wavevectors) const;

    virtual IMaterialImpl* rotatedMaterial(const RotMatrix& transform) const;

    //! Prints object data
    virtual std::string print() const = 0;

    //! Returns name of the material
    const std::string& matName() const { return m_name; }

private:
    void setMagnetization(const R3& magnetization) { m_magnetization = magnetization; }

    const std::string m_name;
    R3 m_magnetization; //!< magnetization
};

#endif // BORNAGAIN_SAMPLE_MATERIAL_IMATERIALIMPL_H
