//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Material/Material.h
//! @brief     Defines and implements class Material.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_MATERIAL_MATERIAL_H
#define BORNAGAIN_SAMPLE_MATERIAL_MATERIAL_H

#include "Base/Vector/RotMatrix.h"
#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>
#include <memory>
#include <ostream>
#include <vector>

#ifndef SWIG
#include "Sample/Material/IMaterialImpl.h"
#endif // SWIG

class SpinMatrix;
class WavevectorInfo;

//! A wrapper for underlying material implementation

class Material {
public:
#ifndef SWIG
    //! Creates material with particular material implementation
    Material(std::unique_ptr<IMaterialImpl>&& material_impl);
#endif // SWIG

    Material(const Material& material);
#ifndef SWIG
    Material(Material&& material) = default;
#endif // SWIG

    Material& operator=(const Material& other);
#ifndef SWIG
    Material& operator=(Material&& other) = default;
#endif // SWIG

    //! Constructs a material with inverted magnetization
    Material inverted() const;

    //! Returns refractive index
    complex_t refractiveIndex(double wavelength) const;

    //! Returns squared refractive index
    complex_t refractiveIndex2(double wavelength) const;

    //! Indicates whether the interaction with the material is scalar.
    //! This means that different polarization states will be diffracted equally
    bool isScalarMaterial() const;

    bool isMagneticMaterial() const;

    //! Returns the name of material
    std::string materialName() const;

#ifndef SWIG
    //! Returns the type of underlying material implementation
    MATERIAL_TYPES typeID() const;

    void checkRefractiveIndex(double wavelength) const;
#endif // SWIG

    //! Get the magnetization (in A/m)
    R3 magnetization() const;

    //! Returns underlying SLD or refractive index value
    complex_t refractiveIndex_or_SLD() const;

    //! Returns true if material underlying data is nullptr
    bool isEmpty() const;

    //! Returns true if material has refractive index of (1.0, 0.0) and zero magnetization.
    bool isDefaultMaterial() const;

    //! Returns (\f$ \pi/\lambda^2 \f$ - sld),
    //! sld (in \f$nm^{-2}\f$) being the scattering length density
    complex_t scalarSubtrSLD(const WavevectorInfo& wavevectors) const;

    //! Returns (\f$ \pi/\lambda^2 \f$ - sld) matrix with magnetization corrections
    SpinMatrix polarizedSubtrSLD(const WavevectorInfo& wavevectors) const;

    Material rotatedMaterial(const RotMatrix& transform) const;

    friend std::ostream& operator<<(std::ostream& ostr, const Material& mat);

private:
#ifndef SWIG
    std::unique_ptr<IMaterialImpl> m_material_impl;
#endif // SWIG
};

//! Comparison operator for material wrapper (equality check)
bool operator==(const Material& left, const Material& right);

//! Comparison operator for material wrapper (inequality check)
bool operator!=(const Material& left, const Material& right);

#endif // BORNAGAIN_SAMPLE_MATERIAL_MATERIAL_H
