//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Material/MaterialBySLDImpl.h
//! @brief     Defines class MaterialBySLDImpl.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_MATERIAL_MATERIALBYSLDIMPL_H
#define BORNAGAIN_SAMPLE_MATERIAL_MATERIALBYSLDIMPL_H

#include "Sample/Material/IMaterialImpl.h"
#include "Sample/Material/MaterialFactoryFuncs.h"

//! Material implementation based on wavelength-independent data (valid for a range of wavelengths)

class MaterialBySLDImpl : public IMaterialImpl {
public:
    friend Material MaterialBySLD(const std::string& name, double sld_real, double sld_imag,
                                  const R3& magnetization);

    ~MaterialBySLDImpl() override = default;

#ifndef SWIG
    //! Returns pointer to a copy of material
    MaterialBySLDImpl* clone() const override;
#endif // SWIG

    //! Returns refractive index
    complex_t refractiveIndex(double wavelength) const override;

    //! Returns squared refractive index
    complex_t refractiveIndex2(double wavelength) const override;

    //! Returns underlying material SLD in AA^-2
    complex_t refractiveIndex_or_SLD() const override;

    //! Returns type of material implementation
    MATERIAL_TYPES typeID() const override { return MATERIAL_TYPES::MaterialBySLD; }

    //! Returns (\f$ \pi/\lambda^2 \f$ - sld), sld (in \f$nm^{-2}\f$) being the scattering length
    //! density
    complex_t scalarSubtrSLD(double lambda0) const override;

    //! Prints object SLD (in \f$ AA^{-2} \f$) and magnetization
    std::string print() const override;

private:
    //! Constructs a wavelength-independent material with a given complex-valued
    //! scattering length density (SLD). SLD units are \f$ nm^{-2} \f$.
    MaterialBySLDImpl(const std::string& name, double sld_real, double sld_imag,
                      const R3& magnetization);
    //! Returns the scattering length density in \f$ nm^{-2} \f$
    complex_t sld() const;

    const double m_sld_real; //!< complex-valued scattering length density
    const double m_sld_imag; //!< imaginary part of scattering length density (negative by default)
};

#endif // BORNAGAIN_SAMPLE_MATERIAL_MATERIALBYSLDIMPL_H
