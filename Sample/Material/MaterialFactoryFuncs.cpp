//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Material/MaterialFactoryFuncs.cpp
//! @brief     Factory functions used to create material instances.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Base/Const/Units.h"
#include "Sample/Material/MaterialBySLDImpl.h"
#include "Sample/Material/MaterialUtil.h"
#include "Sample/Material/RefractiveMaterialImpl.h"

Material RefractiveMaterial(const std::string& name, complex_t refractive_index,
                            const R3& magnetization)
{
    const double delta = 1.0 - refractive_index.real();
    const double beta = refractive_index.imag();
    return RefractiveMaterial(name, delta, beta, magnetization);
}

Material RefractiveMaterial(const std::string& name, double delta, double beta,
                            const R3& magnetization)
{
    std::unique_ptr<RefractiveMaterialImpl> mat_impl(
        new RefractiveMaterialImpl(name, delta, beta, magnetization));
    return {std::move(mat_impl)};
}

Material Vacuum()
{
    return RefractiveMaterial("vacuum", 0.0, 0.0, R3{});
}

// accepts SLD in AA^-2
Material MaterialBySLD(const std::string& name, double sld_real, double sld_imag,
                       const R3& magnetization)
{
    constexpr double inv_sq_angstroms = 1.0 / (Units::angstrom * Units::angstrom);
    std::unique_ptr<MaterialBySLDImpl> mat_impl(new MaterialBySLDImpl(
        name, sld_real * inv_sq_angstroms, sld_imag * inv_sq_angstroms, magnetization));
    return {std::move(mat_impl)};
}

Material MaterialBySLD()
{
    return MaterialBySLD("vacuum", 0.0, 0.0, R3{});
}
