//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Material/MaterialUtil.h
//! @brief     Declares functions in namespace MaterialUtil.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_MATERIAL_MATERIALUTIL_H
#define BORNAGAIN_SAMPLE_MATERIAL_MATERIALUTIL_H

#include "Sample/Material/Admixtures.h"
#include "Sample/Material/Material.h"

//! A number of materials-related helper functions for internal use

namespace MaterialUtil {

//! Function for calculating the reduced potential, used for obtaining the Fresnel coefficients
//! (non-polarized material case)

complex_t ScalarReducedPotential(complex_t n, const R3& k, double n_ref);

//! Function for calculating the reduced potential, used for obtaining the Fresnel coefficients
//! (polarized material case)

SpinMatrix PolarizedReducedPotential(complex_t n, const R3& b_field, const R3& k, double n_ref);

// Utility to compute magnetization correction for reduced potential and scattering length density.
// Used with T = double, complex_t.

template <typename T>
SpinMatrix MagnetizationCorrection(complex_t unit_factor, double magnetic_factor,
                                   const Vec3<T>& polarization);

//! Checks if all non-default materials in _materials_ are of the same type and returns this type.
//! If several types of materials are involved, InvalidMaterialType identifier is returned.

MATERIAL_TYPES checkMaterialTypes(const std::vector<const Material*>& materials);

//! Returns material with averaged properties.
Material averagedMaterial(const std::string& name, const std::vector<double>& weights,
                          const std::vector<Material>& materials);

//! Returns material with averaged properties.
Material averagedMaterial(const Material& base_mat, const Admixtures& admixtures);

} // namespace MaterialUtil

#endif // BORNAGAIN_SAMPLE_MATERIAL_MATERIALUTIL_H
