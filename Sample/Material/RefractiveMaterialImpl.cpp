//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Material/RefractiveMaterialImpl.cpp
//! @brief     Implements class RefractiveMaterialImpl.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Material/RefractiveMaterialImpl.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Vector/WavevectorInfo.h"
#include <sstream>

using PhysConsts::pi;

RefractiveMaterialImpl::RefractiveMaterialImpl(const std::string& name, double delta, double beta,
                                               const R3& magnetization)
    : IMaterialImpl(name, magnetization)
    , m_delta(delta)
    , m_beta(beta < 0.
                 ? throw std::runtime_error(
                       "The imaginary part of the refractive index must be greater or equal zero")
                 : beta)
{
}

RefractiveMaterialImpl* RefractiveMaterialImpl::clone() const
{
    return new RefractiveMaterialImpl(*this);
}

complex_t RefractiveMaterialImpl::refractiveIndex(double) const
{
    return {1.0 - m_delta, m_beta};
}

complex_t RefractiveMaterialImpl::refractiveIndex2(double) const
{
    complex_t result(1.0 - m_delta, m_beta);
    return result * result;
}

complex_t RefractiveMaterialImpl::refractiveIndex_or_SLD() const
{
    return {m_delta, m_beta};
}

complex_t RefractiveMaterialImpl::scalarSubtrSLD(double lambda0) const
{
    if (std::isnan(lambda0))
        throw std::runtime_error("wavelength not set");
    return pi / lambda0 / lambda0 * refractiveIndex2(lambda0);
}

std::string RefractiveMaterialImpl::print() const
{
    std::stringstream s;
    s << "RefractiveMaterial:" << matName() << "<" << this << ">{ "
      << "delta=" << m_delta << ", beta=" << m_beta << ", B=" << magnetization() << "}";
    return s.str();
}
