//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Material/RefractiveMaterialImpl.h
//! @brief     Defines class RefractiveMaterialImpl.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_MATERIAL_REFRACTIVEMATERIALIMPL_H
#define BORNAGAIN_SAMPLE_MATERIAL_REFRACTIVEMATERIALIMPL_H

#include "Sample/Material/IMaterialImpl.h"
#include "Sample/Material/Material.h"

//! Material implementation based on refractive coefficiencts (valid for one wavelength value only)

class RefractiveMaterialImpl : public IMaterialImpl {
public:
    friend Material RefractiveMaterial(const std::string&, double, double, const R3&);

    ~RefractiveMaterialImpl() override = default;

#ifndef SWIG
    //! Returns pointer to a copy of material
    RefractiveMaterialImpl* clone() const override;
#endif // SWIG

    //! Returns refractive index
    //! For this particular implementation returned value does not depend
    //! on passed wavelength
    complex_t refractiveIndex(double wavelength) const override;

    //! Returns squared refractive index.
    //! For this particular implementation returned value does not depend
    //! on passed wavelength.
    complex_t refractiveIndex2(double wavelength) const override;

    //! Returns underlying refractive index value
    complex_t refractiveIndex_or_SLD() const override;

    //! Returns type of material implementation
    MATERIAL_TYPES typeID() const override { return MATERIAL_TYPES::RefractiveMaterial; }

    //! Returns (\f$ \pi/\lambda^2 \f$ - sld), sld (in \f$nm^{-2}\f$) being the scattering length
    //! density. If the wavelength associated with passed wavevector is different from the one
    //! associated with refractive coefficients used during the object construction,
    //! provided result is inconsistent.
    complex_t scalarSubtrSLD(double lambda0) const override;

    //! Prints object data
    std::string print() const override;

private:
    RefractiveMaterialImpl(const std::string& name, double delta, double beta,
                           const R3& magnetization);

    const double
        m_delta; //!< \f$\delta\f$ coefficient for refractive index \f$n = 1 - \delta + i \beta\f$
    const double
        m_beta; //!< \f$\beta\f$ coefficient for refractive index \f$n = 1 - \delta + i \beta\f$
};

#endif // BORNAGAIN_SAMPLE_MATERIAL_REFRACTIVEMATERIALIMPL_H
