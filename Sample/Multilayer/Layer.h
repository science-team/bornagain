//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Multilayer/Layer.h
//! @brief     Defines class Layer.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_MULTILAYER_LAYER_H
#define BORNAGAIN_SAMPLE_MULTILAYER_LAYER_H

#include "Base/Type/OwningVector.h"
#include "Sample/Material/Material.h"
#include "Sample/Scattering/ISampleNode.h"

class Layer;
class ParticleLayout;
class Roughness;

//! Common base for layers and stacks.

class ILayer : public ISampleNode {
public:
#ifndef SWIG
    ILayer* clone() const override = 0;

    //! Unwrapped sequence of all layers contained inside
    virtual std::vector<const Layer*> unwrapped() const = 0;
#endif // SWIG

    virtual void checkMaterials(double wavelength) const = 0;
};

//! A layer in a MultiLayer sample.

class Layer : public ILayer {
public:
    Layer(const Material& material, double thickness = 0, const Roughness* roughness = nullptr);
    Layer(const Material& material, const Roughness* roughness);
    ~Layer() override;

    std::string className() const final { return "Layer"; }
    void checkMaterials(double wavelength) const override;

    void addLayout(const ParticleLayout& layout);

    void setNumberOfSlices(unsigned int n_slices) { m_n_slices = n_slices; }

#ifndef SWIG
    Layer* clone() const override;

    std::vector<const INode*> nodeChildren() const override;

    std::vector<const Layer*> unwrapped() const override;

    std::string validate() const override;

    std::vector<ParaMeta> parDefs() const final { return {{"Thickness", "nm"}}; }

    const Material* material() const override { return &m_material; }
    double thickness() const { return m_thickness; }
    size_t numberOfLayouts() const { return m_layouts.size(); }
    std::vector<const ParticleLayout*> layouts() const;
    unsigned int numberOfSlices() const { return m_n_slices; }
    const Roughness* roughness() const { return m_roughness.get(); }

private:
    Material m_material;                          //!< material
    R3 m_B_field;                                 //!< cached value of magnetic induction
    double m_thickness;                           //!< layer thickness in nanometers
    OwningVector<ParticleLayout> m_layouts;       //!< independent layouts in this layer
    std::unique_ptr<const Roughness> m_roughness; //!< roughness of the top surface. Never null
    unsigned int m_n_slices = 1; //!< number of slices to create for graded layer approach
#endif                           // SWIG
};

#endif // BORNAGAIN_SAMPLE_MULTILAYER_LAYER_H
