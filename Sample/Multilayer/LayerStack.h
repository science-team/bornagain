//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Multilayer/LayerStack.h
//! @brief     Defines class LayerStack.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2024
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_MULTILAYER_LAYERSTACK_H
#define BORNAGAIN_SAMPLE_MULTILAYER_LAYERSTACK_H

#include "Sample/Multilayer/Layer.h"

class LayerStack : public ILayer {
public:
    LayerStack(size_t numRepetitions = 1);
    ~LayerStack() override;

    std::string className() const final { return "LayerStack"; }
    void checkMaterials(double wavelength) const override;

    void addLayer(const Layer& layer);
    void addStack(const LayerStack& substack);

    void setNumberOfPeriods(size_t n) { m_n_periods = n; }
    size_t numberOfPeriods() const { return m_n_periods; }

#ifndef SWIG
    LayerStack* clone() const override;

    std::vector<const INode*> nodeChildren() const override;

    std::vector<const Layer*> unwrapped() const override;

    std::string validate() const override;
    std::vector<ParaMeta> parDefs() const final { return {{"NumRepetitions", ""}}; }
    const std::vector<const ILayer*>& components() const { return m_components.shared(); }

private:
    void addComponent(const ILayer& component);

    size_t m_n_periods;
    OwningVector<const ILayer> m_components;
#endif // SWIG
};

#endif // BORNAGAIN_SAMPLE_MULTILAYER_LAYERSTACK_H
