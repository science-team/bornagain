//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Multilayer/Sample.h
//! @brief     Defines class Sample.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_MULTILAYER_SAMPLE_H
#define BORNAGAIN_SAMPLE_MULTILAYER_SAMPLE_H

#include "Base/Type/OwningVector.h"
#include "Sample/Scattering/ISampleNode.h"
#include <functional>
#include <heinz/Vectors3D.h>

#include <string>

class AutocorrelationModel;
class ILayer;
class Layer;
class LayerStack;
class ParticleLayout;

//! Our sample model: a stack of layers one below the other.

//! Example of system of 4 layers (3 interfaces):
//! @verbatim
//! vacuum      layer 0
//! ---------   interface 0    z=0.0
//! Fe, 20A     layer 1
//! ---------   interface 1    z=-20.0
//! Cr, 40A     layer 2
//! ---------   interface 2    z=-60.0
//! substrate   layer 3
//! @endverbatim

class Sample : public ISampleNode {
public:
    Sample();
    ~Sample() override;

    std::string className() const final { return "Sample"; }

    void addLayer(const Layer& layer);
    void addStack(const LayerStack& substack);

    //! Sets the external field applied to the sample (units: A/m)
    void setExternalField(const R3& ext_field);

    void setName(const std::string& name) { m_sample_name = name; }

    //! Returns roughness spectrum for a given layer index
    double roughnessSpectrum(double spatial_f, int i_layer) const;

    //! Returns roughness rms for a given layer index
    double roughnessRMS(size_t i_layer) const;

#ifndef SWIG
    Sample* clone() const override;

    std::vector<const INode*> nodeChildren() const override;

    std::string validate() const override;

    const std::string& name() const { return m_sample_name; }

    double hig(size_t i) const; //!< Returns top z coordinate of layer i
    double low(size_t i) const; //!< Returns bottom z coordinate of layer i

    size_t numberOfLayers() const;

    const Layer* layer(size_t i_layer) const; //! Layer in unwrapped structure

    R3 externalField() const { return m_ext_field; } //!< (units: A/m)

    void checkMaterials(double wavelength) const;

    const LayerStack& outerStack() const { return *m_outer_stack; }
    void setOuterStack(const LayerStack& outer_stack);

    void checkAndProcess() const;
    std::string validateAmbientSubstrate() const;

private:
    const AutocorrelationModel* autocorrAt(int i_layer) const;
    double maxCutoffSpatialFrequencyAt(size_t i_layer) const;

    std::unique_ptr<LayerStack> m_outer_stack; //!< stack of layers and substacks. Never nullptr
    R3 m_ext_field;                            //!< external magnetic field (in A/m)

    std::string m_sample_name{"Unnamed"};

    mutable std::vector<double> ZInterfaces;
    mutable std::vector<const Layer*> unwrappedLayers;
#endif // SWIG
};

#ifdef BORNAGAIN_PYTHON

//! Extracts a C++ Sample instance from a Python object which wraps a Sample instance
// NOTE: This function needs the SWIG API and is defined in the corresponding SWIG header
#ifndef SWIG
std::unique_ptr<Sample> BA_SWIG_sampleFromPyObject(void* pSample);
#endif // SWIG

#endif // BORNAGAIN_PYTHON

#endif // BORNAGAIN_SAMPLE_MULTILAYER_SAMPLE_H
