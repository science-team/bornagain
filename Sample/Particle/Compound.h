//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/Compound.h
//! @brief     Defines class Compound.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_PARTICLE_COMPOUND_H
#define BORNAGAIN_SAMPLE_PARTICLE_COMPOUND_H

#include "Sample/Particle/IParticle.h"

//! A composition of particles at fixed positions

class Compound : public IParticle {
public:
    Compound();
    ~Compound() override;

    std::string className() const final { return "Compound"; }

    void addComponent(const IParticle& particle);
    void addComponent(const IParticle& particle, const R3& position);
    void addComponents(const IParticle& particle, std::vector<R3> positions);

#ifndef SWIG
    Compound* clone() const override;

    std::vector<const INode*> nodeChildren() const override;

    OwningVector<IParticle> decompose() const override;

    //! Returns number of different particles
    size_t nbrParticles() const { return m_particles.size(); }

    std::vector<const IParticle*> particles() const;

    std::string validate() const override { return ""; }

    Span zSpan() const override;

private:
    OwningVector<IParticle> m_particles;
#endif // SWIG
};

#endif // BORNAGAIN_SAMPLE_PARTICLE_COMPOUND_H
