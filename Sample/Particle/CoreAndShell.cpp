//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/CoreAndShell.cpp
//! @brief     Implements class CoreAndShell.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Particle/CoreAndShell.h"
#include "Base/Type/Span.h"
#include "Base/Util/Assert.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"
#include <iostream>

CoreAndShell::CoreAndShell(const Particle& core, const Particle& shell)
{
    m_core.reset(core.clone());
    m_shell.reset(shell.clone());
}

CoreAndShell::~CoreAndShell() = default;

CoreAndShell* CoreAndShell::clone() const
{
    auto* result = new CoreAndShell(*m_core, *m_shell);
    result->setAbundance(m_abundance);
    if (rotation())
        result->rotate(*rotation());
    result->translate(particlePosition());
    return result;
}

std::vector<const INode*> CoreAndShell::nodeChildren() const
{
    return std::vector<const INode*>() << IParticle::nodeChildren() << m_core << m_shell;
}

Span CoreAndShell::zSpan() const
{
    return m_shell->zSpan() + particlePosition().z();
}
