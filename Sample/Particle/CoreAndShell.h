//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/CoreAndShell.h
//! @brief     Defines CoreAndShell.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_PARTICLE_COREANDSHELL_H
#define BORNAGAIN_SAMPLE_PARTICLE_COREANDSHELL_H

#include "Sample/Particle/IParticle.h"

class Particle;

//! A particle with a core/shell geometry.

class CoreAndShell : public IParticle {
public:
    CoreAndShell(const Particle& core, const Particle& shell);
    ~CoreAndShell() override;

    std::string className() const final { return "CoreAndShell"; }

#ifndef SWIG
    CoreAndShell* clone() const override;

    std::vector<const INode*> nodeChildren() const override;

    const Particle* shellParticle() const { return m_shell.get(); }
    const Particle* coreParticle() const { return m_core.get(); }

    std::string validate() const override { return ""; }

    Span zSpan() const override;

private:
    std::unique_ptr<Particle> m_core;
    std::unique_ptr<Particle> m_shell;
#endif // SWIG
};

#endif // BORNAGAIN_SAMPLE_PARTICLE_COREANDSHELL_H
