//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/Crystal.cpp
//! @brief     Implements class Crystal.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Particle/Crystal.h"
#include "Base/Vector/RotMatrix.h"
#include "Sample/Lattice/Lattice3D.h"
#include "Sample/Particle/Compound.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"

Crystal::Crystal(const IParticle& basis, const Lattice3D& lattice, double position_variance)
    : Crystal(basis.clone(), lattice, position_variance)
{
}

Crystal::Crystal(IParticle* basis, const Lattice3D& lattice, double position_variance)
    : m_basis(basis)
    , m_lattice(std::make_unique<Lattice3D>(lattice))
    , m_position_variance(position_variance)
{
}

Crystal::~Crystal() = default;

Crystal* Crystal::clone() const
{
    return new Crystal(*m_basis, *m_lattice, m_position_variance);
}

std::vector<const INode*> Crystal::nodeChildren() const
{
    return std::vector<const INode*>() << m_basis << m_lattice;
}

Crystal* Crystal::transformed(const R3& translation, const IRotation* rotation) const
{
    const Lattice3D new_lattice = rotation ? m_lattice->rotated(rotation->rotMatrix()) : *m_lattice;
    IParticle* new_basis{m_basis->clone()};
    if (rotation)
        new_basis->rotate(*rotation);
    new_basis->translate(translation);
    return new Crystal(new_basis, new_lattice, m_position_variance);
}
