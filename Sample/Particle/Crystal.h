//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/Crystal.h
//! @brief     Defines class Crystal.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_PARTICLE_CRYSTAL_H
#define BORNAGAIN_SAMPLE_PARTICLE_CRYSTAL_H

#include "Sample/Scattering/ISampleNode.h"
#include <heinz/Vectors3D.h>

class IParticle;
class IRotation;
class Lattice3D;

//! A crystal structure, defined by a Bravais lattice, a basis, and a position variance.
//!
//! Computations are delegated to class ReMesocrystal.
//!
//! Used in Mesocrystal, where it is given an outer shape.
//!

class Crystal : public ISampleNode {
public:
    Crystal(const IParticle& basis, const Lattice3D& lattice, double position_variance = 0);
    ~Crystal() override;

#ifndef SWIG
    Crystal* clone() const override;

    std::vector<const INode*> nodeChildren() const override;
#endif // SWIG

    std::string className() const final { return "Crystal"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Variance", "nm?"}}; }

    const IParticle* basis() const { return m_basis.get(); }
    const Lattice3D* lattice() const { return m_lattice.get(); }
    double position_variance() const { return m_position_variance; }

    Crystal* transformed(const R3& translation, const IRotation* rotation) const;

    std::string validate() const override { return ""; }

private:
    Crystal(IParticle* basis, const Lattice3D& lattice, double position_variance = 0);

    std::unique_ptr<IParticle> m_basis;
    std::unique_ptr<Lattice3D> m_lattice;
    const double m_position_variance;
};

#endif // BORNAGAIN_SAMPLE_PARTICLE_CRYSTAL_H
