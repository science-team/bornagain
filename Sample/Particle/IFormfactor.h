//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/IFormfactor.h
//! @brief     Defines interface IDecoratableBorn.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_PARTICLE_IFORMFACTOR_H
#define BORNAGAIN_SAMPLE_PARTICLE_IFORMFACTOR_H

#include "Base/Spin/SpinMatrix.h" // not forward declared because of SWIG
#include "Base/Spin/Spinor.h"     // not forward declared because of SWIG
#include "Sample/Scattering/ISampleNode.h"
#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>

class IRotation;
class IShape3D;
class Span;
class WavevectorInfo;

//! Abstract base class for Born form factors.
//!
//! In contrast to the generic IReParticle, a Born form factor does not depend
//! on the incoming and outgoing wave vectors ki and kf, except through their
//! difference, the scattering vector q=ki-kf.

class IFormfactor : public ISampleNode {
public:
    IFormfactor();
    IFormfactor(const std::vector<double>& PValues);
    ~IFormfactor() override;

#ifndef SWIG
    IFormfactor* clone() const override = 0;

    //! Creates the Python constructor of this class (or derived classes)
    std::string pythonConstructor() const;
#endif // SWIG

    virtual double volume() const;
    //! Returns the (approximate in some cases) radial size of the particle of this
    //! form factor's shape. This is used for SSCA calculations
    virtual double radialExtension() const = 0;

    virtual Span spanZ(const IRotation* rotation) const;


    //! Default implementation only allows rotations along z-axis
    virtual bool canSliceAnalytically(const IRotation* rot) const;

    complex_t theFF(const WavevectorInfo& wavevectors) const;
    SpinMatrix thePolFF(const WavevectorInfo& wavevectors) const;

    //! Returns scattering amplitude for complex scattering wavevector q=k_i-k_f in case
    //! of matrix interactions. Default implementation calls formfactor(q) and
    //! multiplies with the unit matrix.
    SpinMatrix formfactor_pol(C3 q) const;

    virtual complex_t formfactor(C3 q) const = 0;

    //! Checks whether the point of space lies inside the geometric figure ff.
    virtual bool contains(const R3& position) const = 0;

    //! Checks for exact equality of any two formfactors.
    bool isEqualTo(const IFormfactor* other) const;

protected:
    //! IShape3D object, used to retrieve vertices (which may be approximate in the case
    //! of round shapes). For soft particles, this will be a hard mean shape.
    mutable std::unique_ptr<IShape3D> m_shape3D;
};

#endif // BORNAGAIN_SAMPLE_PARTICLE_IFORMFACTOR_H
