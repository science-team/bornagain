//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/IParticle.cpp
//! @brief     Implements interface IParticle.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Particle/IParticle.h"
#include "Sample/Material/Material.h"
#include "Sample/Scattering/Rotations.h"

IParticle::~IParticle() = default;

std::vector<const INode*> IParticle::nodeChildren() const
{
    return std::vector<const INode*>() << m_rotation;
}

IParticle* IParticle::translate(const R3& translation)
{
    m_position += translation;
    return this;
}

const IRotation* IParticle::rotation() const
{
    return m_rotation.get();
}

IParticle* IParticle::rotate(const IRotation& rotation)
{
    if (m_rotation)
        m_rotation.reset(createProduct(rotation, *m_rotation));
    else
        m_rotation.reset(rotation.clone());
    m_position = rotation.transformed(m_position);
    return this;
}

OwningVector<IParticle> IParticle::decompose() const
{
    OwningVector<IParticle> result;
    result.push_back(this->clone());
    return result;
}

Material IParticle::avgeMaterial() const
{
    throw std::runtime_error("IParticle::avgeMaterial not yet implemented for composed particle");
}

double IParticle::volume() const
{
    throw std::runtime_error("IParticle::volume not yet implemented for composed particle");
}
