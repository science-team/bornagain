//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/IParticle.h
//! @brief     Defines interface IParticle.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_PARTICLE_IPARTICLE_H
#define BORNAGAIN_SAMPLE_PARTICLE_IPARTICLE_H

#include "Base/Type/OwningVector.h"
#include "Sample/Scattering/ISampleNode.h"
#include <heinz/Vectors3D.h>
#include <memory>

class IRotation;
class Span;

//! Abstract base class for Particle, Compound, CoreAndShell, Mesocrystal.
//! Provides position/rotation and form factor. Abundance is inherited from IParticle.

class IParticle : public ISampleNode {
public:
    ~IParticle() override;

    //! Translates the particle, and returns this.
    IParticle* translate(const R3& translation);

    //! Translates the particle, and returns this.
    IParticle* translate(double x, double y, double z) { return translate(R3(x, y, z)); }

    //! Rotates the particle, and returns this.
    IParticle* rotate(const IRotation& rotation);

    //! Sets abundance, i.e. fraction of this type of particles among all particles in the layout.
    void setAbundance(double abundance) { m_abundance = abundance; }

#ifndef SWIG
    IParticle* clone() const override = 0;

    std::vector<const INode*> nodeChildren() const override;

    double abundance() const { return m_abundance; }
    R3 particlePosition() const { return m_position; }
    const IRotation* rotation() const;

    virtual Material avgeMaterial() const;
    virtual double volume() const;
    virtual Span zSpan() const = 0;

    //! Decompose in constituent IParticle objects
    virtual OwningVector<IParticle> decompose() const;

    std::string validate() const override = 0;

protected:
    double m_abundance{1.0};

private:
    R3 m_position;
    std::unique_ptr<IRotation> m_rotation;
#endif // SWIG
};

#endif // BORNAGAIN_SAMPLE_PARTICLE_IPARTICLE_H
