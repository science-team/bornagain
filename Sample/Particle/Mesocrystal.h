//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/Mesocrystal.h
//! @brief     Defines class Mesocrystal.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_PARTICLE_MESOCRYSTAL_H
#define BORNAGAIN_SAMPLE_PARTICLE_MESOCRYSTAL_H

#include "Sample/Particle/IParticle.h"

class Crystal;
class IFormfactor;

//! Information about indexes of particles belonging to the mesocrystal outer shape.

#ifndef SWIG
struct ShapeIndexes {
    I3 min;                       //!< Minimum indexes for each axis
    I3 max;                       //!< Maximum indexes for each axis
    std::vector<I3> basisIndexes; //!< All particles belonging to the shape
    std::vector<std::vector<std::vector<std::pair<int, int>>>>
        k_pairs; //!< Collection of continuous ranges of k indexes for each [i,j] point, where i,j,k
                 //!< are indexes along the 1st, 2nd and 3rd axes correspondingly
};
#endif // SWIG

//! A particle with a crystalline inner structure, made of smaller particles,
//! and an outer shape described by a particle form factor.

class Mesocrystal : public IParticle {
public:
    Mesocrystal(const Crystal& crystal, const IFormfactor& formfactor);
    ~Mesocrystal() override;

    std::string className() const final { return "Mesocrystal"; }

#ifndef SWIG
    Mesocrystal* clone() const override;

    std::vector<const INode*> nodeChildren() const override;

    const IFormfactor* outerShape() const { return m_meso_formfactor.get(); }
    const Crystal& particleStructure() const;

    std::string validate() const override { return ""; }

    Span zSpan() const override;

    std::vector<R3> calcBasisPositions() const;
    ShapeIndexes calcBasisIndexes() const;

private:
    Mesocrystal(Crystal* crystal, IFormfactor* formfactor);

    const std::unique_ptr<Crystal> m_crystal;             //!< Crystalline inner structure
    const std::unique_ptr<IFormfactor> m_meso_formfactor; //!< Outer shape of the mesocrystal

#endif // SWIG
};

#endif // BORNAGAIN_SAMPLE_PARTICLE_MESOCRYSTAL_H
