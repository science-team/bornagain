//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/Particle.cpp
//! @brief     Implements class Particle.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Particle/Particle.h"
#include "Base/Type/Span.h"
#include "Base/Util/Assert.h"
#include "Base/Vector/RotMatrix.h"
#include "Sample/Particle/IFormfactor.h"
#include "Sample/Scattering/Rotations.h"

Particle::Particle(const Material& material, const IFormfactor& formfactor)
    : m_material(material)
    , m_formfactor(formfactor.clone())
{
}

Particle::~Particle() = default;

Particle* Particle::clone() const
{
    ASSERT(m_formfactor);
    auto* result = new Particle(m_material, *m_formfactor);
    result->setAbundance(m_abundance);
    if (rotation())
        result->rotate(*rotation());
    result->translate(particlePosition());

    return result;
}

std::vector<const INode*> Particle::nodeChildren() const
{
    return std::vector<const INode*>() << IParticle::nodeChildren() << m_formfactor;
}

std::string Particle::validate() const
{
    return m_formfactor->validate();
}
double Particle::volume() const
{
    return m_formfactor->volume();
}

Span Particle::zSpan() const
{
    return m_formfactor->spanZ(rotation()) + particlePosition().z();
}
