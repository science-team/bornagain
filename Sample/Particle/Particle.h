//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/Particle.h
//! @brief     Defines class Particle.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_PARTICLE_PARTICLE_H
#define BORNAGAIN_SAMPLE_PARTICLE_PARTICLE_H

#include "Sample/Material/Material.h"
#include "Sample/Particle/IParticle.h"

class IFormfactor;

//! A particle with a form factor and refractive index.

class Particle : public IParticle {
public:
    Particle(const Material& material, const IFormfactor& formfactor);
    ~Particle() override;

    std::string className() const override { return "Particle"; }

#ifndef SWIG
    Particle* clone() const override;

    std::vector<const INode*> nodeChildren() const override;

    const Material* material() const override { return &m_material; }

    const IFormfactor* pFormfactor() const { return m_formfactor.get(); }

    std::string validate() const override;

    Material avgeMaterial() const override { return m_material; }
    double volume() const override;
    Span zSpan() const override;

private:
    const Material m_material;
    std::unique_ptr<const IFormfactor> m_formfactor;
#endif // SWIG
};

#endif // BORNAGAIN_SAMPLE_PARTICLE_PARTICLE_H
