//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/PolyhedralUtil.cpp
//! @brief     Implements class interface IFormfactor.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Particle/PolyhedralUtil.h"
#include "Base/Type/Span.h"
#include "Base/Util/Assert.h"
#include "Base/Vector/RotMatrix.h"
#include "Sample/Scattering/Rotations.h"

Span PolyhedralUtil::spanZ(const std::vector<R3>& vertices, const IRotation* rotation)
{
    ASSERT(!vertices.empty());
    const R3& v0 = vertices[0];
    double z0 = (rotation ? rotation->transformed(v0) : v0).z();
    Span result(z0, z0);
    for (size_t i = 1; i < vertices.size(); ++i) {
        const R3& v = vertices[i];
        ASSERT(std::isfinite(v.x()));
        ASSERT(std::isfinite(v.y()));
        ASSERT(std::isfinite(v.z()));
        double zi = (rotation ? rotation->transformed(v) : v).z();
        ASSERT(std::isfinite(zi));
        result = Span::unite(result, Span(zi, zi));
    }
    return result;
}
