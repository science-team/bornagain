//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Particle/PolyhedralUtil.h
//! @brief     Defines interface IDecoratableBorn.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_PARTICLE_POLYHEDRALUTIL_H
#define BORNAGAIN_SAMPLE_PARTICLE_POLYHEDRALUTIL_H

#include <heinz/Vectors3D.h>
#include <vector>

class IRotation;
class Span;

namespace PolyhedralUtil {

//! Returns z-coordinate range of the vertices after rotation
Span spanZ(const std::vector<R3>& vertices, const IRotation* rotation);

} // namespace PolyhedralUtil

#endif // BORNAGAIN_SAMPLE_PARTICLE_POLYHEDRALUTIL_H
