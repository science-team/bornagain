//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Scattering/ISampleNode.cpp
//! @brief     Implements interface ISampleNode.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Scattering/ISampleNode.h"
#include "Sample/Material/Material.h"
#include <algorithm>

ISampleNode::ISampleNode(const std::vector<double>& PValues)
    : INode(PValues)
{
}

std::vector<const Material*> ISampleNode::containedMaterials() const
{
    std::vector<const Material*> result;
    if (const Material* p_material = material())
        result.push_back(p_material);
    for (const auto* child : nodeChildren()) {
        if (const auto* sample = dynamic_cast<const ISampleNode*>(child)) {
            for (const Material* p_material : sample->containedMaterials())
                result.push_back(p_material);
        }
    }
    return result;
}

bool ISampleNode::isMagnetic() const
{
    const auto materials = containedMaterials();
    return std::any_of(materials.cbegin(), materials.cend(),
                       [](const Material* mat) { return mat->isMagneticMaterial(); });
}
