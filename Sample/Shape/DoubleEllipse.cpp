//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Shape/DoubleEllipse.cpp
//! @brief     Implements class DoubleEllipse.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Shape/DoubleEllipse.h"
#include <algorithm>

// horizontal ellipses (XY plane)
DoubleEllipseZ::DoubleEllipseZ(double r0_x, double r0_y, double z, double rz_x, double rz_y)
{
    auto bottom_face = EllipseVerticesZ(r0_x, r0_y, 0.0);
    size_t n_bottom = bottom_face.size();
    auto top_face = EllipseVerticesZ(rz_x, rz_y, z);
    m_vertices.resize(n_bottom + top_face.size());
    std::move(bottom_face.begin(), bottom_face.end(), m_vertices.begin());
    std::move(top_face.begin(), top_face.end(), m_vertices.begin() + n_bottom);
}

DoubleEllipseZ::~DoubleEllipseZ() = default;

// vertical ellipses (YZ plane) with truncation from top and bottom
DoubleEllipseX::DoubleEllipseX(double x1, double r1_y, double r1_z, double z1_b, double z1_t,
                               double x2, double r2_y, double r2_z, double z2_b, double z2_t)
{
    auto face_1 = EllipseVerticesXtrunc(x1, r1_y, r1_z, z1_b, z1_t);
    size_t n_1 = face_1.size();
    auto face_2 = EllipseVerticesXtrunc(x2, r2_y, r2_z, z2_b, z2_t);
    m_vertices.resize(n_1 + face_2.size());
    std::move(face_1.begin(), face_1.end(), m_vertices.begin());
    std::move(face_2.begin(), face_2.end(), m_vertices.begin() + n_1);
}

DoubleEllipseX::~DoubleEllipseX() = default;
