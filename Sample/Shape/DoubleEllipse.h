//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Shape/DoubleEllipse.h
//! @brief     Defines class DoubleEllipse.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_SHAPE_DOUBLEELLIPSE_H
#define BORNAGAIN_SAMPLE_SHAPE_DOUBLEELLIPSE_H

#include "Sample/Shape/IShape3D.h"

class DoubleEllipseZ : public IShape3D {
public:
    DoubleEllipseZ(double r0_x, double r0_y, double z, double rz_x, double rz_y);
    ~DoubleEllipseZ() override;
};

class DoubleEllipseX : public IShape3D {
public:
    DoubleEllipseX(double x1, double r1_y, double r1_z, double z1_b, double z1_t, double x2,
                   double r2_y, double r2_z, double z2_b, double z2_t);
    ~DoubleEllipseX() override;
};
#endif // BORNAGAIN_SAMPLE_SHAPE_DOUBLEELLIPSE_H
