//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Shape/IShape3D.h
//! @brief     Defines interface IShape3D.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_SHAPE_ISHAPE3D_H
#define BORNAGAIN_SAMPLE_SHAPE_ISHAPE3D_H

#include <heinz/Vectors3D.h>
#include <vector>

//! Abstract base class for different shapes.
//!
//! In contrast to the form factors, these shapes only provide an interface
//! for returning a set of vertices.

class IShape3D {
public:
    IShape3D() = default;
    virtual ~IShape3D() = default;

    //! Retrieves a list of the vertices constituting this concrete shape
    virtual std::vector<R3> vertices() const { return m_vertices; }
    static const size_t N_Circle;

protected:
    //! List of vertices initialized during construction
    std::vector<R3> m_vertices;
};

//! Helper functions to construct lists of vertices
//!
//! Generate vertices of centered rectangle at height z
std::vector<R3> RectangleVertices(double length, double width, double z);

//! Generate vertices of centered horizontal ellipse with given semi-axes at height z
std::vector<R3> EllipseVerticesZ(double r_x, double r_y, double z);

//! Generate vertices of centered vertical ellipse with given semi-axes at position x and truncation
//! heights z_b (bottom) and z_t (top)
std::vector<R3> EllipseVerticesXtrunc(double x, double r_y, double r_z, double z_b, double z_t);

#endif // BORNAGAIN_SAMPLE_SHAPE_ISHAPE3D_H
