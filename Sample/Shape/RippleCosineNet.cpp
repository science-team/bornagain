//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Shape/RippleCosineNet.cpp
//! @brief     Implements class RippleCosineNet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Shape/RippleCosineNet.h"
#include "Base/Const/PhysicalConstants.h"
#include <cmath>

using PhysConsts::pi;

RippleCosineNet::RippleCosineNet(double length, double width, double height)
{
    size_t n_y = IShape3D::N_Circle + 1;
    double y_step = width / (IShape3D::N_Circle);
    m_vertices.resize(2 * n_y);
    for (size_t i = 0; i < n_y; ++i) {
        double y = i * y_step - width / 2.0;
        double z = height * (1.0 + std::cos((2 * pi) * y / width)) / 2.0;
        m_vertices[i] = R3(length / 2.0, y, z);
        m_vertices[n_y + i] = R3(-length / 2.0, y, z);
    }
}

RippleCosineNet::~RippleCosineNet() = default;
