//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Shape/RippleSawtoothNet.cpp
//! @brief     Implements class RippleSawtooth.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/Shape/RippleSawtoothNet.h"

RippleSawtoothNet::RippleSawtoothNet(double length, double width, double height, double asymmetry)
{
    double ymax = width / 2.0 - asymmetry;
    double ymin = -width / 2.0 - asymmetry;
    m_vertices.resize(6);
    m_vertices[0] = R3(length / 2.0, ymax, 0.0);
    m_vertices[1] = R3(length / 2.0, ymin, 0.0);
    m_vertices[2] = R3(length / 2.0, 0.0, height);
    m_vertices[3] = R3(-length / 2.0, ymax, 0.0);
    m_vertices[4] = R3(-length / 2.0, ymin, 0.0);
    m_vertices[5] = R3(-length / 2.0, 0.0, height);
}

RippleSawtoothNet::~RippleSawtoothNet() = default;
