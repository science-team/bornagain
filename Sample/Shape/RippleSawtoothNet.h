//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Shape/RippleSawtoothNet.h
//! @brief     Defines class RippleSawtooth.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_SHAPE_RIPPLESAWTOOTHNET_H
#define BORNAGAIN_SAMPLE_SHAPE_RIPPLESAWTOOTHNET_H

#include "Sample/Shape/IShape3D.h"

class RippleSawtoothNet : public IShape3D {
public:
    RippleSawtoothNet(double length, double width, double height, double asymmetry);
    ~RippleSawtoothNet() override;
};

#endif // BORNAGAIN_SAMPLE_SHAPE_RIPPLESAWTOOTHNET_H
