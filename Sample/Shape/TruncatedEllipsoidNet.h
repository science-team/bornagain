//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/Shape/TruncatedEllipsoidNet.h
//! @brief     Defines class TruncatedEllipsoidNet.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_SHAPE_TRUNCATEDELLIPSOIDNET_H
#define BORNAGAIN_SAMPLE_SHAPE_TRUNCATEDELLIPSOIDNET_H

#include "Sample/Shape/IShape3D.h"

class TruncatedEllipsoidNet : public IShape3D {
public:
    TruncatedEllipsoidNet(double r_x, double r_y, double r_z, double height, double dh);
    ~TruncatedEllipsoidNet() override;
};

#endif // BORNAGAIN_SAMPLE_SHAPE_TRUNCATEDELLIPSOIDNET_H
