//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/SoftParticle/FuzzySphere.cpp
//! @brief     Implements class FuzzySphere.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/SoftParticle/FuzzySphere.h"
#include "Base/Util/Assert.h"
#include "Sample/LibFF/SomeFormfactor.h"
#include "Sample/Shape/TruncatedEllipsoidNet.h"

FuzzySphere::FuzzySphere(const std::vector<double> P)
    : IFormfactor(P)
    , m_radius(m_P[0])
    , m_sigma(m_P[1])
{
    validateOrThrow();
}

FuzzySphere::FuzzySphere(double mean, double sigma)
    : FuzzySphere(std::vector<double>{mean, sigma})
{
}

complex_t FuzzySphere::formfactor(C3 q) const
{
    ASSERT(m_validated);
    double q2 = std::norm(q.x()) + std::norm(q.y()) + std::norm(q.z());
    double dw = std::exp(-q2 * m_sigma * m_sigma / 2.0);
    return dw * SampleUtil::someff::ffSphere(q, m_radius);
}

std::string FuzzySphere::validate() const
{
    std::vector<std::string> errs;
    requestGt0(errs, m_radius, "radius");
    requestGt0(errs, m_sigma, "sigma");
    if (!errs.empty())
        return jointError(errs);

    m_shape3D =
        std::make_unique<TruncatedEllipsoidNet>(m_radius, m_radius, m_radius, 2 * m_radius, 0);

    m_validated = true;
    return "";
}

bool FuzzySphere::contains(const R3&) const
{
    throw std::runtime_error("Soft particle cannot be used as mesocrystal outer shape");
}
