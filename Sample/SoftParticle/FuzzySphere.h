//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/SoftParticle/FuzzySphere.h
//! @brief     Defines and implements class FuzzySphere.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_SOFTPARTICLE_FUZZYSPHERE_H
#define BORNAGAIN_SAMPLE_SOFTPARTICLE_FUZZYSPHERE_H

#include "Sample/Particle/IFormfactor.h"
#include <memory>

//! A sphere with gaussian radius distribution.

class FuzzySphere : public IFormfactor {
public:
    FuzzySphere(std::vector<double> P);
    FuzzySphere(double mean, double sigma);

#ifndef SWIG
    FuzzySphere* clone() const override { return new FuzzySphere(m_radius, m_sigma); }
#endif // SWIG

    std::string className() const final { return "FuzzySphere"; }
    std::vector<ParaMeta> parDefs() const final { return {{"Radius", "nm"}, {"Fuzziness", "nm"}}; }

    double radialExtension() const override { return m_radius; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_radius;
    const double& m_sigma;
};

#endif // BORNAGAIN_SAMPLE_SOFTPARTICLE_FUZZYSPHERE_H
