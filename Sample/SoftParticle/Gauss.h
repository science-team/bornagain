//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/SoftParticle/Gauss.h
//! @brief     Defines class GaussSphere.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_SOFTPARTICLE_GAUSS_H
#define BORNAGAIN_SAMPLE_SOFTPARTICLE_GAUSS_H

#include "Sample/Particle/IFormfactor.h"

//! The form factor of a Gaussian sphere.

class GaussSphere : public IFormfactor {
public:
    GaussSphere(std::vector<double> P);
    GaussSphere(double mean_radius);

#ifndef SWIG
    GaussSphere* clone() const override { return new GaussSphere(m_mean_radius); }
#endif // SWIG

    std::string className() const final { return "GaussSphere"; }
    std::vector<ParaMeta> parDefs() const final { return {{"MeanRadius", "nm"}}; }

    double meanRadius() const { return m_mean_radius; }

    double radialExtension() const override { return m_mean_radius; }

    complex_t formfactor(C3 q) const override;

    std::string validate() const override;

    bool contains(const R3& position) const override;

private:
    const double& m_mean_radius;
    void initialize();
};

#endif // BORNAGAIN_SAMPLE_SOFTPARTICLE_GAUSS_H
