//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/BoxCompositionBuilder.cpp
//! @brief     Implements class BoxCompositionBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/BoxCompositionBuilder.h"
#include "Base/Const/Units.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Polyhedra.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Compound.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

using Units::deg;

namespace {

const Material particleMaterial = RefractiveMaterial("Ag", 1.245e-5, 5.419e-7);
const double layer_thickness = 100.0;
const double length = 50.0;
const double width = 20.0;
const double height = 10.0;

Sample* finalizeMultiLayer(const Compound& composition)
{
    ParticleLayout layout;
    layout.addParticle(composition);

    Layer vacuum_layer(refMat::Vacuum);
    Layer middle_layer(refMat::Teflon, layer_thickness);
    middle_layer.addLayout(layout);
    Layer substrate(refMat::Substrate2);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(middle_layer);
    sample->addLayer(substrate);
    return sample;
}

} // namespace

// --- BoxCompositionRotateXBuilder ---

Sample* ExemplarySamples::createBoxCompositionRotateX()
{
    Particle box(particleMaterial, Box(length / 2.0, width, height));
    Compound composition;
    composition.addComponent(box, R3());
    composition.addComponent(box, R3(length / 2.0, 0.0, 0.0));
    composition.rotate(RotationX(90.0 * deg));
    composition.translate(R3(0.0, 0.0, -layer_thickness / 2.0));
    return finalizeMultiLayer(composition);
}

// --- BoxCompositionRotateYBuilder ---

Sample* ExemplarySamples::createBoxCompositionRotateY()
{
    Particle box(particleMaterial, Box(length / 2.0, width, height));
    Compound composition;
    composition.addComponent(box, R3());
    composition.addComponent(box, R3(length / 2.0, 0.0, 0.0));
    composition.rotate(RotationY(90.0 * deg));
    composition.translate(R3(0.0, 0.0, -layer_thickness / 2.0 + length / 4.0));
    return finalizeMultiLayer(composition);
}

// --- BoxCompositionRotateZBuilder ---

Sample* ExemplarySamples::createBoxCompositionRotateZ()
{
    Particle box(particleMaterial, Box(length / 2.0, width, height));
    Compound composition;
    composition.addComponent(box, R3());
    composition.addComponent(box, R3(length / 2.0, 0.0, 0.0));
    composition.rotate(RotationZ(90.0 * deg));
    composition.translate(R3(0.0, 0.0, -layer_thickness / 2.0 - height / 2.0));
    return finalizeMultiLayer(composition);
}

// --- BoxCompositionRotateZandYBuilder ---

Sample* ExemplarySamples::createBoxCompositionRotateZandY()
{
    Particle box(particleMaterial, Box(length / 2.0, width, height));
    Compound composition;
    composition.addComponent(box, R3());
    composition.addComponent(box, R3(length / 2.0, 0.0, 0.0));
    composition.rotate(RotationZ(90.0 * deg));
    composition.rotate(RotationY(90.0 * deg));
    composition.translate(R3(0.0, 0.0, -layer_thickness / 2.0));
    return finalizeMultiLayer(composition);
}

// --- BoxStackCompositionBuilder ---

// Composition of two boxes which gives you the box (10,20,50) with reference point as usual.
Sample* ExemplarySamples::createBoxStackComposition()
{
    Compound composition;

    // box1 (20,50,5), rotatedZ
    const double box1_length = 20;
    const double box1_width = 50;
    const double box1_height = 5;
    Particle box1(particleMaterial, Box(box1_length, box1_width, box1_height));
    box1.rotate(RotationZ(90. * deg));

    // box2 (5,20,50), rotatedY
    const double box2_length = 5.0;
    const double box2_width = 20.0;
    const double box2_height = 50.0;
    Particle box2(particleMaterial, Box(box2_length, box2_width, box2_height));
    box2.rotate(RotationY(90. * deg));
    box2.translate(R3(-box2_height / 2.0, 0.0, box2_length / 2.0));

    composition.addComponent(box1, R3());
    composition.addComponent(box2, R3(0.0, 0.0, box1_height));
    composition.rotate(RotationY(90.0 * deg));
    composition.translate(R3(0.0, 0.0, -layer_thickness / 2.));

    return finalizeMultiLayer(composition);
}
