//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/BoxCompositionBuilder.h
//! @brief     Defines classes of BoxCompositionBuilder family.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_BOXCOMPOSITIONBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_BOXCOMPOSITIONBUILDER_H

class Sample;

//! Two boxes in particle composition rotated in X by 90 degrees.

namespace ExemplarySamples {

Sample* createBoxCompositionRotateX();

} // namespace ExemplarySamples

//! Two boxes in particle composition rotated in Y by 90 degrees.

namespace ExemplarySamples {

Sample* createBoxCompositionRotateY();

} // namespace ExemplarySamples

//! Two boxes in particle composition rotated in Z by 90 degrees.

namespace ExemplarySamples {

Sample* createBoxCompositionRotateZ();

} // namespace ExemplarySamples

//! Two boxes in particle composition rotated in Z and Y by 90 degrees.

namespace ExemplarySamples {

Sample* createBoxCompositionRotateZandY();

} // namespace ExemplarySamples

//! Two different boxes are first rotated and then composed, composition is then rotated.

namespace ExemplarySamples {

Sample* createBoxStackComposition();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_BOXCOMPOSITIONBUILDER_H
