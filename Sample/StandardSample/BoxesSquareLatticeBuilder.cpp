//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/BoxesSquareLatticeBuilder.cpp
//! @brief     Implements class BoxesSquareLattice2DBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/BoxesSquareLatticeBuilder.h"
#include "Sample/Aggregate/Interference2DLattice.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/Correlation/Profiles2D.h"
#include "Sample/HardParticle/Polyhedra.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

namespace {
Sample* createBoxesSquareLattice2D(double length, double height, double lattice)
{
    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Interference2DLattice iff(SquareLattice2D(lattice, 0));

    Profile2DCauchy pdf(100.0, 100.0, 0);
    iff.setDecayFunction(pdf);

    // particles
    ParticleLayout particle_layout;
    Box ff_box(length, length, height);
    Particle particle(refMat::Particle, ff_box);
    particle_layout.addParticle(particle, 1.0);

    particle_layout.setInterference(iff);

    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}
} // namespace

Sample* ExemplarySamples::createBoxesSquareLattice2D()
{
    return ::createBoxesSquareLattice2D(5, 10, 8);
}

Sample* ExemplarySamples::createBoxesLayer()
{
    return ::createBoxesSquareLattice2D(10, 10, 10);
}
