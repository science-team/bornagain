//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/BoxesSquareLatticeBuilder.h
//! @brief     Defines class BoxesSquareLattice2DBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_BOXESSQUARELATTICEBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_BOXESSQUARELATTICEBUILDER_H

class Sample;

//! Builds sample: square boxes in a square lattice

namespace ExemplarySamples {

Sample* createBoxesSquareLattice2D();

Sample* createBoxesLayer();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_BOXESSQUARELATTICEBUILDER_H
