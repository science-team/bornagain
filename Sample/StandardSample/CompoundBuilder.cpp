//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/CompoundBuilder.cpp
//! @brief     Implements class CompoundBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/CompoundBuilder.h"
#include "Sample/Aggregate/Interference2DLattice.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/Correlation/Profiles2D.h"
#include "Sample/HardParticle/Cylinder.h"
#include "Sample/HardParticle/Sphere.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Compound.h"
#include "Sample/Particle/Particle.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

// --- CompoundBuilder ---

Sample* ExemplarySamples::createCompound()
{
    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    double radius(10.0);
    Sphere sphere_ff(radius);
    Particle sphere(refMat::Particle, sphere_ff);
    ParticleLayout particle_layout;

    std::vector<R3> positions = {{},
                                 {radius, radius / std::sqrt(3.0), std::sqrt(8.0 / 3.0) * radius}};

    Compound basis;

    basis.addComponents(sphere, positions);
    particle_layout.addParticle(basis);

    Interference2DLattice iff(HexagonalLattice2D(radius * 2.0, 0));
    Profile2DCauchy pdf(10, 10, 0);
    iff.setDecayFunction(pdf);

    particle_layout.setInterference(iff);

    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

Sample* ExemplarySamples::createCompoundPlus()
{
    int n_lim = 1;
    double L = 10.0;
    double R = 4.0;

    Sphere sphere_ff(R);
    Particle sphere(refMat::Particle, sphere_ff);
    ParticleLayout particle_layout;

    double size = L * (2 * n_lim + 1 - 1) + 2 * R;
    Cylinder outer_shape_ff(size / 2, size);

    std::vector<R3> positions;
    for (int i = -n_lim; i <= n_lim; i++)
        for (int j = -n_lim; j <= n_lim; j++)
            for (int k = 0; k <= 2 * n_lim; k++) {
                R3 pos = {i * L, j * L, k * L};
                if (outer_shape_ff.contains(pos))
                    positions.push_back(pos);
            }

    Compound basis;
    basis.addComponents(sphere, positions);
    particle_layout.addParticle(basis);

    Layer vacuum_layer(refMat::Vacuum);
    vacuum_layer.addLayout(particle_layout);
    Layer substrate_layer(refMat::Substrate);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}
