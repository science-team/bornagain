//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/CompoundBuilder.h
//! @brief     Defines class CompoundBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_COMPOUNDBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_COMPOUNDBUILDER_H

class Sample;

//! Builds sample.

namespace ExemplarySamples {

//! Two layers of spheres at hex lattice.
Sample* createCompound();

//! Small cylinder "mesocrystal" (shape of "plus" sign).
Sample* createCompoundPlus();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_COMPOUNDBUILDER_H
