//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/CoreAndShellBuilder.cpp
//! @brief     Implements class CoreShellParticleBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/CoreAndShellBuilder.h"
#include "Base/Const/Units.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Polyhedra.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/CoreAndShell.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

using Units::deg;

// --- CoreShellParticleBuilder ---

Sample* ExemplarySamples::createCoreShellParticle()
{
    complex_t n_particle_core(1.0 - 6e-5, 2e-8);
    complex_t n_particle_shell(1.0 - 1e-4, 2e-8);

    Material core_material = RefractiveMaterial("Core", n_particle_core);
    Material shell_material = RefractiveMaterial("Shell", n_particle_shell);

    Layer vacuum_layer(refMat::Vacuum);

    Box ff_box1(16, 16, 8);
    Particle shell_particle(shell_material, ff_box1);

    Box ff_box2(12, 12, 7);
    Particle core_particle(core_material, ff_box2);

    CoreAndShell particle(core_particle, shell_particle);
    vacuum_layer.addLayout(particle);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    return sample;
}

// --- CoreShellBoxRotateZandYBuilder ---

Sample* ExemplarySamples::createCoreShellBoxRotateZandY()
{
    const double layer_thickness(100.0);

    // core shell particle
    const double shell_length(50.0);
    const double shell_width(20.0);
    const double shell_height(10.0);
    double core_length = shell_length / 2.0;
    double core_width = shell_width / 2.0;
    double core_height = shell_height / 2.0;

    Particle core(refMat::Ag, Box(core_length, core_width, core_height));
    core.translate(R3(0.0, 0.0, (shell_height - core_height) / 2.0));
    Particle shell(refMat::AgO2, Box(shell_length, shell_width, shell_height));
    CoreAndShell coreshell(core, shell);
    coreshell.rotate(RotationZ(90.0 * deg));
    coreshell.rotate(RotationY(90.0 * deg));
    coreshell.translate(R3(0.0, 0.0, -layer_thickness / 2.0));

    ParticleLayout layout;
    layout.addParticle(coreshell);

    Layer vacuum_layer(refMat::Vacuum);
    Layer middle_layer(refMat::Teflon, layer_thickness);
    middle_layer.addLayout(layout);
    Layer substrate(refMat::Substrate2);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(middle_layer);
    sample->addLayer(substrate);

    return sample;
}
