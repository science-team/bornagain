//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/CoreAndShellBuilder.h
//! @brief     Defines class CoreShellParticleBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_COREANDSHELLBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_COREANDSHELLBUILDER_H

class Sample;

//! Builds sample: Core Shell Nanoparticles (IsGISAXS example #11).

namespace ExemplarySamples {

Sample* createCoreShellParticle();

} // namespace ExemplarySamples

//! Rotation and translation of core shell box particle in 3 layers system.

namespace ExemplarySamples {

Sample* createCoreShellBoxRotateZandY();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_COREANDSHELLBUILDER_H
