//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/CustomMorphologyBuilder.h
//! @brief     Defines class CustomMorphologyBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_CUSTOMMORPHOLOGYBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_CUSTOMMORPHOLOGYBUILDER_H

class Sample;

//! Builds sample: mixture of different particles (IsGISAXS example #7).

namespace ExemplarySamples {

Sample* createCustomMorphology();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_CUSTOMMORPHOLOGYBUILDER_H
