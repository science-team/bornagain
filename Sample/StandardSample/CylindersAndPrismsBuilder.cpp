//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/CylindersAndPrismsBuilder.cpp
//! @brief     Implements class CylindersAndPrismsBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/CylindersAndPrismsBuilder.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Cylinder.h"
#include "Sample/HardParticle/Polyhedra.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

Sample* ExemplarySamples::createCylindersAndPrisms()
{
    auto* sample = new Sample;

    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    ParticleLayout particle_layout;

    Cylinder ff_cylinder(5.0, 5.0);
    Particle cylinder(refMat::Particle, ff_cylinder);

    Prism3 ff_prism3(10.0, 5.0);
    Particle prism3(refMat::Particle, ff_prism3);

    particle_layout.addParticle(cylinder, 0.5);
    particle_layout.addParticle(prism3, 0.5);

    vacuum_layer.addLayout(particle_layout);

    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}
