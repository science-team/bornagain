//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/CylindersAndPrismsBuilder.h
//! @brief     Defines class CylindersAndPrismsBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_CYLINDERSANDPRISMSBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_CYLINDERSANDPRISMSBUILDER_H

class Sample;

//! Builds sample: mixture of cylinders and prisms without interference (IsGISAXS example #1).

namespace ExemplarySamples {

Sample* createCylindersAndPrisms();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_CYLINDERSANDPRISMSBUILDER_H
