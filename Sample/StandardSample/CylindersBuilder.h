//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/CylindersBuilder.h
//! @brief     Defines classes of CylindersBuilder family.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_CYLINDERSBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_CYLINDERSBUILDER_H

class Sample;

namespace ExemplarySamples {

//! Builds sample: cylinder form factor in DWBA (IsGISAXS example #3, part I).
Sample* createCylindersInDWBA();

//! Builds sample: cylinder form factor in BA (IsGISAXS example #3, part II).
Sample* createCylindersInBA(double height = 5, double radius = 5);

//! Builds sample with large cylinders for MC integration tests.
Sample* createLargeCylindersInDWBA(double height = 1000, double radius = 500);

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_CYLINDERSBUILDER_H
