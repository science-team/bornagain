//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ExemplarySamples.h
//! @brief     includes all StandardSample headers.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_EXEMPLARYSAMPLES_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_EXEMPLARYSAMPLES_H

class Sample;

#include "Sample/StandardSample/BoxCompositionBuilder.h"
#include "Sample/StandardSample/BoxesSquareLatticeBuilder.h"
#include "Sample/StandardSample/CompoundBuilder.h"
#include "Sample/StandardSample/CoreAndShellBuilder.h"
#include "Sample/StandardSample/CustomMorphologyBuilder.h"
#include "Sample/StandardSample/CylindersAndPrismsBuilder.h"
#include "Sample/StandardSample/CylindersBuilder.h"
#include "Sample/StandardSample/FeNiBilayerBuilder.h"
#include "Sample/StandardSample/HomogeneousMultilayerBuilder.h"
#include "Sample/StandardSample/LatticeBuilder.h"
#include "Sample/StandardSample/LayersWithAbsorptionBuilder.h"
#include "Sample/StandardSample/LayersWithAbsorptionBySLDBuilder.h"
#include "Sample/StandardSample/MagneticLayersBuilder.h"
#include "Sample/StandardSample/MagneticParticlesBuilder.h"
#include "Sample/StandardSample/MesocrystalBuilder.h"
#include "Sample/StandardSample/MultiLayerWithRoughnessBuilder.h"
#include "Sample/StandardSample/MultipleLayoutBuilder.h"
#include "Sample/StandardSample/ParacrystalBuilder.h"
#include "Sample/StandardSample/ParticleDistributionsBuilder.h"
#include "Sample/StandardSample/ParticleInVacuumBuilder.h"
#include "Sample/StandardSample/PercusYevickBuilder.h"
#include "Sample/StandardSample/PlainMultiLayerBySLDBuilder.h"
#include "Sample/StandardSample/ResonatorBuilder.h"
#include "Sample/StandardSample/RipplesBuilder.h"
#include "Sample/StandardSample/RotatedPyramidsBuilder.h"
#include "Sample/StandardSample/SimpleLayerBuilder.h"
#include "Sample/StandardSample/SizeDistributionModelsBuilder.h"
#include "Sample/StandardSample/SlicedCompositionBuilder.h"
#include "Sample/StandardSample/SlicedCylindersBuilder.h"
#include "Sample/StandardSample/ThickAbsorptiveSampleBuilder.h"
#include "Sample/StandardSample/TransformationsBuilder.h"
#include "Sample/StandardSample/TwoDimLatticeBuilder.h"

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_EXEMPLARYSAMPLES_H
