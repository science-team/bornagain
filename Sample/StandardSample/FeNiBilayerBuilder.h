//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/FeNiBilayerBuilder.h
//! @brief     Defines various sample builder classes to.
//!            test polarized specular computations
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2020
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_FENIBILAYERBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_FENIBILAYERBUILDER_H

class Sample;

namespace ExemplarySamples {

Sample* createFeNiBilayer();

} // namespace ExemplarySamples

namespace ExemplarySamples {

Sample* createFeNiBilayerTanh();

} // namespace ExemplarySamples

namespace ExemplarySamples {

Sample* createFeNiBilayerNC();

} // namespace ExemplarySamples

namespace ExemplarySamples {

Sample* createFeNiBilayerSpinFlip();

} // namespace ExemplarySamples

namespace ExemplarySamples {

Sample* createFeNiBilayerSpinFlipTanh();

} // namespace ExemplarySamples

namespace ExemplarySamples {

Sample* createFeNiBilayerSpinFlipNC();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_FENIBILAYERBUILDER_H
