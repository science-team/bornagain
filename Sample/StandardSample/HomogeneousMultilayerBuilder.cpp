//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/HomogeneousMultilayerBuilder.cpp
//! @brief     Implements class HomogeneousMultilayerBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/HomogeneousMultilayerBuilder.h"
#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"

Sample* ExemplarySamples::createHomogeneousMultilayer()
{
    const size_t number_of_layers = 10;
    const double delta_ti = -7.36e-7;
    const double delta_ni = 3.557e-6;
    const double delta_si = 7.81e-7;
    const double thick_ti = 3.0; // nm
    const double thick_ni = 7.0; // nm

    Material vacuum_material = Vacuum();
    Material substrate_material = RefractiveMaterial("Si_substrate", delta_si, 0.0);
    Material ni_material = RefractiveMaterial("Ni", delta_ni, 0.0);
    Material ti_material = RefractiveMaterial("Ti", delta_ti, 0.0);

    Layer vacuum_layer(vacuum_material);
    Layer ni_layer(ni_material, thick_ni);
    Layer ti_layer(ti_material, thick_ti);
    Layer substrate_layer(substrate_material);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    for (size_t i = 0; i < number_of_layers; ++i) {
        sample->addLayer(ti_layer);
        sample->addLayer(ni_layer);
    }
    sample->addLayer(substrate_layer);
    return sample;
}
