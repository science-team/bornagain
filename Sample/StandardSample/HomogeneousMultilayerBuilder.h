//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/HomogeneousMultilayerBuilder.h
//! @brief     Defines class HomogeneousMultilayerBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_HOMOGENEOUSMULTILAYERBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_HOMOGENEOUSMULTILAYERBUILDER_H

class Sample;

//! Builds a sample with 10 interchanging homogeneous layers of Ti and Ni on silicone substrate.
//! Ti is 70 angstroms thick, Ni is 30 angstroms thick.
//! No absorption, no roughness, target wavelength is 1.54 angstroms.

namespace ExemplarySamples {

Sample* createHomogeneousMultilayer();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_HOMOGENEOUSMULTILAYERBUILDER_H
