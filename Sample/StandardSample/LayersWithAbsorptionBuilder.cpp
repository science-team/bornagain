//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/LayersWithAbsorptionBuilder.cpp
//! @brief     Implements class LayersWithAbsorptionBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/LayersWithAbsorptionBuilder.h"
#include "Base/Const/Units.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Sphere.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

using Units::deg;

Sample* ExemplarySamples::createLayersWithAbsorptionWithFF(const IFormfactor* ff)
{
    const double middle_layer_thickness(60.0);

    Particle particle(refMat::Ag, *ff);
    particle.rotate(RotationZ(10.0 * deg));
    particle.rotate(RotationY(10.0 * deg));
    particle.rotate(RotationX(10.0 * deg));
    particle.translate(R3(0.0, 0.0, -middle_layer_thickness / 2.0));

    ParticleLayout layout;
    layout.addParticle(particle);

    Layer vacuum_layer(refMat::Vacuum);
    Layer middle_layer(refMat::Teflon, middle_layer_thickness);
    Layer substrate(refMat::Substrate2);

    middle_layer.addLayout(layout);

    auto* result = new Sample;
    result->setName(ff->className());
    result->addLayer(vacuum_layer);
    result->addLayer(middle_layer);
    result->addLayer(substrate);
    return result;
}
