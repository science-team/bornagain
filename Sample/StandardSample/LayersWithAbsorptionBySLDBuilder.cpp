//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/LayersWithAbsorptionBySLDBuilder.cpp
//! @brief     Implements class LayersWithAbsorptionBySLDBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/LayersWithAbsorptionBySLDBuilder.h"
#include "Base/Const/Units.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Sphere.h"
#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"

using Units::deg;

const double middle_layer_thickness(60.0);

Sample* ExemplarySamples::createLayersWithAbsorptionBySLD()
{
    Material ambience_mat = MaterialBySLD("Vacuum", 0.0, 0.0);
    Material middle_mat = MaterialBySLD("Teflon", 4.7573e-6, 1.6724e-12);
    Material substrate_mat = MaterialBySLD("Substrate", 2.0728e-06, 2.3747e-11);
    Material particle_mat = MaterialBySLD("Ag", 3.4682e-06, 1.0309e-08);

    Sphere ff(5.0);

    Particle particle(particle_mat, ff);
    particle.rotate(RotationZ(10.0 * deg));
    particle.rotate(RotationY(10.0 * deg));
    particle.rotate(RotationX(10.0 * deg));
    particle.translate(R3(0.0, 0.0, -middle_layer_thickness / 2.0));

    ParticleLayout layout;
    layout.addParticle(particle);

    Layer vacuum_layer(ambience_mat);
    Layer middle_layer(middle_mat, middle_layer_thickness);
    Layer substrate(substrate_mat);

    middle_layer.addLayout(layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(middle_layer);
    sample->addLayer(substrate);
    return sample;
}
