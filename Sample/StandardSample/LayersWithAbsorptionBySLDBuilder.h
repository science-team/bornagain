//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/LayersWithAbsorptionBySLDBuilder.h
//! @brief     Defines class LayersWithAbsorptionBySLDBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_LAYERSWITHABSORPTIONBYSLDBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_LAYERSWITHABSORPTIONBYSLDBUILDER_H

class Sample;

//! The LayersWithAbsorptionBySLDBuilder class generates a sample with 3 layers with
//! absorption (refractive index has imaginary part). //! The middle layer is populated with
//! particles. MaterialBySLD is used to generate maaterials

namespace ExemplarySamples {

Sample* createLayersWithAbsorptionBySLD();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_LAYERSWITHABSORPTIONBYSLDBUILDER_H
