//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/MagneticLayersBuilder.h
//! @brief    Defines class to build samples with magnetic layers.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_MAGNETICLAYERSBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_MAGNETICLAYERSBUILDER_H

class Sample;

#include "Sample/Interface/TransientModels.h"
#include <string>

//! Builds sample: spheres in substrate layer with a zero magnetic field.

namespace ExemplarySamples {

Sample* createMagneticSubstrateZeroField();

} // namespace ExemplarySamples

//! Builds sample: ambient and one magnetized layer on a non-magnetized substrate.

namespace ExemplarySamples {

Sample* createSimpleMagneticLayer();

} // namespace ExemplarySamples

//! Builds sample: magnetic spheres in a magnetized layer on a non-magnetized substrate.

namespace ExemplarySamples {

Sample* createMagneticLayer();

} // namespace ExemplarySamples

//! Builds sample: magnetic layer on a magnetic substrate with the fields rotated by 90°

namespace ExemplarySamples {

Sample* createSimpleMagneticRotationWithRoughness(const std::string& roughnessKey);

} // namespace ExemplarySamples

//! Builds sample: rotated magnetic spheres in substrate layer with a unit magnetic field.

namespace ExemplarySamples {

Sample* createMagneticRotation();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_MAGNETICLAYERSBUILDER_H
