//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/MagneticParticlesBuilder.cpp
//! @brief     Implements classes to build magnetic samples.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/MagneticParticlesBuilder.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Cylinder.h"
#include "Sample/HardParticle/Sphere.h"
#include "Sample/Interface/Roughness.h"
#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"

// ----------------------------------------------------------------------------
// Magnetic cylinders and zero magnetic field
// ----------------------------------------------------------------------------

Sample* ExemplarySamples::createMagneticParticleZeroField()
{
    const double m_cylinder_radius(5);
    const double m_cylinder_height(5);

    Material vacuum_material = RefractiveMaterial("Vacuum", 0.0, 0.0);
    Material substrate_material = RefractiveMaterial("Substrate", 6e-6, 2e-8);
    R3 magnetic_field(0.0, 0.0, 0.0);
    Material particle_material = RefractiveMaterial("MagParticle", 6e-4, 2e-8, magnetic_field);

    Layer vacuum_layer(vacuum_material);
    Layer substrate_layer(substrate_material);

    Cylinder ff_cylinder(m_cylinder_radius, m_cylinder_height);

    Particle particle(particle_material, ff_cylinder);
    ParticleLayout particle_layout(particle);

    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

// ----------------------------------------------------------------------------
// Magnetic cylinders and non-zero magnetization
// ----------------------------------------------------------------------------

Sample* ExemplarySamples::createMagneticCylinders()
{
    const double m_cylinder_radius(5);
    const double m_cylinder_height(5);

    Material vacuum_material = RefractiveMaterial("Vacuum", 0.0, 0.0);
    Material substrate_material = RefractiveMaterial("Substrate", 15e-6, 0.0);
    R3 magnetization(0.0, 1e6, 0.0);
    Material particle_material = RefractiveMaterial("MagParticle2", 5e-6, 0.0, magnetization);

    Layer vacuum_layer(vacuum_material);
    Layer substrate_layer(substrate_material);

    Cylinder ff_cylinder(m_cylinder_radius, m_cylinder_height);

    Particle particle(particle_material, ff_cylinder);
    ParticleLayout particle_layout(particle);

    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

// ----------------------------------------------------------------------------
// Magnetic spheres inside substrate
// ----------------------------------------------------------------------------

Sample* ExemplarySamples::createMagneticSpheres()
{
    const double m_sphere_radius(5);

    R3 magnetization(0.0, 0.0, 1e7);
    Material particle_material = RefractiveMaterial("Particle", 2e-5, 4e-7, magnetization);
    Material vacuum_material = RefractiveMaterial("Vacuum", 0.0, 0.0);
    Material substrate_material = RefractiveMaterial("Substrate", 7e-6, 1.8e-7);

    Sphere ff_sphere(m_sphere_radius);
    Particle particle(particle_material, ff_sphere);
    R3 position(0.0, 0.0, -2.0 * m_sphere_radius);
    particle.translate(position);

    ParticleLayout particle_layout;
    particle_layout.addParticle(particle);

    Layer vacuum_layer(vacuum_material);
    Layer substrate_layer(substrate_material);
    substrate_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}
