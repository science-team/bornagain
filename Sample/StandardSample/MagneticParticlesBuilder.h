//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/MagneticParticlesBuilder.h
//! @brief    Defines class to build magnetic samples.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_MAGNETICPARTICLESBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_MAGNETICPARTICLESBUILDER_H

class Sample;

//! Builds sample: cylinders with magnetic material and zero magnetic field.

namespace ExemplarySamples {

Sample* createMagneticParticleZeroField();

} // namespace ExemplarySamples

//! Builds sample: cylinders with magnetic material and non-zero magnetic field.

namespace ExemplarySamples {

Sample* createMagneticCylinders();

} // namespace ExemplarySamples

//! Builds sample: spheres with magnetization inside substrate.

namespace ExemplarySamples {

Sample* createMagneticSpheres();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_MAGNETICPARTICLESBUILDER_H
