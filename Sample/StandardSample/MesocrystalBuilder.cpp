//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/MesocrystalBuilder.cpp
//! @brief     Implements class MesocrystalBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/MesocrystalBuilder.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Cylinder.h"
#include "Sample/HardParticle/Polyhedra.h"
#include "Sample/HardParticle/Sphere.h"
#include "Sample/Lattice/Lattice3D.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Crystal.h"
#include "Sample/Particle/Mesocrystal.h"
#include "Sample/Particle/Particle.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

Sample* ExemplarySamples::createMesocrystal()
{
    // mesocrystal lattice
    R3 lattice_basis_a(5.0, 0.0, 0.0);
    R3 lattice_basis_b(0.0, 5.0, 0.0);
    R3 lattice_basis_c(0.0, 0.0, 5.0);
    Lattice3D lattice(lattice_basis_a, lattice_basis_b, lattice_basis_c);

    // spherical particle that forms the base of the mesocrystal
    Sphere sphere_ff(2.0);
    Particle sphere(refMat::Particle, sphere_ff);

    // crystal structure
    Crystal crystal(sphere, lattice);

    // mesocrystal
    Cylinder meso_ff(20.0, 50.0);
    Mesocrystal meso(crystal, meso_ff);

    ParticleLayout particle_layout;
    particle_layout.addParticle(meso);
    particle_layout.setTotalParticleSurfaceDensity(0.001);

    Layer vacuum_layer(refMat::Vacuum);
    vacuum_layer.addLayout(particle_layout);
    Layer substrate_layer(refMat::Substrate);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

Sample* ExemplarySamples::createMesocrystalPlus()
{
    int n = 3;
    double L = 10.0;
    double R = 4.0;

    // mesocrystal lattice
    R3 lattice_basis_a(L, 0.0, 0.0);
    R3 lattice_basis_b(0.0, L, 0.0);
    R3 lattice_basis_c(0.0, 0.0, L);
    Lattice3D lattice(lattice_basis_a, lattice_basis_b, lattice_basis_c);

    // spherical particle that forms the base of the mesocrystal
    Sphere sphere_ff(R);
    Particle sphere(refMat::Particle, sphere_ff);

    // crystal structure
    Crystal crystal(sphere, lattice);

    // mesocrystal
    double size = L * (n - 1) + 2 * R;
    Cylinder meso_ff(size / 2, size);
    Mesocrystal meso(crystal, meso_ff);

    ParticleLayout particle_layout;
    particle_layout.addParticle(meso);

    Layer vacuum_layer(refMat::Vacuum);
    vacuum_layer.addLayout(particle_layout);
    Layer substrate_layer(refMat::Substrate);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}
