//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/MesocrystalBuilder.h
//! @brief     Defines class MesocrystalBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_MESOCRYSTALBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_MESOCRYSTALBUILDER_H

class Sample;

//! Builds sample.

namespace ExemplarySamples {

//! Cylindrical mesocrystal composed of spheres in a cubic lattice.
Sample* createMesocrystal();

//! Small cylinder mesocrystal (shape of "plus" sign).
Sample* createMesocrystalPlus();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_MESOCRYSTALBUILDER_H
