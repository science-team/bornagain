//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/MultiLayerWithRoughnessBuilder.cpp
//! @brief     Implement class MultiLayerWithRoughnessBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/MultiLayerWithRoughnessBuilder.h"
#include "Sample/Interface/Roughness.h"
#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/LayerStack.h"
#include "Sample/Multilayer/Sample.h"

namespace {
Sample* createMultiLayerWithTransientModel(const TransientModel* transient)
{
    const double thicknessA(2.5);
    const double thicknessB(5.0);
    const double sigma(1.0);
    const double hurst(0.3);
    const double lateralCorrLength(5.0);
    const double crossCorrDepth(10.0);

    Material vacuum_material = RefractiveMaterial("Vacuum", 0., 0.);
    Material substrate_material = RefractiveMaterial("Substrate", 15e-6, 0.0);
    Material part_a_material = RefractiveMaterial("PartA", 5e-6, 0.0);
    Material part_b_material = RefractiveMaterial("PartB", 10e-6, 0.0);

    SelfAffineFractalModel autocorrelation(sigma, hurst, lateralCorrLength);
    CommonDepthCrosscorrelation crosscorrelation(crossCorrDepth);
    Roughness roughness(&autocorrelation, transient, &crosscorrelation);

    Layer vacuum_layer(vacuum_material);
    Layer partA_layer(part_a_material, thicknessA, &roughness);
    Layer partB_layer(part_b_material, thicknessB, &roughness);
    Layer substrate_layer(substrate_material, &roughness);

    LayerStack stack(5);
    stack.addLayer(partA_layer);
    stack.addLayer(partB_layer);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addStack(stack);
    sample->addLayer(substrate_layer);
    return sample;
}
} // namespace

Sample* ExemplarySamples::createMultiLayerWithRoughness()
{
    TanhTransient transient;
    return createMultiLayerWithTransientModel(&transient);
}

Sample* ExemplarySamples::createMultiLayerWithNCRoughness()
{
    ErfTransient transient;
    return createMultiLayerWithTransientModel(&transient);
}
