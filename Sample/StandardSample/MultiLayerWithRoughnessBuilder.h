//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/MultiLayerWithRoughnessBuilder.h
//! @brief     Defines class MultiLayerWithRoughnessBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_MULTILAYERWITHROUGHNESSBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_MULTILAYERWITHROUGHNESSBUILDER_H

class Sample;

//! Builds sample: layers with correlated roughness.

namespace ExemplarySamples {

Sample* createMultiLayerWithRoughness();

Sample* createMultiLayerWithNCRoughness();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_MULTILAYERWITHROUGHNESSBUILDER_H
