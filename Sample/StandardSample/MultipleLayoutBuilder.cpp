//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/MultipleLayoutBuilder.cpp
//! @brief     Implements class MultipleLayoutBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/MultipleLayoutBuilder.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Cylinder.h"
#include "Sample/HardParticle/Polyhedra.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

Sample* ExemplarySamples::createMultipleLayout()
{
    const double cylinder_height(5);
    const double cylinder_radius(5);
    const double prisheight(5);
    const double prislength(10);
    const double cylinder_weight(0.5);

    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    ParticleLayout particle_layout_1;
    ParticleLayout particle_layout_2;

    Cylinder ff_cylinder(cylinder_radius, cylinder_height);
    Particle cylinder(refMat::Particle, ff_cylinder);

    Prism3 ff_prism3(prislength, prisheight);
    Particle prism3(refMat::Particle, ff_prism3);

    particle_layout_1.addParticle(cylinder, cylinder_weight);
    particle_layout_2.addParticle(prism3, 1.0 - cylinder_weight);

    vacuum_layer.addLayout(particle_layout_1);
    vacuum_layer.addLayout(particle_layout_2);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}
