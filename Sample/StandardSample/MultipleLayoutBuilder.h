//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/MultipleLayoutBuilder.h
//! @brief     Defines class MultipleLayoutBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_MULTIPLELAYOUTBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_MULTIPLELAYOUTBUILDER_H

class Sample;

//! Builds sample: mixture of cylinders and prisms without interference,
//! using multiple particle layouts

namespace ExemplarySamples {

Sample* createMultipleLayout();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_MULTIPLELAYOUTBUILDER_H
