//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ParacrystalBuilder.cpp
//! @brief     Implements class ParacrystalBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/ParacrystalBuilder.h"
#include "Base/Const/Units.h"
#include "Base/Util/Assert.h"
#include "Sample/Aggregate/Interference2DParacrystal.h"
#include "Sample/Aggregate/InterferenceRadialParacrystal.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Cylinder.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

using Units::deg;

Sample* ExemplarySamples::createRadialParacrystal()
{
    const double m_corr_peak_distance(20.0);
    const double m_corr_width(7);
    const double m_corr_length(1e3);
    const double m_cylinder_height(5);
    const double m_cylinder_radius(5);

    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    InterferenceRadialParacrystal iff(m_corr_peak_distance, m_corr_length);
    Profile1DGauss pdf(m_corr_width);
    iff.setProbabilityDistribution(pdf);
    Cylinder ff_cylinder(m_cylinder_radius, m_cylinder_height);

    Particle particle(refMat::Particle, ff_cylinder);
    ParticleLayout particle_layout(particle);
    particle_layout.setInterference(iff);

    vacuum_layer.addLayout(particle_layout);

    auto* result = new Sample;
    result->addLayer(vacuum_layer);
    result->addLayer(substrate_layer);
    return result;
}

// -----------------------------------------------------------------------------
// Basic2DParacrystalBuilder
// -----------------------------------------------------------------------------

Sample* ExemplarySamples::createBasic2DParacrystalWithFTDis(const IProfile2D* pdf2)
{
    const Profile2DCauchy pdf1(0.1, 0.2, 0);

    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Interference2DParacrystal iff(BasicLattice2D(10.0, 20.0, 30.0 * deg, 45.0 * deg), 1000.0,
                                  20.0 * Units::micrometer, 40.0 * Units::micrometer);

    iff.setProbabilityDistributions(pdf1, *pdf2);

    const Cylinder ff_cylinder(5.0, 5.0);

    Particle particle(refMat::Particle, ff_cylinder);
    ParticleLayout particle_layout(particle);
    particle_layout.setInterference(iff);

    vacuum_layer.addLayout(particle_layout);

    auto* result = new Sample;
    result->setName("Basic2DParacrystal_" + pdf2->className());
    result->addLayer(vacuum_layer);
    result->addLayer(substrate_layer);
    return result;
}

// -----------------------------------------------------------------------------
// HexParacrystalBuilder
// -----------------------------------------------------------------------------

Sample* ExemplarySamples::createHexParacrystal()
{
    const double m_peak_distance(20.0);
    const double m_corr_length(0.0);
    const double m_domain_size_1(20.0 * Units::micrometer);
    const double m_domain_size_2(20.0 * Units::micrometer);
    const double m_cylinder_height(5);
    const double m_cylinder_radius(5);

    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Interference2DParacrystal iff(HexagonalLattice2D(m_peak_distance, 0.0), m_corr_length,
                                  m_domain_size_1, m_domain_size_2);
    iff.setIntegrationOverXi(true);
    Profile2DCauchy pdf(1.0, 1.0, 0);
    iff.setProbabilityDistributions(pdf, pdf);

    Cylinder ff_cylinder(m_cylinder_radius, m_cylinder_height);
    Particle cylinder(refMat::Particle, ff_cylinder);

    ParticleLayout particle_layout(cylinder);
    particle_layout.setInterference(iff);

    vacuum_layer.addLayout(particle_layout);

    auto* result = new Sample;
    result->addLayer(vacuum_layer);
    result->addLayer(substrate_layer);
    return result;
}

// -----------------------------------------------------------------------------
// RectParacrystalBuilder
// -----------------------------------------------------------------------------

Sample* ExemplarySamples::createRectParacrystal()
{
    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Interference2DParacrystal iff(SquareLattice2D(10), 0, 0, 0);
    iff.setIntegrationOverXi(true);
    iff.setDomainSizes(20.0 * Units::micrometer, 20.0 * Units::micrometer);
    Profile2DCauchy pdf1(0.5, 2.0, 0);
    Profile2DCauchy pdf2(0.5, 2.0, 0);
    iff.setProbabilityDistributions(pdf1, pdf2);

    Cylinder ff_cylinder(5.0, 5.0);

    Particle particle(refMat::Particle, ff_cylinder);
    ParticleLayout particle_layout(particle);
    particle_layout.setInterference(iff);

    vacuum_layer.addLayout(particle_layout);

    auto* result = new Sample;
    result->addLayer(vacuum_layer);
    result->addLayer(substrate_layer);
    return result;
}
