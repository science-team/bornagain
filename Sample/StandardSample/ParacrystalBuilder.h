//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ParacrystalBuilder.h
//! @brief     Defines classes of ParacrystalBuilder family.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_PARACRYSTALBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_PARACRYSTALBUILDER_H

class IProfile2D;
class Profile2DComponents;
class Sample;

//! Builds sample: cylinders with 1DDL structure factor (IsGISAXS example #4).

namespace ExemplarySamples {

Sample* createRadialParacrystal();

} // namespace ExemplarySamples

//! Builds sample: basic two-dimensional paracrystal with various probability
//! distribution functions (PDF's). They are initialized via component service.

namespace ExemplarySamples {

Sample* createBasic2DParacrystalWithFTDis(const IProfile2D* pdf2);

} // namespace ExemplarySamples

//! Builds sample: cylinders with 2DDL structure factor (IsGISAXS example #4).

namespace ExemplarySamples {

Sample* createHexParacrystal();

} // namespace ExemplarySamples

//! Builds sample: 2D paracrystal lattice (IsGISAXS example #8).

namespace ExemplarySamples {

Sample* createRectParacrystal();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_PARACRYSTALBUILDER_H
