//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ParticleDistributionsBuilder.h
//! @brief     Defines classes of with different types of particle distributions.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_PARTICLEDISTRIBUTIONSBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_PARTICLEDISTRIBUTIONSBUILDER_H

class Sample;

//! Cylinders in BA with size distributions (IsGISAXS example #3, part II).

namespace ExemplarySamples {

Sample* createCylindersWithSizeDistribution();

} // namespace ExemplarySamples

//! Builds mixture of cylinder particles with different size distribution (IsGISAXS example #2)

namespace ExemplarySamples {

Sample* createTwoTypesCylindersDistribution();

} // namespace ExemplarySamples

//! Rotated Pyramids with the distribution applied to the rotation angle.

namespace ExemplarySamples {

Sample* createRotatedPyramidsDistribution();

} // namespace ExemplarySamples

//! Spherical particles with the distribution applied to the radius and RealLimits defined.

namespace ExemplarySamples {

Sample* createSpheresWithLimitsDistribution();

} // namespace ExemplarySamples

//! Cones with the distribution applied to the angle and RealLimits defined.

namespace ExemplarySamples {

Sample* createConesWithLimitsDistribution();

} // namespace ExemplarySamples

//! Distribution of boxes with main parameter and two linked parameters.

namespace ExemplarySamples {

Sample* createLinkedBoxDistribution();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_PARTICLEDISTRIBUTIONSBUILDER_H
