//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ParticleInVacuumBuilder.cpp
//! @brief     Implements class ParticleInVacuumBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/ParticleInVacuumBuilder.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Sphere.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/StandardSample/ReferenceMaterials.h"
#include <iostream>

Sample* ExemplarySamples::createParticleInVacuumWithFF(const IFormfactor* ff)
{
    Layer vacuum_layer(refMat::Vacuum);

    Particle particle(refMat::Particle, *ff);
    ParticleLayout particle_layout(particle);
    vacuum_layer.addLayout(particle_layout);

    auto* result = new Sample;
    result->setName("ParticleInVacuum_" + ff->className());
    result->addLayer(vacuum_layer);
    return result;
}
