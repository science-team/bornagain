//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ParticleInVacuumBuilder.h
//! @brief     Defines class ParticleInVacuumBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_PARTICLEINVACUUMBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_PARTICLEINVACUUMBUILDER_H

class IFormfactor;
class Sample;

//! The ParticleInVacuumBuilder class generates a sample with single vacuum layer
//! populated with particles of certain types.

namespace ExemplarySamples {

Sample* createParticleInVacuumWithFF(const IFormfactor*);

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_PARTICLEINVACUUMBUILDER_H
