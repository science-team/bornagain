//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/PercusYevickBuilder.cpp
//! @brief     Implements class PercusYevickBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/PercusYevickBuilder.h"
#include "Sample/Aggregate/InterferenceHardDisk.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Cylinder.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

Sample* ExemplarySamples::createHardDisk()
{
    const double m_cylinder_height(5);
    const double m_cylinder_radius(5);
    const double m_disk_radius(5);
    const double m_density(0.006);

    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Cylinder ff_cylinder(m_cylinder_radius, m_cylinder_height);
    Particle particle(refMat::Particle, ff_cylinder);
    ParticleLayout particle_layout(particle);

    InterferenceHardDisk interparticle(m_disk_radius, m_density);
    particle_layout.setInterference(interparticle);

    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}
