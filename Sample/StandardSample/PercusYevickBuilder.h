//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/PercusYevickBuilder.h
//! @brief     Defines classes of PercusYevickBuilder family.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_PERCUSYEVICKBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_PERCUSYEVICKBUILDER_H

class Sample;

//! Builds sample: cylinders with hard disk Percus-Yevick interference.

namespace ExemplarySamples {

Sample* createHardDisk();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_PERCUSYEVICKBUILDER_H
