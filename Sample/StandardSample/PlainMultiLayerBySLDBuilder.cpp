//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/PlainMultiLayerBySLDBuilder.cpp
//! @brief     Implements class PlainMultiLayerBySLDBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/PlainMultiLayerBySLDBuilder.h"
#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"

namespace {

struct MaterialData {
    double sld_real; //!< real part of sld in AA^{-2}
    double sld_imag; //!< imaginary part of sld in AA^{-2}
};

const MaterialData m_si{2.0704e-06, 2.3726e-11};
const MaterialData m_ti{-1.9493e-06, 9.6013e-10};
const MaterialData m_ni{9.4245e-06, 1.1423e-09};

} // namespace

Sample* ExemplarySamples::createPlainMultiLayerBySLD(int n_layers, double thick_ti)
{
    double thick_ni(7.0);

    Material vacuum_material = MaterialBySLD();
    Material substrate_material = MaterialBySLD("Si_substrate", m_si.sld_real, m_si.sld_imag);
    Material ni_material = MaterialBySLD("Ni", m_ni.sld_real, m_ni.sld_imag);
    Material ti_material = MaterialBySLD("Ti", m_ti.sld_real, m_ti.sld_imag);

    Layer vacuum_layer(vacuum_material);
    Layer ni_layer(ni_material, thick_ni);
    Layer ti_layer(ti_material, thick_ti);
    Layer substrate_layer(substrate_material);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    for (int i = 0; i < n_layers; ++i) {
        sample->addLayer(ti_layer);
        sample->addLayer(ni_layer);
    }
    sample->addLayer(substrate_layer);
    return sample;
}
