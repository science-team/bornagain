//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/PlainMultiLayerBySLDBuilder.h
//! @brief     Declares class the PlainMultiLayerBySLDBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_PLAINMULTILAYERBYSLDBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_PLAINMULTILAYERBYSLDBUILDER_H

class Sample;

//! Builds a sample with 10 interchanging homogeneous layers of Ti and Ni on silicone substrate.
//! Ti is 70 angstroms thick, Ni is 30 angstroms thick.

namespace ExemplarySamples {

Sample* createPlainMultiLayerBySLD(int n_layers = 10, double thick_ti = 3.0);

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_PLAINMULTILAYERBYSLDBUILDER_H
