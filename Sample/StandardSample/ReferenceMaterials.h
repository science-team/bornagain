//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ReferenceMaterials.h
//! @brief     Defines materials in namespace refMat.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_REFERENCEMATERIALS_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_REFERENCEMATERIALS_H

class Sample;

#include "Sample/Material/MaterialFactoryFuncs.h"

//! Reference materials for use in tests and exemplary samples.

namespace refMat {

const Material Vacuum = RefractiveMaterial("Vacuum", 0.0, 0.0);
const Material Substrate = RefractiveMaterial("Substrate", 6e-6, 2e-8);
const Material Particle = RefractiveMaterial("Particle", 6e-4, 2e-8);

const Material Ag = RefractiveMaterial("Ag", 1.245e-5, 5.419e-7);
const Material AgO2 = RefractiveMaterial("AgO2", 8.600e-6, 3.442e-7);
const Material Teflon = RefractiveMaterial("Teflon", 2.900e-6, 6.019e-9);

const Material Substrate2 = RefractiveMaterial("Substrate2", 3.212e-6, 3.244e-8);

} // namespace refMat

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_REFERENCEMATERIALS_H
