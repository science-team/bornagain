//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ResonatorBuilder.cpp
//! @brief     Implements class ResonatorBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/ResonatorBuilder.h"
#include "Sample/Interface/Roughness.h"
#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include <memory>

Sample* ExemplarySamples::createResonator(double ti_thickness)
{
    auto* result = new Sample;

    Material m_Si = RefractiveMaterial("Si", 8.25218379931e-06, 0.0);
    Material m_Ti = RefractiveMaterial("Ti", -7.6593316363e-06, 3.81961616312e-09);
    Material m_TiO2 = RefractiveMaterial("TiO2", 1.04803530026e-05, 2.03233519385e-09);
    Material m_Pt = RefractiveMaterial("Pt", 2.52936993309e-05, 7.54553992473e-09);
    Material m_D2O = RefractiveMaterial("D2O", 2.52897204573e-05, 4.5224432814e-13);

    SelfAffineFractalModel autocorrelation(2.0, 0.8, 1e4);
    TanhTransient transient;
    CommonDepthCrosscorrelation crosscorrelation(400);
    Roughness roughness(&autocorrelation, &transient, &crosscorrelation);

    Layer l_TiO2(m_TiO2, 3.0, &roughness);
    Layer l_Ti_top(m_Ti, 10.0, &roughness);
    Layer l_Ti(m_Ti, ti_thickness, &roughness);
    Layer l_Si(m_Si);
    Layer l_Pt(m_Pt, 32.0, &roughness);
    Layer l_D2O(m_D2O, &roughness);

    result->addLayer(l_Si);

    const int nlayers = 3;
    for (size_t i = 0; i < nlayers; ++i) {
        result->addLayer(l_Ti);
        result->addLayer(l_Pt);
    }

    result->addLayer(l_Ti_top);
    result->addLayer(l_TiO2);
    result->addLayer(l_D2O);

    return result;
}
