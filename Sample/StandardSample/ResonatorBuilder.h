//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ResonatorBuilder.h
//! @brief     Defines class ResonatorBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_RESONATORBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_RESONATORBUILDER_H

class Sample;

//! Builds sample: sample with Ti/Pt layers sequence.

namespace ExemplarySamples {

Sample* createResonator(double ti_thickness = 13.0);

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_RESONATORBUILDER_H
