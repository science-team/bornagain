//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/RipplesBuilder.cpp
//! @brief     Implement classes to build various ripples.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/RipplesBuilder.h"
#include "Sample/Aggregate/InterferenceRadialParacrystal.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/CosineRipple.h"
#include "Sample/HardParticle/SawtoothRipple.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

Sample* ExemplarySamples::createCosineRipple()
{
    Layer vacuum_layer(refMat::Vacuum);
    CosineRippleBox ff_ripple1(100.0, 20.0, 4.0);
    Particle ripple(refMat::Particle, ff_ripple1);

    ParticleLayout particle_layout;
    particle_layout.addParticle(ripple, 1.0);
    InterferenceRadialParacrystal interparticle(20.0, 1e7);
    Profile1DGauss pdf(4.0);
    interparticle.setProbabilityDistribution(pdf);
    particle_layout.setInterference(interparticle);

    vacuum_layer.addLayout(particle_layout);

    Layer substrate_layer(refMat::Substrate);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

Sample* ExemplarySamples::createTriangularRipple(double d)
{
    Layer vacuum_layer(refMat::Vacuum);
    SawtoothRippleBox ff_ripple2(100.0, 20.0, 4.0, d);
    Particle ripple(refMat::Particle, ff_ripple2);

    ParticleLayout particle_layout;
    particle_layout.addParticle(ripple, 1.0);
    InterferenceRadialParacrystal interparticle(20.0, 1e7);
    Profile1DGauss pdf(4.0);
    interparticle.setProbabilityDistribution(pdf);
    particle_layout.setInterference(interparticle);

    vacuum_layer.addLayout(particle_layout);
    Layer substrate_layer(refMat::Substrate);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

Sample* ExemplarySamples::createAsymRipple()
{
    return ExemplarySamples::createTriangularRipple(-3);
}
