//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/RipplesBuilder.h
//! @brief     Defines classes to build various ripples.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_RIPPLESBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_RIPPLESBUILDER_H

class Sample;

//! Builds sample: cosine ripple within the 1D-paracrystal model.

namespace ExemplarySamples {

Sample* createCosineRipple();

} // namespace ExemplarySamples

//! Builds sample: triangular ripple within the 1D-paracrystal model (from PRB 85, 235415, 2012).

namespace ExemplarySamples {

Sample* createTriangularRipple(double d = 0);

} // namespace ExemplarySamples

namespace ExemplarySamples {

Sample* createAsymRipple();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_RIPPLESBUILDER_H
