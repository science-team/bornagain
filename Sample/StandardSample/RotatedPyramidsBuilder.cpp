//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/RotatedPyramidsBuilder.cpp
//! @brief     Implements classe RotatedPyramidsBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/RotatedPyramidsBuilder.h"
#include "Base/Const/Units.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Polyhedra.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

using Units::deg;

Sample* ExemplarySamples::createRotatedPyramids()
{
    const double m_length(10);
    const double m_height(5);
    const double m_alpha(Units::deg2rad(54.73));
    const double m_zangle(45. * deg);

    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Pyramid4 ff_pyramid(m_length, m_height, m_alpha);

    Particle pyramid(refMat::Particle, ff_pyramid);

    RotationZ z_rotation(m_zangle);
    pyramid.rotate(z_rotation);

    ParticleLayout particle_layout;
    particle_layout.addParticle(pyramid);

    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}
