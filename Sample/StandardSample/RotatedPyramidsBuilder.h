//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/RotatedPyramidsBuilder.h
//! @brief     Defines class RotatedPyramidsBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_ROTATEDPYRAMIDSBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_ROTATEDPYRAMIDSBUILDER_H

class Sample;

//! Builds sample: Pyramids, rotated pyramids on top of substrate (IsGISAXS example #9)

namespace ExemplarySamples {

Sample* createRotatedPyramids();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_ROTATEDPYRAMIDSBUILDER_H
