//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/SimpleLayerBuilder.cpp
//! @brief     Implements class SimpleLayerBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/SimpleLayerBuilder.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

Sample* ExemplarySamples::createSimpleLayer()
{
    const double layer_thickness(10.0);

    Layer vacuum_layer(refMat::Vacuum);
    Layer middle_layer(refMat::Particle, layer_thickness);
    Layer substrate(refMat::Substrate);

    auto* result = new Sample;
    result->addLayer(vacuum_layer);
    result->addLayer(middle_layer);
    result->addLayer(substrate);
    return result;
}
