//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/SimpleLayerBuilder.h
//! @brief     Defines class SimpleLayerBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_SIMPLELAYERBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_SIMPLELAYERBUILDER_H

class IFormfactor;
class Sample;

//! Generates a sample with single layer without particles

namespace ExemplarySamples {

Sample* createSimpleLayer();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_SIMPLELAYERBUILDER_H
