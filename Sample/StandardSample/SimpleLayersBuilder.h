//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/SimpleLayersBuilder.h
//! @brief     Defines class LayersWithAbsorptionBuilder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_SIMPLELAYERSBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_SIMPLELAYERSBUILDER_H

class IFormfactor;
class MultiLayer;

//! The LayersWithAbsorptionBuilder class generates a sample with 3 layers with
//! absorption (refractive index has imaginary part).
//!
//! The middle layer is populated with particles.

namespace ExemplarySamples {

MultiLayer* createLayersWithAbsorptionWithFF(const IFormfactor*);

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_SIMPLELAYERSBUILDER_H
