//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/SizeDistributionModelsBuilder.h
//! @brief     Defines various sample builder classes to test DA, LMA, SSCA approximations.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_SIZEDISTRIBUTIONMODELSBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_SIZEDISTRIBUTIONMODELSBUILDER_H

class Sample;

//! Creates the sample demonstrating size distribution model in decoupling approximation.
//! Equivalent of Examples/Python/simulation/ex03_Interferences/ApproximationDA.py

namespace ExemplarySamples {

Sample* createSizeDistributionDAModel();

} // namespace ExemplarySamples

//! Creates the sample demonstrating size distribution model in local monodisperse approximation.
//! Equivalent of Examples/Python/simulation/ex03_Interferences/ApproximationLMA.py

namespace ExemplarySamples {

Sample* createSizeDistributionLMAModel();

} // namespace ExemplarySamples

//! Creates the sample demonstrating size distribution model in size space coupling approximation.
//! Equivalent of Examples/Python/simulation/ex03_Interferences/ApproximationSSCA.py

namespace ExemplarySamples {

Sample* createSizeDistributionSSCAModel();

} // namespace ExemplarySamples

//! Builds sample: size spacing correlation approximation (IsGISAXS example #15).

namespace ExemplarySamples {

Sample* createCylindersInSSCA();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_SIZEDISTRIBUTIONMODELSBUILDER_H
