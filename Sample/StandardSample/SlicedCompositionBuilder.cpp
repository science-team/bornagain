//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/SlicedCompositionBuilder.cpp
//! @brief     Defines classes to build various particles crossing interfaces.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/SlicedCompositionBuilder.h"
#include "Base/Const/Units.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/SphericalSegment.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Compound.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

using Units::deg;

Sample* ExemplarySamples::createSlicedComposition()
{
    const double sphere_radius = 10.0;
    const double bottom_cup_height = 4.0;
    const double composition_shift = bottom_cup_height;

    Particle topCup(refMat::Ag, SphericalSegment(sphere_radius, 0, bottom_cup_height));
    Particle bottomCup(refMat::Teflon,
                       SphericalSegment(sphere_radius, 0, sphere_radius * 2 - bottom_cup_height));
    bottomCup.rotate(RotationX(180 * deg));

    Compound composition;
    composition.addComponent(topCup, R3(0.0, 0.0, bottom_cup_height));
    composition.addComponent(bottomCup, R3(0.0, 0.0, bottom_cup_height));
    composition.translate(0, 0, -composition_shift);

    ParticleLayout particle_layout;
    particle_layout.addParticle(composition);

    Layer vacuum_layer(refMat::Vacuum);
    vacuum_layer.addLayout(particle_layout);

    Layer substrate_layer(refMat::Substrate2);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}
