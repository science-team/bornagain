//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/SlicedCompositionBuilder.h
//! @brief     Defines classes to build various particles crossing interfaces.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_SLICEDCOMPOSITIONBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_SLICEDCOMPOSITIONBUILDER_H

class Sample;

//! Builds sample: spherical composition made of top+bottom spherical cups

namespace ExemplarySamples {

Sample* createSlicedComposition();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_SLICEDCOMPOSITIONBUILDER_H
