//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/SlicedCylindersBuilder.h
//! @brief     Defines classes for testing slicing machinery.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_SLICEDCYLINDERSBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_SLICEDCYLINDERSBUILDER_H

class Sample;

//! Builds sample: cylinders on a silicon substrate

namespace ExemplarySamples {

Sample* createSlicedCylinders();

} // namespace ExemplarySamples

//! Provides exactly the same sample as SlicedCylindersBuilder, but with
//! sld-based materials. Assumed wavelength is 1.54 Angstrom.

namespace ExemplarySamples {

Sample* createSLDSlicedCylinders();

} // namespace ExemplarySamples

//! Provides exactly the same sample as SLDSlicedCylindersBuilder, but with
//! cylinders represented as homogeneous layers. SLD-based materials used.
//! Assumed wavelength is 1.54 Angstrom.

namespace ExemplarySamples {

Sample* createAveragedSlicedCylinders();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_SLICEDCYLINDERSBUILDER_H
