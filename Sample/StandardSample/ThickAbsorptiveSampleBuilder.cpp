//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ThickAbsorptiveSampleBuilder.cpp
//! @brief     Implements class to build thick highly-absorptive sample with roughness.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/ThickAbsorptiveSampleBuilder.h"
#include "Sample/Interface/Roughness.h"
#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"

Sample* ExemplarySamples::createThickAbsorptiveSample()
{
    Material vacuum_material = MaterialBySLD("Vacuum", 0.0, 0.0);
    Material au_material = MaterialBySLD("Au", 3.48388057043e-05, 1.79057609656e-05);
    Material si_material = MaterialBySLD("Si", 3.84197565094e-07, 6.28211531498e-07);

    SelfAffineFractalModel autocorrelation(5.0, 0.5, 10.0);
    ErfTransient transient;
    Roughness roughness(&autocorrelation, &transient);

    Layer vacuum_layer(vacuum_material);
    Layer au_layer(au_material, 200.0);
    Layer vacuum_layer_2(vacuum_material, 10.0);
    Layer substrate_layer(si_material, &roughness);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(au_layer);
    sample->addLayer(vacuum_layer_2);
    sample->addLayer(substrate_layer);
    return sample;
}
