//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/ThickAbsorptiveSampleBuilder.h
//! @brief     Defines class to build thick highly-absorptive sample with roughness.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_THICKABSORPTIVESAMPLEBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_THICKABSORPTIVESAMPLEBUILDER_H

class Sample;

namespace ExemplarySamples {

Sample* createThickAbsorptiveSample();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_THICKABSORPTIVESAMPLEBUILDER_H
