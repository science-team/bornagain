//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/TransformationsBuilder.cpp
//! @brief     Implements classes to build samples with different transformations.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/TransformationsBuilder.h"
#include "Base/Const/Units.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/HardParticle/Polyhedra.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

using Units::deg;

Sample* ExemplarySamples::createTransformBox()
{
    const double layer_thickness(100);
    const double length(50);
    const double width(20);
    const double height(10);

    Particle box(refMat::Ag, Box(length, width, height));
    box.rotate(RotationZ(90. * deg));
    box.rotate(RotationY(90. * deg));
    box.translate(R3(0, 0, -layer_thickness / 2.));

    ParticleLayout layout;
    layout.addParticle(box);

    Layer vacuum_layer(refMat::Vacuum);
    Layer middle_layer(refMat::Teflon, layer_thickness);
    middle_layer.addLayout(layout);
    Layer substrate(refMat::Substrate2);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(middle_layer);
    sample->addLayer(substrate);
    return sample;
}
