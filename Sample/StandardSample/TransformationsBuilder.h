//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/TransformationsBuilder.h
//! @brief     Defines classes to build samples with different transformations.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_TRANSFORMATIONSBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_TRANSFORMATIONSBUILDER_H

class Sample;

//! Rotated box in 3 layers system.

namespace ExemplarySamples {

Sample* createTransformBox();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_TRANSFORMATIONSBUILDER_H
