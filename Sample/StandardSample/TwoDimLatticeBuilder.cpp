//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/TwoDimLatticeBuilder.cpp
//! @brief     Implements class IsGISAXS06Builder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sample/StandardSample/TwoDimLatticeBuilder.h"
#include "Base/Const/Units.h"
#include "Sample/Aggregate/Interference2DLattice.h"
#include "Sample/Aggregate/Interference2DSuperLattice.h"
#include "Sample/Aggregate/InterferenceFinite2DLattice.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/Correlation/Profiles2D.h"
#include "Sample/HardParticle/Cylinder.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Compound.h"
#include "Sample/Particle/Particle.h"
#include "Sample/StandardSample/ReferenceMaterials.h"

using Units::deg;

Sample* ExemplarySamples::createBasic2DLattice()
{
    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Interference2DLattice iff(BasicLattice2D(5.0, 10.0, 30.0 * deg, 10.0 * deg));

    Profile2DCauchy pdf(300.0 / 2.0 / pi, 100.0 / 2.0 / pi, 0);
    iff.setDecayFunction(pdf);

    // particles
    ParticleLayout particle_layout;
    Cylinder ff_cyl(5.0, 5.0);
    Particle particle(refMat::Particle, ff_cyl);
    particle_layout.addParticle(particle);

    particle_layout.setInterference(iff);

    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

// -----------------------------------------------------------------------------
// lattice #1:
// -----------------------------------------------------------------------------
Sample* ExemplarySamples::createSquareLattice2D()
{
    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Interference2DLattice iff(SquareLattice2D(10.0, 0));
    Profile2DCauchy pdf(300.0 / 2.0 / pi, 100.0 / 2.0 / pi, 0);
    iff.setDecayFunction(pdf);

    // particles
    ParticleLayout particle_layout;
    Cylinder ff_cyl(5.0, 5.0);
    Particle particle(refMat::Particle, ff_cyl);
    particle_layout.addParticle(particle);

    particle_layout.setInterference(iff);

    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

// -----------------------------------------------------------------------------
// lattice #2: centered
// -----------------------------------------------------------------------------
Sample* ExemplarySamples::createCenteredSquareLattice2D()
{
    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Interference2DLattice interparticle(BasicLattice2D(10.0, 10.0, pi / 2.0, 0));
    Profile2DCauchy pdf(300.0 / 2.0 / pi, 100.0 / 2.0 / pi, 0);
    interparticle.setDecayFunction(pdf);

    Cylinder ff_cyl(5.0, 5.0);
    Particle cylinder(refMat::Particle, ff_cyl);
    std::vector<R3> positions;
    R3 position_1(0.0, 0.0, 0.0);
    R3 position_2(5.0, -5.0, 0.0);
    positions.push_back(position_1);
    positions.push_back(position_2);
    Compound basis;
    basis.addComponents(cylinder, positions);

    ParticleLayout particle_layout;
    particle_layout.addParticle(basis);
    particle_layout.setInterference(interparticle);
    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

// -----------------------------------------------------------------------------
// lattice #3: rotated
// -----------------------------------------------------------------------------
Sample* ExemplarySamples::createRotatedSquareLattice2D()
{
    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Interference2DLattice iff(SquareLattice2D(10.0, 30.0 * deg));
    Profile2DCauchy pdf(300.0 / 2.0 / pi, 100.0 / 2.0 / pi, 30.0 * deg);
    iff.setDecayFunction(pdf);

    ParticleLayout particle_layout;
    // particle
    Cylinder ff_cyl(5.0, 5.0);
    R3 position(0.0, 0.0, 0.0);
    Particle p(refMat::Particle, ff_cyl);
    p.translate(position);
    particle_layout.addParticle(p);
    particle_layout.setInterference(iff);

    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

// -----------------------------------------------------------------------------
// lattice #4: finite square
// -----------------------------------------------------------------------------
Sample* ExemplarySamples::createFiniteSquareLattice2D()
{
    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    InterferenceFinite2DLattice iff(SquareLattice2D(10.0, 0.0), 40, 40);
    iff.setPositionVariance(1.0);

    // particles
    ParticleLayout particle_layout;
    Cylinder ff_cyl(5.0, 5.0);
    Particle particle(refMat::Particle, ff_cyl);
    particle_layout.addParticle(particle, 1.0);

    particle_layout.setInterference(iff);

    vacuum_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}

// -----------------------------------------------------------------------------
// lattice #5: superlattice
// -----------------------------------------------------------------------------
Sample* ExemplarySamples::createSuperLattice()
{
    Layer vacuum_layer(refMat::Vacuum);
    Layer substrate_layer(refMat::Substrate);

    Interference2DSuperLattice iff(SquareLattice2D(200.0, 0.0), 40, 40);
    InterferenceFinite2DLattice substructure(SquareLattice2D(10.0, 0.0), 10, 10);
    iff.setSubstructureIFF(substructure);
    iff.setPositionVariance(1.0);

    // particles
    ParticleLayout particle_layout;
    Cylinder ff_cyl(5.0, 10.0);
    Particle particle(refMat::Vacuum, ff_cyl);
    particle.translate(R3(0.0, 0.0, -10.0));
    particle_layout.addParticle(particle);

    particle_layout.setInterference(iff);
    particle_layout.setTotalParticleSurfaceDensity(100.0 / 4e4);

    substrate_layer.addLayout(particle_layout);

    auto* sample = new Sample;
    sample->addLayer(vacuum_layer);
    sample->addLayer(substrate_layer);
    return sample;
}
