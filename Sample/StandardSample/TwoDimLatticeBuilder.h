//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sample/StandardSample/TwoDimLatticeBuilder.h
//! @brief     Defines class IsGISAXS06Builder.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SAMPLE_STANDARDSAMPLE_TWODIMLATTICEBUILDER_H
#define BORNAGAIN_SAMPLE_STANDARDSAMPLE_TWODIMLATTICEBUILDER_H

class Sample;

//! Builds sample: 2D lattice with arbitrary angle and different lattice length_1 and length_2.

namespace ExemplarySamples {

Sample* createBasic2DLattice();

} // namespace ExemplarySamples

//! Builds sample: 2D lattice with different disorder (IsGISAXS example #6).

namespace ExemplarySamples {

Sample* createSquareLattice2D();

} // namespace ExemplarySamples

//! Builds sample: 2D lattice with different disorder (IsGISAXS example #6).

namespace ExemplarySamples {

Sample* createCenteredSquareLattice2D();

} // namespace ExemplarySamples

//! Builds sample: 2D lattice with different disorder (IsGISAXS example #6).

namespace ExemplarySamples {

Sample* createRotatedSquareLattice2D();

} // namespace ExemplarySamples

//! Builds sample: 2D finite lattice with thermal disorder.

namespace ExemplarySamples {

Sample* createFiniteSquareLattice2D();

} // namespace ExemplarySamples

//! Builds sample: 2D finite lattice of 2D finite lattices (superlattice).

namespace ExemplarySamples {

Sample* createSuperLattice();

} // namespace ExemplarySamples

#endif // BORNAGAIN_SAMPLE_STANDARDSAMPLE_TWODIMLATTICEBUILDER_H
