//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Background/ConstantBackground.cpp
//! @brief     Implements class ConstantBackground.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sim/Background/ConstantBackground.h"
#include "Base/Util/Assert.h"

ConstantBackground::ConstantBackground(const std::vector<double> P)
    : IBackground(P)
    , m_background_value(m_P[0])
{
    validateOrThrow();
}

ConstantBackground::ConstantBackground(double background_value)
    : ConstantBackground(std::vector<double>{background_value})
{
}

ConstantBackground* ConstantBackground::clone() const
{
    return new ConstantBackground(m_background_value);
}

double ConstantBackground::addBackground(double intensity) const
{
    ASSERT(m_validated);
    return intensity + m_background_value;
}

std::string ConstantBackground::validate() const
{
    std::vector<std::string> errs;
    requestGe0(errs, m_background_value, "background_value");
    if (!errs.empty())
        return jointError(errs);

    m_validated = true;
    return "";
}
