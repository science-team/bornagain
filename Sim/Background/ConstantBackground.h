//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Background/ConstantBackground.h
//! @brief     Defines class ConstantBackground.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_BACKGROUND_CONSTANTBACKGROUND_H
#define BORNAGAIN_SIM_BACKGROUND_CONSTANTBACKGROUND_H

#include "Sim/Background/IBackground.h"

//! Constant background, to be added to simulated scattering.
//!

class ConstantBackground : public IBackground {
public:
    ConstantBackground(std::vector<double> P);
    ConstantBackground(double background_value);

#ifndef SWIG
    ConstantBackground* clone() const override;
#endif // SWIG

    std::string className() const final { return "ConstantBackground"; }
    std::vector<ParaMeta> parDefs() const final { return {{"BackgroundValue", ""}}; }

    double backgroundValue() const { return m_background_value; }

    double addBackground(double intensity) const override;

    std::string validate() const override;

private:
    const double& m_background_value;
};

#endif // BORNAGAIN_SIM_BACKGROUND_CONSTANTBACKGROUND_H
