//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Background/IBackground.cpp
//! @brief     Implements interface IBackground.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sim/Background/IBackground.h"

IBackground::IBackground(const std::vector<double>& PValues)
    : INode(PValues)
{
}

IBackground::~IBackground() = default;
