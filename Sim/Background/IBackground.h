//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Background/IBackground.h
//! @brief     Defines interface IBackground.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_BACKGROUND_IBACKGROUND_H
#define BORNAGAIN_SIM_BACKGROUND_IBACKGROUND_H

#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"

//! Abstract base class for background noise, to be added to simulated scattering.
//!
//! Base class of ConstantBackground, PoissonBackground.


class IBackground : public ICloneable, public INode {
public:
    IBackground(const std::vector<double>& PValues);
    ~IBackground() override;

#ifndef SWIG
    IBackground* clone() const override = 0;
#endif // SWIG

    virtual double addBackground(double element) const = 0;
};

#endif // BORNAGAIN_SIM_BACKGROUND_IBACKGROUND_H
