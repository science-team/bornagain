//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Background/PoissonBackground.cpp
//! @brief     Implements class PoissonBackground.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sim/Background/PoissonBackground.h"
#include "Base/Math/Functions.h"

PoissonBackground::PoissonBackground()
    : IBackground({})
{
}

PoissonBackground* PoissonBackground::clone() const
{
    return new PoissonBackground;
}

double PoissonBackground::addBackground(double intensity) const
{
    return Math::GeneratePoissonRandom(intensity);
}
