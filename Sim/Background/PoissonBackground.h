//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Background/PoissonBackground.h
//! @brief     Defines class PoissonBackground.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_BACKGROUND_POISSONBACKGROUND_H
#define BORNAGAIN_SIM_BACKGROUND_POISSONBACKGROUND_H

#include "Sim/Background/IBackground.h"

//! Poisson distributed noise, to be added to simulated scattering.
//!

class PoissonBackground : public IBackground {
public:
    PoissonBackground();

#ifndef SWIG
    PoissonBackground* clone() const override;
#endif // SWIG

    std::string className() const final { return "PoissonBackground"; }

    double addBackground(double intensity) const override;
};

#endif // BORNAGAIN_SIM_BACKGROUND_POISSONBACKGROUND_H
