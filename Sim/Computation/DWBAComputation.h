//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Computation/DWBAComputation.h
//! @brief     Defines class DWBAComputation.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_COMPUTATION_DWBACOMPUTATION_H
#define BORNAGAIN_SIM_COMPUTATION_DWBACOMPUTATION_H

class DiffuseElement;
class ReSample;
class SimulationOptions;

namespace Compute {

double scattered_and_reflected(const ReSample& re_sample, const SimulationOptions& options,
                               DiffuseElement& ele);

} // namespace Compute

#endif // BORNAGAIN_SIM_COMPUTATION_DWBACOMPUTATION_H
