//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Computation/GISASSpecularContribution.h
//! @brief     Defines class GISASSpecularContribution.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_COMPUTATION_GISASSPECULARCONTRIBUTION_H
#define BORNAGAIN_SIM_COMPUTATION_GISASSPECULARCONTRIBUTION_H

class DiffuseElement;
class ReSample;

namespace Compute {

//! Computes the specular signal in the bin where q_parallel = 0. Used by DWBAComputation.
double gisasSpecularContribution(const ReSample& re_sample, const DiffuseElement& ele);

} // namespace Compute

#endif // BORNAGAIN_SIM_COMPUTATION_GISASSPECULARCONTRIBUTION_H
