//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Computation/RoughMultiLayerContribution.h
//! @brief     Defines class RoughMultiLayerContribution.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_COMPUTATION_ROUGHMULTILAYERCONTRIBUTION_H
#define BORNAGAIN_SIM_COMPUTATION_ROUGHMULTILAYERCONTRIBUTION_H

class DiffuseElement;
class ReSample;

namespace Compute {

//! Computes the diffuse reflection from the rough interfaces of a sample.
//! Used by DWBAComputation.
double roughMultiLayerContribution(const ReSample& re_sample, const DiffuseElement& ele);

} // namespace Compute

#endif // BORNAGAIN_SIM_COMPUTATION_ROUGHMULTILAYERCONTRIBUTION_H
