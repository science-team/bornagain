//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Computation/SpecularComputation.h
//! @brief     Defines class SpecularComputation.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_COMPUTATION_SPECULARCOMPUTATION_H
#define BORNAGAIN_SIM_COMPUTATION_SPECULARCOMPUTATION_H

#include <heinz/Complex.h>

class SpinMatrix;

namespace Compute {

double magneticR(const SpinMatrix& R, const SpinMatrix& polMatrix, const SpinMatrix& anaMatrix);

double scalarR(complex_t R);

} // namespace Compute

#endif // BORNAGAIN_SIM_COMPUTATION_SPECULARCOMPUTATION_H
