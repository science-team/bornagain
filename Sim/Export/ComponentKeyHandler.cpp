//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Export/ComponentKeyHandler.cpp
//! @brief     Implement class ComponentKeyHandler.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sim/Export/ComponentKeyHandler.h"
#include "Base/Util/Assert.h"
#include <algorithm>

void ComponentKeyHandler::insertModel(const std::string& tag, const INode* s)
{
    m_objects[tag].emplace_back(s);
}

std::string ComponentKeyHandler::obj2key(const INode* s) const
{
    for (const auto& it : m_objects) {
        const std::vector<const INode*>& v = it.second;
        const auto vpos = std::find(v.begin(), v.end(), s);
        if (vpos == std::end(v))
            continue;
        const std::string& tag = it.first;
        if (v.size() == 1)
            return tag;
        return tag + "_" + std::to_string(vpos - v.begin() + 1);
    }
    ASSERT_NEVER; // object not found
}
