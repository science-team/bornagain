//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Export/ComponentKeyHandler.h
//! @brief     Defines class ComponentKeyHandler.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_EXPORT_COMPONENTKEYHANDLER_H
#define BORNAGAIN_SIM_EXPORT_COMPONENTKEYHANDLER_H

#include <map>
#include <string>
#include <vector>

class INode;

//! Stores INode instances, associates them with given tag, and provides unique keys.

class ComponentKeyHandler {
public:
    void insertModel(const std::string& tag, const INode* s);

    template <typename T> std::vector<const T*> objectsOfType() const;
    std::string obj2key(const INode* s) const;

private:
    std::map<std::string, std::vector<const INode*>> m_objects;
};

template <typename T> std::vector<const T*> ComponentKeyHandler::objectsOfType() const
{
    std::vector<const T*> result;
    for (const auto& it : m_objects)
        for (const INode* s : it.second)
            if (const auto* c = dynamic_cast<const T*>(s); c)
                result.emplace_back(c);
    return result;
}

#endif // BORNAGAIN_SIM_EXPORT_COMPONENTKEYHANDLER_H
