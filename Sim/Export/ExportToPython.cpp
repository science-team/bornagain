//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Export/ExportToPython.cpp
//! @brief     Implements namespace ExportToPython.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sim/Export/ExportToPython.h"
#include "Base/Py/PyFmt.h"
#include "Sim/Export/SampleToPython.h"
#include "Sim/Export/SimulationToPython.h"
#include "Sim/Simulation/ISimulation.h"

std::string Py::Export::sampleCode(const Sample& sample)
{
    const std::string code = SampleToPython().sampleCode(sample);
    return "import bornagain as ba\n" + Py::Fmt::printImportedSymbols(code) + "\n\n" + code;
}

std::string Py::Export::simulationPlotCode(const ISimulation& simulation)
{
    return SimulationToPython().simulationPlotCode(simulation);
}

std::string Py::Export::simulationSaveCode(const ISimulation& simulation, const std::string& fname)
{
    return SimulationToPython().simulationSaveCode(simulation, fname);
}
