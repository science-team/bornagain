//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Export/ExportToPython.h
//! @brief     Defines namespace ExportToPython.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_EXPORT_EXPORTTOPYTHON_H
#define BORNAGAIN_SIM_EXPORT_EXPORTTOPYTHON_H

#include <string>

class ISimulation;
class Sample;

//! Wraps methods that serialize objects to Python.

namespace Py::Export {

std::string sampleCode(const Sample& sample);
std::string simulationPlotCode(const ISimulation& simulation);
std::string simulationSaveCode(const ISimulation& simulation, const std::string& fname);

} // namespace Py::Export

#endif // BORNAGAIN_SIM_EXPORT_EXPORTTOPYTHON_H
