//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Export/MaterialKeyHandler.cpp
//! @brief     Implement class MaterialKeyHandler.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sim/Export/MaterialKeyHandler.h"
#include "Base/Util/Assert.h"
#include "Sample/Material/Material.h"

void MaterialKeyHandler::insertMaterial(const Material* mat)
{
    for (const auto& it : m_Mat2Unique)
        if (*it.second == *mat) {
            m_Mat2Unique.emplace(mat, it.second);
            return;
        }
    m_Mat2Unique.emplace(mat, mat);

    const std::string key = "material_" + mat->materialName();

    // material must not be exported more than once
#ifdef BA_CPP20
    ASSERT(!m_Key2Mat.contains(key));
#else
    ASSERT(m_Key2Mat.find(key) == m_Key2Mat.end());
#endif // BA_CPP20

    m_Key2Mat.emplace(key, mat);
}

const std::string& MaterialKeyHandler::mat2key(const Material* mat) const
{
    const Material* unique_mat = m_Mat2Unique.at(mat);
    for (const auto& it : m_Key2Mat)
        if (it.second == unique_mat)
            return it.first;
    ASSERT_NEVER;
}
