//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Export/MaterialKeyHandler.h
//! @brief     Defines class MaterialKeyHandler.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_EXPORT_MATERIALKEYHANDLER_H
#define BORNAGAIN_SIM_EXPORT_MATERIALKEYHANDLER_H

#include <map>
#include <string>

class Material;

//! Stores Material instances, associates them with given tag, and provides unique keys.

class MaterialKeyHandler {
public:
    void insertMaterial(const Material* mat);

    const std::map<const std::string, const Material*>& materialMap() const { return m_Key2Mat; }
    const std::string& mat2key(const Material* mat) const;

private:
    std::map<const Material*, const Material*> m_Mat2Unique;
    std::map<const std::string, const Material*> m_Key2Mat;
};

#endif // BORNAGAIN_SIM_EXPORT_MATERIALKEYHANDLER_H
