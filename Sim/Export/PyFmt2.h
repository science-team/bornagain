//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Export/PyFmt2.h
//! @brief     Defines namespace pyfmt2.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_EXPORT_PYFMT2_H
#define BORNAGAIN_SIM_EXPORT_PYFMT2_H

#include <functional>
#include <string>

class IDistribution1D;
class IShape2D;
class ParameterDistribution;
class Scale;

//! Utility functions for writing Python code snippets.

namespace Py::Fmt2 {

std::string representShape2D(const std::string& indent, const IShape2D* ishape, bool mask_value,
                             std::function<std::string(double)> printValueFunc);

std::string printAxis(const Scale* axis, const std::string& unit);

std::string printParameterDistribution(const ParameterDistribution& par_distr,
                                       const std::string& distVarName);

std::string printDistribution(const IDistribution1D& distr);

} // namespace Py::Fmt2

#endif // BORNAGAIN_SIM_EXPORT_PYFMT2_H
