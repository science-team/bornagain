//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Export/SampleToPython.h
//! @brief     Defines class SampleToPython.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_EXPORT_SAMPLETOPYTHON_H
#define BORNAGAIN_SIM_EXPORT_SAMPLETOPYTHON_H

#include <string>

class IParticle;
class Sample;

//! Generates Python code snippet from domain (C++) objects representing sample construction.

class SampleToPython {
public:
    SampleToPython();
    ~SampleToPython() = default;

    std::string sampleCode(const Sample& sample);
};

#endif // BORNAGAIN_SIM_EXPORT_SAMPLETOPYTHON_H
