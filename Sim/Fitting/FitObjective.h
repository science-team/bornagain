//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Fitting/FitObjective.h
//! @brief     Defines class FitObjective.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_FITTING_FITOBJECTIVE_H
#define BORNAGAIN_SIM_FITTING_FITOBJECTIVE_H

#include "Sim/Fitting/FitTypes.h"
#include <string>
#include <vector>

class Datafield;
class FitStatus;
class IChiSquaredModule;
class IMetricWrapper;
class IterationInfo;
class ObjectiveMetric;
class PyBuilderCallback;
class PyObserverCallback;
class SimDataPair;

namespace mumufit {
class MinimizerResult;
class Parameters;
} // namespace mumufit

//! Holds vector of SimDataPair%s (experimental data and simulation results) for use in fitting.
//! Thereby supports simultaneous fitting of several data sets and model functions,
//! as demonstrated in example fit52/multiple_datasets.

class FitObjective {
public:
    FitObjective();
    ~FitObjective();

    void execSimulations(const mumufit::Parameters& params);

    void setObjectiveMetric(const std::string& metric);
    //! Sets objective metric to the FitObjective.
    //! @param metric: metric name
    //! @param norm: metric norm name (defaults to L2-norm)
    void setObjectiveMetric(const std::string& metric, const std::string& norm);

    //! Initializes printing to standard output on every_nth fit iteration.
    void initPrint(int every_nth);

    double evaluate(const mumufit::Parameters& params);

    std::vector<double> evaluate_residuals(const mumufit::Parameters& params);

    //! Should be explicitly called on last iteration to notify all observers.
    void finalize(const mumufit::MinimizerResult& result);

    //! Initializes observer callback to be called on every_nth fit iteration.
    void initPlot(int every_nth, void* pCallable, PyCaller_t pCall);

    Datafield simulationResult(size_t i_item = 0) const;
    Datafield experimentalData(size_t i_item = 0) const;
    Datafield relativeDifference(size_t i_item = 0) const;
    Datafield absoluteDifference(size_t i_item = 0) const;

    std::vector<double> flatExpData() const;
    std::vector<double> flatSimData() const;

    size_t nPairs() const;

    IterationInfo iterationInfo() const;

#ifndef SWIG
    //! Adds a C++ function which builds a simulation.
    void addFitPair(const simulation_builder_t& sim_fn, const Datafield& expData,
                    const double weight = 1.0);

    //! Adds a Python function which builds a simulation.
    //! Calls from Python pass through overloaded function defined in libBornAgainSim.i.
    void addFitPair(void* pSimulationCallable, PySimulate_t pSimulationCaller, PyFree_t pFree,
                    const Datafield& expData, const double weight = 1.0);

    void setChiSquaredModule(const IChiSquaredModule& module);
    void setObjectiveMetric(std::unique_ptr<ObjectiveMetric> metric);

    bool containsUncertainties(size_t i_item) const;
    bool allPairsHaveUncertainties() const;

    //! Returns a reference to i-th SimDataPair.
    const SimDataPair& dataPair(size_t i_item = 0) const;

    void initPlot(int every_nth, fit_observer_t&& observer);

    bool isCompleted() const;

    void interruptFitting();

    bool isInterrupted() const;

    bool isFirstIteration() const;

    mumufit::MinimizerResult minimizerResult() const;

private:
    using DataPairAccessor = std::vector<double> (SimDataPair::*)() const;

    std::vector<double> composeArray(DataPairAccessor getter) const;

    std::vector<SimDataPair> m_sim_data_pairs;
    std::unique_ptr<IMetricWrapper> m_metric_module;
    std::unique_ptr<FitStatus> m_fit_status;
#endif // SWIG
};

#endif // BORNAGAIN_SIM_FITTING_FITOBJECTIVE_H
