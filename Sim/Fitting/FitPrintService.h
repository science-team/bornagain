//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Fitting/FitPrintService.h
//! @brief     Defines class FitPrintService.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_FITTING_FITPRINTSERVICE_H
#define BORNAGAIN_SIM_FITTING_FITPRINTSERVICE_H

#include "Fit/Tool/WallclockTimer.h"
#include <string>

class FitObjective;

//! Prints fit statistics to standard output during minimizer iterations.

class FitPrintService {
public:
    void print(const FitObjective& objective);

private:
    std::string iterationHeaderString(const FitObjective& objective);
    std::string parameterString(const FitObjective& objective);
    std::string fitResultString(const FitObjective& objective);

    WallclockTimer m_run_time;
    WallclockTimer m_last_call_time;
};

#endif // BORNAGAIN_SIM_FITTING_FITPRINTSERVICE_H
