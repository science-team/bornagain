//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Fitting/FitStatus.h
//! @brief     Defines class FitStatus.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_FITTING_FITSTATUS_H
#define BORNAGAIN_SIM_FITTING_FITSTATUS_H

#include "Sim/Fitting/FitObserver.h"
#include "Sim/Fitting/IterationInfo.h"
#include <functional>
#include <vector>

class FitObjective;
class FitPrintService;

namespace mumufit {
class MinimizerResult;
}

//! Contains status of the fitting (running, interupted etc) and all intermediate
//! information which has to be collected during the fit.
//! Owned by FitObjective.

class FitStatus {
public:
    FitStatus(const FitObjective* fit_objective);
    ~FitStatus();

    void setInterrupted();
    bool isInterrupted() const;
    bool isCompleted() const;

    void update(const mumufit::Parameters& params, double chi2);

    void initPrint(int every_nth);

    void addObserver(int every_nth, fit_observer_t&&);

    IterationInfo iterationInfo() const { return m_iteration_info; }

    mumufit::MinimizerResult minimizerResult() const;

    //! Should be explicitly called on last iteration to notify all observers.
    void finalize(const mumufit::MinimizerResult& result);

private:
    enum StatusCode { IDLE, RUNNING, COMPLETED, FAILED, INTERRUPTED };
    StatusCode m_code;
    FitObserver<FitObjective> m_observers;
    std::unique_ptr<FitPrintService> m_print_service;
    const FitObjective* m_fit_objective;
    IterationInfo m_iteration_info;
    std::unique_ptr<mumufit::MinimizerResult> m_minimizer_result;
};

#endif // BORNAGAIN_SIM_FITTING_FITSTATUS_H
