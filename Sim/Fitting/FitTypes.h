//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Fitting/FitTypes.h
//! @brief     Defines common types for fitting library.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_FITTING_FITTYPES_H
#define BORNAGAIN_SIM_FITTING_FITTYPES_H

#include <functional>
#include <memory>

class FitObjective;
class ISimulation;

namespace mumufit {
class Parameters;
}

using simulation_builder_t =
    std::function<std::unique_ptr<ISimulation>(const mumufit::Parameters&)>;
using fit_observer_t = std::function<void(const FitObjective&)>;

typedef void (*PySimulate_t)(void*, const mumufit::Parameters&, ISimulation*&, void*&);
typedef void (*PyFree_t)(void* pyObject);
typedef void (*PyCaller_t)(void* pyObject, const FitObjective& fit_objective);

#endif // BORNAGAIN_SIM_FITTING_FITTYPES_H
