//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Fitting/ObjectiveMetricUtil.h
//! @brief     Defines ObjectiveMetric utilities and corresponding namespace.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_FITTING_OBJECTIVEMETRICUTIL_H
#define BORNAGAIN_SIM_FITTING_OBJECTIVEMETRICUTIL_H

#include <functional>
#include <memory>
#include <string>
#include <vector>

class ObjectiveMetric;

//! Utility functions related to class ObjectiveMetric.

namespace ObjectiveMetricUtil {

//! Returns L1 normalization function.
std::function<double(double)> l1Norm();
//! Returns L2 normalization function.
std::function<double(double)> l2Norm();

//! Creates the specified metric with the default norm.
std::unique_ptr<ObjectiveMetric> createMetric(const std::string& metric);

//! Creates the metric with the specified norm
std::unique_ptr<ObjectiveMetric> createMetric(std::string metric, std::string norm);

//! Prints available metric options
std::string availableMetricOptions();

//! Returns the names of the norms used by ObjectiveMetric.
std::vector<std::string> normNames();

//! Returns the names of the objective metrics used.
std::vector<std::string> metricNames();

//! Returns default norm name.
std::string defaultNormName();

//! Returns default metric name.
std::string defaultMetricName();

} // namespace ObjectiveMetricUtil

#endif // BORNAGAIN_SIM_FITTING_OBJECTIVEMETRICUTIL_H
