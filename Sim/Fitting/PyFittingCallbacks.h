//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Fitting/PyFittingCallbacks.h
//! @brief     Defines family of PyFittingCallbacks classes.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_FITTING_PYFITTINGCALLBACKS_H
#define BORNAGAIN_SIM_FITTING_PYFITTINGCALLBACKS_H

//! Collection of wrapper classes to call Python callable from C++.

#include "Device/Data/Datafield.h"
#include "Fit/Param/Parameters.h"
#include "Sim/Fitting/FitTypes.h"

#ifndef SWIG

class ISimulation;

class SimulationWrapper {
public:
    // C++ function which returns an ISimulation instance;
    // signature:
    // fn(Parameters) -> ISimulation instance
    simulation_builder_t cSimulationFn = nullptr;

    // Python function which returns an ISimulation instance wrapped in a Python object;
    // signature:
    // fn(Parameters:Python-dict) -> Python-Object which wraps an ISimulation instance
    void* pySimulationFn = nullptr;
    PySimulate_t pySimulate = nullptr;
    PyFree_t pyFree = nullptr;

    // Generated ISimulation instance
    std::unique_ptr<ISimulation> simulation;

    // Executes the C++/Python simulation-builder function
    Datafield simulate(const mumufit::Parameters& prm);

    // Discards the byproduct of the simulation (if any):
    // The C++ ISimulation instance and the PythonObject which wraps
    // the C++ ISimulation instance
    void discard();

    // Checks validity and throw exceptions if not valid
    void check() const;

    SimulationWrapper() = default;
    SimulationWrapper(const SimulationWrapper& other);
    ~SimulationWrapper();

private:
    // Extra Python object which wraps the returned C++ ISimulation instance
    void* m_pythonObject = nullptr;
    // Executes the C++ function to produce an ISimulation instance
    Datafield m_executeSimulation(const mumufit::Parameters& prm);

    // Executes the Python function to produce an ISimulation instance
    Datafield m_executePySimulation(const mumufit::Parameters& prm);
};
#endif // SWIG

#endif // BORNAGAIN_SIM_FITTING_PYFITTINGCALLBACKS_H
