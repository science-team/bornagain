//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Fitting/SimDataPair.h
//! @brief     Defines class SimDataPair.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_FITTING_SIMDATAPAIR_H
#define BORNAGAIN_SIM_FITTING_SIMDATAPAIR_H

#include "Sim/Fitting/FitTypes.h"
#include "Sim/Fitting/SimulationWrapper.h" // SimulationWrapper

class Datafield;

//! Holds pair of simulation/experimental data to fit.

class SimDataPair {
public:
    SimDataPair(const SimulationWrapper& sim, const Datafield& raw_data, const double weight = 1.0);

    SimDataPair(SimDataPair&& other) noexcept;

    ~SimDataPair();

    void execSimulation(const mumufit::Parameters& params);

    bool containsUncertainties() const;

    //! Returns the result of last computed simulation
    Datafield simulationResult() const;

    //! Returns the experimental data cut to the ROI area
    Datafield experimentalData() const;

    //! Returns the relative difference between simulated
    //! and experimental data cut to the ROI area
    Datafield relativeDifference() const;

    //! Returns the absolute difference between simulated
    //! and experimental data cut to the ROI area
    Datafield absoluteDifference() const;

    //! Returns the flattened simulated intensities cut to the ROI area
    std::vector<double> simulation_array() const;

    //! Returns the flattened experimental data cut to the ROI area
    std::vector<double> experimental_array() const;

    //! Returns the flattened experimental uncertainties
    //! cut to the ROI area. If no uncertainties are available,
    //! Returns a zero-filled array sized to the ROI area.
    std::vector<double> uncertainties_array() const;

    double weight() const { return m_weight; }

private:
    void validate() const;

    //! ISimulation builder from the user to construct simulation for given set of parameters.
    SimulationWrapper m_simulation_builder;

    // TODO: replace `std::unique_ptr<Datafield>` with `Datafield`
    //! Raw experimental data as obtained from the user.
    std::unique_ptr<Datafield> m_raw_data;
    //! Weight in multidata fitting
    double m_weight;

    //! Current simulation results. Masked areas are nullified.
    std::unique_ptr<Datafield> m_sim_data;

    //! Experimental data cut to the ROI. Masked areas are nullified.
    std::unique_ptr<Datafield> m_exp_data;
};

#endif // BORNAGAIN_SIM_FITTING_SIMDATAPAIR_H
