//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Fitting/SimulationWrapper.h
//! @brief     Defines class SimulationWrapper
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_SIM_FITTING_SIMULATIONWRAPPER_H
#define BORNAGAIN_SIM_FITTING_SIMULATIONWRAPPER_H

//! Collection of wrapper classes to call Python callable from C++.

#include "Fit/Param/Parameters.h"
#include "Sim/Fitting/FitTypes.h"

class Datafield;
class ISimulation;

class SimulationWrapper {
public:
    SimulationWrapper() = default;
    SimulationWrapper(const SimulationWrapper& other);
    ~SimulationWrapper();

    // C++ function which returns an ISimulation instance;
    // signature:
    // fn(Parameters) -> ISimulation instance
    simulation_builder_t cSimulationFn = nullptr;

    // Python function which returns an ISimulation instance wrapped in a Python object;
    // signature:
    // fn(Parameters:Python-dict) -> Python-Object which wraps an ISimulation instance
    void* pySimulationFn = nullptr;
    PySimulate_t pySimulate = nullptr;
    PyFree_t pyFree = nullptr;

    // Generated ISimulation instance
    std::unique_ptr<ISimulation> simulation;

    // Executes the C++/Python simulation-builder function
    Datafield simulate(const mumufit::Parameters& prm);

    // Discards the byproduct of the simulation (if any):
    // The C++ ISimulation instance and the PythonObject which wraps
    // the C++ ISimulation instance
    void discard();

    // Checks validity and throw exceptions if not valid
    void check() const;

private:
    //! Python object that wraps the returned C++ ISimulation instance
    void* m_pythonObject = nullptr;

    //! Executes the C++ function to produce an ISimulation instance
    Datafield m_executeSimulation(const mumufit::Parameters& prm);

    //! Executes the Python function to produce an ISimulation instance
    Datafield m_executePySimulation(const mumufit::Parameters& prm);
};

#endif // BORNAGAIN_SIM_FITTING_SIMULATIONWRAPPER_H
