//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Residual/ChiSquaredModule.h
//! @brief     Defines class ChiSquaredModule.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_RESIDUAL_CHISQUAREDMODULE_H
#define BORNAGAIN_SIM_RESIDUAL_CHISQUAREDMODULE_H

#include "Sim/Residual/IChiSquaredModule.h"

//! Calculation of chi2 between two data sets.

class ChiSquaredModule : public IChiSquaredModule {
public:
    ChiSquaredModule() = default;
    ChiSquaredModule(const ChiSquaredModule& other) = default;
    ~ChiSquaredModule() override = default;

    double residual(double a, double b) override;

#ifndef SWIG
    ChiSquaredModule* clone() const override { return new ChiSquaredModule(*this); }
#endif // SWIG
};

#endif // BORNAGAIN_SIM_RESIDUAL_CHISQUAREDMODULE_H
