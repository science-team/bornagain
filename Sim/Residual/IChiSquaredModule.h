//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Residual/IChiSquaredModule.h
//! @brief     Defines interface IChiSquaredModule.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_RESIDUAL_ICHISQUAREDMODULE_H
#define BORNAGAIN_SIM_RESIDUAL_ICHISQUAREDMODULE_H

#include "Base/Type/ICloneable.h"
#include <memory>

class IIntensityFunction;
class IVarianceFunction;

//! Interface residual calculations.

class IChiSquaredModule : public ICloneable {
public:
    IChiSquaredModule();
    ~IChiSquaredModule() override;

#ifndef SWIG
    //! clone method
    IChiSquaredModule* clone() const override = 0;
#endif // SWIG

    //! Returns squared function
    const IVarianceFunction* varianceFunction() const;

    //! Sets squared function
    void setVarianceFunction(const IVarianceFunction& variance_function);

    //! Returns data rescaler.
    virtual const IIntensityFunction* getIntensityFunction() const;

    //! Sets data rescaler.
    virtual void setIntensityFunction(const IIntensityFunction& intensity_function);

    virtual double residual(double a, double b) = 0;

protected:
    IChiSquaredModule(const IChiSquaredModule& other);

    std::unique_ptr<IVarianceFunction> m_variance_function;
    std::unique_ptr<IIntensityFunction> m_intensity_function;
};

#endif // BORNAGAIN_SIM_RESIDUAL_ICHISQUAREDMODULE_H
