//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Residual/IIntensityFunction.h
//! @brief     Defines and implements the interface class IIntensityFunction.
//!            and its child classes IntensityFunctionLog, IntensityFunctionSqrt
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_RESIDUAL_IINTENSITYFUNCTION_H
#define BORNAGAIN_SIM_RESIDUAL_IINTENSITYFUNCTION_H

//! Interface for applying arbitrary function to the measured intensity.

class IIntensityFunction {
public:
    virtual ~IIntensityFunction();

#ifndef SWIG
    virtual IIntensityFunction* clone() const = 0;
#endif // SWIG

    virtual double evaluate(double value) const = 0;
};

//! Algorithm for applying log function to the measured intensity.

class IntensityFunctionLog : public IIntensityFunction {
public:
#ifndef SWIG
    IntensityFunctionLog* clone() const override;
#endif // SWIG
    double evaluate(double value) const override;
};

//! Algorithm for applying sqrt function to the measured intensity.

class IntensityFunctionSqrt : public IIntensityFunction {
public:
#ifndef SWIG
    IntensityFunctionSqrt* clone() const override;
#endif // SWIG
    double evaluate(double value) const override;
};

#endif // BORNAGAIN_SIM_RESIDUAL_IINTENSITYFUNCTION_H
