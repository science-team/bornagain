//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Residual/VarianceFunctions.h
//! @brief     Defines class IVarianceFunctiones.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_RESIDUAL_VARIANCEFUNCTIONS_H
#define BORNAGAIN_SIM_RESIDUAL_VARIANCEFUNCTIONS_H


//! Variance function interface.

class IVarianceFunction {
public:
    IVarianceFunction() = default;
    virtual ~IVarianceFunction() = default;
    IVarianceFunction(const IVarianceFunction&) = delete;
    IVarianceFunction& operator=(const IVarianceFunction&) = delete;

#ifndef SWIG
    virtual IVarianceFunction* clone() const = 0;
#endif // SWIG

    virtual double variance(double real_value, double simulated_value) const = 0;
};

//! Returns 1.0 as variance value

class VarianceConstantFunction : public IVarianceFunction {
public:
#ifndef SWIG
    VarianceConstantFunction* clone() const override;
#endif // SWIG
    double variance(double, double) const override;
};

//! Returns max(sim, epsilon)

class VarianceSimFunction : public IVarianceFunction {
public:
    explicit VarianceSimFunction(double epsilon = 1.0);
    double variance(double exp, double sim) const override;

#ifndef SWIG
    VarianceSimFunction* clone() const override;
#endif // SWIG

private:
    double m_epsilon;
};

#endif // BORNAGAIN_SIM_RESIDUAL_VARIANCEFUNCTIONS_H
