//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Scan/AlphaScan.h
//! @brief     Declares class AlphaScan.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_SCAN_ALPHASCAN_H
#define BORNAGAIN_SIM_SCAN_ALPHASCAN_H

#include "Sim/Scan/PhysicalScan.h"

//! Scan type with grazing angles as coordinate values and a unique wavelength.
//! Features footprint correction.
class AlphaScan : public PhysicalScan {
public:
    AlphaScan(const Scale& alpha_axis);
    AlphaScan(std::vector<double> points);
    AlphaScan(int nbins, double alpha_i_min, double alpha_i_max);
    ~AlphaScan() override;

    std::string className() const final { return "AlphaScan"; }

    void setAlphaOffset(double offset) { m_alpha_offset = offset; }

#ifndef SWIG
    AlphaScan* clone() const override;

    //! Generates simulation elements for specular simulations
    std::vector<ScanElement> generateElements() const override;

    double alphaOffset() const { return m_alpha_offset; }

private:
    double m_alpha_offset{0};

#endif // SWIG
};

#endif // BORNAGAIN_SIM_SCAN_ALPHASCAN_H
