//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Scan/BeamScan.h
//! @brief     Declares interface BeamScan.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_SCAN_BEAMSCAN_H
#define BORNAGAIN_SIM_SCAN_BEAMSCAN_H

#include "Base/Const/Units.h"
#include "Base/Type/CloneableVector.h"
#include "Base/Type/ICloneable.h"
#include "Param/Node/INode.h"
#include <cstddef>
#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>
#include <memory>
#include <vector>

class Beam;
class Frame;
class IDistribution1D;
class PolFilter;
class Scale;
class ScanElement;
class SliceStack;
class SpinMatrix;
struct ParameterSample;

//! Abstract base class for all types of specular scans.

class BeamScan : public ICloneable, public INode {
public:
    BeamScan(Scale* axis);
    ~BeamScan() override;

    void setIntensity(double intensity);

    //! Sets the polarization density matrix according to the given Bloch vector
    void setPolarization(const R3& bloch_vector);

    //! Sets the polarization analyzer characteristics of the detector
    void setAnalyzer(const R3& Bloch_vector = {}, double mean_transmission = 0.5);

#ifndef SWIG
    BeamScan* clone() const override = 0;

    std::vector<const INode*> nodeChildren() const override;

    double commonIntensity() const;
    double intensityAt(size_t i) const;

    const R3& commonPolarization() const;
    R3 polarizationAt(size_t i) const;

    const PolFilter* analyzer() const { return m_pol_analyzer.get(); }

    SpinMatrix polarizerMatrixAt(size_t i) const;
    SpinMatrix analyzerMatrix() const;

    //! Generates simulation elements for specular simulations
    virtual std::vector<ScanElement> generateElements() const = 0;

    //! Returns coordinate axis assigned to the data holder
    const Scale* coordinateAxis() const { return m_axis.get(); }

    //! Returns the number of scan points
    size_t nScan() const;

    //! Returns the number of distribution samples
    virtual size_t nDistributionSamples() const = 0;

    //! Returns the number of elements this simulation needs to calculate
    size_t nElements() const { return nScan() * nDistributionSamples(); }

    //! Returns kz values for Abeles computation of reflection/transition coefficients
    std::vector<complex_t> produceKz(const SliceStack& slices, const R3& k) const;

    static constexpr double defaultIntensity = 1.;
    static constexpr double defaultWavelength = 0.1;               // nm
    static constexpr double defaultGrazingAngle = 1. * Units::deg; // rad

protected:
    void copyBeamScan(BeamScan* dest) const; //!< Used by subclass::clone
    std::vector<ParameterSample> drawDistribution(IDistribution1D* distrib, double point) const;

    const std::unique_ptr<Scale> m_axis;
    std::unique_ptr<PolFilter> m_pol_analyzer;
    CloneableVector<Beam> m_beams;

private:
    bool isCommonIntensity() const;
    bool isCommonPolarization() const;

#endif // SWIG
};

#endif // BORNAGAIN_SIM_SCAN_BEAMSCAN_H
