//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Scan/LambdaScan.cpp
//! @brief     Implements class LambdaScan.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Sim/Scan/LambdaScan.h"
#include "Base/Axis/MakeScale.h"
#include "Base/Axis/Scale.h"
#include "Base/Const/PhysicalConstants.h"
#include "Base/Vector/GisasDirection.h"
#include "Device/Beam/Beam.h"
#include "Device/Beam/IFootprint.h"
#include "Param/Distrib/ParameterSample.h"
#include "Resample/Element/ScanElement.h"
#include <algorithm> // is_sorted

using PhysConsts::pi;

LambdaScan::LambdaScan(Scale* lambdaScale)
    : PhysicalScan(lambdaScale)
{
    std::vector<double> axis_values = m_axis->binCenters();
    if (!std::is_sorted(axis_values.begin(), axis_values.end()))
        throw std::runtime_error("LambdaScan: wavelength values are not "
                                 "sorted in ascending order.");
    if (axis_values.front() <= 0)
        throw std::runtime_error("LambdaScan: non-positive wavelengths.");

    for (size_t i = 0; i < nScan(); i++) {
        auto* beam = new Beam(defaultIntensity, m_axis->binCenter(i), defaultGrazingAngle);
        m_beams.push_back(beam);
    }
}

LambdaScan::LambdaScan(const Scale& lambdaScale)
    : LambdaScan(lambdaScale.clone())
{
}

LambdaScan::LambdaScan(std::vector<double> points)
    : LambdaScan(newListScan("lambda (nm)", std::move(points)))
{
}

LambdaScan::LambdaScan(int nbins, double lambda_min, double lambda_max)
    : LambdaScan(newEquiScan("lambda (nm)", nbins, lambda_min, lambda_max))
{
}

LambdaScan::~LambdaScan() = default;

LambdaScan* LambdaScan::clone() const
{
    auto* result = new LambdaScan(*m_axis);
    copyPhysicalScan(result);
    return result;
}

std::vector<ScanElement> LambdaScan::generateElements() const
{
    std::vector<ScanElement> result;
    result.reserve(nElements());

    for (size_t i = 0; i < m_axis->size(); ++i) {
        const std::vector<ParameterSample> alphaDistrib =
            drawDistribution(m_alpha_distrib.get(), grazingAngleAt(i));
        const std::vector<ParameterSample> lambdaDistrib =
            drawDistribution(m_lambda_distrib.get(), wavelengthAt(i));
        const std::vector<ParameterSample> phiDistrib =
            drawDistribution(m_phi_distrib.get(), azimuthalAngleAt(i));
        for (auto ad : alphaDistrib) {
            const double alpha = ad.value;
            for (auto ld : lambdaDistrib) {
                const double lambda = ld.value;
                for (auto pd : phiDistrib) {
                    const double phi = pd.value;
                    const R3 kvec = vecOfLambdaAlphaPhi(lambda, -alpha, -phi);
                    const bool computable =
                        lambda > 0 && alpha >= 0 && alpha <= (pi / 2) && kvec.z() <= 0;
                    const double weight = ad.weight * ld.weight * pd.weight;
                    const double footprint = footprintAt(i) ? footprintAt(i)->calculate(alpha) : 1;
                    result.emplace_back(i, computable, weight, intensityAt(i), footprint,
                                        polarizerMatrixAt(i), analyzerMatrix(), lambda, alpha, phi,
                                        kvec);
                }
            }
        }
    }
    return result;
}
