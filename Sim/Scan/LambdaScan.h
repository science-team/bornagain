//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Scan/LambdaScan.h
//! @brief     Declares class LambdaScan.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_SCAN_LAMBDASCAN_H
#define BORNAGAIN_SIM_SCAN_LAMBDASCAN_H

#include "Sim/Scan/PhysicalScan.h"

class IDistribution1D;

//! Scan type with z-components of scattering vector as coordinate values.
//! Wavelength and incident angles are not accessible separately.

class LambdaScan : public PhysicalScan {
public:
    LambdaScan(const Scale& lambdaScale);
    LambdaScan(std::vector<double> points);
    LambdaScan(int nbins, double lambda_min, double lambda_max);

    ~LambdaScan() override;

    std::string className() const final { return "LambdaScan"; }

#ifndef SWIG
    LambdaScan* clone() const override;

    //! Generates simulation elements for specular simulations
    std::vector<ScanElement> generateElements() const override;

private:
    LambdaScan(Scale* lambdaScale);
#endif // SWIG
};

#endif // BORNAGAIN_SIM_SCAN_LAMBDASCAN_H
