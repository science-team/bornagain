//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Scan/QzScan.h
//! @brief     Declares class QzScan.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_SCAN_QZSCAN_H
#define BORNAGAIN_SIM_SCAN_QZSCAN_H

#include "Sim/Scan/BeamScan.h"
#include <memory>

class IDistribution1D;

//! Scan type with z-components of scattering vector as coordinate values.
//! Wavelength and incident angles are not accessible separately.

class QzScan : public BeamScan {
public:
    //! Accepts qz-value vector (in inverse nm)
    QzScan(std::vector<double> qs_nm);
    QzScan(const Scale& qs_nm);
    //! Sets q-defined specular scan. Accepts either numpy array of q-values sorted in ascending
    //! order or an Scale object with q-values. Alternatively an axis can be defined in-place, then
    //! the first passed parameter is the number of bins, second - minimum on-axis q-value,
    //! third - maximum on-axis q_value.
    QzScan(int nbins, double qz_min, double qz_max);

    ~QzScan() override;

    std::string className() const final { return "QzScan"; }

    void setRelativeQResolution(const IDistribution1D& distr, double rel_dev);

    void setAbsoluteQResolution(const IDistribution1D& distr, double std_dev);
    //! Sets qz resolution function and list of standard deviations.
    void setVectorResolution(const IDistribution1D& distr, const std::vector<double>& std_devs);

    void setOffset(double offset) { m_offset = offset; }

#ifndef SWIG
    QzScan* clone() const override;

    std::vector<const INode*> nodeChildren() const override;

    const IDistribution1D* qzDistribution() const { return m_qz_distrib.get(); }

    //! Generates simulation elements for specular simulations
    std::vector<ScanElement> generateElements() const override;

    size_t nDistributionSamples() const override;

    bool resolution_is_relative() const { return m_relative_resolution; }
    const std::vector<double>& resolution_widths() const { return m_resol_width_factor; }

private:
    QzScan(Scale* qs_nm);

    std::unique_ptr<const IDistribution1D> m_qz_distrib;
    std::vector<double> m_resol_width_factor;
    bool m_relative_resolution{false};
    double m_offset = 0.;

#endif // SWIG
};

#endif // BORNAGAIN_SIM_SCAN_QZSCAN_H
