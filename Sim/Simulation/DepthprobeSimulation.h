//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Simulation/DepthprobeSimulation.h
//! @brief     Defines class DepthprobeSimulation.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_SIMULATION_DEPTHPROBESIMULATION_H
#define BORNAGAIN_SIM_SIMULATION_DEPTHPROBESIMULATION_H

#include "Sim/Simulation/ISimulation.h"

class BeamScan;
class IFootprint;
class Scale;
class ScanElement;

extern const int ZDirection_None;
extern const int ZDirection_Reflected;
extern const int ZDirection_Transmitted;
extern const int WaveProperty_Intensity;
extern const int WaveProperty_Modulus;
extern const int WaveProperty_Phase;

//! Simulation of radiation depth profile.
//!
//! Holds an instrument and sample model.
//! Computes radiation intensity as function of incoming glancing angle and penetration depth.
//! Scattered rays are neglected.
//! Only refraction, reflection and attenuation of the incoming beam are accounted for.

class DepthprobeSimulation : public ISimulation {
public:
    DepthprobeSimulation(const BeamScan& scan, const Sample& sample, const Scale& zaxis,
                         int flags = 0);
    ~DepthprobeSimulation() override;

    std::string className() const final { return "DepthprobeSimulation"; }

#ifndef SWIG

    const BeamScan* scan() const { return m_scan.get(); }

    const Scale& z_axis() const { return *m_z_axis.get(); }

    std::vector<const INode*> nodeChildren() const override;

private:
    //... Overridden executors:
    void initScanElementVector() override;

    void runComputation(const ReSample& re_sample, size_t iElement, double weight) override;

    //... Overridden getters:
    bool force_polarized() const override { return false; }

    //! Returns the number of elements this simulation needs to calculate
    size_t nElements() const override;

    size_t nOutChannels() const override;

    Datafield packResult() const override;

    //... Model components:
    std::unique_ptr<BeamScan> m_scan;
    std::unique_ptr<Scale> m_z_axis;
    const int m_flags;

    //... Caches:
    std::vector<ScanElement> m_eles;
#endif // SWIG
};

#endif // BORNAGAIN_SIM_SIMULATION_DEPTHPROBESIMULATION_H
