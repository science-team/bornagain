//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Simulation/ISimulation.h
//! @brief     Defines interface ISimulation.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_SIMULATION_ISIMULATION_H
#define BORNAGAIN_SIM_SIMULATION_ISIMULATION_H

#include "Param/Distrib/ParameterDistribution.h"
#include "Param/Node/INode.h"
#include <functional>
#include <heinz/Vectors3D.h>

class Datafield;
class DistributionHandler;
class Frame;
class IBackground;
class IComputation;
class ProgressHandler;
class ReSample;
class Sample;
class SimulationOptions;

//! Abstract base class, holds the infrastructure to run a simulation.
//!
//! Base class of SpecularSimulation, OffspecSimulation, and DepthprobeSimulation.
//!
//! Holds an instrument and sample model. Provides the common infrastructure
//! to run a simulation: multithreading, batch processing, averaging over
//! parameter distributions, etc.
//!
//! Simulations are run, and results returned, by the function ISimulation::simulate().

class ISimulation : public INode {
public:
    ISimulation(const Sample& sample);
    ~ISimulation() override;

    ISimulation(const ISimulation&) = delete;

    //... Setters:
    void setBackground(const IBackground& bg);

    void setTerminalProgressMonitor();

    SimulationOptions& options();

    //... Executor:
    Datafield simulate(); //!< Runs a simulation, and returns the result.

#ifndef SWIG
    //... Setter:
    void subscribe(const std::function<bool(size_t)>& inform);

    //... Getters:
    std::vector<const INode*> nodeChildren() const override;

    const Sample* sample() const;
    const IBackground* background() const;
    const std::vector<ParameterDistribution>& paramDistributions() const;

    const SimulationOptions& options() const;

protected:
    ProgressHandler& progress();

    DistributionHandler& distributionHandler();

    std::vector<double> m_cache;

private:
    //... Executor:
    void runSingleSimulation(const ReSample& re_sample, size_t batch_start, size_t batch_size,
                             double weight = 1.0);

    //... Virtual executors:
    virtual void initDistributionHandler() {}

    //! Put into a clean state for running a simulation.
    virtual void prepareSimulation() {}

    //! Initializes the vector of ISimulation elements.
    virtual void initScanElementVector() {}

    //! Runs a single threaded computation for a given range of simulation elements.
    virtual void runComputation(const ReSample& re_sample, size_t iElement, double weight) = 0;

    //... Virtual getters:
    //! Forces polarized computation even in absence of sample magnetization or external fields
    virtual bool force_polarized() const = 0;

    //! Returns the number of elements this simulation needs to calculate.
    virtual size_t nElements() const = 0;

    //! Returns the number of output channels to be computed. Determines size of m_cache.
    virtual size_t nOutChannels() const { return nElements(); }

    //! Returns simulation result, based on intensity held in elements vector.
    virtual Datafield packResult() const = 0;

    //... Simulation model:
    std::unique_ptr<const Sample> m_sample;
    std::unique_ptr<const IBackground> m_background;
    std::unique_ptr<SimulationOptions> m_options;

    //... Computational auxiliaries:
    std::unique_ptr<DistributionHandler> m_distribution_handler;
    std::unique_ptr<ProgressHandler> m_progress;
#endif // SWIG
};

#endif // BORNAGAIN_SIM_SIMULATION_ISIMULATION_H
