//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Simulation/OffspecSimulation.h
//! @brief     Defines class OffspecSimulation.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_SIMULATION_OFFSPECSIMULATION_H
#define BORNAGAIN_SIM_SIMULATION_OFFSPECSIMULATION_H

#include "Base/Type/OwningVector.h"
#include "Sim/Simulation/ISimulation.h"

class Datafield;
class OffspecDetector;
class PhysicalScan;
class Pixel;
class ScanElement;

//! Off-specular scattering simulation.
//!
//! Holds an instrument and sample model.
//! Computes reflected and scattered intensity as function of incident and final glancing angle.

class OffspecSimulation : public ISimulation {
public:
    OffspecSimulation(const PhysicalScan& scan, const Sample& sample,
                      const OffspecDetector& detector);
    ~OffspecSimulation() override;

    std::string className() const final { return "OffspecSimulation"; }

#ifndef SWIG
    std::vector<const INode*> nodeChildren() const override;

    const PhysicalScan* scan() const { return m_scan.get(); }
    const OffspecDetector& detector() const { return *m_detector; }

private:
    //... Overridden executors:
    void prepareSimulation() override;

    void initScanElementVector() override;

    void runComputation(const ReSample& re_sample, size_t iElement, double weight) override;

    //... Overridden getters:
    bool force_polarized() const override;

    size_t nElements() const override;

    Datafield packResult() const override;

    //... Model components:
    std::unique_ptr<PhysicalScan> m_scan;
    std::unique_ptr<OffspecDetector> m_detector;

    //... Caches:
    OwningVector<const Pixel> m_pixels; //!< All unmasked pixels inside ROI.
    std::vector<ScanElement> m_eles;
#endif // SWIG
};

#endif // BORNAGAIN_SIM_SIMULATION_OFFSPECSIMULATION_H
