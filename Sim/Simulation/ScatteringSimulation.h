//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Simulation/ScatteringSimulation.h
//! @brief     Defines class ScatteringSimulation.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_SIMULATION_SCATTERINGSIMULATION_H
#define BORNAGAIN_SIM_SIMULATION_SCATTERINGSIMULATION_H

#include "Base/Type/OwningVector.h"
#include "Sim/Simulation/ISimulation.h"

class Beam;
class IDetector;
class IShape2D;
class Pixel;

//! GISAS simulation.
//!
//! Holds an instrument and sample model.
//! Computes the scattered intensity as function of 2D detector coordinates.

class ScatteringSimulation : public ISimulation {
public:
    ScatteringSimulation(const Beam& beam, const Sample& sample, const IDetector& detector);
    ~ScatteringSimulation() override;

    std::string className() const final { return "ScatteringSimulation"; }

    Beam& beam() { return *m_beam; }
    IDetector& detector() { return *m_detector; }

    void addParameterDistribution(ParameterDistribution::WhichParameter whichParameter,
                                  const IDistribution1D& distribution);

#ifndef SWIG
    const Beam& beam() const { return *m_beam; }
    const IDetector& detector() const { return *m_detector; }
    const IDetector* getDetector() const { return m_detector.get(); }

private:
    //... Overridden executors:
    void initDistributionHandler() override;

    void prepareSimulation() override;

    void runComputation(const ReSample& re_sample, size_t iElement, double weight) override;

    //... Overridden getters:
    bool force_polarized() const override;

    //! Returns the number of elements this simulation needs to calculate
    size_t nElements() const override;

    Datafield packResult() const override;

    //... Model components:
    std::shared_ptr<Beam> m_beam;
    std::unique_ptr<IDetector> m_detector;

    //... Caches:
    std::vector<size_t> m_active_indices; //!< The sequence of bin indices (unmasked, in ROI)
    OwningVector<const Pixel> m_pixels;   //!< All unmasked pixels inside ROI.

#endif // SWIG
};

#endif // BORNAGAIN_SIM_SIMULATION_SCATTERINGSIMULATION_H
