//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Simulation/SpecularSimulation.h
//! @brief     Defines class SpecularSimulation.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_SIMULATION_SPECULARSIMULATION_H
#define BORNAGAIN_SIM_SIMULATION_SPECULARSIMULATION_H

#include "Sim/Simulation/ISimulation.h"

class BeamScan;
class ScanElement;

//! Specular reflectometry simulation.
//!
//! Holds an instrument and sample model.
//! Computes detected intensity as function of the glancing angles alpha_i=alpha_f.

class SpecularSimulation : public ISimulation {
public:
    SpecularSimulation(const BeamScan& scan, const Sample& sample);
    ~SpecularSimulation() override;

    std::string className() const final { return "SpecularSimulation"; }

#ifndef SWIG
    //! Returns internal data handler
    const BeamScan* scan() const { return m_scan.get(); }

private:
    //... Overridden executors:
    void initScanElementVector() override;

    void runComputation(const ReSample& re_sample, size_t iElement, double weight) override;

    //... Overridden getters:
    bool force_polarized() const override;

    //! Returns the number of elements this simulation needs to calculate
    size_t nElements() const override;

    Datafield packResult() const override;

    //... Model components:
    std::unique_ptr<const BeamScan> m_scan;

    //... Caches:
    std::vector<ScanElement> m_eles;
#endif // SWIG
};

#endif // BORNAGAIN_SIM_SIMULATION_SPECULARSIMULATION_H
