//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Sim/Simulation/includeSimulations.h
//! @brief     Includes all leave classes below ISimulation
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_SIM_SIMULATION_INCLUDESIMULATIONS_H
#define BORNAGAIN_SIM_SIMULATION_INCLUDESIMULATIONS_H

#include "Sim/Simulation/DepthprobeSimulation.h"
#include "Sim/Simulation/OffspecSimulation.h"
#include "Sim/Simulation/ScatteringSimulation.h"
#include "Sim/Simulation/SpecularSimulation.h"

#endif // BORNAGAIN_SIM_SIMULATION_INCLUDESIMULATIONS_H
