//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/Functional/Consistence/CompareTwoReferences.cpp
//! @brief     Implements function compareTwoReferences for use in core consistence test.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "BATesting.h"
#include "Base/Util/Assert.h"
#include "Base/Util/PathUtil.h"
#include "Device/Data/Datafield.h"
#include "Device/IO/DiffUtil.h"
#include "Device/IO/IOFactory.h"
#include <cstdlib>
#include <iostream>
#include <memory>

namespace {

Datafield load(const std::string& name)
{
    ASSERT(!name.empty());
    const std::string path = Base::Path::jointPath(BATesting::ReferenceDir_Suite, name + ".int");
    try {
        return IO::readData2D(path, IO::Filetype2D::bornagain2D);
    } catch (const std::exception&) {
        std::cout << "Data file " << path << " could not be read.\n"
                  << "Run the pertinent Core standard test, copy the fresh data file"
                  << " to the reference directory,\n"
                  << "then rerun this test." << std::endl;
        exit(-1);
    }
}

} // namespace

int compareTwoReferences(const std::string& name0, const std::string& name1, const double limit)
{
    Datafield data0 = load(name0);
    Datafield data1 = load(name1);
    return DiffUtil::checkRelativeDifference(data0, data1, limit);
}
