//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/Functional/Fitting/SimfitTestPlan.h
//! @brief     Defines class SimfitTestPlan.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_TESTS_FUNCTIONAL_FITTING_SIMFITTESTPLAN_H
#define BORNAGAIN_TESTS_FUNCTIONAL_FITTING_SIMFITTESTPLAN_H

#include "Fit/TestEngine/MinimizerTestPlan.h"
#include "Sim/Fitting/FitTypes.h"
#include <memory>

class Datafield;
class Sample;

//! Contains all logic to construct FitObjective, setup Minimizer and check minimization results.

class SimfitTestPlan : public MinimizerTestPlan {
public:
    SimfitTestPlan(const simulation_builder_t& sim_builder, bool residual_based = false,
                   const std::vector<ParameterReference>& parameter_references = {});
    ~SimfitTestPlan() override;

    bool checkMinimizer(mumufit::Minimizer& minimizer) const override;

protected:
    virtual std::unique_ptr<FitObjective> createFitObjective() const;

    //! Creates sample for given set of fit parameters.
    virtual std::unique_ptr<Sample> createMultiLayer(const mumufit::Parameters&) const
    {
        return {};
    }
    virtual std::unique_ptr<Datafield> fakeData() const;

    simulation_builder_t m_sim_builder;
    const bool m_residual_based;
};

#endif // BORNAGAIN_TESTS_FUNCTIONAL_FITTING_SIMFITTESTPLAN_H
