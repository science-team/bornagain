//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/Functional/GUIProjectCompatibility/GUIprojects.cpp
//! @brief     Defines functions to test projects compatibility.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2023
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "BATesting.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include "GUI/View/Main/ProjectManager.h"
#include "GUI/View/Widget/AppConfig.h"
#include "Tests/GTestWrapper/google_test.h"
#include <filesystem>

class GUIProjectCompatibility : public ::testing::Test {
public:
    GUIProjectCompatibility()
    {
        gApp = std::make_unique<AppConfig>();
        gDoc = std::make_unique<ProjectDocument>();
    }
    void openProject(const std::string& project_name)
    {
        namespace fs = std::filesystem;
        const auto path = fs::path(BATesting::GuiProjectsDir) / fs::path(project_name)
                          / fs::path(project_name + ".ba");
        ProjectManager pm(nullptr);
        EXPECT_NO_THROW(pm.openProject(QString::fromStdString(path.string())));
    }
};


// TEST_F(GUIProjectCompatibility, v20_0)
//{
//     openProject("v20_0");
// }

// TEST_F(GUIProjectCompatibility, v20_2)
//{
//     openProject("v20_2");
// }

// TEST_F(GUIProjectCompatibility, v21_0)
//{
//     openProject("v21_0");
// }

// TEST_F(GUIProjectCompatibility, v21_1)
//{
//     openProject("v21_1");
// }
