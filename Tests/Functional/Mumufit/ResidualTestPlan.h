//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/Functional/Mumufit/ResidualTestPlan.h
//! @brief     Defines class ResidualTestPlan.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_TESTS_FUNCTIONAL_MUMUFIT_RESIDUALTESTPLAN_H
#define BORNAGAIN_TESTS_FUNCTIONAL_MUMUFIT_RESIDUALTESTPLAN_H

#include "Fit/Minimizer/Types.h"
#include "Fit/TestEngine/MinimizerTestPlan.h"

class ResidualTestPlan : public MinimizerTestPlan {
public:
    using test_funct_t = std::function<double(double, const std::vector<double>&)>;

    ResidualTestPlan(const std::string&, const test_funct_t& func);

    void finalizeParameters();

    fcn_residual_t residualFunction() const;
    bool checkMinimizer(mumufit::Minimizer& minimizer) const;

private:
    std::vector<double> evaluate(const std::vector<double>& pars) const;
    std::vector<double> m_xvalues;
    std::vector<double> m_data_values;
    const test_funct_t m_test_func;
};

#endif // BORNAGAIN_TESTS_FUNCTIONAL_MUMUFIT_RESIDUALTESTPLAN_H
