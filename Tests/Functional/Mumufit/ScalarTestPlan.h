//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/Functional/Mumufit/ScalarTestPlan.h
//! @brief     Defines class ScalarTestPlan.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_TESTS_FUNCTIONAL_MUMUFIT_SCALARTESTPLAN_H
#define BORNAGAIN_TESTS_FUNCTIONAL_MUMUFIT_SCALARTESTPLAN_H

#include "Fit/Minimizer/Types.h"
#include "Fit/TestEngine/MinimizerTestPlan.h"

//! Testing logic for Minimizer and scalar-type objective functions.

class ScalarTestPlan : public MinimizerTestPlan {
public:
    ScalarTestPlan(const std::string&, const scalar_function_t& func, double expected_minimum,
                   double tolerance = 0.01);

    bool checkMinimizer(mumufit::Minimizer& minimizer) const;

private:
    fcn_scalar_t scalarFunction() const;
    bool minimumAsExpected(double found_minimum, double tolerance = 0.01) const;

    const scalar_function_t m_objective_function; //!< objective function to minimize
    const double m_expected_minimum;              //!< expected function minimum
    //!< Tolerance on found minimum of objective function wrt expected.
    const double m_tolerance_on_minimum;
};

#endif // BORNAGAIN_TESTS_FUNCTIONAL_MUMUFIT_SCALARTESTPLAN_H
