############################################################################
 # BornAgain/Tests/Py/Fit/CMakeLists.txt
############################################################################

# The Python scripts in this directory contain unit tests
# of the Python API of fit related classes from C++ source directory Fit.

set(OUTPUT_DIR ${TEST_OUTPUT_DIR}/PyFit)
file(MAKE_DIRECTORY ${OUTPUT_DIR})

file(GLOB tests RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.py)

foreach(_test ${tests})
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${_test} ${OUTPUT_DIR}/${_test} COPYONLY)
    add_test(NAME Py.Fit.${_test}
        WORKING_DIRECTORY ${OUTPUT_DIR}
        COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${BA_PY_SOURCE_OUTPUT_DIR}
        ${Python3_EXECUTABLE} ${OUTPUT_DIR}/${_test})
endforeach()
