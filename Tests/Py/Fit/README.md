## Functional tests of libBornAgainFit

Collection of fitting tests for libBornAgainFit library.
Different geometries, number of fit parameters, variety
of minimizers and minimization strategies.
