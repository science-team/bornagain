"""
Testing python specific API for Minimizer related classes.
"""
import unittest
import bornagain as ba


class MinimizerAPITest(unittest.TestCase):

    def test_ParameterAttribute(self):
        """
        Testing p.value attribute
        """
        par = ba.Parameter("par", 1)
        self.assertEqual(par.value, 1)
        par.value = 42.0
        self.assertEqual(par.value, 42)

    def test_ParametersSetIterator(self):
        """
        Testing of python iterator over defined fit parameters.
        """

        pars = ba.Parameters()
        self.assertEqual(pars.size(), 0)

        pars.add(ba.Parameter("par0", 1, ba.AttLimits.limitless()))
        pars.add(ba.Parameter("par1", 2, ba.AttLimits.limitless()))
        expected_names = ["par0", "par1"]
        for index, p in enumerate(pars):
            self.assertEqual(p.name(), expected_names[index])

    def test_ParametersAdd(self):
        """
        Testing Parameters::add method
        """

        params = ba.Parameters()
        params.add("par0", 0)
        params.add("par1", 1, min=1)
        params.add("par2", 2, max=2)
        params.add("par3", 3, min=1, max=2)
        params.add("par4", 4, vary=False)

        self.assertTrue(params["par0"].limits().isLimitless())
        self.assertTrue(params["par1"].limits().isLowerLimited())
        self.assertEqual(params["par1"].limits().min(), 1)
        self.assertTrue(params["par2"].limits().isUpperLimited())
        self.assertEqual(params["par2"].limits().max(), 2)

        self.assertTrue(params["par3"].limits().isLimited())
        self.assertEqual(params["par3"].limits().min(), 1)
        self.assertEqual(params["par3"].limits().max(), 2)

        self.assertTrue(params["par4"].limits().isFixed())


if __name__ == '__main__':
    unittest.main()
