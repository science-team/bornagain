############################################################################
 # BornAgain/Tests/Py/Varia/CMakeLists.txt
############################################################################

set(OUTPUT_DIR ${TEST_OUTPUT_DIR}/PyFu)
file(MAKE_DIRECTORY ${OUTPUT_DIR})

set(infrastructure PyFuTestInfrastructure.py)
file(GLOB tests RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.py)
foreach(util ${infrastructure})
    list(REMOVE_ITEM tests ${util})
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${util} ${OUTPUT_DIR}/${util} @ONLY)
endforeach()

foreach(_test ${tests})
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${_test} ${OUTPUT_DIR}/${_test} COPYONLY)
    add_test(NAME Py.Functional.${_test}
        WORKING_DIRECTORY ${OUTPUT_DIR}
        COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${BA_PY_SOURCE_OUTPUT_DIR}
        ${Python3_EXECUTABLE} ${OUTPUT_DIR}/${_test})
endforeach()
