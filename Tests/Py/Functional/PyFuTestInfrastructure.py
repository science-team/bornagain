"""
Collection of utils for testing
"""

import bornagain as ba
from bornagain import deg #, ba_plot as bp


def get_simulation_MiniGISAS(sample, n=25):
    beam = ba.Beam(1, 0.1, 0.2 * deg)
    detector = ba.SphericalDetector(n, -2*deg, 2*deg, n, 0, 2*deg)
    return ba.ScatteringSimulation(beam, sample, detector)

def run_simulation_MiniGISAS(sample):
    return get_simulation_MiniGISAS(sample, 11).simulate()
