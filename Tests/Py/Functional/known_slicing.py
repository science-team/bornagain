"""
Check slicing mechanism for spherical particles crossing an interface,
using known decomposition of particles (as opposed to automatic slicing, tested elsewhere)
"""

import unittest
import PyFuTestInfrastructure as infrastruct
import bornagain as ba
from bornagain import deg, R3

matSubstrate = ba.RefractiveMaterial("Substrate", 3.2e-6, 3.2e-8)
matVacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
matParticle = ba.RefractiveMaterial("Ag", 1.2e-5, 5.4e-7)

R = 10.0
dz = 4.0  # shift beneath interface


def get_sample(particle_to_air=None, particle_to_substrate=None):
    """
    Returns a sample, with given particles attached to substrate or vacuum layer.
    """

    vacuum_layer = ba.Layer(matVacuum)
    if particle_to_air:
        layout = ba.ParticleLayout()
        layout.addParticle(particle_to_air)
        vacuum_layer.addLayout(layout)

    substrate = ba.Layer(matSubstrate)
    if particle_to_substrate:
        layout = ba.ParticleLayout()
        layout.addParticle(particle_to_substrate)
        substrate.addLayout(layout)

    sample = ba.Sample()
    sample.addLayer(vacuum_layer)
    sample.addLayer(substrate)
    return sample


class SlicedSpheresTest(unittest.TestCase):

    def testSphericalCupOnTopOfSubstrate(self):
        print("""
        Simulation #1: truncated sphere on top of substrate.
        Simulation #2: sphere crossing the interface.
        Both particles are made of same material as substrate.
        Same scattering expected from both sample models.
        """, flush=True)

        # truncated sphere (dz removed from bottom) on top of substrate
        SphericalSegment = ba.Particle(matSubstrate,
                                      ba.SphericalSegment(R, 0, dz))

        # same without truncation, sphere penetrating into substrate layer
        sphere = ba.Particle(matSubstrate, ba.Sphere(R))
        sphere.translate(0, 0, -dz)

        d1 = infrastruct.run_simulation_MiniGISAS(get_sample(SphericalSegment))
        d2 = infrastruct.run_simulation_MiniGISAS(get_sample(sphere))
        self.assertTrue(ba.checkRelativeDifference(d1, d2, 1e-15))

    def testSphericalLacuneInSubstrate(self):
        print("""
        Similar to previous. Truncated sphere and sphere are made of vacuum material.
        From scattering point of view, both cases should look like an vacuum lacune in substrate.
        """, flush=True)

        # Sphere truncated from top. Intended to go below interface.
        SphericalSegment = ba.Particle(matVacuum,
                                      ba.SphericalSegment(R, dz, 0))
        SphericalSegment.translate(0, 0, -dz)

        # sphere crossing interface to look like truncated sphere above
        sphere = ba.Particle(matVacuum, ba.Sphere(R))
        sphere.translate(0, 0, -dz)

        d1 = infrastruct.run_simulation_MiniGISAS(get_sample(SphericalSegment))
        d2 = infrastruct.run_simulation_MiniGISAS(get_sample(sphere))
        self.assertTrue(ba.checkRelativeDifference(d1, d2, 1e-15))

    def testSpheresCrossingInterface(self):
        print("""
        Same particle in same position, but attached to two different layers.
        Of course, results shall be identical.
        """, flush=True)

        # Sphere intended for vacuum layer and crossing interface
        sphere1 = ba.Particle(matParticle, ba.Sphere(R))
        sphere1.translate(0, 0, -dz)

        # Sphere intended for substrate layer and crossing interface
        sphere2 = ba.Particle(matParticle, ba.Sphere(R))
        sphere2.translate(0, 0, -dz)

        d1 = infrastruct.run_simulation_MiniGISAS(get_sample(sphere1))
        d2 = infrastruct.run_simulation_MiniGISAS(get_sample(sphere2))
        self.assertTrue(ba.checkRelativeDifference(d1, d2, 1e-14))


class SlicedSpheresTest(unittest.TestCase):

    def get_compound(self, top_material, bottom_material):
        """
        Returns particle compound representing sphere made of two different materials.
        Origin of new object is at the bottom of resulting sphere.
        """

        topCup = ba.Particle(top_material,
                             ba.SphericalSegment(R, 0, dz))
        bottomCup = ba.Particle(bottom_material,
                                ba.SphericalSegment(R, R * 2 - dz, 0))

        #  origin of resulting sphere will be at the bottom
        result = ba.Compound()
        result.addComponent(topCup, R3(0, 0, dz))
        result.addComponent(bottomCup, R3(0, 0, 0))

        return result

    def get_compound_v2(self, top_material, bottom_material):
        """
        Returns particle compound representing sphere made of two different materials.
        Alternative to previous method.
        Rotation is used to get bottom part
        """

        topCup = ba.Particle(top_material,
                             ba.SphericalSegment(R, 0, dz))
        bottomCup = ba.Particle(bottom_material, ba.SphericalSegment(R, 0, R * 2 - dz))
        bottomCup.rotate(ba.RotationX(180 * deg))

        #  origin of resulting sphere will be at the bottom
        result = ba.Compound()
        result.addComponent(topCup, R3(0, 0, dz))
        result.addComponent(bottomCup, R3(0, 0, dz))

        return result

    def testCompound(self):
        print("""
        Compares two simulation intended to  provide identical results.
        Simulation #1: spherical particle on top of substrate
        Simulation #2: spherical compound on top of substrate, where
        top and bottom are made of same material.
        """, flush=True)

        sphere = ba.Particle(matParticle, ba.Sphere(R))
        compound = self.get_compound(matParticle, matParticle)

        d1 = infrastruct.run_simulation_MiniGISAS(get_sample(sphere))
        d2 = infrastruct.run_simulation_MiniGISAS(get_sample(compound))
        self.assertTrue(ba.checkRelativeDifference(d1, d2, 2e-14))

    def testCompoundBuilds(self):
        print("""
        Compares two simulation intended to  provide identical results.
        Simulation #1: spherical particle compound on top of substrate
        Simulation #2: same, but made using rotation
        """, flush=True)

        compound1 = self.get_compound(matParticle, matSubstrate)
        compound2 = self.get_compound_v2(matParticle, matSubstrate)

        d1 = infrastruct.run_simulation_MiniGISAS(get_sample(compound1))
        d2 = infrastruct.run_simulation_MiniGISAS(get_sample(compound2))
        self.assertTrue(ba.checkRelativeDifference(d1, d2, 1e-14))

#    def testSlicedCompound(self):
#        print("""
#        Compares two simulation intended to  provide identical results.
#        Simulation #1: spherical particle on top of substrate.
#        Simulation #2: spherical compound on top of substrate,
#                       where top and bottom are made of same material.
#        Both particles are inserted in vacuum layer with shift to go below interface
#        """, flush=True)

#        shift = 3.0

#        sphere = ba.Particle(matParticle, ba.Sphere(R))
#        sphere.translate(0, 0, -shift)

#        compound = self.get_compound(matParticle, matParticle)
#        compound.translate(0, 0, -shift)

#        d1 = infrastruct.run_simulation_MiniGISAS(get_sample(sphere))
#        d2 = infrastruct.run_simulation_MiniGISAS(get_sample(compound))
#        self.assertTrue(ba.checkRelativeDifference(d1, d2, 1e-14))

#    def testSphericalCupOnTopOfSubstrate(self):
#        print("""
#        Compares two simulation intended to  provide identical results.
#        Simulation #1: truncated sphere on top of substrate.
#        Simulation #2: spherical particle compound crossing the interface.
#        Bottom part of compound is made from substrate material.
#        both cases should look like a truncated sphere on top of substrate.
#        """, flush=True)

#        # truncated sphere on top of substrate with height 16nm
#        SphericalSegment = ba.Particle(matParticle,
#                                      ba.SphericalSegment(R, 0, dz))

#        # Particle compound, top part made of same material, as particle.
#        # Bottom part made of same material as substrate.
#        compound = self.get_compound(matParticle, matSubstrate)
#        compound.translate(0, 0, -dz)

#        d1 = infrastruct.run_simulation_MiniGISAS(get_sample(SphericalSegment))
#        d2 = infrastruct.run_simulation_MiniGISAS(get_sample(compound))
#        self.assertTrue(ba.checkRelativeDifference(d1, d2, 1e-15))


if __name__ == '__main__':
    unittest.main()
