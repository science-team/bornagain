"""
BornAgain functional test.
Test of rotation/position of particle composition.
The composition consists of two boxes made of the same material.
It is compared against reference single box made of the same material.
Composition might be rotated to get reference shape.
Both, reference box and composition are placed in the center of
middle layer of 3 layers system.
"""

import unittest
import PyFuTestInfrastructure as infrastruct
from bornagain import *

layer_thickness = 100.0
com_length = 50.0
com_width = 20.0
com_height = 10.0
particle_material = RefractiveMaterial("Ag", 1.245e-5, 5.419e-7)


class TransformBoxCompositionTest(unittest.TestCase):

    def get_sample(self, particle):
        mAmbience = RefractiveMaterial("Vacuum", 0, 0)
        mMiddle = RefractiveMaterial("Teflon", 2.900e-6, 6.019e-9)
        mSubstrate = RefractiveMaterial("Substrate", 3.212e-6, 3.244e-8)

        layout = ParticleLayout()
        layout.addParticle(particle)

        vacuum_layer = Layer(mAmbience)
        middle_layer = Layer(mMiddle, layer_thickness)
        # vacuum_layer.addLayout(layout)
        middle_layer.addLayout(layout)
        substrate = Layer(mSubstrate)

        sample = Sample()
        sample.addLayer(vacuum_layer)
        sample.addLayer(middle_layer)
        sample.addLayer(substrate)
        return sample

    def get_result(self, particle):
        sample = self.get_sample(particle)
        simulation = infrastruct.get_simulation_MiniGISAS(sample)
        return simulation.simulate()

    def test_BoxComposition(self):
        """
        Compares simple box with the one composed from two smaller boxes
        of the same material to get same size
        """
        # reference box
        length = 50.0
        width = 20.0
        height = 10.0
        particle = Particle(particle_material, Box(length, width, height))
        particle.translate(
            R3(0, 0, -layer_thickness / 2.0 - height / 2))

        reference = self.get_result(particle)

        # composition
        box = Particle(particle_material,
                       Box(com_length / 2, com_width, com_height))
        composition = Compound()
        # composition = Compound(box, positions)
        composition.addComponent(box, R3(0, 0, 0))
        composition.addComponent(box, R3(com_length / 2, 0, 0))
        composition.translate(
            R3(0, 0, -layer_thickness / 2.0 - com_height / 2))

        data = self.get_result(composition)

        self.assertTrue(checkRelativeDifference(data, reference, 1e-10))

    def test_BoxCompositionRotateX(self):
        """
        Compares simple box with the one composed from two smaller boxes
        of the same material to get same size.
        Composed box is rotated around X, to get same orientation as the original box.
        """
        # reference box
        length = 50.0
        width = 10.0
        height = 20.0
        particle = Particle(particle_material, Box(length, width, height))
        particle.translate(
            R3(0, 0, -layer_thickness / 2.0 - height / 2))

        reference = self.get_result(particle)

        # composition
        box = Particle(particle_material,
                       Box(com_length / 2, com_width, com_height))
        composition = Compound()
        composition.addComponent(box, R3(0, 0, 0))
        composition.addComponent(box, R3(com_length / 2, 0, 0))
        composition.rotate(RotationX(90 * deg))
        composition.translate(R3(0, 0, -layer_thickness / 2.))

        data = self.get_result(composition)

        self.assertTrue(checkRelativeDifference(data, reference, 1e-10))

    def test_BoxCompositionRotateY(self):
        """
        Compares simple box with the one composed from two smaller boxes
        of the same material to get same size.
        Composed box is rotated around Y, to get same orientation as the original box.
        """
        # reference box
        length = 10.0
        width = 20.0
        height = 50.0
        particle = Particle(particle_material, Box(length, width, height))
        particle.translate(
            R3(0, 0, -layer_thickness / 2.0 - height / 2))

        reference = self.get_result(particle)

        # composition
        box = Particle(particle_material,
                       Box(com_length / 2, com_width, com_height))
        composition = Compound()
        composition.addComponent(box, R3(0, 0, 0))
        composition.addComponent(box, R3(com_length / 2, 0, 0))
        composition.rotate(RotationY(90 * deg))
        composition.translate(
            R3(0, 0, -layer_thickness / 2. + com_length / 4.))

        data = self.get_result(composition)

        self.assertTrue(checkRelativeDifference(data, reference, 1e-10))

    def test_BoxCompositionRotateZ(self):
        """
        Compares simple box with the one composed from two smaller boxes
        of the same material to get same size.
        Composed box is rotated around Z, to get same orientation as the original box.
        """
        # reference box
        length = 20.0
        width = 50.0
        height = 10.0
        particle = Particle(particle_material, Box(length, width, height))
        particle.translate(
            R3(0, 0, -layer_thickness / 2.0 - height / 2))

        reference = self.get_result(particle)

        # composition
        box = Particle(particle_material,
                       Box(com_length / 2, com_width, com_height))
        composition = Compound()
        composition.addComponent(box, R3(0, 0, 0))
        composition.addComponent(box, R3(com_length / 2, 0, 0))
        composition.rotate(RotationZ(90 * deg))
        composition.translate(
            R3(0, 0, -layer_thickness / 2.0 - com_height / 2))

        data = self.get_result(composition)

        self.assertTrue(checkRelativeDifference(data, reference, 1e-10))

    def test_BoxCompositionRotateZandY(self):
        """
        Compares simple box with the one composed from two smaller boxes
        of the same material to get same size.
        Composed box is rotated around Y, to get same orientation as the original box.
        """
        # reference box
        length = 10.0
        width = 50.0
        height = 20.0
        particle = Particle(particle_material, Box(length, width, height))
        particle.translate(
            R3(0, 0, -layer_thickness / 2.0 - height / 2))

        reference = self.get_result(particle)

        # composition
        box = Particle(particle_material,
                       Box(com_length / 2, com_width, com_height))
        composition = Compound()
        composition.addComponent(box, R3(0, 0, 0))
        composition.addComponent(box, R3(com_length / 2, 0, 0))
        composition.rotate(RotationZ(90 * deg))
        composition.rotate(RotationY(90 * deg))
        composition.translate(R3(0, 0, -layer_thickness / 2.))

        data = self.get_result(composition)

        self.assertTrue(checkRelativeDifference(data, reference, 1e-10))

    def test_BoxStackComposition(self):
        """
        2 different boxes are first rotated and then combined into the composition.
        Composition is then rotated.
        Composition is compared against reference box.
        """
        # reference box
        length = 10.0
        width = 20.0
        height = 50.0
        particle = Particle(particle_material, Box(length, width, height))
        particle.translate(
            R3(0, 0, -layer_thickness / 2.0 - height / 2))
        reference = self.get_result(particle)

        # composition
        composition = Compound()

        # box1 (20,50,5), rotatedZ
        box1_length = 20.0
        box1_width = 50.0
        box1_height = 5.0
        box1 = Particle(particle_material,
                        Box(box1_length, box1_width, box1_height))
        box1.rotate(RotationZ(90 * deg))

        # box2 (5,20,50), rotatedY
        box2_length = 5.0
        box2_width = 20.0
        box2_height = 50.0
        box2 = Particle(particle_material,
                        Box(box2_length, box2_width, box2_height))
        box2.rotate(RotationY(90 * deg))
        box2.translate(R3(-box2_height / 2, 0, box2_length / 2))

        composition.addComponent(box1, R3(0, 0, 0))
        composition.addComponent(box2, R3(0, 0, box1_height))
        composition.rotate(RotationY(90 * deg))
        composition.translate(R3(0, 0, -layer_thickness / 2.))

        data = self.get_result(composition)

        self.assertTrue(checkRelativeDifference(data, reference, 1e-10))


if __name__ == '__main__':
    unittest.main()
