"""
BornAgain functional test.
Test of rotation/position of core shell particle.
Reference CoreShell particle is compared against the test CoreShell,
which has different dimensions but rotated/translated to be like the reference one.
Particles are placed in the center of middle layer.
"""

import unittest
import PyFuTestInfrastructure as infrastruct
from bornagain import *

mAmbience = RefractiveMaterial("Vacuum", 0, 0)
mSubstrate = RefractiveMaterial("Substrate", 3e-6, 3e-8)
mMiddle = RefractiveMaterial("Teflon", 2.9e-6, 6e-9)
mCore = RefractiveMaterial("Ag", 1.2e-5, 5e-7)
mShell = RefractiveMaterial("AgO2", 8.6e-6, 3.4e-7)

layer_thickness = 100.0


class TransformCoreShellBoxTest(unittest.TestCase):

    def get_sample(self, particle):

        layout = ParticleLayout()
        layout.addParticle(particle)

        vacuum_layer = Layer(mAmbience)
        middle_layer = Layer(mMiddle, layer_thickness)
        middle_layer.addLayout(layout)
        substrate = Layer(mSubstrate)

        sample = Sample()
        sample.addLayer(vacuum_layer)
        sample.addLayer(middle_layer)
        sample.addLayer(substrate)
        return sample

    def get_result(self, particle):
        sample = self.get_sample(particle)
        simulation = infrastruct.get_simulation_MiniGISAS(sample)
        return simulation.simulate()

    def test_SameMaterialCoreShellBox(self):
        """
        Box particle placed in the center of the middle layer is compared against
        CoreShell particle, where core and shell are made of the same material.
        """

        shell_length = 50.0
        shell_width = 20.0
        shell_height = 10.0
        particle = Particle(mCore, Box(shell_length, shell_width,
                                       shell_height))
        particle.translate(
            R3(0, 0, -layer_thickness / 2.0 - shell_height / 2))

        reference = self.get_result(particle)

        core_length = shell_length / 2.0
        core_width = shell_width / 2.0
        core_height = shell_height / 2.0
        shell = Particle(mCore, Box(shell_length, shell_width, shell_height))
        core = Particle(mCore, Box(core_length, core_width, core_height))
        core.translate(R3(0, 0, (shell_height - core_height) / 2))
        coreshell = CoreAndShell(core, shell)
        coreshell.translate(
            R3(0, 0, -layer_thickness / 2.0 - shell_height / 2))

        data = self.get_result(coreshell)

        self.assertTrue(checkRelativeDifference(data, reference, 1e-10))

    def test_CoreShellBoxRotateZ(self):
        """
        Two CoreShell particles are compared against each other. Reference CoreShell
        particle has dimensions (20,50,10) and is placed in the center of middle layer.
        Second CoreShell particle has dimensions (50,20,10) with one rotationZ to be
        like the reference one.
        """

        # building reference CoreShell particle
        shell_length = 20.0
        shell_width = 50.0
        shell_height = 10.0
        core_length = shell_length / 2.0
        core_width = shell_width / 2.0
        core_height = shell_height / 2.0

        core_ref = Particle(mCore, Box(core_length, core_width, core_height))
        core_ref.translate(R3(0, 0, (shell_height - core_height) / 2))
        shell_ref = Particle(mShell,
                             Box(shell_length, shell_width, shell_height))
        coreshell_ref = CoreAndShell(core_ref, shell_ref)
        coreshell_ref.translate(
            R3(0, 0, -layer_thickness / 2.0 -
               shell_height / 2))  # center of coreshell in center of the layer

        reference = self.get_result(coreshell_ref)

        # building second CoreShell particle
        shell_length = 50.0
        shell_width = 20.0
        shell_height = 10.0
        core_length = shell_length / 2.0
        core_width = shell_width / 2.0
        core_height = shell_height / 2.0

        core = Particle(mCore, Box(core_length, core_width, core_height))
        core.translate(R3(0, 0, (shell_height - core_height) / 2))
        shell = Particle(mShell, Box(shell_length, shell_width, shell_height))
        coreshell = CoreAndShell(core, shell)
        coreshell.rotate(RotationZ(90 * deg))
        coreshell.translate(
            R3(0, 0, -layer_thickness / 2.0 - shell_height /
               2))  # center of coreshell  in center of the layer

        data = self.get_result(coreshell)

        self.assertTrue(checkRelativeDifference(data, reference, 1e-10))

    def test_CoreShellBoxRotateY(self):
        """
        Two CoreShell particles are compared against each other. Reference CoreShell
        particle has dimensions (10,20,50) and is placed in the center of middle layer.
        Second CoreShell particle has dimensions (50,20,10) with one rotationZ to be
        like the reference one.
        """
        # building reference CoreShell particle
        shell_length = 10.0
        shell_width = 20.0
        shell_height = 50.0
        core_length = shell_length / 2.0
        core_width = shell_width / 2.0
        core_height = shell_height / 2.0

        core_ref = Particle(mCore, Box(core_length, core_width, core_height))
        core_ref.translate(R3(0, 0, (shell_height - core_height) / 2))
        shell_ref = Particle(mShell,
                             Box(shell_length, shell_width, shell_height))
        coreshell_ref = CoreAndShell(core_ref, shell_ref)
        coreshell_ref.translate(
            R3(0, 0, -layer_thickness / 2.0 -
               shell_height / 2))  # center of coreshell in center of the layer

        reference = self.get_result(coreshell_ref)

        # building second CoreShell particle
        shell_length = 50.0
        shell_width = 20.0
        shell_height = 10.0
        core_length = shell_length / 2.0
        core_width = shell_width / 2.0
        core_height = shell_height / 2.0

        core = Particle(mCore, Box(core_length, core_width, core_height))
        core.translate(R3(0, 0, (shell_height - core_height) / 2))
        shell = Particle(mShell, Box(shell_length, shell_width, shell_height))
        coreshell = CoreAndShell(core, shell)
        coreshell.rotate(RotationY(90. * deg))
        coreshell.translate(
            R3(0, 0, -layer_thickness /
               2))  # center of coreshell  in center of the layer

        data = self.get_result(coreshell)

        self.assertTrue(checkRelativeDifference(data, reference, 1e-10))

    def test_CoreShellBoxRotateZandY(self):
        """
        Two CoreShell particles are compared against each other. Reference CoreShell
        particle has dimensions (10,50,20) and is placed in the center of middle layer.
        Second CoreShell particle has dimensions (50,20,10) with two rotations to be
        like the reference one.
        """

        # building reference CoreShell particle
        shell_length = 10.0
        shell_width = 50.0
        shell_height = 20.0
        core_length = shell_length / 2.0
        core_width = shell_width / 2.0
        core_height = shell_height / 2.0

        core_ref = Particle(mCore, Box(core_length, core_width, core_height))
        core_ref.translate(R3(0, 0, (shell_height - core_height) / 2))
        shell_ref = Particle(mShell,
                             Box(shell_length, shell_width, shell_height))
        coreshell_ref = CoreAndShell(core_ref, shell_ref)
        coreshell_ref.translate(
            R3(0, 0, -layer_thickness / 2.0 -
               shell_height / 2))  # center of coreshell in center of the layer

        reference = self.get_result(coreshell_ref)

        # building second CoreShell particle
        shell_length = 50.0
        shell_width = 20.0
        shell_height = 10.0
        core_length = shell_length / 2.0
        core_width = shell_width / 2.0
        core_height = shell_height / 2.0

        core = Particle(mCore, Box(core_length, core_width, core_height))
        core.translate(R3(0, 0, (shell_height - core_height) / 2))
        shell = Particle(mShell, Box(shell_length, shell_width, shell_height))
        coreshell = CoreAndShell(core, shell)
        coreshell.rotate(RotationZ(90 * deg))
        coreshell.rotate(RotationY(90 * deg))
        # rotation changes reference point, which now coincide with center of the volume
        coreshell.translate(
            R3(0, 0, -layer_thickness /
               2))  # center of coreshell  in center of the layer

        data = self.get_result(coreshell)

        self.assertTrue(checkRelativeDifference(data, reference, 1e-10))


if __name__ == '__main__':
    unittest.main()
