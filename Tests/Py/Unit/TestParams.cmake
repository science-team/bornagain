# Display the parameters used for testing phase
add_test(NAME TestParams
    COMMAND ${CMAKE_COMMAND} -E echo
    "CMAKE_SOURCE_DIR = '${CMAKE_SOURCE_DIR}'\n"
    "CMAKE_LIBRARY_OUTPUT_DIRECTORY = '${CMAKE_LIBRARY_OUTPUT_DIRECTORY}'\n"
    "OUTPUT_DIR = '${OUTPUT_DIR}'\n"
    "TEST_OUTPUT_DIR_PY_PERSIST = '${TEST_OUTPUT_DIR_PY_PERSIST}'\n"
    "FIG_DIR = '${FIG_DIR}'\n"
    "EXAMPLES_PUBL_DIR = '${EXAMPLES_PUBL_DIR}'\n"
    "Python3_EXECUTABLE = '${Python3_EXECUTABLE}'\n"
    "Python3_LIBRARIES = '${Python3_LIBRARIES}'\n"
    "Python3_INCLUDE_DIRS = '${Python3_INCLUDE_DIRS}'"
)
