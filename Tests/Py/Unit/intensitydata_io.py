# Functional test: tests of IO operations with the IntensityData object

import unittest, numpy
import bornagain as ba
from bornagain.numpyutil import Arrayf64Converter as dac


class IOTest(unittest.TestCase):
    """
    Test serialization of IntensityData
    """
    def setUp(self):
        self.arr = numpy.array([[0, 1, 2.], [3, 4, 5.]])
        numpy.savetxt('intensitydata.txt', self.arr)
        self.dat = ba.readData2D("intensitydata.txt")

    def test_1(self):
        print()
        print("Test: numpy array -> text file -> datafield -> npArray")
        print("input array: ", self.arr)
        print("read back as datafield: ", self.dat.flatVector())

        arr1 = dac.asNpArray(self.dat.dataArray())
        print("exported to NumPy ", arr1, flush=True)
        self.assertTrue(numpy.array_equal(arr1, self.arr))

    def test_2(self):
        print()
        print("Test: numpy array -> text file -> datafield -> flatVector -> numpy.array")
        print("input array: ", self.arr)
        print("read back as datafield: ", self.dat.flatVector())

        arr2 = numpy.array(self.dat.flatVector())
        arr2.shape = self.arr.shape
        print("converted to NumPy ", arr2, flush=True)
        self.assertTrue(numpy.array_equal(arr2, self.arr))

if __name__ == '__main__':
    unittest.main()
