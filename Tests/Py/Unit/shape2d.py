import unittest
import numpy
import bornagain as ba


class Shape2DTest(unittest.TestCase):

    def test_constructPolygonFromList(self):
        """
        Testing construction of polygon from lists
        """

        # initializing from list
        p = ba.Polygon([[-1,-.5], [2,-.5], [2,1.5], [-1,1.5]])
        self.assertTrue(p.contains(-0.75, -0.25))
        self.assertTrue(p.contains(1.5, 1))

    def test_constructPolygonFromNumpy(self):
        """
        Testing construction of polygon from two Numpy arrays
        """

        # initialization from numpy array
        points = numpy.array([[-1, -0.5], [2, -0.5], [2, 1.5], [-1, 1.5]])

        p = ba.Polygon(points)
        self.assertTrue(p.contains(-0.75, -0.25))
        self.assertTrue(p.contains(1.5, 1))


if __name__ == '__main__':
    unittest.main()
