//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/SimFactory/MakeSimulations.h
//! @brief     Declares simulation builders for standard tests.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifdef SWIG
#error no need to expose this header to Swig
#endif // SWIG
#ifndef BORNAGAIN_TESTS_SIMFACTORY_MAKESIMULATIONS_H
#define BORNAGAIN_TESTS_SIMFACTORY_MAKESIMULATIONS_H

#include <memory>
#include <string>

class BeamScan;
class DepthprobeSimulation;
class OffspecSimulation;
class Sample;
class ScatteringSimulation;
class SpecularSimulation;

//! Simulations for standard tests.

namespace test::makeSimulation {

std::unique_ptr<ScatteringSimulation> MiniGISAS(const Sample& sample);
std::unique_ptr<ScatteringSimulation> MiniGISAS_v2(const Sample& sample);
std::unique_ptr<ScatteringSimulation> MiniZPolarizedGISAS(const Sample& sample,
                                                          const std::string& polCase);
std::unique_ptr<ScatteringSimulation> BasicGISAS(const Sample& sample);
std::unique_ptr<ScatteringSimulation> SpinflipGISAS(const Sample& sample);
std::unique_ptr<ScatteringSimulation> MiniGISASBeamDivergence(const Sample& sample);
std::unique_ptr<ScatteringSimulation> MiniGISASDetectorResolution(const Sample& sample);
std::unique_ptr<ScatteringSimulation> MiniGISASSpecularPeak(const Sample& sample);
std::unique_ptr<ScatteringSimulation> GISASWithMasks(const Sample& sample);
std::unique_ptr<ScatteringSimulation> MaxiGISAS(const Sample& sample);
std::unique_ptr<ScatteringSimulation> MaxiGISAS00(const Sample& sample);
std::unique_ptr<ScatteringSimulation> MiniGISASMonteCarlo(const Sample& sample);
std::unique_ptr<ScatteringSimulation> SphericalDetWithRoi(const Sample& sample);
std::unique_ptr<ScatteringSimulation> RectDetWithRoi(const Sample& sample);
std::unique_ptr<ScatteringSimulation> MiniGISASFit(const Sample& sample);
std::unique_ptr<ScatteringSimulation> ExtraLongWavelengthGISAS(const Sample& sample);

std::unique_ptr<OffspecSimulation> MiniOffspec(const Sample& sample);

BeamScan* BasicSpecularScan(bool vsQ);
std::unique_ptr<SpecularSimulation> BasicSpecular(const Sample& sample, bool vsQ,
                                                  double intensity = 1.);
std::unique_ptr<SpecularSimulation> BasicYPolarizedSpecular(const Sample& sample,
                                                            const std::string& polCase, bool vsQ);
std::unique_ptr<SpecularSimulation> SpecularWithGaussianBeam(const Sample& sample);
std::unique_ptr<SpecularSimulation> SpecularWithSquareBeam(const Sample& sample);
std::unique_ptr<SpecularSimulation> SpecularDivergentBeam(const Sample& sample);
std::unique_ptr<SpecularSimulation> TOFRWithRelativeResolution(const Sample& sample);
std::unique_ptr<SpecularSimulation> TOFRWithPointwiseResolution(const Sample& sample);

std::unique_ptr<DepthprobeSimulation> BasicDepthprobe(const Sample& sample);
std::unique_ptr<DepthprobeSimulation> BasicDepthprobeShiftedZ(const Sample& sample, double shift);

} // namespace test::makeSimulation

#endif // BORNAGAIN_TESTS_SIMFACTORY_MAKESIMULATIONS_H
