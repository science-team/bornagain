############################################################################
 # BornAgain/Tests/Suite/CMakeLists.txt (called from top-level CMakeLists.txt)
############################################################################

include_directories(${CMAKE_SOURCE_DIR}/Tests/Functional/TestMachinery)

add_subdirectory(Persist)
if(BORNAGAIN_PYTHON)
    add_subdirectory(Py)
endif()
if(BA_GUI)
    add_subdirectory(GUI)
    add_subdirectory(XML)
endif()
