//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/Suite/Common/RunTest.cpp
//! @brief     Implements function run for use in standard tests.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "Tests/Suite/Common/RunTest.h"
#include "Tests/Suite/Common/TestSuite.h" // provides F_TEST macros to be executed by gtest
#include <iostream>

//! This function, called from run, has different implementations in Core|Py|Gui tests:
bool checkSimulation(const std::string& name, ISimulation& direct_simulation, double precision,
                     double snr, int allowed_outliers);

//! This function is called through EXPECT_TRUE macros in TestSuite.h.
//! It runs the given simulation.
//! It then compares with reference data, or with results from Py or GUI runs.

int runTest(const std::string& test_name, ISimulation& simulation, const double limit)
{
    std::cout << "run std test " << test_name << std::endl;

    return checkSimulation(test_name, simulation, limit, limit, 30);
}
