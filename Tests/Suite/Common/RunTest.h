//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/Suite/Common/RunTest.h
//! @brief     Declares function run for use in standard tests.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_TESTS_SUITE_COMMON_RUNTEST_H
#define BORNAGAIN_TESTS_SUITE_COMMON_RUNTEST_H

#include <string>

class ISimulation;

int runTest(const std::string& test_name, ISimulation& simulation, double limit);

#endif // BORNAGAIN_TESTS_SUITE_COMMON_RUNTEST_H
