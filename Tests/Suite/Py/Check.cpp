//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/Suite/Py/Check.cpp
//! @brief     Implements function checkSimulation for Python standard test.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "BABuild.h"
#include "BATesting.h"
#include "Base/Util/PathUtil.h"
#include "Device/Data/Datafield.h"
#include "Device/IO/DiffUtil.h"
#include "Device/IO/IOFactory.h"
#include "Sim/Export/ExportToPython.h"
#include "Sim/Simulation/ISimulation.h"
#include <filesystem>
#include <fstream>
#include <iostream>

namespace fs = std::filesystem;

namespace {

Datafield domainData(const std::string& test_name, const ISimulation& direct_simulation)
{
    const auto dir = fs::path(BATesting::TestOutDir) / fs::path("Suite") / fs::path("Py");
    Base::Path::createDirectories(dir.string());

    const std::string output_path = (dir / fs::path(test_name + ".ref.int")).string();
    std::remove(output_path.c_str());
    std::cout << "- removed old output " << output_path << std::endl;

    // Generate Python script
    const std::string pyscript = (dir / fs::path(test_name + ".py")).string();
    std::ofstream f(pyscript);
    f << Py::Export::simulationSaveCode(direct_simulation, output_path);
    f.close();
    std::cout << "- wrote Python script " << pyscript << std::endl;

    // Run Python script
#ifndef _WIN32
    const std::string sys_command = std::string("PYTHONPATH=") + BABuild::pythonPackDir() + " "
                                    + std::string("NOSHOW=TRUE") + " " + BABuild::pythonExecutable()
                                    + " -B " + pyscript;
#else
    const std::string sys_command = std::string("set PYTHONPATH=") + BABuild::pythonPackDir()
                                    + " & " + std::string("set NOSHOW=TRUE") + " & \""
                                    + BABuild::pythonExecutable() + "\" -B " + pyscript;
#endif
    std::cout << "- system call: " << sys_command << std::endl;
    int err = std::system(sys_command.c_str());
    std::cout << "- system call returned " << err << " (0x" << std::hex << err << ")" << std::endl;
    if (err)
        throw std::runtime_error("Exported Python script did not execute properly");

    return IO::readData2D(output_path, IO::Filetype2D::bornagain2D);
}

} // namespace

//! Run simulation directly (in C+ core) and through Python export, and compare results.

bool checkSimulation(const std::string& name, ISimulation& direct_simulation, double precision,
                     double snr, int allowed_outliers)
{
    std::cout << "PySuite test: checkSimulation(" << name << ")" << std::endl;

    const Datafield domain_data = ::domainData(name, direct_simulation);
    std::cout << "- got domain data" << std::endl;
    const Datafield ref_data = direct_simulation.simulate();
    std::cout << "- ran simulation" << std::endl;

    return DiffUtil::checkConsistence(domain_data, ref_data, precision, snr, allowed_outliers);
}
