#include "Base/Axis/Coordinate.h"

#include "BATesting.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(Coordinate, Label)
{
    const Coordinate c1("bla");
    EXPECT_EQ(c1.name(), "bla");
    EXPECT_EQ(c1.unit(), "");

    const Coordinate c2("bla ()");
    EXPECT_EQ(c2.name(), "bla");
    EXPECT_EQ(c2.unit(), "");

    const Coordinate c3("bla (deg)");
    EXPECT_EQ(c3.name(), "bla");
    EXPECT_EQ(c3.unit(), "deg");

    const Coordinate c4("bla(deg)"); // bad taste but nonetheless supported
    EXPECT_EQ(c4.name(), "bla");
    EXPECT_EQ(c4.unit(), "deg");

    const Coordinate c5("(deg)"); // nonsense but nonetheless supported
    EXPECT_EQ(c5.name(), "");
    EXPECT_EQ(c5.unit(), "deg");

    EXPECT_THROW(Coordinate("bla deg)"), std::runtime_error);
}
