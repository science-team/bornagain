#include "Base/Axis/MakeScale.h"

#include "Base/Axis/Scale.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(EquiDivision, IndexedAccessor)
{
    Scale a1 = EquiDivision("length", 100, 0.0, 10.0);
    EXPECT_EQ(100u, a1.size());
    EXPECT_EQ(0.0, a1.min());
    EXPECT_EQ(10.0, a1.max());
    EXPECT_DOUBLE_EQ(0.05, a1.binCenter(0));
    EXPECT_DOUBLE_EQ(0.15, a1.binCenter(1));
    EXPECT_DOUBLE_EQ(6.55, a1.binCenter(65));
    EXPECT_DOUBLE_EQ(9.95, a1.binCenter(99));

    Scale a2 = EquiDivision("name", 3, -1.5, 1.5);
    EXPECT_DOUBLE_EQ(-1.0, a2.binCenter(0));
    EXPECT_DOUBLE_EQ(0.0, a2.binCenter(1));
    EXPECT_DOUBLE_EQ(1.0, a2.binCenter(2));
    EXPECT_THROW(a2.binCenter(3), std::out_of_range);
}

TEST(EquiDivision, VectorOfUnitLength)
{
    Scale axis = EquiDivision("name", 1, 1.0, 2.0);
    EXPECT_EQ(1u, axis.size());
    EXPECT_EQ(double(1.0), axis.min());
    EXPECT_EQ(double(2.0), axis.max());
    EXPECT_EQ(1.5, axis.binCenter(0));
}

TEST(EquiDivision, FindClosestIndex)
{
    Scale v1 = EquiDivision("name", 2, 0.0, 1.0);
    EXPECT_EQ(size_t(2), v1.size());
    EXPECT_EQ(size_t(0), v1.closestIndex(0.0));
    EXPECT_EQ(size_t(0), v1.closestIndex(0.25));
    EXPECT_EQ(size_t(1), v1.closestIndex(0.5));
    EXPECT_EQ(size_t(1), v1.closestIndex(0.6));
    //    EXPECT_THROW( v1.closestIndex(1.0), std::runtime_error);
    EXPECT_EQ(size_t(1), v1.closestIndex(1.0));

    Scale v2 = EquiDivision("name", 3, -1.5, 1.5);
    EXPECT_EQ(size_t(0), v2.closestIndex(-1.5));
    EXPECT_EQ(size_t(0), v2.closestIndex(-1.0));
    EXPECT_EQ(size_t(1), v2.closestIndex(-0.5));
    EXPECT_EQ(size_t(1), v2.closestIndex(0.0));
    EXPECT_EQ(size_t(2), v2.closestIndex(0.5));
    EXPECT_EQ(size_t(2), v2.closestIndex(1.499));
    //    EXPECT_THROW( v2.closestIndex(1.5), std::runtime_error);
    EXPECT_EQ(size_t(2), v2.closestIndex(1.5));
}

TEST(EquiDivision, CheckBin)
{
    Scale axis = EquiDivision("name", 20, 0, 10);

    Bin1D bin0 = axis.bin(0);
    EXPECT_DOUBLE_EQ(0.25, bin0.center());
    EXPECT_DOUBLE_EQ(0.0, bin0.min());
    EXPECT_DOUBLE_EQ(0.5, bin0.max());
    EXPECT_DOUBLE_EQ(0.5, bin0.binSize());

    Bin1D bin3 = axis.bin(3);
    EXPECT_DOUBLE_EQ(1.75, bin3.center());
    EXPECT_DOUBLE_EQ(1.5, bin3.min());
    EXPECT_DOUBLE_EQ(2.0, bin3.max());
    EXPECT_DOUBLE_EQ(0.5, bin3.binSize());

    Bin1D bin10 = axis.bin(10);
    EXPECT_DOUBLE_EQ(5.25, bin10.center());
    EXPECT_DOUBLE_EQ(5.0, bin10.min());
    EXPECT_DOUBLE_EQ(5.5, bin10.max());

    Bin1D bin19 = axis.bin(19);
    EXPECT_DOUBLE_EQ(9.75, bin19.center());
    EXPECT_DOUBLE_EQ(9.5, bin19.min());
    EXPECT_DOUBLE_EQ(10.0, bin19.max());

    EXPECT_THROW(axis.bin(20), std::out_of_range);

    Scale axis2 = EquiDivision("name", 3, -1, 2.0);
    EXPECT_DOUBLE_EQ(-0.5, axis2.bin(0).center());
    EXPECT_DOUBLE_EQ(0.5, axis2.bin(1).center());
    EXPECT_DOUBLE_EQ(1.5, axis2.bin(2).center());
}

TEST(EquiDivision, CheckEquality)
{
    Scale b1 = EquiDivision("axis", 99, -1.01, 3.3);
    Scale b2 = EquiDivision("axis", 99, -1.01, 3.3);
    EXPECT_TRUE(b1 == b2);
    Scale b3 = EquiDivision("axissss", 99, -1.01, 3.3);
    Scale b4 = EquiDivision("axis", 99, -1.0, 3.3);
    Scale b5 = EquiDivision("axis", 99, -1.01, 3.29);
    Scale b6 = EquiDivision("axiss", 98, -1.01, 3.3);
    EXPECT_FALSE(b1 == b3);
    EXPECT_FALSE(b1 == b4);
    EXPECT_FALSE(b1 == b5);
    EXPECT_FALSE(b1 == b6);
}

TEST(EquiDivision, CheckClone)
{
    Scale a1 = EquiDivision("axis", 99, -1.2, 5.4);
    Scale* clone = a1.clone();
    EXPECT_TRUE(a1 == *clone);
    delete clone;
}

TEST(EquiDivision, BinCenters)
{
    Scale axis = EquiDivision("name", 3, -1.5, 1.5);
    std::vector<double> centers = axis.binCenters();
    EXPECT_EQ(size_t(3), centers.size());
    EXPECT_DOUBLE_EQ(-1.0, centers[0]);
    EXPECT_DOUBLE_EQ(0.0, centers[1]);
    EXPECT_DOUBLE_EQ(1.0, centers[2]);

    EXPECT_DOUBLE_EQ(axis.binCenter(0), centers[0]);
    EXPECT_DOUBLE_EQ(axis.binCenter(1), centers[1]);
    EXPECT_DOUBLE_EQ(axis.binCenter(2), centers[2]);
}
