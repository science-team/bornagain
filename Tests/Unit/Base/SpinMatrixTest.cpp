#include "Base/Spin/SpinMatrix.h"

#include "Base/Spin/Spinor.h"
#include "Tests/GTestWrapper/google_test.h"

const complex_t A{1., 5.};
const complex_t B{2., 6.};
const complex_t C{3., 7.};
const complex_t D{4., 8.};


TEST(SpinMatrix, Retrieve)
{
    SpinMatrix M(A, B, C, D);
    EXPECT_EQ(M.a, A);
    EXPECT_EQ(M.b, B);
    EXPECT_EQ(M.c, C);
    EXPECT_EQ(M.d, D);
}

TEST(SpinMatrix, Adjoint)
{
    SpinMatrix M0(A, B, C, D);
    SpinMatrix M1 = M0.adjoint();
    SpinMatrix M2 = M1.adjoint();
    EXPECT_EQ(M0.a, conj(M1.a));
    EXPECT_EQ(M0.b, conj(M1.c));
    EXPECT_EQ(M0.c, conj(M1.b));
    EXPECT_EQ(M0.d, conj(M1.d));
    EXPECT_EQ(M0.a, M2.a);
    EXPECT_EQ(M0.b, M2.b);
    EXPECT_EQ(M0.c, M2.c);
    EXPECT_EQ(M0.d, M2.d);
}

TEST(SpinMatrix, Column)
{
    SpinMatrix M(A, B, C, D);
    EXPECT_EQ(M.col0().u, A);
    EXPECT_EQ(M.col0().v, C);
    EXPECT_EQ(M.col1().u, B);
    EXPECT_EQ(M.col1().v, D);
}
