#include "BATesting.h"

#include "Base/Util/StringUtil.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(StringUtils, toInt)
{
    EXPECT_TRUE(Base::String::to_int("1", nullptr));
    EXPECT_FALSE(Base::String::to_int("a", nullptr));

    int v = 2;
    EXPECT_FALSE(Base::String::to_int("", &v));
    EXPECT_EQ(v, 2);

    EXPECT_FALSE(Base::String::to_int(" ", &v));
    EXPECT_EQ(v, 2);

    EXPECT_FALSE(Base::String::to_int("r", &v));
    EXPECT_EQ(v, 2);

    EXPECT_FALSE(Base::String::to_int("1r", &v));
    EXPECT_EQ(v, 2);

    EXPECT_FALSE(Base::String::to_int("1r  ", &v));
    EXPECT_EQ(v, 2);

    EXPECT_FALSE(Base::String::to_int("r1", &v));
    EXPECT_EQ(v, 2);

    EXPECT_FALSE(Base::String::to_int("1 r", &v));
    EXPECT_EQ(v, 2);

    EXPECT_FALSE(Base::String::to_int("r 1", &v));
    EXPECT_EQ(v, 2);

    EXPECT_FALSE(Base::String::to_int("111111111111111111111111111111111", &v));
    EXPECT_EQ(v, 2);

    EXPECT_FALSE(Base::String::to_int("1\t", &v));
    EXPECT_EQ(v, 2);

    EXPECT_FALSE(Base::String::to_int("\t1", &v));
    EXPECT_EQ(v, 2);

    EXPECT_TRUE(Base::String::to_int("1", &v));
    EXPECT_EQ(v, 1);

    EXPECT_TRUE(Base::String::to_int(" 2", &v));
    EXPECT_EQ(v, 2);

    EXPECT_TRUE(Base::String::to_int(" 3 ", &v));
    EXPECT_EQ(v, 3);

    EXPECT_TRUE(Base::String::to_int("4 ", &v));
    EXPECT_EQ(v, 4);

    EXPECT_TRUE(Base::String::to_int("01", &v));
    EXPECT_EQ(v, 1);

    EXPECT_TRUE(Base::String::to_int("0000", &v));
    EXPECT_EQ(v, 0);

    EXPECT_TRUE(Base::String::to_int("65535", &v));
    EXPECT_EQ(v, 65535);

    EXPECT_TRUE(Base::String::to_int("-10", &v));
    EXPECT_EQ(v, -10);

    EXPECT_TRUE(Base::String::to_int("  -20  ", &v));
    EXPECT_EQ(v, -20);
}

TEST(StringUtils, trimFront)
{
    EXPECT_EQ(Base::String::trimFront(" "), "");
    EXPECT_EQ(Base::String::trimFront(" a"), "a");
    EXPECT_EQ(Base::String::trimFront("\t a"), "a");
    EXPECT_EQ(Base::String::trimFront("\t a", " "), "\t a");
    EXPECT_EQ(Base::String::trimFront("\t\ta"), "a");
    EXPECT_EQ(Base::String::trimFront("a "), "a ");
}

TEST(StringUtils, startsWith)
{
    EXPECT_TRUE(Base::String::startsWith("a", "a"));
    EXPECT_TRUE(Base::String::startsWith("ab", "a"));
    EXPECT_TRUE(Base::String::startsWith(" a", " a"));
    EXPECT_TRUE(Base::String::startsWith("abc", "ab"));
    EXPECT_TRUE(Base::String::startsWith("\tabc", "\t"));
    EXPECT_TRUE(Base::String::startsWith("abc", ""));

    EXPECT_FALSE(Base::String::startsWith("ab", "abc"));
    EXPECT_FALSE(Base::String::startsWith("abc", "A"));
    EXPECT_FALSE(Base::String::startsWith(" a", "a"));
}
