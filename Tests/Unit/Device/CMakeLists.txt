include(GoogleTest) # provides gtest_discover_tests

set(test UnitTestDevice)

file(GLOB source_files "*.cpp" ${CMAKE_SOURCE_DIR}/Tests/GTestWrapper/TestAll.cpp)

add_executable(${test} ${source_files})
target_compile_definitions(${test} PRIVATE -DBORNAGAIN_PYTHON)
target_link_libraries(${test} BornAgainDevice gtest)

gtest_discover_tests(${test} DISCOVERY_TIMEOUT 300 TEST_PREFIX Unit.Device.)

file(MAKE_DIRECTORY ${TEST_OUTPUT_DIR}/Unit/Device)
