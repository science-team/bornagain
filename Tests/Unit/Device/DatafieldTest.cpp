#include "Device/Data/Datafield.h"

#include "Base/Axis/MakeScale.h"
#include "Base/Axis/Scale.h"
#include "Device/Data/DataUtil.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(Datafield, rotatedDatafield)
{
    Datafield a({newEquiDivision("axis0", 2, 1., 2.), newEquiDivision("axis1", 3, 3., 4.)},
                {0., 1., 2., 3., 4., 5.});

    Datafield b = DataUtil::rotatedDatafield(a, 5);

    EXPECT_EQ(b.axis(0).size(), 3);
    EXPECT_EQ(b.axis(1).size(), 2);

    EXPECT_EQ(b[0], 2.);
    EXPECT_EQ(b[1], 5.);
    EXPECT_EQ(b[2], 1.);
    EXPECT_EQ(b[3], 4.);
    EXPECT_EQ(b[4], 0.);
    EXPECT_EQ(b[5], 3.);

    Datafield c = DataUtil::rotatedDatafield(a, -6);

    EXPECT_EQ(c.axis(0).size(), 2);
    EXPECT_EQ(c.axis(1).size(), 3);

    EXPECT_EQ(c[0], 5.);
    EXPECT_EQ(c[1], 4.);
    EXPECT_EQ(c[2], 3.);
    EXPECT_EQ(c[3], 2.);
    EXPECT_EQ(c[4], 1.);
    EXPECT_EQ(c[5], 0.);

    Datafield d = DataUtil::rotatedDatafield(a, 3);

    EXPECT_EQ(d.axis(0).size(), 3);
    EXPECT_EQ(d.axis(1).size(), 2);

    EXPECT_EQ(d[0], 3.);
    EXPECT_EQ(d[1], 0.);
    EXPECT_EQ(d[2], 4.);
    EXPECT_EQ(d[3], 1.);
    EXPECT_EQ(d[4], 5.);
    EXPECT_EQ(d[5], 2.);
}

TEST(Datafield, create2DArrayfromDatafield)
{
    Datafield a{{newEquiDivision("axis0", 3, 1., 3.), newEquiDivision("axis1", 2, 10., 20.)}};
    EXPECT_EQ(6u, a.size());

    EXPECT_EQ(3u, a.axis(0).size()); // no. of rows
    EXPECT_EQ(2u, a.axis(1).size()); // no. of cols

    std::vector<double> v{0, 1, 2, 3, 4, 5};
    a.setVector(v);

    EXPECT_EQ(v[0], 0);
    EXPECT_EQ(v[1], 1);
    EXPECT_EQ(v[2], 2);
    EXPECT_EQ(v[3], 3);
    EXPECT_EQ(v[4], 4);
    EXPECT_EQ(v[5], 5);

    auto v2 = a.flatVector();
    EXPECT_EQ(v2, v);

    auto v3 = a.values2D();

    double2d_t v3expected{{0, 1, 2}, {3, 4, 5}};
    EXPECT_EQ(v3, v3expected);
}

TEST(Datafield, ctor2D)
{
    std::vector<double> arr_in{1, 2, 3, 4, 5, 6};
    double2d_t array_2d{{1., 2., 3.}, {4., 5., 6.}};
    const Datafield data("x ()", "y ()", array_2d);
    EXPECT_EQ(arr_in, data.flatVector());
}

TEST(Datafield, DatafieldToVector2D)
{
    Datafield data({newEquiDivision("axis0", 3, 10., 20.), newEquiDivision("axis1", 2, 30., 40.)});
    const std::vector<double> values = {0., 1., 2., 10., 11., 12.};

    data.setVector(values);

    auto vec = data.values2D();
    const double2d_t expected = {{0., 1., 2.}, {10., 11., 12.}};
    EXPECT_EQ(vec, expected);
}
