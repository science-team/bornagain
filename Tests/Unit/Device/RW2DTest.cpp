#include "BATesting.h"
#include "Base/Axis/MakeScale.h"
#include "Base/Axis/Scale.h"
#include "Device/Data/Datafield.h"
#include "Device/IO/IOFactory.h"
#include "Device/IO/ReadWrite2DTable.h"
#include "Device/IO/ReadWriteINT.h"
#include "Device/IO/ReadWriteTiff.h"
#include "Tests/GTestWrapper/google_test.h"
#include <filesystem>

namespace fs = std::filesystem;

static const fs::path testdir =
    fs::path(BATesting::TestOutDir) / fs::path("Unit") / fs::path("Device");

class RW2DTest : public ::testing::Test {
protected:
    RW2DTest()
    {
        for (size_t i = 0, size = m_model_data.size(); i < size; ++i)
            m_model_data[i] = static_cast<double>(i);
    }

    Datafield m_model_data{{newEquiDivision("x", 5, 1.0, 5.0), newEquiDivision("y", 10, 6.0, 7.0)}};
};


TEST_F(RW2DTest, rwInt)
{
    std::stringstream ss;
    Util::RW::writeBAInt(m_model_data, ss);

    Datafield result = Util::RW::readBAInt(ss);

    auto compare_axis = [this, &result](size_t index) {
        EXPECT_EQ(m_model_data.axis(index).size(), result.axis(index).size());
        EXPECT_EQ(m_model_data.axis(index).min(), result.axis(index).min());
        EXPECT_EQ(m_model_data.axis(index).max(), result.axis(index).max());
    };

    EXPECT_EQ(m_model_data.rank(), result.rank());
    compare_axis(0);
    compare_axis(1);
    for (size_t i = 0, size = m_model_data.size(); i < size; ++i)
        EXPECT_EQ(m_model_data[i], result[i]);
}

TEST_F(RW2DTest, rwNumpyMatrix)
{
    std::stringstream ss;
    Util::RW::write2DTable(m_model_data, ss);

    Datafield result = Util::RW::read2DTable(ss);
    EXPECT_EQ(m_model_data.rank(), result.rank());
    for (size_t i = 0, size = m_model_data.size(); i < size; ++i)
        EXPECT_EQ(m_model_data[i], result[i]);
}

TEST_F(RW2DTest, ascii)
{
    const std::string fname = (testdir / fs::path("rw2dtestdata")).string();
    IO::writeDatafield(m_model_data, fname);

    Datafield result = IO::readData2D(fname, IO::csv2D);
    EXPECT_EQ(m_model_data.rank(), result.rank());
    for (size_t i = 0, size = m_model_data.size(); i < size; ++i)
        EXPECT_EQ(m_model_data[i], result[i]);
}

TEST_F(RW2DTest, gz)
{
    const std::string fname = (testdir / fs::path("rw2dtestdata.gz")).string();
    IO::writeDatafield(m_model_data, fname);

    Datafield result = IO::readData2D(fname, IO::csv2D);
    EXPECT_EQ(m_model_data.rank(), result.rank());
    for (size_t i = 0, size = m_model_data.size(); i < size; ++i)
        EXPECT_EQ(m_model_data[i], result[i]);
}

TEST_F(RW2DTest, bz2)
{
    const std::string fname = (testdir / fs::path("rw2dtestdata.bz2")).string();
    IO::writeDatafield(m_model_data, fname);

    Datafield result = IO::readData2D(fname, IO::csv2D);
    EXPECT_EQ(m_model_data.rank(), result.rank());
    for (size_t i = 0, size = m_model_data.size(); i < size; ++i)
        EXPECT_EQ(m_model_data[i], result[i]);
}

#ifdef BA_TIFF_SUPPORT

TEST_F(RW2DTest, rwTiff)
{
    std::stringstream ss;
    Util::RW::writeTiff(m_model_data, ss);

    Datafield result = Util::RW::readTiff(ss);
    EXPECT_EQ(m_model_data.rank(), result.rank());
    for (size_t i = 0, size = m_model_data.size(); i < size; ++i)
        EXPECT_EQ(m_model_data[i], result[i]);
}

#endif // BA_TIFF_SUPPORT
