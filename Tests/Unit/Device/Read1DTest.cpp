#include "BATesting.h"

#include "Base/Axis/Bin.h"
#include "Base/Axis/Scale.h"
#include "Device/Data/Datafield.h"
#include "Device/IO/IOFactory.h"
#include "Tests/GTestWrapper/google_test.h"
#include <iostream>

TEST(Read1D, readMotofit)
{
    const auto path = BATesting::ExampleDataDir + "/specular/ILL-FIGARO.mft";
    Datafield data = IO::readData1D(path, IO::Filetype1D::mft);
    EXPECT_EQ(data.rank(), 1);
    EXPECT_EQ(data.size(), 120);
    EXPECT_NEAR(data.xAxis().binCenter(119), 0.290719, 1e-7);
    EXPECT_EQ(data.xAxis().bin(119).binSize(), 0);
    EXPECT_NEAR(data[119], 4.86941e-7, 1e-13);
    EXPECT_NEAR(data.errorSigmas()[119], 2.55580e-7, 1e-13);
}

TEST(Read1D, MotofitToInt) // use mft data to test writing and reading from/to ba-int format
{
    const auto path1 = BATesting::ExampleDataDir + "/specular/ILL-D17.mft";
    const auto path2 = BATesting::TestOutDir + "/Unit/Device/ILL-D17.int";
    Datafield data1 = IO::readData1D(path1, IO::Filetype1D::mft);
    std::cout << "Read datafield with " << data1.size() << " entries" << std::endl;
    IO::writeDatafield(data1, path2);
    Datafield data2 = IO::readData1D(path2, IO::Filetype1D::bornagain1D);
    EXPECT_EQ(data2.rank(), 1);
    EXPECT_EQ(data2.size(), 454);
    EXPECT_NEAR(data2.xAxis().binCenter(453), 0.210597, 1e-7);
    EXPECT_EQ(data2.xAxis().bin(453).binSize(), 0);
    EXPECT_NEAR(data2[453], 6.40348e-7, 1e-13);
    EXPECT_NEAR(data2.errorSigmas()[453], 7.21361e-7, 1e-13);
}
