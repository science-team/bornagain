#include "BATesting.h"

#include "Device/Data/Datafield.h"
#include "Device/IO/IOFactory.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(Read2D, readSANSRaw)
{
    const auto fname = BATesting::ExampleDataDir + "/scatter2d/SANSDRaw.001";
    Datafield data = IO::readData2D(fname, IO::Filetype2D::nicos2D);
    EXPECT_EQ(data.rank(), 2);
    EXPECT_EQ(data.size(), 16384);
    EXPECT_EQ(data[128 * 62 + 70], 443);
}
