#include "Base/Axis/Frame.h"

#include "Base/Axis/MakeScale.h"
#include "Base/Axis/Scale.h"
#include "Base/Const/Units.h"
#include "Device/Detector/SphericalDetector.h"
#include "Tests/GTestWrapper/google_test.h"
#include <memory>

using Units::deg;

//! Testing region of interest with reasonable area within the detector.

TEST(RegionOfInterest, constructor)
{
    SphericalDetector detector(8, -3 * deg, 5 * deg, 4, 0 * deg, 4 * deg);

    // creating region of interest
    double xlow(-1.9 * deg), ylow(1.1 * deg), xup(2.9 * deg), yup(2.85 * deg);
    detector.setRegionOfInterest(xlow, ylow, xup, yup);
    EXPECT_EQ(detector.regionOfInterestBounds(0).first, xlow);
    EXPECT_EQ(detector.regionOfInterestBounds(0).second, xup);
    EXPECT_EQ(detector.regionOfInterestBounds(1).first, ylow);
    EXPECT_EQ(detector.regionOfInterestBounds(1).second, yup);

    // checking total size of the detector and roi in it
    EXPECT_EQ(detector.totalSize(), 32u);
    EXPECT_EQ(detector.sizeOfRegionOfInterest(), 10u);

    // converting local roi index to global detector index
    EXPECT_EQ(detector.roiToFullIndex(0), 9);
    EXPECT_EQ(detector.roiToFullIndex(4), 13);
    EXPECT_EQ(detector.roiToFullIndex(5), 17);
    EXPECT_EQ(detector.roiToFullIndex(9), 21);
}

//! Testing region of interest which is larger than the detector.

TEST(RegionOfInterest, largeArea)
{
    SphericalDetector detector(8, -3 * deg, 5 * deg, 4, 0 * deg, 4 * deg);

    // creating region of interest
    double xlow(-3.9 * deg), ylow(-1.1 * deg), xup(6.9 * deg), yup(5.85 * deg);
    detector.setRegionOfInterest(xlow, ylow, xup, yup);

    // checking total size of the detector and roi in it
    EXPECT_EQ(detector.totalSize(), 32u);
    EXPECT_EQ(detector.sizeOfRegionOfInterest(), 32u);

    // converting local roi index to global detector index
    EXPECT_EQ(detector.roiToFullIndex(5), 5u);
    EXPECT_EQ(detector.roiToFullIndex(6), 6u);
    EXPECT_EQ(detector.roiToFullIndex(9), 9u);
    EXPECT_EQ(detector.roiToFullIndex(27), 27u);
}

//! Testing clone

TEST(RegionOfInterest, clone)
{
    SphericalDetector detector(8, -3 * deg, 5 * deg, 4, 0 * deg, 4 * deg);

    // creating region of interest
    double xlow(-1.9 * deg), ylow(1.1 * deg), xup(2.9 * deg), yup(2.85 * deg);
    detector.setRegionOfInterest(xlow, ylow, xup, yup);

    std::unique_ptr<SphericalDetector> clone(detector.clone());

    EXPECT_EQ(clone->regionOfInterestBounds(0).first, xlow);
    EXPECT_EQ(clone->regionOfInterestBounds(0).second, xup);
    EXPECT_EQ(clone->regionOfInterestBounds(1).first, ylow);
    EXPECT_EQ(clone->regionOfInterestBounds(1).second, yup);

    // checking total size of the detector and roi in it
    EXPECT_EQ(clone->totalSize(), 32u);
    EXPECT_EQ(clone->sizeOfRegionOfInterest(), 10u);
}
