#include "Fit/Param/AttLimits.h"

#include "Tests/GTestWrapper/google_test.h"

TEST(AttLimits, InitialState)
{
    AttLimits limits;
    EXPECT_FALSE(limits.isFixed());
    EXPECT_FALSE(limits.isLimited());
    EXPECT_FALSE(limits.isUpperLimited());
    EXPECT_FALSE(limits.isLowerLimited());
    EXPECT_TRUE(limits.isLimitless());
}

TEST(AttLimits, LowerLimited)
{
    AttLimits limits = AttLimits::lowerLimited(1.0);
    EXPECT_FALSE(limits.isFixed());
    EXPECT_FALSE(limits.isLimited());
    EXPECT_FALSE(limits.isUpperLimited());
    EXPECT_TRUE(limits.isLowerLimited());
    EXPECT_FALSE(limits.isLimitless());
    EXPECT_EQ(1.0, limits.min());
    EXPECT_EQ(0.0, limits.max());
}

TEST(AttLimits, UpperLimited)
{
    AttLimits limits = AttLimits::upperLimited(1.0);
    EXPECT_FALSE(limits.isFixed());
    EXPECT_FALSE(limits.isLimited());
    EXPECT_TRUE(limits.isUpperLimited());
    EXPECT_FALSE(limits.isLowerLimited());
    EXPECT_FALSE(limits.isLimitless());
    EXPECT_EQ(0.0, limits.min());
    EXPECT_EQ(1.0, limits.max());
}

TEST(AttLimits, Fixed)
{
    AttLimits limits = AttLimits::fixed();
    EXPECT_TRUE(limits.isFixed());
    EXPECT_FALSE(limits.isLimited());
    EXPECT_FALSE(limits.isUpperLimited());
    EXPECT_FALSE(limits.isLowerLimited());
    EXPECT_FALSE(limits.isLimitless());
    EXPECT_EQ(0.0, limits.min());
    EXPECT_EQ(0.0, limits.max());
}

TEST(AttLimits, Limited)
{
    AttLimits limits = AttLimits::limited(1.0, 2.0);
    EXPECT_FALSE(limits.isFixed());
    EXPECT_TRUE(limits.isLimited());
    EXPECT_FALSE(limits.isUpperLimited());
    EXPECT_FALSE(limits.isLowerLimited());
    EXPECT_FALSE(limits.isLimitless());
    EXPECT_EQ(1.0, limits.min());
    EXPECT_EQ(2.0, limits.max());
}
