include(GoogleTest)

set(test UnitTestGUI)

file(GLOB source_files *.cpp)

find_package(Qt6Core REQUIRED)
find_package(Qt6Test REQUIRED) # provides QSignalSpy and QTest

include_directories(${CMAKE_SOURCE_DIR}/Tests/Unit/utilities)

set(CMAKE_AUTOMOC ON)
add_executable(${test} ${source_files})
target_compile_definitions(${test} PRIVATE -DBORNAGAIN_PYTHON)
target_link_libraries(${test} BornAgainGUI gtest Qt6::Core Qt6::Test)

set(test_data_dir ${TEST_OUTPUT_DIR}/Unit/GUI)
file(MAKE_DIRECTORY ${test_data_dir})

gtest_discover_tests(${test} DISCOVERY_TIMEOUT 300 TEST_PREFIX Unit.GUI.
    WORKING_DIRECTORY ${test_data_dir})
