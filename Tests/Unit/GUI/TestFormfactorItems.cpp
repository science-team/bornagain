#include "Base/Const/Units.h"
#include "Base/Math/Numeric.h"
#include "GUI/Model/Sample/FormfactorItems.h"
#include "Sample/HardParticle/Polyhedra.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(FormfactorItems, Pyramid2Item)
{
    // to domain
    Pyramid2Item item;
    item.setLength(20.0);
    item.setWidth(16.0);
    item.setHeight(13.0);
    item.setAlpha(60.0);
    auto P_ff = item.createFormfactor();
    auto* p_ff = dynamic_cast<Pyramid2*>(P_ff.get());
    EXPECT_TRUE(p_ff);
    EXPECT_EQ(p_ff->length(), 20.0);
    EXPECT_EQ(p_ff->width(), 16.0);
    EXPECT_EQ(p_ff->height(), 13.0);
    EXPECT_TRUE(Numeric::almostEqual(p_ff->alpha(), Units::deg2rad(60.0), 2));
}
