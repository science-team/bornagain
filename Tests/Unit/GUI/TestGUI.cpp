// This is the main program for the executable UnitTestGUI
// (for non-GUI unit tests, the corresponding source is Tests/GTestWrapper/TestAll.cpp).

#include "Tests/GTestWrapper/google_test.h"
#include <QCoreApplication>

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    Q_UNUSED(app);

    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
