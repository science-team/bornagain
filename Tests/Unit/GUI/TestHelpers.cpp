#include "GUI/Model/Util/Path.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(Helpers, VersionString)
{
    int vmajor(0), vminor(0);

    EXPECT_EQ(true, GUI::Path::parseVersion("199.19", vmajor, vminor));
    EXPECT_EQ(199, vmajor);
    EXPECT_EQ(19, vminor);

    EXPECT_FALSE(GUI::Path::parseVersion("1.0.0", vmajor, vminor));

    QString min_version("20.3");
    EXPECT_TRUE(GUI::Path::isVersionMatchMinimal("20.3", min_version));
    EXPECT_TRUE(GUI::Path::isVersionMatchMinimal("20.4", min_version));
    EXPECT_TRUE(GUI::Path::isVersionMatchMinimal("21.0", min_version));
    EXPECT_TRUE(GUI::Path::isVersionMatchMinimal("123.45", min_version));

    EXPECT_FALSE(GUI::Path::isVersionMatchMinimal("20.2", min_version));
    EXPECT_FALSE(GUI::Path::isVersionMatchMinimal("19.19", min_version));
    EXPECT_FALSE(GUI::Path::isVersionMatchMinimal("18.0", min_version));
    EXPECT_FALSE(GUI::Path::isVersionMatchMinimal("0.9", min_version));
}
