#include "GUI/Model/Material/MaterialItem.h"
#include "GUI/Model/Material/MaterialsSet.h"
#include "GUI/Model/Util/Backup.h"
#include "Tests/GTestWrapper/google_test.h"
#include <memory>

TEST(MaterialsSet, addRefractiveMaterial)
{
    std::unique_ptr<MaterialsSet> model(new MaterialsSet);

    EXPECT_TRUE(model->empty());

    const double delta(0.2), beta(0.1);
    const QString name("MaterialName");
    MaterialItem* material = model->addRefractiveMaterialItem(name, delta, beta);

    EXPECT_EQ(model->size(), 1);

    EXPECT_EQ(material->matItemName(), name);
    EXPECT_TRUE(material->hasRefractiveIndex());
    EXPECT_EQ(material->delta().dVal(), delta);
    EXPECT_EQ(material->beta().dVal(), beta);
}

TEST(MaterialsSet, addSLDMaterial)
{
    std::unique_ptr<MaterialsSet> model(new MaterialsSet);

    EXPECT_TRUE(model->empty());

    const double sld_real(0.2), sld_imag(0.1);
    const QString name("MaterialName");
    MaterialItem* material = model->addSLDMaterialItem(name, sld_real, sld_imag);

    EXPECT_EQ(model->size(), 1);

    EXPECT_EQ(material->matItemName(), name);
    EXPECT_FALSE(material->hasRefractiveIndex());
    EXPECT_EQ(material->sldRe().dVal(), sld_real);
    EXPECT_EQ(material->sldIm().dVal(), sld_imag);
}

//! Checks the method which returns MaterialItem from known identifier.

TEST(MaterialsSet, materialFromIdentifier)
{
    MaterialsSet model;
    MaterialItem* material1 = model.addRefractiveMaterialItem("aaa", 1.0, 2.0);
    MaterialItem* material2 = model.addRefractiveMaterialItem("bbb", 3.0, 4.0);
    EXPECT_EQ(material1, model.materialItemFromIdentifier(material1->identifier()));
    EXPECT_EQ(material2, model.materialItemFromIdentifier(material2->identifier()));
    EXPECT_EQ(nullptr, model.materialItemFromIdentifier("non-existing-identifier"));
}

//! Checks the method which returns MaterialItem from material name.

TEST(MaterialsSet, materialFromName)
{
    MaterialsSet model;
    MaterialItem* material1 = model.addRefractiveMaterialItem("aaa", 1.0, 2.0);
    MaterialItem* material2 = model.addRefractiveMaterialItem("bbb", 3.0, 4.0);
    EXPECT_EQ(material1, model.materialItemFromName(material1->matItemName()));
    EXPECT_EQ(material2, model.materialItemFromName(material2->matItemName()));
    EXPECT_EQ(nullptr, model.materialItemFromName("non-existing-name"));
}

//! Default MaterialProperty construction.

TEST(MaterialsSet, defaultMaterialProperty)
{
    MaterialsSet model;

    // should throw in the absence of any materials
    // EXPECT_FAILED_ASSERT(model.defaultMaterialItem()); <--- TODO: restore

    // adding materials to the model, default property should refer to first material in a model
    MaterialItem* mat1 = model.addRefractiveMaterialItem("Something1", 1.0, 2.0);
    model.addRefractiveMaterialItem("Something2", 3.0, 4.0);
    EXPECT_EQ(model.defaultMaterialItem(), mat1);
}

TEST(MaterialsSet, serializeRefractiveMaterial)
{
    MaterialItem material;
    material.setRefractiveIndex(11, 12);
    material.setMatItemName("name");
    material.setMagnetization(R3(1, 2, 3));
    const auto a1 = GUI::Util::createBackup(&material);

    MaterialItem target;
    GUI::Util::restoreBackup(&target, a1);

    // XML comparison
    const auto a2 = GUI::Util::createBackup(&target);
    EXPECT_EQ(a1, a2);

    // Content comparison
    EXPECT_EQ(target.matItemName(), "name");
    EXPECT_TRUE(target.hasRefractiveIndex());
    EXPECT_EQ(target.delta().dVal(), 11);
    EXPECT_EQ(target.beta().dVal(), 12);
    EXPECT_EQ(target.magnetization().r3(), R3(1, 2, 3));
}

TEST(MaterialsSet, serializeSLDMaterial)
{
    MaterialItem material;
    material.setScatteringLengthDensity(complex_t(11, 12));
    material.setMatItemName("name");
    material.setMagnetization(R3(1, 2, 3));
    const auto a1 = GUI::Util::createBackup(&material);

    MaterialItem target;
    GUI::Util::restoreBackup(&target, a1);

    // XML comparison
    const auto a2 = GUI::Util::createBackup(&target);
    EXPECT_EQ(a1, a2);

    // Content comparison
    EXPECT_EQ(target.matItemName(), "name");
    EXPECT_FALSE(target.hasRefractiveIndex());
    EXPECT_EQ(target.sldRe().dVal(), 11);
    EXPECT_EQ(target.sldIm().dVal(), 12);
    EXPECT_EQ(target.magnetization().r3(), R3(1, 2, 3));
}
