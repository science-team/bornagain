#include "GUI/Model/Sample/ProfileItems.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(ProfileItems, Profile1DCauchy)
{
    // to domain
    Profile1DCauchyItem item;
    item.setOmega(2.0);
    auto pdf = item.createProfile();
    const auto* cauchy = dynamic_cast<Profile1DCauchy*>(pdf.get());
    EXPECT_EQ(cauchy->omega(), 2.0);

    // from domain
    Profile1DCauchy pdf2(3.0);
    Profile1DCauchyItem item2;
    item2.setOmega(pdf2.omega());
    EXPECT_EQ(item2.omega(), 3.0);
}
