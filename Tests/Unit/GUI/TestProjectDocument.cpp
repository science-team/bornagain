#include "Base/Axis/Frame.h"
#include "Device/Data/Datafield.h"
#include "GUI/Model/Data/Data2DItem.h"
#include "GUI/Model/File/DatafileItem.h"
#include "GUI/Model/File/DatafilesSet.h"
#include "GUI/Model/Project/ProjectDocument.h"
#include "GUI/Model/Project/ProjectUtil.h"
#include "GUI/Model/Sim/InstrumentItems.h"
#include "GUI/Model/Sim/InstrumentsSet.h"
#include "GUI/Model/Util/Path.h"
#include "Tests/GTestWrapper/google_test.h"
#include "Tests/Unit/GUI/Utils.h"
#include <QFileInfo>
#include <QSignalSpy>
#include <QUuid>

class TestProjectDocument : public ::testing::Test {
protected:
    //! helper method to modify something in a model
    void modelsModifier(ProjectDocument& doc)
    {
        auto* instrument = doc.instrumentsRW()->at(0);
        instrument->setName(QUuid::createUuid().toString());
        gDoc->setModified();
    }
};

TEST_F(TestProjectDocument, projectDocument)
{
    const QString projectDir("projectDocument");
    UTest::GUI::create_dir(projectDir);
    const QString ext = QString(GUI::Util::Project::projectFileExtension);
    const QString projectFileName(projectDir + "/document" + ext);

    gDoc = std::make_unique<ProjectDocument>();
    // Checking initial document status
    EXPECT_FALSE(gDoc->isModified());
    EXPECT_FALSE(gDoc->hasValidNameAndPath());
    EXPECT_EQ(gDoc->projectDir(), QString());
    EXPECT_EQ(gDoc->projectName(), "Untitled");

    auto* instrument = new Scatter2DInstrumentItem;
    gDoc->instrumentsRW()->add_item(instrument);
    instrument->setName("GISAS");

    // Checking document name and isModified status after project save
    gDoc->saveProjectFileWithData(projectFileName);
    EXPECT_TRUE(gDoc->hasValidNameAndPath());
    EXPECT_EQ(gDoc->projectDir(), projectDir);
    EXPECT_EQ(gDoc->projectName(), "document");
    EXPECT_EQ(gDoc->projectFullPath(), projectFileName);
    EXPECT_FALSE(gDoc->isModified());

    QSignalSpy spyDocument(gDoc.get(), SIGNAL(modifiedStateChanged()));
    EXPECT_EQ(spyDocument.count(), 0);

    // Changing document and checking its status
    modelsModifier(*gDoc);
    EXPECT_TRUE(gDoc->isModified());
    // EXPECT_EQ(spyDocument.count(), 1); TODO 18feb24 restore

    // Saving document
    gDoc->saveProjectFileWithData(projectFileName);
    EXPECT_FALSE(gDoc->isModified());
    // EXPECT_EQ(spyDocument.count(), 2); TODO 18feb24 restore

    QFileInfo info(projectFileName);
    EXPECT_TRUE(info.exists());
}

TEST_F(TestProjectDocument, projectDocumentWithData)
{
    gDoc = std::make_unique<ProjectDocument>();

    const QString projectDir("projectDocumentWithData");
    UTest::GUI::create_dir(projectDir);
    const QString ext = QString(GUI::Util::Project::projectFileExtension);

    auto* instrument = new Scatter2DInstrumentItem;
    gDoc->instrumentsRW()->add_item(instrument);
    instrument->setName("GISAS");
    DatafileItem* realData = UTest::GUI::createRealData2D("TestData", *gDoc->datafilesRW(), 0.);
    ASSERT(realData);
    DataItem* data_item = realData->dataItem();
    std::unique_ptr<Frame> frame = gDoc->instruments()->at(0)->createFrame();
    Datafield df(*frame);
    data_item->setDatafield(df);

    gDoc->setProjectDir(projectDir);
    gDoc->saveProjectFileWithData(projectDir + "/untitled" + ext);

    QFileInfo info(projectDir + "/untitled" + ext);
    EXPECT_TRUE(info.exists());

    info.setFile(projectDir + "/realdata_TestData.int");
    EXPECT_TRUE(info.exists());
}

//! Testing ProjectDocument on simple documents (no heavy data).
//! ProjectDocument should not be able to save project file.
//! Regression test for issue #1136 ("After a failed project saving, no saving takes place any more;
//! crash when changing projects")

TEST_F(TestProjectDocument, failingProjectSave)
{
    const QString projectDir("failingSaveService");
    // do NOT create dir in order to force saving to fail
    const QString ext = QString(GUI::Util::Project::projectFileExtension);
    const QString projectFileName(projectDir + "/document" + ext);

    gDoc = std::make_unique<ProjectDocument>();
    auto* instrument = new Scatter2DInstrumentItem;
    gDoc->instrumentsRW()->add_item(instrument);
    instrument->setName("GISAS");
    modelsModifier(*gDoc);

    EXPECT_FALSE(QFile::exists(projectFileName));

    QSignalSpy spyDocument(gDoc.get(), SIGNAL(modifiedStateChanged()));

    EXPECT_THROW(gDoc->saveProjectFileWithData(projectFileName), std::runtime_error);

    EXPECT_EQ(spyDocument.count(), 0);
    EXPECT_FALSE(QFile::exists(projectFileName));

    // after failed save, document should still be in modified state
    EXPECT_TRUE(gDoc->isModified());
}
