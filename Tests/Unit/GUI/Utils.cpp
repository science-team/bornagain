//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/Unit/GUI/Utils.cpp
//! @brief     Implements auxiliary test functions in a namespace.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2015
//! @authors   Scientific Computing Group at MLZ Garching
//
//  ************************************************************************************************

#include "Tests/Unit/GUI/Utils.h"
#include "Base/Axis/MakeScale.h"
#include "Device/Data/Datafield.h"
#include "Device/IO/IOFactory.h"
#include "GUI/Model/Data/DataItem.h"
#include "GUI/Model/File/DatafileItem.h"
#include "GUI/Model/File/DatafilesSet.h"
#include "GUI/Model/Project/ProjectUtil.h"

void UTest::GUI::create_dir(const QString& dir_name)
{
    if (QFile::exists(dir_name))
        QDir(dir_name).removeRecursively();

    if (!QDir(".").mkdir(dir_name))
        throw std::runtime_error("Cannot create '" + dir_name.toStdString()
                                 + "' in parent directory '.'.");
}

Datafield UTest::GUI::makeData1D(double value, int nx, double x_min, double x_max)
{
    std::vector<const Scale*> axes;
    axes.emplace_back(newEquiDivision("x", nx, x_min, x_max));

    Datafield result(axes);
    result.setAllTo(value);
    return result;
}

Datafield UTest::GUI::makeData2D(double value, int nx, double x_min, double x_max, int ny,
                                 double y_min, double y_max)
{
    std::vector<const Scale*> axes;
    axes.emplace_back(newEquiDivision("x", nx, x_min, x_max));
    axes.emplace_back(newEquiDivision("y", ny, y_min, y_max));

    Datafield result(axes);
    result.setAllTo(value);
    return result;
}

DatafileItem* UTest::GUI::createRealData1D(const QString& name, DatafilesSet& set, double value)
{
    auto* dfi = new DatafileItem(name, makeData1D(value));
    set.add_item(dfi);
    return dfi;
}

DatafileItem* UTest::GUI::createRealData2D(const QString& name, DatafilesSet& set, double value)
{
    auto* dfi = new DatafileItem(name, makeData2D(value));
    set.add_item(dfi);
    return dfi;
}
