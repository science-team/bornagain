#include "Sample/HardParticle/HardParticles.h"

#include "Base/Const/PhysicalConstants.h"
#include "Sample/Scattering/Rotations.h"
#include "Tests/Unit/Numeric/MultiQTest.h"

using PhysConsts::pi;

class FormfactorOtherTest : public testing::Test {};

//*********** cylinders ***************

TEST_F(FormfactorOtherTest, HorizontalCylinderAsCylinder)
{
    const double R = .3, L = 3;
    Cylinder f0(R, L);
    HorizontalCylinder f1(R, L);

    int failures = formfactorTest::run_test_for_many_q(
        [&](C3 q) -> complex_t { return f0.formfactor(q); },
        [&](C3 q) -> complex_t { return f1.formfactor(q.rotatedY(pi / 2)); }, 1e-99, 100, 2e-15,
        true);
    EXPECT_EQ(failures, 0);
}

TEST_F(FormfactorOtherTest, HorizontalCylinderSlices)
{
    const double R = .3, L = 3;
    HorizontalCylinder f0(R, L);
    HorizontalCylinder f1a(R, L, -R, -0.3 * R);
    HorizontalCylinder f1b(R, L, -0.3 * R, +R);

    int failures = formfactorTest::run_test_for_many_q(
        [&](C3 q) -> complex_t { return f0.formfactor(q); },
        [&](C3 q) -> complex_t {
            return f1a.formfactor(q) + exp_I(q.z() * 0.7 * R) * f1b.formfactor(q);
        },
        1e-99, 100, 1.2e-3, true);
    EXPECT_EQ(failures, 0);
}
