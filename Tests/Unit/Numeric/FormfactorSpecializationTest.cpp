#include "Sample/HardParticle/HardParticles.h"

#include "Tests/GTestWrapper/google_test.h"
#include "Tests/Unit/Numeric/MultiQTest.h"
#include <ff/Face.h> // for diagnostic
#include <iomanip>

//! Compare form factor for particle shapes A and B, where A is given special
//! parameter values so that it coincides with the more symmetric B.

class FFSpecializationTest : public testing::Test {
protected:
    void run_test(IFormfactor* ff0, IFormfactor* ff1, double eps, double qmag1, double qmag2)
    {
        int failures = formfactorTest::run_test_for_many_q(
            [&](C3 q) -> complex_t { return ff0->formfactor(q); },
            [&](C3 q) -> complex_t { return ff1->formfactor(q); }, qmag1, qmag2, eps);
        EXPECT_EQ(failures, 0);
    }
};

//*********** cylinders ***************

TEST_F(FFSpecializationTest, HorizontalCylinderSlicedVsUnsliced)
{
    const double R = .3, L = 3;
    HorizontalCylinder p0(R, L);
    HorizontalCylinder p1(R, L, -R, +R * (1 - 3e-16));
    run_test(&p0, &p1, 3e-8, 1e-99, 100);
}

//*********** spheroids ***************

TEST_F(FFSpecializationTest, HemiEllipsoidAsSphericalSegment)
{
    const double R = 1.07;
    HemiEllipsoid p0(R, R, R);
    SphericalSegment p1(R, 0, R);
    run_test(&p0, &p1, 6e-12, 1e-99, 5e2);
}

TEST_F(FFSpecializationTest, EllipsoidalCylinderAsCylinder)
{
    const double R = .8, H = 1.2;
    EllipsoidalCylinder p0(R, R, H);
    Cylinder p1(R, H);
    run_test(&p0, &p1, 5e-12, 1e-99, 50);
}

TEST_F(FFSpecializationTest, SphericalSegmentAsSphere)
{
    const double R = 1.;
    SphericalSegment p0(R, 0, 0);
    Sphere p1(R);
    run_test(&p0, &p1, 1e-12, .02, 5e1);
}

TEST_F(FFSpecializationTest, SpheroidAsSphere)
{
    const double R = 1.;
    Spheroid p0(R, R);
    Sphere p1(R);
    run_test(&p0, &p1, 1e-12, 1e-99, 50);
}
