############################################################################
 # BornAgain/Tests/Unit/PyBinding/CMakeLists.txt
############################################################################

include(GoogleTest) # provides gtest_discover_tests

set(test TestPyBinding)

file(GLOB source_files *.cpp)

add_executable(${test} ${source_files} ${CMAKE_SOURCE_DIR}/Tests/GTestWrapper/TestAll.cpp)
target_include_directories(${test}
    PUBLIC
    ${Python3_INCLUDE_DIRS}
    ${AUTO_WRAP_DIR}
)

target_link_libraries(${test} BornAgainSim BornAgainPyCore gtest ${Python3_LIBRARIES})

target_compile_definitions(${test} PRIVATE -DBORNAGAIN_PYTHON)

gtest_discover_tests(${test} TEST_PREFIX Unit.PyBinding. PROPERTIES ENVIRONMENT "PYTHONPATH=${BA_PY_SOURCE_OUTPUT_DIR}")
