//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Tests/Unit/PyBinding/PyInfoTest.cpp
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "BABuild.h"
#include "BAVersion.h"
#include "PyCore/Embed/PyInterpreter.h"
#include "PyCore/Embed/PyObjectPtr.h"
#include "Tests/GTestWrapper/google_test.h"

#include <iostream>

//! Accessing to the information about Python used during the build, content of path.sys variable.

TEST(PyInfo, SysPath)
{
    // Python build info
    std::cout << "pythonExecutable(): " << BABuild::pythonExecutable() << std::endl;
    std::cout << "pythonInterpreterID(): " << BABuild::pythonInterpreterID() << std::endl;
    std::cout << "pythonVersionString(): " << BABuild::pythonVersionString() << std::endl;
    std::cout << "pythonLibraries(): " << BABuild::pythonLibraries() << std::endl;
    std::cout << "pythonIncludeDirs(): " << BABuild::pythonIncludeDirs() << std::endl;
    std::cout << "pythonSiteLib(): " << BABuild::pythonSiteLib() << std::endl;

    // BornAgain build
    std::cout << "buildLibDir(): " << BABuild::buildLibDir() << std::endl;
    std::cout << "pythonPackDir(): " << BABuild::pythonPackDir() << std::endl;

    // Runtime info

    // initialize Python interpreter
    PyInterpreter::initialize();
    EXPECT_TRUE(PyInterpreter::isInitialized());

    std::string runtime_info = PyInterpreter::runtimeInfo();
    EXPECT_TRUE(!runtime_info.empty());
    std::cout << "Python runtime info:" << std::endl;
    std::cout << runtime_info << std::endl;
}

//! Comparing results of GetVersionNumber() function obtained in "embedded" and "native C++" ways.

TEST(PyInfo, GetAttribute)
{
    // initialize Python interpreter
    PyInterpreter::initialize();
    EXPECT_TRUE(PyInterpreter::isInitialized());

    const std::string ba_pypack_dir{BABuild::pythonPackDir()};
    std::cout << "BornAgain Python package: '" << ba_pypack_dir << "'\n";

    PyObjectPtr ba_mod = PyInterpreter::BornAgain::import(ba_pypack_dir);
    EXPECT_TRUE(ba_mod.valid());

    const std::string ba_vstr{BornAgain::version};
    const std::string py_vstr{
        PyInterpreter::getStrAttribute(ba_mod.get(), std::string("version_str"))};
    std::cout << "BornAgain version via Python: '" << py_vstr << "'\n";
    std::cout << "BornAgain version from Core:  '" << ba_vstr << "'\n";
    if (py_vstr != ba_vstr) {
        std::cout << "Version numbers disagree, test will fail.\n"
                  << "Possible reasons:\n"
                  << "- Python bindings not regenerated\n"
                  << "- Python module load from wrong location\n";
        EXPECT_TRUE(false);
    }
    EXPECT_TRUE(true);
}
