#include "Sample/HardParticle/Sphere.h"

#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Sample/Particle/Compound.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(Compound, CompoundDefaultConstructor)
{
    std::unique_ptr<Compound> composition(new Compound);
    std::vector<R3> positions;
    positions.emplace_back();
    EXPECT_EQ(0u, composition->nbrParticles());
}

TEST(Compound, CompoundClone)
{
    Compound composition;
    R3 position = R3(1.0, 1.0, 1.0);
    Material material = RefractiveMaterial("Vacuum", 0.0, 0.0);
    Sphere ff(5.);
    Particle particle(material, ff);
    composition.addComponent(particle, position);

    std::unique_ptr<Compound> clone(composition.clone());
    std::vector<const INode*> children = clone->nodeChildren();
    EXPECT_EQ(children.size(), 1u);
    const auto* p_particle = dynamic_cast<const IParticle*>(children[0]);
    EXPECT_EQ(p_particle->rotation(), nullptr);
    EXPECT_EQ(p_particle->particlePosition(), position);
}
