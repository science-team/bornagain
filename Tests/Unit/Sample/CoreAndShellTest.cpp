#include "Sample/Particle/CoreAndShell.h"

#include "Base/Const/Units.h"
#include "Sample/HardParticle/HardParticles.h"
#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(CoreAndShell, ComplexCoreShellClone)
{
    Material core_material = RefractiveMaterial("Ag", 1.245e-5, 5.419e-7);
    Material shell_material = RefractiveMaterial("AgO2", 8.600e-6, 3.442e-7);

    double shell_length(50);
    double shell_width(20);
    double shell_height(10);
    double core_length = shell_length / 2;
    double core_width = shell_width / 2;
    double core_height = shell_height / 2;

    R3 relative_pos(0, 0, (shell_height - core_height) / 2);

    Particle core(core_material, Box(core_length, core_width, core_height));
    core.translate(relative_pos);
    Particle shell(shell_material, Box(shell_length, shell_width, shell_height));
    CoreAndShell coreshell(core, shell);
    coreshell.rotate(RotationY(90 * Units::deg));
    coreshell.translate(R3(0, 0, -10));

    CoreAndShell* clone = coreshell.clone();
    EXPECT_EQ(coreshell.coreParticle()->particlePosition(), relative_pos);
    EXPECT_EQ(clone->coreParticle()->particlePosition(), relative_pos);
    delete clone;
}
