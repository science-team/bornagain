#include "Sample/Lattice/Lattice2D.h"

#include "Tests/GTestWrapper/google_test.h"

TEST(Lattice2D, basicLatticeClone)
{
    const double length1(1.0), length2(2.0), angle(3.0), xi(4.0);
    BasicLattice2D lattice(length1, length2, angle, xi);

    std::unique_ptr<Lattice2D> clone(lattice.clone());
    EXPECT_EQ(clone->length1(), length1);
    EXPECT_EQ(clone->length2(), length2);
    EXPECT_EQ(clone->latticeAngle(), angle);
    EXPECT_EQ(clone->rotationAngle(), xi);
}
