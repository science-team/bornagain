#include "Sample/Interface/Roughness.h"

#include "Tests/GTestWrapper/google_test.h"

// test clone Roughness
TEST(Roughness, RoughnessClone)
{
    SelfAffineFractalModel originalAutocorrelation(3.1, 0.2, 3.3);
    ErfTransient transient;
    Roughness original(&originalAutocorrelation, &transient);

    Roughness* clone = original.clone();
    const SelfAffineFractalModel* cloneAC =
        dynamic_cast<const SelfAffineFractalModel*>(clone->autocorrelationModel());

    EXPECT_TRUE(cloneAC);
    EXPECT_EQ(cloneAC->sigma(), originalAutocorrelation.sigma());
    EXPECT_EQ(cloneAC->hurst(), originalAutocorrelation.hurst());
    EXPECT_EQ(cloneAC->lateralCorrLength(), originalAutocorrelation.lateralCorrLength());
    delete clone;
}
