#include "Sample/Interface/LayerRoughness.h"

#include "Tests/GTestWrapper/google_test.h"

// test clone LayerRoughness
TEST(LayerRoughness, LayerRoughnessClone)
{
    K_CorrelationModel originalAutocorrelation(3.1, 0.2, 3.3);
    ErfInterlayer interlayer;
    LayerRoughness original(&originalAutocorrelation, &interlayer);

    LayerRoughness* clone = original.clone();
    const K_CorrelationModel* cloneAC =
        dynamic_cast<const K_CorrelationModel*>(clone->autocorrelationModel());

    EXPECT_TRUE(cloneAC);
    EXPECT_EQ(cloneAC->sigma(), originalAutocorrelation.sigma());
    EXPECT_EQ(cloneAC->hurst(), originalAutocorrelation.hurst());
    EXPECT_EQ(cloneAC->lateralCorrLength(), originalAutocorrelation.lateralCorrLength());
    delete clone;
}
