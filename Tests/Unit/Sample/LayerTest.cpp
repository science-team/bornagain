#include "Sample/Multilayer/Layer.h"

#include "Base/Const/Units.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Tests/GTestWrapper/google_test.h"

TEST(Layer, LayerGetAndSet)
{
    Material vacuum = RefractiveMaterial("Vacuum", 0, 0);
    Layer layer(vacuum, 10 * Units::nm);

    std::unique_ptr<Layer> clone(layer.clone());
    EXPECT_EQ(vacuum, *clone->material());
    EXPECT_EQ(0u, clone->layouts().size());
    EXPECT_EQ(10, clone->thickness());
    EXPECT_EQ(clone->numberOfLayouts(), 0u);
}
