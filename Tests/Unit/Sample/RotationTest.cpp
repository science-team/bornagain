#include "Base/Vector/RotMatrix.h"

#include "Sample/Scattering/Rotations.h"
#include "Tests/GTestWrapper/google_test.h"
#include <memory>

TEST(Rotation, XRotations)
{
    double angle = 1.0;
    RotationX rot(angle);
    auto rot_matrix = rot.rotMatrix();
    auto P_rot = std::unique_ptr<IRotation>(IRotation::createRotation(rot_matrix));
    auto* p_rot_cast = dynamic_cast<RotationX*>(P_rot.get());
    ASSERT_NE(p_rot_cast, nullptr);
    EXPECT_DOUBLE_EQ(p_rot_cast->angle(), angle);
}

TEST(Rotation, YRotations)
{
    double angle = 1.0;
    RotationY rot(angle);
    auto rot_matrix = rot.rotMatrix();
    auto P_rot = std::unique_ptr<IRotation>(IRotation::createRotation(rot_matrix));
    auto* p_rot_cast = dynamic_cast<RotationY*>(P_rot.get());
    ASSERT_NE(p_rot_cast, nullptr);
    EXPECT_DOUBLE_EQ(p_rot_cast->angle(), angle);
}

TEST(Rotation, ZRotations)
{
    double angle = 1.0;
    RotationZ rot(angle);
    auto rot_matrix = rot.rotMatrix();
    auto P_rot = std::unique_ptr<IRotation>(IRotation::createRotation(rot_matrix));
    auto* p_rot_cast = dynamic_cast<RotationZ*>(P_rot.get());
    ASSERT_NE(p_rot_cast, nullptr);
    EXPECT_DOUBLE_EQ(p_rot_cast->angle(), angle);
}

TEST(Rotation, EulerRotations)
{
    const double epsilon = 1e-12;
    double alpha = 1.0;
    double beta = 0.2;
    double gamma = -0.5;
    RotationEuler rot(alpha, beta, gamma);
    auto rot_matrix = rot.rotMatrix();
    auto P_rot = std::unique_ptr<IRotation>(IRotation::createRotation(rot_matrix));
    auto* p_rot_cast = dynamic_cast<RotationEuler*>(P_rot.get());
    ASSERT_NE(p_rot_cast, nullptr);
    EXPECT_NEAR(p_rot_cast->alpha(), alpha, epsilon);
    EXPECT_NEAR(p_rot_cast->beta(), beta, epsilon);
    EXPECT_NEAR(p_rot_cast->gamma(), gamma, epsilon);
}
