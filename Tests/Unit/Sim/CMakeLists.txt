include(GoogleTest) # provides gtest_discover_tests

set(test UnitTestSim)

file(GLOB source_files "*.cpp" ${CMAKE_SOURCE_DIR}/Tests/GTestWrapper/TestAll.cpp)

add_executable(${test} ${source_files})
target_compile_definitions(${test} PRIVATE -DBORNAGAIN_PYTHON)
target_link_libraries(${test} BornAgainSim gtest)

gtest_discover_tests(${test} DISCOVERY_TIMEOUT 300 TEST_PREFIX Unit.Sim.)
