#include "Sim/Fitting/FitObjective.h"
#include "Tests/GTestWrapper/google_test.h"
#include "Tests/Unit/Sim/FittingTestHelper.h"

TEST(FitObjective, twoDatasets)
{
    // creating two simulation builders
    FittingTestHelper helper1(2, 3);
    simulation_builder_t builder1 = [&](const mumufit::Parameters& pars) {
        return helper1.createSimulation(pars);
    };

    FittingTestHelper helper2(3, 4);
    simulation_builder_t builder2 = [&](const mumufit::Parameters& pars) {
        return helper2.createSimulation(pars);
    };

    // creating two datasets
    const double exp_value1 = 10.0;
    const double exp_value2 = 20.0;
    auto data1 = helper1.createTestData(exp_value1);
    auto data2 = helper2.createTestData(exp_value2);

    // creating FitObjective with two simulation/data pairs.
    FitObjective objective;
    objective.addFitPair(builder1, *data1);
    objective.addFitPair(builder2, *data2);

    // running simulation once
    mumufit::Parameters params;
    objective.evaluate(params);

    // checking flat array with experimental data made of two Datafield
    std::vector<double> expected_exp1(helper1.size(), exp_value1);
    std::vector<double> expected_exp2(helper2.size(), exp_value2);
    expected_exp1.insert(expected_exp1.end(), expected_exp2.begin(), expected_exp2.end());

    EXPECT_EQ(expected_exp1, objective.flatExpData());
}
