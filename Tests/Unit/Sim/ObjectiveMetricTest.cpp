#include "Sim/Fitting/ObjectiveMetric.h"
#include "Sim/Fitting/ObjectiveMetricUtil.h"
#include "Tests/GTestWrapper/google_test.h"
#include <cmath>

TEST(ObjectiveMetric, Chi2)
{
    std::vector<double> sim_data{1.0, 2.0, 3.0, 4.0};
    std::vector<double> exp_data{2.0, 1.0, 4.0, 3.0};
    std::vector<double> uncertainties{0.1, 0.1, 0.5, 0.5};

    Chi2Metric metric;

    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, sim_data, uncertainties), 0.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, sim_data), 0.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data, uncertainties), 208.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data), 4.0);

    metric.setNorm(ObjectiveMetricUtil::l1Norm());
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data, uncertainties), 24.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data), 4.0);
}

TEST(ObjectiveMetric, PoissonLike)
{
    std::vector<double> sim_data{1.0, 2.0, 4.0, 4.0};
    std::vector<double> exp_data{2.0, 1.0, 5.0, 3.0};

    PoissonLikeMetric metric;

    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, sim_data), 0.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data), 2.);

    metric.setNorm(ObjectiveMetricUtil::l1Norm());

    std::vector<double> sim_data_2 = sim_data;
    sim_data_2[1] = 1.0;
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data_2, exp_data), 2.);

    EXPECT_DOUBLE_EQ(metric.computeFromArrays({}, {}, {}), 0.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays({}, {}), 0.0);
}

TEST(ObjectiveMetric, Log)
{
    std::vector<double> sim_data{1.0, 10.0, 1e2, 1e4};
    std::vector<double> exp_data{10.0, 1.0, 1e3, 1e5};
    std::vector<double> uncertainties{0.1, 0.1, 0.5, 0.5};

    LogMetric metric;

    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, sim_data, uncertainties), 0.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, sim_data), 0.0);
    EXPECT_NEAR(metric.computeFromArrays(sim_data, exp_data, uncertainties), 2.12097e11, 1e6);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data), 4.0);

    metric.setNorm(ObjectiveMetricUtil::l1Norm());
    EXPECT_NEAR(metric.computeFromArrays(sim_data, exp_data, uncertainties), 465375, 0.5);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data), 4.0);

    EXPECT_DOUBLE_EQ(metric.computeFromArrays({}, {}, {}), 0.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays({}, {}), 0.0);
}

TEST(ObjectiveMetric, meanRelativeDifference)
{
    std::vector<double> sim_data{1.0, 2.0, 4.0, 4.0};
    std::vector<double> exp_data{2.0, 1.0, 2.0, 2.0};
    std::vector<double> uncertainties{1.0, 1.0, 2.0, 1.0};

    meanRelativeDifferenceMetric metric;

    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, sim_data), 0.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data), 4.0 / 9);

    metric.setNorm(ObjectiveMetricUtil::l1Norm());
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data), 4.0 / 3);

    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data, uncertainties), 5.0);

    EXPECT_DOUBLE_EQ(metric.computeFromArrays({}, {}, {}), 0.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays({}, {}), 0.0);
}

TEST(ObjectiveMetric, RQ4)
{
    std::vector<double> sim_data{1.0, 2.0, 4.0, 4.0};
    std::vector<double> exp_data{2.0, 1.0, 2.0, 2.0};
    std::vector<double> uncertainties{1.0, 1.0, 2.0, 1.0};

    RQ4Metric metric;

    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, sim_data), 0.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data), 10.0);

    metric.setNorm(ObjectiveMetricUtil::l1Norm());
    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data), 6.0);

    EXPECT_DOUBLE_EQ(metric.computeFromArrays(sim_data, exp_data, uncertainties), 5.0);

    EXPECT_DOUBLE_EQ(metric.computeFromArrays({}, {}, {}), 0.0);
    EXPECT_DOUBLE_EQ(metric.computeFromArrays({}, {}), 0.0);
}

TEST(ObjectiveMetric, createMetric)
{
    auto result = ObjectiveMetricUtil::createMetric("Poisson-like");
    EXPECT_TRUE(dynamic_cast<PoissonLikeMetric*>(result.get()));
    // Since norm functions lack equality comparison, check the equality of applying them
    EXPECT_DOUBLE_EQ(result->norm()(2.0), ObjectiveMetricUtil::l2Norm()(2.0));

    result = ObjectiveMetricUtil::createMetric("Poisson-Like", "L1");
    EXPECT_TRUE(dynamic_cast<PoissonLikeMetric*>(result.get()));
    EXPECT_DOUBLE_EQ(result->norm()(2.0), ObjectiveMetricUtil::l1Norm()(2.0));

    result = ObjectiveMetricUtil::createMetric("poisson-like", "l1");
    EXPECT_TRUE(dynamic_cast<PoissonLikeMetric*>(result.get()));
    EXPECT_DOUBLE_EQ(result->norm()(2.0), ObjectiveMetricUtil::l1Norm()(2.0));

    result = ObjectiveMetricUtil::createMetric("poissoN-likE", "L2");
    EXPECT_TRUE(dynamic_cast<PoissonLikeMetric*>(result.get()));
    EXPECT_DOUBLE_EQ(result->norm()(2.0), ObjectiveMetricUtil::l2Norm()(2.0));

    result = ObjectiveMetricUtil::createMetric("poisson-like");
    EXPECT_TRUE(dynamic_cast<PoissonLikeMetric*>(result.get()));
    EXPECT_DOUBLE_EQ(result->norm()(2.0), ObjectiveMetricUtil::l2Norm()(2.0));

    result = ObjectiveMetricUtil::createMetric("chi2");
    EXPECT_TRUE(dynamic_cast<Chi2Metric*>(result.get()));

    result = ObjectiveMetricUtil::createMetric("Chi2");
    EXPECT_TRUE(dynamic_cast<Chi2Metric*>(result.get()));

    result = ObjectiveMetricUtil::createMetric("Log");
    EXPECT_TRUE(dynamic_cast<LogMetric*>(result.get()));

    result = ObjectiveMetricUtil::createMetric("log");
    EXPECT_TRUE(dynamic_cast<LogMetric*>(result.get()));

    result = ObjectiveMetricUtil::createMetric("reldiff");
    EXPECT_TRUE(dynamic_cast<meanRelativeDifferenceMetric*>(result.get()));

    result = ObjectiveMetricUtil::createMetric("RelDiff");
    EXPECT_TRUE(dynamic_cast<meanRelativeDifferenceMetric*>(result.get()));

    // TODO restore rq4:
    // https://jugit.fz-juelich.de/mlz/bornagain/-/issues/568
    //    result = ObjectiveMetricUtil::createMetric("RQ4");
    //    EXPECT_TRUE(dynamic_cast<RQ4Metric*>(result.get()));

    //    result = ObjectiveMetricUtil::createMetric("rq4");
    //    EXPECT_TRUE(dynamic_cast<RQ4Metric*>(result.get()));
}
