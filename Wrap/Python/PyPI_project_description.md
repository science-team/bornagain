BornAgain is a software to simulate and fit neutron and x-ray
reflectometry and scattering at grazing incidence (GISANS and GISAXS),
using the distorted-wave Born approximation (DWBA).

This Python wheel contains the BornAgain Python module, which
is built on top of the BornAgain C++ core. This is all what is
needed to use BornAgain through its Python API.

If the BornAgain GUI shall also be used, then another installation
method must be chosen, see the
[BornAgain installation instructions](https://bornagainproject.org/latest/installation).

#### Homepage
https://www.bornagainproject.org

#### Copyright
[Forschungszentrum Jülich GmbH](http://www.fz-juelich.de) 2013-

#### License
GNU General Public License v3 or higher
(see [COPYING](https://jugit.fz-juelich.de/mlz/bornagain/-/blob/main/COPYING))

### Citation
See [CITATION](https://jugit.fz-juelich.de/mlz/bornagain/-/blob/main/CITATION))
