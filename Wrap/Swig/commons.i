%feature("autodoc");

%include "stdint.i"
%include "std_complex.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"

%include "deprecation.i"
%include "warnings.i"

#define SWIG_FILE_WITH_INIT

%{
#define SWIG_FILE_WITH_INIT
%}

#define GCC_DIAG_OFF(x)
#define GCC_DIAG_ON(x)

#ifndef BORNAGAIN_PYTHON
#define BORNAGAIN_PYTHON
#endif

%template(vdouble1d_T) std::vector<double>;
%template(vdouble2d_T) std::vector<std::vector<double>>;
%template(vector_integer_T) std::vector<int>;
%template(vinteger2d_T) std::vector<std::vector<int>>;
%template(vector_longinteger_T) std::vector<unsigned long int>;
%template(vector_complex_T) std::vector< std::complex<double>>;
%template(vector_string_T) std::vector<std::string>;
%template(map_string_double_T) std::map<std::string, double>;
%template(pvacuum_double_T) std::pair<double, double>;
%template(vector_pvacuum_double_T) std::vector<std::pair<double, double>>;

// Automatic exception handling:

%include "exception.i"

%{
// Convert expanded macro s to C string.
#define xstr(s) #s

#include "Base/Util/Assert.h"

// Enhance an error message.
// Macro SWIG_BA_VERSION must be set when Swig is invoked (currently this is done in function SwigLib).

std::string bug_msg(const bug&ex) {
    std::string msg = "Failed C++ assertion in BornAgain-" xstr(SWIG_BA_VERSION) " catched by Python:\n";
    msg.append(ex.what());
    msg.append(
        "\n\n"
        "Please help us to fix this bug by reporting the above to the maintainers:\n"
        "- https://jugit.fz-juelich.de/mlz/bornagain/-/issues/new or\n"
        "- mail to contact@bornagainproject.org.\n"
        "See also\n"
        "- https://bornagainproject.org/latest/howto/get-help.");
    return msg;
}

std::string exception_msg(const std::exception& ex) {
    std::string msg = "C++ exception in BornAgain-" xstr(SWIG_BA_VERSION) " catched by Python:\n";
    msg.append(ex.what());
    return msg;
}

%}

// Convert C++ exception to Python exception;
//   see https://www.swig.org/Doc4.1/Library.html#Library_stl_exceptions.

%exception {
    try {
        $action
    } catch (const bug& ex) {
	SWIG_exception(SWIG_RuntimeError, bug_msg(ex).c_str());
    } catch (const std::exception& ex) {
	SWIG_exception(SWIG_RuntimeError, exception_msg(ex).c_str());
    }
}

// Propagate Python exceptions;
//   source: https://stackoverflow.com/q/4811492.
%feature("director:except") {
    if( $error != NULL ) {
        PyObject *ptype, *pvalue, *ptraceback;
        PyErr_Fetch( &ptype, &pvalue, &ptraceback );
        PyErr_Restore( ptype, pvalue, ptraceback );
        PyErr_Print();
        Py_Exit(1);
    }
}
