%feature("autodoc");

%include "stdint.i"
%include "std_complex.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"

%include "deprecation.i"
%include "warnings.i"

#define SWIG_FILE_WITH_INIT

%{
#define SWIG_FILE_WITH_INIT
%}

#define GCC_DIAG_OFF(x)
#define GCC_DIAG_ON(x)

#ifndef BORNAGAIN_PYTHON
#define BORNAGAIN_PYTHON
#endif

%template(vdouble1d_T) std::vector<double>;
%template(vdouble2d_T) std::vector<std::vector<double>>;
%template(vector_integer_T) std::vector<int>;
%template(vinteger2d_T) std::vector<std::vector<int>>;
%template(vector_longinteger_T) std::vector<unsigned long int>;
%template(vector_complex_T) std::vector< std::complex<double>>;
%template(vector_string_T) std::vector<std::string>;
%template(map_string_double_T) std::map<std::string, double>;
%template(pvacuum_double_T) std::pair<double, double>;
%template(vector_pvacuum_double_T) std::vector<std::pair<double, double>>;

// Automatic exception handling: Convert C++ exceptions to Python exceptions;
// see <https://www.swig.org/Doc4.1/Library.html#Library_stl_exceptions>

%include "exception.i"

%exception {
  try {
    $action
  } catch (const std::exception& ex) {
      // message shown in the Python interpreter
      const std::string msg {"BornAgain C++ Exception: " + std::string(ex.what())};
      SWIG_exception(SWIG_RuntimeError, msg.c_str());
  }
}


// Propagate Python exceptions
// source: <https://stackoverflow.com/q/4811492>
%feature("director:except") {
    if( $error != NULL ) {
        PyObject *ptype, *pvalue, *ptraceback;
        PyErr_Fetch( &ptype, &pvalue, &ptraceback );
        PyErr_Restore( ptype, pvalue, ptraceback );
        PyErr_Print();
        Py_Exit(1);
    }
}
