%import(module="libBornAgainBase") <heinz/Complex.h>
%import(module="libBornAgainBase") <heinz/Vectors3D.h>
%ignore ICloneable;
%import(module="libBornAgainBase") "Base/Type/ICloneable.h"
%import(module="libBornAgainBase") "Base/Type/Span.h"
%import(module="libBornAgainBase") "Base/Vector/RotMatrix.h"
%import(module="libBornAgainBase") "Base/Axis/Scale.h"
%import(module="libBornAgainBase") "Base/Axis/Frame.h"
%import(module="libBornAgainBase") "Base/Py/ArrayWrapper.h"

%template(R3) Vec3<double>;
%template(C3) Vec3<std::complex<double>>;
%template(vector_R3) std::vector<Vec3<double>>;
