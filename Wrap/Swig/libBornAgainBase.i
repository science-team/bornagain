// ************************************************************************** //
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Wrap/Swig/libBornAgainBase.i
//! @brief     SWIG interface file for libBornAgainBase
//!
//! @homepage  http://apps.jcns.fz-juelich.de/BornAgain
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2013
//! @authors   Scientific Computing Group at MLZ Garching
//
// ************************************************************************** //

%module(moduleimport="import $module") "libBornAgainBase"

%include "commons.i"

%{
#include <heinz/Complex.h>
#include "Base/Axis/Frame.h"
#include "Base/Axis/Scale.h"
#include "Base/Axis/MakeScale.h"
#include "Base/Const/Units.h"
#include "Base/Math/Numeric.h"
#include "Base/Type/ICloneable.h"
#include "Base/Type/Span.h"
#include "Base/Vector/RotMatrix.h"
#include "Base/Py/ArrayWrapper.h"
%}

%include "heinz/Complex.h"
%ignore ICloneable;
%include "Base/Type/ICloneable.h"
%include "Base/Type/Span.h"

%include "Base/Const/Units.h"
%include "Base/Math/Numeric.h"

%include "heinz/Vectors3D.h"
%include "Base/Vector/RotMatrix.h"

%include "Base/Axis/Scale.h"
%include "Base/Axis/MakeScale.h"
%include "Base/Axis/Frame.h"

%include "Base/Py/ArrayWrapper.h"

%template(R3) Vec3<double>;
%template(C3) Vec3<std::complex<double>>;
