// ************************************************************************** //
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Wrap/Swig/libBornAgainDevice.i
//! @brief     SWIG interface file for libBornAgainDevice
//!
//!            Configuration is done in Device/CMakeLists.txt
//!
//! @homepage  http://apps.jcns.fz-juelich.de/BornAgain
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2013
//! @authors   Scientific Computing Group at MLZ Garching
//
// ************************************************************************** //

%module(moduleimport="import $module") "libBornAgainDevice"

%include "commons.i"

%{
#include "Base/Axis/Frame.h"
#include "Base/Axis/Scale.h"
#include "Base/Spin/SpinMatrix.h"
#include "Device/Analyze/Peaks.h"
#include "Device/Beam/Beam.h"
#include "Device/Beam/FootprintGauss.h"
#include "Device/Beam/FootprintSquare.h"
#include "Device/Data/Datafield.h"
#include "Device/Detector/SphericalDetector.h"
#include "Device/Detector/OffspecDetector.h"
#include "Device/IO/DiffUtil.h"
#include "Device/IO/IOFactory.h"
#include "Device/IO/ImportSettings.h"
#include "Device/Mask/Ellipse.h"
#include "Device/Mask/Line.h"
#include "Device/Mask/Polygon.h"
#include "Device/Mask/Rectangle.h"
#include "Device/Resolution/IDetectorResolution.h"
#include "Device/Resolution/ResolutionFunction2DGaussian.h"
#include "Param/Distrib/ParameterDistribution.h"
%}

%include "fromBase.i"
%include "fromParam.i"

%import(module="libBornAgainFit") "Fit/Param/AttLimits.h"
%import(module="libBornAgainFit") "Fit/Param/Parameters.h"
%import(module="libBornAgainFit") "Fit/Param/Parameter.h"

%include "Base/Axis/Coordinate.h" // required by ImportSettings

%include "Device/Data/Datafield.h"

%include "Device/Beam/Beam.h"
%include "Device/Beam/IFootprint.h"
%include "Device/Beam/FootprintGauss.h"
%include "Device/Beam/FootprintSquare.h"

%include "Device/Mask/IShape2D.h"
%include "Device/Mask/Ellipse.h"
%include "Device/Mask/Line.h"
%include "Device/Mask/Polygon.h"
%include "Device/Mask/Rectangle.h"

%include "Device/Resolution/IDetectorResolution.h"
%include "Device/Resolution/IResolutionFunction2D.h"
%include "Device/Resolution/ResolutionFunction2DGaussian.h"

%include "Device/Detector/IDetector.h"
%include "Device/Detector/SphericalDetector.h"
%include "Device/Detector/OffspecDetector.h"

%include "Device/IO/DiffUtil.h"
%include "Device/IO/ImportSettings.h"
%include "Device/IO/IOFactory.h"

%include "Device/Analyze/Peaks.h"
