// ************************************************************************** //
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Wrap/Swig/libBornAgainParam.i
//! @brief     SWIG interface file for libBornAgainParam
//!
//!            Configuration is done in Param/CMakeLists.txt
//!
//! @homepage  http://apps.jcns.fz-juelich.de/BornAgain
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2013
//! @authors   Scientific Computing Group at MLZ Garching
//
// ************************************************************************** //

%module(moduleimport="import $module") "libBornAgainParam"

%include "commons.i"

%{
#include "Param/Distrib/ParameterSample.h"
#include "Param/Distrib/Distributions.h"
#include "Param/Distrib/ParameterDistribution.h"
%}

%import(module="libBornAgainBase") <heinz/Complex.h>
%ignore ICloneable;
%import(module="libBornAgainBase") "Base/Type/ICloneable.h"

%template(vector_parsample_t) std::vector<ParameterSample>;
%include "Param/Node/INode.h"

%include "Param/Distrib/ParameterSample.h"
%include "Param/Distrib/Distributions.h"
%include "Param/Distrib/ParameterDistribution.h"
