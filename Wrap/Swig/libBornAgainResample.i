// ************************************************************************** //
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Wrap/Swig/libBornAgainResample.i
//! @brief     SWIG interface file for libBornAgainResample
//!
//!            Configuration is done in Resample/CMakeLists.txt
//!
//! @homepage  http://apps.jcns.fz-juelich.de/BornAgain
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2013
//! @authors   Scientific Computing Group at MLZ Garching
//
// ************************************************************************** //

%module(moduleimport="import $module") "libBornAgainResample"

%include "commons.i"

%{
#include "Resample/Option/SimulationOptions.h"
%}

%include "Resample/Option/SimulationOptions.h"

%{
#include <heinz/Vectors3D.h>
#include "Resample/Swig/MultiLayerFuncs.h"
%}

// The following goes verbatim from libBornAgainSim.i to libBornAgainSim_wrap.cxx.
// Note that the order matters, as base classes must be included before derived classes.

%include "fromBase.i"

%include "Resample/Swig/MultiLayerFuncs.h"
