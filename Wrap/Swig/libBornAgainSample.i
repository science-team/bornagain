// ************************************************************************** //
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Wrap/Swig/libBornAgainSample.i
//! @brief     SWIG interface file for libBornAgainSample
//!
//!            Configuration is done in Sample/CMakeLists.txt
//!
//! @homepage  http://apps.jcns.fz-juelich.de/BornAgain
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2013
//! @authors   Scientific Computing Group at MLZ Garching
//
// ************************************************************************** //

%module(moduleimport="import $module") "libBornAgainSample"

%include "commons.i"

%{
#include "Base/Axis/Frame.h"
#include "Base/Type/Span.h"
#include "Base/Vector/RotMatrix.h"
#include "Param/Distrib/ParameterDistribution.h"
#include "Sample/Aggregate/Interference1DLattice.h"
#include "Sample/Aggregate/Interference2DLattice.h"
#include "Sample/Aggregate/Interference2DParacrystal.h"
#include "Sample/Aggregate/Interference2DSuperLattice.h"
#include "Sample/Aggregate/InterferenceFinite2DLattice.h"
#include "Sample/Aggregate/InterferenceHardDisk.h"
#include "Sample/Aggregate/InterferenceNone.h"
#include "Sample/Aggregate/InterferenceRadialParacrystal.h"
#include "Sample/Aggregate/ParticleLayout.h"
#include "Sample/Correlation/IPeakShape.h"
#include "Sample/HardParticle/HardParticles.h"
#include "Sample/Interface/Roughness.h"
#include "Sample/Interface/RoughnessMap.h"
#include "Sample/Lattice/BakeLattice.h"
#include "Sample/Lattice/ISelectionRule.h"
#include "Sample/Lattice/Lattice3D.h"
#include "Sample/Material/MaterialBySLDImpl.h"
#include "Sample/Material/MaterialFactoryFuncs.h"
#include "Sample/Material/RefractiveMaterialImpl.h"
#include "Sample/Multilayer/Layer.h"
#include "Sample/Multilayer/LayerStack.h"
#include "Sample/Multilayer/Sample.h"
#include "Sample/Particle/Compound.h"
#include "Sample/Particle/CoreAndShell.h"
#include "Sample/Particle/Crystal.h"
#include "Sample/Particle/Mesocrystal.h"
#include "Sample/Particle/Particle.h"
#include "Sample/Scattering/Rotations.h"
#include "Sample/SoftParticle/SoftParticles.h"
%}

%include "fromBase.i"
%include "fromParam.i"

%include "Sample/Material/Material.h"
%include "Sample/Material/MaterialFactoryFuncs.h"
%include "Sample/Material/IMaterialImpl.h"
%include "Sample/Material/RefractiveMaterialImpl.h"
%include "Sample/Material/MaterialBySLDImpl.h"

%include "Sample/Scattering/ISampleNode.h"
%include "Sample/Particle/IFormfactor.h"
%include "Sample/Scattering/Rotations.h"

%include "Sample/Particle/Crystal.h"
%include "Sample/Particle/IParticle.h"
%include "Sample/Particle/Mesocrystal.h"
%include "Sample/Particle/Compound.h"
%include "Sample/Particle/CoreAndShell.h"

%include "Sample/Correlation/Profiles1D.h"
%include "Sample/Correlation/Profiles2D.h"
%include "Sample/Correlation/IPeakShape.h"

%include "Sample/Aggregate/IInterference.h"
%include "Sample/Aggregate/Interference1DLattice.h"
%include "Sample/Aggregate/Interference2DLattice.h"
%include "Sample/Aggregate/Interference2DParacrystal.h"
%include "Sample/Aggregate/Interference2DSuperLattice.h"
%include "Sample/Aggregate/InterferenceFinite2DLattice.h"
%include "Sample/Aggregate/InterferenceHardDisk.h"
%include "Sample/Aggregate/InterferenceNone.h"
%include "Sample/Aggregate/InterferenceRadialParacrystal.h"
%include "Sample/Aggregate/ParticleLayout.h"

%include "Sample/Interface/Roughness.h"
%include "Sample/Interface/RoughnessMap.h"

%include "Sample/Multilayer/Layer.h"
%include "Sample/Multilayer/LayerStack.h"
%include "Sample/Multilayer/Sample.h"

%include "Sample/Interface/AutocorrelationModels.h"
%include "Sample/Interface/TransientModels.h"
%include "Sample/Interface/CrosscorrelationModels.h"


%include "Sample/HardParticle/IFormfactorPolyhedron.h"
%include "Sample/HardParticle/IFormfactorPrism.h"
%include "Sample/HardParticle/IProfileRipple.h"

%include "Sample/HardParticle/Polyhedra.h"

%include "Sample/HardParticle/Cone.h"
%include "Sample/HardParticle/Cylinder.h"
%include "Sample/HardParticle/EllipsoidalCylinder.h"
%include "Sample/HardParticle/HemiEllipsoid.h"
%include "Sample/HardParticle/HorizontalCylinder.h"
%include "Sample/HardParticle/Sphere.h"
%include "Sample/HardParticle/SphericalSegment.h"
%include "Sample/HardParticle/Spheroid.h"
%include "Sample/HardParticle/SpheroidalSegment.h"

%include "Sample/HardParticle/Bar.h"
%include "Sample/HardParticle/CosineRipple.h"
%include "Sample/HardParticle/SawtoothRipple.h"

%include "Sample/HardParticle/LongBoxGauss.h"
%include "Sample/HardParticle/LongBoxLorentz.h"

%include "Sample/SoftParticle/Gauss.h"
%include "Sample/SoftParticle/FuzzySphere.h"

%include "Sample/Lattice/ISelectionRule.h"
%include "Sample/Lattice/Lattice3D.h"
%include "Sample/Lattice/Lattice2D.h"
%include "Sample/Lattice/BakeLattice.h"


// defines a function with no arguments and returns a Python Sample object

%ignore BA_SWIG_sampleFromPyObject;
%ignore BA_SWIG_PyFormfactor;

%inline
%{

// creates a C++ Sample instance from Python object which wraps a Sample instance
std::unique_ptr<Sample> BA_SWIG_sampleFromPyObject(void* pSample)
{
    if (!pSample)
        throw std::runtime_error("BA_SWIG_sampleFromPyObject: Sample PyObject is null");

    PyObject* pySample {reinterpret_cast<PyObject*>(pSample)};

    // Construct a C++ object from the Python object.
    // This conversion requires a SWIG-produced Python API.
    swig_type_info* pTypeInfo = SWIG_TypeQuery("Sample*");
    void* argp1 = nullptr;
    const int res = SWIG_ConvertPtr(pySample, &argp1, pTypeInfo, 0);
    if (!SWIG_IsOK(res))
        throw std::runtime_error("BA_SWIG_sampleFromPyObject: "
                                 "PyObject did not yield a Sample instance");

    // clone the Sample instance
    std::unique_ptr<Sample> sample { reinterpret_cast<Sample*>(argp1)->clone() };

    return sample;
}

//========================================

// Wrapper for Python Formfactor class
class BA_SWIG_PyFormfactor: public IFormfactor {
public:
    BA_SWIG_PyFormfactor() = delete;

    BA_SWIG_PyFormfactor(PyObject* pyFormfactor)
    {
        if (!m_isFormfactor(pyFormfactor)) {
            throw std::runtime_error("BA_SWIG_PyFormfactor (SWIG): The given Python object "
                "is not of proper type.");
        }

        m_pyFormfactorObj = pyFormfactor;

        // increase the Python reference count to keep the Python object alive
        Py_INCREF(m_pyFormfactorObj);
    }

    ~BA_SWIG_PyFormfactor() {
        Py_XDECREF(m_pyFormfactorObj);
    }

    double radialExtension() const override {
        throw std::runtime_error("BA_SWIG_PyFormfactor (SWIG): 'radialExtension' method "
                                 "is not defined for the Python formfactor");
    }

    bool contains(const R3&) const override {
        throw std::runtime_error("BA_SWIG_PyFormfactor (SWIG): 'contains' method "
                                 "is not defined for the Python formfactor");
    }

    complex_t formfactor(C3 q) const override {
        return m_pyFormfactor(q);
    }

    Span spanZ(const IRotation* rotation) const override {
        return m_pySpanZ(rotation);
    };

    BA_SWIG_PyFormfactor* clone() const override {
        return new BA_SWIG_PyFormfactor(m_pyFormfactorObj);
    }

    std::string className() const override {
        return "CustomPyFormfactor";
    }

private:

    PyObject* m_pyFormfactorObj = nullptr;

    static bool m_isFormfactor(PyObject* pObj)
        {
        if (!pObj)
            return false;

        const char method_name[] = "formfactor";

        // the Python class is expected to have a 'formfactor' callable method
        if (!PyObject_HasAttrString(pObj, method_name))
            return false;

        PyObject* pMethod = PyObject_GetAttrString(pObj, method_name);

        if (!PyCallable_Check(pMethod)) {
            Py_DECREF(pMethod);
            return false;
        }

        Py_DECREF(pMethod);
        return true;
        }

    complex_t m_pyFormfactor(C3 q) const
        {
        // call a method which takes a single Python object as input
        PyObject* pMethod = PyObject_GetAttrString(m_pyFormfactorObj, "formfactor");

        // make a Python-wrapped C++ instance as the call argument
        // NOTE: PyObject* SWIG_NewPointerObj(void* ptr, swig_type_info* ty, int own)
        // creates a new Python pointer object.
        C3* C3_ptr = &q;
        swig_type_info* pTypeInfo_C3 = SWIG_TypeQuery("C3*");
        PyObject* arg1 = SWIG_NewPointerObj(SWIG_as_voidptr(C3_ptr), pTypeInfo_C3, 0);
        PyObject* pResult = PyObject_CallFunctionObjArgs(pMethod, arg1, NULL);
        Py_DECREF(arg1);
        Py_DECREF(pMethod);

        if (!pResult) {
            PyErr_SetString(PyExc_RuntimeError,
              "m_pyFormfactor (SWIG): Calling Python method 'formfactor' failed");
            return 0;
        }

        complex_t result;

        // check for Python numerical types: complex, float and int
        if(PyComplex_Check(pResult)) {
            result = complex_t{PyComplex_RealAsDouble(pResult),
                               PyComplex_ImagAsDouble(pResult)};
        } else if(PyFloat_Check(pResult)) {
            result = PyFloat_AsDouble(pResult);
        } else if(PyLong_Check(pResult)) {
            result = PyLong_AsDouble(pResult);
        } else {
            Py_DECREF(pResult);
            PyErr_SetString(PyExc_RuntimeError,
              "m_pyFormfactor (SWIG): Calling Python method 'formfactor' did not produce complex, float or int value");
            return 0;
        }

        Py_DECREF(pResult);

        return result;
    }

    Span m_pySpanZ(const IRotation* rotation) const
        {
        // call a method which takes a single Python object as input
        PyObject* pMethod = PyObject_GetAttrString(m_pyFormfactorObj, "spanZ");

        if (!pMethod)
            throw std::runtime_error("m_pySpanZ (SWIG): "
              "Python object has no 'spanZ' method");

        // make a Python-wrapped C++ instance as the call argument
        // NOTE: PyObject* SWIG_NewPointerObj(void* ptr, swig_type_info* ty, int own)
        // creates a new Python pointer object.
        IRotation* IRotation_ptr = const_cast<IRotation*>(rotation);
        swig_type_info* pTypeInfo_IRotation = SWIG_TypeQuery("IRotation*");
        PyObject* arg1 = SWIG_NewPointerObj(SWIG_as_voidptr(IRotation_ptr),
                                            pTypeInfo_IRotation, 0);
        PyObject* pResult = PyObject_CallFunctionObjArgs(pMethod, arg1, NULL);
        Py_DECREF(arg1);
        Py_DECREF(pMethod);

        if (!pResult) {
            throw std::runtime_error("m_pySpanZ (SWIG): "
              "Calling Python method 'spanZ' failed");
        }

        // extract the C++ value from the resulting Python object
        swig_type_info* pTypeInfo_Span = SWIG_TypeQuery("Span*");
        void* Span_ptr = nullptr;
        int res = SWIG_ConvertPtr(pResult, &Span_ptr, pTypeInfo_Span,  0);

        if (!SWIG_IsOK(res))
            throw std::runtime_error("m_pySpanZ (SWIG): "
              "Calling Python 'spanZ' method did not yield a 'Span' instance");

        Span result = *(reinterpret_cast<Span*>(Span_ptr));
        Py_DECREF(pResult);

        return result;
    }
};
%}

%include "Sample/Particle/Particle.h"

%extend Particle {
// new constructor for the Python interface (see <https://stackoverflow.com/a/33585437>)
// NOTE: The extra final argument is unused and meant only to differentiate unambiguously
// the call signature from the main constructor defined in the C++ code.

Particle(const Material& material, PyObject* pyFormfactor, const int)
{
    // wrap the PythonObject and produce a Particle from the wrapped object
    BA_SWIG_PyFormfactor ff_wrapper(pyFormfactor);
    Particle* new_particle = new Particle(material, ff_wrapper);
    return new_particle;
}

};

// extend the Python class
%pythoncode %{

class Particle(Particle):
    def __init__(self, material_, ff_):
        if isinstance(ff_, IFormfactor):
            super().__init__(material_, ff_)
        elif hasattr(ff_, 'formfactor'):
            super().__init__(material_, ff_, 0)
        else:
            raise TypeError("Particle: The given Python object is not a proper Formfactor. "
               "A minimal Formfactor object must have at least a 'formfactor' method "
               "which takes a 'C3' (vector) instance as argument and "
               "returns a complex or float.")

%};
