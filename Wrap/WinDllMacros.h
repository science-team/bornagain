//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      Wrap/WinDllMacros.h
//! @brief     Defines export/import macros for global data symbols.
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2015
//! @authors   Scientific Computing Group at MLZ Garching
//
//  ************************************************************************************************

#ifndef BORNAGAIN_WRAP_WINDLLMACROS_H
#define BORNAGAIN_WRAP_WINDLLMACROS_H

//! Macros for export/import global functions and variables (Windows only).
//!
//! Needed for Windows, empty for Linux and Mac.
//!
//! Note that classes and other non-data symbols are exported automatically thanks to
//! the CMake variable CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS.
//!
//! One such macro is needed for each component library that exports data sybols.

#ifdef _WIN32

#ifdef BA_BASE_BUILD_DLL
#define BA_BASE_API_ __declspec(dllexport)
#else
#define BA_BASE_API_ __declspec(dllimport)
#endif // BA_BASE_BUILD_DLL

#ifdef BA_DEVICE_BUILD_DLL
#define BA_DEVICE_API_ __declspec(dllexport)
#else
#define BA_DEVICE_API_ __declspec(dllimport)
#endif // BA_DEVICE_BUILD_DLL

#ifdef BA_GUI_BUILD_DLL
#define BA_GUI_API_ __declspec(dllexport)
#else
#define BA_GUI_API_ __declspec(dllimport)
#endif // BA_GUI_BUILD_DLL

#else // not _WIN32

#define BA_BASE_API_
#define BA_DEVICE_API_
#define BA_GUI_API_

#endif // _WIN32

#endif // BORNAGAIN_WRAP_WINDLLMACROS_H
