#!/usr/bin/env python3
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, std_samples, std_simulations

material_particle = ba.RefractiveMaterial("Particle", 1e-6, 0)
ff = ba.Icosahedron(5.13*nm)


def get_sample(omega):
    particle = ba.Particle(material_particle, ff)
    particle.rotate(ba.RotationY(omega*deg))
    return std_samples.sas_sample_with_particle(particle)


def get_simulation(sample):
    n = 201
    return std_simulations.sas(sample, n)


if __name__ == '__main__':
    titles = ['face normal', 'vertex normal', 'edge normal']
    angles = [48.1897, -52.6226, 69.0948]
    results = []
    for i in range(len(angles)):
        sample = get_sample(angles[i])
        simulation = get_simulation(sample)
        result = simulation.simulate()
        result.setTitle(titles[i])
        results.append(result)

    bp.plot_to_row(results)
    bp.plt.show()
