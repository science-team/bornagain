#!/usr/bin/env python3
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, std_samples, std_simulations

material_particle = ba.RefractiveMaterial("Particle", 1e-6, 0)
ff = ba.Spheroid(6.85*nm, 1.5*nm)


def get_sample(omega):
    particle = ba.Particle(material_particle, ff)
    particle.rotate(ba.RotationY(omega*deg))
    return std_samples.sas_sample_with_particle(particle)


def get_simulation(sample):
    n = 201
    return std_simulations.sas(sample, n)


if __name__ == '__main__':
    results = []
    for omega in [0, 45, 90]:
        title = r'$\theta=%d^\circ$' % omega
        sample = get_sample(omega)
        simulation = get_simulation(sample)
        result = simulation.simulate()
        result.setTitle(title)
        results.append(result)

    bp.plot_to_row(results)
    bp.plt.show()
