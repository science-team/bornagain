#!/usr/bin/env python3
"""
Fitting experimental data: spherical nanoparticles with size distribution
in 3 layers system (experiment at GALAXI).
"""
import os
import bornagain as ba
from bornagain import deg, nm, ba_fitmonitor, R3
from matplotlib import pyplot as plt

radius = 5.75*ba.nm
sigma = 0.4
distance = 53.6*ba.nm
disorder = 10.5*ba.nm
kappa = 17.5
ptfe_thickness = 22.1*ba.nm
hmdso_thickness = 18.5*ba.nm

def get_sample():

    # defining materials
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_Si = ba.RefractiveMaterial("Si", 5.7816e-6, 1.0229e-7)
    material_Ag = ba.RefractiveMaterial("Ag", 2.2475e-5, 1.6152e-6)
    material_PTFE = ba.RefractiveMaterial("PTFE", 5.20509e-6, 1.9694e-8)
    material_HMDSO = ba.RefractiveMaterial("HMDSO", 2.0888e-6, 1.3261e-8)

    # collection of particles with size distribution
    nparticles = 20
    nfwhm = 2.0
    sphere_ff = ba.Sphere(radius)

    sphere = ba.Particle(material_Ag, sphere_ff)
    position = R3(0, 0, -1*hmdso_thickness)
    sphere.translate(position)
#     ln_distr = ba.DistributionLogNormal(radius, sigma)
#     par_distr = ba.ParameterDistribution(
#         "/Particle/Sphere/Radius", ln_distr, nparticles, nfwhm,
#     part_coll = ba.ParticleDistribution(sphere, par_distr)

    # interference function
    interference = ba.InterferenceRadialParacrystal(
        distance, 1e6*ba.nm)
    interference.setKappa(kappa)
    interference.setDomainSize(2e4*nm)
    pdf = ba.Profile1DGauss(disorder)
    interference.setProbabilityDistribution(pdf)

    # assembling particle layout
    layout = ba.ParticleLayout()
    layout.addParticle(sphere, 1)
    layout.setInterference(interference)
    layout.setTotalParticleSurfaceDensity(1)

    # roughness
    r_ptfe = ba.Roughness(2.3*ba.nm, 0.3, 5*ba.nm)
    r_hmdso = ba.Roughness(1.1*ba.nm, 0.3, 5*ba.nm)

    # layers
    vacuum_layer = ba.Layer(vacuum)
    hmdso_layer = ba.Layer(material_HMDSO, hmdso_thickness, r_hmdso)
    hmdso_layer.addLayout(layout)
    ptfe_layer = ba.Layer(material_PTFE, ptfe_thickness, r_ptfe)
    substrate_layer = ba.Layer(material_Si)

    # assembling sample
    sample = ba.Sample()
    sample.addLayer(vacuum_layer)
    sample.addLayer(hmdso_layer)
    sample.addLayer(ptfe_layer)
    sample.addLayer(substrate_layer)

    return sample
