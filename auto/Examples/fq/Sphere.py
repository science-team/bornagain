#!/usr/bin/env python3
"""
Plot |F(q)| vs q for sphere.

"""
import bornagain as ba
import numpy as np
from bornagain import ba_plot as bp, deg, nm, C3

if __name__ == '__main__':
    ff = ba.Sphere(1, True)
    qmax = 32

    v = ff.volume()
    n = 1000
    x = [qmax*i/(n - 1) for i in range(n)]
    y = [ff.formfactor(C3(q, 0, 0)).real/v for q in x]
    ym = [-f for f in y]

    bp.plt.semilogy(x, y)
    bp.plt.semilogy(x, ym)

    bp.plt.xlim([0, qmax])
    bp.plt.ylim([5e-5, 2])

    label_fontsize = 18
    bp.plt.xlabel("$qR$", fontsize=label_fontsize)
    bp.plt.ylabel("$|F(q)|$", fontsize=label_fontsize)

    bp.plt.tight_layout()
    bp.plt.show()
