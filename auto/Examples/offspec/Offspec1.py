#!/usr/bin/env python3
"""
Basic offspec example.
Sample is a grating on a substrate, modelled by a 1d lattice of long boxes
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Particles
    ff = ba.Box(1000*nm, 20*nm, 10*nm)
    particle = ba.Particle(material_particle, ff)
    particle_rotation = ba.RotationZ(90*deg)
    particle.rotate(particle_rotation)

    # Interference functions
    iff = ba.Interference1DLattice(100*nm, 0)
    iff_pdf = ba.Profile1DCauchy(1e6*nm)
    iff.setDecayFunction(iff_pdf)

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(particle)
    layout.setInterference(iff)
    layout.setTotalParticleSurfaceDensity(0.01)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    nscan = 200
    ny = 50
    scan = ba.AlphaScan(nscan, 0.1*deg, 10*deg)
    scan.setIntensity(1e9)
    scan.setWavelength(0.1*nm)
    detector = ba.OffspecDetector(ny, -1*deg, +1*deg, nscan, 0.1*deg, 10*deg)
    return ba.OffspecSimulation(scan, sample, detector)


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    bp.plot_datafield(result, intensity_min=1)
    bp.plt.show()
