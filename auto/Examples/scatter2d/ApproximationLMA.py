#!/usr/bin/env python3
"""
Cylinders of two different sizes in Local Monodisperse Approximation
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    """
    A sample with cylinders of two different sizes on a substrate.
    The cylinder positions are modelled in Local Monodisperse Approximation.
    """

    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Form factors
    ff_1 = ba.Cylinder(5*nm, 5*nm)
    ff_2 = ba.Cylinder(8*nm, 8*nm)

    # Particles
    particle_1 = ba.Particle(material_particle, ff_1)
    particle_2 = ba.Particle(material_particle, ff_2)

    # Interference functions
    iff_1 = ba.InterferenceRadialParacrystal(16.8*nm, 1000*nm)
    iff_1_pdf = ba.Profile1DGauss(3*nm)
    iff_1.setProbabilityDistribution(iff_1_pdf)
    iff_2 = ba.InterferenceRadialParacrystal(22.8*nm, 1000*nm)
    iff_2_pdf = ba.Profile1DGauss(3*nm)
    iff_2.setProbabilityDistribution(iff_2_pdf)

    # Particle layouts
    layout_1 = ba.ParticleLayout()
    layout_1.addParticle(particle_1, 0.8)
    layout_1.setInterference(iff_1)
    layout_1.setTotalParticleSurfaceDensity(0.01)
    layout_2 = ba.ParticleLayout()
    layout_2.addParticle(particle_2, 0.2)
    layout_2.setInterference(iff_2)
    layout_2.setTotalParticleSurfaceDensity(0.01)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout_1)
    layer_1.addLayout(layout_2)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 200
    detector = ba.SphericalDetector(n, 0., 2*deg, n, 0., 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    bp.plot_datafield(result)
    bp.plt.show()
