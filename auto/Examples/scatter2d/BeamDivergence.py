#!/usr/bin/env python3
"""
Cylinder form factor in DWBA with beam divergence
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    from bornagain import std_samples
    return std_samples.cylinders()


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 100
    detector = ba.SphericalDetector(n, 0., 2*deg, n, 0., 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    distr_1 = ba.DistributionLogNormal(0.1*nm, 0.1, 5, 2)
    simulation.addParameterDistribution(
        ba.ParameterDistribution.BeamWavelength, distr_1)
    distr_2 = ba.DistributionGaussian(0.2*deg, 0.1*deg, 5, 2)
    simulation.addParameterDistribution(
        ba.ParameterDistribution.BeamGrazingAngle, distr_2)
    distr_3 = ba.DistributionGaussian(0, 0.1*deg, 5, 2)
    simulation.addParameterDistribution(
        ba.ParameterDistribution.BeamAzimuthalAngle, distr_3)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    bp.plot_datafield(result)
    bp.plt.show()
