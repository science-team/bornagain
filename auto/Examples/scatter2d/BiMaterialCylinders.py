#!/usr/bin/env python3
"""
Cylindrical particle made from two materials.
Particle crosses air/substrate interface.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, R3


def get_sample():
    """
    A multilayer with substrate/air layers.
    Vacuum layer contains cylindrical particles made of two materials.
    Particle shifted down to cross interface.
    """

    # Materials
    material_Ag = ba.RefractiveMaterial("Ag", 1.245e-05, 5.419e-07)
    material_substrate = ba.RefractiveMaterial("Substrate", 3.212e-06,
                                               3.244e-08)
    material_Teflon = ba.RefractiveMaterial("Teflon", 2.9e-06, 6.019e-09)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Form factors
    ff_1 = ba.Cylinder(10*nm, 4*nm)
    ff_2 = ba.Cylinder(10*nm, 10*nm)

    # Particles
    subparticle_1 = ba.Particle(material_Ag, ff_1)
    subparticle_1.translate(R3(0, 0, 10*nm))
    subparticle_2 = ba.Particle(material_Teflon, ff_2)

    # Composition of particles at specific positions
    particle = ba.Compound()
    particle.addComponent(subparticle_1)
    particle.addComponent(subparticle_2)
    particle.translate(R3(0, 0, -10*nm))

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(particle)
    layout.setTotalParticleSurfaceDensity(1)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 100
    detector = ba.SphericalDetector(n, -1*deg, 1*deg, n, 0., 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    bp.plot_datafield(result)
    bp.plt.show()
