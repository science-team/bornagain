#!/usr/bin/env python3
"""
Cylinder form factor in DWBA with constant background
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    from bornagain import std_samples
    return std_samples.cylinders()


def get_simulation(sample):
    beam = ba.Beam(1e6, 0.1*nm, 0.2*deg)
    n = 100
    detector = ba.SphericalDetector(n, 0., 2*deg, n, 0., 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    background = ba.ConstantBackground(1e3)
    simulation.setBackground(background)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    bp.plot_datafield(result)
    bp.plt.show()
