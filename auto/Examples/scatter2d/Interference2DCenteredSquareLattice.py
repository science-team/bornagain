#!/usr/bin/env python3
"""
2D lattice with disorder, centered square lattice
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, R3


r = 3*nm  # particle radius
a = 25*nm  # lattice constant


def get_sample():
    """
    A sample with cylinders on a substrate,
    forming a 2D centered square lattice
    """

    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Particles
    ff = ba.Cylinder(r, r)
    particle_1 = ba.Particle(material_particle, ff)
    particle_2 = ba.Particle(material_particle, ff)
    particle_2.translate(R3(a/2, a/2, 0))

    # Composition of particles at specific positions
    basis = ba.Compound()
    basis.addComponent(particle_1)
    basis.addComponent(particle_2)

    # 2D lattices
    lattice = ba.SquareLattice2D(a, 0*deg)

    # Interference functions
    iff = ba.Interference2DLattice(lattice)
    iff_pdf = ba.Profile2DCauchy(48*nm, 16*nm, 0)
    iff.setDecayFunction(iff_pdf)

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(basis)
    layout.setInterference(iff)
    layout.setTotalParticleSurfaceDensity(0.0016)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 200
    detector = ba.SphericalDetector(n, -2*deg, 2*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    bp.plot_datafield(result)
    bp.plt.show()
