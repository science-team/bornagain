#!/usr/bin/env python3
"""
GISAS by a a distribution of differently oriented
square lattices of cylinders on a substrate.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, R3


def get_sample():
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-6, 2e-8)
    material_particle = ba.RefractiveMaterial("Particle", 6e-4, 2e-8)

    toplayer = ba.Layer(vacuum)
    substrate = ba.Layer(material_substrate)

    ff = ba.Cylinder(3*nm, 4*nm)
    particle = ba.Particle(material_particle, ff)

    distr = ba.DistributionGate(0*deg, 90*deg)
    distr.setNSamples(21)
    for parsample in distr.distributionSamples():
        layout = ba.ParticleLayout(particle)
        iff = ba.Interference2DLattice(ba.SquareLattice2D(25*nm, parsample.value))
        iff.setDecayFunction(ba.Profile2DCauchy(100*nm, 100*nm, 0))
        layout.setInterference(iff)
        layout.setTotalParticleSurfaceDensity(0.1*parsample.weight)
        toplayer.addLayout(layout)

    sample = ba.Sample()
    sample.addLayer(toplayer)
    sample.addLayer(substrate)
    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 100
    detector = ba.SphericalDetector(n, -1*deg, 1*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    bp.plot_datafield(result)
    bp.plt.show()
