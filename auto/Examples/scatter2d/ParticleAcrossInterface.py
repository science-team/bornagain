#!/usr/bin/env python3
"""
Scattering from particles that cross the interface between vacuum and substrate.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, R3


def get_sample():
    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Particles
    ff = ba.Cylinder(5*nm, 5*nm)
    particle = ba.Particle(material_particle, ff)
    particle.translate(0, 0, -3*nm)

    # Particle layouts
    layout = ba.ParticleLayout(particle)
    layout.setTotalParticleSurfaceDensity(0.01)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 100
    detector = ba.SphericalDetector(n, -1*deg, 1*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    bp.plot_datafield(result)
    bp.plt.show()
