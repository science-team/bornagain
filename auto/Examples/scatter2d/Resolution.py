#!/usr/bin/env python3
"""
Basic GISAS from oriented boxes, with different detector resolutions.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    """
    Dilute random assembly of oriented boxes on a substrate.
    """
    from bornagain import std_samples

    mat = ba.RefractiveMaterial("Particle", 6e-4, 2e-08)
    ff = ba.Box(30*nm, 30*nm, 30*nm)
    particle = ba.Particle(mat, ff)

    return std_samples.substrate_plus_particle(particle)


if __name__ == '__main__':
    sample = get_sample()

    # Beam
    wavelength = 0.1*nm
    alpha_i = 0.2*deg
    beam = ba.Beam(1, wavelength, alpha_i)

    # Detector
    nx = 142
    ny = 200
    detector = ba.SphericalDetector(nx, -1.5*deg, 1.5*deg, ny, 0, 3*deg)

    results = []

    simulation = ba.ScatteringSimulation(beam, sample, detector)
    result = simulation.simulate()
    result.setTitle("no resolution")
    results.append(result)

    detector.setResolutionFunction(
        ba.ResolutionFunction2DGaussian(0.2*deg, 0.01*deg))
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    result = simulation.simulate()
    result.setTitle("resolution 0.2deg, 0.01deg")
    results.append(result)

    detector.setResolutionFunction(
        ba.ResolutionFunction2DGaussian(0.2*deg, 0.2*deg))
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    result = simulation.simulate()
    result.setTitle("resolution 0.2deg, 0.2deg")
    results.append(result)
    bp.plot_to_row(results)
    bp.plt.show()
