#!/usr/bin/env python3
"""
Spheres in a finite layer. For testing the slicing machinery.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    # defining materials
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_layer1 = ba.RefractiveMaterial("Layer1", 4e-6, 2e-8)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-6, 2e-8)
    material_particle = ba.RefractiveMaterial("Particle", 3e-5, 2e-8)

    # particle
    ff = ba.Sphere(5*nm)
    particle = ba.Particle(material_particle, ff)
    layout = ba.ParticleLayout()
    layout.addParticle(particle)

    # interference function
    interference = ba.InterferenceHardDisk(10*nm, .002)
    layout.setInterference(interference)

    # layers
    layer1 = ba.Layer(material_layer1, 12*nm)
    layer1.setNumberOfSlices(10)

    substrate = ba.Layer(material_substrate)
    substrate.addLayout(layout)

    sample = ba.Sample()
    sample.addLayer(ba.Layer(vacuum))
    sample.addLayer(layer1)
    sample.addLayer(substrate)
    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 100
    detector = ba.SphericalDetector(n, -2*deg, 2*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    simulation.options().setUseAvgMaterials(True)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    bp.plot_datafield(result)
    bp.plt.show()
