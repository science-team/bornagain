#!/usr/bin/env python3
"""
Specular simulation with footprint correction for a square beam

"""
import bornagain as ba
from bornagain import angstrom, ba_plot as bp, deg, std_samples

sample = std_samples.alternating_layers()


def simulate(footprint, title):
    n = 500
    scan = ba.AlphaScan(n, 0.6*deg/n, 0.6*deg)
    scan.setWavelength(1.54*angstrom)
    scan.setFootprint(footprint)
    simulation = ba.SpecularSimulation(scan, sample)

    result = simulation.simulate()
    result.setTitle(title)
    return result


if __name__ == '__main__':
    beam_sample_ratio = 0.01  # beam-to-sample size ratio

    results = [
        simulate(ba.FootprintSquare(beam_sample_ratio), "With footprint"),
        simulate(None, "Without footprint"),
    ]

    bp.plot_multicurve(results, legendloc='lower left')
    bp.plt.show()
