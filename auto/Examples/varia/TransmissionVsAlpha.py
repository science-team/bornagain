#!/usr/bin/env python3
"""
Flattened depth-probe simulation:
Intensity as function of incident angle alpha_i for a few depths z.
"""

import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample(depth):
    material_vac = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_A = ba.RefractiveMaterial("A", 5e-5, 0)
    material_sub = ba.RefractiveMaterial("Substrate", 3e-05, 0)

    sample = ba.Sample()
    sample.addLayer(ba.Layer(material_vac))
    sample.addLayer(ba.Layer(material_A, depth))
    sample.addLayer(ba.Layer(material_sub))

    return sample

def simulate(depth):
    sample = get_sample(depth)
    n = 500
    alpha_max = 0.8 * deg
    scan = ba.AlphaScan(n, alpha_max / n, alpha_max)
    scan.setWavelength(0.3*nm)

    z_axis = ba.ListScan("z (nm)", [-depth])
    simulation = ba.DepthprobeSimulation(scan, sample, z_axis, 0)

    result = simulation.simulate().flat()
    result.setTitle(f'{depth} nm')
    return result

if __name__ == '__main__':
    depths = [20, 40, 60]
    results = [simulate(d) for d in depths]
    bp.plot_multicurve(results, legendloc='lower right')
    bp.plt.show()
