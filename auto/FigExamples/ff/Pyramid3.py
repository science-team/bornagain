#!/usr/bin/env python3
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, std_samples, std_simulations

material_particle = ba.RefractiveMaterial("Particle", 1e-6, 0)
ff = ba.Pyramid3(14*nm, 6.8*nm, 70*deg)


def get_sample(omega):
    particle = ba.Particle(material_particle, ff)
    particle.rotate(ba.RotationZ(omega*deg))
    return std_samples.sas_sample_with_particle(particle)


def get_simulation(sample):
    n = 201
    return std_simulations.sas(sample, n)


if __name__ == '__main__':
    results = []
    for omega in [0, 15, 30]:
        title = r'$\omega=%d^\circ$' % omega
        sample = get_sample(omega)
        simulation = get_simulation(sample)
        result = simulation.simulate()
        result.setTitle(title)
        results.append(result)

    plotargs = bp.parse_commandline()
    bp.plot_to_row(results, **plotargs)
    bp.export(**plotargs)
