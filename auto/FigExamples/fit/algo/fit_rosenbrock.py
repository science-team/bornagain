#!/usr/bin/env python3

import bornagain as ba

def rosenbrock(P):
    x = P["x"].value
    y = P["y"].value
    tmp1 = y - x*x
    tmp2 = 1 - x
    return 100*tmp1*tmp1 + tmp2*tmp2

P = ba.Parameters()
P.add("x", value=-1.2, min=-5.0, max=5.0, step=0.01)
P.add("y", value=1.0, min=-5.0, max=5.0, step=0.01)

minimizer = ba.Minimizer()
result = minimizer.minimize(rosenbrock, P)
print(result.toString())
