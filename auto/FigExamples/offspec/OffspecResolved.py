#!/usr/bin/env python3
"""
Same sample as in Offspec1, but simulated as GISAS, i.e. without integration in phi
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm
from bornagain.numpyutil import Arrayf64Converter as dac


def get_sample():
    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Particles
    ff = ba.Box(1000*nm, 20*nm, 10*nm)
    particle = ba.Particle(material_particle, ff)
    particle_rotation = ba.RotationZ(90*deg)
    particle.rotate(particle_rotation)

    # Interference functions
    iff = ba.Interference1DLattice(100*nm, 0)
    iff_pdf = ba.Profile1DCauchy(1e6*nm)
    iff.setDecayFunction(iff_pdf)

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(particle)
    # layout.setInterference(iff)
    layout.setTotalParticleSurfaceDensity(0.01)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def simulate(sample):
    beam = ba.Beam(1e9, 0.1 * nm, 1.0 * deg)
    detector = ba.SphericalDetector(501, -2*deg, 2*deg, 1, 0.94*deg, 1.06*deg)

    sim = ba.ScatteringSimulation(beam, sample, detector)
    return sim.simulate().flat()

if __name__ == '__main__':
    sample = get_sample()
    result = simulate(sample)
    sum = dac.asNpArray(result.dataArray()).sum()
    print(f'sum = {sum/1e9}')
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
