#!/usr/bin/env python3
"""
GISAS simulation that includes the specular peak
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    """
    A lattice of box-shaped particles on a substrate.
    """

    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 3e-05, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Particles
    ff = ba.Box(5*nm, 5*nm, 10*nm)
    particle = ba.Particle(material_particle, ff)

    # Lattice and interference function
    lattice = ba.BasicLattice2D(8*nm, 8*nm, 90*deg, 0)
    iff = ba.Interference2DLattice(lattice)
    iff_pdf = ba.Profile2DCauchy(100*nm, 100*nm, 0)
    iff.setDecayFunction(iff_pdf)

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(particle)
    layout.setInterference(iff)
    layout.setTotalParticleSurfaceDensity(0.015625)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 101
    detector = ba.SphericalDetector(n, -2*deg, 2*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    simulation.options().setUseAvgMaterials(True)
    simulation.options().setIncludeSpecular(True)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
