#!/usr/bin/env python3
"""
Mixture of cylinders and prisms without interference
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    """
    A sample with uncorrelated cylinders and prisms on a substrate.
    """

    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Form factors
    ff_1 = ba.Cylinder(5*nm, 5*nm)
    ff_2 = ba.Prism3(10*nm, 5*nm)

    # Particles
    particle_1 = ba.Particle(material_particle, ff_1)
    particle_2 = ba.Particle(material_particle, ff_2)

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(particle_1, 0.5)
    layout.addParticle(particle_2, 0.5)
    layout.setTotalParticleSurfaceDensity(0.01)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 100
    detector = ba.SphericalDetector(n, -1*deg, 1*deg, n, 0., 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
