#!/usr/bin/env python3
"""
Spheres on two hexagonal close packed layers
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, R3


def get_sample():
    """
    A sample with spheres on a substrate,
    forming two hexagonal close packed layers.
    """

    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Form factors
    ff_1 = ba.Sphere(10*nm)
    ff_2 = ba.Sphere(10*nm)

    # Particles
    particle_1 = ba.Particle(material_particle, ff_1)
    particle_2 = ba.Particle(material_particle, ff_2)
    particle_2_position = R3(10*nm, 10*nm, 17.3205080757*nm)
    particle_2.translate(particle_2_position)

    # Composition of particles at specific positions
    basis = ba.Compound()
    basis.addComponent(particle_1)
    basis.addComponent(particle_2)

    # 2D lattices
    lattice = ba.HexagonalLattice2D(20*nm, 0)

    # Interference functions
    iff = ba.Interference2DLattice(lattice)
    iff_pdf = ba.Profile2DCauchy(10*nm, 10*nm, 0)
    iff.setDecayFunction(iff_pdf)

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(basis)
    layout.setInterference(iff)
    layout.setTotalParticleSurfaceDensity(0.00288675134595)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 200
    detector = ba.SphericalDetector(n, -1*deg, 1*deg, n, 0, 1*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
