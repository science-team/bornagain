#!/usr/bin/env python3
"""
Functional test: Magnetic cylinders in DWBA with zero magnetic field
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, R3


def get_sample():
    # Materials
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-6, 2e-8)
    B = R3(0, 0, 0)
    material_particle = ba.RefractiveMaterial("magParticle", 6e-4, 2e-8, B)

    # Particles
    cylinder_ff = ba.Cylinder(5*nm, 5*nm)
    cylinder = ba.Particle(material_particle, cylinder_ff)

    particle_layout = ba.ParticleLayout()
    particle_layout.addParticle(cylinder, 1)

    # Multilayer
    vacuum_layer = ba.Layer(vacuum)
    vacuum_layer.addLayout(particle_layout)
    substrate_layer = ba.Layer(material_substrate, 0)
    sample = ba.Sample()
    sample.addLayer(vacuum_layer)
    sample.addLayer(substrate_layer)

    return sample


def get_simulation(sample):
    n = 100
    beam = ba.Beam(1e2, 0.1*nm, 0.2*deg)
    detector = ba.SphericalDetector(n, 0, 2*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


def simulate():
    sample = get_sample()
    simulation = get_simulation(sample)
    return simulation.simulate()


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
