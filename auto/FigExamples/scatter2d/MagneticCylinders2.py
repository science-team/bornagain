#!/usr/bin/env python3
"""
Functional test: Magnetic cylinders in DWBA with zero magnetic field
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, R3, nm


def get_sample():
    # Materials
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_substrate = ba.RefractiveMaterial("Substrate", 15e-6, 0)
    B = R3(0, 1e6, 0)
    material_particle = ba.RefractiveMaterial("magParticle", 5e-6, 0, B)

    # Particles
    cylinder_ff = ba.Cylinder(5*nm, 5*nm)
    cylinder = ba.Particle(material_particle, cylinder_ff)

    particle_layout = ba.ParticleLayout()
    particle_layout.addParticle(cylinder, 1)

    # Multilayer
    vacuum_layer = ba.Layer(vacuum)
    vacuum_layer.addLayout(particle_layout)
    substrate_layer = ba.Layer(material_substrate, 0)
    sample = ba.Sample()
    sample.addLayer(vacuum_layer)
    sample.addLayer(substrate_layer)

    return sample


def get_simulation(sample, pol_dir):
    z_up = R3(0, 0, 1)
    n = 100
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    detector = ba.SphericalDetector(n, -1*deg, 1*deg, n, 0, 2*deg)

    beam.setPolarization(pol_dir)
    detector.setAnalyzer(z_up)

    return ba.ScatteringSimulation(beam, sample, detector)


def simulate(title, pol_dir):
    sample = get_sample()
    simulation = get_simulation(sample, pol_dir)
    result = simulation.simulate()
    result.setTitle(title)
    return result


if __name__ == '__main__':
    z_up = R3(0, 0, 1)
    z_dn = R3(0, 0, -1)
    results = []
    results.append(simulate('++', z_up))
    results.append(simulate('+-', z_dn))
    results.append(simulate('-+', z_up))
    results.append(simulate('--', z_dn))
    plotargs = bp.parse_commandline()
    plotargs['fontsize'] = 12
    bp.plot_to_grid(results, 2, **plotargs)
    bp.export(**plotargs)
