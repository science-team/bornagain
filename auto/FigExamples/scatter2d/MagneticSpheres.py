#!/usr/bin/env python3
"""
Spin-flip scattering by magnetic spheres
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, R3


def get_sample():
    # Materials
    B = R3(0, 0, 1e7)
    material_particle = ba.RefractiveMaterial("Particle", 2e-05, 4e-07, B)
    material_substrate = ba.RefractiveMaterial("Substrate", 7e-06, 1.8e-07)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Particles
    ff = ba.Sphere(5*nm)

    particle = ba.Particle(material_particle, ff)
    particle_position = R3(0, 0, -10*nm)
    particle.translate(particle_position)

    layout = ba.ParticleLayout()
    layout.addParticle(particle, 1)
    layout.setTotalParticleSurfaceDensity(0.01)

    # Multilayer
    layer_1 = ba.Layer(vacuum)
    layer_2 = ba.Layer(material_substrate)
    layer_2.addLayout(layout)

    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e12, 0.1*nm, 0.5*deg)
    n = 200
    detector = ba.SphericalDetector(n, -3*deg, 3*deg, n, 0, 4*deg)

    polarizer_vec = R3(0, 0, 1)
    analyzer_vec = R3(0, 0, -1)
    beam.setPolarization(polarizer_vec)
    detector.setAnalyzer(analyzer_vec)

    return ba.ScatteringSimulation(beam, sample, detector)


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
