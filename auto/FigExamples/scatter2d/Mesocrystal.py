#!/usr/bin/env python3
"""
Cylindrical mesocrystal on a substrate
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, R3


def get_sample():
    """
    A sample with a cylindrically shaped mesocrystal on a substrate.
    """

    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Particles
    inner_ff = ba.Sphere(2*nm)
    inner_particle = ba.Particle(material_particle, inner_ff)

    # 3D lattices
    lattice = ba.Lattice3D(R3(5*nm, 0, 0), R3(0, 5*nm, 0),
                           R3(0, 0, 5*nm))

    # Crystals
    crystal = ba.Crystal(inner_particle, lattice)

    # Mesocrystals
    outer_ff = ba.Cylinder(20*nm, 50*nm)
    outer_particle = ba.Mesocrystal(crystal, outer_ff)

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(outer_particle, 1)
    layout.setTotalParticleSurfaceDensity(0.001)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 200
    detector = ba.SphericalDetector(n, -2*deg, 2*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
