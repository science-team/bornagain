#!/usr/bin/env python3
"""
GISAS by cylinders with Gaussian radius distribution on a substrate.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    layout = ba.ParticleLayout()
    layout.setTotalParticleSurfaceDensity(0.01)

    distr = ba.DistributionGaussian(10*nm, 1*nm)
    for parsample in distr.distributionSamples():
        ff = ba.Cylinder(parsample.value, 5*nm)
        particle = ba.Particle(material_particle, ff)
        layout.addParticle(particle, parsample.weight)

    layer = ba.Layer(vacuum)
    layer.addLayout(layout)

    sample = ba.Sample()
    sample.addLayer(layer)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 200
    detector = ba.SphericalDetector(n, -1*deg, 1*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
