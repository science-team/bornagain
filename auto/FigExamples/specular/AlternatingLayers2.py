#!/usr/bin/env python3
"""
Basic example of specular reflectometry simulation,
using a standard sample from Wrap/Python/std_samples.py.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, angstrom, std_samples


def get_sample():
    return std_samples.alternating_layers()


def get_simulation(sample):
    n = 500
    scan = ba.AlphaScan(n, 2*deg/n, 2*deg)
    scan.setWavelength(1.54*angstrom)
    return ba.SpecularSimulation(scan, sample)


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
