#!/usr/bin/env python3
"""
Example of simulating a reflectometry experiment
with a rough sample using BornAgain.

"""
import bornagain as ba
from bornagain import nm, ba_plot as bp, deg, nm


def get_sample():
    # Materials
    vacuum = ba.MaterialBySLD("Vacuum", 0, 0)
    material_ti = ba.MaterialBySLD("Ti", -1.9493e-06, 0)
    material_ni = ba.MaterialBySLD("Ni", 9.4245e-06, 0)
    material_substrate = ba.MaterialBySLD("SiSubstrate", 2.0704e-06, 0)

    # roughness
    autocorr = ba.SelfAffineFractalModel(1*nm, 0.7, 25*nm)
    transient = ba.TanhTransient()
    roughness = ba.Roughness(autocorr, transient)

    # Layers
    ambient_layer = ba.Layer(vacuum)
    ti_layer = ba.Layer(material_ti, 3*nm, roughness)
    ni_layer = ba.Layer(material_ni, 7*nm, roughness)
    substrate_layer = ba.Layer(material_substrate, roughness)

    # Periodic stack
    n_repetitions = 10
    stack = ba.LayerStack(n_repetitions)
    stack.addLayer(ti_layer)
    stack.addLayer(ni_layer)
    
    # Sample
    sample = ba.Sample()
    sample.addLayer(ambient_layer)
    sample.addStack(stack)
    sample.addLayer(substrate_layer)

    return sample


def get_simulation(sample):
    n = 500
    scan = ba.AlphaScan(n, 2*deg/n, 2*deg)
    scan.setWavelength(0.154*nm)
    return ba.SpecularSimulation(scan, sample)


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
