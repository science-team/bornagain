#!/usr/bin/env python3
"""
Example of simulating a reflectometry experiment
with a rough sample using BornAgain.

"""
import bornagain as ba
from bornagain import angstrom, ba_plot as bp, deg, nm


def get_sample():
    # Materials
    vacuum = ba.MaterialBySLD("Vacuum", 0, 0)
    material_ti = ba.MaterialBySLD("Ti", -1.9493e-06, 0)
    material_ni = ba.MaterialBySLD("Ni", 9.4245e-06, 0)
    material_substrate = ba.MaterialBySLD("SiSubstrate", 2.0704e-06, 0)

    # roughness
    autocorr = ba.K_CorrelationModel(1*nm)
    interlayer = ba.TanhInterlayer()
    roughness = ba.LayerRoughness(autocorr, interlayer)

    # Layers
    ambient_layer = ba.Layer(vacuum)
    ti_layer = ba.Layer(material_ti, 30*angstrom, roughness)
    ni_layer = ba.Layer(material_ni, 70*angstrom, roughness)
    substrate_layer = ba.Layer(material_substrate, roughness)

    # Periodic stack
    n_repetitions = 10
    stack = ba.LayerStack(n_repetitions)
    stack.addLayer(ti_layer)
    stack.addLayer(ni_layer)
    
    # Sample
    sample = ba.Sample()
    sample.addLayer(ambient_layer)
    sample.addStack(stack)
    sample.addLayer(substrate_layer)

    return sample


def get_simulation(sample):
    n = 500
    scan = ba.AlphaScan(n, 2*deg/n, 2*deg)
    scan.setWavelength(1.54*angstrom)
    return ba.SpecularSimulation(scan, sample)


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_simulation_result(result, **plotargs)
    bp.export(**plotargs)
