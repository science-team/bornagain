#!/usr/bin/env python3
"""
An example of defining reflectometry instrument
for time of flight experiment. In this example
we will use purely qz-defined beam,
without explicitly specifying
incident angle or a wavelength.
Note that these approaches work with SLD-based
materials only.
"""
import numpy as np
import bornagain as ba
from bornagain import ba_plot as bp, std_samples


def get_sample():
    return std_samples.alternating_layers()


def get_simulation(sample):
    "Specular simulation with a qz-defined beam"
    n = 500
    qzs = np.linspace(0.01, 1, n)  # qz-values
    scan = ba.QzScan(qzs)
    return ba.SpecularSimulation(scan, sample)


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
