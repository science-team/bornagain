#!/usr/bin/env python3
"""
Thin-film sample as in basic depth-probe example.
Computes modulus of amplitude of transmitted field
as function of of incident angle alpha and depth z.
"""

import bornagain as ba
import Depthprobe1 as dp1

if __name__ == '__main__':
    flags = ba.ZDirection_Transmitted | ba.WaveProperty_Modulus
    dp1.run_example(flags)
