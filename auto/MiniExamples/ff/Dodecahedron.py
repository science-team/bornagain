#!/usr/bin/env python3
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, std_samples, std_simulations

material_particle = ba.RefractiveMaterial("Particle", 1e-6, 0)
ff = ba.Dodecahedron(3.38*nm)


def get_sample(omega):
    particle = ba.Particle(material_particle, ff)
    particle.rotate(ba.RotationY(omega*deg))
    return std_samples.sas_sample_with_particle(particle)


def get_simulation(sample):
    n = 11
    return std_simulations.sas(sample, n)


if __name__ == '__main__':
    titles = ['face normal', 'vertex normal', 'edge normal']
    angles = [26.5651, -52.6226, 58.2825]
    results = []
    for i in range(len(angles)):
        sample = get_sample(angles[i])
        simulation = get_simulation(sample)
        result = simulation.simulate()
        result.setTitle(titles[i])
        results.append(result)

    plotargs = bp.parse_commandline()
    bp.plot_to_row(results, **plotargs)
    bp.export(**plotargs)
