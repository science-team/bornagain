#!/usr/bin/env python3
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, std_samples, std_simulations

material_particle = ba.RefractiveMaterial("Particle", 1e-6, 0)
ff = ba.Sphere(4.13*nm)


def get_sample():
    particle = ba.Particle(material_particle, ff)
    return std_samples.sas_sample_with_particle(particle)


def get_simulation(sample):
    n = 11
    return std_simulations.sas(sample, n)


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
