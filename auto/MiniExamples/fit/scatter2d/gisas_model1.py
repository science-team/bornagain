"""
Parametric model of a GISAS simulation.
The idealized sample model consists of dilute cylinders on a substrate.
"""

import bornagain as ba
from bornagain import deg, nm

vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
material_substrate = ba.RefractiveMaterial("Substrate", 6e-6, 2e-8)
material_particle = ba.RefractiveMaterial("Particle", 6e-4, 2e-8)


def get_sample(P):
    cylinder_height = P["cylinder_height"]
    cylinder_radius = P["cylinder_radius"]

    ff = ba.Cylinder(cylinder_radius, cylinder_height)
    cylinder = ba.Particle(material_particle, ff)
    layout = ba.ParticleLayout()
    layout.addParticle(cylinder)

    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)
    return sample


def get_simulation(P):
    beam = ba.Beam(10**P['lg(intensity)'], 0.1*nm, 0.2*deg)
    n = 100
    det = ba.SphericalDetector(n, -1.5*deg, 1.5*deg, n, 0, 3*deg)
    sample = get_sample(P)

    simulation = ba.ScatteringSimulation(beam, sample, det)
    simulation.setBackground(
        ba.ConstantBackground(10**P['lg(background)']))

    return simulation


def start_parameters_1():
    P = ba.Parameters()
    P.add("lg(intensity)", 5)
    P.add("lg(background)", 1)
    P.add("cylinder_height", 6.*nm, min=0.01)
    P.add("cylinder_radius", 6.*nm, min=0.01)
    return P

if __name__ == '__main__':
    raise Exception("This source file is a module for use from fitting examples."
                    " It is not meant to be called as a main program.")
