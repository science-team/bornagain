import bornagain as ba
from bornagain import deg, nm
import numpy as np

def get_sample(P):
    """
    Build the sample made of uncorrelated cylinders on top of a substrate
    """
    radius = P["radius"]
    height = P["height"]

    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-6, 2e-8)
    material_particle = ba.RefractiveMaterial("Particle", 6e-4, 2e-8)

    cylinder_ff = ba.Cylinder(radius, height)
    cylinder = ba.Particle(material_particle, cylinder_ff)

    layout = ba.ParticleLayout()
    layout.addParticle(cylinder)

    vacuum_layer = ba.Layer(vacuum)
    vacuum_layer.addLayout(layout)
    substrate_layer = ba.Layer(material_substrate, 0)

    sample = ba.Sample()
    sample.addLayer(vacuum_layer)
    sample.addLayer(substrate_layer)

    return sample


def get_simulation(P):
    """
    Create and return GISAXS simulation with beam and detector defined
    """
    n = 11
    beam = ba.Beam(1e8, 0.1*nm, 0.2*deg)
    sample = get_sample(P)
    detector = ba.SphericalDetector(n, -1*deg, 1*deg, n, 0, 2*deg)

    return ba.ScatteringSimulation(beam, sample, detector)


def fake_data():
    """
    Generating "real" data by adding noise to the simulated data.
    """
    P = {'radius': 5*nm, 'height': 10*nm}

    simulation = get_simulation(P)
    result = simulation.simulate()

    return result.noisy(0.1, 0.1)

if __name__ == '__main__':
    raise Exception("This source file is a module for use from fitting examples."
                    " It is not meant to be called as a main program.")
