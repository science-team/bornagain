#!/usr/bin/env python3
"""
Cylinders of two different sizes in Size-Spacing Coupling Approximation
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm

def get_sample():
    """
    A sample with cylinders of two different sizes on a substrate.
    The cylinder positions are modelled in Size-Spacing Coupling  Approximation.
    """

    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Form factors
    ff_1 = ba.Cylinder(5*nm, 5*nm)
    ff_2 = ba.Cylinder(8*nm, 8*nm)

    # Particles
    particle_1 = ba.Particle(material_particle, ff_1)
    particle_2 = ba.Particle(material_particle, ff_2)

    # Interference functions
    iff = ba.InterferenceRadialParacrystal(18*nm, 1000*nm)
    iff.setKappa(1)
    iff_pdf = ba.Profile1DGauss(3*nm)
    iff.setProbabilityDistribution(iff_pdf)

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(particle_1, 0.8)
    layout.addParticle(particle_2, 0.2)
    layout.setInterference(iff)
    layout.setTotalParticleSurfaceDensity(0.01)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 11
    detector = ba.SphericalDetector(n, 0., 2*deg, n, 0., 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
