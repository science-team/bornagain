#!/usr/bin/env python3
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    # Define materials
    material_Si = ba.RefractiveMaterial("Si", 5.7816e-06, 1.0229e-07)
    material_Vacuum = ba.RefractiveMaterial("Vacuum", 0.0, 0.0)

    # Define layers
    layer_1 = ba.Layer(material_Vacuum)
    layer_4 = ba.Layer(material_Si)

    # Define sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_4)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.5*deg)
    n = 11
    detector = ba.SphericalDetector(n, -0.5*deg, 0.5*deg, n, 0., 1*deg)
    detector.setResolutionFunction(
        ba.ResolutionFunction2DGaussian(0.04*deg, 0.03*deg))
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    simulation.options().setUseAvgMaterials(True)
    simulation.options().setIncludeSpecular(True)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
