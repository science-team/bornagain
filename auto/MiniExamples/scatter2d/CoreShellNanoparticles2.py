#!/usr/bin/env python3
"""
Core shell nanoparticles.
Alternative implementation using particle composition
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, std_samples, R3


def get_sample():
    """
    A sample with box-shaped core-shell particles.
    """

    # Materials
    material_Diff = ba.RefractiveMaterial("Core", -4e-5, 0)
    material_Shell = ba.RefractiveMaterial("Shell", 1e-4, 2e-8)

    # Form factors
    ff_1 = ba.Box(12*nm, 12*nm, 7*nm)
    ff_2 = ba.Box(16*nm, 16*nm, 8*nm)

    # Particles
    core = ba.Particle(material_Diff, ff_1)
    shell = ba.Particle(material_Shell, ff_2)
    particle = ba.Compound()
    particle.addComponent(shell)
    particle.addComponent(core, R3(0, 0, 0.5*nm))

    return std_samples.sas_sample_with_particle(particle)


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = 11
    detector = ba.SphericalDetector(n, 0., 2*deg, n, 0., 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
