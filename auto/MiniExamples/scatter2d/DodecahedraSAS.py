#!/usr/bin/env python3
"""
Basic example for regular small-angle scattering (SAS).
Sample is a dilute assembly of ordered dodecahedra.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_particle = ba.RefractiveMaterial("Particle", 6e-4, 2e-8)

    # Finite sample layer, contains particles:
    ff = ba.Dodecahedron(12*nm)
    particle = ba.Particle(material_particle, ff)
    layout = ba.ParticleLayout()
    layout.addParticle(particle)
    solution_layer = ba.Layer(vacuum, 1000*nm)
    solution_layer.addLayout(layout)

    # Flat sample layer sandwiched between semi-infinite vacuum layers:
    sample = ba.Sample()
    sample.addLayer(ba.Layer(vacuum))
    sample.addLayer(solution_layer)
    sample.addLayer(ba.Layer(vacuum))
    return sample


def get_simulation(sample):
    # Beam from above (perpendicular to sample):
    beam = ba.Beam(1e9, 0.4*nm, 0.001*deg)

    # Detector opposite to source:
    n = 11  # number of pixels per direction
    detector = ba.SphericalDetector(n, -5*deg, 5*deg, n, -5*deg, 5*deg)

    return ba.ScatteringSimulation(beam, sample, detector)


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
