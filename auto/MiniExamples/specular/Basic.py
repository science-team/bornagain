#!/usr/bin/env python3
"""
"""
import numpy as np
import bornagain as ba
from bornagain import ba_plot as bp, nm


def get_sample():
    # Materials
    material_A = ba.MaterialBySLD("A", 5e-06, 0)
    material_substrate = ba.MaterialBySLD("Substrate", 2e-06, 0)

    # Layers
    ambient_layer = ba.Layer(ba.Vacuum())
    film = ba.Layer(material_A, 30*nm)
    substrate_layer = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(ambient_layer)
    sample.addLayer(film)
    sample.addLayer(substrate_layer)

    return sample

def get_simulation(sample):
    "Specular simulation with a qz-defined beam"
    n = 50
    qzs = np.linspace(0.01, 1, n)  # qz-values
    scan = ba.QzScan(qzs)
    return ba.SpecularSimulation(scan, sample)


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
