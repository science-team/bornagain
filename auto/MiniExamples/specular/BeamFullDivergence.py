#!/usr/bin/env python3
"""
An example of taking into account beam angular and wavelength
divergence in reflectometry calculations with BornAgain.
"""
import bornagain as ba
from bornagain import angstrom, ba_plot as bp, deg, std_samples

# input parameters
wavelength = 1.54*angstrom

# convolution parameters
d_wl = 0.01*wavelength  # spread width for wavelength
d_ang = 0.01*deg  # spread width for incident angle


def get_sample():
    return std_samples.alternating_layers()


def get_simulation(sample):
    """
    A specular simulation with beam and detector defined.
    """
    n = 50

    scan = ba.AlphaScan(n, 2*deg/n, 2*deg)
    scan.setWavelength(wavelength)

    alpha_distr = ba.DistributionGaussian(0, d_ang, 25, 3.)
    scan.setGrazingAngleDistribution(alpha_distr)

    wavelength_distr = ba.DistributionGaussian(0, d_wl, 25, 3.)
    scan.setWavelengthDistribution(wavelength_distr)

    return ba.SpecularSimulation(scan, sample)

    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
