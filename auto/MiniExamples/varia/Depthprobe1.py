#!/usr/bin/env python3
"""
Basic example of depth-probe simulation:
Computes intensity as function of incident angle alpha and depth z.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm

def get_sample():
    # Materials
    material_vac = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_A = ba.RefractiveMaterial("A", 6e-5, 0)
    material_sub = ba.RefractiveMaterial("Substrate", 3e-05, 0)

    # Sample
    sample = ba.Sample()
    sample.addLayer(ba.Layer(material_vac))
    sample.addLayer(ba.Layer(material_A, 100*nm))
    sample.addLayer(ba.Layer(material_sub))

    return sample


def get_simulation(sample, flags):
    n = 50

    scan = ba.AlphaScan(n, 0*deg, 1*deg)
    scan.setWavelength(0.3*nm)

    z_axis = ba.EquiDivision("z (nm)", n, -130*nm, 30*nm)
    simulation = ba.DepthprobeSimulation(scan, sample, z_axis, flags)

    return simulation


def run_example(flags=0):
    sample = get_sample()
    simulation = get_simulation(sample, flags)
    result = simulation.simulate()
    plotargs = bp.parse_commandline()
    plotargs['aspect'] = 'auto'
    plotargs['intensity_min'] = 1e-12
    plotargs['intensity_max'] = 1e2
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)


if __name__ == '__main__':
    run_example()
