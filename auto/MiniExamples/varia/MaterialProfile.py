#!/usr/bin/env python3
"""
Plot SLD and magnetization profiles for a
multilayer sample with rough interfaces.
"""

import bornagain as ba
from bornagain import angstrom, ba_plot as bp, R3
import numpy as np


def get_sample():
    # materials
    vacuum = ba.MaterialBySLD("Vacuum", 0, 0)
    material_ti = ba.MaterialBySLD("Ti", -1.9493e-06, 0)
    B_ni = R3(-5e7, 0, 0)
    material_ni = ba.MaterialBySLD("Ni", 9.4245e-06, 0, B_ni)
    B_substrate = R3(1e8, 0, 0)
    material_substrate = ba.MaterialBySLD("SiSubstrate", 2.0704e-06, 0,
                                          B_substrate)
    
    # roughness
    autocorr = ba.SelfAffineFractalModel(5*angstrom, 0.5, 10*angstrom)
    transient = ba.TanhTransient()
    roughness = ba.Roughness(autocorr, transient)

    # layers
    ambient_layer = ba.Layer(vacuum)
    ti_layer = ba.Layer(material_ti, 30*angstrom, roughness)
    ni_layer = ba.Layer(material_ni, 70*angstrom, roughness)
    substrate_layer = ba.Layer(material_substrate)

    # periodic stack
    n_repetitions = 4
    stack = ba.LayerStack(n_repetitions)
    stack.addLayer(ti_layer)
    stack.addLayer(ni_layer)

    # sample
    sample = ba.Sample()
    sample.addLayer(ambient_layer)
    sample.addStack(stack)
    sample.addLayer(substrate_layer)

    return sample


if __name__ == '__main__':
    sample = get_sample()
    n_points = 400
    z_min, z_max = ba.defaultMaterialProfileLimits(sample)
    z_points = ba.generateZValues(n_points, z_min, z_max)

    sld = ba.materialProfileSLD(sample, n_points, z_min, z_max)
    mag_X = ba.magnetizationProfile(sample, "X", n_points, z_min, z_max)

    bp.plt.subplot(2, 1, 1)
    bp.plt.plot(z_points, np.real(sld))
    bp.plt.title("Re(SLD)")

    bp.plt.subplot(2, 1, 2)
    bp.plt.plot(z_points, mag_X)
    bp.plt.title("X magnetization")

    bp.plt.tight_layout()
    plotargs = bp.parse_commandline()
    bp.export(**plotargs)
