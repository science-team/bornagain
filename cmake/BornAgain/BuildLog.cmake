##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/BuildLog.cmake
##! @brief     Print summary of build and install variables.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

set(log "${CMAKE_BINARY_DIR}/bornagain_build.log")
string(TIMESTAMP CURRENT_DATE "%Y-%m-%d %H:%M")

file(WRITE "${log}"
    "=== Build Summary (${CURRENT_DATE}) ===\n\n"
    "[${CMAKE_PROJECT_NAME}]\n"
    "  version (CMAKE_PROJECT_VERSION) = ${CMAKE_PROJECT_VERSION}\n"
    "  homepage (CMAKE_PROJECT_HOMEPAGE_URL) = ${CMAKE_PROJECT_HOMEPAGE_URL}\n"
    "  system (CMAKE_SYSTEM_NAME) = ${CMAKE_SYSTEM_NAME}\n"
    "  architecture (BA_ARCHITECTURE) = ${BA_ARCHITECTURE}\n"
    "  build type (CMAKE_BUILD_TYPE) = ${CMAKE_BUILD_TYPE}\n"
    "  CMake version (CMAKE_VERSION) = ${CMAKE_VERSION}\n"
    "  CMake make program (CMAKE_MAKE_PROGRAM) = ${CMAKE_MAKE_PROGRAM}\n"
    "  CMake generator (CMAKE_GENERATOR) = ${CMAKE_GENERATOR}\n"
    "  components (AllComponents) = ${AllComponents}\n"

    "\n[git]\n"
    "  branch (GIT_BRANCH) = ${GIT_BRANCH}\n"
    "  commit (GIT_COMMIT) = ${GIT_COMMIT}\n"
    "  tag (GIT_TAG) = ${GIT_TAG}\n"

    "\n[options]\n"
    "  graphical user interface (BA_GUI) = ${BA_GUI}\n"
    "  tiff files read/write support (BA_TIFF_SUPPORT) = ${BA_TIFF_SUPPORT}\n"
    "  libraries with Python support (BORNAGAIN_PYTHON) = ${BORNAGAIN_PYTHON}\n"
    "  generate Python bindings during build with SWIG (CONFIGURE_BINDINGS) = ${CONFIGURE_BINDINGS}\n"
    "  configure Doxygen files (CONFIGURE_SRC_DOCS) = ${CONFIGURE_SRC_DOCS}\n"
    "  build with test coverage information (BA_COVERAGE) = ${BA_COVERAGE}\n"
    "  build with debug optimization (BA_DEBUG_OPTIMIZATION) = ${BA_DEBUG_OPTIMIZATION}\n"
    "  allow diagnostic variables for some algorithms (ALGORITHM_DIAGNOSTIC) = ${ALGORITHM_DIAGNOSTIC}\n"
    "  install header files (BA_CPP_API) = ${BA_CPP_API}\n"
)

if(APPLE)
    file(APPEND "${log}"
        "  create a MacOS bundle (BA_APPLE_BUNDLE) = ${BA_APPLE_BUNDLE}\n")
endif()

file(APPEND "${log}"
    "\n[compile]\n"
    "  CXX compiler (CMAKE_CXX_COMPILER) = ${CMAKE_CXX_COMPILER}\n"
    "  Compiler ID (CMAKE_CXX_COMPILER_ID) = ${CMAKE_CXX_COMPILER_ID}\n"
    "  CXX flags (CMAKE_CXX_FLAGS) = ${CMAKE_CXX_FLAGS}\n"

    "\n[link]\n"
    "  Shared linker Flags (CMAKE_SHARED_LINKER_FLAGS) = ${CMAKE_SHARED_LINKER_FLAGS}\n"
    "  Exe linker Flags (CMAKE_EXE_LINKER_FLAGS) = ${CMAKE_EXE_LINKER_FLAGS}\n"

    "\n[dependencies]\n"
    "  [Boost]\n"
    "    version (Boost_VERSION) = ${Boost_VERSION}\n"
    "    include dir (Boost_INCLUDE_DIR) = ${Boost_INCLUDE_DIR}\n"
    "  [FFTW3]\n"
    "    version (FFTW3_VERSION) = ${FFTW3_VERSION}\n"
    "    include dir (FFTW3_INCLUDE_DIR) = ${FFTW3_INCLUDE_DIR}\n"
    "    libraries (FFTW3_LIBRARIES) = ${FFTW3_LIBRARIES}\n"
    "  [GSL]\n"
    "    version (GSL_VERSION) = ${GSL_VERSION}\n"
    "    include dir (GSL_INCLUDE_DIR) = ${GSL_INCLUDE_DIR}\n"
    "    libraries (GSL_LIBRARY, GSL_CBLAS_LIBRARY) = ${GSL_LIBRARY}; ${GSL_CBLAS_LIBRARY}\n"
    "  [Heinz]\n"
    "    version (LibHeinz_VERSION) = ${LibHeinz_VERSION}\n"
    "    include dir (LibHeinz_DIR) = ${LibHeinz_DIR}\n"
    "  [cerf]\n"
    "    version (cerf_VERSION) = ${cerf_VERSION}\n"
    "    include dir (Cerf_INCLUDE_DIR) = ${Cerf_INCLUDE_DIR}\n"
    "    libraries (Cerf_LIBRARIES) = ${Cerf_LIBRARIES}\n"
    "  [FormFactor]\n"
    "    version (formfactor_VERSION) = ${formfactor_VERSION}\n"
    "    include dir (formfactor_INCLUDE_DIR) = ${formfactor_INCLUDE_DIR}\n"
    "    libraries (formfactor_LIBRARIES) = ${formfactor_LIBRARIES}\n"
    "  [Qt]\n"
    "    version (Qt_VERSION) = ${Qt_VERSION}\n"
    "    dir (Qt_DIR) = ${Qt_DIR}\n"
    "    include dir (Qt_INCLUDE_DIR) = ${Qt_INCLUDE_DIR}\n"
    "    libraries (Qt_LIBRARIES) = ${Qt_LIBRARIES}\n"
    "  [OpenGL]\n"
    "    version (OPENGL_VERSION) = ${OPENGL_VERSION}\n"
    "    include dir (OPENGL_INCLUDE_DIR) = ${OPENGL_INCLUDE_DIR}\n"
    "    libraries (OPENGL_LIBRARIES) = ${OPENGL_LIBRARIES}\n"
    "  [ZLib]\n"
    "    version (ZLIB_VERSION) = ${ZLIB_VERSION}\n"
    "    include dir (ZLIB_INCLUDE_DIR) = ${ZLIB_INCLUDE_DIR}\n"
    "    libraries (ZLIB_LIBRARIES) = ${ZLIB_LIBRARIES}\n"
    "  [BZip2]\n"
    "    version (BZIP2_VERSION) = ${BZIP2_VERSION}\n"
    "    include dir (BZIP2_INCLUDE_DIR) = ${BZIP2_INCLUDE_DIR}\n"
    "    libraries (BZIP2_LIBRARIES) = ${BZIP2_LIBRARIES}\n"
    "  [tiff]\n"
    "    version (TIFF_VERSION) = ${TIFF_VERSION}\n"
    "    include dir (TIFF_INCLUDE_DIR) = ${TIFF_INCLUDE_DIR}\n"
    "    libraries (TIFF_LIBRARIES) = ${TIFF_LIBRARIES}\n"

    "\n[Python]\n"
    "  version (Python3_VERSION) = ${Python3_VERSION}\n"
    "  executable (Python3_EXECUTABLE) = ${Python3_EXECUTABLE}\n"
    "  include dirs (Python3_INCLUDE_DIRS) = ${Python3_INCLUDE_DIRS}\n"
    "  libraries (Python3_LIBRARIES) = ${Python3_LIBRARIES}\n"
    "  sitelib (Python3_SITELIB) = ${Python3_SITELIB}\n"
    "  stdlib (Python3_STDLIB) = ${Python3_STDLIB}\n"
    "  [NumPy]\n"
    "    version (Python3_NumPy_VERSION) = ${Python3_NumPy_VERSION}\n"
    "    include dir (Python3_NumPy_INCLUDE_DIRS) = ${Python3_NumPy_INCLUDE_DIRS}\n"
    "  Build ${CMAKE_PROJECT_NAME} Python package (BA_PY_PACK) = ${BA_PY_PACK}\n"
    "  ${CMAKE_PROJECT_NAME} Python wheel dir (WHEEL_DIR) = ${WHEEL_DIR}\n"

    "\n[SWIG]\n"
    "  version (SWIG_VERSION) = ${SWIG_VERSION}\n"
    "  executable (SWIG_EXECUTABLE) = ${SWIG_EXECUTABLE}\n"
    "  flags (SWIG_FLAGS) = ${SWIG_FLAGS}\n"
    "  CMake definitions (SWIG_USE_FILE) = ${SWIG_USE_FILE}\n"
    "  command (SWIG_COMMAND) = ${SWIG_COMMAND}\n"
    "  API dir (SWIG_DIR) = ${SWIG_DIR}\n"

    "\n[pod2man]\n"
    "  executable (POD2MAN_EXECUTABLE) = ${POD2MAN_EXECUTABLE}\n"

    "\n[tests]\n"
    "  perform tests (BA_TESTS) = ${BA_TESTS}\n"

    "\n[examples]\n"
    " auto wrap dir (AUTO_WRAP_DIR) = ${AUTO_WRAP_DIR}\n"
    " test examples dir (EXAMPLES_TEST_DIR) = ${EXAMPLES_TEST_DIR}\n"
    " public examples dir (EXAMPLES_PUBL_DIR) = ${EXAMPLES_PUBL_DIR}\n"
    " examples figures dir (EXAMPLES_FIGURES_DIR) = ${EXAMPLES_FIGURES_DIR}\n"

    "\n[directories]\n"
    "  [build]\n"
    "    RPATH (CMAKE_BUILD_RPATH) = ${CMAKE_BUILD_RPATH}\n"
    "    build (BornAgain_BINARY_DIR) = ${BornAgain_BINARY_DIR}\n"
    "    libraries (CMAKE_LIBRARY_OUTPUT_DIRECTORY) = ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}\n"
    "    runtime (CMAKE_RUNTIME_OUTPUT_DIRECTORY) = ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}\n"
    "    source (BUILD_SRC_DIR) = ${BUILD_SRC_DIR}\n"
    "    include (BUILD_INC_DIR) = ${BUILD_INC_DIR}\n"
    "    test (TEST_OUTPUT_DIR) = ${TEST_OUTPUT_DIR}\n"
    "    var (BUILD_VAR_DIR) = ${BUILD_VAR_DIR}\n"
    "    man (BUILD_MAN_DIR) = ${BUILD_MAN_DIR}\n"

    "  [install]\n"
    "    RPATH (CMAKE_INSTALL_RPATH) = ${CMAKE_INSTALL_RPATH}\n"
    "    path prefix (CMAKE_INSTALL_PREFIX) = ${CMAKE_INSTALL_PREFIX}\n"
    "    user executables (destination_bin) = ${destination_bin}\n"
    "    include (destination_include) = ${destination_include}\n"
    "    libraries (destination_lib) = ${destination_lib}\n"
    "    share (destination_share) = ${destination_share}\n"
    "    examples (destination_examples) = ${destination_examples}\n"
    "    images (destination_images) = ${destination_images}\n"
    "    man pages (destination_man) = ${destination_man}\n"
    "    installer name (CPACK_PACKAGE_FILE_NAME) = ${CPACK_PACKAGE_FILE_NAME}\n"
    "    source package name (CPACK_SOURCE_PACKAGE_FILE_NAME) = ${CPACK_SOURCE_PACKAGE_FILE_NAME}\n"
    "    package components (CPACK_COMPONENTS_ALL) = ${CPACK_COMPONENTS_ALL}\n"
)

set(BA_BUILD_LOG "${log}")
