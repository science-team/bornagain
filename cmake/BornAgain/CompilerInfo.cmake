##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/CompilerInfo.cmake
##! @brief     Prints the final compiler flags
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

message(STATUS "BornAgain Architecture: ${BA_ARCHITECTURE}")

message(STATUS "Default compiler flags (may be changed in later CMake steps):")
if(CMAKE_CONFIGURATION_TYPES)
    if("Release" IN_LIST CMAKE_CONFIGURATION_TYPES)
        message("    Release: ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE}")
    endif()
    if("Debug" IN_LIST CMAKE_CONFIGURATION_TYPES)
        message("    Debug: ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG}")
    endif()
else()
    if(CMAKE_BUILD_TYPE STREQUAL "Release")
        message("    ${CMAKE_BUILD_TYPE}: ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE}")
    elseif(CMAKE_BUILD_TYPE STREQUAL "Debug")
        message("    ${CMAKE_BUILD_TYPE}: ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG}")
    else()
        message(FATAL_ERROR "Unsupported CMAKE_BUILD_TYPE '${CMAKE_BUILD_TYPE}'")
    endif()
endif()

message(STATUS "Shared linker Flags: ${CMAKE_SHARED_LINKER_FLAGS}")
message(STATUS "Exe linker Flags: ${CMAKE_EXE_LINKER_FLAGS}")
