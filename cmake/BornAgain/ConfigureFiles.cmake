##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/ConfigureFiles.cmake
##! @brief     Configure some files given as *.in
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

if(WIN32)
    # Necessary to provide correct slashes in BABuild.h
    file(TO_CMAKE_PATH ${Python3_EXECUTABLE} Python3_EXECUTABLE)
    file(TO_CMAKE_PATH ${Python3_STDLIB} Python3_STDLIB)
    file(TO_CMAKE_PATH ${Python3_INCLUDE_DIRS} Python3_INCLUDE_DIRS)
    file(TO_CMAKE_PATH ${Python3_SITELIB} Python3_SITELIB)

    # Necessary for Visual Studio (multi-config-generator): provide also debug-exe of python3
    string(REPLACE ".exe" "_d.exe"  Python3_EXECUTABLE_DEBUG ${Python3_EXECUTABLE})
endif()

configure_file(${CONFIGURABLES_DIR}/BAVersion.h.in  ${BUILD_INC_DIR}/BAVersion.h @ONLY)
configure_file(${CONFIGURABLES_DIR}/BABuild.h.in  ${BUILD_INC_DIR}/BABuild.h @ONLY)
configure_file(${CONFIGURABLES_DIR}/BATesting.h.in  ${BUILD_INC_DIR}/BATesting.h @ONLY)

configure_file(${CONFIGURABLES_DIR}/CTestCustom.cmake.in ${CMAKE_BINARY_DIR}/CTestCustom.cmake)
