##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/Coverage.cmake
##! @brief     Sets up custom target 'coverage'. Outcome will be written to html_dir.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

include(cmake/commons/CoverageFunction)

set(coverage_ignore_dirs "'/usr/*'")
list(APPEND coverage_ignore_dirs "'*/3rdparty/*'")

set(html_dir ${CMAKE_CURRENT_BINARY_DIR}/coverage)
file(MAKE_DIRECTORY ${html_dir})

# use function defined in commons/CoverageFunction
add_coverage_target(coverage "${coverage_ignore_dirs}" ${html_dir})
