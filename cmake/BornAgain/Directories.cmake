##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/Directories.cmake
##! @brief     Make directories, and set directory valued variables.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************
include(GNUInstallDirs)

# -----------------------------------------------------------------------------
# source directories
# -----------------------------------------------------------------------------

set(TOOL_DIR ${CMAKE_SOURCE_DIR}/devtools)
set(WRAP_DIR ${CMAKE_SOURCE_DIR}/Wrap)
set(SWIG_DIR ${WRAP_DIR}/Swig)

set(GUI_PROJECTS_DIR            ${CMAKE_SOURCE_DIR}/Tests/GUIprojects)
set(REFERENCE_DIR               ${CMAKE_SOURCE_DIR}/Tests/ReferenceData)
set(REFERENCE_DIR_CORESPECIAL   ${REFERENCE_DIR}/CoreSpecial)
set(REFERENCE_DIR_MINIEXAMPLES  ${REFERENCE_DIR}/MiniExamples)
set(REFERENCE_DIR_SUITE         ${REFERENCE_DIR}/Suite)

# -----------------------------------------------------------------------------
# auto directories (refreshed when CONFIGURE options are set)
# -----------------------------------------------------------------------------

set(AUTO_WRAP_DIR ${CMAKE_SOURCE_DIR}/auto/Wrap)
set(EXAMPLES_SRC_DIR ${CMAKE_SOURCE_DIR}/rawEx)
set(EXAMPLES_DATA_DIR ${CMAKE_SOURCE_DIR}/testdata)
set(EXAMPLES_PUBL_DIR ${CMAKE_SOURCE_DIR}/auto/Examples)
set(EXAMPLES_TEST_DIR ${CMAKE_SOURCE_DIR}/auto/MiniExamples)
set(EXAMPLES_FIGURES_DIR ${CMAKE_SOURCE_DIR}/auto/FigExamples)

file(MAKE_DIRECTORY ${AUTO_WRAP_DIR})
file(MAKE_DIRECTORY ${EXAMPLES_PUBL_DIR})
file(MAKE_DIRECTORY ${EXAMPLES_TEST_DIR})
file(MAKE_DIRECTORY ${EXAMPLES_FIGURES_DIR})

# -----------------------------------------------------------------------------
# output directories
# -----------------------------------------------------------------------------

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY         ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY         ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG   ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

set(BUILD_VAR_DIR ${CMAKE_BINARY_DIR}/var)
set(BUILD_INC_DIR ${CMAKE_BINARY_DIR}/inc)
set(BUILD_SRC_DIR ${CMAKE_BINARY_DIR}/src)

set(TEST_OUTPUT_DIR             ${CMAKE_BINARY_DIR}/test_output)
set(TEST_OUTPUT_DIR_SUITE       ${TEST_OUTPUT_DIR}/Suite)

configure_file("${CONFIGURABLES_DIR}/auto_README.in.md" "${CMAKE_SOURCE_DIR}/auto/README.md" @ONLY)

file(MAKE_DIRECTORY ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
file(MAKE_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
file(MAKE_DIRECTORY ${BUILD_VAR_DIR})
file(MAKE_DIRECTORY ${BUILD_INC_DIR})
file(MAKE_DIRECTORY ${BUILD_SRC_DIR})
file(MAKE_DIRECTORY ${TEST_OUTPUT_DIR})
file(MAKE_DIRECTORY ${TEST_OUTPUT_DIR_SUITE})

# -----------------------------------------------------------------------------
# file extensions
# -----------------------------------------------------------------------------
set(libprefix _lib)

if(WIN32)
    # under Windows, .pyd files are needed as the Python extension; see
    # <https://docs.python.org/3/faq/windows.html#is-a-pyd-file-the-same-as-a-dll>
    set(libsuffix .pyd)
else()
    set(libsuffix .so)
endif()

# -----------------------------------------------------------------------------
# destinations
# -----------------------------------------------------------------------------
set(destination_suffix ${CMAKE_PROJECT_NAME})

set(dstIds "")

if(BA_APPLE_BUNDLE)
    # The variable destination_root must be set with a trailing slash
    # to keep it compatible with the default value, which is just the empty string.
    set(destination_root BornAgain.app/Contents/)
    list(APPEND dstIds "bundle")
endif()

set(destination_bin ${destination_root}${CMAKE_INSTALL_BINDIR})
set(destination_lib ${destination_root}${CMAKE_INSTALL_LIBDIR})
if(WIN32)
    # on Window, the libraries must be in the same folder as the executable
    set(destination_lib ${destination_bin})
endif()
set(destination_include ${destination_root}${CMAKE_INSTALL_INCLUDEDIR}/${CMAKE_PROJECT_NAME})
set(destination_share ${destination_root}${CMAKE_INSTALL_DATAROOTDIR}/${CMAKE_PROJECT_NAME})
set(destination_examples ${destination_share}/Examples)
set(destination_images ${destination_share}/Images)
set(destination_man ${destination_root}${CMAKE_INSTALL_DATAROOTDIR}/man/man1)

list(APPEND dstIds bin lib include share examples images man)

message(STATUS "Destination directories:")
foreach(dstId IN LISTS dstIds)
    message("     ${dstId} -> ${destination_${dstId}}")
endforeach()
