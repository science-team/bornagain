##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/FindLibraries.cmake
##! @brief     Find required libraries.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

# list of extra dependencies (needed for a self-contained package)
set(BA_Dependencies "")

if(WIN32)
    set(CMAKE_FIND_LIBRARY_SUFFIXES .dll.a .lib)
endif()

find_package(LibHeinz 2.0.0 REQUIRED CONFIG)
message(STATUS "LibHeinz: version=${LibHeinz_VERSION}, incl=${LibHeinz_INCLUDE_DIR}")

find_package(formfactor 0.3.0 REQUIRED CONFIG)
# absolute path is needed for deployment in Python wheel
get_target_property(formfactor_LIBRARIES formfactor IMPORTED_LOCATION_RELEASE)
if(NOT formfactor_LIBRARIES)
    get_target_property(formfactor_LIBRARIES formfactor IMPORTED_LOCATION_NOCONFIG)
endif()
if(NOT formfactor_LIBRARIES)
    message(FATAL_ERROR "Formfactor libraries not found (path: '${formfactor_DIR}').")
endif()
message(STATUS "formfactor: version=${formfactor_VERSION}, incl=${formfactor_INCLUDE_DIR}, "
    "lib=${formfactor_LIBRARIES}")

list(APPEND BA_Dependencies "${formfactor_LIBRARIES}")

find_package(Threads REQUIRED)
find_package(FFTW3 REQUIRED)

list(APPEND BA_Dependencies "${FFTW3_LIBRARIES}")

find_package(GSL REQUIRED)
message(STATUS "GSL: version=${GSL_VERSION}, incl=${GSL_INCLUDE_DIR}, libs=${GSL_LIBRARIES}")

list(APPEND BA_Dependencies "${GSL_LIBRARIES}")

if(WIN32)
    find_package(cerf CONFIG REQUIRED COMPONENTS shared CXX)
    add_compile_definitions(CERF_AS_CPP=ON)
    get_property(Cerf_LIBRARIES TARGET cerf::cerfcpp PROPERTY LOCATION)
    message(STATUS "cerf: C++ version = ${cerf_VERSION}, lib=${Cerf_LIBRARIES}")
    set(cerf_target cerf::cerfcpp)
else()
    # For the time being, under *nix we request the C version of libcerf.
    find_package(cerf CONFIG REQUIRED COMPONENTS shared C)
    get_property(Cerf_LIBRARIES TARGET cerf::cerf PROPERTY LOCATION)
    message(STATUS "cerf: C version = ${cerf_VERSION}, lib=${Cerf_LIBRARIES}")
    set(cerf_target cerf::cerf)
endif()

list(APPEND BA_Dependencies "${Cerf_LIBRARIES}")

# --- Compression ---
set(ZLIB_USE_STATIC_LIBS OFF)
find_package(ZLIB REQUIRED)
list(APPEND BA_Dependencies "${ZLIB_LIBRARIES}")
message(STATUS "ZLIB: version=${ZLIB_VERSION}, incl=${ZLIB_INCLUDE_DIRS}, libs=${ZLIB_LIBRARIES}")

set(BZip2_USE_STATIC_LIBS OFF)
find_package(BZip2 REQUIRED)
list(APPEND BA_Dependencies "${BZip2_LIBRARIES}")
message(STATUS "BZIP2: version=${BZIP2_VERSION}, incl=${BZIP2_INCLUDE_DIRS}, libs=${BZIP2_LIBRARIES}")

# --- Boost headers (we only use header-only components) ---
set(Boost_NO_BOOST_CMAKE ON) # prevent shortcut
set(Boost_USE_MULTITHREADED ON)
set(Boost_NO_WARN_NEW_VERSIONS ON) # suppress annoying and pointless warnings

find_package(Boost 1.74 REQUIRED)
message(STATUS "Boost: includes at ${Boost_INCLUDE_DIRS}, libraries at ${Boost_LIBRARY_DIRS}")
message(STATUS "Boost libraries: ${Boost_LIBRARIES}; ${Boost_Additional_Libraries}")
if(NOT Boost_FOUND)
    message(FATAL_ERROR "Boost not found")
endif()

# === optional packages ===

# --- Tiff ---
if(BA_TIFF_SUPPORT)
    message(STATUS "Looking for libtiff (use -DBA_TIFF_SUPPORT=OFF to disable)")
    find_package(TIFF 4.0.2 REQUIRED COMPONENTS CXX)

    list(APPEND BA_Dependencies "${TIFF_LIBRARIES}")
endif()
