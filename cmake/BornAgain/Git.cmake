##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/Git.cmake
##! @brief     Retrieve info from Git, and set variables GIT_*, for use in logging.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

# obtain the hash of the last commit
execute_process(COMMAND git log --pretty=format:%h -n 1
                OUTPUT_VARIABLE GIT_COMMIT
                ERROR_QUIET
)
string(STRIP "${GIT_COMMIT}" GIT_COMMIT)

# Check whether we got any revision (which isn't
# always the case, e.g. when someone downloaded a zip
# file from Github instead of a checkout)
if (NOT "${GIT_COMMIT}" STREQUAL "")
    # git tag (if any)
    execute_process(
        COMMAND git describe --exact-match --tags
        OUTPUT_VARIABLE GIT_TAG ERROR_QUIET)
    # git branch name
    execute_process(
        COMMAND git branch --show-current
        OUTPUT_VARIABLE GIT_BRANCH)

    string(STRIP "${GIT_TAG}" GIT_TAG)
    string(STRIP "${GIT_BRANCH}" GIT_BRANCH)
endif()
