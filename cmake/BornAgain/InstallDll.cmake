##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/InstallDll.cmake
##! @brief     Create install targets for library dependencies under Windows.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

# list of Windows extra dependencies (needed for a self-contained package)
set(BA_Dependencies_WIN32 "")

## GSL library
message(STATUS "Installation of GSL library:")
foreach(lib ${GSL_LIBRARIES})
    get_filename_component(UTF_BASE_NAME ${lib} NAME_WE)
    get_filename_component(UTF_PATH ${lib} PATH)
    list(APPEND BA_Dependencies_WIN32 "${UTF_PATH}/${UTF_BASE_NAME}.dll")
    install(FILES ${UTF_PATH}/${UTF_BASE_NAME}.dll
        DESTINATION ${destination_lib} COMPONENT WinLibraries
        CONFIGURATIONS [Release|MinSizeRel|RelWithDebInfo])
    message("    ${UTF_PATH}/${UTF_BASE_NAME}.dll - will be installed in ${destination_lib}")
endforeach()

## CERF library
message(STATUS "Installation of Cerf library:")
install(FILES ${Cerf_LIBRARIES} DESTINATION ${destination_lib} COMPONENT WinLibraries)
message(STATUS "Cerf dll: ${Cerf_LIBRARIES} - will be installed in ${destination_lib}")
list(APPEND BA_Dependencies_WIN32 "${Cerf_LIBRARIES}")

## FFTW3
message(STATUS "Installation of FFTW3:")
foreach(lib ${FFTW3_LIBRARIES})
    get_filename_component(UTF_BASE_NAME ${lib} NAME_WE)
    get_filename_component(UTF_PATH ${lib} PATH)
    list(APPEND BA_Dependencies_WIN32 "${UTF_PATH}/${UTF_BASE_NAME}.dll")
    install(FILES ${UTF_PATH}/${UTF_BASE_NAME}.dll
        DESTINATION ${destination_lib} COMPONENT WinLibraries
        CONFIGURATIONS [Release|MinSizeRel|RelWithDebInfo])
    message("    ${UTF_PATH}/${UTF_BASE_NAME}.dll - will be installed in ${destination_lib}")
endforeach()

## compression libraries
message(STATUS "Installation of compression libraries:")
foreach(lib ${ZLIB_LIBRARIES} ${BZIP2_LIBRARIES})
    get_filename_component(UTF_BASE_NAME ${lib} NAME_WE)
    get_filename_component(UTF_PATH ${lib} PATH)
    list(APPEND BA_Dependencies_WIN32 "${UTF_PATH}/${UTF_BASE_NAME}.dll")
    install(FILES ${UTF_PATH}/${UTF_BASE_NAME}.dll
        DESTINATION ${destination_lib} COMPONENT WinLibraries
        CONFIGURATIONS [Release|MinSizeRel|RelWithDebInfo])
    message("    ${UTF_PATH}/${UTF_BASE_NAME}.dll - will be installed in ${destination_lib}")
endforeach()

## TIFF
if(BA_TIFF_SUPPORT)
    ## Same as for Boost above: list may contain mix of debug/optimized libs
    message(STATUS "Installation of TIFF:")
    set(entryShallBeSkipped false)
    foreach(LIB ${TIFF_LIBRARIES})
        if(entryShallBeSkipped)
            set(entryShallBeSkipped false)
            continue()
        endif()

        if(${LIB} STREQUAL "debug")
            set(entryShallBeSkipped true)
            continue()
        endif()

        if(${LIB} STREQUAL "optimized")
            set(entryShallBeSkipped false)
            continue()
        endif()

        get_filename_component(UTF_NAME ${LIB} NAME)
        if(${UTF_NAME} STREQUAL "tiff.lib")
            message("    No installation for ${UTF_NAME}: This is the static TIFF library")
            continue()
        endif()
        string(REPLACE ".lib" ".dll" DLL "${LIB}")
        if(NOT EXISTS ${DLL})
            message(FATAL_ERROR
                "Dynamic link library ${DLL} (derived from ${LIB}) does not exist")
        endif()
        list(APPEND BA_Dependencies_WIN32 "${DLL}")
        install(FILES ${DLL} COMPONENT WinLibraries
            DESTINATION ${destination_lib}
            CONFIGURATIONS [Release|MinSizeRel|RelWithDebInfo])
        message("    ${DLL} - will be installed in ${destination_lib}")
    endforeach()
endif()

## formfactor library
message(STATUS "Installation of formfactor library:")
set(formfactor_DLL ${formfactor_LIBRARIES})

if(NOT EXISTS ${formfactor_DLL})
    message(FATAL_ERROR
        "Dynamic link library ${formfactor_DLL} (need for formfactor) does not exist")
endif()
list(APPEND BA_Dependencies_WIN32 "${formfactor_DLL}")
install(FILES ${formfactor_DLL} DESTINATION ${destination_lib} COMPONENT WinLibraries)
message(STATUS "formfactor dll: ${formfactor_DLL} - will be installed in ${destination_lib}")
