##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/Linux.cmake
##! @brief     Set variables and compiler flags for Linux.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

#... Set architecture variables

if(${CMAKE_SYSTEM_PROCESSOR} STREQUAL x86_64)
    set(BA_ARCHITECTURE linux_x64)
else()
    set(BA_ARCHITECTURE "linux_${CMAKE_SYSTEM_PROCESSOR}")
endif()

#... Set compile options

set(CMAKE_EXPORT_COMPILE_COMMANDS ON) # generate compile_commands.json, used by code analysis tools

set(CMAKE_CXX_FLAGS_RELEASE        "-O3")
set(CMAKE_CXX_FLAGS_DEBUG          "-g3")
if(GCC)
    string(APPEND CMAKE_CXX_FLAGS_DEBUG " -O0") # was longtime -Og
else()
    string(APPEND CMAKE_CXX_FLAGS_DEBUG " -O0") # was longtime -O2
endif()

string(APPEND CMAKE_CXX_FLAGS " -I${CMAKE_SOURCE_DIR}/Wrap -pipe ${BIT_ENVIRONMENT}")
string(APPEND CMAKE_CXX_FLAGS " -Wall -W -Woverloaded-virtual -Wno-unknown-pragmas -fPIC")
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    string(APPEND CMAKE_CXX_FLAGS " -Wno-restrict") # under g++ -std=c++20, warnings from <string>
endif()
set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--no-undefined")
