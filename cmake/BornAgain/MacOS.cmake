##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/MacOS.cmake
##! @brief     Set variables and compiler flags for MacOS.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

#... Check OS version

if(NOT CMAKE_SYSTEM_NAME MATCHES Darwin)
    message(FATAL_ERROR "Found a non-Darwin system, currently unsupported")
endif()

execute_process(COMMAND sw_vers "-productVersion"
    COMMAND cut -d . -f 1-2
    OUTPUT_VARIABLE MACOSX_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND sw_vers "-productVersion"
    COMMAND cut -d . -f 2
    OUTPUT_VARIABLE MACOSX_MINOR OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "Found a MacOS System ${MACOSX_VERSION}, minor ${MACOSX_MINOR}")

if(${MACOSX_VERSION} LESS 11)
    message(FATAL_ERROR "Found a MacOS < 11.0, which is unsupported")
endif()

#... Set architecture variables

if(${CMAKE_SYSTEM_PROCESSOR} MATCHES arm)
    set(BA_ARCHITECTURE "mac_arm")
else()
    set(BA_ARCHITECTURE "mac_x64")
endif()

#... Set compile options

# On OS X, the `-dead_strip` flag removes unneeded symbols/object files, and
# `-dead_strip_dylibs` flag removes unneeded libraries [see `man ld` on OSX]
set(DEADSTRIP_LFLAGS "-dead_strip -dead_strip_dylibs")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS}")
string(APPEND CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS " -m64 ${DEADSTRIP_LFLAGS}")
string(APPEND CMAKE_CXX_FLAGS " -m64 -I${CMAKE_SOURCE_DIR}/Wrap")

if(CMAKE_COMPILER_IS_GNUCXX)
    message(STATUS "Found GNU compiler collection")
    execute_process(COMMAND ${CMAKE_CXX_COMPILER}
        -dumpversion OUTPUT_VARIABLE GCC_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)

    string(APPEND CMAKE_CXX_FLAGS " -pipe -W -Wall -Woverloaded-virtual -fsigned-char -fno-common")
    set(CINT_CXX_DEFINITIONS
        "-DG__REGEXP -DG__UNIX -DG__SHAREDLIB -DG__ROOT -DG__REDIRECTIO\
 -DG__OSFDLL -DG__STD_EXCEPTION")

    string(APPEND CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS
        " -flat_namespace -undefined dynamic_lookup")

    # Select flags.
    set(CMAKE_CXX_FLAGS_RELEASE        "-O2")
    set(CMAKE_CXX_FLAGS_DEBUG          "-g3 -O2 -fno-reorder-blocks -fno-inline")

    #settings for cint
    set(CPPPREP "${CMAKE_CXX_COMPILER} -E -C")
    set(CXXOUT "-o ")
    set(EXEEXT "")
    set(SOEXT "so")

elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL Clang OR ${CMAKE_CXX_COMPILER_ID} STREQUAL AppleClang)
    message(STATUS "Found LLVM compiler collection")
    execute_process(COMMAND ${CMAKE_CXX_COMPILER}
        -dumpversion OUTPUT_VARIABLE GCC_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)

    string(APPEND CMAKE_CXX_FLAGS " -pipe -W -Wall -Woverloaded-virtual -fsigned-char -fno-common")

    string(APPEND CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS
        " -flat_namespace -undefined dynamic_lookup")

    # Select flags.
    set(CMAKE_CXX_FLAGS_RELEASE        "-O2")
    set(CMAKE_CXX_FLAGS_DEBUG          "-g3 -O2 -fno-inline")

    #settings for cint
    set(CPPPREP "${CMAKE_CXX_COMPILER} -E -C")
    set(CXXOUT "-o ")
    set(EXEEXT "")
    set(SOEXT "so")

else()
    message(FATAL_ERROR "Found unsupported compiler.")

endif()
