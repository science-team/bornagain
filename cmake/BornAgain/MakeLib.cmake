##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/MakeLib.cmake
##! @brief     Define function MakeLib, for configuring one component library.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

# Add a BornAgain library to the Python wheel
# NOTE: Target 'ba_wheel' must be already defined.
# NOTE: only used by function MakeLib.

function(add_library_to_wheel lib)

    add_dependencies(ba_wheel ${lib})
    # eg., 'libBornAgain.so'
    get_target_property(libfilename ${lib} _BASEFILENAME)

    # copy the shared library and its Python interface to the Python wheel folder
    set(_dst ${BA_PY_LIBRARY_OUTPUT_DIR}/)

    add_custom_command(TARGET ${lib}
        POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy "$<TARGET_FILE:${lib}>" ${_dst}/${libfilename}
        COMMENT "Add library ${lib} to the Python wheel..."
    )

    get_target_property(lib_LIBRARY_PY ${lib} _LIBRARY_PY)
    string(STRIP ${lib_LIBRARY_PY} lib_LIBRARY_PY)
    if(lib_LIBRARY_PY)
        add_custom_command(TARGET ${lib}
            POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy "${lib_LIBRARY_PY}" ${_dst}
            COMMENT "Add Python wrapper-script of library ${lib} to the Python wheel..."
        )
    endif()

endfunction()


# Configure one component library.

function(MakeLib lib swigtmpdir source_files include_files)

    string(STRIP "${swigtmpdir}" swigtmpdir)

    set_target_properties(${lib} PROPERTIES
        SOURCES "${source_files}"
        PREFIX ${libprefix}
        SUFFIX ${libsuffix}
        OUTPUT_NAME ${lib}
        # eg., libBornAgainBase.so
        _BASEFILENAME ${libprefix}${lib}${libsuffix})

    get_target_property(lib_dir ${lib} LIBRARY_OUTPUT_DIRECTORY)
    get_target_property(lib_name ${lib} _BASEFILENAME)
    set(BornAgain_LIBRARIES "$CACHE{BornAgain_LIBRARIES};${lib_dir}/${lib_name}"
        CACHE INTERNAL "BornAgain libraries")


    # Set runtime-location of library dependencies
    # See our deployment paper (Nejati et al 2024) for explanation.
    if(LINUX)
        set(link_flags "-Wl,--disable-new-dtags,-rpath,\$ORIGIN:\$ORIGIN/../extra")
        set_target_properties(${lib} PROPERTIES LINK_FLAGS ${link_flags})
    elseif(APPLE)
        target_link_options(${lib} PRIVATE
            "-Wl,-rpath,@loader_path,-rpath,@loader_path/extra")
    endif()


    if(BORNAGAIN_PYTHON)
        target_compile_definitions(${lib} PRIVATE -DBORNAGAIN_PYTHON)

        # SWIG-produced interface
        if(swigtmpdir)
            SwigLib(${lib} ${swigtmpdir} "${include_files}")
        endif()

        # add library to the Python wheel
        if (BA_PY_PACK)
            add_library_to_wheel(${lib})
        endif(BA_PY_PACK)

        if(WIN32)
            # Under Windows, libraries must be in the same folder as that of the executable
            add_custom_command(
                TARGET ${lib}
                POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E copy
                ${CMAKE_BINARY_DIR}/bin/${libprefix}${lib}${libsuffix}
                ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${libprefix}${lib}${libsuffix}
            )
        endif()

    endif() # BORNAGAIN_PYTHON

    # installation
    install(TARGETS ${lib}
        LIBRARY DESTINATION ${destination_lib} COMPONENT Libraries
        RUNTIME DESTINATION ${destination_lib} COMPONENT Libraries)

    if(BA_CPP_API)
        foreach(file ${include_files})
            get_filename_component(dir ${file} DIRECTORY)
            install(FILES ${file} DESTINATION ${destination_include}/${name}/${dir})
        endforeach()
    endif()

endfunction()
