##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/Pack.cmake
##! @brief     Top-level CPack configuration.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

# BornAgain packaging

include(InstallRequiredSystemLibraries)

configure_file(COPYING LICENSE.txt COPYONLY)
set(CPACK_RESOURCE_FILE_LICENSE ${CMAKE_BINARY_DIR}/LICENSE.txt)

set(CPACK_PACKAGE_RELOCATABLE True)

set(CPACK_SOURCE_PACKAGE_FILE_NAME ${CMAKE_PROJECT_NAME}-${CMAKE_PROJECT_VERSION})
set(CPACK_PACKAGE_INSTALL_DIRECTORY ${CPACK_SOURCE_PACKAGE_FILE_NAME})

set(SELECTED_PYTHON_VERSION "python${Python3_VERSION_MAJOR}.${Python3_VERSION_MINOR}")
set(_pkgname ${CPACK_SOURCE_PACKAGE_FILE_NAME}-${SELECTED_PYTHON_VERSION}-${BA_ARCHITECTURE})

set(CPACK_PACKAGE_VENDOR "Forschungszentrum Juelich GmbH")
set(CPACK_PACKAGE_MAINTAINER "Scientific Computing Group at Heinz Maier-Leibnitz Zentrum Garching")
set(CPACK_PACKAGE_CONTACT "contact@bornagainproject.org")
set(CPACK_PACKAGE_HOMEPAGE_URL "https://bornagainproject.com")
set(CPACK_RESOURCE_FILE_README "${CMAKE_SOURCE_DIR}/README.md")
set(CPACK_VERBATIM_VARIABLES TRUE)

if(WIN32)
    # use Qt Installer Framework
    set(CPACK_GENERATOR IFW)

    set(CPACK_IFW_PACKAGE_TITLE "BornAgain ${PROJECT_VERSION}")
    set(CPACK_IFW_PACKAGE_NAME "BornAgain")
    set(CPACK_IFW_PACKAGE_ICON "${CMAKE_CURRENT_SOURCE_DIR}/GUI/images/BornAgain.ico" )
    set(CPACK_IFW_PACKAGE_WINDOW_ICON "${CPACK_IFW_PACKAGE_ICON}")
    set(CPACK_IFW_VERBOSE ON)
    set(CPACK_IFW_TARGET_DIRECTORY "C:/BornAgain-${PROJECT_VERSION}")
    set(CPACK_IFW_ARCHIVE_COMPRESSION 9)  # Ultra compression
    set(CPACK_IFW_PACKAGE_ALLOW_NON_ASCII_CHARACTERS OFF)
    set(CPACK_IFW_PACKAGE_START_MENU_DIRECTORY "BornAgain ${PROJECT_VERSION}")

elseif(APPLE)
    set(CPACK_GENERATOR "DragNDrop")

elseif(UNIX)
    set(CPACK_GENERATOR STGZ) # generates self-extracting tgz with file extension .sh

endif()

set(CPACK_PACKAGE_FILE_NAME ${_pkgname})

# NOTE: The 'Unspecified' component is a _default_ component of CPack needed to finalize
# the installer. It must be at the end of all other components.
set(_ALL_COMPONENTS Libraries Headers Examples Applications Runtime Unspecified)

if(WIN32)
    set(CPACK_COMPONENTS_ALL ${_ALL_COMPONENTS} WinLibraries)
else()
    set(CPACK_COMPONENTS_ALL ${_ALL_COMPONENTS})
endif()


include(CPack)

if(WIN32)
    include(CPackIFW REQUIRED)

    # add the script to create desktop and start-menu shortcuts under Windows
    cpack_ifw_configure_component(Applications
        LICENSES "License" "${CMAKE_SOURCE_DIR}/COPYING"
        SCRIPT "${CMAKE_SOURCE_DIR}/cmake/BornAgain/qt_installscript_windows.js")
endif(WIN32)

# reset package name (CPack bug: the original value is overwritten by CPack module)
set(CPACK_PACKAGE_FILE_NAME ${_pkgname})

message(STATUS "Installer name: ${CPACK_PACKAGE_FILE_NAME}")
message(STATUS "Source package name: ${CPACK_SOURCE_PACKAGE_FILE_NAME}")
message(STATUS "Package components: ${CPACK_COMPONENTS_ALL}")
