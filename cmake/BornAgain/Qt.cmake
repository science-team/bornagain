##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/Qt.cmake
##! @brief     Find OpenGL and Qt.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

find_package(OpenGL REQUIRED)

set(QtComponents Core Gui Widgets PrintSupport Svg OpenGL OpenGLWidgets)
find_package(Qt6 COMPONENTS ${QtComponents} REQUIRED)

message(STATUS "Found Qt6 version ${Qt6Core_VERSION}")

if(${Qt6Core_VERSION} VERSION_LESS "6.2")
    message(FATAL_ERROR "Qt 6.2 is required, have ${Qt6Core_VERSION}")
elseif(${Qt6Core_VERSION} VERSION_GREATER_EQUAL "6.2")
    string(APPEND CMAKE_CXX_FLAGS " -DQT_NO_DEPRECATED_WARNINGS")
endif()

set(Qt_DIR "${QT6_INSTALL_PREFIX}")
set(Qt_VERSION "${Qt6Core_VERSION}")
set(Qt_INCLUDE_DIR "${QT6_INSTALL_PREFIX}/${QT6_INSTALL_HEADERS}")
set(Qt_PLUGINS_DIR "${QT6_INSTALL_PREFIX}/${QT6_INSTALL_PLUGINS}")
set(Qt_LIBS_DIR "${QT6_INSTALL_PREFIX}/${QT6_INSTALL_LIBS}")
set(Qt_BIN_DIR "${QT6_INSTALL_PREFIX}/${QT6_INSTALL_BINS}")

foreach(cmp Core Gui Widgets PrintSupport Svg OpenGL)
    string(APPEND qt_libs " ${Qt6${cmp}_LIBRARIES}")
endforeach()
set(Qt_LIBRARIES "${qt_libs}")

message(STATUS "  Qt ${Qt_VERSION} directory: ${Qt_DIR}")
message(STATUS "  Qt include directory: '${Qt_INCLUDE_DIR}'")
message(STATUS "  Qt libraries directory: '${Qt_LIBS_DIR}'")
message(STATUS "  Qt plugins directory: '${Qt_PLUGINS_DIR}'")
message(STATUS "  Qt binaries directory: '${Qt_BIN_DIR}'")

get_target_property(Qt6Widgets_location Qt6::Widgets LOCATION_Release)
message(STATUS "  ${Qt6Widgets_LIBRARIES} ${Qt6Widgets_location}")
get_target_property(Qt6Core_location Qt6::Core LOCATION_Release)
message(STATUS "  ${Qt6Core_LIBRARIES} ${Qt6Core_location}")
get_target_property(Qt6Gui_location Qt6::Gui LOCATION_Release)
message(STATUS "  ${Qt6Gui_LIBRARIES} ${Qt6Gui_location}")
get_target_property(Qt6OpenGL_location Qt6::OpenGL LOCATION_Release)
message(STATUS "  ${Qt6OpenGL_LIBRARIES} ${Qt6OpenGL_location}")
