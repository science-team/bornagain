##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/SourceChecks.cmake
##! @brief     Define test targets for checking source line length and other code formatting.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

#### Check automatized formatting of C++ sources

foreach(dir ${AllComponents} Tests)
    file(GLOB_RECURSE src1 ${dir}/*.cpp)
    file(GLOB_RECURSE src2 ${dir}/*.h)
    add_test(NAME ClangFormat.${dir}
        COMMAND clang-format --dry-run --Werror ${src1} ${src2})
endforeach()

#### Check non-automatized C++ rules

add_test(NAME scg.CppStyle
    COMMAND ${Python3_EXECUTABLE} ${TOOL_DIR}/checks/check-scg-cpp-style.py
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

#### Check line lengths in Python examples

file(GLOB_RECURSE sources ${EXAMPLES_PUBL_DIR}/*.py)
    # Will fail after "rm auto". Run "cmake; make; cmake" to get EXAMPLES_PUBL_DIR right.
add_test(NAME LineLength.PyExamples
    COMMAND ${Python3_EXECUTABLE} ${TOOL_DIR}/checks/check-line-length.py 85 ${sources})
