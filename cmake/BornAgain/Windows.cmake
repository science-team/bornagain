##  ************************************************************************************************
##
##  BornAgain: simulate and fit reflection and scattering
##
##! @file      cmake/BornAgain/Windows.cmake
##! @brief     Set variables and compiler flags for Windows.
##!
##! @homepage  http://www.bornagainproject.org
##! @license   GNU General Public License v3 or higher (see COPYING)
##! @copyright Forschungszentrum Jülich GmbH 2024
##! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
##
##  ************************************************************************************************

#... Set architecture variables

set(BA_ARCHITECTURE win64)

#... Set compile options

set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)

string(APPEND CMAKE_CXX_FLAGS " /MD /MP")

string(APPEND CMAKE_SHARED_LINKER_FLAGS_RELEASE
    " /NODEFAULTLIB:libcmt.lib /NODEFAULTLIB:msvcrtd.lib")

string(APPEND CMAKE_SHARED_LINKER_FLAGS_DEBUG
    " /NODEFAULTLIB:libcmt.lib")

add_compile_options($<$<CONFIG:Debug>:/ZI>)
add_compile_options("/wd4068") # don't warn about unknown pragmas
