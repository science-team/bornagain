// Installer javascript needed to create the desktop icon and start-menu shortcut Windows.
// The script is referenced in PackWindows.cmake and used by Qt IFW installer at install time.

function Component()
{
    // default constructor
}

Component.prototype.createOperations = function ()
{
    // call the base createOperations
    component.createOperations();

    // see: <https://doc.qt.io/qtinstallerframework/operations.html>
    if (systemInfo.productType === "windows")
    {
        // add desktop shortcut for the app
        component.addOperation("CreateShortcut",
                               "@TargetDir@/bin/bornagain.exe",
                               "@DesktopDir@/BornAgain.lnk",
                               "workingDirectory=@TargetDir@/bin",
                               "iconPath=@TargetDir@/bin/bornagain.exe", "iconId=0",
                               "description=Start BornAgain");

        // add start-menu shortcut to start the app
        component.addOperation("CreateShortcut",
                               "@TargetDir@/bin/bornagain.exe",
                               "@StartMenuDir@/Start BornAgain.lnk",
                               "workingDirectory=@TargetDir@/bin",
                               "iconPath=@TargetDir@/bin/bornagain.exe", "iconId=0",
                               "description=Start BornAgain");

        // add start-menu shortcut to uninstall the app
        component.addOperation("CreateShortcut",
                               "@TargetDir@/maintenancetool.exe",
                               "@StartMenuDir@/Uninstall BornAgain.lnk",
                               "iconPath=@TargetDir@/bin/bornagain.exe", "iconId=0",
                               "description=Uninstall BornAgain",
                               " --uninstall");
    }
}
