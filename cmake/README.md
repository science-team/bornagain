## Bornagain/cmake

CMake related sources.

The following overview last updated 12apr24.

### cmake/BornAgain

BornAgain specific code (in contrast to directories commons and find)

The following are included directly from top-level CMakeLists.txt:

BuildLog.cmake                write build and install configuration to log file
CompilerInfo.cmake            print compiler flags
ConfigureFiles.cmake          configure some *.in files
Coverage.cmake                define target 'coverage'
Directories.cmake             make directories, and set directory valued variables
FindLibraries.cmake           find required libraries
Git.cmake                     set Git info variables GIT_*, for use in logging
InstallDll.cmake              create library install targets under Windows
Linux.cmake                   set variables and compiler flags for Linux
MacOS.cmake                   set variables and compiler flags for MacOS
MakeLib.cmake                 define function MakeLib, used in component/CMakeLists
NixInstall.cmake              create install targets for Unix
Pack.cmake                    CPack configuration
Qt.cmake                      find OpenGL and Qt
SourceChecks.cmake            define test targets for source formatting
Windows.cmake                 set variables and compiler flags for Windows

### cmake/commons

CMake code that can also be used other projects:

CoverageFunction.cmake        used by BornAgain/Coverage
GetFilenameComponent.cmake
PreventInSourceBuilds.cmake

### cmake/configurables

auto_README.in.md
BABuild.h.in
BATesting.h.in
BAVersion.h.in
config_build.h.in
CTestCustom.cmake.in
FindBornAgain.cmake.in
FixPack.cmake.in
MacOSXBundleInfo.plist.in
qt.conf.mac

### cmake/find
FindFFTW3.cmake
FindTIFF.cmake

### cmake/multipython

FindCustomPython3.cmake
MakePythonWheel.cmake
PyDependences.cmake
SwigLib.cmake

### cmake/tests

find_pkg.py*
test_libcerf.cpp
