# CoverageFunction.cmake
#
# description: find coverage tools and provide function add_coverage_target
#
# usage:
#        cd <debug build directory>
#        cmake .. -DCMAKE_BUILD_TYPE=Debug -DBA_COVERGAE=ON
#        make (or ninja)
#        ctest (or 'make check'  or run BornAgain manually) WITHOUT -j, i.e. NO PARALLEL PROCESSING !!
#        cmake --build . --config Debug --target coverage
#
#        -> result is in coverage/index.html
#
# authors:    Jonathan Fisher, Joachim Wuttke
# maintainer: Scientific Computing Group, JCNS at MLZ Garching
# copyright:  Forschungszentrum Juelich GmbH, 2016-2025
#
# license: Public Domain

if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    message(FATAL_ERROR "C++ compiler is not GNU but '${CMAKE_CXX_COMPILER_ID}'; don't know how to set code coverage flags!")
endif()
string(APPEND CMAKE_CXX_FLAGS " -coverage")

find_program(GCOV_COMMAND gcov REQUIRED)
find_program(LCOV_COMMAND lcov REQUIRED)
find_program(GENHTML_COMMAND genhtml REQUIRED)

# function to add a coverage target
# it will scan the working directory for coverage info, ignoring the directories in ignore_directories
function(add_coverage_target targetname ignore_directories html_dir)

    set(lcov_output ${CMAKE_CURRENT_BINARY_DIR}/${targetname}.info)

    add_custom_target(${targetname}
        COMMAND ${LCOV_COMMAND}  --ignore-errors inconsistent,mismatch,negative --directory ${CMAKE_CURRENT_BINARY_DIR} --capture --output-file ${lcov_output})

    add_custom_target(${targetname}_reset
        COMMAND ${LCOV_COMMAND}  --ignore-errors inconsistent,mismatch,negative --directory ${CMAKE_CURRENT_BINARY_DIR} --zerocounters)

    foreach(dirname ${ignore_directories})
        message(STATUS "ignoring dirname = ${dirname} in code coverage")
        add_custom_command(TARGET ${targetname} POST_BUILD
            COMMAND ${LCOV_COMMAND}  --ignore-errors inconsistent,mismatch,negative --remove ${lcov_output} ${dirname} --output-file ${lcov_output}.clean
            COMMAND ${CMAKE_COMMAND} -E rename ${lcov_output}.clean ${lcov_output})
    endforeach()

    add_custom_command(TARGET ${targetname} POST_BUILD
        COMMAND ${GENHTML_COMMAND} ${lcov_output} --output-directory ${html_dir}
        COMMENT "open index.html in your webbrowser to see the code coverage report.")

endfunction()
