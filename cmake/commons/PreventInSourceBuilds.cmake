# PreventInSourceBuilds.cmake
#
# Forbids in-source builds.
#
# Rationale:
# There is no good reason to run CMake from the main source directory,
# as this would mess up the directory with compilation output.
# If CMake is nonetheless run in-source, then this is most likely by accident.
# This script is meant to prevent this specific accident.
#
# Usage:
# Insert the line "include(PreventInSourceBuilds)" near the top
# of the top-level CMakeLists.txt.
#
# License:
# No rights reserved (CC0).
#
# Author:
# Scientific Computing Group of JCNS-MLZ.
#
# Copyright:
# Forschungszentrum Jülich GmbH 2021

function(prevent_in_source_builds)
  # make sure the user doesn't play dirty with symlinks
  get_filename_component(srcdir ${CMAKE_SOURCE_DIR} REALPATH)
  get_filename_component(bindir ${CMAKE_BINARY_DIR} REALPATH)

  # disallow in-source builds
  if(srcdir STREQUAL bindir)
    message(FATAL_ERROR "\

CMake must not to be run in the source directory. \
Rather create a dedicated build directory and run CMake there. \
To clean up after this aborted in-place compilation:
  rm -r CMakeCache.txt CMakeFiles
")
  endif()
endfunction()

prevent_in_source_builds()
