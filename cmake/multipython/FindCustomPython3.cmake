function(find_custom_python3)
    # searches for a Python platform in the default paths or in a given path.
    # usage:
    # * find the default Python platform with verbose mode off:
    #   find_custom_python3(DEFAULT_PATH)
    # * find Python platform in `path/to/py38` with verbose mode on:
    #   find_custom_python3(PATHS "path/to/py38" VERBOSE)

    # parse function arguments for text like 'PATHS xy',
    # and set local variable '__PATHS' to 'xy'.
    cmake_parse_arguments("_" "DEFAULT_PATH;VERBOSE" "PATHS" "" ${ARGN})

    # if the version is already found and the variables are cached, then do nothing.
    if(Python3_FOUND AND Python3_Development_FOUND)
        return()
    endif()

    # NOTE: Unsetting the cache variable is necessary to force a new search for
    # the given Python version; otherwise, CMake will re-use the old variables.
    unset(Python3_DIR CACHE)

    set(version_minor_min 8) # minimum Python version = 3.8

    if(APPLE)
        # on MacOS, this is needed to find the Python platform in a non-standard directory
        set(__CMAKE_FIND_FRAMEWORK ${CMAKE_FIND_FRAMEWORK})
        set(CMAKE_FIND_FRAMEWORK NEVER)
    endif(APPLE)

    # if DEFAULT_PATH is set, the Python platform will be sought in
    # the CMake default paths. This is used for finding the _main_
    # Python version.
    if(__DEFAULT_PATH)
        find_package(Python3 3.${version_minor_min} REQUIRED
            COMPONENTS Interpreter Development)
    else()
        find_package(Python3 3.${version_minor_min} REQUIRED CONFIG
            COMPONENTS Interpreter Development
            PATHS "${__PATHS}" NO_DEFAULT_PATH)
    endif()

    if(APPLE)
        set(CMAKE_FIND_FRAMEWORK ${__CMAKE_FIND_FRAMEWORK})
    endif(APPLE)

    # store the exported parameters in the internal cache;
    # eg., with pytag 'py39', 'Python3_LIBRARY_DIRS_py39' will be exported, etc.
    set(_exportPars Python3_FOUND Python3_Development_FOUND
        Python3_VERSION
        Python3_VERSION_MAJOR Python3_VERSION_MINOR Python3_VERSION_PATCH
        Python3_INCLUDE_DIRS Python3_LIBRARY_DIRS
        Python3_SITELIB Python3_STDLIB
        Python3_LIBRARIES Python3_LIBRARY_RELEASE Python3_EXECUTABLE
        )

    # Find Python3 DLL under Windows
    if(WIN32)
        if(NOT Python3_LIBRARY_DLL)
            # if `Python3_LIBRARY_DLL` is not set, then assume that
            # Python3 DLL (eg. `python39.dll`) is in the same folder as
            # the Python3 executable and has a name similar to the
            # Python3 library (eg. `python39.lib`), but with `.dll` extension.
            get_filename_component(_pydll_name ${Python3_LIBRARY_RELEASE} NAME_WE)
            get_filename_component(_pydll_dir ${Python3_EXECUTABLE} DIRECTORY)
            set(Python3_LIBRARY_DLL "${_pydll_dir}/${_pydll_name}.dll")
        endif()

        list(APPEND _exportPars Python3_LIBRARY_DLL)
    endif()

    if(Python3_FOUND)
        foreach(pyvar IN LISTS _exportPars)
            set(${pyvar} ${${pyvar}} CACHE INTERNAL "")
        endforeach()
    endif()

    if(__VERBOSE)
        message(STATUS "${CMAKE_CURRENT_FUNCTION} (version >= 3.${version_minor_min}):")
        message(STATUS "   Python3_VERSION: ${Python3_VERSION}")
        message(STATUS "   Python3_VERSION_MINOR: ${Python3_VERSION_MINOR}")
        message(STATUS "   Python3_VERSION_PATCH: ${Python3_VERSION_PATCH}")
        message(STATUS "   Python3_INTERPRETER_ID: ${Python3_INTERPRETER_ID}")
        message(STATUS "   Python3_EXECUTABLE: ${Python3_EXECUTABLE}")
        message(STATUS "   Python3_STDLIB: ${Python3_STDLIB}")
        message(STATUS "   Python3_SITELIB: ${Python3_SITELIB}")
        message(STATUS "   Python3_INCLUDE_DIRS: ${Python3_INCLUDE_DIRS}")
        message(STATUS "   Python3_LIBRARIES: ${Python3_LIBRARIES}")
        message(STATUS "   Python3_LIBRARY_RELEASE: ${Python3_LIBRARY_RELEASE}")
        if(WIN32)
        message(STATUS "   Python3_LIBRARY_DLL: ${Python3_LIBRARY_DLL}")
        endif()
        message(STATUS "   Python3_LIBRARY_DIRS: ${Python3_LIBRARY_DIRS}")
        # if(Python3_FOUND)
        #     message(STATUS "   Exported parameters: \{${_exportPars}\}")
        # endif()
    endif()

    # unset the cache variable to avoid interference with
    # later calls to `find_package(Python3)`.
    unset(Python3_DIR CACHE)

endfunction()
