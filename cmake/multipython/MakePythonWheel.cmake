set(header "Python wheel (${Python3_VERSION}):")

# Python package directory structure:
#
# py  {Python package build-workspace}
# |...{Package setup config files}
# +--src
# |  |
# |  +--bornagain
# |     |...{Package init files}
# |     +--lib  {BornAgain libraries and their Python wrappers}
# |        |
# |        +--extra  {Extra dependencies}
# |
# +--wheel  {wheel for the Python version}

set(WHEEL_ROOT_DIR "${CMAKE_SOURCE_DIR}/Wrap/Python")

# output directories:

set(PY_OUTPUT_DIR "${CMAKE_BINARY_DIR}/py")
set(WHEEL_DIR "${PY_OUTPUT_DIR}/wheel")
set(BA_PY_SOURCE_OUTPUT_DIR "${PY_OUTPUT_DIR}/src")
set(BA_PY_INIT_OUTPUT_DIR "${PY_OUTPUT_DIR}/src/bornagain")
set(BA_PY_LIBRARY_OUTPUT_DIR "${PY_OUTPUT_DIR}/src/bornagain/lib")
set(BA_PY_EXTRA_LIBRARY_OUTPUT_DIR "${PY_OUTPUT_DIR}/src/bornagain/lib/extra")

# make the directories

file(MAKE_DIRECTORY
    "${PY_OUTPUT_DIR}"
    "${WHEEL_DIR}"
    "${BA_PY_SOURCE_OUTPUT_DIR}"
    "${BA_PY_INIT_OUTPUT_DIR}"
    "${BA_PY_LIBRARY_OUTPUT_DIR}"
    "${BA_PY_EXTRA_LIBRARY_OUTPUT_DIR}"
    )

# configure setup files
configure_file(${WHEEL_ROOT_DIR}/setup.cfg.in ${PY_OUTPUT_DIR}/setup.cfg @ONLY)
configure_file(${WHEEL_ROOT_DIR}/setup.py.in ${PY_OUTPUT_DIR}/setup.py @ONLY)
configure_file(${WHEEL_ROOT_DIR}/pyproject.toml ${PY_OUTPUT_DIR} COPYONLY)
# MANIFEST.in is needed for inclusion of binary files under Windows
configure_file(${WHEEL_ROOT_DIR}/MANIFEST.in ${PY_OUTPUT_DIR} COPYONLY)

# copy text files
configure_file(${CMAKE_SOURCE_DIR}/README.md ${PY_OUTPUT_DIR} COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/COPYING ${PY_OUTPUT_DIR} COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/CITATION ${PY_OUTPUT_DIR} COPYONLY)


# dummy C extension for the Python package
# NOTE: Dummy C extension is used merely to set the correct wheel tag according to PEP 425

# determine the full wheel-name
execute_process(
    COMMAND ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/cmake/multipython/wheelname.py
    ${CMAKE_PROJECT_NAME} ${CMAKE_PROJECT_VERSION}
    OUTPUT_VARIABLE _BA_WHEEL_NAMES
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

# name for a pure-Python wheel, eg. 'BA-3.2.1-py3-none-any'
list(GET _BA_WHEEL_NAMES 0 BA_PURE_WHEEL_NAME)
set(BA_PY_PURE_WHEEL_NAME "${BA_PURE_WHEEL_NAME}.whl")

# name for a platform-dependent Python wheel; eg., 'BA-3.2.1-cp311-cp311-linux_x86_64'
list(GET _BA_WHEEL_NAMES 1 BA_WHEEL_NAME)
set(BA_PY_WHEEL_NAME "${BA_WHEEL_NAME}.whl")

message(STATUS "Python pure wheel-name: '${BA_PURE_WHEEL_NAME}'")
message(STATUS "Python platform-dependent wheel-name: '${BA_WHEEL_NAME}'")

# store init files for the Python package

configure_file(${WHEEL_ROOT_DIR}/src/bornagain/__init__.py.in
    ${BA_PY_INIT_OUTPUT_DIR}/__init__.py @ONLY)

file(GLOB _sources ${WHEEL_ROOT_DIR}/src/bornagain/*.py)
foreach(_src ${_sources})
    configure_file(${_src} ${BA_PY_INIT_OUTPUT_DIR})
endforeach()
file(GLOB _sources ${WHEEL_ROOT_DIR}/src/bornagain/lib/*.py)
foreach(_src ${_sources})
    configure_file(${_src} ${BA_PY_LIBRARY_OUTPUT_DIR})
endforeach()

# external library dependencies

if(WIN32)
    set(_extra_libs "${BA_Dependencies_WIN32}")
else()
    set(_extra_libs "${BA_Dependencies}")
endif()

if(APPLE)
    # On MacOS, building the Python packages needs further effort
    # which is performed in a dedicated shell script which
    # should be executed after build process is done (like CPack).
    set(MACPKG_EXTRA_LIBS "${_extra_libs}")

    foreach(_src ${_extra_libs})
        configure_file(${_src} ${BA_PY_EXTRA_LIBRARY_OUTPUT_DIR} COPYONLY)
    endforeach()

    if (BA_PY_PACK)
        # BA_PY_MODE=ON sets up the script to build the Python wheel
        set(BA_PY_MODE "ON")
        configure_file("${CMAKE_SOURCE_DIR}/deploy/mac/mac_package.py.in"
            ${BUILD_VAR_DIR}/mac_py_package.py @ONLY)
        unset(BA_PY_MODE)

        add_custom_target(ba_wheel
            COMMAND ${Python3_EXECUTABLE} "${BUILD_VAR_DIR}/mac_py_package.py"
            COMMENT "${header} Script to build the Python wheel: "
            "'${BUILD_VAR_DIR}/mac_py_package.py'"
        )
    endif(BA_PY_PACK)

elseif(LINUX)
    # On Linux, building the Python packages needs further effort
    # which is performed in a dedicated shell script which
    # should be executed after build process is done (like CPack).
    configure_file("${CMAKE_SOURCE_DIR}/deploy/linux/mk_wheel_multilinux.sh.in"
        ${BUILD_VAR_DIR}/mk_wheel_multilinux.sh @ONLY)

    add_custom_target(ba_wheel
        COMMAND bash ${BUILD_VAR_DIR}/mk_wheel_multilinux.sh
        COMMENT "${header} Making the Python wheel..."
    )

    set(WHEEL_DIR "${WHEEL_DIR}/manylinux/")

else()
    # Windows
    message(STATUS "Will copy extra libs (${_extra_libs}) to ${BA_PY_EXTRA_LIBRARY_OUTPUT_DIR}")
    foreach(_src ${_extra_libs})
        configure_file(${_src} ${BA_PY_EXTRA_LIBRARY_OUTPUT_DIR} COPYONLY)
    endforeach()

    add_custom_target(ba_wheel
        COMMAND ${Python3_EXECUTABLE} -m pip -v wheel ${PY_OUTPUT_DIR} --no-deps --wheel ${WHEEL_DIR}
        COMMAND ${CMAKE_COMMAND} -E rename "${WHEEL_DIR}/${BA_PY_PURE_WHEEL_NAME}"
        "${WHEEL_DIR}/${BA_PY_WHEEL_NAME}"
        COMMENT "${header} Making the Python wheel..."
        )
endif()

# install the wheel in the proper directory
install(DIRECTORY "${WHEEL_DIR}/" DESTINATION "${destination_share}/wheel/"
    COMPONENT Libraries OPTIONAL FILES_MATCHING PATTERN "*.whl")

message(STATUS "${header} ${BA_PY_WHEEL_NAME} will be installed in ${destination_share}/wheel/")
