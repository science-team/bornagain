# Python dependencies

# find Python platform with/without a given path
include(cmake/multipython/FindCustomPython3)
if(SEARCH_PYTHON_PATH)
    message(STATUS "PyDependences: Custom Python platform path = '${SEARCH_PYTHON_PATH}'")
    find_custom_python3(PATHS "${SEARCH_PYTHON_PATH}" VERBOSE)
else()
    message(STATUS "PyDependences: Using default Python platform")
    find_custom_python3(DEFAULT_PATH VERBOSE)
endif()

# check presence of some Python modules
message(STATUS "Searching required Python packages...")
set(py_packages "")
if (BA_PY_PACK)
    # Python packages needed for building the BornAgain wheel
    list(APPEND py_packages "pip;wheel")
    if(LINUX)
        # on Linux, `auditwheel` is needed to produce 'manylinux' wheels repair (PEP 599)
        list(APPEND py_packages "auditwheel")
    endif()
endif()

if (BA_TESTS)
    # Python packages needed for tests
    list(APPEND py_packages "numpy;matplotlib;lmfit;fabio")
endif()
foreach(pkg ${py_packages})
    message(STATUS "Python package ${pkg}")
    execute_process(
        COMMAND ${Python3_EXECUTABLE} -c "import ${pkg}"
        RESULT_VARIABLE PKG_FOUND)
    if(NOT PKG_FOUND EQUAL 0)
        message(FATAL_ERROR "Python package '${pkg}' not found")
    endif()
endforeach()

# configure SWIG bindings
if(CONFIGURE_BINDINGS)
    find_package(SWIG 4.2 EXACT REQUIRED)
    include(${SWIG_USE_FILE})
    message(STATUS "Found SWIG version ${SWIG_VERSION} at ${SWIG_EXECUTABLE} "
        "with flags '${SWIG_FLAGS}'; CMake definitions in ${SWIG_USE_FILE}")

    # cache SWIG-related variables
    foreach(var_ SWIG_EXECUTABLE SWIG_VERSION SWIG_FLAGS SWIG_USE_FILE)
        set(${var_} ${${var_}} CACHE INTERNAL "")
    endforeach()
endif()

if(BA_PY_PACK)
    # define the target to make a Python wheel
    include(cmake/multipython/MakePythonWheel)
endif()

# install Python example scripts
install(DIRECTORY ${EXAMPLES_PUBL_DIR}
    DESTINATION ${destination_share} COMPONENT Examples FILES_MATCHING PATTERN *.py )
