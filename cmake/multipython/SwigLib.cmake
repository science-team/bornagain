# Configure Python bindings for one component library.
# Called from function MakeLib.

function(SwigLib lib swigtmpdir include_files)

    if(NOT BORNAGAIN_PYTHON)
        message(FATAL_ERROR "Function SwigLib called though BORNAGAIN_PYTHON=false\
 (building a Python API with Swig makes little sense\
 if functions that specifically support Python are disabled)")
   endif()

    message(STATUS "SwigLib: ${lib} => ${swigtmpdir}")

    if(CONFIGURE_BINDINGS)
        # static (manually written) input files
        set(swig_dependencies
            ${SWIG_DIR}/lib${lib}.i
            ${SWIG_DIR}/deprecation.i
            ${SWIG_DIR}/warnings.i
        )

        foreach(fname ${swig_dependencies})
            if(NOT EXISTS ${fname})
                message(FATAL_ERROR "Could NOT find SWIG input ${fname}")
            endif()
        endforeach()

        file(MAKE_DIRECTORY ${swigtmpdir})

        # Run Swig.
        #     Please keep -Werror, in order not to overlook critical warnings.
        #     Dispensable warnings are disabled in Wrap/Swig/warnings.i.
        #     Joachim, oct20.
        set(SWIG_FLAGS "-c++;-python;-Werror;-o;${AUTO_WRAP_DIR}/lib${lib}_wrap.cpp;-outdir;${swigtmpdir}"
            ";-I${LibHeinz_INCLUDE_DIR}"
            ";-I${CMAKE_SOURCE_DIR};-I${CMAKE_BINARY_DIR}/inc;-DSWIG_BA_VERSION=${BornAgain_VERSION}")

        add_custom_command(
            OUTPUT ${swigtmpdir}/lib${lib}.py
                   ${AUTO_WRAP_DIR}/lib${lib}_wrap.cpp
                   ${AUTO_WRAP_DIR}/lib${lib}.py
            # produce the Python wrapper
            COMMAND ${SWIG_EXECUTABLE} ${SWIG_FLAGS} ${SWIG_DIR}/lib${lib}.i
            # copy the Python wrapper to the destination build directory
            COMMAND ${CMAKE_COMMAND} -E copy
                    "${swigtmpdir}/lib${lib}.py"
                    "${AUTO_WRAP_DIR}/lib${lib}.py"
            DEPENDS ${swig_dependencies} ${include_files}
            COMMENT "SWIG: build Python wrapper ${AUTO_WRAP_DIR}/lib${lib}.py"
        )

        string(REPLACE ";" " " SWIG_FLAGS_STR "${SWIG_FLAGS}")
        set(SWIG_FLAGS_STR "${SWIG_FLAGS_STR}")
        set(SWIG_DIR "${SWIG_DIR}")
    endif()

    # assumes that lib${lib}_wrap.h, lib${lib}_wrap.cpp and lib${lib}.py
    # are already present in ${AUTO_WRAP_DIR}
    add_custom_target(lib${lib}_py_wrappers
        DEPENDS
        ${AUTO_WRAP_DIR}/lib${lib}_wrap.cpp
        ${AUTO_WRAP_DIR}/lib${lib}.py
        COMMENT "SWIG: use Python wrapper ${AUTO_WRAP_DIR}/lib${lib}.py"
    )

    # include Python headers and link to Python shared library
    target_include_directories(${lib} PRIVATE ${Python3_INCLUDE_DIRS})
    target_link_libraries(${lib} PRIVATE ${Python3_LIBRARIES})

    set_target_properties(${lib} PROPERTIES
        _LIBRARY_PY "${AUTO_WRAP_DIR}/lib${lib}.py"
    )

    add_dependencies(${lib} lib${lib}_py_wrappers)

    if((CLANG) OR (GCC))
        # suppress warnings from auto-generated code (last updated for Swig 4.0.1)
        set_source_files_properties(${AUTO_WRAP_DIR}/lib${lib}_wrap.cpp
            PROPERTIES COMPILE_OPTIONS
            "\
-Wno-unused-parameter;\
-Wno-missing-field-initializers;\
-Wno-sometimes-uninitialized;\
-Wno-deprecated-declarations")
    endif()

    # add SWIG-produced C++ wrapper source code to library sources
    get_target_property(source_files ${lib} SOURCES)
    list(APPEND source_files ${AUTO_WRAP_DIR}/lib${lib}_wrap.cpp)
    set_target_properties(${lib} PROPERTIES
        SOURCES "${source_files}")

endfunction()
