#!/usr/bin/env python3

# A BornAgain auxiliary script.
# License: Public Domain.

# Checks whether a given Python3 module is installed.
#
# Takes module name as argument.
# Returns 0 if module can be imported. Otherwise returns 1.

import sys

pkg = sys.argv[1]
try:
    __import__(pkg)
except:
    print("     find_pkg.py: package", pkg, "not found")
    sys.exit(1)

print("     find_pkg.py: package", pkg, "found")
sys.exit(0)
