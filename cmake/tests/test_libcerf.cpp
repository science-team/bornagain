#include "3rdparty/Core/cerfcpp.h"

#include <complex>

int main()
{
    std::complex<double> z(1.0, 3.0);

    cerfcx(z);

    return 0;
}
