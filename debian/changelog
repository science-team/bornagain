bornagain (22~git20250228144901.6998e3c+ds3-1) unstable; urgency=medium

  * New upstream release.
  * Update Build-Depends:

 -- Roland Mas <lolando@debian.org>  Tue, 04 Mar 2025 11:45:49 +0100

bornagain (22~git20250131135257.50eb7b6+ds3-3) unstable; urgency=medium

  * Show computed results near the relevant error message.

 -- Roland Mas <lolando@debian.org>  Sat, 01 Feb 2025 14:00:48 +0100

bornagain (22~git20250131135257.50eb7b6+ds3-2) unstable; urgency=medium

  * Show computed results near the relevant error message.

 -- Roland Mas <lolando@debian.org>  Fri, 31 Jan 2025 20:13:45 +0100

bornagain (22~git20250131135257.50eb7b6+ds3-1) unstable; urgency=medium

  * New upstream release.
  * Print computed results during testsuite runs when they differ from
    reference data, to help upstream debug the platform-dependent
    failures.

 -- Roland Mas <lolando@debian.org>  Fri, 31 Jan 2025 16:19:16 +0100

bornagain (22~git20250124151459.1c7b3fe+ds3-1) unstable; urgency=medium

  * New upstream release.

 -- Roland Mas <lolando@debian.org>  Fri, 24 Jan 2025 17:35:11 +0100

bornagain (22~git20241218175952.966c34a+ds3-1) unstable; urgency=medium

  * New upstream release.
  * Bug fix: "FTBFS: make[1]: *** [debian/rules:42:
    override_dh_auto_configure] Error 2", thanks to Lucas Nussbaum
    (Closes: #1091129).

 -- Roland Mas <lolando@debian.org>  Fri, 03 Jan 2025 19:15:51 +0100

bornagain (22~git20241125114253.51de53a+ds3-1) unstable; urgency=medium

  * New upstream release.

 -- Roland Mas <lolando@debian.org>  Tue, 26 Nov 2024 19:27:59 +0100

bornagain (22~git20241119164839.81ff4bd+ds3-1) unstable; urgency=medium

  * Bug fix: "Missing Build-Depends on python3-setuptools", thanks to
    stefanor@debian.org</a>; (Closes: #1080565).
  * New upstream release.

 -- Roland Mas <lolando@debian.org>  Wed, 20 Nov 2024 17:43:49 +0100

bornagain (22~git20240726093306.cb41cc4+ds3-2) unstable; urgency=medium

  * Fix autopkgtest so that it actually fails on error.
  * Bug fix: "error while loading shared libraries: _libBornAgainGUI.so:
    cannot open shared object file: No such file or directory", thanks to
    Picca Frédéric-Emmanuel (Closes: #1078439).
  * This also fixes: "please add autopkgtests (to add coverage for
    python3-matplotlib)", thanks to Sandro Tosi (Closes: #1036492).

 -- Roland Mas <lolando@debian.org>  Tue, 13 Aug 2024 23:03:24 +0200

bornagain (22~git20240726093306.cb41cc4+ds3-1) unstable; urgency=medium

  * New upstream release. This fixes "ftbfs with GCC-14", thanks to
    Matthias Klose (Closes: #1074853).

 -- Roland Mas <lolando@debian.org>  Fri, 26 Jul 2024 16:23:15 +0200

bornagain (22~git20240516130453.4d28e9f+ds3-1) experimental; urgency=medium

  * New upstream snapshot.

 -- Roland Mas <lolando@debian.org>  Wed, 22 May 2024 16:50:55 +0200

bornagain (22~git20240514151320.6d9f79d+ds3-3) experimental; urgency=medium

  * Fix --build=all,source builds.

 -- Roland Mas <lolando@debian.org>  Wed, 22 May 2024 16:48:41 +0200

rnagain (22~git20240514151320.6d9f79d+ds3-2) experimental; urgency=medium

  * Fix another instance of an arch-specific path in debian/rules.

 -- Roland Mas <lolando@debian.org>  Wed, 15 May 2024 10:55:50 +0200

bornagain (22~git20240514151320.6d9f79d+ds3-1) experimental; urgency=medium

  * New upstream snapshot.

 -- Roland Mas <lolando@debian.org>  Wed, 15 May 2024 01:08:16 +0200

bornagain (22~git20240514090930.55112b1+ds3-1) experimental; urgency=medium

  * New upstream snapshot.

 -- Roland Mas <lolando@debian.org>  Tue, 14 May 2024 11:17:05 +0200

bornagain (22~git20240513132534.e278a05+ds3-2) experimental; urgency=medium

  * Fix arch-specific path in debian/rules to help with the i386/i686
    dichotomy.

 -- Roland Mas <lolando@debian.org>  Mon, 13 May 2024 23:33:55 +0200

bornagain (22~git20240513132534.e278a05+ds3-1) experimental; urgency=medium

  * New upstream snapshot.

 -- Roland Mas <lolando@debian.org>  Mon, 13 May 2024 15:53:20 +0200

bornagain (22~git20240512164208.1811a04+ds3-3) experimental; urgency=medium

  * New upstream snapshot.
  * Enable -DBA_3ARCH=ON as suggested by upstream.

 -- Roland Mas <lolando@debian.org>  Sun, 12 May 2024 20:02:42 +0200

bornagain (22~20240509+ds3-1) experimental; urgency=medium

  * Snapshot of upstream Git's main branch.

 -- Roland Mas <lolando@debian.org>  Thu, 09 May 2024 20:24:15 +0200

bornagain (21.2~20240509+ds3-1) experimental; urgency=medium

  * Snapshot of upstream Git's r21 branch

 -- Roland Mas <lolando@debian.org>  Thu, 09 May 2024 18:14:39 +0200

bornagain (21.1+ds3-5) unstable; urgency=medium

  * Tweak size detection of numeric types; this should fix some of the
    arch-specific FTBFSes.

 -- Roland Mas <lolando@debian.org>  Tue, 30 Apr 2024 16:41:19 +0200

bornagain (21.1+ds3-4) unstable; urgency=medium

  * Bug fix: "Please drop dependencies on python3-distutils", thanks to
    Graham Inggs (Closes: #1065835).

 -- Roland Mas <lolando@debian.org>  Thu, 21 Mar 2024 22:38:28 +0100

bornagain (21.1+ds3-3) unstable; urgency=medium

  * Link against Swig 4.2; this fixes: "FTBFS: make[1]: ***
    [debian/rules:41: override_dh_auto_configure] Error 2", thanks to
    Lucas Nussbaum (Closes: #1064750).
  * For some reason, this also fixes: "ftbfs with LTO (link time
    optimization) enabled", thanks to Matthias Klose (Closes: #1015364).

 -- Roland Mas <lolando@debian.org>  Wed, 28 Feb 2024 18:29:19 +0100

bornagain (21.1+ds3-2) unstable; urgency=medium

  * d/rules: fix FTBFS on arch all
  * d/rules: use the current cmake options
  * split the arch/indep install target

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 14 Dec 2023 16:58:21 +0100

bornagain (21.1+ds3-1) unstable; urgency=medium

  * d/control: Added B-D libcerf-dev (>= 2.3)
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 13 Dec 2023 09:23:08 +0100

bornagain (21.0+ds3-2) unstable; urgency=medium

  * Bug fix: "bornagain executable can't find _libBornAgainGUI.so",
    thanks to Heine Larsen (Closes: #1043337).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 29 Aug 2023 17:55:37 +0200

bornagain (21.0+ds3-1) unstable; urgency=medium

  * New upstream version 21.0+ds3
  * refresh the patch series
  * d/rules: move all the install target into the rules file
  * Bug fix: "please add autopkgtests (to add coverage for
    python3-numpy)", thanks to Sandro Tosi (Closes: #1035141).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 29 Aug 2023 16:27:16 +0200

bornagain (20.2+ds3-2) unstable; urgency=medium

  * Patch to increase error margins for testsuite.
  * Fix Hugo build.

 -- Roland Mas <lolando@debian.org>  Tue, 08 Aug 2023 16:39:53 +0200

bornagain (20.2+ds3-1) unstable; urgency=medium

  * New upstream release.
  * Add myself as uploader.
  * Use newly-packaged libheinz and libformfactor.
  * Use Qt6.
  * Update install paths for *.pc, *.so, *.cmake files.
  * Build HTML docs with Hugo (several patches needed).
  * Update debian/copyright (thanks, Lintian!).
  * Re-enable testsuite (hence the build-dependency on clang-format).
  * Re-enable dh_dwz.

 -- Roland Mas <lolando@debian.org>  Sun, 30 Jul 2023 12:43:39 +0200

bornagain (1.19.0-4) unstable; urgency=medium

  [ Neil Williams ]
  * Update control to add Debian PaN maintainers

  [ Sébastien Delafond ]
  * d/control: use Freexian Packaging Team instead of seb@debian.org in Uploaders

  [ Picca Frédéric-Emmanuel ]
  * d/watch: upstream migrated to gitlab

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 24 Jul 2023 14:57:07 +0200

bornagain (1.19.0-3) unstable; urgency=medium

  * Bug fix: "FTBFS: Precomputed.h:29:11: error: ‘size_t’ has not been
    declared", thanks to Lucas Nussbaum (Closes: #997069).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 21 Nov 2021 13:44:46 +0100

bornagain (1.19.0-2) unstable; urgency=medium

  * Bug fix: "buggy cmake file", thanks to Yangfl (Closes: #987700).
  * Bug fix: "contains ccache prebuilt objects", thanks to Yangfl (Closes:
    #987677).
  * Bug fix: "ftbfs with GCC-11", thanks to Matthias Klose (Closes:
    #983998).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 27 Sep 2021 21:01:20 +0200

bornagain (1.19.0-1) unstable; urgency=medium

  [Neil Williams]
  * Add Frédéric Picca as Uploader

  [Picca Frédéric-Emmanuel]
  * d/watch: fixed the url.
  * New upstream version 1.19.0
  * d/pathes: refreshed
  * Use secure URI in Homepage field.
  * Update standards version to 4.6.0, no changes needed.
  * Apply multi-arch hints.
    + bornagain-doc: Add Multi-Arch: foreign.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 27 Sep 2021 20:09:13 +0200

bornagain (1.18.0-1) unstable; urgency=medium

  [ Picca Frédéric-Emmanuel ]
  * d/rules: Use --no-parallel to avoid FTBFS.

  [ Sébastien Delafond ]
  * Change install method
  * New upstream version 1.18.0 (Closes: #972762)

 -- Sebastien Delafond <seb@debian.org>  Tue, 08 Dec 2020 09:56:37 +0100

bornagain (1.16.0-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 1.16.0

 -- Sebastien Delafond <seb@debian.org>  Fri, 17 Apr 2020 16:45:26 +0200

bornagain (1.15.0-1) unstable; urgency=medium

  [ Mika Pflüger ]
  * Initial release (Closes: #844618)

  [ Sébastien Delafond ]
  * d/{control,copyright}

 -- Sebastien Delafond <seb@debian.org>  Fri, 17 Apr 2020 15:18:18 +0200
