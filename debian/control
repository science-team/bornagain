Source: bornagain
Maintainer: Debian PaN Maintainers <debian-pan-maintainers@alioth-lists.debian.net>
Uploaders:
 Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>,
 Mika Pflüger <debian@mikapflueger.de>,
 Picca Frédéric-Emmanuel <picca@debian.org>,
 Roland Mas <lolando@debian.org>,
Section: science
Priority: optional
Build-Depends:
 clang-format <!nocheck>,
 cmake,
 debhelper-compat (= 13),
 dh-sequence-python3,
 doxygen-latex <!nodoc>,
 graphviz <!nodoc>,
 libboost-all-dev,
 libbz2-dev,
 libcerf-dev (>= 2.3),
 libeigen3-dev,
 libfftw3-dev,
 libformfactor-dev,
 libgsl0-dev,
 libheinz-dev,
 libqt6opengl6-dev,
 libtiff-dev,
 libxkbcommon-dev,
 libyaml-cpp-dev,
 python3-corner <!nopython>,
 python3-dev <!nopython>,
 python3-emcee <!nopython>,
 python3-fabio <!nopython>,
 python3-lmfit <!nopython>,
 python3-matplotlib <!nopython>,
 python3-numpy <!nopython>,
 python3-pip <!nopython>,
 python3-scipy <!nopython>,
 python3-setuptools <!nopython>,
 python3-yaml <!nopython>,
 qt6-base-dev,
 qt6-svg-dev,
 swig <!nopython>,
Build-Depends-Indep:
 hugo <!nodoc>,
 texlive-fonts-extra <!nodoc>,
 texlive-xetex <!nodoc>,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/bornagain
Vcs-Git: https://salsa.debian.org/science-team/bornagain.git
Homepage: https://bornagainproject.org/
Rules-Requires-Root: no

Package: bornagain
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 python3-bornagain,
Description: Simulate and fit X-ray and neutron GISAS -- binary
 BornAgain is a software package to simulate and fit small-angle scattering at
 grazing incidence. It supports analysis of both X-ray (GISAXS) and neutron
 (GISANS) data. Calculations are carried out in the framework of the distorted
 wave Born approximation (DWBA). BornAgain provides a graphical user interface
 for interactive use as well as a generic Python and C++ framework for modeling
 multilayer samples with smooth or rough interfaces and with various types of
 embedded nanoparticles.
 .
 BornAgain supports:
 .
 Layers:
  * Multilayers without any restrictions on the number of layers
  * Interface roughness correlation
  * Magnetic materials
 .
 Particles:
  * Choice between different shapes of particles (form factors)
  * Particles with inner structures
  * Assemblies of particles
  * Size distribution of the particles (polydispersity)
 .
 Positions of Particles:
  * Decoupled implementations between vertical and planar positions
  * Vertical distributions: particles at specific depth in layers or on top.
  * Planar distributions:
    - fully disordered systems
    - short-range order distribution (paracrystals)
    - two- and one-dimensional lattices
 .
 Input Beam:
  * Polarized or unpolarized neutrons
  * X-ray
  * Divergence of the input beam (wavelength, incident angles) following
    different distributions
  * Possible normalization of the input intensity
 .
 Detector:
  * Off specular scattering
  * Two-dimensional intensity matrix, function of the output angles
 .
 Use of BornAgain:
  * Simulation of GISAXS and GISANS from the generated sample
  * Fitting to reference data (experimental or numerical)
  * Interactions via Python scripts or Graphical User Interface
 .
 If you use BornAgain in your work, please cite
  C. Durniak, M. Ganeva, G. Pospelov, W. Van Herck, J. Wuttke (2015), BornAgain
  — Software for simulating and fitting X-ray and neutron small-angle
  scattering at grazing incidence, version <version you used>,
  http://www.bornagainproject.org

Package: python3-bornagain
Architecture: any
Depends:
 python3-matplotlib,
 python3-numpy,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Simulate and fit X-ray and neutron GISAS -- Python3
 BornAgain is a software package to simulate and fit small-angle scattering at
 grazing incidence. It supports analysis of both X-ray (GISAXS) and neutron
 (GISANS) data. Calculations are carried out in the framework of the distorted
 wave Born approximation (DWBA). BornAgain provides a graphical user interface
 for interactive use as well as a generic Python and C++ framework for modeling
 multilayer samples with smooth or rough interfaces and with various types of
 embedded nanoparticles.
 .
 BornAgain supports:
 .
 Layers:
  * Multilayers without any restrictions on the number of layers
  * Interface roughness correlation
  * Magnetic materials
 .
 Particles:
  * Choice between different shapes of particles (form factors)
  * Particles with inner structures
  * Assemblies of particles
  * Size distribution of the particles (polydispersity)
 .
 Positions of Particles:
  * Decoupled implementations between vertical and planar positions
  * Vertical distributions: particles at specific depth in layers or on top.
  * Planar distributions:
    - fully disordered systems
    - short-range order distribution (paracrystals)
    - two- and one-dimensional lattices
 .
 Input Beam:
  * Polarized or unpolarized neutrons
  * X-ray
  * Divergence of the input beam (wavelength, incident angles) following
    different distributions
  * Possible normalization of the input intensity
 .
 Detector:
  * Off specular scattering
  * Two-dimensional intensity matrix, function of the output angles
 .
 Use of BornAgain:
  * Simulation of GISAXS and GISANS from the generated sample
  * Fitting to reference data (experimental or numerical)
  * Interactions via Python scripts or Graphical User Interface
 .
 If you use BornAgain in your work, please cite
  C. Durniak, M. Ganeva, G. Pospelov, W. Van Herck, J. Wuttke (2015), BornAgain
  — Software for simulating and fitting X-ray and neutron small-angle
  scattering at grazing incidence, version <version you used>,
  http://www.bornagainproject.org
 .
  This package contains the Python bindings for use in scripts.

Package: bornagain-doc
Architecture: all
Section: doc
Depends:
 fonts-font-awesome,
 fonts-open-sans,
 fonts-roboto,
 libjs-bootstrap,
 libjs-jquery,
 libjs-popper.js,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 bornagain,
Multi-Arch: foreign
Description: Simulate and fit X-ray and neutron GISAS -- doc
 BornAgain is a software package to simulate and fit small-angle scattering at
 grazing incidence. It supports analysis of both X-ray (GISAXS) and neutron
 (GISANS) data. Calculations are carried out in the framework of the distorted
 wave Born approximation (DWBA). BornAgain provides a graphical user interface
 for interactive use as well as a generic Python and C++ framework for modeling
 multilayer samples with smooth or rough interfaces and with various types of
 embedded nanoparticles.
 .
 BornAgain supports:
 .
 Layers:
  * Multilayers without any restrictions on the number of layers
  * Interface roughness correlation
  * Magnetic materials
 .
 Particles:
  * Choice between different shapes of particles (form factors)
  * Particles with inner structures
  * Assemblies of particles
  * Size distribution of the particles (polydispersity)
 .
 Positions of Particles:
  * Decoupled implementations between vertical and planar positions
  * Vertical distributions: particles at specific depth in layers or on top.
  * Planar distributions:
    - fully disordered systems
    - short-range order distribution (paracrystals)
    - two- and one-dimensional lattices
 .
 Input Beam:
  * Polarized or unpolarized neutrons
  * X-ray
  * Divergence of the input beam (wavelength, incident angles) following
    different distributions
  * Possible normalization of the input intensity
 .
 Detector:
  * Off specular scattering
  * Two-dimensional intensity matrix, function of the output angles
 .
 Use of BornAgain:
  * Simulation of GISAXS and GISANS from the generated sample
  * Fitting to reference data (experimental or numerical)
  * Interactions via Python scripts or Graphical User Interface
 .
 If you use BornAgain in your work, please cite
  C. Durniak, M. Ganeva, G. Pospelov, W. Van Herck, J. Wuttke (2015), BornAgain
  — Software for simulating and fitting X-ray and neutron small-angle
  scattering at grazing incidence, version <version you used>,
  http://www.bornagainproject.org
 .
  This package contains the BornAgain documentation.
