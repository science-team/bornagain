#!/usr/bin/make -f
# output every command that modifies files on the build system.
export DH_VERBOSE = 1

# deal with profiles
CONFIG_SWITCHES=-DCMAKE_DEB_HOST_MULTIARCH=$(DEB_HOST_MULTIARCH)

ifneq ($(filter nodoc,$(DEB_BUILD_PROFILES)),)
	CONFIG_SWITCHES += -DCONFIGURE_DOXY=OFF
else
	CONFIG_SWITCHES += -DCONFIGURE_DOXY=ON
endif

ifneq ($(filter nopython,$(DEB_BUILD_PROFILES)),)
	CONFIG_SWITCHES += -DBORNAGAIN_PYTHON=OFF -DCONFIGURE_BINDINGS=OFF
else
	CONFIG_SWITCHES += -DBORNAGAIN_PYTHON=ON -DCONFIGURE_BINDINGS=ON
endif

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk
CFLAGS+=$(CPPFLAGS)
CXXFLAGS+=$(CPPFLAGS)

export CMAKE_OPTIONS := \
  -DCMAKE_CXX_FLAGS="$(CXXFLAGS)" \
  -DCMAKE_SHARED_LINKER_FLAGS_RELEASE="$(LDFLAGS)" \
  -DCMAKE_PREFIX_PATH="/usr/share/cmake/heinz;/usr/share/cmake/formfactor" \
  -DCMAKE_LIBRARY_PATH=/usr/lib/$(DEB_HOST_MULTIARCH)/bornagain \
  -DCMAKE_BUILD_TYPE=Release \
  -DBA_3ARCH=ON

%:
	dh $@

execute_after_dh_clean:
	rm -rf hugo/public hugo/.hugo_build.lock
	find auto -name *.pyc -delete

override_dh_auto_configure:
	dh_auto_configure -- $(CMAKE_OPTIONS) $(CONFIG_SWITCHES) -DCMAKE_LIBRARY_PATH=$(DEB_HOST_MULTIARCH)

override_dh_auto_test:
	cd obj-$(DEB_HOST_GNU_TYPE) && make -j1 test ARGS\+=--verbose ARGS\+=-j1

override_dh_auto_build-arch:
	dh_auto_build

override_dh_auto_build-indep:
	dh_auto_build
	cd hugo && hugo

execute_after_dh_auto_install:
	find debian/tmp -name __pycache__ | xargs -r rm -r
	rm -f debian/tmp/usr/share/Bornagain/bornagain_build.log

override_dh_install-arch:
	#  bornagain
	dh_install -p bornagain usr/bin/bornagain
	dh_install -p bornagain usr/lib/*/bornagain/_libBornAgainGUI.so
	dh_install -p bornagain usr/share/BornAgain/Images/* usr/share/bornagain/images/
	dh_install -p bornagain usr/share/man/

	# python3-bornagain
	dh_install -p python3-bornagain -X_libBornAgainGUI.so usr/lib/*/bornagain/*.so
	mkdir -p debian/tmp/usr/lib/python3/dist-packages/bornagain/lib
	cp obj-$(DEB_HOST_GNU_TYPE)/py/src/bornagain/lib/lib*.py debian/tmp/usr/lib/python3/dist-packages/bornagain/lib
	cp obj-$(DEB_HOST_GNU_TYPE)/py/src/bornagain/lib/__init__.py debian/tmp/usr/lib/python3/dist-packages/bornagain/lib
	cp obj-$(DEB_HOST_GNU_TYPE)/py/src/bornagain/*.py debian/tmp/usr/lib/python3/dist-packages/bornagain/
	dh_install -p python3-bornagain usr/lib/python3/*

override_dh_install-indep:
	# bornagain-doc
	dh_install -p bornagain-doc usr/share/BornAgain/Examples/* usr/share/doc/bornagain/examples/
	dh_install -p bornagain-doc hugo/public/* usr/share/doc/bornagain/html

execute_before_dh_missing-arch:
	# rm remaining files (workaround FTBFS...)
	rm -rf debian/tmp/usr/share

execute_before_dh_missing-indep:
	# rm remaining files (workaround FTBFS...)
	rm  -f debian/tmp/usr/bin/bornagain
	rm -rf debian/tmp/usr/lib
	rm -rf debian/tmp/usr/share/BornAgain/
	rm -f  debian/tmp/usr/share/man/man1/bornagain.1*
