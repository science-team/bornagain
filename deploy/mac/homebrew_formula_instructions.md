# Homebrew documentation:
* https://docs.brew.sh/Formula-Cookbook
* https://docs.brew.sh/Formula-Cookbook#variables-for-directory-locations
* https://docs.brew.sh/Python-for-Formula-Authors
* https://rubydoc.brew.sh/Formula

# Example formulas
* python@3.11 formula: <https://github.com/Homebrew/homebrew-core/blob/c0218d50084e300cba26da84028acfd4917ce623/Formula/p/python@3.11.rb>

* Qt formula: <https://github.com/Homebrew/homebrew-core/blob/c0218d50084e300cba26da84028acfd4917ce623/Formula/q/qt.rb>

* Heller, "Homebrew tutorial: How to use Homebrew for MacOS", <https://www.infoworld.com/article/3328824/homebrew-tutorial-how-to-use-homebrew-for-macos.html>

----------

# create a local clone of the Homebrew repository in deliberate directory, as explained in the Homebrew documentation,
# for instance, in directory `/Users/myuser/tmp/homebrew`:
```
HOMEBREW_PREFIX="/Users/myuser/tmp/homebrew";
mkdir -p "HOMEBREW_PREFIX"
cd "$HOMEBREW_PREFIX"

brew tap --force homebrew/core
```

# to use the local Homebrew installation the environmental variables must be set properly:

for instance, if the local Homebrew repository is in directory `/Users/myuser/tmp/homebrew`:

```
export HOMEBREW_PREFIX="/Users/myuser/tmp/homebrew";
export HOMEBREW_CELLAR="$HOMEBREW_PREFIX/Cellar";
export HOMEBREW_REPOSITORY="$HOMEBREW_PREFIX";
export PATH="$HOMEBREW_PREFIX/bin:$HOMEBREW_PREFIX/sbin${PATH+:$PATH}";
export MANPATH="$HOMEBREW_PREFIX/share/man${MANPATH+:$MANPATH}:";
export INFOPATH="$HOMEBREW_PREFIX/share/info:${INFOPATH:-}";
export HOMEBREW_EDITOR=emacs
export QT_PLUGIN_PATH="$HOMEBREW_PREFIX/Cellar/qt/6.7.0_1/share/qt/plugins/"
export hbrew=$HOMEBREW_PREFIX/bin/brew
```

----------

# create recipe template
```
HOMEBREW_NO_AUTO_UPDATE=1 HOMEBREW_NO_INSTALL_FROM_API=1 $hbrew create --cmake --set-version 22 --set-name bornagain https://jugit.fz-juelich.de/mlz/bornagain/
```

# install the formula
```
HOMEBREW_NO_AUTO_UPDATE=1 HOMEBREW_NO_INSTALL_FROM_API=1 $hbrew install --keep-tmp bornagain
```

Add verbose option `-v` or `--verbose`, if needed.

# interactive installation of the formula (might be useful for debugging)
```
HOMEBREW_NO_AUTO_UPDATE=1 HOMEBREW_NO_INSTALL_FROM_API=1 $hbrew install --interactive --keep-tmp bornagain
```

# audit the formula before submitting
```
HOMEBREW_NO_AUTO_UPDATE=1 HOMEBREW_NO_INSTALL_FROM_API=1 $hbrew audit --strict --new bornagain
```

----------
