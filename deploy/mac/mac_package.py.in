#  **************************************************************************  #
#   BornAgain: simulate and fit reflection and scattering
#
#   @file      mac_package.py.in
#   @brief     Adjusts the library references in the MacOS bundle to make
#              them relocatable.
#
#   @homepage  http://apps.jcns.fz-juelich.de/BornAgain
#   @license   GNU General Public License v3 or higher (see COPYING)
#   @copyright Forschungszentrum Juelich GmbH 2016
#   @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
#  **************************************************************************  #

# Adjust MacOS Bundle:
# This script is called by CPack to adjust the bundle contents before making
# the final DMG file. The package root directory is provided as an argument when
# CPack runs the script (see `cmake/configurables/FixPack.cmake.in`).
# The script follows the rules explained in `man dyld (1)`.
# Furthermore, it uses MacOS native tools `otool` and `install_name_tool` to adjust the binaries.
# It finds the dependencies of the core libraries up to a maximum level.
# The dependencies are then copied to the corresponding package directories,
# and library references and RPATHs are modified accordingly.

# Adjust Python wheel:
# In the 'Python mode', the script produces a Python wheel
# (see `cmake/multipython/MakePythonWheel.cmake`).

# Requires MacOS-specific native tools `otool` and `install_name_tool`.
# NOTE: Starting with macOS Catalina (macOS 10), Macs will use `zsh` as
# the default login shell and interactive shell across the operating system.
# All newly created user accounts in macOS Catalina will use zsh by default;
# see <https://support.apple.com/en-us/HT208050>

# See further:
# - Runtime linking on Mac <https://matthew-brett.github.io/docosx/mac_runtime_link.html>
# - Apple developer documentation: 'Run-Path Dependent Libraries' <https://apple.co/3HVbMWm>
# - Loading Dynamic Libraries on Mac <http://clarkkromenaker.com/post/library-dynamic-loading-mac>
# - <https://stackoverflow.com/q/66268814>
# - dlopen manpage
# - 'Qt for macOS - Deployment' <https://doc.qt.io/qt-6/macos-deployment.html>
# - 'Using qt.conf' <https://doc.qt.io/qt-6/qt-conf.html>

# Package structure:
# <Package-root>
#  |
#  +--MacOS  {includes main executable: `bornagain`}
#  |
#  +--lib  {main libraries, like `_libBornAgainBase.so`}
#  |
#  +--bin  {not used}
#  |
#  +--Library  {extra libraries, like `libformfactor`}
#  |
#  +--Frameworks  {Qt framework}
#  |
#  +--PlugIns  {Qt plugins)
#  |
#  +--Resources  {icons and `qt.conf`}
#  |
#  +--share
#     |
#     +--BornAgain-<version>
#        |
#        +--Examples
#        |
#        +--Images
#------------------------------------------------------------------------------80

import sys, os, shutil, glob, platform, re, subprocess as subp
from collections import namedtuple

PKGROOT = '<PKG-ROOT>'

def mkdirs(path:str):
    if not os.path.exists(path):
        os.makedirs(path)


def removeprefix(str_:str, pfx:str):
    return str_.split(pfx, 1)[-1]


def splat(str_list:str, separator = ' '):
    return [x for x in str_list.split(separator) if x]


def copyfile(src:str, dst:str, overwrite=False):
    if not overwrite and os.path.exists(dst): return
    shutil.copy(src, dst)


def dylib_id(libpath:str):
    """ Returns the base library id for the given library """

    return subp.check_output(["otool", "-XD", libpath], encoding='utf-8').strip()


def rm_list_duplicates(list0:list):
    """ Removes duplicates from a given list """

    return list(set(list0))


def dylib_deps(libpath:str):
    """ Uses 'otool' to get the 1st-order dependencies of a library (raw output).
    Obtains the dependencies for a given library;
    removes the name of the library itself from the references.
    NOTE: Under MacOS, a Mach-O binary sometimes depends on itself.

    * otool output example: '  /usr/local/opt/foo.dylib (compatibility ...)'
    """

    deps = subp.check_output(["otool", "-XL", libpath], encoding='utf-8').split('\n')
    return [d.strip() for d in deps if d]


def find_dylibs(abspath:str):
    """ Returns the filenames corresponding to the pattern *.so or *.dylib """

    return glob.glob(abspath + '/*.so') + glob.glob(abspath + '/*.dylib')


def get_depends1(libpath:str) -> list:
    """ Obtains 1st-order dependencies for a given file.
    Obtains the 'non-system' dependencies for a given library;
    eg. '  /usr/local/opt/foo.1.dylib (compatibility ...)' => '/usr/local/opt/foo.dylib'
    * system dependencies pattern: /System/, /Library/, /usr/lib/, /usr/local/lib
    [See man dyld(1)]
    """

    # discard system dependencies
    deps_ = [d for d in dylib_deps(libpath)
             if not re.match(r'/(usr/lib|usr/local/lib|System|Library)/', d.strip())]
    deps = list()
    for d in deps_:
        # extract library path
        m_ = re.search(r'\s*([\w@/.-]+)\s+.*', d)
        if m_: deps.append(m_.group(1))

    return deps


def common_root(abspath1:str, abspath2:str):
    """ Finds the common root of two given _absolute_ paths """

    if abspath1 == abspath2:
        # if paths are equal, the root is equal to either of them
        return abspath1

    # convert paths to arrays of directories:
    # replace '/' with blank and make an array out of the result;
    # eg. '/root/lib/opt' => [root, lib, opt]
    dirs1 = [d for d in abspath1.split('/') if d]
    idxMax1 = len(dirs1)
    dirs2 = [d for d in abspath2.split('/') if d]
    idxMax2 = len(dirs2)
    common_root = ""
    for idx in range(min(idxMax1, idxMax2)):
        # extract the head (topmost) directory name from paths
        # eg. 'root/lib/opt' => 'root'
        head1 = dirs1[idx]
        head2 = dirs2[idx]

        # if both paths have the same head, then add head to the common root;
        # otherwise, the common root is already obtained
        if head1 != head2: break
        # add the common head to the root
        common_root += '/' + head1

    return common_root


def find_rpath(bin_abspath:str, lib_abspath:str, lib_relpath:str=""):
    """ Finds the proper rpath for given binary pointing to a reference library
    # usage: find_rpath(bin_abspath, lib_abspath, lib_relpath)
    # example:
    # bin_abspath='/root/usr/opt/bin'
    # lib_abspath='/root/usr/Frameworks/Qux/lib'
    # lib_relpath='Qux/lib'
    # returns: `../../Frameworks`
    """

    # drop the final '/' chars from all paths
    bin_abspath = os.path.dirname(bin_abspath)  # target binary for which a rpath is obtained
    lib_abspath = os.path.dirname(lib_abspath)  # referenced library
    lib_relpath = os.path.dirname(lib_relpath)  # relative path to the referenced library
    # find the common root path
    root_path = common_root(bin_abspath, lib_abspath)
    if not root_path: return lib_abspath

    # obtain the path from the binary to the root
    # eg. '/root/local/opt' => 'local/opt' => '../..'
    _binpth_to_root = re.sub(root_path + '(/|$)', '', bin_abspath)
    binpth_to_root = re.sub('[^/]+', '..', _binpth_to_root)

    # obtain the path from root to the referenced library;
    # eg. '/root/local/opt' => 'local/opt'
    # then, drop the relative path of the library from the end
    _libpth_from_root = re.sub(root_path + '(/|$)', '', lib_abspath)
    libpth_from_root = re.sub('(^|/)' + lib_relpath + '$', '', _libpth_from_root)

    # return the proper relative RPATH to the referenced library
    # eg. '../../Frameworks/Qt'
    libpth_from_bin = libpth_from_root if not binpth_to_root \
        else binpth_to_root + '/' + libpth_from_root
    # return eg. '@loader_path/../../Frameworks'
    return "@loader_path/" + libpth_from_bin


class PythonFramework:
    """ Python framework details """

    # regexp to extract the Python dependence;
    # eg., '/Frameworks/Python.framework/Versions/3.9/Python'
    libdir_re = r'[\w./@.-]+'
    pylibname_re = r'Python|libpython.+\.dylib'
    py_fmwk_re = re.compile('(' + libdir_re + ')/(' + pylibname_re + ')')

    # regexp to extract the Python framework path
    # eg. '/opt/python@3.9/Frameworks/Python.framework/Versions/3.9/Python'
    #   => '/opt/python@3.9/Frameworks/Python.framework/Versions/3.9/'
    py_fmwk_path_re = r'(.*)/(Python|libpython).*'
    # possible framework paths on MacOS
    framework_paths = ("/usr/local/Library/Frameworks",
                       "/Library/Frameworks", "/usr/local/Frameworks",
                       "/opt/homebrew/Frameworks")

    @staticmethod
    def dependence(dependencies:list) -> (str, str):
        """ Extracts the Python dependency from the given dependencies
        # NOTE: Under OSX, Python's library name is either 'Python', or
        # for Python3.9, 'libpython3.9.dylib'.
        """

        # regexp to correct the Python dependence; eg.:
        # '/Users/usr1/.pyenv/versions/3.9.18/lib/libpython3.9.dylib' => 'libpython3.9.dylib'
        # '/Frameworks/Python.framework/Versions/3.9/Python' => 'libpython3.9.dylib'
        # '/Frameworks/Python.framework/Versions/3.9/libpython3.9.dylib' => 'libpython3.9.dylib'
        # obtain the dependencies
        pydepends_path_list = [d for d in dependencies if PythonFramework.py_fmwk_re.match(d)]
        pydepends_path = pydepends_filename = None
        if pydepends_path_list:
            pydepends_filename = pydepends_path_list[0]
            mtch = PythonFramework.py_fmwk_re.match(pydepends_filename)
            pydepends_path = mtch[1]
            pydepends_filename = mtch[2]
        # return the Python dependence fullpath and filename
        return pydepends_path, pydepends_filename

    @staticmethod
    def framework_path(pydepends_path:str) -> (str, str):
        """ Produces proper Python framework paths for a given Python dependency """

        # extract the Python framework path
        py_fmwk_dir = re.match(PythonFramework.py_fmwk_path_re, pydepends_path, re.IGNORECASE)[1]

        # when the library is like '.../Versions/3.9/Python', then
        # add an extra '/lib' to the framework path;
        # this is needed since it refers to the standard location of the
        # Python shared library on OSX, '.../Versions/3.9/lib/libpython39.dylib'.
        if pydepends_path.endswith('Python'):
            py_fmwk_dir += '/lib'

        # regexp to extract the Python version; eg. '3.9'
        pyversion = None
        pyversion_m = re.match(r'.+versions/([0-9.]+).*', pydepends_path, re.IGNORECASE)
        if pyversion_m:
            pyversion = pyversion_m[1]
        else:
            pyversion_m = re.match(r'.+/libpython(.+)\.dylib', pydepends_path)
            if pyversion_m:
                pyversion = pyversion_m[1]
        # '3.9.18' => '3.9'
        pyversion = re.match(r'(\d+.\d+).*', pyversion)[1]
        if not pyversion:
            raise ValueError("Cannot extract Python version from path '%s'"
                             % pydepends_path)
        # RPATHs corresponding to the common OSX framework paths
        py_fmwk_basepath = "Python.framework/Versions/" + pyversion + "/lib"
        # collect proper RPATHs for Python framework
        py_fmwk_rpaths = [py_fmwk_dir]
        for pth in PythonFramework.framework_paths:
            py_fmwk_rpaths.append(pth + '/' + py_fmwk_basepath)
        # return a list of possible framework paths
        return py_fmwk_rpaths, pyversion

    @staticmethod
    def make_wheel(python_exe, py_output_dir, wheel_dir, pure_wheelname, wheelname):
        # make the Python wheel
        subp.check_output([python_exe, '-m', 'pip', '-v', 'wheel', py_output_dir,
                           '--no-deps', '--wheel', wheel_dir])
        # rename the pure-Python wheel to a platform-dependent wheel
        os.rename(pure_wheelname, wheelname)

class dylibRPATHs:
    """ Obtains the RPATHs of a library """

    # extract the LC_RPATH sections from the output of `otool -l` command
    LC_RPATH_rx = re.compile(r'.+RPATH.*\n.+\n.+\n')
    # extract 'rpath' from a LC_RPATH section
    rpath_rx = re.compile(r'path\s+([\w@/.-]+)\s+.*')
    # aux. dictionary to find the source file from a library reference
    libId_src_tbl = dict()

    @staticmethod
    def find(libpath:str) -> tuple:
        """ Returns the RPATHs for the given library """
        otool_output = subp.check_output(["otool", "-l", libpath], encoding='utf-8')
        # 'raw' rpaths which might include '@loader_path'
        _rpaths = dylibRPATHs.rpath_rx.findall(
            '\n'.join(dylibRPATHs.LC_RPATH_rx.findall(otool_output)))
        return tuple(r.rstrip('/') for r in _rpaths)

    @staticmethod
    def resolve(rpathref:str, rpaths:list):
        """ resolve a given rpath-reference to a list of possible paths
        according to a given list of RPATHs """
        return tuple(rpathref.replace('@rpath', p) for p in rpaths) if rpaths else tuple()

    @staticmethod
    def resolve_by_table(ref:str):
        """ resolve a given rpath-reference to a table of reference => path """
        return dylibRPATHs.libId_src_tbl.get(ref, '')


class LibraryFile:
    """ Library binary file """

    def __init__(self, dtype:str='', src:str='', ref:str='', rpaths:list=None,
                 dst:str='', ID:str='', dependencies:set=None, parent=None):
        self.dtype = dtype  # dependency type
        self.src = src   # path to the library file
        self.ref = ref    # reference to the library
        self.rpaths = rpaths if rpaths else set()  # rpaths of the library
        self.dst = dst    # destination of the library in the package
        self.ID = ID  # library ID
        self.ID_new = '' # new library ID
        # list of dependencies of the library
        self.dependencies = dependencies if dependencies else set()
        self.parent = parent

    def follow(self):
        """ Determines if the library dependencies should be followed """

        # do not follow Python dependencies
        ref_basename = os.path.basename(self.ref).lower()
        if 'python' in ref_basename:
            return False
        # follow only libraries for which there is an absolute source path
        return self.src.startswith('/')

    @staticmethod
    def find_source(lib, parent_lib):
        """ Finds the absolute path to the source, as far as possible """

        if lib.ref.startswith('@rpath') and parent_lib:
            if parent_lib.rpaths:
                parent_lib_dir = os.path.dirname(parent_lib.src)
                rpaths_fixed = tuple(os.path.realpath(r.replace('@loader_path', parent_lib_dir))
                                     for r in parent_lib.rpaths)
                # TODO: store fixed RPATHs
                lib.src = next(
                    filter(os.path.isfile, dylibRPATHs.resolve(lib.ref, rpaths_fixed)), '')
        elif lib.ref.startswith('@loader_path'):
            # relative reference; eg. '@rpath/foo.dylib'
            lib.dtype = 'rel@loader_path'
            lib.src = (os.path.dirname(parent_lib.src) + '/'
                       + removeprefix(lib.ref, '@loader_path'))
        elif lib.ref.startswith('/'):
            # absolute path
            lib.src = lib.ref

        # last attempt
        if not lib.src: lib.src = dylibRPATHs.resolve_by_table(lib.ref)
        # find the realpath of the source
        if lib.src: lib.src = os.path.realpath(lib.src)
        return lib.src

    @staticmethod
    def dependency_type(lib):
        """ Finds the dependency type """

        if lib.dtype: return lib.dtype
        dep = lib.ref
        dep_basename_l = os.path.basename(dep).lower()
        if 'python' in dep_basename_l:
            lib.dtype = 'framework_py'
        elif '.framework/' in dep.lower() and os.path.basename(dep).startswith('Qt'):
            lib.dtype = 'framework_qt'
        elif dep.startswith('@loader_path'):
            # relative reference; eg. '@rpath/foo.dylib'
            lib.dtype = 'rel@loader_path'
        elif dep.startswith('@rpath'):
            # relative reference; eg. '@rpath/foo.dylib'
            lib.dtype = 'rel@rpath'
        elif '.framework/' in dep.lower():
            # Frameworks must be considered separately
            # eg. '/opt/qt@6/lib/QtGui.framework/Versions/6/QtGui'
            lib.dtype = 'framework'
        elif dep.startswith('/'):
            # absolute reference; eg. '/usr/opt/libA.so'
            lib.dtype = 'abs'
        else:
            lib.dtype = 'unknown'

        return lib.dtype

    def __str__(self):
        return ("LibraryFile: dtype=%s, src=%s, ref=%s,"
                "rpaths=%s, dst=%s, ID=%s, dependencies=%s, parent-ref=%s"
                % (self.dtype, self.src, self.ref, self.rpaths, self.dst, self.ID,
                   self.dependencies, self.parent.ref if self.parent else None))

    def __repr__(self):
        return self.__str__()


class Settings:
    """ MacOS package configuration """

    # TODO: make intermediate variables internal

    # Python-related variables
    PyInfo = namedtuple("PythonInfo", ["dir", "lib", "version", "rpaths"])

    def __init__(self, pkg_root,
                 main_exe_src, core_libs, extra_libs,
                 python_mode, pkg_py_root_dir, pkg_py_lib_dir, pkg_py_extra_lib_dir,
                 qt_framework_root, qt_framework_plugins_dir, qt_plugins_dir, qt_plugins):

        # map {reference/abs. path => LibraryFile}
        self.libs = dict()
        # package-related directories
        fwdir = "Frameworks"
        self.dirs = {
            #-- absolute paths
            "root": pkg_root, # package root dir
            "qt_root": qt_framework_root,
            "qt_root_plugins_dir": qt_framework_plugins_dir,
            #-- relative paths
            "exe": "MacOS", # main executable dir
            "corelib": "lib",  # core library dir
            "exlib": "Library",  # external libraries dir
            "FW": fwdir,  # frameworks dir
            "FW_qt": fwdir + '/Qt',  # Qt framework dir
            "FW_qt_plugin": qt_plugins_dir,  # Qt plugins dir
        }

        # Python-package mode
        self.python_mode = bool(python_mode)
        if self.python_mode:
            self.dirs['root'] = pkg_py_root_dir
            self.dirs['corelib'] = removeprefix(pkg_py_lib_dir, pkg_py_root_dir + '/')
            core_libs.extend(find_dylibs(pkg_py_lib_dir))
            self.dirs['exlib'] = removeprefix(pkg_py_extra_lib_dir, pkg_py_root_dir + '/')
            extra_libs.extend(find_dylibs(pkg_py_extra_lib_dir))
        else:
            # main executable
            _main_exe_dst = self.dirs['root'] + '/' + self.dirs['exe'] \
                + '/' + os.path.basename(main_exe_src)
            self.libs = {main_exe_src:
                         LibraryFile(dtype='main_exe', src=main_exe_src, dst=_main_exe_dst)}
            #-- Qt plugins
            self.libs.update({self.dirs['qt_root_plugins_dir'] + '/' + l:
                              LibraryFile(dtype='plugin_qt', src=l) for l in qt_plugins})

        # core libraries
        self.libs.update({l: LibraryFile(dtype='core_lib', src=l) for l in set(core_libs)})
        # extra libraries
        self.libs.update({l: LibraryFile(dtype='extra_lib', src=l) for l in set(extra_libs)})
        # all initial libraries
        self.libs_init = tuple(self.libs.keys())
        # map {source-path => destination-path}
        self.src_dst_tbl = dict()
        # map {source-path => destination-path}
        self.copy_tbl = dict()
        # map {destination-path => LibraryFile}
        self.dst_tbl = dict()
        # map {reference => destination-path}
        self.ref_dst_tbl = dict()
        # map {reference_initial => reference_new}
        self.ref_tbl = dict()
        # Python-related info
        self.py:Settings.PyInfo = None

    @staticmethod
    def _dependencies_at_level(libs_cur:dict, pkg_libs:dict) -> dict:
        """ Obtains all dependencies at the current level """

        deps_lv:dict[(str, LibraryFile)] = dict()  # map {ref => parent-LibraryFile}
        for lib_ref, parent_lib in libs_cur.items():
            # if not at the root level (i.e. library has parent) and
            # library has been already discovered, then
            # neglect the library to avoid infinite loops
            if parent_lib and lib_ref in pkg_libs: continue

            lib = pkg_libs.setdefault(lib_ref, LibraryFile())
            lib.ref = lib_ref
            lib.parent = parent_lib
            # determine the type of the library and its source path
            if not lib.src: LibraryFile.find_source(lib, parent_lib)
            LibraryFile.dependency_type(lib)
            # consider only absolute paths for the next level
            if not lib.follow(): continue
            # verify the existence of the file
            if not os.path.isfile(lib.src):
                raise FileNotFoundError(f"Dependency does not exist: '{lib.src}'")
            # find the 1st-level dependencies of the library
            lib.dependencies = get_depends1(lib.src)
            # find RPATH references of the library
            lib.rpaths = set(dylibRPATHs.find(lib.src))
            # find library ID; eg., '@rpath/local/libA.dylib'
            lib.ID = dylib_id(lib.src)
            # add the library ID to the ID-table
            dylibRPATHs.libId_src_tbl[lib.ID] = lib.src
            # add the library and its parent for the next iteration
            deps_lv.update({d: lib for d in lib.dependencies})

        return deps_lv

    @staticmethod
    def dependencies(pkg_libs:dict, pkg_dirs:dict, libs_init:list, level_max:int):
        """ Gathers all dependencies for the given initial libraries
        up to a maximum (but arbitrary) level.
        """

        print("* Find dependencies (up to level %s)..." % (level_max))
        # root level libraries/binaries
        for ref, pkglib in pkg_libs.items():
            pkglib.parent = None
            pkglib.src = ref

        # map {library-id => library source path}
        dylibRPATHs.libId_src_tbl = {dylib_id(l): l for l in libs_init}
        # find dependencies of libraries recursively,
        # beginning from the initial libraries
        level = 0 # current level nr.
        libs_lv = {l: None for l in pkg_libs.keys()}  # map {library ref => parent}
        while libs_lv:
            level += 1
            # avoid going infinitely deep (eg. due to some mistake)
            if level > level_max:
                print("* Dependency level %i exceeds the maximum allowed depth (%i)."
                      % (level, level_max))
                break
            # obtain all dependencies at the current level
            print("  + [L%i] dependencies:" % level)
            for ref, parent_lib in libs_lv.items():
                print("    - [L%i] %s <= %s"
                      % (level, ref.replace(pkg_dirs['root'], PKGROOT),
                         parent_lib.src.replace(pkg_dirs['root'], PKGROOT)
                         if parent_lib and parent_lib.src else 'None'))
            # parents for the next iteration
            libs_lv = Settings._dependencies_at_level(libs_lv, pkg_libs)

            # TODO:
            # if no source file is found for a ref., make another attempt
            # to find the source via the reference table
            # for lib in (l for l in libs_lv.values() if l and not l.src):
            #     lib.src = dylibRPATHs.resolve_by_table(lib.ref)
        print("==>END.")
        return pkg_libs

    @staticmethod
    def destinations(pkg_libs, pkg_dirs, libs_init) -> (dict, dict):
        """ Determines destinations for the binaries in the package """

        # regex to obtain Qt framework subdirectory
        # eg. "/usr/local/Cellar/qt/6.5.1_3/lib/QtWidgets.framework/Versions/A/QtWidgets"
        #     => ['QtWidgets.framework', '/Versions/A/QtWidgets']
        qt_fw_rpath_rx = re.compile(r'.+/(Qt\w+.framework)/(.+)', re.IGNORECASE)
        # Qt framework destination
        pkg_qt_framework_dir = pkg_dirs['root'] + '/' + pkg_dirs['FW_qt']
        # Qt plugins destination
        pkg_qt_plugins_dir = os.path.realpath(pkg_dirs['root'] + '/' + pkg_dirs['FW_qt_plugin'])

        # extra libraries destination
        # eg. 'usr/local/opt/libA.9.dylib' => '<Package>/Library/libA.9.dylib'
        # the name of the libraries must be the same as the library ids
        pkg_exlib_dir = os.path.realpath(pkg_dirs['root'] + '/' + pkg_dirs['exlib'])

        # core libraries destination
        # eg. 'buildir/lib/CoreLib.so' => '<Package>/lib/CoreLib.so'
        # the name of the libraries must be the same as the library ids
        pkg_corelib_dir = os.path.realpath(pkg_dirs['root'] + '/' + pkg_dirs['corelib'])
        # map {source-path => destination-path}
        pkg_copy_tbl = {p.src: p.dst for p in pkg_libs.values() if p.dst}
        # map {destination-path => LibraryFile}
        pkg_dst_tbl = dict()

        for ref, lib in pkg_libs.items():
            # neglect libraries which have no source path or
            # those which have already a destination
            if not lib.src: continue
            if lib.dst:
                pkg_dst_tbl[lib.dst] = lib
                continue

            basename_new = os.path.basename(lib.ID)

            # make new library ids:
            # eg. '/usr/opt/lib/libtiff.6.dylib' => '@rpath/libtiff.6.dylib'
            # '@loader_path/../lib/libicudata.73.dylib' => '@rpath/libicudata.73.dylib'
            if basename_new: lib.ID_new = "@rpath/" + basename_new
            # Qt framework libraries
            if lib.dtype == 'framework_qt':
                _mg = qt_fw_rpath_rx.match(lib.src).groups()
                # eg. destination = <PKG-ROOT>/Frameworks/qt/QtWidgets.framework/Versions/A/QtWidgets"
                _libID = _mg[0] + '/' + basename_new

                # eg. '/opt/qt/QtCore.framework/Versions/A/QtCore' =>
                #     '@rpath/QtCore.framework/Versions/A/QtCore'
                lib.ID_new = "@rpath/" + _libID
                lib.dst = pkg_qt_framework_dir + '/' + _libID
            elif lib.dtype == 'plugin_qt':
                # full source path of the plugin; eg. '/opt/qt@5/plugins/platforms/libqcocoa.dylib'.
                # full destination path of the plugin (same directory under the _package_ plugins dir);
                # eg. '<Package>/PlugIns/platforms/libqcocoa.dylib'
                lib.dst = pkg_qt_plugins_dir + '/' \
                    + removeprefix(lib.src, pkg_dirs['qt_root_plugins_dir'] + '/')
            elif lib.dtype == 'core_lib':
                lib.dst = pkg_corelib_dir + '/' + basename_new
            elif lib.dtype == 'extra_lib':
                lib.dst = pkg_exlib_dir + '/' + basename_new
            # dependencies with absolute path
            elif lib.dtype == 'abs':
                lib.dst = pkg_exlib_dir + '/' + basename_new
            # dependencies with relative '@loader_path' reference for which a source path is found
            elif lib.dtype == 'rel@loader_path':
                if lib.src in pkg_copy_tbl:
                    lib.dst = pkg_copy_tbl[lib.src]
                else:
                    lib.dst = pkg_exlib_dir + '/' + basename_new
            # dependencies with '@rpath' reference for which a source path is found
            elif lib.dtype == 'rel@rpath':
                if lib.src in pkg_copy_tbl:
                    lib.dst = pkg_copy_tbl[lib.src]
                else:
                    lib.dst = pkg_exlib_dir + '/' + basename_new

            if not lib.dst: continue
            # verify that when the source is the same, the destinations is the same, regardless of the ref.
            if lib.src in pkg_copy_tbl and lib.dst != pkg_copy_tbl[lib.src]:
                raise AssertionError("Multiple destinations found for a single source file"
                    "'%s': %s != %s" % (lib.src, lib.dst, pkg_copy_tbl[lib.src]))

            pkg_copy_tbl[lib.src] = lib.dst
            pkg_dst_tbl[lib.dst] = lib

        return pkg_copy_tbl, pkg_dst_tbl

    @staticmethod
    def reference_destinations(pkg_libs:dict) -> dict:
        """ Determines destinations for the references """

        # map {basename => destination-path}
        pkg_basename_dst_tbl = {os.path.basename(p.dst): p.dst
                                for p in pkg_libs.values() if p.dst}
        # map {reference => destination-path}
        pkg_ref_dst_tbl = dict()
        for lib in pkg_libs.values():
            for dep in lib.dependencies:
                # attempt to obtain the destination from package library
                dep_lib = pkg_libs.get(dep)
                # ignore Python framework files
                if dep_lib and dep_lib.dtype == "framework_py": continue
                _dst = dep_lib.dst if dep_lib else None
                # otherwise, attempt to find the destination upon the basename
                pkg_ref_dst_tbl[dep] = _dst \
                    or pkg_basename_dst_tbl.get(os.path.basename(dep)) or ''

        return pkg_ref_dst_tbl

    @staticmethod
    def new_references_rpaths(pkg_libs:dict, pkg_dst_tbl:dict, pkg_ref_dst_tbl:dict) -> (dict, tuple):
        """ Determines proper RPATHs for each library """

        pkg_ref_tbl = dict() # map {reference_initial => reference_new}
        pkg_dst_lib = {l.dst: l for l in pkg_dst_tbl.values()} # map {destination path => LibraryFile}

        #-- determine proper Python RPATHs
        # find all Python framework binaries
        py_dir = py_lib = py_version = py_rpaths = None
        py_framework_files = {l.ref: l for l in pkg_libs.values()
                              if l.dtype == "framework_py"}

        py_dir, py_lib = PythonFramework.dependence(py_framework_files.keys())
        py_rpaths, py_version = PythonFramework.framework_path(py_dir + '/' + py_lib)

        # modify the the library name: Python -> libpython<Python-version>.dylib
        if py_lib.lower() == "python":
            # extract Python's major and minor versions
            _v_maj, _v_min, *_ = py_version.split('.')
            # construct the standard name of the Python shared library
            py_lib = "libpython" + _v_maj + "." + _v_min + ".dylib"

        for ref in py_framework_files.keys():
            pkg_ref_tbl[ref] = "@rpath/" + py_lib

        #-- determine all proper RPATHs for package binaries
        for ref, lib in ((r, l) for r, l in pkg_libs.items() if l.dst):
            rpaths_ini = set(lib.rpaths)  # initial RPATHs
            rpaths_new = set()  # new RPATHs
            # record references conversions
            if lib.ID_new: pkg_ref_tbl[ref] = lib.ID_new
            for dep in lib.dependencies:
                # add Python-related RPATHs
                if re.match(PythonFramework.py_fmwk_path_re, dep):
                    rpaths_new.update(py_rpaths)
                # add RPATHs related to other libraries within the package
                _dep_dst = pkg_ref_dst_tbl.get(dep)
                if not _dep_dst: continue
                # find the proper RPATH
                _rel_path = pkg_dst_lib[_dep_dst].ID_new.replace("@rpath/", "")
                _rpth = find_rpath(lib.dst, _dep_dst, _rel_path)
                rpaths_new.add(_rpth.rstrip('/'))

            # new RPATHs should be different from the initial ones
            lib.rpaths = rpaths_new.difference(rpaths_ini)

        return pkg_ref_tbl, (py_dir, py_lib, py_version, py_rpaths)

#------------------------------------------------------------------------------80

_core_libs = "@MACPKG_CORE_LIBS@"  # core libraries and executables
core_libs = splat(_core_libs, ';')
_extra_libs = "@MACPKG_EXTRA_LIBS@"

def filter_deps(dep):
    """ allow dependencies which are .so or .dylib shared libraries """
    s_ = dep.split('.')
    return 'so' in s_ or 'dylib' in s_

extra_libs = list(filter(filter_deps, splat(_extra_libs, ';')))

main_exe_src = "@MACPKG_MAIN_EXE@"
pkg_root = ""
if len(sys.argv) > 1:
    # root dir, eg. '/tmp/bornagain.app/Contents'
    pkg_root = os.path.dirname(sys.argv[1] + '/')

# Python-related variables
python_mode = bool("@BA_PY_MODE@")
python_exe = "@Python3_EXECUTABLE@"
pkg_py_output_dir = os.path.dirname("@PY_OUTPUT_DIR@/")
pkg_py_root_dir = os.path.dirname("@BA_PY_INIT_OUTPUT_DIR@/")
pkg_py_lib_dir = os.path.dirname("@BA_PY_LIBRARY_OUTPUT_DIR@/")
pkg_py_extra_lib_dir = os.path.dirname("@BA_PY_EXTRA_LIBRARY_OUTPUT_DIR@/")
pkg_py_wheel_dir = os.path.dirname("@WHEEL_DIR@/")
pkg_py_pure_wheel_name = os.path.realpath("@WHEEL_DIR@/@BA_PY_PURE_WHEEL_NAME@")
pkg_py_wheel_name = os.path.realpath("@WHEEL_DIR@/@BA_PY_WHEEL_NAME@")

# Qt-related variables
# eg. input Qt dir = '/usr/local/opt/qt@5/lib/cmake/Qt'
#   => '/usr/local/opt/qt@5'
qtdir = "@Qt_DIR@/"
# Qt plugins paths relative to Qt root dir
qt_plugins_rel_dir = "@MACPKG_QT_PLUGINS_RELDIR@/"
qt_framework_root = os.path.dirname(qtdir)
qt_framework_plugins_dir = os.path.dirname(qt_framework_root + '/' + qt_plugins_rel_dir)

# list of required Qt plugins
_qt_plugins = "@MACPKG_QT_PLUGINS@"
qt_plugins = splat(_qt_plugins, ';')
qt_plugins_dir = "@MACPKG_QT_PLUGINS_DIR@/"

if python_mode:
    if not python_exe:
        raise ValueError("Error: Provide the Python executable.")

    if not pkg_py_root_dir:
        raise ValueError("Error: Provide the root directory of the Python package.")

    if not extra_libs:
        core_libs = find_dylibs(pkg_py_lib_dir)

    pkg_root = pkg_py_root_dir
else:
    if not pkg_root:
        raise ValueError("Error: Provide the root directory of the package.")

    if not main_exe_src:
        raise ValueError("Error: Main executable does not exist.")

#------------------------------------------------------------------------------80

print(f"Running {' '.join(sys.argv)} .....")
print("* Package variables:")
print("  + package root = '%s'" % (pkg_root))
print("  + main executable = '%s'" % (main_exe_src))
print("  + core libraries = '%s'" % (core_libs))
print("  + extra libraries = '%s'" % (extra_libs))
print("* Python:")
print("  + Python mode = %s" % (python_mode))
print("  + Python executable = '%s'" % (python_exe))
print("  + Python package root dir = '%s'" % (pkg_py_root_dir))
print("  + Python package libraries dir = '%s'" % (pkg_py_lib_dir))
print("  + Python package extra libraries dir = '%s'" % (pkg_py_extra_lib_dir))
print("  + Python wheel dir = '%s'" % (pkg_py_wheel_dir))
print("* Qt:")
print("  + Qt framework root = '%s'" % (qt_framework_root))
print("  + Qt plugings dir = '%s'" % (qt_plugins_dir))
print("  + Qt plugings = '%s'" % (qt_plugins))

#-- package definition
pkg = Settings(pkg_root,
               main_exe_src, core_libs, extra_libs,
               python_mode, pkg_py_root_dir, pkg_py_lib_dir, pkg_py_extra_lib_dir,
               qt_framework_root, qt_framework_plugins_dir, qt_plugins_dir, qt_plugins)

#-- print package information
print("* Package directories:")
for lbl, val in pkg.dirs.items():
    print("  + %s: %s" % (lbl, val))

print("* Initial binaries:")
for fnm in pkg.libs_init:
    # eg., '+ lib/libA.dylib'
    print("  + '%s'" % removeprefix(fnm, pkg.dirs['root'] + '/'))

#-- find the dependencies of the binaries
pkg.libs = Settings.dependencies(pkg.libs, pkg.dirs, pkg.libs_init, level_max=10)

#-- determine the destination of libraries in the package
# map {source-path => destination-path}
pkg.src_dst_tbl, pkg.dst_tbl = Settings.destinations(pkg.libs, pkg.dirs, pkg.libs_init)

#-- print all discovered dependencies
print("* All dependencies (type, source, reference):")
for lib in sorted(pkg.libs.values(), key=lambda l: (l.dtype, l.src)):
    print("  + [%s]: %s {%s}" %
          (lib.dtype,
           lib.src.replace(pkg_root, PKGROOT),
           lib.ref.replace(pkg_root, PKGROOT)))

print("* Installation table (source, destination):")
for src in sorted(pkg.src_dst_tbl.keys()):
    dst = pkg.src_dst_tbl.get(src)
    print("  + %s => %s" %
          (src, dst.replace(pkg_root, PKGROOT) if dst else 'None'))

#-- determine proper RPATHs for each library
print("* Building reference-destination table...")
pkg.ref_dst_tbl = Settings.reference_destinations(pkg.libs)
for ref, dst in pkg.ref_dst_tbl.items():
    if not dst:
        raise AssertionError("Reference '%s' is not resolved" % (ref))
    print("  + {%s} => %s" % (ref, dst.replace(pkg_root, PKGROOT)))

pkg.ref_tbl, py_info = Settings.new_references_rpaths(pkg.libs, pkg.dst_tbl, pkg.ref_dst_tbl)
pkg.py = Settings.PyInfo(*py_info)

print("* Python dependence of package libraries:")
if pkg.py.lib:
    print("  + path: '%s'" % pkg.py.dir)
    print("  + library: '%s'" % pkg.py.lib)
    print("  + version: '%s'" % pkg.py.version)
    print("  + framework paths:")
    for pth in pkg.py.rpaths:
        print("   - %s" % pth)
else:
    print("  + No Python dependence found.")

print("* Reference conversion table (ref_old, ref_new):")
for ref_old, ref_new in pkg.ref_tbl.items():
    print("  + %s => %s" % (ref_old, ref_new))

print("* Library RPATHs (source, RPATHs):")
for lib in (l for l in pkg.dst_tbl.values() if l.rpaths):
    print("  + %s: %s" % (lib.src.replace(pkg_root, PKGROOT),
                        lib.rpaths))

#-- copy binaries to their package destinations
print("* Install package files...")
for dst, lib in pkg.dst_tbl.items():
    mkdirs(os.path.dirname(dst))
    copyfile(lib.src, dst)

#-- apply RPATHs
print("* Add proper RPATHs to the binaries...")
for lib in pkg.dst_tbl.values():
    # eg. RPATHS for 'lib/libA.dylib': ../Library , ../Frameworks/Qt
    # eg. install_name_tool libA.so -add_rpath RPATH1 -add_rpath RPATH2 ...
    _cmd_list = []
    for cmd in (["-add_rpath", r] for r in lib.rpaths):
        _cmd_list.extend(cmd)
    # execute the command
    if not _cmd_list: continue
    _prc = subp.run(["install_name_tool", lib.dst, *_cmd_list])
    if _prc.returncode != 0:
        print("  + Command failed: %s" % (_prc.args))

# modify the library references;
# eg. '/usr/local/opt/foo.dylib' => '@rpath/foo.dylib'
print("* Change library references in binaries...")
for lib in pkg.dst_tbl.values():
    # eg. install_name_tool libA.so -change REF1_old REF1_new -change REF2_old REF2_new ...
    _cmd_list = []
    for ref_old in lib.dependencies:
        ref_new = pkg.ref_tbl[ref_old]
        if ref_old != ref_new: _cmd_list.extend(["-change", ref_old,ref_new])
    # execute the command
    if not _cmd_list: continue
    _prc = subp.run(["install_name_tool", lib.dst, *_cmd_list])
    if _prc.returncode != 0:
        print("  + Command failed: %s" % (_prc.args))

#-- make Python wheel (when in Python mode)
if pkg.python_mode:
    print("* Creating Python wheel in '%s'..." % (pkg_py_wheel_dir))
    print("  + Python executable = '%s'" % python_exe)
    print("  + rename wheel: '%s' => '%s'" % (pkg_py_pure_wheel_name, pkg_py_wheel_name))

    PythonFramework.make_wheel(python_exe, pkg_py_output_dir,
                               pkg_py_wheel_dir, pkg_py_pure_wheel_name, pkg_py_wheel_name)

#-- END
print("* Done.\n")
