Open Git Branches
=================

# j.generic-isInside

By Ludwig Jaeck. Formerly ff-pip.
Use generic isInside algorithm.
Can no longer be merged.
https://jugit.fz-juelich.de/mlz/bornagain/-/merge_requests/1236

# j.predict_wheel_name

Set Python wheel tag without dummy-C-source trick.
https://jugit.fz-juelich.de/mlz/bornagain/-/merge_requests/2507 on hold.

# j.853

Reintroduce <ranges>.
https://jugit.fz-juelich.de/mlz/bornagain/-/merge_requests/2190 on hold.
Wating for compiler update on mac_x86.
