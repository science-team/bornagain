#!/usr/bin/env python3
"""
Dump formfactor for different q scans
"""

import math
import bornagain as ba

eps = 0.005

if __name__ == '__main__':

    width = 1.
    height = 1.
    length = 100.0

    particle = ba.FormFactorCosineRippleBox(length, width, height)
#    particle = ba.FormFactorSawtoothRippleBox(length, width, height, 0.)

    print("# " + particle.shapeName() + " form factor, for different q scans")

    print("# q vs |F(q)| for q in direction 010")
    t = 0.2
    while t < 200:
        q = ba.C3(0, t, 0)
        print(t, " ", abs(particle.formfactor_at_bottom(q)))
        t *= 1+eps
    print()

    print("# q vs |F(q)| for q in direction 0001")
    t = 0.2
    while t < 200:
        q = ba.C3(0, 0, t)
        print(t, " ", abs(particle.formfactor_at_bottom(q)))
        t *= 1+eps
    print()

    print("# q vs |F(q)| for q in direction 011")
    t = 0.2
    s2 = math.sqrt(2)
    while t < 200:
        q = ba.C3(0, t/s2, t/s2)
        print(t, " ", abs(particle.formfactor_at_bottom(q)))
        t *= 1+eps
    print()

    print("# q vs |F(q)| for |q|=50, q on cercle in 100 plane")
    ampl = 50
    t = 0
    while t < 2*math.pi:
        q = ba.C3(0, ampl*math.cos(t), ampl*math.sin(t))
        print(t, " ", abs(particle.formfactor_at_bottom(q)))
        t += eps
    print()
