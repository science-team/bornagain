#*** Powershell Build Script ***
# This script is only intended for debugging purposes on MS-Windows platform.

# execute under windows:
# $ powershell -NoProfile -NonInteractive -ExecutionPolicy Bypass -f <build-script>
# build a particular MSBuild project:
# $ msbuild BornAgainBase.vcxproj [-target:BornAgainBase] -property:Configuration=Release

# stop on first error
$ErrorActionPreference = "Stop"

$SRC_DIR = "C:/tmp/bornagain"
$OPT_DIR = "C:/opt/x64"
$FFTW3_INCLUDE_DIR = "$OPT_DIR/include"
$FFTW3_LIB = "$OPT_DIR/lib/libfftw3-3.lib"
$BOOST_DIR = "$OPT_DIR/boost_current"
$BOOST_INCLUDE_DIR = "$BOOST_DIR/include"
$BOOST_LIB_DIR = "$BOOST_DIR/lib"
$QT_MSVC_DIR = "C:/Qt/6.2.4/msvc2019_64"
$QTCMake_DIR = "$QT_MSVC_DIR/lib/cmake"

# change the system PATH temporarily (needed for tests)
$PY_PLATFORM = "C:/Users/admin/.pyenv/pyenv-win/versions/3.11.5/"
# $PY_PLATFORM = "C:/Users/admin/.pyenv/pyenv-win/versions/3.10.11/"
# $PY_PLATFORM = "C:/Users/admin/.pyenv/pyenv-win/versions/3.9.13/"
# $PY_PLATFORM = "C:/Users/admin/.pyenv/pyenv-win/versions/3.8.10/"
# $PY_PLATFORM = "C:/Users/admin/.conda/envs/py311"
# $PY_PLATFORM = "C:/opt/multipython/Python310"

$Env:Path = "$QT_MSVC_DIR/bin;$PY_PLATFORM;$Env:Path"
$BUILD_DIR = "build"

# set minimal path for build
# $Env:Path = "%SystemRoot%\system32;%SystemRoot%;%SystemRoot%\System32\Wbem;$QT_MSVC_DIR\bin;C:\Program Files\Python39;$OPT_DIR\lib;$BOOST_LIB_DIR;C:\Program Files\Git\cmd;C:\Program Files\CMake\bin;C:\msys64\usr\bin;C:\msys64\mingw64\bin;C:\Program Files\ninja-win\;C:\Program Files (x86)\NSIS;"

echo "#--- DIAGNOSTICS ---"

# list powershell properties
echo "# Powershell <$PSHOME>`n  PS Profile <$PROFILE>`n  PS Command-Path <$PSCOMMANDPATH>"

# list all environmental variables
echo "----------------------------------------"
dir Env:

# make the CMake build directory
echo "----------------------------------------"
mkdir -Force "$BUILD_DIR"
cd "$BUILD_DIR"
pwd
echo "#--- CONFIGURE ---"
cmake --version

# use Ninja

# Visual Studio path <https://github.com/microsoft/vswhere/wiki/Find-VC>

$vsPath = &"${env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer\vswhere.exe" -latest -products * -requires Microsoft.VisualStudio.Component.VC.Tools.x86.x64 -property installationpath

echo "Microsoft Visual Studio path = '$vsPath'"

Import-Module (Get-ChildItem $vsPath -Recurse -File -Filter Microsoft.VisualStudio.DevShell.dll).FullName

Enter-VsDevShell -VsInstallPath $vsPath -SkipAutomaticLocation -DevCmdArguments '-arch=x64'

Set-Item -Path "env:CC" -Value "cl.exe"
Set-Item -Path "env:CXX" -Value "cl.exe"

cmake -G "Ninja" -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH="$OPT_DIR;$PY_PLATFORM;$QTCMake_DIR" -DCMAKE_INCLUDE_PATH="$BOOST_INCLUDE_DIR" -DCMAKE_LIBRARY_PATH="$BOOST_LIB_DIR" -DCMAKE_C_COMPILER="cl.exe" -DCMAKE_CXX_COMPILER="cl.exe" -B. ..

# use MSBuild
# cmake -G "Visual Studio 17 2022" -A x64 -T host=x64 `
#     -DCMAKE_PREFIX_PATH="$OPT_DIR;$QTCMake_DIR" `
#     -DQTDIR="$QT_MSVC_DIR" `
#     -DFFTW3_INCLUDE_DIR="$FFTW3_INCLUDE_DIR" -DFFTW3_LIBRARY="$FFTW3_LIB" `
#     -DCMAKE_INCLUDE_PATH="$OPT_DIR/include;$BOOST_INCLUDE_DIR" -DCMAKE_LIBRARY_PATH="$OPT_DIR/lib;$BOOST_LIB_DIR" `
#     -DCMAKE_BUILD_TYPE=Release `
#     -DCMAKE_C_COMPILER="cl.exe" -DCMAKE_CXX_COMPILER="cl.exe" `
#     -B. ..

echo "#--- BUILD ---"
pwd
$build_timer =  [system.diagnostics.stopwatch]::StartNew()
# MSBuild
# cmake --build . --config Release -j20  -- /fl /flp:logfile=BornAgainMSBuild.log

# Ninja
cmake --build . --config Release -j20

cmake --build . --config Release --target ba_wheel

$build_timer.Stop()

echo "#--- TEST ---"
$test_timer =  [system.diagnostics.stopwatch]::StartNew()
ctest -C Release --parallel 8 --output-on-failure
$test_timer.Stop()

echo "#--- PACKAGING ---"
# build package
cpack -C Release -B ./installer .

echo "----------------------------------------"
echo "#--- Total Build Time = $($build_timer.Elapsed.TotalMinutes) min(s)."
echo "#--- Total Test Time = $($test_timer.Elapsed.TotalMinutes) min(s)."
