# BornAgain-website

Documentation repository for the scientific software [BornAgain](http://www.bornagainproject.org).

## About BornAgain

BornAgain is an open-source research software for simulating and fitting
neutron and x-ray reflectometry and grazing-incidence small-angle scattering,
developed by the [Scientific Computing Group](http://computing.mlz-garching.de)
at [JCNS](http://fz-juelich.de/jcns)-[MLZ](http://www.mlz-garching.de/).

The project web site is http://www.bornagainproject.org;
the source repository is https://jugit.fz-juelich.de/mlz/bornagain.

## About this documentation repository

This Git repository https://jugit.fz-juelich.de/mlz/doc/bornagain-www) contains
the sources for the documentation shown at http://www.bornagainproject.org.
The sources are processed by the static site generator [Hugo](https://gohugo.io/).
The documentation theme [hugo-theme-scgdoc](https://github.com/scgmlz/hugo-theme-scgdoc)
is included as a submodule.

Instructions for using Hugo and for contributing to this project are at
https://www.bornagainproject.org/latest/howto/contribute-to-documentation.


## Figures

The figures for the documentation are produced by Inkscape 1.0.2 (2021-01-15).
