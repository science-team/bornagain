+++
title = "Documentation"
weight = 50
+++

# Documentation

This is the documentation of BornAgain version {{< version-name >}}.
For other releases, see [here]({{< ref-home "documentation">}}).

Software and documentation are work in progress.
Some features of the software are not yet documented.
[Contact us]({{< ref-home "contact" >}}) for any questions.

##### Contents

{{% children  %}}

##### Other documents

Complementary information can be found in the

* Reference Paper: [J. Appl. Cryst. 2020](https://doi.org/10.1107/S1600576719016789)
* Update for releases 1.17 to 20: [EPJ Web Conf. 286, 06004 (2023)](https://doi.org/10.1051/epjconf/202328606004)

Older materials are at

* https://github.com/scgmlz/BornAgain-tutorial
* [Physics Reference]({{% files-versioned %}}/BornAgain-PhysicsReference.pdf), fragmentary
