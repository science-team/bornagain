+++
title = "Documentation"
weight = 50
+++

# Documentation

This is the documentation of BornAgain version {{< version-name >}}.
For other releases, see [here]({{< ref-home "documentation">}}).

Software and documentation are work in progress.
Some features of the software are not yet documented.
[Contact us]({{< ref-home "contact" >}}) for any questions.

##### Contents

{{% children  %}}

##### Other documents

Complementary information can be found in the

* Reference Paper: [J. Appl. Cryst. 2020](http://journals.iucr.org/j/issues/2020/01/00/ge5067/ge5067.pdf)
* [Physics Reference]({{% files-versioned %}}/BornAgain-PhysicsReference.pdf)

Older course materials are at

* https://github.com/scgmlz/BornAgain-tutorial
