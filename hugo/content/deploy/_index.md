+++
title = "Install or build"
weight = 20
+++

## Install or build BornAgain

BornAgain is supported under the operating systems Linux, Windows, and Mac OS X.

BornAgain (unless specially build for GUI only)
requires Python3 and a number of Python modules. See the [Install Python](py) section.

For each of these, we provide self-contained binary installers,
as described in the [Install BornAgain](install) section.

For Linux, it may be even more convenient to use the Debian/Ubuntu package
provided by external maintainers at https://tracker.debian.org/pkg/bornagain.

See the [BuildnAgain](building) section to compile BornAgain from source.
