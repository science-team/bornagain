+++
title = "Unix"
weight = 30
+++

# Build BornAgain under Unix

This section describes how to build BornAgain from source
under a Unix-like environment (Linux, MacOS).

{{% children  %}}
