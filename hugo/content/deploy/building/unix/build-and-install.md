+++
title = "Build and install BornAgain"
weight = 30
+++

## Build and install BornAgain

The following instructions must be executed for the software projects
- libheinz,
- libformfactor,
- BornAgain,

in the given order.

Each source package comes with configuration files for the
cross-platform build system [CMake](https://cmake.org). Using CMake,
it takes the following commands to build and install the software:

```bash
$ cd <source_dir>
$ mkdir build # see note [1]
$ cd build
$ cmake [<options>] .. # see note [4]
$ make -j4  # see note [2]
$ make ba_wheel  # make the Python wheel
$ ctest -j4
$ make install # see note [3]
```

##### Note [1]: Build directory

The build process must take place "out of place", i.e. not directly in the source directory.
Otherwise, location and name of the build directory are arbitrary.
We nonetheless recommend to follow our convention,
create a subdirectory under the source directory, and call it `build`.

##### Note [2]: Make or Ninja

With GNU Make, the option `-j<n>` indicates the number of processors to be used in parallel.

In our experience, the alternative build software `Ninja` is faster and therefore to be preferred.
To use Ninja,
  * install it (e.g. Debian package ninja-build)
  * call `cmake` with option `-GNinja`
  * replace calls of `make -j<n>` by `ninja`.

##### Note [3]: Install location

The install directory can be specified by supplying the option
`-DCMAKE_INSTALL_PREFIX=<install_dir>` to the `cmake` command.
Depending on the choice of the installation directory,
it may or may not be necessary to run the command `make install`
(or `ninja install`) under `sudo`.

##### Note [4]: Other CMake options

Besides the `cmake` options already mentioned, you may need
`-DCMAKE_PREFIX_PATH=<paths>`
where `<paths>` is a semicolon-separated list of directories
specifying directories that are searched by CMake for
the libraries and auxiliary programs that are needed for building BornAgain.
