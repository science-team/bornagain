+++
title = "Run a first simulation"
weight = 40
+++

## Run a first simulation

This section explains how to run a first simulation.

* [Post installation steps](#post-installation-steps")
* [Running the first Python simulation](#running-the-first-python-simulation")
* [Running the BornAgain GUI](#running-the-borngain-gui")

#### Post installation steps

If a Python package is produced in the build phase, then after installation, the Python package ('wheel') can be found under the folder
`<install_dir>/python/<python-version>/wheel`.
The wheel can be installed via `pip`; for instance
```bash
python3 -m pip install BornAgain-<detailed-version>.whl
```
or
```bash
pip3 install BornAgain-<detailed-version>.whl
```

#### Running the first Python simulation

In your installation directory you will find the following directory structure:

```
|-- bin                   - Links to executables
|-- include
|   |-- BornAgain-{{< version-name >}}    - C++ headers for development purposes
|-- lib
|   |-- BornAgain-{{< version-name >}}    - The BornAgain libraries
|-- share
|   |-- BornAgain-{{< version-name >}}
|       |-- Examples      - Directory with examples
```

Run an example and enjoy your first BornAgain simulation plot.

```python
python <install_dir>/share/BornAgain-{{< version-name >}}/Examples/scatter2d/CylindersAndPrisms.py
```

#### Running the BornAgain GUI

The BornAgain application can be run by executing following command
```bash
$ <install_dir>/bin/bornagain
```
