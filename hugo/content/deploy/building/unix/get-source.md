+++
title = "Get the source"
weight = 20
+++

## Get the sources

Besides the sources of [BornAgain]({{% url-jugit %}}/bornagain) you need
those of our support libraries [libheinz]({{% url-jugit %}}/libheinz)
and {{% link-libformfactor %}}.

If in doubt, install the tarballs of the latest published release:
```bash
{{% url-jugit %}}/libheinz/-/releases
{{% url-jugit %}}/libformfactor/-/releases
{{% url-jugit %}}/bornagain/-/releases
```


If you are ready to live on the edge, you may also clone our Git repository
and work on the current "main" branch.

#### Source tarballs

The `tar` command allows downloading and unpacking in one go:
```bash
$ wget -qO- {{% url-jugit %}}/libheinz/-/archive/main/libheinz-main.tar.gz | tar zx
$ wget -qO- {{% url-jugit %}}/libformfactor/-/archive/main/libformfactor-main.tar.gz | tar zx
$ wget -qO- {{% url-jugit %}}/bornagain/-/archive/main/bornagain-main.tar.gz | tar zx
```

#### Git

You can also get access to the source code by cloning our public Git repository:
```bash
$ git clone {{% url-jugit %}}/libheinz.git
$ git clone {{% url-jugit %}}/libformfactor.git
$ git clone {{% url-jugit %}}/bornagain.git
```
