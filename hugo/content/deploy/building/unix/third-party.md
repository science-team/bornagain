+++
title = "Install third-party software"
weight = 10
+++

## Install third-party software

As a prerequisite, Python and some Python modules must be installed.
For the recommended installation under pyenv, see
* [Python on Linux](/deploy/py/linux).
* [Python on Mac](/deploy/py/mac).

Further required software:

* Compiler with basic C++-20 support (`gcc` or `clang`)
* `cmake >= 3.20`
* `gsl >= 1.15` (GNU scientific library)
* `fftw3 >= 3.3.1` (fast Fourier transform)
* `libtiff >= 4.0.2` (TIFF image loader)
* `libcerf >= 2.0` (complex error function) [1]
* `boost >= 1.65` [2]
* `zlib1g-dev >= 1.2.11` (zlib compression library)
* `bzip2 >= 1.0.8` (bzip2 compression library)
* `Qt6`

[1] The library `libcerf` happens to be maintained by our own group.
Nonetheless it is treated as an external dependency because packages
are provided by all major Linux distributions (see https://pkgs.org).

[2] From `libboost`, we need a number of header-only components,
including `algorithm/string`, `bimap`, `geometry`, `numeric/interval`, `polymorphic_cast`.

Recommended software:

* For generating a man page:
  * `perl`
* For [MatPlotLib usetex mode](/ref/result/matplotlib):
  * `texlive-latex-extra` (and dependencies)
  * `dvipng`
  * `cm-super-minimal`

All these can be easily installed on most Linux distributions using
the system's package manager. Below are a few examples for several
selected operating systems. Note that other distributions
(Fedora, Mint, etc) may have different commands for invoking the
package manager and slightly different names of packages (like `boost`
instead of `libboost` etc). Besides that, the installation should be
very similar.

### Debian / Ubuntu / ...

Install required packages:
```
$ sudo apt-get install build-essential git cmake \
  libgsl-dev libfftw3-dev libtiff5-dev \
  libboost-dev \
  libcerf-dev qt6-base-dev qt6-svg-dev mesa-common-dev
```

If CMake fails with "Could NOT find OpenGL", then install `libgl1-mesa-dev`.
If it fails with "Could NOT find OpenGL (missing: EGL)", then install `libegl1-mesa-dev`.

We assume that Python is [installed under pyenv](/deploy/py/linux);
otherwise we also need the packages
```
  python3 python3-dev python3-pip python3-numpy python3-matplotlib python3-lmfit
```

Note: Normally we should also depend on `libcerf-dev`,
which however is presently outdated.
Therefore Debian users should install libcerf from source,
https://jugit.fz-juelich.de/mlz/libcerf.

### OpenSuse
Install required packages:
```
$ sudo zypper install gcc-c++ git-core cmake gsl-devel \
  libboost_*-devel fftw3-devel libcerf-devel libtiff-devel \
  libqt6-qtbase-devel libqt6-qttools-devel libqt6-qtsvg-devel
```

We assume that Python is [installed under pyenv](/deploy/py/linux);
otherwise we also need the packages
```
  python3-devel python3-pip \
  python3-numpy-devel python3-matplotlib python3-lmfit
```

### MacOS

To install third-party dependencies on MacOS, we recommend using
the package manager <a href=https://brew.sh/>Homebrew</a>:

```
brew install git cmake fftw gsl libcerf python boost libtiff zlib bzip2 qt6
pip3 install matplotlib numpy
```

By default, Homebrew installs packages to `/opt/homebrew` or `/usr/local`,
depending on the architecture dependent MacOS version.
The Homebrew installation directory can be obtained via the command:
```
$ brew --prefix
```

Possibly, this path has to be passed to CMake using the argument `-DCMAKE_PREFIX_PATH`
(see the [build instructions](build-and-install).

Add Qt to your path environment variable:
```
export PATH=/usr/local/opt/qt6/bin/:$PATH
```
