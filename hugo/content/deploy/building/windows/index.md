+++
title = "Windows"
weight = 50
+++

## Build under Windows

The following instructions have been tested under Windows 10.

### Prerequisites

#### Visual Studio

Download **Visual Studio Community Edition** from from the official website [visualstudio.microsoft.com](https://visualstudio.microsoft.com)
The current build instructions are tested with Visual Studio 2022.

#### Qt

Download Qt from from the official website [qt.io](https://www.qt.io/download-open-source)

> Make sure to choose the open source version of Qt and download the online installer.

Select the latest Qt version with long-time support (LTS) and open the sub tree.
To build BornAgain, only the 'MSVC 2019 64-bit' component is needed.
Qt Installer Framework is required for creating the installer executable.

Make a symbolic link to the directory corresponding to the current Qt version:
if `Qt 6.y.z` (e.g. `Qt 6.3.5`) installed in `C:\Qt\6.y.z`, then in Windows `cmd` (with administrative rights) execute:

```
$ mklink /D "C:\Qt\current" "C:\Qt\6.y.z"
$ mklink /D "C:\Qt\msvc" "C:\Qt\6.y.z\msvc2019_64"
```

#### CMake

Download the **Windows x64 Installer** from the official website [cmake.org/download](https://cmake.org/download)

#### Python + Packages

Python and some Python modules must be installed, as described in
[Python on Windows](/deploy/py/win).

#### SWIG

If you want to re-build the Python-API with SWIG, download and install SWIG
from the official website
[swig.org/download.html](http://www.swig.org/download.html).
Installation instructions are at http://www.swig.org/Doc4.1/windows.html.

Unpack the ZIP file and add its folder to PATH (so e.g. `C:\swigwin-4.0.2`).

#### Libraries

Download
- from https://computing.mlz-garching.de/download/WinLibs:
  - `libfftw3.win64.zip` (fast Fourier transform)
  - `libtiff.win64.zip` (Support for TIFF images)
  - `gsl_*_win64_shared.zip` (GNU Scientific Library)
  - `boost_*.7z` (C++ Library boost, including BZIP2 and ZLib)
- from https://jugit.fz-juelich.de/mlz/libcerf/-/releases
  - from latest release > Asserts > Packages, `cerfcpp-*-win64.zip`
- from https://jugit.fz-juelich.de/mlz/libformfactor/-/releases/
  - from latest release > Asserts > Packages, `formfactor-*-win64.zip`
Create the folders

`C:\opt\x64\include`\
`C:\opt\x64\lib`

and paste the corresponding content from the ZIP files in those two folders.

#### System Enviroment Variables

After those installations your `PATH` should contain

```
C:\Qt\6.y.z\msvc2019_64\bin
C:\Program Files\Python39\Scripts\
C:\Program Files\Python39\
C:\opt\x64\include
C:\opt\x64\lib
C:\Program Files\CMake\bin
```

### Build

With all prerequisites completed, you should be able to build and test BornAgain in a PowerShell.
In there, execute

```
$OPTDIR = "C:/opt/x64"
$FFTW3_INCLUDE_DIR = "$OPTDIR/include"
$FFTW3_LIB = "$OPTDIR/lib/libfftw3-3.lib"
$QTDIR = "C:/Qt/current/msvc2019_64"
$QTCMake_DIR = "$QTDIR/lib/cmake"
$BUILD_DIR = "build"

mkdir -Force "$BUILD_DIR"
cd "$BUILD_DIR"

cmake -G "Visual Studio 17 2022" -A x64 -T host=x64 -DQTDIR="$QTDIR" -DQt6_DIR="$QTCMake_DIR/Qt6" -DQt6Test_DIR="$QTCMake_DIR/Qt6Test" -DFFTW3_INCLUDE_DIR="$FFTW3_INCLUDE_DIR" -DFFTW3_LIBRARY="$FFTW3_LIB" -DCMAKE_INCLUDE_PATH="$OPTDIR/include" -DCMAKE_LIBRARY_PATH="$OPTDIR/lib" -DCMAKE_CXX_COMPILER_LAUNCHER=ccache -DCMAKE_C_COMPILER="cl.exe" -DCMAKE_CXX_COMPILER="cl.exe" -B. ..

cmake --build . --config Release
cmake --build . --config Release --target ba_wheel

ctest -C Release --parallel 8 --output-on-failure
```
<br>

<b> Build with Ninja </b>

Compilation might be accelerated if Ninja is used as a build system.
Download the Ninja binary for Windows from [ninja-build.org](https://ninja-build.org). Add the binaries to a directory in your `PATH`; e.g. `C:\Program Files\ninja`.

To use Ninja as the build system, the compilation cannot be performed in a PowerShell but in the Visual Studio Shell **x64 Native Tools Command Prompt for VS 2019** which you can search for in the start menu.

Use the following commands to build BornAgain:
```
set OPTDIR=C:/opt/x64
set FFTW3_INCLUDE_DIR=%OPTDIR%/include
set FFTW3_LIB=%OPTDIR%/lib/libfftw3-3.lib
set QTDIR=C:/Qt/current/msvc2019_64
set QTCMake_DIR=%QTDIR%/lib/cmake
set BUILD_DIR=buildnj

mkdir "%BUILD_DIR%"
cd "%BUILD_DIR%"

cmake --version

cmake -G "Ninja" -DCMAKE_BUILD_TYPE=Release -DQTDIR="%QTDIR%" -DQt6_DIR="%QTCMake_DIR%/Qt6" -DQt6Test_DIR="%QTCMake_DIR%/Qt6Test" -DFFTW3_INCLUDE_DIR="%FFTW3_INCLUDE_DIR%" -DFFTW3_LIBRARY="%FFTW3_LIB%" -DCMAKE_INCLUDE_PATH="%OPTDIR%/include" -DCMAKE_LIBRARY_PATH="%OPTDIR%/lib" -DCMAKE_C_COMPILER="cl.exe" -DCMAKE_CXX_COMPILER="cl.exe" -B. ..

echo "## BUILD START:" %time%
cmake --build . --config Release --parallel 8
cmake --build . --config Release --target ba_wheel

echo "## BUILD END:" %time%
ctest -C Release --parallel 8 --output-on-failure
```

</details>
