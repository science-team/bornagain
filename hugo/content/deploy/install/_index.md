+++
title = "Install BornAgain"
weight = 10
+++

## Installing BornAgain

To install BornAgain from binary packages, follow these platform-specific instructions:

  * [Linux](linux)
  * [Windows](windows)
  * [Mac](macos)

Usually, we recommend installing the latest public release.

However, users who need specific novel features or/and are willing to help us as testers
are welcome to download the latest

  * [development snapshot](snapshot)
