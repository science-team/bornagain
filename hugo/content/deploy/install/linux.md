+++
title = "Linux"
weight = 30
+++

## Install on Linux

### Prequisites

##### Python

As a prerequisite, Python must be installed.
For the recommended installation under pyenv, see [Python on Linux](/deploy/py/linux).

##### glibc

Our BornAgain binaries (pip package and installer) are built on Debian 11 and require [glibc](https://www.gnu.org/software/libc) version 2.31 or higher.

To see which glibc version is available on your system, run
```
$ ldd --version
```
Versions for other Linux distributions can be seen at
https://repology.org/project/glibc/versions

### Installation methods

##### as Python-only package

If the BornAgain GUI is not required as only Python scripting mode is to be used,
then BornAgain can be installed as a Python package from the `pip` repository:
```
$ python -m pip install bornagain
```

##### as a Debian package

A BornAgain Debian/Ubuntu package (Python and GUI) is provided by external maintainers
  at https://tracker.debian.org/pkg/bornagain.

The only drawback is that versions may be lagging behind,
in *unstable* by a couple of months, in *stable* by several years.

##### using our installer

A Linux installer for BornAgain (GUI and Python scripting)
can be downloaded from {{% files-versioned %}}/linux_x64.
Choose the installer that fits your version of Python.

The installer is self-extracting and does _not_ need root permission.

We recommend installation to a dedicated directory (here `~/ba`):
```
$ chmod u+x {{% recommended-linuxinstaller %}} # permit execution
$ {{% recommended-linuxinstaller %}} --help # show options
$ ba_path='~/ba'
$ mkdir $ba_path
$ ./{{% recommended-linuxinstaller %}} --exclude-subdir --skip-license --prefix=$ba_path
```
Then the GUI can be executed:
```
$ cd $ba_path
$ ./bin/bornagain
```

The corresponding Python wheel is included in the folder `$ba_path/share/BornAgain/wheel/`
and can be installed via a the following command:
```
$ python -m pip install $ba_path/share/BornAgain/wheel/*.whl
```

The GUI package was tested with the standard X11 window system.
If you are using the alternative Wayland window system,
please let us know about your experience.
