+++
title = "MacOS"
weight = 70
+++

## Install on MacOS

#### Prequisite: Python

As a prerequisite, Python must be installed, see [Python on Mac](/deploy/py/mac).

#### Install BornAgain as Python-only package

If the BornAgain GUI is not required as only Python scripting mode is to be used,
then BornAgain can be installed as a Python package from the `pip` repository:
```
$ python -m pip install bornagain
```

#### Install BornAgain GUI application

A Linux installer for BornAgain (Python and GUI)
can be downloaded from
  * {{% files-versioned %}}/mac_arm or
  * {{% files-versioned %}}/mac_x64.

Choose the installer that fits your version of Python.

After downloading the installer, double click `.dmg` file to mount it, accept the license agreement and then drag the BornAgain icon onto the Applications shortcut icon.

{{< figscg src="/img/draw/installation_macdmg2.png" class="center">}}

Depending on your system's security settings you might not be able to open BornAgain directly from the Launchpad. In this case, search Apple help pages for something like "Open a Mac app from an unidentified developer".
