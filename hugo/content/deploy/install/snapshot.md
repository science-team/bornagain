+++
title = "Development snapshot"
weight = 90
+++

## Install development snapshot

Usually, we recommend installing the latest public release.
However, users who need specific novel features or/and are willing to help us as testers
are welcome to download the latest development snapshot.

Here, we explain the download path for Windows.
For other platforms, pertinent paths will be easily found by analogy.

Go to the jobs listing at https://jugit.fz-juelich.de/mlz/bornagain/-/jobs.

Look for an entry like
{{< figscg src="/img/job_win_main.png" class="float-left">}}
that has the green tag <img src="/img/job_tag_passed.png"/>,
the gray tag <img src="/img/job_tag_main.png"/>,
and the blue tag <img src="/img/job_tag_win.png"/>.

Follow the link underneath the green "Passed" tag <img src="/img/job_tag_passed.png"/> to page
that shows the build log. On the right margin,
there is a "Job artifacts" section:
{{< figscg src="/img/job_artifacts.png" class="float-left">}}
Follow the link "Browse", then "build", then "installer".
With two more clicks, download the installer.
