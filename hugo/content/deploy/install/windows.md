+++
title = "Windows"
weight = 50
+++

## Install on Windows

#### Install Python environment

As a prerequisite, Python must be installed, see [Python on Windows](/deploy/py/win).

#### Install BornAgain as a Python package

To install BornAgain as a pure Python package (for scripting only, no GUI),
download and install the latest BornAgain Python wheel from the [PyPI](https://pypi.org) repository:
```
$ python -m pip install bornagain
```

#### Install full BornAgain (GUI and Python scripting)

To install full BornAgain (GUI app and Python module),
download an installer from {{% files-versioned %}}.
The installer should be chosen accordingly to the Python minor version installed on the
destination machine, for example `{{% recommended-wininstaller %}}`.

After downloading the installer,
double click the `.exe` file and follow the instructions on the screen.

{{< figscg src="/img/bornagainapp_32.png" class="float-left">}}

Use the BornAgain icon located on the desktop to start the GUI.
Refer to [Using graphical user interface](/gui)
section for a basic overview of GUI functionality.

The corresponding Python wheel is included in the folder `C:/<Bornagain installation path>/share/BornAgain/wheel/`
and can be installed via a the following command:
```
$ python -m pip install C:/<Bornagain installation path>/share/BornAgain/wheel/share/BornAgain/wheel/*.whl
```

### Troubleshooting

Frequent problems include the following:

* [BornAgain.exe cannot start](#dll-not-found)
* [How many Python distributions are installed on the system?](#how-many-python)
* [Does the Python interpreter version matches the BornAgain installation?](#does-interpreter-match)

#### BornAgain.exe cannot start
{{% anchor "dll-not-found" %}}

{{< figscg src="/img/win_dll_not_found.png" class="center" width="450px">}}

This message appears on running `BornAgain.exe` if BornAgain cannot find Python libraries.

If Python is correctly installed on the system according to [Install Python environment](#install-python-environment), check if it discoverable by the system. Open command line or PowerShell and type
```
$ python
```
If Python interpreter has not started, then its installation directory is not in system `PATH`.
Open

`Edit the system environment variables` -> `Environment variables` -> `System variables` -> `Path` -> `Edit`

and add Python installation directory to the end

{{< figscg src="/img/win_path.png" class="center" width="450px">}}

Note that the directory of the Python executable can be found via the following Python command:
```
$ python -c "import os,sys; print(os.path.dirname(os.path.realpath(sys.executable)));"
```

#### How many Pythons are installed on the system?
{{% anchor "how-many-python" %}}

Having more than one Python interpreter installed on the system may cause subtle errors.
Managing their co-existence is possible, but may require special skills.
It may be preferable to uninstall all Python versions but one.

#### Does the Python interpreter version match the BornAgain installation?
{{% anchor "does-interpreter-match" %}}

BornAgain is a `64-bit` application and requires a `64-bit` Python3 installed on the system.

The Python version number (major.minor), indicated in the BornAgain installer name
(e.g. `{{% recommended-wininstaller %}}`) must
match the Python installation on your system.
