+++
title = "Install Python"
weight = 5
+++

## Install Python and Python modules

BornAgain requires Python3 (version >= 3.10) and a number of Python modules.

The only exception is if BornAgain has been specially built
with the flag `BORNAGAIN_PYTHON=OFF`, which results in a GUI app
that has no support for import or export of Python scripts.

Therefore, before [installing](/deploy/install) or [building](/deploy/building)
BornAgain, install Python following these operating-system specific instructions:

  * [Python on Linux](linux)
  * [Python on Windows](win)
  * [Python on Mac](mac)

Then, install the
  * [Python modules](modules)
required by BornAgain.
