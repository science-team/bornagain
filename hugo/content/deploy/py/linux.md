+++
title = "Python on Linux"
weight = 10
+++

## Install Python on Linux

Any Linux distribution (Debian/Ubuntu, Fedora, etc) provides packages
for Python and for many Python modules. This approach, however, runs into
difficulties if one needs Python modules that are _not_ available in the
distribution. Installing them using the Python package manager Pip can
cause inconsistencies and break the system. Therefore, distributions at
some point (e.g. Debian 12) disabled Pip, which terminates with
`error: externally-managed-environment`. While this behavior can be
overridden by a special flag, we advise against.
Rather, we recommend escaping from Python version hell by using the
Python version manager [pyenv](https://github.com/pyenv/pyenv).

#### Requirements

To provide a backend for the Python plotting module matplotlib,
install the GUI toolkit Tk before installing pyenv (otherwise,
you will need to rerun `pyenv install <version>`).
Also, two other development packages are required.
A warning about missing sqlite3 support can be ignored.

| Distribution | Packages  |
| ------------ | --------  |
| Debian       | tk-dev   libssl-dev   libreadline-dev |
| Redhat       | tk-devel libssl-devel libreadline-devel |
| Archlinux    | Tk       ? ? |


#### Python in pyenv

Pyenv installation requires a few more libraries and headers:

| Distribution | Packages  |
| ------------ | --------  |
| Debian       | libffi-dev libsqlite3-dev libbz2-dev liblzma-dev |

Prepare the shell by adding
```
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
```
to `~/.bashrc` (or whatever startup configuration file), and
restart the shell.

Then:
```bash
# install pyenv
curl https://pyenv.run | bash

# install Python (e.g. {{% recommended-python %}})
pyenv install {{% recommended-python %}}
pyenv global {{% recommended-python %}}
which python # shows path in virtual environment

# install Python modules
pip install numpy matplotlib corner emcee lmfit scipy tqdm wheel auditwheel setuptools
```
For explanations, see the page on required or recommended [modules](modules).
