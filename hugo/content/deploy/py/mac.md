+++
title = "Python on Mac"
weight = 30
+++

## Install Python on Mac

The pre-installed system Python on MacOS may be outdated,
and may be in conflict with external module requirements.
Therefore we strongly recommend to install Python and the Python module manager `pip`
from scratch.

The preferred installation method for Python is using Homebrew.
For instance, to install Python 3.11, use
```
brew install python@3.11
```
