+++
title = "Python modules"
weight = 80
+++

## Python modules required by BornAgain


The BornAgain GUI (unless specially built without Python support)
requires the Python module
  * numpy

All BornAgain Python scripting examples require
* matplotlib

Furthermore, a small number of fit script examples require the Python modules
  * corner
  * emcee
  * fabio
  * lmfit
  * scipy
  * tqdm

These are not required for _installing_ and _running_ most of BornAgain.
However, they should be present when _building_ BornAgain
lest some tests will fail.
