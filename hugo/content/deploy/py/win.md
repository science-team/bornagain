+++
title = "Python on Windows"
weight = 20
+++

## Install Python on Windows

Run the installer and follow the steps.

To use BornAgain GUI application, the installation directory should be added to the system `PATH`:

{{< figscg src="/img/win_add_to_PATH.png" class="center" width="450px">}}

Ensure that `pip` package manager is checked for installation. Command
```
$ python -m ensurepip
```
will install `pip` if it is missing.

#### BornAgain with Anaconda

BornAgain can also work the Python from the Anaconda platform.
Note that the path to directory which includes the Python platform should be added to the system `PATH`.

The directory of the Python executable can be found via the following Python command:
```
$ python -c "import os,sys; print(os.path.dirname(os.path.realpath(sys.executable)));"
```

On Windows, Anaconda is installed by default under `C:\Users\<username>\Anaconda3\` where `<username>` is the name of the current user account.
Different Python versions are stored in sub-directories of Anaconda's root path;
see the official Anaconda documentation for further details.

#### Verifying system path

Open “Environment Variables” window.
To do that, type “environment variables” in the search box of the taskbar.

{{< figscg src="/img/env_win_search.png" class="center" width="450px">}}

In the “System variables” pane, click on “Path”, and then click on the “Edit...” button.

{{< figscg src="/img/env_variables.png" class="center" width="450px">}}

In the “Edit environment variable” window, check if the Python path is present.
If not, click “New” button, add type the corresponding path.
Click “OK” button to close the windows and save the Path.
