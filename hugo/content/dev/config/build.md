+++
title = "Build"
weight = 30
+++

## Building as developer

As developer, configure CMake with
```
cmake .. -DDEV=ON
```
This activates additional build targets.
In particular, all contents of the `auto` directory is regenerated if out of sync.

#### Prerequisites

Developers need additional tools, beyond those required for building BornAgain from source:
- Swig, to regenerate `auto/Wrap`;
- Ruby, which provides the embedded interpreter erb, to regenerate `auto/Examples` and `auto/MiniExamples`;
- Doxygen, to autogenerate the API documentation;
- clang-format, to enforce proper source formatting.

Of these, clang-format is not controlled by `make`, but must be run manually.

Optionally, the following tools can be needed:
- Graphviz, to visualize project hierarchy with doxygen;
- hugo, to generate [web documentation](/dev/dev/webdoc).

{{% children  %}}
