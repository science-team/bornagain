+++
title = "Pyenv"
weight = 80
+++


## pyenv

[Pyenv](https://github.com/pyenv/pyenv) is a platform to install and manage
different versions of Python, isolated from the default Python version.
Its advantage compared to the other options, like Anaconda, is its simplicity and smallness.
Currently, to build BornAgain libraries for different versions of Python,
we use the `pyenv` platform.

#### Installation

##### Linux

Obtain the installer from <https://github.com/pyenv/pyenv-installer>.
To install `pyenv`, use
```
curl https://pyenv.run | bash
```
By default, `pyenv` is installed in `$HOME/.pyenv`.

The dependencies must be installed before building Python;
see <https://github.com/pyenv/pyenv/wiki#suggested-build-environment>:

```
sudo apt install build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev curl \
libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

##### MacOS

Install `pyenv` via `homebrew`:
```
brew install pyenv
```

The rest of the `pyenv` instructions are like those for Linux.


##### Windows

On MS-Windows, [pyenv-win](https://github.com/pyenv-win) should be used.

The available Python versions are distributed as _binary_, therefore
one must choose the version from a given list.

The Python environments are installed by default under `%USERPROFILE%\.pyenv\pyenv-win\versions`.

To obtain the `pyenv` root, enter the following in `cmd`:
```
echo %PYENV_ROOT%
```


#### Using pyenv

**Obtain a list of available Python versions**
```
pyenv install --list
```

**Obtain a list of available Python versions**
```
pyenv install --list
```

**Install a Python version**
To install a certain version, say 3.10, use:
```
pyenv install 3.10
```

**List installed versions**
```
pyenv versions
```

**Obtain the `pyenv` root folder where all versions are stored**
```
pyenv root
```
**Obtain the path to the current version of Python in pyenv**
```
pyenv which python3
```

**Install Python packages for a certain version of Python**

If the path to the Python executable is, for instance, `$HOME/.pyenv/versions/3.10.12/bin/python3`,
then to install `numpy` for this specific Python version, one should use
```
$HOME/.pyenv/versions/3.10.12/bin/python3 -m pip install numpy
```

#### Build BornAgain with a specific Python version

To build BornAgain with a specific Python version, the path to the Python platform should be included in `CMAKE_PREFIX_PATH` variable;
for instance, if Python 3.10.12 is installed in `$HOME/.pyenv/versions/3.10.12` folder, then `CMAKE_PREFIX_PATH` should include this path.
