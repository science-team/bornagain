+++
title = "Code analyses"
weight = 80
+++

## Some code analyses

#### GUI

Data and job view class hierarchy:

{{< figscg src="/img/auto/src_doc/data+job-view.svg" width="800px">}}

Qt graphics classes:

{{< figscg src="/img/auto/src_doc/qclasses.svg" width="800px">}}

Mask collaboration:

{{< figscg src="/img/auto/src_doc/mask-classes.svg" width="800px">}}

#### Python

Calls inside ba_plot:

{{< figscg src="/img/auto/src_doc/ba_plot.svg" width="800px">}}
