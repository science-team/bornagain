+++
title = "Debug"
weight = 60
+++

## Using gdb

If BornAgain GUI or a script crashes, re-run under gdb.

#### Scripts under pyenv

If Python is under pyenv control the command
```
gdb python
```
will not work. Instead, use
```
gdb -ex r --args bash python <script>
```
(source: https://stackoverflow.com/a/53007303/1017348)
