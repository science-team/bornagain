+++
title = "Manual checks"
weight = 20
+++

## Manual checks

Tests that cannot be automatized should occasionally be run manually.

#### Unused code

Unused code, in particular unused functions, can be detected with the tool
[xunused](https://github.com/mgehre/xunused).

```bash
cd build
cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
vi compile_commands.json # rm source files in 3rdparty and Fit/3rdparty
xunused compile_commands.json 2>&1 >/dev/null | tee /io/unused.txt
```

Use judgement: not all currently unused functions must be removed.

#### Static code analysis with clang-tidy

...

#### Coverage

...
