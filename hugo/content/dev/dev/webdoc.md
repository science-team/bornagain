+++
title = "Webdoc"
weight = 70
+++

## How to contribute to this documentation

Here we explain how to edit this documentation locally and submit changes for online publication.

### General information

This web site is generated using the static site generator [Hugo](https://gohugo.io/).
Sources are mostly writen in the MarkDown format.
Hugo translates them into HTML.

To submit improvements of the documentation proceed as follows:

### Install Hugo

Install Hugo either using a package manager,
or directly from upstream:
Download the archive file for your platform from
https://github.com/gohugoio/hugo/releases .
This archive contains a single binary.
Put it into a directory that is in your PATH.

### Run Hugo locally

Go to cloned repository and run Hugo.
```
$ cd BornAgain/hugo
$ hugo server -D
```

Hugo will print a line like
```
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
```

Point your web browser to that URL (most often `http://localhost:1313/`).
You should see a local copy of the BornAgain home page.
From there you should be able to navigate through the entire BornAgain documentation.

Keep the Hugo server running while modifying documentation source files.

### Modify the documentation

The documentation sources are in directory `hugo/content/`.
They consist of [Markdown](https://www.markdownguide.org/getting-started/) files.
For example, the source of the page you are looking at right now, is
```
content/developers-corner/contribute-to-documentation.md
```

Create a feature branch to work on:
```
$ git checkout -b my_feature
```
where `my_feature` should describe what you want to improve.

Edit any source file. Your local Hugo will then update the web site within a fraction of a second.

Whenever you made a significant change, commit it to your feature branch:
```
$ git commit
```

### Check all links

Run 
```
$ hugo server --disableFastRender
```
to create a website under an address like `http://localhost:1313/git-main/`.

If checkout from the older release branch `rVersion`, the address will be `http://localhost:1313/Version/`.

Here and below `Version` is the version number or "git-main".

Install [`linkchecker`](https://linux.die.net/man/1/linkchecker) (Debian package has same name). Run 
```
$ linkchecker http://localhost:1313/Version/
```
The operation may take few minutes.

Ignore the warnings. Resolve the errors. Submit MR against corresponding branch.


### Submit a merge request

Confine work to one topic. For other topics, start new feature branches.

When one feature branch is ready for review, push it to our git server:
```
$ git push
```
This requires developer access, which we will grant you upon informal request.

The terminal output will contain a message like
```
remote: To create a merge request for test, visit:
remote:   {{% url-jugit %}}/bornagain/-/merge_requests/new?merge_request%5Bsource_branch%5D=my_feature
remote:
```
Login to Jugit. Visit the indicated URL to open a Merge Request.


### Update visible pages

After the MR is merged, login to the web server and go to the hugo directory:
```
$ cd ba/Version/repo/hugo
```
Make sure you are on the right branch. Do
```
$ git pull --all
```
Run hugo. Check that the changes are online at `bornagainproject.org/Version`.

