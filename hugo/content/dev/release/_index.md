+++
title = "Release"
weight = 90
+++

## Release procedure

#### Test phase

- Decide whether it's a minor or major increment, and set version number in `CMakeLists.txt`
- Decide and set recommended_python_minor in `hugo/hugo.toml`
- Declare feature freeze
- Release new versions of library dependencies (heinz, ff) if they have changed
- Start release branch
- Update CHANGELOG
- Build packages, and test them
  - installation procedures according to webdocs
  - run Python scripts
  - [run and test GUI session](test)
- Start new branch in ba-www, and write release letter (`content/news/release-*.md`)

#### Finalize release

- Finalize bornagain (MRs against release branch):
  - Finalize CHANGELOG
  - In hugo/hugo.toml, update `baseURL`, `release_number`, `version_name`, `url_blob`
- After merger into release branch, set git tag v`x.y`

Finalize ba-www (in fresh release branch):
- Finalize release letter
- Update content/documentation.md
- Update static/js/version_switch
- In `hugo.toml`, update `version_mm`
- Hold back MR until everything else is ready

Upload installers and wheels (this makes the release irreversible):
- Build installers and wheels for all Python versions
- Installers to www@scgbuild:www/ext/ba/files/`version`.`patch`/
- Wheels to PyPI

#### Publish

- Create release in Jugit
- As www on @scg3
  ```
  cd ~/ba
  cp -pr <old_version>/repo <new_version>/repo; # for hotfix, use extant directory
  git fetch --all --prune
  git checkout <release_branch>
  git pull --all
  cd build
  cmake ..
  make
  ctest
  make figures
  cd ../hugo
  hugo
  cd ../..
  ln -si repo/hugo/public hugo-public
  cd ~/www/ba/public
  ln -si /home/www/ba/VERSION/hugo-public VERSION
  rm latest
  ln -si VERSION latest
  ```
- Merge release branch to ba-www => Docs will automatically be updated

- Inform users
  - Mail to BornAgain mailing list
  - For major milestones, mail to external mailing lists (neutron, sa_scat [unsigned mails only!])
  - Update "Latest" section on computing.mlz-garching.de

#### Continue development

- Merge changes back into `main`
  - fork new feature branch from `main`
  - merge release branch
  - restore "git-main" in `hugo/hugo.toml`
  - update version number in `CMakeLists.txt`
  - create MR against `main`
