+++
title = "Manual GUI test"
weight = 90
+++

## Manual GUI test

Repeat tests on all platforms.

Only importing samples from Python examples is worth repeating for different Python versions.

All steps are given in order of execution.

### Backward compatibility

- open projects from [Tests/GUIprojects]({{% url-src %}}/Tests/GUIprojects)
- for each project check that all parameters are loaded and represented properly
- check that jobs are loaded 
- rerun all jobs and ensure that the new simulation results look as expected
- repeat simulations for each instrument choosing it from the list in "Simulation" view
- close GUI

### Importing samples from Python

- install Python package
- open GUI application
- open new project
- go to "Sample" view and load one example from [specular]({{% url-src %}}/auto/Examples/specular) and one from [scatter2d]({{% url-src %}}/auto/Examples/scatter2d)

### Pure simulation

- switch to "Instrument" view
- create instances of "GISAS", "Offspecular", "Specular" and "Depthprobe" instruments  with default parameters
- switch to "Simulation" view
- create simulations:
  - with "scatter2d" example for "GISAS" and "Offspecular"
  - with "specular" example for "Specular" and "Depthprobe"
- switch to "Instrument" view
- add distributions:
  - lambda, alpha and phi to GISAS instrument
  - lambda, alpha to Specular instrument
- switch to "Simulation" view
- turn on simulation options "Average layer material for Fresnel calculations" and "Include specular peak"
- repeat simulations with distributions
- export simulations as script and run them in Python as is

### Data loading

- switch to "Data" view
- load 1D data from [ba-intern](https://jugit.fz-juelich.de/mlz/intern/ba-intern/-/blob/main/expdata/Reflectivity_from_Ni_film/Ni_LabCourse_R(Q).txt), units: 1/A
  - data should be loadable with default loader settings
  - look at the plot axes, labels, units, values should be correct
  - link 1D data to the "Specular" instrument, click "Yes, please modify instrument" if asked
  - switch to "Specular" instrument in "Instrument" view
  - check that scan points are picked from the loaded data and non-editable
  - open "Properties" panel and check that axis units are switchable after linking
- load 2D (REFSANS filter) data from [ba-intern](https://jugit.fz-juelich.de/mlz/intern/ba-intern/-/blob/main/expdata/REFSANS_example/p15101_00008744_8_16.csv)
  - switch to "Mask Editor" presentation and add a mask
  - link 2D data to the "GISAS" instrument, click "Yes, please modify instrument" if asked
  - open "Properties" panel and check that axis units are switchable after linking

### Simulation with data

- specular simulation:
  - switch to "Simulation" view
  - choose specular instrument with corresponding sample and 1D data
  - simulate
  - check that both curves are given at the same argument points
- GISAS simulation:
  - switch to "Simulation" view
  - choose GISAS instrument with corresponding sample and 2D data
  - simulate
  - check that all color maps have the same range and scale
  - check that mask is present in both simulation and loaded data 

### Save and load

- save the project
- close GUI
- open GUI
- open the project
- check that everything is loaded correctly

