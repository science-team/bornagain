+++
title = "Fitting along slices"
weight = 20
+++

## Fitting along slices

Here we demonstrate how to fit along slices. The idea is that the user defines the positions of vertical and horizontal lines crossing the detector plane in regions of most interest (Yoneda wings, Bragg peaks, etc.) and then finds the sample parameters which fits those regions best.

Such an approach uses much less CPU while still giving a chance to find the optimal sample parameters. In general, however, it is arguable whether fitting along slices makes more sense than fitting using the whole detector image.

Technically, the idea is to mask the whole detector except thin lines, one vertical and one horizontal, representing slices. This will make the simulation and the fitting procedure to calculate only along these indicated slices.

* In the given script we simulate a dilute random assembly of cylinders on a substrate.
The fitting procedure looks for the cylinder's height and radius.
* Function `get_masked_simulation` masks the entire detector except for a horizontal and a vertical stripe.
* The custom `PlotObserver` class, which plots the fit along the slices
at every 10th fit iteration.

{{< galleryscg >}}
{{< figscg src="/img/draw/fit_along_slices.png" width="600px" caption="Fit window">}}
{{< /galleryscg >}}

{{< show-ex file="fit/scatter2d/fit_along_slices.py" >}}
