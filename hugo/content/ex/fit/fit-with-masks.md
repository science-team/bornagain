+++
title = "Fitting with masks"
weight = 10
+++

## Fitting with masks

In this example we demonstrate how to mask certain areas on the detector image to exclude their influence on the fitting procedure.  This can be done by invoking the method `addMask` on a simulation object.

```
simulation = ScatteringSimulation()
simulation.addMask(Rectangle(x1, y1, x2, y2), mask_value)
```

where `Rectangle` is related to the shape of the mask in detector coordinates, `mask_value` can be either `True` (area is excluded from the simulation and fit) or `False` (area will stay in the simulation and will be taken into account in $\chi^2$ calculations during the fit). There can be an arbitrary number of masks of various shapes added to the simulation one after another. Each subsequent mask overrides the previously defined `mask_value` in the given area.

* In the given script we simulate a dilute random assembly of cylinders on a substrate.
The fitting procedure looks for the cylinder's height and radius.
* To demonstrate ways of setting complex masks,
the simulated image looks like a Pac-Man from the ancient arcade game.

{{< galleryscg >}}
{{< figscg src="/img/draw/fit_with_masks.png" width="600px" caption="Fit window">}}
{{< /galleryscg >}}

{{< show-ex file="fit/scatter2d/fit_with_masks.py" >}}
