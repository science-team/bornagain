+++
title = "Minimizer settings"
weight = 30
+++

## Minimizer settings

This example demonstrates how to change the minimizer algorithm and some of its settings. For example, the following lines

```python
minimizer = ba.Minimizer()
minimizer.setMinimizer("Minuit2", "Migrad", "MaxFunctionCalls=500;Strategy=2")
```

will set the internal minimizer to "Minuit2", its internal algorithm to "Migrad" and then pass additional options, limiting the maximum number of calls and an internal minimization strategy.

The list of available minimizers and their options can be seen with

```python
print(ba.MinimizerFactory().catalogueDetailsToString())
```


For more information, see the
[minimizer settings tutorial](/ref/fit/minimizers).

{{< show-ex file="fit/scatter2d/minimizer_settings.py" >}}
