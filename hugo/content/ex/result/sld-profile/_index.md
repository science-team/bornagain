+++
title = "SLD profile"
weight = 50
+++

### Scattering length density (SLD) profile

The scattering length density (SLD) profile of a BornAgain sample model
can be retrieved with the function `materialProfile` from the BornAgain
submodule `sample_tools`.

The submodule can be accessed through the statement
```
from bornagain import sample_tools.
```

The function can be called as
```
zpoints, slds = sample_tools.materialProfile(sample, n)
```
with the input arguments
- `sample` is a user-defined sample model,
- `n` is the number of points on the z axis for which the SLD is to be computed

and the output values
- `zpoints`, an array of z values,
- `slds`, an array of complex SLD values at the depths given by `zpoints`.

The output can be plotted e.g. with the MatPlotLib command
```
matplotlib.pyplot.plot(zpoints, np.real(slds)).
```

For full examples, see
{{% children %}}
