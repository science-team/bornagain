+++
title = "SLD profile with a sliced particle layer"
weight = 30
+++

### SLD profile with a sliced particle layer

This example shows how to plot a z dependent Scattering Length Density (SLD) profile
caused by a dense assembly of conical particles.

{{< figscg src="/img/auto/varia/MaterialProfileWithParticles.png" width="500px" >}}

The staircase profile comes from slicing of the vacuum layer that accounts for the z dependent
particle density. The slicing is specified by the statement
```
ambient_layer.setNumberOfSlices(20).
```

{{< show-ex file="varia/MaterialProfileWithParticles.py" >}}
