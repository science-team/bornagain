+++
title = "Uncorrelated mixture"
weight = 78
+++

### Uncorrelated mixture of particles

Scattering from a mixture of cylinders and prisms without interference.

* The sample comprises a substrate on which are deposited, in equal proportion, cylinders and prisms.
* All particles are made of the same material.
* Each type of particle has the same orientation.
* The cylinders are $5$ nm high and $5$ nm in radius.
* Each prism is $5$ nm high with an equilateral triangular base, whose side length is equal to $10$ nm.
* There is no interference between the waves scattered by these particles. The distribution is therefore diluted.
* The incident neutron beam is characterized by a wavelength of 0.1 nm.
* The incident angles are $\alpha\_i = 0.2 ^{\circ}$ and $\varphi\_i = 0^{\circ}$.
* The simulation is performed using the Distorted Wave Born Approximation (due to the presence of a substrate).

{{< galleryscg >}}
{{< figscg src="/img/draw/CylindersAndPrisms_setup.jpg" width="350px" caption="Real-space model">}}
{{< figscg src="/img/auto/scatter2d/CylindersAndPrisms.png" width="350px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/CylindersAndPrisms.py" >}}
