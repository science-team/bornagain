+++
title = "Rotation example"
weight = 31
+++

### Rotated Pyramids

Scattering from a monodisperse distribution of rotated pyramids.

This example illustrates how the in-plane rotation of non-radially symmetric particles influences the scattering pattern.

* The sample is made of pyramids deposited on a substrate.
* Each pyramid is characterized by a squared-base side length of $10$ nm, a height of $5$ nm, and a base angle $\alpha$ equal to $54.73^{\circ}$.
* These particles are rotated in the $(x, y)$ plane by $45^{\circ}$.
* There is no interference between the scattered waves.
* The wavelength is equal to 0.1 nm.
* The incident angles are $\alpha\_i = 0.2 ^{\circ}$ and $\varphi\_i = 0^{\circ}$.

{{< galleryscg >}}
{{< figscg src="/img/draw/RotatedPyramids_setup.jpg" width="350px" caption="Real-space model">}}
{{< figscg src="/img/auto/scatter2d/RotatedPyramids.png" width="350px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/RotatedPyramids.py" >}}
