+++
title = "Roughness"
weight = 50
+++

## Script examples: rough interfaces

{{< figscg src="/img/draw/CorrelatedRoughness_setup.jpg" width="350px" caption="Multilayer sample model">}}

{{% children %}}
