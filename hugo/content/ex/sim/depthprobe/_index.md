+++
title = "Depth probe"
weight = 80
+++

## Depth probe examples

Intensity as function of incident angle $\alpha_\text{i}$ and depth $z$.

Class reference:
- [DepthProbe](/ref/sim/class/depthprobe)

Examples
- [Basic example: thin film on substrate](/ex/sim/depthprobe/depthprobe1)
- [Transmitted amplitude modulus](/ex/sim/depthprobe/transmitted_modulus)
- [Resonator](/ex/sim/depthprobe/resonator)
