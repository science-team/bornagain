+++
title = "Resonator"
weight = 80
+++

## Intensity distribution in neutron resonator

We consider a neutron resonator, composed of one Ti/Pt bilayer.

{{< figscg src="/img/draw/ResonatorSample.png" width="500" class="center">}}

The beam comes from the Si side.
By convention the beam always comes ''from above''.
Accordingly, we consider Si the ''ambient'' material, placed ''on top'' of the sample.

As a result, we obtain the neutron intensity
as function of depth and incident angle $\alpha_i$.

{{< figscg src="/img/auto/varia/Resonator.png" width="500" class="center">}}

{{< show-ex file="varia/Resonator.py" >}}
