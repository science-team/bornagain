+++
title = "SAS"
weight = 40
+++

## Simulation types: Small-angle scattering

While BornAgain is designed for GISAS experiments (using the Distorted Wave Born
Approximation), it naturally also contains the regular (plane wave) Born Approximation.
Accordingly, BornAgain can also simulate standard small-angle scattering (SAS).

However, there exist
[several other specialized SAS softwares](http://smallangle.org/content/software).
Therefore we do not advertise BornAgain for analysing SAS experiments,
and in general we do not provide user support for this application domain.
We rather recommend [SASView](https://www.sasview.org/),
which is institutionally supported by the European Spallation Source,
and was designated as standard SAS software in the European SINE2020 project.

Yet BornAgain can be an appropriate choice in cases where the sample structure or
the experimental conditions are not covered by other software.
For example, other softwares provide no, or limited, support for polarized SANS.
Here, we show how such experiments can be simulated with BornAgain.

### Basic example

To simulate conventional small-angle scattering with BornAgain,
we turn source and detector by 90 degrees with respect to grazing-incidence geometry.

The following basic example simulates
scattering from a dilute random assembly of monodisperse, oriented dodecahedra.


{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/DodecahedraSAS.png" width="500px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/DodecahedraSAS.py" >}}
