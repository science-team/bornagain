+++
title = "GUI overview"
weight = 20
+++

## GUI overview

The basic features of the interface are explained below.

   * [Projects View](#projects-view)
   * [Instrument View](#instrument-view)
   * [Sample View](#sample-view)
   * [Simulation View](#simulation-view)
   * [Jobs View](#jobs-view)

### Projects view

When you start BornAgain GUI, you will be presented with the Projects View, where you can

   * Create new projects
   * Open recent projects


{{< figscg src="/img/draw/gui_projectsview_l.png" width="800px" class="center" >}}

You can use the view selector located on the left vertical panel (1) to change to one of the following views.


|  |  |
|:--|:--|
| {{< figscg src="/img/draw/main_projectsview.png" width="50px" class="center" >}} | Switch to Projects View |
| {{< figscg src="/img/draw/main_instrumentview.png" width="50px" class="center" >}} | Define the instrument geometry: beam and detector parameters |
| {{< figscg src="/img/draw/main_sampleview.png" width="50px" class="center" >}} | Build the sample |
| {{< figscg src="/img/draw/main_importview.png" width="50px" class="center" >}} | Import experimental data to fit |
| {{< figscg src="/img/draw/main_simulationview.png" width="50px" class="center" >}} | Run a simulation |
| {{< figscg src="/img/draw/main_jobview.png" width="50px" class="center" >}} | Switch to see job results, tune parameters in real time, fit the data |
|  |  |


### Instrument View

The Instrument View is used to create new scattering instruments and adjust their settings. The view consists of the instrument selector located on the left (1) and the instrument settings window located on the right (2).

{{< figscg src="/img/draw/gui_instrumentview_l.png" width="800px" class="center">}}

On the instrument settings window (2) you can modify the settings of the currently selected instrument:

   * The name of the instrument
   * The beam parameters
   * The detector parameters
   * Parameters for polarization analysis
   * Background


### Sample View

The Sample View allows you to design the sample via a graphical interface. For simplicity, one can start with a predefined sample from the Examples menu.

{{< figscg src="/img/draw/gui_sampleview_l1.png" width="800px" class="center">}}

Here, the "Rotated pyramids" predefined sample is selected. The model consists of two layers, “Vacuum” and “Substrate”,
which are distinguished by colors. A manipulatable 3d graphical representation of the sample is displayed in the Real Space pane.

{{< figscg src="/img/draw/gui_sampleview_l2.png" width="800px" class="center">}}

The defining parameters of each layer can be shown and modified by expanding the layer.

{{< figscg src="/img/draw/gui_sampleview_l3.png" width="800px" class="center">}}

The layers can be moved up or down, or removed from the sample using the set of buttons on the right (denoted by an orange rectangle).
Note that vertical order of layers represent the same order in the real space
(for instance, in the image below, `Layer 0` is on top of `Layer 1`).

The color of the layer can also be changed with these buttons.

The 3d representation of each layer can be shown separately by clicking the 3D button for each layer (denoted by green rectangles).
To display the 3d representation of the full sample, press the 3d button of the Sample.

{{< figscg src="/img/draw/gui_sampleview_l4.png" width="800px" class="center">}}

The Python script corresponding to the sample can be displayed by checking `View`→`Python Script`.

{{< figscg src="/img/draw/gui_sampleview_l5.png" width="800px" class="center">}}

* Sample construction

To construct a new sample from scratch, one should use the "New" button and then add a layer
by clicking the “Add layer” button.

{{< figscg src="/img/draw/gui_sampleview_create1.png" width="800px" class="center">}}

The material of the layer can be changed via the material editor. Note that the default material is _vacuum_.

{{< figscg src="/img/draw/gui_sampleview_create2.png" width="800px" class="center">}}

The interference function (particle layout) can be selected via the Type drop-down menu.

{{< figscg src="/img/draw/gui_sampleview_create3.png" width="800px" class="center">}}

The type of the particles can be selected via the “Add particle” menu.

{{< figscg src="/img/draw/gui_sampleview_create4.png" width="800px" class="center">}}

In the same way, the user can create an arbitrary number of samples, each with an arbitrary number of layers. If this is the case, during the configuration of the simulation the user will have to choose which sample for the simulation. The sample is considered as valid for the simulation, if it contains at least one layer.

### Simulation View

The Simulation View contains the following parts

   * The Data selection box for selecting the instrument and the sample to simulate (1)
   * The Calculation parameters box for changing the calculation parameters (2)
   * The Execution parameters box for changing the execulation parameters (3)
   * The “Run Simulation” and “Export to Python Script” buttons

{{< figscg src="/img/draw/gui_simulationview_l1.png" width="800px" class="center">}}

The names of the defined instruments and samples are displayed in the Data Selection box (1). There, the user can select a combination for running a simulation.

Clicking on the “Run Simulation” button immediately starts the simulation. When completed, the current view is automatically switched to the Jobs View showing the simulation results. This behaviour can be modified by changing to “Run in Background” in (3).

### Jobs View

The Jobs View displays the results of the simulation. It has two different presentations called

   * [Job View Activity](#job-view-activity)
   * [Real Time Activity](#real-time-activity)

Job View Activity is shown by default.

#### Job View Activity

The layout of the Job View Activity consists of five elements

   * The “Job Selector” widget (1) for selecting the specific job to be displayed
   * The “Job Properties” widget (2) contains basic information about the currently selected job
   * The intensity data widget (3) shows the intensity data of the currently selected job
   * The toolbar (4) contains a set of control elements for the job selector and the intensity data widgets. In the right corner of the toolbar one can switch to the Projections widget.
   * The “View” menu allows switching between “Job View”, “Real Time View” and “Fitting” activities (or panels).

{{< figscg src="/img/draw/gui_jobview_l1.png" width="800px" class="center">}}

> The two completed jobs can be seen in the job selector widget (1), with `job2` currently selected and displayed.

{{< alert theme="info" >}}
 The intensity image in widget (3) offers several ways of interaction:

   * Using the mouse wheel to zoom in and out
   * Dragging the color legend bar on the right of the image to change the min, max range of the z-axis
   * The toolbar (4) on top of the intensity data widget gives access to more options via “Fourier” or “Properties” (left buttons) and “Heat Map/Projections/Fit” (right drop-down menu)
{{< /alert >}}


##### Projections

The Projections panel is accessible from the drop-down menu on the upper right-hand side of the Jobs View.

{{< figscg src="/img/draw/gui_jobview_projections_menu.png" width="800px" class="center">}}

Horizontal or vertical projections can be added to the image via the buttons on the toolbar on the right-hand side.
The projection plots will be displayed in the pane below the image; note the orange rectangles.

The projections can be selected via the arrow icon (denoted by a green rectangle). The selected projection can be moved using arrow keys or the mouse.
It can be deleted by pressing the `Delete` key.

{{< figscg src="/img/draw/gui_jobview_projections.png" width="800px" class="center">}}


#### Real Time Activity

The “Real Time Activity” panel can be switched on by `View` → `Real Time Activity`.

{{< figscg src="/img/draw/gui_jobview_realtime_l1.png" width="800px" class="center">}}

Switching to “Real Time Activity” will display a parameter tree (denoted by an orange rectangle)
which represents the set of all parameters that have been used during the construction of the scattering instrument and the sample. Each displayed parameter value can be adjusted using a slider. The simulation will run in the background and the intensity image will be constantly updated reflecting the influence of the given parameter on the simulation results.

{{< figscg src="/img/draw/gui_jobview_realtime_l2.png" width="800px" class="center">}}

{{< alert theme="info" >}}
**Note**

The Real Time View works smoothly only for simple geometries, when the simulation requires fractions of a second to run. For more complex geometries, demanding more CPU power, the user will see a progress bar and any movements of the slider will not have a direct influence on the produced image (Intensity Data widget). In this case the user may try to speed up the simulation by decreasing the number of detector channels in the Instrument View and submitting a new job by running the simulation from the Simulation View.

{{< /alert >}}

{{< alert theme="warning" >}}
**Important**

The jobs in the Jobs View are completely isolated from the rest of the program. Any adjustments of the sample parameters in the Sample View or the instrument parameters in the Instrument View won't have any influence on the jobs already completed or still running in the Jobs View. Similarly, in “Real Time Activity” mode, any parameter adjustments made in the parameter tree will not be propagated back into the Sample or Instrument Views.

{{< /alert >}}
