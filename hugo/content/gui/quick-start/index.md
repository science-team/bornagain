+++
title = "Quick Start"
weight = 10
+++

## Quick Start

This tutorial demonstrates how to run the simulation and get the simulated intensity data image in a few mouse clicks.

### 1. Run BornAgain graphical user interface (GUI)
{{< figscg src="/img/draw/BornAgain_48x48.png" width="48px" class="float-left" >}}
Use the BornAgain icon located on the desktop (Windows) or on Launchpad (MacOS) to start the GUI.

For Linux users: run an executable located in BornAgain installation directory from the command line:
```bash
$ <install_dir>/bin/bornagain
```

When you start the BornAgain GUI, you will be presented with the Projects panel.
Click on the "New Project" button to create a new project.

{{< figscg src="/img/draw/gui_quickstart_projects.png" width="800px" class="center">}}

### 2. Select and configure an instrument

Switch to Instrument panel to select and configure an instrument; e.g. GISAS.
Upon selection, the detailed modifiable definition of the instrument is displayed in the instrument pane.

{{< figscg src="/img/draw/gui_quickstart_instruments.png" width="800px" class="center">}}

### 3. Create and configure a sample

Switch to Sample panel to create and/or modify a sample. A set of predefined samples are available from the Examples menu.

{{< figscg src="/img/draw/gui_quickstart_sample.png" width="800px" class="center">}}

Upon selection, the detailed modifiable definition of the sample is displayed in the sample pane. In the image below, "Core shell particles" is selected as the sample.

{{< figscg src="/img/draw/gui_quickstart_sample_coreshell.png" width="800px" class="center">}}

### 4. Run the simulation

Switch to the Simulation panel and press the "Run Simulation" button to execute the simulation.

{{< figscg src="/img/draw/gui_quickstart_simulation.png" width="800px" class="center">}}

When the simulation is completed, the current view will be automatically switched to the Jobs panel which shows the results of the simulation.

{{< figscg src="/img/draw/gui_quickstart_jobs.png" width="800px" class="center">}}
