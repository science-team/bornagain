+++
title = "Cite BornAgain"
weight = 100
+++

# How to cite BornAgain

## Generic reference

The canonical reference for BornAgain is the journal article:
> Gennady Pospelov, Walter Van Herck, Jan Burle, Juan M. Carmona Loaiza, Céline Durniak, Jonathan M. Fisher, Marina Ganeva, Dmitry Yurov and Joachim Wuttke:<br>
> *BornAgain*: software for simulating and fitting grazing-incidence small-
angle scattering.<br>
> [J. Appl. Cryst. 53, 262–276 (2020)](https://doi.org/10.1107/S1600576719016789)

When using release 1.17 or later, also cite the update
> Ammar Nejati, Mikhail Svechnikov and Joachim Wuttke:<br>
> BornAgain, software for GISAS and reflectometry: Releases 1.17 to 20.<br>
> [EPJ Web Conf. 286, 06004 (2023)](https://doi.org/10.1051/epjconf/202328606004)

Use of the software should additionally be documented by citing a specific version
thereof:<br>
> BornAgain — Software for simulating and fitting X-ray and neutron small-angle scattering at grazing incidence, version ⟨version_number⟩ (⟨release_date⟩), https://www.bornagainproject.org

Material from the documentation and the entire web site
may be shared and reused under the terms of the of the Creatice Commons license
[CC BY](https://creativecommons.org/licenses/by/4.0/).

## Special papers

To be cited in special context only.

#### Deployment

> A. Nejati, M. Svechnikov, J. Wuttke:<br>
> Deploying a C++ Software with (or without) Python Embedding and Extension.<br>
> Proceedings of the 4th conference for research software engineering in Germany, deRSE24.<br>
> [Electronic Communications of the EASST 83 (2025)](https://doi.org/10.14279/eceasst.v83.2596)

#### Software engineering principles

> J. Wuttke, S. Cottrell, M. A. Gonzalez, A. Kaestner, A. Markvardsen, T. H. Rod, P. Rozyczko, G. Vardanyan:<br>
> Guidelines for collaborative development of sustainable data treatment software.<br>
> [J. Neutron Res. 24, 33-72 (2022)](https://doi.org/10.3233/JNR-220002)

#### Polyhedral form factors

> Joachim Wuttke:<br>
> Numerically stable form factor of any polygon and polyhedron.<br>
> [J. Appl. Cryst. 54, 580-587 (2021)](https://doi.org/10.1107/S1600576721001710)
