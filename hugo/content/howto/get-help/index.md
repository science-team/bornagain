+++
title = "Request help or report bugs"
weight = 90
+++


Let us know if you encounter a problem installing or running BornAgain,
or if you have a question about BornAgain models or numerics.

You may contact us in private by [email](mailto:contact@bornagainproject.org),
or in public by submitting an issue to our [issue tracker]({{% url-issues %}}).
External users need to self-register to get write access to our git manager
Jugit, as described at
[computing.mlz-garching.de/tech/jugit-login](https://computing.mlz-garching.de/tech/jugit-login).


Public conversation has the advantage that solved issues, over time,
will form a worldwide readable, and searchable, data base.

<div class="card border-info mb-4 mx-auto" style="max-width:500px">
    <div class="card-header">Use the issue tracker to</div>
    <div class="card-body">
    <ul>
<li> report bug
<li> request help with problems in installing or running BornAgain
<li> propose a new feature
<li> ask a question about BornAgain API or model implementations
</ul>
  <a class="btn btn-secondary" href="{{% url-issues %}}">Create issue</a>
  </div>
</div>

Especially for bug reports, supply all relevant information that may help
us to analyse the issue. In particular, submit the full output of the last,
unsuccessful command. Provide a screen copy, or use the redirection `[command] >& [logfile]`.

Note that in all installers, a detailed BornAgain build log is included
under the `share` folder for further info and debugging.
Please send this build log along with your request.

We very much welcome your feedback!
