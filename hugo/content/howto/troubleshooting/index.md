+++
title = "Solve known problems"
weight = 60
+++

## How to solve known problems

* [Linux specific](#linux-specific)
  * [BornAgain GUI starts but shows a blank window](#bornagain-gui-starts-but-shows-a-blank-window)
* [Windows specific](#windows-specific)
  * [Missing api-ms-win-crt-runtime-l1-1-0.dll](#missing-api-ms-win-crt-runtime-l1-1-0-dll)
* [MacOS specific](#macos-specific)
  * [Incompatible library version of libgsl.0.dylib](#a-incompatible-gsl)
* [Building from source on Linux](#building-from-source-on-linux)
  * [Configuration (cmake) fails](#configuration-cmake-fails)
  * [Qt5 configuration problems](#qt5-configuration-problems)
  * [Compilation (make) fails](#compilation-make-fails)
* [Common](#common)
  * [Functional test (ctest) fails](#functional-test-ctest-fails)
  * [Fatal Python error: PyThreadState_Get: no current thread](#a-no-current-thread)

<hr>

### Linux specific

#### BornAgain GUI starts but shows a blank window

This problem is observed with Anaconda Python platform and is related to the required `GLIBCXX` version (`GLIBCXX_3.4.30`).

Scan `libstdc++` of the conda environment for the list of the compatible `libc` versions to see if `GLIBCXX_3.4.30` is included:
```
$ conda_env_path=<path to conda>/envs/<conda env name>
$ strings $conda_env_path/lib/libstdc++.so.6 | grep GLIBCXX_3.4.30
```

If this reveals that `GLIBCXX_3.4.30` is not included in the list of the compatible `libc` version, then find the `libstdc++` of the _system_ via
```
$ ldconfig -p | grep libstdc++.so
```

and scan the system `libstdc++` for the list of the compatible `libc` versions to see if `GLIBCXX_3.4.30` is included:
```
$ strings /usr/lib/x86_64-linux-gnu/libstdc++.so.6 | grep GLIBCXX_3.4.30
```

If `GLIBCXX_3.4.30` is included in the list of compatible versions, then set `LD_LIBRARY_PATH` environment variable to use the  `libstdc++` of the system when running the BornAgain GUI:
```
$ export LD_LIBRARY_PATH=/lib/x86_64-linux-gnu/:$conda_env_path/lib:
$ bin/bornagain
```

### Windows specific

#### Missing api-ms-win-crt-runtime-l1-1-0.dll

On some Windows machines, you might get the message "The program can't start because api-ms-win-crt-runtime-l1-1-0.dll is missing from your computer". You then have to install the Visual C++ Redistributable for Visual Studio 2015. See for example the discussion at [stackoverflow](http://stackoverflow.com/questions/33265663/api-ms-win-crt-runtime-l1-1-0-dll-is-missing-when-open-office-file).

Before installing the Visual C++ Redistributable, install all Windows updates. Then download and install the [Visual C++ Redistributable from Microsoft](https://www.microsoft.com/en-us/download/details.aspx?id=48145).

<hr>

### MacOS specific

{{% anchor "a-incompatible-gsl" %}}

##### Incompatible library version of libgsl.0.dylib

When Mantid is installed on the system, BornAgain might conflict with its libraries if the path to Mantid is specified in the `DYLD_LIBRARY_PATH` and/or `PYTHONPATH` environment variables. This conflict can occur while running any of the BornAgain Python examples or just during the import of BornAgain into Python, with an error message similar to:

```bash
$ python -c "import bornagain"

Incompatible library version: libBornAgainSim.so requires
version 18.0.0 or later, but libgsl.0.dylib provides version 16.0.0
```

Make sure that the path to the Mantid folders is not defined in your `DYLD_LIBRARY_PATH` and/or `PYTHONPATH` variables by running the commands `echo $DYLD_LIBRARY_PATH` and `echo $PYTHONPATH`. If this is the case, unset the given variables to prevent the MacOS loader from loading Mantid

```bash
$ unset DYLD_LIBRARY_PATH; unset $PYTHONPATH
```

This should resolve the issue.

{{< notice tip >}}
To see which libraries get loaded by the MacOS loader during the import of BornAgain, use

```bash
$ export DYLD_PRINT_LIBRARIES=1
$ python -c "import bornagain"
```
{{< /notice >}}

<hr>

### Building from source on Linux

##### CMake configuration phase fails

To accelerate the build process, `cmake` maintains a cache. The downside is that this cache can cause false positives (erroneous error reports) and false negatives (`cmake` overlooking missing dependencies). Unless `cmake` has been run for the first the time in a fresh build directory, troubleshooting should start by removing the cache

```bash
$ rm CMakeCache.txt
```

and running `cmake` anew. If that does not help, then empty the build directory completely, and run `cmake` yet again.

Next, carefully read the error message, which will have the following form:

```
CMake Error at [cmake module]:[line number] (message):
  [specific error message]
Call Stack (most recent call first):
  [cmake module]:[line] ([command])
  ...
-- Configuring incomplete, errors occurred!
See also "[build_dir]/CMakeFiles/CMakeOutput.log".
See also "[build_dir]/CMakeFiles/CMakeError.log".
```

The most frequent error is a missing dependence because a third party library not installed on the system, or not found by `cmake`.

If you require our support, then please rerun `cmake` in an empty build directory and provide a full copy of its output, most easily generated by output redirection

```bash
$ cmake [<options>] <source_dir> >& my_cmake.log
```

Besides the so obtained my_cmake.log also send us three other log files produced by `cmake` in the build directory:

```
CMakeCache.txt
CMakeFiles/CMakeOutput.log
CMakeFiles/CMakeError.log
```

<hr>

#### Compilation phase fails

In the case of a complex system setup, with libraries of different versions scattered across multiple places, `cmake` might fail in identifying the correct paths to the libraries or include directories. This might lead at some point to a compilation failure. This might happen, for example, during the compilation of the `BornAgain` graphical user interface if the system has both Qt5 and Qt6 libraries installed. Please send us the build log generated by running the following make command

```bash
$ make VERBOSE=1 >& my_build.log
```

<hr>

### Common

#### Functional test (ctest) fails

{{< notice note >}}
A frequent cause for errors at this stage is a conflict with older versions of `BornAgain` installed on the system. Make sure the `LD_LIBRARY_PATH` and `PYTHONPATH` variables do not contain the older installation location and run `ctest` again.
{{< /notice >}}

If `ctest` fails, then rerun it without the multi-core option `-j[N]`. This yields clearer, sorted output, and allows to rule out errors due to thread-safety violations.

Error messages from `ctest` have the form

```
The following tests FAILED:
    53 - CoreSuite/HexParacrystal (Failed)
   123 - PySuite/HexParacrystal (Failed)
For details, see the log files in Testing/Temporary/
More info at ...
Errors while running CTest
```

To analyse these errors one by one, it is convenient to run `ctest` on a single test:

```bash
$ ctest -V -R PySuite/HexParacrystal
```

Then analyse the output in `Testing/Temporary/LastTest.log`. Alternatively, set the environment variable `CTEST_OUTPUT_ON_FAILURE=1` to get full terminal output from the failed tests. Yet another alternative would be to run the test binary directly from `<build_dir>/bin`.

When reporting errors to us, please run just the plain `ctest` without options `-j` or `-R`, and submit `Testing/Temporary/LastTest.log`.

<hr>

{{% anchor "a-no-current-thread" %}}
#### "Fatal Python error: PyThreadState_Get: no current thread"

This error shows up when a Python module tries to use a Python library that is different than the one the interpreter uses. This might happen if the user's system contains several  Python installations.

For example, BornAgain was compiled against `/usr/lib64/libpython3.10.so` and then used by mistake with a Python interpreter from another installation. This interpreter might depend on his own version of `libpython3.10.so`  located, for example, at `/usr/local`. Importing of the bornagain module into this interpreter will cause the loading of two different `libpython3.10.so` libraries: one will be loaded by the bornagain module, the other will be loaded by the interpreter itself. This will cause the given fatal Python error. Make sure you are running the correct Python interpreter and that your `PYTHONPATH` and `DYLD_LIBRARY_PATH` doesn't contain the old BornAgain installation.
