+++
title = "Introduction"
weight = 10
+++

## Introduction

BornAgain is an open-source research software to simulate and fit neutron and x-ray reflectometry,
off-specular scattering, and small-angle scattering (SAS),
especially under grazing incidence (GISAS).

Its name, BornAgain, alludes to the central role of the distorted-wave
Born approximation in the physical description of the scattering
process.  The software provides a generic framework for modeling
multilayer samples with smooth or rough interfaces and with various
types of embedded nanoparticles.

{{< figure src="/img/draw/welcome_640.png" class="center">}}

BornAgain almost completely reproduces the functionality of the widely
used program IsGISAXS ([Lazzari 2002](/lit/lazzari2002), [Lazzari 2006](/lit/lazzari2006)).
BornAgain goes beyond IsGISAXS by supporting an
unrestricted number of layers and particles, diffuse reflection from
rough layer interfaces, particles with inner structures, neutron
polarization and magnetic scattering.  Thanks to a consequential
object-oriented design, BornAgain provides a solid base for future
extensions in response to specific user needs.

BornAgain is a multi-platform software, with active support for Linux,
MacOS and Microsoft Windows. It is free and open source, available
under the GNU General Public License (GPL v3 or higher).

For a broad overview, see the journal article
[J. Appl. Cryst. 53, 262–276 (2020)](https://doi.org/10.1107/S1600576719016789),
which is also the primary means to [cite BornAgain](/howto/cite)
in scientific publications.

{{< alert theme="info" >}}
**Disclaimer**

Software and documentation are work in progress.  While basic GISAS
simulations have been validated in many ways, we can never exclude
that some fringe cases are implemented incorrectly.  Up to the user
to convince themselves that results are physically plausible.

If in doubt, please [contact us]({{% ref-home "contact" %}}).
We are grateful for all kind of feedback: criticism,
praise, bug reports, feature requests or contributed modules. If
questions go beyond normal user support, we will be glad to discuss a
scientific collaboration.

{{< /alert >}}
