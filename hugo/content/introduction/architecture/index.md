+++
title = "Architectural overview"
weight = 20
+++

## Architectural overview

BornAgain is a multi-platform framework available in Windows, MacOS and Linux.
The C++ code, all configuration files, tests and Python scripts are all open-source,
available at {{% url-jugit %}}/bornagain.

##### General Structure

The framework consists of shared libraries and a standalone graphical user
interface, all written in C++. Thanks to the Python bindings the libraries can be imported
into Python as external modules.

BornAgain depends on a few external and well established open-source
libraries: [boost](http://www.boost.org/), [GNU Scientific
Library](http://www.gnu.org/software/gsl/), [Fast Fourier
Transformation](http://www.fftw.org/) and
[Qt6](http://www.qt.io/developers/) libraries. They must be installed
on the system to run BornAgain on Unix platforms. In the case of
Windows and MacOS they are added to the system automatically during
the installation of BornAgain.

Other dependencies are included in the source archive
under {{% ref-src "3rdparty" %}} to facilitate deployment and installation.

##### Object Oriented Approach in Simulation Description

Dynamic class hierarchies can be built on the fly to represent complex
sample structures.
For example, to simulate the scattering from a mixture of cylinders
and prisms deposited on a substrate, the following tree of objects has
to be created.

{{< figure src="/img/draw/nodes_architecture4.png" class="center">}}

The parent Sample object represents the sample and contains three
children: the semi-infinite air layer, the semi-infinite substrate
layer, and the interface between them. The air layer contains the
so-called ParticleLayout object, which holds information about the
particles populating the layer and the interference between them. Each
particle is fully defined via its children: the material the particle
is made of and the form factor representing the particle's shape.

In the graphical user interface, the sample editor provides a tree view
of the sample model. In the figure below, this editor is contrasted
with an equivalent Python script.
It is possible to export and import Python scripts from and to the GUI.

{{< figure src="/img/draw/nodes_architecture5.png" class="center">}}

GUI and Python API are explained in details in the following sections of this documentation.
