+++
title = "Babonneau2010"
+++

D. Babonneau (2010):

FitGISAXS: software package for modelling and analysis of GISAXS data using IGOR Pro.

[J Appl Cryst 35, 406-421](https://doi.org/10.1107/S0021889810020352).
