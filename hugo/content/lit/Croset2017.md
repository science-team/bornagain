+++
title = "Croset2021"
+++

Bernard Croset (2017):

Form factor of any polyhedron: a general compact formula and its singularities.

[J Appl Cryst 50, 1245-1255](https://doi.org/10.1107/S1600576717010147).
