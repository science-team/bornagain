+++
title = "Lazzari2002"
+++

R. Lazzari (2002):

IsGISAXS: a program for grazing-incidence small-angle X-ray scattering analysis of supported islands

[J. Appl. Cryst. 35, 406-421](https://doi.org/10.1107/S0021889802006088).