+++
title = "Pospelov2020"
+++

Gennady Pospelov, Walter Van Herck, Jan Burle, Juan M. Carmona Loaiza, Céline Durniak, Jonathan M. Fisher, Marina Ganeva, Dmitry Yurov and Joachim Wuttke (2020):

BornAgain: software for simulating and fitting grazing-incidence small- angle scattering

[J. Appl. Cryst. 53, 262–276](https://doi.org/10.1107/S1600576719016789).

<i>This is the standard reference for BornAgain, see also [how to cite](/howto/cite).</i>
