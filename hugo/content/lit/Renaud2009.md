+++
title = "Renaud2009"
+++

G. Renaud, R. Lazzari and F. Leroy (2009):

Probing surface and interface morphology with Grazing Incidence Small Angle X-Ray Scattering.

[Surf. Sci. Rep. 64, 255-380](https://doi.org/10.1016/j.surfrep.2009.07.002).
