+++
title = "Sears1992"
+++

Varley F. Sears (1992):

Neutron scattering lengths and cross sections.

[Neutron News 3, 26-37](https://doi.org/10.1080/10448639208218770).
