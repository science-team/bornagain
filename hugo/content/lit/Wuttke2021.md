+++
title = "Wuttke2021"
+++

Joachim Wuttke (2021):

Numerically stable form factor of any polygon and polyhedron.

[J Appl Cryst 54, 580-587](https://doi.org/10.1107/S1600576721001710).
