+++
title = "Python scripting"
weight = 40
+++

## Python scripting

#### About

The Python application programming interface (API)
enables users to run simulations and fits
from the high-level programming language Python.
This can be done in interactive sessions or by executing a script,
using the command line, a Jupyter notebook, or an
integrated development environment (IDE) like PyCharm.

Scripting is more versatile than the GUI and provides functionality
that is not yet available in the GUI, or never will be. For instance,
one can

  * set up arbitrarily complicated combined and constrained fits;
  * batch process huge data sets;
  * react to a simulation or fit outcome through control clauses;
  * extend the functionality of the BornAgain core, for instance
    by adding particle form factors or correlation functions.

##### Install

Install Python and the BornAgain Python module as explained in the
[installation instructions](/deploy).

##### Check installation

Launch a Python shell, notebook or IDE.
At the Python command prompt, enter the command
```
import bornagain
```
If the BornAgain module is found, Python will just print the next command prompt.
Otherwise it will raise a `ModuleNotFoundError`.

##### Run first example script

[Run a first example script](run).
Then read the [explanation](syntax).

##### Modify some example

Finally, to start productive work,
choose an example script that somehow ressembles your application problem,
and modify it step by step.

Examples can be found
- in these web docs in section [Script examples](/ex);
- in the BornAgain sources in directory {{% ref-src "auto/Examples" %}}.

Most examples are covered by nightly tests.
If nonetheless an example does not work as expected,
then please [report this as a bug](/howto/get-help).

##### Further tutorials

- [Fitting](fit)
- [Upgrading](upgrading)