+++
title = "Run a script"
weight = 10
+++

## Running a script

To get started with BornAgain scripting, run a first example.

We assume that BornAgain and Python are
[installed](/deploy)
and that the Python interpreter can `import bornagain`
(see [preceding page](/py), "Check Installation").

We shall now run a first example script.
This and all other example scripts can also be found in the BornAgain distribution,
in directory {{% ref-src "auto/Examples" %}}.

Download the following example script,
using the link just below the code frame.
Save the script under the name `AlternatingLayers1.py`.

{{< show-ex file="specular/AlternatingLayers1.py">}}
<p>

For a discussion of the content of this script,
see [simulation/reflectometry](/ref/sim/class/specular).

### From the command line

At the command prompt in a terminal, launch the script with the command
```bash
python AlternatingLayers1.py
```
(on systems that still have Python2, the command may rather be `python3`).

As result, a MatPlotLib window should pop up, and display this reflectometry curve:

{{< figscg src="/img/auto/specular/AlternatingLayers1.png" width="500" class="center">}}

#### Short call (Linux, Mac)

Under a Unix shell,
the script can also be launched without typing `python` at the command prompt:
```bash
AlternatingLayers1.py
```
This is made possible by the line "`#!/usr/bin/env python3`" on top of the script.
It may be necessary to make the script executable by
```bash
chmod a+x AlternatingLayers1.py
```
