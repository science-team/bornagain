+++
title = "Understand the syntax"
weight = 11
+++

## Python syntax used in BornAgain scripts

We shall employ the same reflectometry script
as on the [preceding page](run)
to explain some Python syntax used in BornAgain scripts.

For easy reference, here again the full script:

{{< show-ex file="specular/AlternatingLayers1.py">}}
<p>

We shall now explain the code.

The [shebang](https://en.wikipedia.org/wiki/Shebang_(Unix)) line
```python
#!/usr/bin/env python3
```
makes the script executable under Unix-like operating systems,
see the chapter on [how to run](/py/run) scripts
from the command line. It is ignored otherwise, as is all
text from the comment start character `#` to the end of the line.

Lines between triple quotes
```python
"""
Text, text, text
"""
```
are also comments.

The function
```python
def get_sample():
    ...
```
constructs and returns a sample model.
For more reference, see the [sample](/ref/sample) section.

The function
```python
def get_simulation(sample):
    ...
```
constructs and returns a simulation model.
For more information, see the [simulation](/ref/sim) section,
and specifically the [reflectometry](/ref/sim/class/specular) reference.


The clause
```python
if __name__ == '__main__':
```
ensures that the following statements are only executed
if the script is called directly, as a "main" program.
This precaution is required by the GUI, where scripts can be imported without
being immediately executed.

The lines
```python
sample = get_sample()
simulation = get_simulation(sample)
result = simulation.simulate()
```
construct a sample and instrument model and run a simulation.
The function `simulate()` returns a
[Datafield](/ref/data/datafield) instance.

The line
```python
bp.plot_simulation_result(result)
```
plots the simulated reflectivity as function of the incident glancing angle,
using MatPlotLib.
