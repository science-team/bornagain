+++
title = "Upgrading"
weight = 30
+++

# Upgrading Python scripts after BornAgain version change

As we are working on simplifying and unifying the BornAgain Python API,
each major release of BornAgain brings some changes that breaks some scripts.

With BornAgain-22, we provide for the first time a conversion script
that replaces _some_ of the outdated constructs.
This script can be found in the BornAgain source tree at
 {{% ref-src "scripts/upgrade-21-to-22.py" %}}.
