+++
title = "Data types"
weight = 60
+++

# Datafield and other types

In BornAgain, the basic class for holding experimental and simulated data is
* [Datafield](datafield)
  * [Export](datafield/export) --- save to file

Other data types:
* [SLD profile](sld-profile) --- can be retrieved from sample model

Manipulation and processing of data:
* [Find peaks in 2d data](datafield/find-peaks)
