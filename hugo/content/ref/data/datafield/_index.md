+++
title = "Class Datafield"
weight = 20
+++

# Class Datafield

Class {{% ref-class "Device/Data" "Datafield" %}} holds experimental or simulated scattering data.

Most important, the function `simulation.simulate()` returns an object of class `Datafield`.

A `Datafield` object contains
- an array with radiation intensities,
- optionally, an array with error estimates,
- coordinate axes, and
- optionally, a title.

### Modify

To set the title of Datafield `df`:
```python
df.setTitle("text")
```

This is particularly useful for disambiguating datafields that are put together in a list.
Functions `make_plot` and `plot_multicurve` from module `ba_plot`
use datafield titles to generate a legend.

### Export

For detailed reference, see [Export](export).

To save a Datafield `df` in a file:
```python
ba.IOFactory.writeDatafield(df, file_name)
```
where `file_name` must have an extension `.(txt|int|tif)`,
and may have a second extension `.(gz|bz2)`.

To extract NumPy arrays from a Datafield `df`:
```python
df.xCenters()  # centers of x bins
df.dataArray() # intensities
df.errors()    # error estimates thereof
```

### Plot

For detailed reference, see [Plotting](/ref/plot).

To plot a Datafield `df`:

```python
from bornagain import ba_plot as bp
bp.plot_simulation_result(df)
bp.plt.show()
```

### Analyze

To determine pekas in a 2d Datafield, use
* [ba.FindPeaks](find-peaks)