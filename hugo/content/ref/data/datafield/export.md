+++
title = "Export"
weight = 20
+++

## Export a Datafield

#### Save in various formats

A `Datafield` can be saved in various data formats
through the BornAgain Python method
```python
import bornagain as ba
simulation = ...
result = simulation.simulate()
ba.IOFactory.writeDatafield(result, file_name)
```
where `file_name` must have one of the extensions
- `.txt` --- ASCII file with 2D array [nrow][ncol], layout as in NumPy,
- `.int` --- BornAgain internal ASCII format,
- `.tif` --- 32-bits TIFF file,

to which one may append a second extension that specifies compression:
- `.gz`,
- `.bz2`.

#### Export to NumPy

To extract data from `Datafield` in form of NumPy arrays, use
```python
result = simulation.simulate()
arr_x  = result.xCenters()  # centers of x bins
arr_i  = result.dataArray() # intensities
arr_di = result.errors()    # error estimates thereof
```
To save any such array, use the NumPy method
```python
numpy.savetxt("intensity.txt", arr)
```
