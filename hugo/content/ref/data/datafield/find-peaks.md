+++
title = "Find intensity peaks"
weight = 70
+++

## Finding intensity peaks

To find the intensity peaks from a GISAXS simulation,
use the method `FindPeaks`.
It returns an array of (x,y) coordinates:

```python
    result = run_simulation().histogram2d()
    peaks = ba.FindPeaks(result, 2, "nomarkov", 0.001)
    peaks_x = [peak[0] for peak in peaks]
    ypeak_y = [peak[1] for peak in peaks]
```

{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/FindPeaks.png" width="670px" caption="Intensity images">}}
{{< /galleryscg >}}

The following script offers a complete example in which the peaks are found after carrying on a GISAXS simulation. This particular example uses as a sample a grating of long boxes distributed along a 1D lattice.

{{< show-ex file="scatter2d/FindPeaks.py" >}}
