+++
title = "SLD profile with rough interfaces"
weight = 20
+++

### SLD profile with rough interfaces

This example shows how to plot the Scattering Length Density (SLD) profile
for a multilayer sample with rough interfaces.

{{< figscg src="/img/auto/varia/MaterialProfile.png" width="500px" >}}

The sample consists of alternating Ti and Ni layers with rough interfaces.

{{< show-ex file="varia/MaterialProfile.py" >}}
