+++
title = "TIFF"
weight = 10
+++

## TIFF data format

Before loading the main binary data, BornAgain reads the following metadata tags from `tiff` file:

* [ImageWidth](https://www.awaresystems.be/imaging/tiff/tifftags/imagewidth.html)
* [ImageLength](https://www.awaresystems.be/imaging/tiff/tifftags/imagelength.html)
* [BitsPerSample](https://www.awaresystems.be/imaging/tiff/tifftags/bitspersample.html)
* [SamplesPerPixel](https://www.awaresystems.be/imaging/tiff/tifftags/samplesperpixel.html)
* [SampleFormat](https://www.awaresystems.be/imaging/tiff/tifftags/sampleformat.html)

If any of these tags is not found, an error message will be shown.

The requirement to values of the tags are following:

* `BitsPerSample` should be 8, 16 or 32.
* `SamplesPerPixel` should be 1, which means grayscale image with one channel, corresponding to signal intensity.
* `SampleFormat` should be 1, 2 (integer numbers) or 3 (floating-point numbers). `SampleFormat==3` requires value of `BitsPerSample==32`.

### Checking tags

File tags can be checked by external utilites.

In Linux there are `tiffinfo` from `libtiff`
```bash
$ tiffinfo file.tiff
```

or `identify` from `imagemagic`
```bash
$ identify -verbose file.tiff
```

In Windows [AsTiffTagViewer](https://www.awaresystems.be/imaging/tiff/astifftagviewer.html) can be used.

Alternatively, online services can show metadata tags.

### Unsupported tiff formats

If there is a case of `tiff` data file that is not yet supported, please contact us and provide the example.
