+++
title = "Examples"
weight = 900
+++

## Fitting examples

For an introduction, see [Python tutorial > Fitting](/py/fit).

For an informal API documentation, see [Reference > Fitting](/ref/fit).

The following examples refer to experimental or synthetic data
in directory {{% ref-src "testdata" %}}.
To make these data findable,
the environment variable `BA_DATA_DIR` must point
to the copy of the `testdata/` directory in your local BornAgain installation.

{{% children depth="2" %}}
