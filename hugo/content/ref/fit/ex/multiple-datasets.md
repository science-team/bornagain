+++
title = "Simultaneous fit of two datasets"
weight = 40
+++

## Simultaneous fit of two datasets

In this example we demonstrate how to fit two datasets simultaneously.

Suppose that we have a sample measured twice for two different incident angles. We are going to fit both datasets simultaneously to find the unknown sample parameters.

To do this, we define one dataset (a pair of real data and corresponding simulation builder) for the first incidence angle and another pair for the second incidence angle. We add both pairs to the `FitObjective` and run the fit as usual.

In the given script we simulate a dilute random assembly of hemi-ellipsoids on a substrate.
The particle form factor has 3 parameters: `radius_a` and `height` are parameters to find, `radius_b` is fixed.

The custom `PlotObserver` class plots the fit for the two datasets every 10th iteration.

{{< galleryscg >}}
{{< figscg src="/img/draw/multiple_datasets.png" width="600px" caption="Fit window">}}
{{< /galleryscg >}}

{{< show-ex file="fit/scatter2d/multiple_datasets.py" >}}
