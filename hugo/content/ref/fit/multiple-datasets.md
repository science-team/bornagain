+++
title = "Multiple datasets"
weight = 80
+++

## Fitting several datasets at once

Several datasets can be fitted simultaneously,
either using the BornAgain fit facilities, or using external minimizers.

### With the BornAgain fit facilities

Add different data and different fit models to an instance of
class [FitObjective]({{% url-src %}}/Sim/Fitting/FitObjective.h):

```python
    fit_objective = ba.FitObjective()
    fit_objective.addSimulationAndData(simulation1, data1[, stdv1][, wgt1])
    fit_objective.addSimulationAndData(simulation2, data2[, stdv2][, wgt2])
```

The optional `stdv` arguments hold error estimates.
The optional weights `wgt` are by default set to 1.

For a full example, see [Examples > Fitting > Simultaneous fit](/ref/fit/ex/multiple-datasets).

### With external minimizer

See the [honeycomb fit example](/ref/fit/ex/reflectometry-honeycomb),
where differential evolution is called with an objective function that
has contributions from several reflectometry datasets and models:

```python
    def objective_function(*args):
        ...
        return <sum of squared relative differences>

    result = scipy.optimize.differential_evolution(
        objective_function,
        ...)
```
