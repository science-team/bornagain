+++
title = "Beam"
weight = 20
+++

## Beam model

The incident beam must be specified for scattering simulations with 2D detector
(SAS and GISAS).
For other simulations, the beam is set internally according to the
given [scan](../scan) model.

To create a beam, use
```python
beam = ba.Beam(intensity, wavelength, alpha, phi=0)
```
For the wavelenth, use unit multiplier `*nm` or `*angstrom`.
For the grazing angle $\alpha\_\text{i}$, use unit multiplier `*deg`.
The azimuthal angle $\varphi\_\text{i}$ can be omitted,
as in most applications the default value 0 is pertinent.

After creation of a beam, additional properties can be set with
```python
beam.setFootprint(footprint)
beam.setPolarization(polarization)
```
For the arguments, see [footprint](footprint) and [polarization](../pol).

The beam intensity may appear inconsequential for specular simulations
because resulting reflectivities are normalized.
However, the intensity does influence the simulated [noise](/ref/sim/setup/bg).

{{% children  %}}
