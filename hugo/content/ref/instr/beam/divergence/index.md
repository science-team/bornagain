+++
title = "Beam divergence"
weight = 50
+++

## Beam divergence

By default, the incident beam is perfectly monochromatic and collimated.
Here we show how to set finite distributions of wavelengths and of grazing angles.

* The wavelength follows a log-normal distribution around the mean value of 0.1 nm with a scale parameter equal to $0.1$.
* Both incident angles follow a Gaussian distribution around the average values $\alpha\_i = 0.2 ^{\circ}$ and $\varphi\_i = 0^{\circ}$, respectively and $\sigma_{\alpha\_i} = \sigma\_{\varphi\_i} = 0.1^{\circ}$.

First, the [distributions](/ref/other/distributions) should be defined:

```python
distr_lambda = ba.DistributionLogNormal(0.1*nm, 0.1, 5, 2)
distr_alpha = ba.DistributionGaussian(0.2*deg, 0.1*deg, 5, 2)
distr_phi = ba.DistributionGaussian(0, 0.1*deg, 5, 2)
```

Then the distributions are to be applied to the `simulation` object in the following way:

```python
simulation = ba.ScatteringSimulation(...)
simulation.addParameterDistribution(role, distribution)
```

The `role` is a special object itself, indicating which parameter is distributed. 
There are three roles:

```python
role_wavelength = ba.ParameterDistribution.BeamWavelength
role_grazing_angle = ba.ParameterDistribution.BeamGrazingAngle
role_azimuthal_angle = ba.ParameterDistribution.BeamAzimuthalAngle
```

Distributions are passed to the `simulation` together with their `role`:

```python
simulation.addParameterDistribution(role_wavelength, distr_lambda)
simulation.addParameterDistribution(role_grazing_angle, distr_alpha)
simulation.addParameterDistribution(role_azimuthal_angle, distr_phi)
```

#### Example

The DWBA simulation example is shown for a standard sample model:

* The sample is composed of monodisperse cylinders deposited on a substrate.
* The cylinders are dilute and distributed at random,
  hence there is no interference between scattered waves.

{{< galleryscg >}}
{{< figscg src="/img/draw/BeamDivergence_setup.jpg" width="350px" caption="Real-space model">}}
{{< figscg src="/img/auto/scatter2d/BeamDivergence.png" width="350px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/BeamDivergence.py" >}}
