+++
title = "Footprint"
weight = 10
+++

### Footprint correction

The footprint effect originates from the angle-dependent intersection
of a finite beam with a finite sample.
It should be considered for all scanning simulations.

{{< galleryscg >}}
  {{< figscg src="/img/draw/FootprintScheme.png" width="350px">}}
{{< /galleryscg >}}

When taking into account footprint correction, there are two possible options for the
beam shape in `BornAgain`:

* Square beam --- the beam has sharp edges and square cross-section,
                  its intensity is uniformly distributed.
* Gaussian beam --- the beam is infinite in space, while its intensity has Gaussian
                    distribution along the radius of the beam.

The footprint correction for square beam is defined by
`FootprintSquare` command, which has the signature

```python
footprint = FootprintSquare(beam_to_sample_width_ratio)
```

Here `footprint` is an object later passed to the simulation, while `beam_to_sample_width_ratio`
defines the ratio between the widths of beam and sample.

In the case of the Gaussian beam the footprint object is created with

```python
footprint = FootprintGauss(beam_to_sample_width_ratio)
```

The command signature is exactly the same as in the case with the square beam,
but the beam width required for `beam_to_sample_width_ratio`
is now defined as the beam diameter associated with the intensity level equal to $I_0 \cdot e^{-\frac{1}{2}}$,
where $I_0$ is the on-axis (maximal) intensity.

Footprint can be applied for both scan and single beam positions:

```python
beam.setFootprint(footprint)  # for scattering
scan.setFootprint(footprint)  # for specular and offspec
```

In example below a square beam is considered, with `beam_to_sample_width_ratio` being equal to $0.01$.
The incident angle range was made rather small in this example
(from $0.0$ to $0.6$ degrees) in order to emphasize
the footprint impact at small incident angles.
In other respects this example exactly matches the
[reflectometry simulation tutorial](/ref/sim/class/specular).

{{< galleryscg >}}
{{< figscg src="/img/auto/specular/FootprintCorrection.png" width="450px">}}
{{< /galleryscg >}}

{{< show-ex file="specular/FootprintCorrection.py" >}}
