+++
title = "Detectors"
weight = 40
+++

## Detectors

For SAS and GISAS simulations, see

* [SphericalDetector](/ref/instr/det/2d).

For off-specular simulations, see

* [Offspec detector](/ref/instr/det/offspec).
