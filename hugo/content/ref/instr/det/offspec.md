+++
title = "Offspec detector"
weight = 80
+++

## Off-specular detector

The constructor is
```
ba.OffspecDetector(n_phi, phi_min, phi_max, n_alpha, alpha_min, alpha_max)
```

Scattering intensity is computed for `n_phi` azimuthal bins, then naively summed.

Results may vary widely for even vs odd `n_phi`. Check!
Choose the phi range so narrow, and `n_phi` so large that results become stable.
