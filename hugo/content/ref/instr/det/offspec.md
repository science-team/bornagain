+++
title = "Offspec detector"
weight = 80
+++

## Off-specular detector

Will change in one of the next releases.

For the time being, the constructor is
```
ba.OffspecDetector(nphi, phi_min, phi_max, nalpha, alpha_min, alpha_max)
```

Scattering intensity is computed for `nphi` azimuthal bins, then naively summed.

Results may vary widely for even vs odd `nphi`. Check!
Choose the phi range so narrow, and `nphi` so large that results become stable.
