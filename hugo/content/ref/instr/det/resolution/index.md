+++
title = "2d detector resolution"
weight = 20
+++

### Resolution function of a two-dimensional scattering detector

If a detector has a resolution function, then the simulated intensity map
is blurred by convolution with that function.

Currently, only Gaussian resolution functions are supported.

## Gaussian distribution

Create a Gaussian resolution function, and attach it to a detector,
as follows:
```
resol = ba.ResolutionFunction2DGaussian(horizontal_width, vertical_width)
detector.setResolutionFunction(resol)
```

For an example, see [resolution 2d example](/ref/instr/ex/resol/2d).
