+++
title = "SphericalDetector"
weight = 10
+++

## SphericalDetector

For SAS and GISAS simulations,
the two-dimensional area detector is modelled by
the {{% ref-class "Device/Detector" "SphericalDetector" %}} class.

### Geometry

A `SphericalDetector` is a 2D detector defined in angular coordinates.
All pixels have equal size.

{{< figscg src="/img/draw/detector1.svg" width="100%" >}}

In BornAgain, detector images are shown as seen from the back of the detector.
In this way we ensure that the $\varphi$ or $q_y$ axis has the same
orientation as the real-space $y$ axis.

For plotting and data import and export,
BornAgain pretends that the detector bins correspond to
bins of constant width in $\varphi$ or $q_y$ and  $\alpha$ or $q_z$,
even though this is only true in first approximation.

For the actual scattering computation, BornAgain uses true scattering angles,
computed as nonlinear functions of the pixel coordinates.

Further distortions arise
if the detector is not perpendicular to the `x` axis.
This is not unlikely in GISAS
where it makes sense to position the detector perpendicular
to the reflected or transmitted beam.
Corrections for these and other distortions will be implemented
when requested by users.

### Constructor

The constructor has the signature
```
detector = ba.SphericalDetector(n_phi, phi_min, phi_max, n_alpha, alpha_min, alpha_max)
```

The arguments `phi_min, phi_max, alpha_min, alpha_max` define the range of the detector in
direction $\varphi_\text{f},\alpha_\text{f}$.

The arguments `n_phi, n_alpha` are the number of bins per direction;
their product is the number of pixels of the detector.

### Masks

When fitting theoretical models to measured diffraction images,
it can be helpful to mask part of the detector area.
See

* [Fit with masks](/ref/fit/ex/fit-with-masks)

### Resolution

For modeling the detector resolution, see [Detector resolution](/ref/instr/det/resolution).

### History

Until BornAgain 21, we had a `RectangularDetector` and a `SphericalDetector`.
In BornAgain 22, they were merged into `SphericalDetector` class.
