+++
title = "Neutron polarization"
weight = 30
+++

## Neutron polarization

Reflectometry example:
* [Magnetic layer](magnetic-layer)
* [Ditto, with imperfect polarizers and detector resolution](magnetic-layer-imperfect)

Scattering example:
* [Polarized scattering](polarized-scattering)
