+++
title = "Magnetic layer"
weight = 10
+++

### Polarized neutron reflectometry with a magnetic layer

This is the introductory example for specular reflectometry with polarized neutrons.

The sample consists of a single magnetic layer on top of a substrate.
The magnetic field vector `B` lies in the xy plane.
The field direction forms an angle of `Bang = 30 * deg` with the x axis.
The field magnitude `Bmag=1e8` is in units of A/m.
The field is passed as an optional function argument to the constructor `ba.MaterialBySLD`.

The neutron polarizer and analyzer directions are set through
```
    scan.setPolarization(polarizer_vec)
    scan.setAnalyzer(analyzer_vec)
```


The resulting reflectivities are shown in this plot:

{{< galleryscg >}}
{{< figscg src="/img/auto/specular/MagneticLayer.png" width="650px" caption="Reflectivity">}}
{{< /galleryscg >}}

As expected, we find the birefringent behavior.
The critical angles also depends on the polarization.

The curve for polarizer settings `-+` is not shown
because it coincides with the `+-` curve.

Here is the complete example:

{{< show-ex file="specular/MagneticLayer.py" >}}
