+++
title = "Polarized scattering"
weight = 200
+++

## Polarized scattering by magnetic particles

This example shows a GISAS simulation with polarized beam,
magnetic sample, and polarization analyzing detector.

The sample is made of magnetic spherical particles
embedded in the substrate of a simple 2-layer system.
The magnetization is in z direction.

The incident beam is polarized in +z direction.
The polarization analyzer transmits neutron with -z polarization component.
Accordingly, only spin-flip scattering is detected.

{{< figscg src="/img/auto/scatter2d/MagneticSpheres.png" width="500px" class="center" >}}

{{< show-ex file="scatter2d/MagneticSpheres.py" >}}
