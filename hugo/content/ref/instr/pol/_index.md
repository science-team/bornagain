+++
title = "Neutron polarization"
weight = 60
+++

## Neutron polarization

BornAgain takes neutron polarization into account
if there is magnetism in the sample model
or if a polarizer is set in the instrument model.

Sample magnetization is explained in
[Sample model > Materials > Magnetization](/ref/sample/material/magnetization).

Beam polarization is set through
```python
polarizer_vec = R3(x, y, z)
beam.setPolarization(polarizer_vec) # for scattering
scan.setPolarization(polarizer_vec) # for specular and offspec
```
The `polarizer_vec` is a Bloch vector.
If it is a unit vector, then the beam polarization is perfect.
A shorter vector indicates imperfect polarization.

The analyzer properties are through
```python
analyzer_vec = R3(x, y, z)
detector.setAnalyzer(analyzer_vec)  # for scattering and offspec
scan.setAnalyzer(analyzer_vec)  # for specular
```
The first argument is a unit vector that
specifies probability of projecting on some direction during mesurement.

Examples are under [Examples > Instrument > Polarization](/ref/instr/ex/pol).

Examples to be moved:
{{% children  %}}
