+++
title = "Scan"
weight = 20
+++

## Scan models

Scans are needed to construct simulations of types
[reflectometry](/ref/sim/class/specular),
[off-specular scattering](/ref/sim/class/offspec),
[depth probe](/ref/sim/class/depthprobe).

There are two types of scan:
- Alpha scans, for instruments that are mechanically scanning
  the glancing angle $\alpha\_\text{i}$;
- Qz scans, for time-of-flight instruments.

### Alpha scans

To specify a scan with `n` equidistant steps in the glancing angle $\alpha\_\text{i}$, use
```python
scan = ba.AlphaScan(n, alpha_start, alpha_stop)
```

Usage is demonstrated by most examples in
{{% ref-ex "specular" %}}.

For other sequences of $\alpha\_\text{i}$ values, use the more generic
```python
scan = ba.AlphaScan(axis)
```

The Axis API may change soon and will be documented then.

After constructing a scan, set the wavelength and optionally a constant offset
$\delta\alpha\_\text{i}$ using
```python
scan.setWavelength(lambda)
scan.setAlphaOffset(dalpha)
```

### Lambda scans

To specify a scan with `n` equidistant steps in the neutron wavelength $\lambda$, use
```python
scan = ba.LambdaScan(n, lambda_start, lambda_stop)
```

To specify a scan with any other sequence of $\lambda$ values, use
```python
scan = ba.LambdaScan(lambda_list)
```

Usage in off-specular scattering is demonstrated by
{{% ref-ex "specular/OffspecLambda.py" %}}.

See also the example page [off-specular simulation](/ex/sim/offspec).

### Qz scans

To specify a scan with `n` equidistant steps in the vertical wavenumber $q_z$, use
```python
scan = ba.QzScan(n, qz_start, qz_stop)
```

For other sequences of $q_z$ values, use the more generic
```python
scan = ba.AlphaScan(axis)
```

The Axis API may change soon and will be documented then.

A third alternative consists in passing a NumPy array,
```python
import numpy as np
qz_vector = np.linspace(0.01, 1, n)
scan = ba.QzScan(qz_vector)
```

After constructing a scan, set the wavelength and optionally a constant offset
$\delta q_z$ using
```python
scan.setWavelength(lambda)
scan.setOffset(dqz)
```

Usage is demonstrated in page [time-of-flight reflectometry](tof).

Handling of resolution will change soon.
The current API is demonstrated in page
[time-of-flight reflectometry with resolution](tof-resolution).
