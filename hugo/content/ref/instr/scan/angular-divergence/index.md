+++
title = "Beam Angular Spread"
weight = 60
+++

### Beam Angular Spread in Specular Simulations

This example demonstrates beam angular spread effects in reflectivity computations.
It also offers a comparison with data generated using another well known code: GenX.
Further information about reflectometry simulations can be found in the
[Reflectometry Simulation Tutorial](/ref/sim/class/specular).

The observed reflectometry signal can be affected either by a spread in the beam wavelength or in the incident angle.

{{< galleryscg >}}
{{< figscg src="/img/auto/specular/VsGenx.png" width="500px" caption="Intensity image">}}
{{< /galleryscg >}}

In this example, a Gaussian distribution is used to spread the incident angle, with a standard deviation of $\sigma_{\alpha} = 0.01^{\circ}$.

{{< show-ex file="specular/VsGenx.py" >}}

Reference data: {{% ref-ex "data/genx_angular_divergence.dat.gz" %}}
