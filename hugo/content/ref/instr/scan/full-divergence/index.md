+++
title = "Beam Wavelength Spread"
weight = 65
+++

### Beam Wavelength Spread in Specular Simulations

This example demonstrates beam wavelength spread effects in reflectivity computations.
All simulation parameters (except for those related to beam spread itself)
coincide with those defined in
[reflectometry simulation tutorial](/ref/sim/class/specular).

In real specular experiments the observed reflectivity is always affected
by the beam spread in both wavelength and incident angle.

{{< galleryscg >}}
{{< figscg src="/img/auto/specular/BeamFullDivergence.png" width="500px" caption="Intensity image">}}
{{< /galleryscg >}}

In this example the following parameters related to the spread of the beam were set to the simulation:

* Gaussian distributions both in wavelength and incident angle
* The mean value for beam wavelength $\lambda_0 = 0.154$ nm
* Standard deviation in the wavelength $\sigma_{\lambda} = 0.01 \cdot \lambda_0$
* Standard deviation in the incident angle $\sigma_{\alpha} = 0.01^{\circ}$

As one can see from the Python script, the definitions of beam parameter distributions
match ones described in [similar example for GISAS simulations](/ref/instr/beam/divergence).
However, in the case of the incident angle one should always use a distribution with zero mean,
since the actual mean value is substituted by `SpecularSimulation` in dependence on the
defined grazing angle range.
If the distribution of the incident angle has non-zero mean value, an exception
is thrown:

```python
terminate called after throwing an instance of 'std::runtime_error'
  what():  Error in SpecularSimulation: parameter distribution of beam grazing angle should have zero mean.
```

{{< show-ex file="specular/BeamFullDivergence.py" >}}
