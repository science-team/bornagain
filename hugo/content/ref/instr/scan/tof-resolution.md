+++
title = "TOF resolution"
weight = 30
+++

## Resolution effects in TOF Reflectometry

In the following reflectometry example, each scan point has a distribution of $q_z$ values.

The constructor
```python
ba.DistributionGaussian(0., 1., 25, 2.)
```
specifies a Gaussian distribution with mean 0, standard deviation 1,
25 sampling points, and a cut-off at 2 sigma. For other distributions (besides Gaussian),
see [distributions](/ref/other/distributions).

The statements
```python
scan = ba.QzScan(qzs)
scan.setVectorResolution(distr, dq)
```
take arrays `qzs` and `dq` as arguments. These arrays must have the same length `n`.
For each scan point (i=0,..,n-1), the $q_z$ values have a Gaussian distribution with
mean `qzs[i]` and `dq[i]`.

{{< galleryscg >}}
  {{< figscg src="/img/auto/specular/TimeOfFlightReflectometry.png" width="350px" caption="TOF simulation without resolution effects" >}}
  {{< figscg src="/img/auto/specular/TOFRWithResolution.png" width="350px" caption="TOF simulation with $dq = 0.03\,q$" >}}
{{< /galleryscg >}}


{{< show-ex file="specular/TOFRWithResolution.py" >}}
