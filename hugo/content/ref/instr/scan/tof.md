+++
title = "TOF reflectometry"
weight = 30
+++

## Time-of-flight reflectometry

This example shows how to set up a $q_z$ scan using a NumPy array.
This can be used to simulate a time-of-flight (TOF) reflectometry experiment.


{{< galleryscg >}}
{{< figscg src="/img/auto/specular/TimeOfFlightReflectometry.png" width="500px" >}}
{{< /galleryscg >}}

{{< show-ex file="specular/TimeOfFlightReflectometry.py" >}}
