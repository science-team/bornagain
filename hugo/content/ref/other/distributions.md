+++
title = "Distributions"
weight = 60
+++

## Parameter distributions

##### Constructors

A distribution is constructed by one of the following:
```python
distr = ba.DistributionGate(start, stop, n_samples=25)
distr = ba.DistributionCosine(mean, hwhm, n_samples = 25)
distr = ba.DistributionGaussian(mean, std_dev, n_samples=25, rel_samplig_width=2)
distr = ba.DistributionLorentz(mean, hwhm, n_samples=25, rel_samplig_width=2)
distr = ba.DistributionLogNormal(median, scale_param, n_samples=25, rel_samplig_width=2)
```

##### Usage

Parameter distributions are either supplied to beam divergence or detector resolution
classes as described there, or they are used programmatically.

In programmatical usage, the function call
```
parsamples = distr.distributionSamples()
```
returns a vector of parameter samples. Each `ParameterSample` has a `value` and a `weight`.
The sample (scattering target) model is then constructed as incoherent sum of components
with given parameter values and weights.
For instance, in the [PolydisperseCylinders example](/ref/sample/particle/ex/polydispersity),
a layout is filled with uncorrelated particles:
```python
distr = ba.DistributionGaussian(10*nm, 1*nm)
for parsample in distr.distributionSamples():
    ff = ba.Cylinder(parsample.value, 5*nm)
    particle = ba.Particle(material_Particle, ff)
    layout.addParticle(particle, parsample.weight)
```

##### Examples

- [PolydisperseCylinders](/ref/sample/particle/ex/polydispersity): layout filled with uncorrelated particles
- [LatticeOrientationDistribution](/ref/sample/interference/ex/lattice-ori-distr): layer filled with uncorrelated layouts