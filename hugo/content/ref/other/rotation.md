+++
title = "Rotation"
weight = 20
+++

## Particle rotation


##### Constructors

Rotations are created by the following constructors:

```python
# Euler z-x-z sequence of rotations
RotationEuler(alpha, beta, gamma)

# rotation around X axis by given angle
RotationX(angle)

# rotation around Y axis by given angle
RotationY(angle)

# rotation around Z axis by given angle
RotationZ(angle)
```

Angle arguments are understood in radians.
Use `*deg` to convert from degrees.

The Euler angles `alpha`, `beta`, `gamma` refer to the `z-x'-z''` convention:
- The object is first rotated by `alpha` about the `z` axis.
The coordinate axes, rotated along with the object, shall be denoted by a prime:
`z'=z`, while `x',y'` differ from `x,y`.
- The second rotation is by `beta` about the `x'` axis.
The coordinate axes, rotated along, are now denoted by a double prime.
- The third rotation is by `gamma` about the `z''` axis.

##### Usage

Rotations can be applied to particles and to inter-particle structures.

Initial orientations are specified by the drawings in the
[form factors](/ref/sample/particle/ff) and
[inter-particle structures](/ref/sample/interference) documentation.

For the time being, particle rotations are carried out around the reference point
in the center of the bottom of the particle. This shall be revised soon.

Rotations affect only rotated objects, not the coordinate system.
Therefore subsequent rotations or translations are carried out with respect to
the original coordinate system.

##### Examples

- [particle rotation](/ref/sample/particle/ex/particle-rotation)
