+++
title = "Plotting"
weight = 70
+++

# Plotting with ba_plot

To plot simulation results (possibly along with experimental data),
different methods are viable:
* [Export](/ref/data/datafield/export) simulated data to files,
and plot them with some external tool;
* [Convert](/ref/data/datafield/export) simulated data to NumPy arrays,
and plot them with any Python-based tool;
* Plot simulated data with [MatPlotLib](https://matplotlib.org) under Python,
using utility functions provided by BornAgain.

In the following, we will explain the last of these three methods.

### Overview

MatPlotLib-based plotting functions are provided by Python module `ba_plot`
that is part of the `bornagain` Python package.
It is typically used through
```python
from bornagain import ba_plot as bp
```

It provides the functions
```python
plot_simulation_result(result, **plotargs) # plot one Datafield
plot_multicurve(results, **plotargs)       # several 1d Datafields in one frame
make_plot(results, ncol, **plotargs)       # one Datafield per frame in a grid
make_plot_row(results, **plotargs)         # one Datafield per frame in a row
```

where `result` is a Datafield as returned by `simulation.simulate()`,
and `results` is a list of Datafields.

To finalize a plot, call one of
```python
bp.plt.show()          # launch MatPlotLib window to display the plot
bp.export(**plotargs)  # write the plot to an image file (explained below)
```

### Further reading

See
* [Plot functions](functions), more on the above listed plot functions
* [Heat map](heatmap), change the color scheme for 2D plots
* [LaTeX markup](latex), in labels and legends, `$...$` typeset with LaTeX

See also:
* [Plotting with axes in different units](axes-in-different-units)


### Code location

The functions documented in this section of the online manual
are all implemented in the Python module
[`ba_plot`]({{% url-src %}}/Wrap/Python/src/bornagain/ba_plot.py).

If our plot functions are not versatile enough for your needs,
then copy, rename, and modify them.
