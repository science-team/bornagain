+++
title = "Plot axes in different units"
weight = 210
+++

### Plotting with axes in different units

*The functionality demonstrated in this example is currently broken.*

Repair is on our agenda: https://jugit.fz-juelich.de/mlz/bornagain/-/issues/716.

....

In this example we demonstrate how modify coordinate axes in 2D plots.

The example is based on the standard GISAS simulation [Cylinders in DWBA](/ref/sim/class/scattering/ex/gisas).

The simulated Datafield are transformed using the function calls
```python
result = simulation.simulate()
result2 = detector.field2bins(result)    # coordinates are u, v as dimensionless bin numbers
result3 = detector.field2angles(result)  # coordinates are phi_f, alpha_f in degrees
result4 = detector.field2q(result)       # coordinates are q_y, q_z in 1/nm
```

The transforming functions are members of class FlatDetector because
they require knowledge of detector geometry and incident wave vector.

Note that the transform is exact only for axes that pass through the origin.
Intensitiy are not rebinned.

{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/AxesInDifferentUnits.png" width="670px" caption="Intensity images">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/AxesInDifferentUnits.py" >}}
