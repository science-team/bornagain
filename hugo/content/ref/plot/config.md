+++
title = "Configuration"
weight = 50
+++

# Plot configuration

Images in BornAgain examples are generated using the Python library
[MatPlotLib](https://matplotlib.org).

Default settings can be overridden
by function arguments or MatPlotLib ressources.

Exceptionally, [heat map](heatmap) supports an environment variable.

### Parameterization with plotargs

In the functions
```
plot_simulation_result(result, **plotargs)
plot_multicurve(results, **plotargs)
make_plot(results, ncol, **plotargs)
make_plot_row(results, **plotargs)
```
the placeholder `**plotargs` stands for optional keyword arguments.
These can be supplied in three ways:

(1) Directly like in
```python
    bp.plot_simulation_result(result, cmap='viridis', legendloc='lower right')
    bp.plt.show()
```

(2) Through a dictionary variable like in
```python
    plotargs = {}
    plotargs['aspect'] = 'auto'
    plotargs['intensity_min'] = 1e-12
    plotargs['intensity_max'] = 1e2
    bp.plot_simulation_result(result, **plotargs)
    bp.plt.show()
```

(3) If a script contains the lines
```python
    plotargs = bp.parse_commandline()
    bp.plot_simulation_result(result, **plotargs)
```

then parameters can be passed from the command line, like
```bash
python3 <script> intensity_min=1e-9
```

### Supported arguments

Support keyword arguments include

* `figfile`, only in combination `bp.export`, see [Export](export).
* `intensity_min`,
* `intensity_max`,
* `xlabel`,
* `ylabel`,
* `zlabel`,
* `legendloc`: where a legend is placed,
* `aspect`: [aspect ratio](https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.set_aspect.html), default is 'auto', other possible values are 'equal', or a number,
* `cmap`: see [heat map](heatmap).

For other optional arguments that may or may not be respected, see
the [MatPlotLib customizing tutorial](https://matplotlib.org/stable/tutorials/introductory/customizing.html).
