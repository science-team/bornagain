+++
title = "Export"
weight = 70
+++

# Export plot to file

### Interactively

To create image files interactively,
run a script that contains the usual
```python
bp.plt.show()
```
then click the "diskette" icon in the MatPlotLib window.

### Programmatically

To create image files programmatically,
replace the Python statement
```python
bp.plt.show()
```
by one of
```python
bp.export(**plotargs)
```
and use one of the three parameterization mechanisms
described in the [Configuration](config) page
to pass a keyword argument `figfile=<filename>`.
The image format is deduced from the extension of the filename.

### Image formats

Which image formats are supported depends on the local MatPlotLib installation.
To find out, either click on the export ("diskette") icon in an interactive MatPlotLib widget,
or run the following Python lines:
```python
import matplotlib.pyplot as plt
fig = plt.figure()
print(fig.canvas.get_supported_filetypes())
```

For plots generated by BornAgain, prefer a vector image format,
with extension `.svg`, `.eps`, `.ps`, or `.pdf`.
Maybe `.emf` is still useful under Windows.
