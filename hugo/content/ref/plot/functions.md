+++
title = "Functions"
weight = 10
+++

# Plot functions

The following plot functions are provided by the module `ba_plot` in the `bornagain` Python package.

### Plot single Datafield

A single Datafield can be plotted with
```python
from bornagain import ba_plot as bp
simulation = ...
result = simulation.simulate()
bp.plot_simulation_result(result)
bp.plt.show()
```

This function is used in a majority of our scripting examples,
for instance in the following short, basic examples:
* [specular reflectivity](/ref/sim/class/specular/ex)
* [GISAS](/ref/sim/class/scattering/ex/gisas)


### Plot several curves

Specular simulations yield one-dimensional Datafields that are plotted as curves y(x).
Several such curves can be plotted in one frame.
This is often used to demonstrate the effect of varying one parameter.
See e.g. the examples
* [Magnetic reflectivity](/ref/instr/ex/pol/magnetic-layer) with different polarizations
* [Rough interfaces](/ref/sample/roughness/specular_2models) with different transient models

Typical usage:
```python
from bornagain import ba_plot as bp

def simulate(p):
    simulation = ... # depends on parameter p
    return simulation.simulate()

P = [...] # list of parameter values
results = [simulate(p) for p in P]
bp.plot_multicurve(results)
bp.plt.show()
```

### Plot several frames

To plot several Datafields in as many frames, use one of

```python
make_plot(results, ncol, **plotargs)
make_plot_row(results, **plotargs)
```

The first of these creates a grid with `ncol` frames per row.
The second puts all frames in a single row;
it is equivalent to `make_plot(results, len(results), **plotargs)`.

Typical usage is as for `plot_multicurve`, see code snippet above.

Examples:
* [Grid example](/ref/sample/interference/lattice2d/position-variance);
* [Row example](/ref/instr/ex/resol/2d).
