+++
title = "Heat map"
weight = 63
+++

# Heat map

MatPlotLib supports various color schemes for plotting color-coded pixel values in 2d,
see the
[MatPlotLib color schemes tutorial](https://matplotlib.org/stable/tutorials/colors/colormaps.html).

It is generally recommended to use one of the five "perceptually uniform sequential" color schemes:
![MatPlotLib perceptually uniform sequential color schemes](/img/matplotlib_pus_colormaps.png "Perceptually uniform sequential color schemes from MatPlotLib.")

When plotting through `ba_plot`, the color scheme is determined in this order:
* from the optional `plotargs` arguments if there is one with keyword `cmap`, like `cmap='jet'`;
* otherwise from the environment variable `CMAP`, if defined;
* otherwise from the hard-coded default "inferno".

Note that the variable `image.cmap` from
[matplotlib.pyplot.rcParams](https://matplotlib.org/stable/users/explain/customizing.html)
is ignored.
This is a concious choice, intended to facilitate configuration for BornAgain users
who are no MatPlotLib experts.
