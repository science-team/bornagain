+++
title = "LaTeX markup"
weight = 68
+++

# LaTeX markup

Text enclosed between dollar signs `$...$` is interpreted as LaTeX markup.
It allows us to plot greek letters (`$\alpha$`), superscripts (`$F^2$`),
subscripts (`$\phi_{\rm i}`$), and special symbols (`$90^\circ$`).

By default, it is processed by MatPlotLib's own LaTeX processor.
However, if _usetex mode_ is activated, then the original LaTeX text
processor is used, and labels are typeset in LaTeX's computer modern fonts.
For _usetex mode_ to work, it may be necessary to install extra software.
Under Debian, the required packages are `texlive-latex-extra` (and
dependencies), `dvipng` and `cm-super-minimal`. We have no clue how
to support _usetex mode_ under Windows.

Original LaTeX fonts differ in many respects from MatPlotLib's default
Helvetica fonts.  Fonts from these different families should not be mixed.
Therefore, in _usetex mode_ it is important that *all* plot labels and titles
be enclosed in `$...$`, or for upright text `${\rm ...}$`.

To activate _usetex mode_, do one of the following:
* set environment variable `USETEX` to `True`, or
* set [matplotlib.pyplot.rcParams](https://matplotlib.org/stable/users/explain/customizing.html)
  entry `rc['text.usetex']` to `True`.
