+++
title = "Class Datafield"
weight = 80
+++

## Class Datafield

The function `simulation.simulate()` returns an object of class `Datafield`.

A `Datafield` object contains
- simulated radiation intensities,
- coordinate axes, and
- optionally, a title.

#### Plot

For plotting a `Datafield` object,
our Python module `ba_plot` provides the function `plot_simulation_result(result)`.

Usage:

```python
from bornagain import ba_plot as bp
simulation = ...
result = simulation.simulate()
bp.plot_simulation_result(result)
```

For a full example, see the for instance the [basic reflectometry example](/ex/sim/specular).

#### Export to NumPy

A simulation result can be exported to a NumPy array, using
```python
result = simulation.simulate()
arr_x  = result.npXcenters() # centers of x bins
arr_i  = result.npArray()    # intensities
arr_di = result.npErrors()   # error estimates thereof
```
To save any such array, use the NumPy method
```python
numpy.savetxt("intensity.txt", arr)
```

#### Save in various formats

Alternatively, a simulation result can be directly saved
through the BornAgain Python method
```python
import bornagain as ba
result = simulation.simulate()
ba.IOFactory.writeDatafield(result, file_name)
```
where `file_name` must have one of the extensions
- `.txt` - ASCII file with 2D array [nrow][ncol], layout as in NumPy;
- `.int` - BornAgain internal ASCII format;
- `.tif` - 32-bits TIFF file.

To this extension, one may concatenate a second extension to enforce compression:
- `.gz` or
- `.bz2`


#### History

Prior to BornAgain 22, we had a class `SimulationResult` that inherited from `Datafield`.
