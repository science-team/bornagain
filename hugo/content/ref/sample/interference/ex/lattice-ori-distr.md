+++
title = "Lattice orientation distribution"
weight = 81
+++

## Lattice orientation distribution example

This example shows GISAS by an isotropic mixture of lattices.
On top of a substrate, we have square lattices of cylinders.
The lattice orientation has a uniform distribution over the full range
form $0^\circ$ to $90^\circ$.

The differently oriented lattices are incoherently summed
using a loop over the parameter distribution samples.
This is currently supported only under Python scripting, not in the GUI.

Class references: [Layer](/ref/sample/multilayer/layer), [Distributions](/ref/other/distributions).

{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/LatticeOrientationDistribution.png" width="450px">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/LatticeOrientationDistribution.py" >}}
