+++
title = "1D lattice"
weight = 10
+++

## 1D lattice

In this example we simulate the scattering from infinite 1D repetition of rectangular patches (rectangular grating). This is done by using the interference function of a 1D lattice together with very long boxes.

* By-default, the axis of the one-dimensional lattice coincides with the $x$-axis of the reference cartesian frame, so it coinsides with the beam direction.
* Long boxes are placed along a one-dimensional lattice on top of substrate, the lattice_length parameter corresponds to the grating period.
* The size of boxes is initially chosen to form a grating which is perpendicular to the beam (long side of the box is along $y$-axis).
* Keep in mind that `length`, `width`, `height` in the `Box(length, width, height)` constructor correspond to the directions in the $x,y,z$ axes, in that order, so to achieve the desired setup we use the values: `length`= $10$ nm, `width`= $10000$ nm, `height`= $10$ nm.
* The whole grating is rotated at the end by an angle of $45^{\circ}$ with respect to the beam axis. This is achieved by rotating _both_ the 1D lattice and the long boxes (see lines 25 and 34).
* To avoid the problem of rapidly oscillating form factors of long boxes (see [this example](/ref/sim/setup/options/mc) for more details), the simulation is performed in monte carlo integration mode.

{{< galleryscg >}}
{{< figscg src="/img/draw/Interference1DLattice_setup.jpg" width="200px" caption="Real-space model">}}
{{< figscg src="/img/draw/Interference1DLattice_sketch.jpg" width="200px" caption="Sketch">}}
{{< figscg src="/img/auto/scatter2d/Interference1DLattice.png" width="200px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/Interference1DLattice.py" >}}


## Explanation

A one dimensional lattice can be viewed as a chain of particles placed at regular intervals on a single axis. The plot below represents one possible use case, where infinitely long (or very long) boxes are placed at nodes of a 1d lattice to form a grating.

{{< figscg src="/img/draw/particles_at_1d_latice.jpg" width="600px" class="center">}}

See the BornAgain user manual (Chapter 3.4.1, One Dimensional Lattice) for details about the theory.

### Interference1DLattice constructor

The interference function is created using its constructor

```python
Interference1DLattice(length, xi)
"""
length   : lattice constant, in nanometers
xi       : rotation of the lattice with respect to the x-axis, in radians
"""
```

`Length` is the length of the lattice basis vector `a` expressed in nanometers (see plot below). `xi` ($\xi$) is the angle defining the lattice orientation. It is taken as the angle between the basis vector of the lattice and the x-axis of the Cartesian coordinate system. It is defined in radians and set to 0 by default.

{{< figscg src="/img/draw/interference_1d_C.png" width="800px" class="center">}}

When the beam azimuthal angle $\varphi_f$ is zero, the beam direction coincides with x-axis. In this case, the $\xi$ angle can be considered as the lattice rotation with respect to the beam.

### Position variance

The position variance parameter `Var`, as depicted on the top plot, allows to introduce uncertainty for each particle position around the lattice point by applying a corresponding Debye-Waller factor.

It can be set using the `setPositionVariance(value)` method, where `value` is given in $nm^2$.

```python
lattice = Interference1DLattice(100*nm, 0.0*deg)
lattice.setPositionVariance(0.1)
```

By default the variance is set to zero.

### Decay function

To account for finite size effects of the lattice, a decay function should be assigned to the interference function. This function encodes the loss of coherent scattering from lattice points with increasing distance between them. The origin of this loss of coherence could be attributed to the coherence length of the beam or to the domain structure of the lattice.

In the picture below a one dimensional lattice is represented in real space as a probability
density function for finding a particle at a given coordinate on the x-axis.
In the presence of a decay function, this density can be given as
$\sum F\_{decay}(x)\cdot\delta(x-na)$.

{{< figscg src="/img/draw/lattice1d_decay_real.png" width="800px" class="center">}}

The Fourier transform of this distribution $P(x)$ provides the scattering amplitude in reciprocal space. An exponential decay law in real space with the decay length $\lambda$
will give a Cauchy distribution with characteristic width $1/\lambda$ in reciprocal space, as shown below.

{{< figscg src="/img/draw/lattice1d_decay_reciprocal.png" width="800px" class="center">}}

The decay function can be set using the setDecayFunction(decay) method of the 1D interference function.


```python
iff = Interference1DLattice(10.0*nm)
iff.setDecayFunction(FTDecayFunction1DCauchy(1000.0*nm))
```

### List of decay functions

BornAgain supports four types of one-dimensional decay functions.

+ `FTDecayFunction1DCauchy(decay_length)`

One-dimensional Cauchy decay function in reciprocal space,
corresponds to $exp(-|x|/\lambda)$ in real space.


+ `FTDecayFunction1DGauss(decay_length)`

One-dimensional Gauss decay function in reciprocal space,
corresponds to $exp(-x^2/(2*\lambda^2))$ in real space.

+ `FTDecayFunction1DTriangle(decay_length)`

One-dimensional triangle decay function in reciprocal space,
corresponds to $1-|x|/\lambda$, if $|x|<\lambda$, in real space.

+ `FTDecayFunction1DVoigt(decay_length, eta)`

One-dimensional pseudo-Voigt decay function in reciprocal space, corresponds to $eta*Gauss + (1-eta)*Cauchy$.

The parameter $\lambda$ (decay length) is given in nanometers, it is used to set the characteristic length scale at which loss of coherence takes place. In the case of the pseudo-Voigt distribution an additional dimensionless parameter `eta` is used to balance between the Gaussian and Cauchy profiles.

### Particle Density

During the simulation setup the particle density has to be explicitely specified by the user for correct normalization of overall intensity. This is done by using the `ParticleLayout.setParticleDensity(density)` method. The density parameter is given here in "number of particles per square nanometer".

### Lattice rotation in details

In this paragraph we would like to clarify the relation between the lattice orientation and the orientation of the particles forming the lattice.

{{< alert theme="info" >}}
 The rotation of the lattice doesn't change the orientation of the particles with respect to the beam direction.
{{< /alert >}}

### Setup 1

For example, we would like to create a grating: a repetition of rectangular patches perpendicular to the beam, as shown on the plot (view from the top of the sample).

{{< figscg src="/img/draw/interference_1d_particles_orientA.png" width="800px" class="center">}}

The long side of boxes is aligned along y-axes of the reference plane, the lattice axis coincides with the beam direction, the long side of the boxes is perpendicular to the beam.

To achieve such a setup, the following code should be used.

```python
layout = ba.ParticleLayout()

box = ba.Particle(material, ba.Box(10*nm, 1000*nm, 10*nm))
layout.addParticle(box)
layout.setInterference(ba.Interference1DLattice(40*nm))
```

### Setup 2

If we rotate the lattice by a certain amount, the orientation of the boxes stays the same, but the effective grating period gets smaller. The setup and code are shown below.

{{< figscg src="/img/draw/interference_1d_particles_orientB.png" width="800px" class="center">}}

```python
box = ba.Particle(material, ba.Box(10*nm, 1000*nm, 10*nm))
layout.addParticle(box)
layout.setInterference(ba.Interference1DLattice(40*nm, 30.0*deg))
```

### Setup 3

If we want to preserve the grating period and make our boxes rotate with respect to the beam, a separate rotation should be applied.

{{< figscg src="/img/draw/interference_1d_particles_orientC.png" width="800px" class="center">}}

```python
box = ba.Particle(material, ba.Box(10*nm, 1000*nm, 10*nm))
box.setRotation(ba.RotationZ(30.0*deg))
layout.addParticle(box)
layout.setInterference(ba.Interference1DLattice(40*nm, 30.0*deg))
```
