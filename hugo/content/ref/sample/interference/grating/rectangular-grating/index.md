+++
title = "Rectangular grating"
weight = 85
+++

### Interference - Rectangular Grating

This example demonstrates how to perform a simulation of a grating using very long boxes and a 1D lattice. [Interference of a 1D lattice](/ref/sample/interference/grating/lattice1d) may provide useful background information.

{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/RectangularGrating.png" width="4500px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/RectangularGrating.py" >}}

To get rid of oscillation arising from large-particle form factors, Monte-carlo integration is used.

{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/GratingMC.png" width="4500px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/GratingMC.py" >}}
