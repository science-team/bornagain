+++
title = "Hexagonal lattices with basis"
weight = 20
+++

### Hexagonal lattices with basis

Scattering from two hexagonal close packed layers of spheres.

* The sample is made of spherical particles deposited on a substrate.
* These $10$-nanometer-radius particles are positioned in a hexagonal close packed structure:
  * each layer is generated using a two-dimensional hexagonal lattice with a lattice length of $20$ nm and its $a$-axis parallel to the $x$-axis of the reference Cartesian frame.
  * the vertical stacking is done by specifying the position of a "seeding" particle for each layer:
    $(0,0,0)$ for the first layer and $(R,R,\sqrt{3}R)$ for the second layer, $R$ being the radius of the spherical particle.
* The wavelength is equal to 0.1 nm.
* The incident angles are $\alpha\_i = 0.2 ^{\circ}$ and $\varphi\_i = 0^{\circ}$.

{{< galleryscg >}}
{{< figscg src="/img/draw/HexagonalLatticesWithBasis_setup.jpg" width="350px" caption="Real-space model">}}
{{< figscg src="/img/auto/scatter2d/HexagonalLatticesWithBasis.png" width="350px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/HexagonalLatticesWithBasis.py" >}}
