+++
title = "2D rotated square lattice"
weight = 30
+++

### Interference 2D rotated square lattice

Scattering from cylinders positioned in a square lattice
whose main axes are rotated with respect to the reference cartesian frame.


{{< galleryscg >}}
{{< figscg src="/img/draw/Interference2DRotatedSquareLattice_setup.jpg" width="350px" caption="Real-space model">}}
{{< figscg src="/img/auto/scatter2d/Interference2DRotatedSquareLattice.png" width="350px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/Interference2DRotatedSquareLattice.py" >}}
