+++
title = "2D square finite lattice"
weight = 30
+++

### Interference - Square Finite Lattice

This example demonstrates how to perform a simulation of scattering from cylinders
positioned in a finite square lattice (as opposed to the standard infinite lattice model).

This example is similar to [Interference of a rotated square lattice](/ref/sample/interference/lattice2d/interference-2d-rotated-square-lattice).

{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/Interference2DSquareFiniteLattice.png" width="4500px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/Interference2DSquareFiniteLattice.py" >}}
