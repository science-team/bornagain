+++
title = "Position variance"
weight = 30
+++

## 2D lattice with position variance

A position variance parameter describes fluctuations of particle position
around lattice points.
It gives rise to an attenuation factor of Debye-Waller type.

It can be set using the method `setPositionVariance(variance)`
of class Interference2DLattice.
The argument `variance` is in nm$^2$.

By default the variance is zero.

### Example: square lattice without and with variance

A square lattice of hemispheres on a substrate, for different lattice orientation angles xi.

{{< figscg src="/img/auto/scatter2d/PositionVariance.png">}}

{{< show-ex file="scatter2d/PositionVariance.py" >}}
