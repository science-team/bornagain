+++
title = "Spheres on Hexagonal Lattice"
weight = 50
+++

### Spheres on Hexagonal Lattice

Scattering from spheres in a hexagonal lattice.

* The sample is made of spherical particles with radii of $10$ nm deposited on a substrate
and positioned in a hexagonal lattice.
* This two-dimensional lattice is characterized by a lattice length of $20$ nm. Its a-axis coincides with the x-axis of the reference Cartesian frame.
* The wavelength is equal to 0.1 nm.
* The incident angles are $\alpha\_i = 0.2 ^{\circ}$ and $\varphi\_i = 0^{\circ}$.

{{< galleryscg >}}
{{< figscg src="/img/draw/SpheresAtHexLattice_setup.jpg" width="350px" caption="Real-space model">}}
{{< figscg src="/img/auto/scatter2d/SpheresAtHexLattice.png" width="350px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/SpheresAtHexLattice.py" >}}
