+++
title = "Materials"
weight = 30
+++

## Materials

The refractive properties of a homogeneous `Material` can be specified
through two different functions:

* `RefractiveMaterial`, based on the refractive index,
* `MaterialBySLD`, based on the scattering length density (SLD).

`RefractiveMaterial` is equally suitable for X-rays or neutrons.
However, it does not account for the wavelength dependence of the refractive index.
This leads to incorrect results if there is too much spread in the incoming wavelength,
as is regularly the case in neutron time-of-flight experiments.

`MaterialBySLD` is intended for neutron experiments with significant wavelength spread.
Refractive indices as function of wavelength are computed internally from constant SLDs.

##### Material by refractive index

A `Material` can be created through

```python
material = ba.RefractiveMaterial("name", delta, beta)
```

where `name` is the arbitrary name of the material associated with its complex refractive index $n = 1 - \delta + i\beta$.

Optionally, the constructor takes an additional
[magnetization](/ref/sample/material/magnetization) argument.

##### Material by scattering length density

A `Material` can also be created through

```python
<material> = ba.MaterialBySLD("name", sld_real, sld_imag)
```

Here `sld_real` and `sld_imag` are the real and imaginary parts
of the material scattering length density (SLD) according to the following convention
([Sears 1992](/lit/sears1992)):

$$SLD = sld\_\{real\} - i \cdot sld\_\{imag\}$$

In this convention, the imaginary part of the SLD must be nonnegative.

SLD units are inverse square angstroms: $ \overset{\circ}{A^{-2}} $.

Optionally, the constructor takes an additional
[magnetization](/ref/sample/material/magnetization) argument.

Magnetization units are ampere per meter: $ A/m $.

SLD values for a wide variety of materials can be calculated
with numerous online SLD-calculators, e.g. these ones:

* [sld-calculator.appspot.com](https://sld-calculator.appspot.com/)
* [SLD calculator by NIST](https://www.ncnr.nist.gov/resources/activation/)

The first of these returns values in inverse square angstroms,
which are also the input units for `MaterialBySLD`.
Thus the SLD values found with the calculator
can be directly copied and pasted into a BornAgain script.

##### Default = Vacuum

Both `RefractiveMaterial` and `MaterialBySLD` can be created with empty constructors:

```python
material = ba.RefractiveMaterial()
# or
material = ba.MaterialBySLD()
```

In this case the "default" material is created, i.e. a material with the name `vacuum`, zero SLD/refractive index and zero magnetization.

##### Restrictions and Limitations

Although `MaterialBySLD` and `RefractiveMaterial` are interchangeable in the case of neutron experiments with fixed wavelength,
they must not be used together in one sample.

`MaterialBySLD` does not account for causes of damping other than nuclear absorption.
In particular, incoherent and inelastic scattering are neglected.
Usually, such extra damping would change the imaginary part of the refractive index
only little compared with the real part;
neglecting it therefore should have no big effect upon simulation results.
