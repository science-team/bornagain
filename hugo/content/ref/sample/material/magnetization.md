+++
title = "Magnetization"
weight = 80
+++

## Magnetization

Magnetic fields are described by real-valued 3d vectors.
They are always understood in units of A/m.

```python
magnetic_field = ba.R3(x, y, z)
```

Magnetic materials are constructed as
```python
material = ba.RefractiveMaterial(name, delta, beta, magnetic_field)
# or
material = ba.MaterialBySLD(name, sld_real, sld_imag, magnetic_field)
```

Examples are under [Examples > Instrument > Neutron polarization](/ref/instr/ex/pol).
