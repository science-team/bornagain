+++
title = "Sample"
weight = 35
+++

## Class Sample (toplevel sample model)

In BornAgain, for the time being, a sample is always represented by an
instance of class `Sample`, even if it is a single-layer SAS sample.

##### Create an instance and add layers

The sample is created by the constructor call
```python
sample = ba.Sample()
```

Layers are then added using either of
```python
sample.addLayer(layer)
```

For the arguments, see sections [Layer](layer) and [Roughness](../roughness).

The layer added first is the top layer (typically vacuum).
The layer added last is the bottom layer (typically substrate).

##### Set properties

Global properties of the sample are set through the following functions:

```python
sample.setRoughnessModel(roughnessModel)
```
which takes an argument of type [RoughnessModel](../roughness/specular_2models);

```python
sample.setCrossCorrLength(crossCorrLength)
```
where the argument is the vertical roughness correlation length in nm;

```python
sample.setExternalField(ext_field);
```
where the argument is the external magnetic field in A/m.
