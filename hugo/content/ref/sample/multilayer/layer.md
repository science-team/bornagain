+++
title = "Layer"
weight = 36
+++

## Class Layer

A [Sample](/ref/sample/multilayer) is composed of one or several instances
of class `Layer`.

##### Create an instance

Each instance is created using the constructor call
```python
layer_i = ba.Layer(material, thickness=0, roughness=nullptr)
```
Typically, the variable name will contain an index `i`.
By convention, the numbering starts with the top layer, typically `layer_1`.

For the first argument, see [Material](/ref/sample/material).

The optional second argument is the layer thickness in nm.
If it is omitted, then the default value of 0 indicates an infinite thickness;
this is to be used for the bottom or top layer,
or for the sole component of a single-layer sample.

##### Set properties

To add a [ParticleLayout](/ref/sample/multilayer/layout), use
```python
layer_i.addLayout(layout)
```
If there are several layouts, then their scattering intensities will be added incoherently.

To divide the layer into `n` slices for the reflectivity computation, use
```python
layer_i.setNumberOfSlices(n)
```
