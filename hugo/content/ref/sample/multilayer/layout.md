+++
title = "Layout"
weight = 37
+++

## Class ParticleLayout

A [Layer](layer) may contain any number of instances of class `ParticleLayout`.
If a layer has more than one layout, then their scattering intensities add incoherently.

##### Create an instance and add particles

To create a particle layout and add kinds of particles, use
```python
layout = ba.ParticleLayout()
layout.addParticle(particle, abundance)
```

For the `particle` argument, see [Particle](../particle).

If the `abundance` argument is omitted, then the abundance will be taken from the
particle, which by default has an abundance of 1.

If there is one single kind of particles, then the `addParticle` call
can be replaced by a constructor call with `particle` argument:
```python
layout = ba.ParticleLayout(particle)
```


##### Set properties

To set an inter-particle structure, use
```python
layout.setInterference(interparticle)
```
For the argument, see [Particle assemblies](../interference).

To set the total particle surface density (in units of nm$^{-2}$), use
```python
layout.setTotalParticleSurfaceDensity(particle_density)
```
