+++
title = "Particles"
weight = 50
+++

## Particle class

##### Usage

The standard way to add particles to a sample model is
```python
particle = ...

layout = ba.ParticleLayout()
layout.addParticle(particle, abundance)

layer = ba.Layer(material, thickness)
layer.addLayout(layout)

sample = ba.Sample()
sample.addLayer(layer)
```

##### Vertical alignement

This will be revised soon. For the time being:

The bottommost point of a particle is placed at the bottom interface
of the layer to which is added.
Exception: if a particle is added to a bottom (substrate) layer,
then the bottom of the particle is placed at the top interface of the layer.

Alignement can be changed with the `translate` function (see below).

##### Create a Particle instance

The `particle` in the above code snippet can be a simple or a composite particle.

To create a simple particle, use
```python
particle = ba.Particle(material, formfactor)
```

For the constructor arguments, see sections [Material](/ref/sample/material)
and [Formfactor](/ref/sample/particle/ff).

To create a composed particle, see
- [CoreShellParticle](/ref/sample/particle/composition/core-shell),
- [Compound](/ref/sample/particle/composition/compound) - also used as base in lattices,
- Mesocrystal.

[Magnetization](/ref/sample/material/magnetization) is supported as a material property.

##### Set location and orientation

To translate any particle, simple or composite, use either the vectorial form
```python
from bornagain import R3
translation = R3(x, y, z)
particle.translate(translation)
```
or
```python
particle.translate(x, y, z)
```

If z>0, then the particle is translated upwards.

To change the orientation, use code like
```python
rotate = ba.RotationEuler(alpha, beta, gamma)
rotate = ba.RotationX(angle) # alternative
particle.rotate(rotation)
```

For more information on the argument, see [Rotation](/ref/other/rotation).

Translations and rotations may be applied in any order. They are not commutative.
