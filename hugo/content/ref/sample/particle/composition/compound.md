+++
title = "Compound particle"
weight = 80
+++

## Compound particle

This tutorial demonstrates how to construct complex shape particles.

All particles implemented in BornAgain are defined by their form factors,
their sizes and the materials they are made of.
The form factor library provides access to a list of about 20 elementary shapes.
A special `Compound` object allows to compose elementary particles into complex shapes
and treat the resulting object as a single particle:
it can be rotated, translated and included in the layer via a particle layout.
The scattering from such a particle will account for coherent interference between sub particles.

### Creating particle composition

The `Compound` object is created via one of its constructors.

```python
# creates an empty particle composition
composition = Compound()

# creates a composition made of a single particle at a given position
composition = Compound(particle, position = R3(0,0,0))

# creates a composition and includes copies of a particle at specified positions
Compound(particle, positions = [pos1, pos2, ...])
```

The position of the particle defines the relative position
of the particle's reference point in the coordinate system of the particle composition.

`Compound` can be modified via following methods

```python
# add particle to the composition at specified position
composition.addParticle(particle, position = R3(0,0,0))

# add copies of the particles at specified positions
composition.addParticles(particle, positions = [pos1, pos2, ...])
```

### Examples

In following plot we demonstrate the creation of 3 different compositions

{{< figscg src="/img/draw/tutorial_composition_examples.png" class="center">}}

The hollow cross-shape on the left is constructed by adding four copies of the same box
shaped particle at 4 different positions

```python
length, width, height = 10.0*nanometer, 10.0*nanometer, 3.0*nanometer
box = Particle(box_material, Box(length, width, height))

positions = [R3(length, 0, 0), R3(0, -width, 0), R3(-length, 0, 0), R3(0, width, 0)]
composition = Compound(box, positions)
```

The stack of boxes at the center is constructed by placing 3 boxes made of different materials on top of each other.

```python
length, width, height = 10.0*nanometer, 10.0*nanometer, 5.0*nanometer
ff_box = Box(length, width, height)

composition = Compound()
composition.addParticle(Particle(material1, ff_box), R3(0, 0, 0))
composition.addParticle(Particle(material2, ff_box), R3(0, 0, height))
composition.addParticle(Particle(material3, ff_box), R3(0, 0, 2.0*height))
```

The full sphere on the right is composed of two truncated spheres,
each made of a different material. The bottom half is rotated first
by 180 degrees about the Y axis before adding it to the composition.

```python
radius = 16.0*nanometer
top_half = Particle(material1, SphericalSegment(radius, radius))
bottom_half = Particle(material2, SphericalSegment(radius, radius))
bottom_half.setRotation(RotationY(180.*degree))

composition = Compound()
composition.addParticle(top_half)
composition.addParticle(bottom_half)
```
