+++
title = "Compound particle"
weight = 10
+++

## Compound particle example

In this example it is modelled a multi layer consisting of a substrate layer and an air layer.
Cylindrical particles made of two materials are added to the air layer
and their $z$ coordinate is shifted downwards in order to cross the air-substrate interface.

{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/BiMaterialCylinders.png" width="450px" >}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/BiMaterialCylinders.py" >}}
