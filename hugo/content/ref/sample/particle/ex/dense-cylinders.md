+++
title = "Dense cylinders"
weight = 55
+++

## Dense cylinders

Supposing a `Simulation` has been defined in which some layers contain embedded particles of different materials; to regard those layers as composed by a single material, the `setUseAvgMaterials` method is used:

```
simulation.options().setUseAvgMaterials(True)
```

{{< figscg src="/img/auto/scatter2d/CylindersInAverageLayer.png" width="500px" class="center" caption="The figure shows the intensity map produced by the script below." >}}

The script below shows how to average materials when simulating scattering from a square lattice of cylinders inside a finite layer.

{{< show-ex file="scatter2d/CylindersInAverageLayer.py" >}}
