+++
title = "Crossing an interface"
weight = 10
+++

### Particles crossing an interface

In this example, very similar to our [basic GISAS example](/ref/sim/class/scattering/ex/gisas),
it is shown how to position particles in order to cross multilayer interfaces: the $z$ position of particles originally located within the air layer must be adjusted slightly downwards in order to cross the air-substrate interface.

The simulation kernel automatically detects particles crossing interfaces and adjusts the calculations accordingly, causing a drop on speed to complete each simulation.

The script below models a air-substrate bilayer in which cylindrical particles made of two materials are added to the air layer and their $z$ coordinate is shifted downwards in order to cross the air-substrate interface.


{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/ParticleAcrossInterface.png" width="450px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/ParticleAcrossInterface.py" >}}
