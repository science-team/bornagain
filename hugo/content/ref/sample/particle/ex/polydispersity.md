+++
title = "Polydispersity"
weight = 80
+++

## Polydisperse particles example

This example shows GISAS by polydisperse cylinders on a substrate.
The radii of the cylinders follow a Gaussian distribution.

The uncorrelated particles are added to a layout using a loop
over the parameter distribution samples. This is currently supported
only under Python scripting, not in the GUI.

Class references: [Layout](../../multilayer/layout),
[Distributions](/ref/other/distributions).

{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/PolydisperseCylinders.png" width="450px">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/PolydisperseCylinders.py" >}}
