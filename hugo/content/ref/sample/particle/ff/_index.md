+++
title = "Form factor"
weight = 5
+++

## Particle form factor

#### Theory

A particle shape is described
through the density function $n(\mathbf{r})$ in real space,
or the form factor
$$F(\mathbf{q}) = \int d^3r\thinspace e^{i\mathbf{q}\mathbf{r}} n(\mathbf{r})$$
in reciprocal space.

For a "hard" particle, $n(\mathbf{r})$ is an indicator function
with value 1 inside, 0 outside.
For a "soft" particle, $n(\mathbf{r})$ has a smooth transition from the inside limit of&nbsp;1
to the outside limit of&nbsp;0.

At large $q$, the exponential factor in the integral defining $F(\mathbf{q})$
oscillates rapidly. This leads to substantial errors if $q$ is of the same order
or larger than the inverse detector bin length.
In such cases, we recommend [Monte-Carlo integration over detector bins](/ref/sim/setup/options/mc).

By construction, $F(\mathbf{q})$ is a holomorphic function.
Therefore expressions previously derived for real wave vectors $\mathbf{q}$
also hold for complex $\mathbf{q}$.

#### Implementation

The interface class {{% ref-class "Sample/Particle" "IFormfactor" %}}
cannot be instantiated directly.
Instead, one has to choose one of the following child classes:

- hard particles:
  - spheroids:
  - cylinders and cones:
  - polygonal prisms:
  - pyramids (frusta):
    * rectangular
    * trigonal
    * quadratic: Pyramid4, Bipyramid4
    * hexagonal
  - other polyhedra:
- soft particles:
  - spherical:

Using Python scripting, it is also possible to define a [custom form factor](custom).

#### Usage

Form factors are used to define the shape of particles:
```python
ff = ba.Cylinder(5*nm, 5*nm)
particle = ba.Particle(material_Particle, ff)
```

#### Literature

Form factor catalogues in earlier software:
- IsGISAXS: [Lazzari 2006](/lit/lazzari2006) (manual),
  [Renaud et al 2009](/lit/renaud2009) (review).
- FitGISAXS: [Babonneau 2010](/lit/babonneau2010) (paper),
  [Babonneau 2013](/lit/babonneau2013) (software archive).

Stable algorithm for computing form factors of polyhedra: [Wuttke 2021](/lit/wuttke2021).
