+++
title = "Custom formfactor"
weight = 60
+++

### Custom formfactor

Scattering from a monodisperse distribution of particles, whose form factor is defined by the user.

* This example shows how users can simulate their own particle shape by implementing the analytical expression of its form factor.
* The particular shape used here is a polyhedron, whose planar cross section is a "plus" shape with a side length of $20$ nm and a height of $15$ nm.
* These particles are distributed on a substrate.
* There is no interference between the scattered waves.
* The wavelength is equal to 0.1 nm.
* The incident angles are $\alpha\_i = 0.2 ^{\circ}$ and $\varphi\_i = 0^{\circ}$.

{{< galleryscg >}}
{{< figscg src="/img/draw/CustomFormfactor_setup.jpg" width="350px" caption="Real-space model">}}
{{< figscg src="/img/auto/scatter2d/CustomFormfactor.png" width="350px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/CustomFormfactor.py" >}}
