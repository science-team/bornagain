+++
title = "Box"
+++

### Box

A rectangular cuboid.

<img src="/img/draw/ff/3d/Box.png" width="30%" >
<img src="/img/draw/ff/2d/Box_xy.svg" width="30%">
<img src="/img/draw/ff/2d/Box_xz.svg" width="30%">

#### Constructor

```python
Box(L, W, H)
```

Parameters:
- L, length of the base
- W, width of the base
- H, height

#### Usage

As for any other [Form factor](/ref/sample/particle/ff).

#### Implementation

Class [Box]({{% url-src %}}/Sample/HardParticle/Polyhedra.h) inherits from the interface class
{{% ref-class "Sample/Particle" "IFormfactor" %}}.

Form factor is computed as
$$F(\mathbf{q})=LWH \space \exp\Big(iq_{z}\dfrac{H}{2}\Big) \space \text{sinc}\Big(q_{x}\dfrac{L}{2}\Big) \space \text{sinc}\Big(q_{y}\dfrac{W}{2}\Big) \space \text{sinc}\Big(q_{z}\dfrac{H}{2}\Big).$$

Volume [has been validated]({{% url-src %}}/Tests/Unit/Sample/FormfactorBasicTest.cpp)
against
$$V=LWH.$$

#### Related shapes

More general:
- [Pyramid2](/ref/sample/particle/ff/hard/pyramid2) or [Pyramid4](/ref/sample/particle/ff/hard/pyramid4), if sides are not vertical.

More special:
<!--- [Bar](/ref/sample/particle/ff/ripples/bar), if elongated in L or W direction.-->
- Bar, if elongated in L or W direction.

Modified cubes:
- [TruncatedCube](/ref/sample/particle/ff/hard/truncatedcube), if $L=W=H$ and corners are facetted.
- [CantellatedCube](/ref/sample/particle/ff/hard/cantellatedcube), if $L=W=H$ and corners and edges are facetted.
 
#### Example

Scattering by uncorrelated, oriented boxes for horizontal incidence. Rotation around $z$ axis:

<img src="/img/auto/ff/Box.png">

Generated by {{% ref-ex "ff/Box.py" %}}.

#### History

Agrees with "Box" form factor of IsGISAXS [[manual](/lit/lazzari2006), Eq. 2.37;
[Renaud 2009](/lit/renaud2009), Eq. 214],
except for factors $1/2$ in the definition of parameters $L,\space W,\space H$.
