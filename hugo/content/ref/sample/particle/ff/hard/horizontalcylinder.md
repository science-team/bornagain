+++
title = "HorizontalCylinder"
+++

### HorizontalCylinder

A horizontal truncated circular cylinder.
<!--
<img src="../3d/HorizontalCylinder.png" width="30%" >
<img src="../cut/HorizontalCylinder_xy.svg" width="30%">
<img src="../cut/HorizontalCylinder_xz.svg" width="30%">
-->
#### Constructor

Full form ```python
HorizontalCylinder(R, H, b, t) 
```
and short form 
```python
HorizontalCylinder(R, H) == HorizontalCylinder(R, H, -R, R) 
```


Parameters:
- R, radius
- H, height
- b, bottom boundary, relative to the center
- t, top boundary, relative to the center

Constraint:

$ -R \le b \le t \le R $ 

#### Usage

As for any other [Form factor](/ref/sample/particle/ff).

#### Implementation

Class {{% ref-class "Sample/HardParticle" "HorizontalCylinder" %}} inherits from the interface class
{{% ref-class "Sample/Particle" "IFormfactor" %}}.

Form factor is computed as

$$ F(\mathbf{q})=2 L \space \text{sinc}\Big(q_x\dfrac{L}{2}\Big) 
\int_{b}^{t} \text{d}z \space \sqrt{R^2-z^2} \space \text{sinc}(q_y\sqrt{R^2-z^2})  \exp\[iq_z(z-b)\] , $$

Volume [has been validated]({{% url-src %}}/Tests/Unit/Sample/FormfactorBasicTest.cpp)
against
$$V=L \Big\( t\sqrt{R^2-t^2}- b\sqrt{R^2-b^2} + R^2 \Big\[\text{asin}\Big(\frac{t}{R}\Big) - \text{asin}\Big(\frac{b}{R}\Big) \Big\]
 \Big\).$$

#### Related shapes

More general:
- [Cylinder](/ref/sample/particle/ff/hard/cylinder), if vertical and non-truncated.
 
#### Example

Scattering by uncorrelated, oriented horizontal truncated cylinders for horizontal incidence. Rotation around $z$ axis:

<img src="/img/auto/ff/HorizontalCylinder.png">

Generated by {{% ref-ex "ff/HorizontalCylinder.py" %}}.

#### History

Introduced in BornAgain 1.20.
