+++
title = "Spheroid"
+++

### Spheroid

A full spheroid, generated by rotating an ellipse around the vertical axis.

<img src="/img/draw/ff/3d/Spheroid.png" width="30%" >
<img src="/img/draw/ff/2d/Spheroid_xy.svg" width="30%">
<img src="/img/draw/ff/2d/Spheroid_xz.svg" width="30%">

#### Constructor

```python
Spheroid(R_xy, R_z)
```

Parameters:
- $R_{xy}$, horizontal radius
- $R_z$, vertical radius

**Attention** This has changed in BornAgain 22.
Previously, the second argument was $H=2R_z$.

#### Usage

As for any other [Form factor](/ref/sample/particle/ff).

#### Implementation

Class {{% ref-class "Sample/HardParticle" "Spheroid" %}} inherits from the interface class
{{% ref-class "Sample/Particle" "IFormfactor" %}}.

Form factor is computed as

$$ F(\mathbf{q})=4\pi R_{xy}^2R_z \exp(iq_zR_z) \dfrac{\sin(s) - s \cos(s)\}{s^3} , $$
with the notation
$$ s := \sqrt{(R_{xy}q_x)^2 + (R_{xy}q_y)^2+ (R_zq_z)^2}. $$

Volume [has been validated]({{% url-src %}}/Tests/Unit/Sample/FormfactorBasicTest.cpp)
against
$$ V=\dfrac{4\pi}{3} R_{xy}^2R_z. $$

#### Related shapes

More special:
- [Sphere](/ref/sample/particle/ff/hard/sphere), if $R_z=R_{xy}.$

More general:
- [SpheroidalSegment](/ref/sample/particle/ff/hard/spheroidalsegment), if truncated from the top and/or the bottom.

#### Example

Scattering by uncorrelated, oriented spheroids for horizontal incidence. Rotation around $y$ axis:

<img src="/img/auto/ff/Spheroid.png">

Generated by {{% ref-ex "ff/Spheroid.py" %}}.

#### History

Replicates the "Full spheroid" in IsGISAXS [[manual](/lit/lazzari2006), Eq. 2.36; [Renaud 2009](/lit/renaud2009), Eq. 227], except for wrong
factors of 2 in their volume formula and form factor implementation, and "Spheroid" in FitGISAXS [[Babonneau 2013](/lit/babonneau2013)]. Up to BornAgain
1.16, our form factor computation followed IsGISAXS in using numeric integration in
the $z$ coordinate.

In BornAgain, named "FullSpheroid" up to version 1.19.

Up to BornAgain 21, the second argument was $H=2R_z$.
