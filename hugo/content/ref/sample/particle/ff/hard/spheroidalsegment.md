+++
title = "SpheroidalSegment"
+++

### SpheroidalSegment

A vertically oriented, horizontally truncated spheroid.

<img src="/img/draw/ff/3d/SpheroidalSegment.png" width="30%" >
<img src="/img/draw/ff/2d/SpheroidalSegment_xy.svg" width="30%">
<img src="/img/draw/ff/2d/SpheroidalSegment_xz.svg" width="30%">

#### Constructor

```python
SpheroidalSegment(R_xy, R_z, t, b)
```

Parameters:
- $R_{xy}$, horizontal radius
- $R_z$, vertical radius
- $ t $, cut from top
- $ b $, cut from bottom

Constraint:

$ t+b \le 2R_z $

#### Usage

As for any other [Form factor](/ref/sample/particle/ff).

#### Implementation

Class {{% ref-class "Sample/HardParticle" "SpheroidalSegment" %}} inherits from the interface class
{{% ref-class "Sample/Particle" "IFormfactor" %}}.

Computation involves numerical integration in vertical direction,

$$ F(\mathbf{q})=2\pi \exp\[iq_z(R_z-b)\]  \int_{-R_z+b}^{R_z-t} \text{d}z \space r_z^2 \frac{J_1(q_{||}r_z)}{q_{||}r_z} \exp(iq_z z), $$

with the notation

$$ q_{||} := \sqrt{q_x^2 + q_y^2}, \quad r_z:=R_{xy}\sqrt{1-z^2/R_z^2} $$

Volume [has been validated]({{% url-src %}}/Tests/Unit/Sample/FormfactorBasicTest.cpp)
against
$$ V=\dfrac{\pi}{3} \Big(\frac{R_{xy}}{R_z}\Big)^2 \[ 4R_z^3 - 3R_z(t^2+b^2) + (t^3 + b^3) \]. $$

#### Related shapes

- [HemiEllipsoid](/ref/sample/particle/ff/hard/hemiellipsoid), top half of ellipsoid.

More special:
- [Spheroid](/ref/sample/particle/ff/hard/spheroid), if $t=b=0.$
- [SphericalSegment](/ref/sample/particle/ff/hard/sphericalsegment), if $R_z=R_{xy}$.

#### Example

Scattering by uncorrelated, oriented truncated spheroids for horizontal incidence. Rotation around $y$ axis:

<img src="/img/auto/ff/SpheroidalSegment.png">

Generated by {{% ref-ex "ff/SpheroidalSegment.py" %}}.

#### History

Agrees with the IsGISAXS form factor "Spheroid" [[manual](/lit/lazzari2006), Eq. 2.42].
