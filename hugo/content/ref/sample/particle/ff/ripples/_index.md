+++
title = "Ripples"
weight = 20
+++

## Ripples

Elongated particles, or ripples, are typically used to model lamellar cuts or gratings.

As everywhere else in BornAgain
only single scattering in the DWBA is simulated.
This can be insufficient for periodic gratings
that cause noticeable higher-order diffraction.
But for the foreseeable future, multiple-scattering effects are not in the scope of BornAgain.

We choose ripples to be elongated in $x$ direction.
Different profiles in the $YZ$ plane can be set, for example, bar, cosine, sawtooth.
For each of them, different profiles can also be chosen in the $XZ$ plane.

[The approach to setting the ripple profiles will be changed soon]({{% url-issues %}}/60)

#### Implementation

Form factor is computed as 

$$  F(\mathbf{q}) = f_\parallel(q_x) f_\bot(q_y,q_z) $$

The longitudinal factor $ f_\parallel $ is defined in $XZ$ plane for the following shapes:

##### Box

$$ f_\parallel(q_x)=L \space \text{sinc}(q_xL/2) $$

##### Gauss

$$ f_\parallel(q_x)= \space \frac{L}{\sqrt{2 \pi}} \text{e}^{- \frac{{(q_x L)}^{2}}{2}} $$

##### Lorentz

$$ f_\parallel(q_x)= \space \frac{L}{1+{(q_x L)}^{2}} $$

The transversal factor $ f_\bot(q_y,q_z) $ is defined by the ripple type

{{% children %}}

#### History

Full documentation and API support for all ripple form factors appeared in
BornAgain 1.17. Before that release, the Lorentz factor $ f_\parallel $ had an extra factor of 2.5.
The Gauss factor  $ f_\parallel $ was completely wrong up through BornAgain 20.
