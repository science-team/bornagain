+++
title = "FuzzySphere"
+++

### FuzzySphere

A sphere with a fuzzy surface.

#### Constructor

```python
FuzzySphere(R, sigma)
```

Parameters:
- R, mean radius.
- sigma, fuzziness length.

#### Usage

As for any other [Form factor](/ref/sample/particle/ff).

#### Implementation

Class {{% ref-class "Sample/SoftParticle" "FuzzySphere" %}} inherits
from the interface class {{% ref-class "Sample/Particle" "IFormfactor" %}}.

The form factor is computed as
$$F(q)=F_{\rm sphere}(q) \exp(-\sigma^2 q^2 /2)$$
where $F_{\rm sphere}(q)$ is the form factor of a [hard sphere](../hard/sphere).

#### History

Previously named "SphereGaussianRadius".
In BornAgain 1.20 renamed and simplified to match the
[fuzzy_sphere](https://www.sasview.org/docs/user/models/fuzzy_sphere.html?highlight=fuzzy%20sphere)
model of SasView.

The SasView manual gives credit to
M Stieger, J. S Pedersen, P Lindner, W Richtering,
[Langmuir 20, 7283-7292 (2004)](https://doi.org/10.1021/la049518x).