+++
title = "Gauss"
+++

### Gauss

A soft particle with Gaussian density distribution
$$n(\mathbf{r})=e^{-r^2/(2R^2)}.$$

#### Constructor

```python
Gauss(R)
```

Parameters:
- R, mean radius.

#### Usage

As for any other [Form factor](/ref/sample/particle/ff).

#### Implementation

Class {{% ref-class "Sample/SoftParticle" "Gauss" %}} inherits
from the interface class {{% ref-class "Sample/Particle" "IFormfactor" %}}.

The form factor is computed as
$$F(q)=R^3 \exp\left(-|q|^2R^2/(4\pi)\right).$$
