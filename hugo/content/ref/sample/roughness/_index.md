+++
title = "Roughness"
weight = 40
+++

# Rough interfaces

Rough interfaces modify the Fresnel transmission and reflection coefficients,
and cause diffuse scattering.

### Interface models

For detailed reference, see [API](api).

Each layer (except for the semi-infinite top layer)
supports an optional roughness property that can be set as follows:

```python
autocorr = ba.SelfAffineFractalModel(vertical_rms, hurst, horizontal_correlation_length)
transient = ba.TanhTransient()
crosscorrelation = ba.CommonDepthCrosscorrelation(vertical_correlation_length)

roughness = ba.Roughness(autocorr, transient, crosscorrelation)
layer = ba.Layer(material, thickness, roughness) # roughness refers to top interface
```

{{% children %}}
