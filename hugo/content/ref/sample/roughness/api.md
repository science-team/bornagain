+++
title = "API"
weight = 10
+++

# Rough interface API

The rough interface constructor has the signatures

```python
roughness = ba.Roughness(interface_model, transient)
roughness = ba.Roughness(interface_model, transient, crosscorrelation)
```

For the three arguments, see the following three pages:
- [Interface model](../interface)
- [Transient](../transient)
- [Cross-correlation](../xcorr)
