+++
title = "Interface model"
weight = 11
+++

# Rough interface models

### Interface model

The mathematical model for one interface must be specified through one of

```python
ba.SelfAffineFractalModel
ba.LinearGrowthModel
```

These models define the behavior of autocorrelation function of roughness
or, better to say, its Fourier spectrum in the full range of spatial frequencies from 0 to infinity.

##### Self-affine fractal model

```python
ba.SelfAffineFractalModel(sigma, hurst, lateral_corr_length, max_spatial_freq=0.5)
```

where
- `sigma`, $\sigma$, is the root-mean-square amptitude of out-of-plane (transversal) fluctuations,
- `hurst`, $H$, is a fractal exponent `H` with `0<H<1`,
- `lateral_corr_length`, $\xi$, is the lateral (in-plane) correlation length,
- `max_spatial_freq`, $\nu_{max}$,  is a cut-off spatial frequency.

This is the K-correlation model of
[Palasantzas 1993](https://doi.org/10.1103/PhysRevB.48.14472).
The autocorrelation spectrum is

$$
S(\nu)=\dfrac{4 \pi H \sigma^2\xi^2}{(1+(2\pi\nu)^2)^{1+H}} ,\quad  for \quad  \nu<\nu_{max}
$$

The main property is that it remains nearly constant at low $\nu$
and transitions into a straight line on a log-log scale at high $\nu$,
exhibiting an inflection point between these two regions.

##### Linear growth model

```python
ba.LinearGrowthModel(particle_volume, damp1, damp2, damp3, damp4, max_spatial_freq=0.5)
```

where
- `particle_volume`, $\Omega$, is the volume of particle (atom, molecule, cluster)
coming to the surface during film deposition,
- `damp1-damp4`, $a_1-a_4$, are damping coefficients, responsible for replication
of underlying roughness and the growth of the new independent one,
- `max_spatial_freq`, $\nu_{max}$,  is a cut-off spatial frequency.

This is the model described by [Stearns 1993](https://doi.org/10.1063/1.109593)
and extended by [Stearns and Gullikson 2000](https://doi.org/10.1016/S0921-4526(99)01897-9).

The model describes the evolution of the roughness spectrum on the top surface of a growing film.
Its autocorrelation spectrum depends not only on the model parameters but also on the film thickness
and the spectrum of the underlying interface.
Therefore, the model _cannot_ be applied directly to the substrate.

The autocorrelation spectrum is

$$
S_{above}(\nu)=S_{below}(\nu)e^{-b(\nu)t} + \Omega\dfrac{1-e^{-b(\nu)t}}{b(\nu)}, \quad
      for \quad  \nu<\nu_{max}
$$
where
$t$ - film thickness,
$b(\nu)$ - relaxation function

An essential property of the model is that it describes not only autocorrelation
but also cross-correlation properties.
Therefore, it does not require [cross-correlation](../xcorr) to be specified separately.
