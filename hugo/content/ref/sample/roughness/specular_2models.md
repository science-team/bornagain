+++
title = "Different roughness models"
weight = 120
+++

### Specular reflectivity with different roughness models

This example demonstrates how to apply different roughness models
in a specular reflectivity calculation. The considered sample is
exactly the same as the one described in the
[reflectometry tutorial](/ref/sim/class/specular),
and the [basic roughness tutorial](/ref/sample/roughness).
Hewever, now the computation is performed twice with the standard $tanh$ interface profile
and the Névot-Croce roughness model that arises from a Gaussian distribution of the
deviation from the mean-surface position.

{{< figscg src="/img/auto/specular/RoughnessModel.png" width="650px">}}

In both cases, the root-mean-square deviation from the mean surface position is chosen
to be $\sigma = 1$ nm.

{{< show-ex file="specular/RoughnessModel.py" >}}
