+++
title = "Reflectivity example"
weight = 110
+++

### Specular reflectivity with interface roughness

This example demonstrates how to compute reflected signal from
a multilayered sample with surface roughness. All the experiment
layout is exactly the same as the one described in
[reflectometry tutorial](/ref/sim/class/specular),
but now all the layers (except the ambient media) have roughness on the top surface. The
roughness is characterized by root-mean-square deviation from the mean surface position
$\sigma = 1$ nm.

{{<figscg src="/img/auto/specular/SpecularSimulationWithRoughness.png" width="350px">}}

When comparing the result of the simulation to the result obtained in the
[reflectometry tutorial](/ref/sim/class/specular),
one can notice up to two orders of magnitude attenuation of the reflected signal due to
the roughness of the sample.

{{< show-ex file="specular/SpecularSimulationWithRoughness.py" >}}
