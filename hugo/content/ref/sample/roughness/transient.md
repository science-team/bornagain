+++
title = "Transient"
weight = 12
+++


# Interlayer transients

The interlayer transient must be specified through one of

```python
ba.ErfTransient()
ba.TanhTransient()
```
