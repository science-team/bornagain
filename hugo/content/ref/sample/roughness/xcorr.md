+++
title = "Cross-correlation"
weight = 13
+++

# Inter-interface correlation

The correlation between different interfaces
is an optional argument of the `Roughness` constructor.

It can be specified through one of

```python
CommonDepthCrosscorrelation(cross_corr_depth)
SpatialFrequencyCrosscorrelation(base_crosscorr_depth, base_frequency, power)
```