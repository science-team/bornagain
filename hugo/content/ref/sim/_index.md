+++
title = "Simulation model"
weight = 10
+++

## Simulation model

To set up and run a BornAgain simulation, one needs to create an instance of
a simulation class. This class contains information about the scattering
target ("sample") and about the simulated instrument (beam or scan, detector).

#### Simulation classes

The following simulation classes are available:

* [Specular](class/specular),
* [Scattering](class/scattering) (SAS and GISAS),
* [Off-specular scattering](class/offspec),
* [Depth probe](class/depthprobe) - vertical intensity profile
  as function of incident glancing angle.

#### Common functions

The following functions are common to all simulation classes:

Setters:
* [setBackground(_background_)](setup/bg)
* [setTerminalProgressMonitor()](setup/progress)
* [options()](setup/options) - gives access to option setters

Getter:
* `simulate()` - runs simulation and returns
  [Datafield](/ref/data/datafield)
