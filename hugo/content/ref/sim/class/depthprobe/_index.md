+++
title = "Depth probe"
weight = 80
+++

## Depth probe simulation

Depth probe simulation is an auxiliary simulation type, which helps to visualize
the total intensity in dependence on the beam incidence angle and the position in
the sample.

#### Compute intensity

To compute the total wave intensity as function of $\alpha_\text{i}$ and z,
use
```python
import bornagain as ba
scan = ...
sample = ...
z_axis = ba.EquiDivision("z (nm)", nz, z_min, z_max)
simulation = ba.DepthprobeSimulation(scan, sample, z_axis)
# ... set options
result = simulation.simulate()
```

Supported scan types: grazing angle scan, wavelength scan, wavenumber qz scan.

For the constructor arguments, see sections [scan](/ref/instr/scan),
[sample](/ref/sample).

For optional settings, see [simulation options](/ref/sim/setup/options).

For the return type of function `simulate()`,
see [Datafield](/ref/data/datafield).

#### Partial waves, modulus, phase

The constructor takes an optional `flags` argument
```
simulation = ba.DepthprobeSimulation(scan, sample, z_axis, flags)
```

Flags may designate a partial beam
```
ba.ZDirection_None        # = 0, may be ommitted: total field
ba.ZDirection_Reflected   # = 1, reflected beam only
ba.ZDirection_Transmitted # = 2, transmitted beam only
```
or/and a property of the simulated wave field
```
ba.WaveProperty_Intensity # = 0, may be ommitted: intensity, |psi|^2
ba.WaveProperty_Modulus   # = 4, modulus of wave amplitude, |psi|
ba.WaveProperty_Phase     # = 8, phase of wave amplitude, arg(psi), in rad
```
To combine flags from each of these groups, combine them with the "or" operator:
```
flags = ba.ZDirection_Reflected | ba.WaveProperty_Modulus
```

#### Examples

- [Basic example: thin film on substrate](/ref/sim/class/depthprobe/ex/depthprobe1)
- [Transmitted amplitude modulus](/ref/sim/class/depthprobe/ex/transmitted_modulus)
- [Resonator](/ref/sim/class/depthprobe/ex/resonator)
