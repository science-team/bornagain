+++
title = "Examples"
weight = 900 
+++

## Depth probe examples

Intensity as function of incident angle $\alpha_\text{i}$ and depth $z$.

Class reference:
- [DepthProbe](/ref/sim/class/depthprobe)

Examples
- [Basic example: thin film on substrate](depthprobe1)
- [Transmitted amplitude modulus](transmitted_modulus)
- [Resonator](resonator)
