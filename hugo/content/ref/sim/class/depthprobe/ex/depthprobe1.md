+++
title = "Thin film"
weight = 10
+++

## Intensity distribution in a thin-film sample.

We consider a thin film on a substrate.

We compute the neutron intensity
as function of depth and incident angle $\alpha_i$.

{{< figscg src="/img/auto/varia/Depthprobe1.png" width="500" class="center">}}

{{< show-ex file="varia/Depthprobe1.py" >}}
