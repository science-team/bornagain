+++
title = "Transmitted modulus"
weight = 10
+++

## Transmitted modulus

Modulus of the amplitude of the partial wave field propagating in
transmission direction,
as function of depth and incident angle $\alpha_i$.

Same sample as in the basic example [depthprobe1](depthprobe1),
a thin film on a substrate.


{{< figscg src="/img/auto/varia/TransmittedModulus.png" width="500" class="center">}}

{{< show-ex file="varia/TransmittedModulus.py" >}}
