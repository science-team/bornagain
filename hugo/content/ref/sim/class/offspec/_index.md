+++
title = "Off-specular"
weight = 30
+++

## Off-specular scattering simulation

To set up and run a simulation with off-specular scattering, use
```python
import bornagain as ba
scan = ...
sample = ...
detector = ba.OffspecDetector(...)
simulation = ba.OffspecSimulation(scan, sample, detector)
# ... set options
result = simulation.simulate()
```

Supported scan types: grazing angle scan, wavelength scan.

For the constructor arguments, see sections [scan](/ref/instr/scan),
[sample](/ref/sample), and [OffspecDetector](/ref/instr/det/offspec).

For optional settings, see [simulation options](/ref/sim/setup/options).

For the return type of function `simulate()`,
see [Datafield](/ref/data/datafield).

##### Examples

* [basic off-specular simulation](/ref/sim/class/offspec/ex/index.md)
