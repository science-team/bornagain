+++
title = "Off-specular"
weight = 20
+++

## Off-specular scattering examples

Class reference: [OffspecSimulation](/ref/sim/class/offspec).

#### Sample model

Off-specular scattering from a monodisperse distribution of long boxes.

* The sample is made of very long boxes with length equal to $1000$ nm, width $20$ nm and height $10$ nm.
* The particles are distributed along a one-dimensional lattice with a lattice spacing of $100$ nm in the $x$-direction.
* The particles are rotated around the $z$-axis by $90^{\circ}$ so that their "infinite" dimension is parallel to the $y$-direction.

{{< galleryscg >}}
{{< figscg src="/img/draw/Offspec_Setup.jpg" width="350px" caption="Real-space model">}}
{{< /galleryscg >}}

#### Measurement models

Example Offspec1 models an instrument that is scanning $\alpha\_i$.

Example OffspecLambda models a time-of-flight neutron reflectometer that is scanning
the incident wavelength $\lambda$.

Reference pages:
- [OffspecSimulation](/ref/sim/class/offspec)
- [OffspecDetector](/ref/instr/det/offspec)
- [AlphaScan, LambdaScan](/ref/instr/scan)

<div style="display: flex; flex-direction: row;">
{{< figscg src="/img/auto/offspec/Offspec1.png" caption="Example Offspec1" style="flex: 40%">}}
<p style="flex: 20%"/>
{{< figscg src="/img/auto/offspec/OffspecLambda.png" caption="Example OffspecLambda" style="flex: 40%">}}
</div>

{{< show-ex file="offspec/Offspec1.py" >}}
{{< show-ex file="offspec/OffspecLambda.py" >}}
