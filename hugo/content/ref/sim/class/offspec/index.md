+++
title = "Off-specular"
weight = 30
+++

## Off-specular scattering simulation

To set up and run a simulation with off-specular scattering, use
```python
import bornagain as ba

sample = ...

# one of
scan = ba.AlphaScan(...)
scan = ba.LambdaScan(...)

detector = ba.OffspecDetector(...)
simulation = ba.OffspecSimulation(scan, sample, detector)

result = simulation.simulate()
```

For the constructor arguments, see sections [scan](/ref/instr/scan),
[sample](/ref/sample), and [OffspecDetector](/ref/instr/det/offspec).

For optional settings, see [simulation options](/ref/sim/setup/options).

For the return type of function `simulate()`,
see [Datafield](/ref/result/datafield).

##### Examples

* [basic off-specular simulation](/ex/sim/offspec)
