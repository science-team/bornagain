+++
title = "Scattering"
weight = 20
+++

## Scattering simulation

To set up and run a scattering simulation, use
```python
import bornagain as ba
beam = ...
sample = ...
detector = ba.SphericalDetector(...)
simulation = ba.ScatteringSimulation(beam, sample, detector)
# ... set options
result = simulation.simulate()
```

For the constructor arguments, see sections [beam](/ref/instr/beam),
[sample](/ref/sample), and [detector](/ref/instr/det).

For optional settings, see [simulation options](/ref/sim/setup/options).

For the return type of function `simulate()`,
see [Datafield](/ref/data/datafield).

##### SAS vs GISAS

At this level, the same syntax applies to SAS and GISAS
(small-angle scattering under normal or grazing incidence).

The difference comes from the beam and sample model:

For SAS, use an incident beam with a grazing angle of $90^\circ$
(see e.g. the basic [sas example](ex/sas)).
For GISAS, the incident beam has a grazing angle of at most a few degrees
(see e.g. the basic [gisas example](ex/gisas)).

The distorted-wave Born approximation (DWBA) is automatically
applied if a sample has more than one layer
(see [gisas-no-dwba-terms](ex/born-approximation) for a GISAS model
with no reflecting layer interface, hence described by the regular Born approximation).
