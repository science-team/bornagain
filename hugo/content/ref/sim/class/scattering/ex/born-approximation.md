+++
title = "without DWBA terms"
weight = 15
+++

## GISAS without DWBA terms

This example shows scattering
from a monolayer that contains a dilute random assembly
of monodisperse cylindrical disks.

This is the same system as in our [basic GISAS example](/ref/sim/class/scattering/ex/gisas)
except that there is no substrate.
In consequence, there are no reflections,
and therefore the DWBA boils down to the ordinary Born approximation.

{{< galleryscg >}}
{{< figscg src="/img/draw/CylindersInBA_setup.jpg" width="350px" caption="Real-space model">}}
{{< figscg src="/img/auto/scatter2d/CylindersInBA.png" width="350px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/CylindersInBA.py" >}}
