+++
title = "GISAS"
weight = 10
+++

## GISAS simulation example

In the following introductory example,
we take a standard sample model from module
[bornagain.std_samples]({{% url-src %}}/Wrap/Python/std_samples.py),
a dilute random assembly of monodisperse cylindrical disks on a substrate.

{{< galleryscg >}}
{{< figscg src="/img/draw/CylindersInDWBA_setup.jpg" width="350px" caption="Real-space model">}}
{{< figscg src="/img/auto/scatter2d/Cylinders.png" width="350px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/Cylinders.py" >}}
<p>

### Explanation

#### Function get_simulation

The simulation of type [ScatteringSimulation](../)
is defined by beam, sample, and detector.

The incoming beam is defined by the constructor `Beam` with arguments intensity,
wavelength, and glancing angle.

To define the wavelength, we use the unit multiplier `nm`.
As it happens, the internal unit for microscopic lengths in BornAgain is one nanometer;
therefore the constant `nm` is just `1`, and `*nm` could be omitted from the code,
but we recommend to leave it for the benefit of human readers.

The spherical detector has nPix=200 bins for both coordinate axis.
The azimuthal angle $\varphi\_\text{f}$ extends from $-2^\circ$ to $+2^\circ$;
the glancing angle $\alpha\_\text{f}$ from $0^\circ$ to $3^\circ$.

#### Main program

The last stance in the script is the main program.

The function `parse_args` digests command-line arguments; in particular,
a command-line argument like `sim_n=100` can be used to overwrite the
default pixel size (which is retrieved by `bp.simargs['n']` in function
`get_simulation`.

The function call `simulation.simulate()` runs the simulation and returns
a [Datafield](/ref/data/datafield) object.


### Further reading

* [GISAS with specular reflection](/ref/sim/setup/options/specular)
* [GISAS without DWBA terms](born-approximation),
  with a single sample layer so that the DWBA boils down to the ordinary Born approximation.
