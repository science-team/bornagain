+++
title = "Monte-Carlo"
weight = 80
+++

## Scattering by large particles with Monte-Carlo integration

Reference: [Monte-Carlo](/ref/sim/setup/options/mc)

This example demonstrates that for large particles (~$1000$ nm) the contribution to the scattered intensity from the form factor oscillates rapidly within one detector bin and analytical calculations (performed for the bin center) give completely a wrong intensity pattern. In this case Monte-Carlo integrations over detector bin should be used.

The simulation generates four plots using different sizes of the particles, (radius $=10$ nm, height $=20$ nm) or (radius $=1$ $\mu$m, height $=2$ $\mu$m), and different calculation methods: analytical calculations or Monte-Carlo integration. The other parameters are identical:

* The sample is made of a monodisperse distribution of cylinders, deposited randomly on a substrate.
* There is no interference between the scattered waves.
* The wavelength is equal to 0.1 nm.
* The incident angles are $\alpha\_i = 0.2 ^{\circ}$ and $\varphi\_i = 0^{\circ}$.

{{< galleryscg >}}
{{< figscg src="/img/draw/LargeParticles_setup.jpg" width="700px" caption="Real-space model">}}
{{< figscg src="/img/auto/scatter2d/LargeParticles.png" width="350px" caption="Intensity image">}}
{{< /galleryscg >}}

{{< show-ex file="scatter2d/LargeParticles.py" >}}
