+++
title = "Polarized SANS"
weight = 45
+++

### Polarized SANS

This example shows how to simulate polarized SANS with BornAgain, using the Born
Approximation.

The main difference between simulating GISAS and SAS in BornAgain is the presence
of only a single layer in the multilayer object. This triggers the software to
calculate the differential scattering cross section in the Born Approximation.

In this example, a sample with a magnetic core-shell particle is constructed.
Beam and detector are setup to detect the spin-flip scattering channel and the
result of this simulation is plotted as usual.

{{< figscg src="/img/auto/scatter2d/PolarizedSANS.png" width="600px" >}}

{{< show-ex file="scatter2d/PolarizedSANS.py" >}}
