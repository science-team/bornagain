+++
title = "with specular peak"
weight = 18
+++

## Option: include specular reflection

Only if option [include specular](/ref/sim/setup/options/specular)
is set, simulated detector images will include the specular reflection
of the incident beam.

Example: scattering by dilute boxes on a substrate.

{{< figscg src="/img/auto/scatter2d/BoxesWithSpecularPeak.png" width="500px" class="center" >}}

{{< show-ex file="scatter2d/BoxesWithSpecularPeak.py" >}}
