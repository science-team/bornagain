+++
title = "Specular"
weight = 10
+++

## Specular reflectometry simulation

To set up and run a specular reflectometry simulation, use
```python
import bornagain as ba
scan = ...
sample = ...
simulation = ba.SpecularSimulation(scan, sample)
# ... set options
result = simulation.simulate()
```

Supported scan types: grazing angle scan, wavelength scan, wavenumber qz scan.

For the constructor arguments, see sections [scan](/ref/instr/scan)
and [sample](/ref/sample).

For optional settings, see [simulation options](/ref/sim/setup/options).

For the return type of function `simulate()`,
see [Datafield](/ref/data/datafield).


##### Examples

* [basic specular simulation](ex)
* [specular signal from a rough sample](/ref/sample/roughness/specular_default)
* [real-life fitting: Pt layer](/ref/fit/ex/reflectometry-pt-layer)
* [real-life fitting: magnetic lattice](/ref/fit/ex/reflectometry-honeycomb)

##### Further reading

* [beam footprint correction](/ref/instr/beam/footprint)
* [beam divergence in specular simulations](/ref/instr/beam/divergence)
* [basic roughness tutorial](/ref/sample/roughness)
