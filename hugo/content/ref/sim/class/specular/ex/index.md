+++
title = "Example"
weight = 10
+++

## Basic specular reflectometry example

As introductory example, we simulate specular reflectometry
by a sample that consists of 10 Ti/Ni double layers on a Si substrate.

##### Full script

The same script has been used in the tutorial to explain [usage](/py/run)
and [syntax](/py/syntax) of BornAgain Python code.

Root class reference: [SpecularSimulation](/ref/sim/class/specular).

{{< figscg src="/img/auto/specular/AlternatingLayers1.png" width="500" class="center">}}

{{< show-ex file="specular/AlternatingLayers1.py">}}


##### Shorthand with standard sample

Since we will use the same sample in several other examples,
we provide the shorthand `std_samples.alternating_layers()`
so that the above script can be shortened as

{{< show-ex file="specular/AlternatingLayers2.py">}}
