+++
title = "Specular"
weight = 10
+++

## Specular reflectometry simulation

To set up and run a specular reflectometry simulation, use
```python
import bornagain as ba
scan = ...
sample = ...
simulation = ba.SpecularSimulation(scan, sample)
# ... set options
result = simulation.simulate()
```

For the constructor arguments, see sections [scan](/ref/instr/scan)
and [sample](/ref/sample).

For optional settings, see [simulation options](/ref/sim/setup/options).

For the return type of function `simulate()`,
see [Datafield](/ref/result/datafield).


##### Examples

* [basic specular simulation](/ex/sim/specular)
* [specular signal from a rough sample](/ex/sample/roughness/specular_default)
* [real-life fitting: Pt layer](/ex/fit/reflectometry-pt-layer)
* [real-life fitting: magnetic lattice](/ex/fit/reflectometry-honeycomb)

##### Further reading

* [beam footprint correction](/ref/instr/beam/footprint)
* [beam divergence in specular simulations](/ref/instr/beam/divergence)
* [basic roughness tutorial](/ref/sample/roughness)
