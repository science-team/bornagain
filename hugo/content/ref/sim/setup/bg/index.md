+++
title = "Background"
weight = 80
+++

### Poisson background

To add a Poisson background to a `Simulation` instance, use

```python
bg = ba.PoissonBackground()
simulation.setBackground(bg)
```

In this case, the output intensity is randomly distributed around the exact value with discrete Poisson statistics.

The lower the intensity of the probing beam, the lower the signal-to-noise ratio.

### Constant background

To add a constant background to a `Simulation` instance, use

```python
bg = ba.ConstantBackground(1e3)
simulation.setBackground(bg)
```

Background is applied before the [specular simulation](/ref/sim/class/specular) is normalized, so the threshold value will also be divided by the probe beam intensity. 

{{< galleryscg >}}
{{< figscg src="/img/auto/scatter2d/ConstantBackground.png" width="450px" caption="Intensity image">}}
{{< /galleryscg >}}

The script below shows how to add a constant background in the context of a GISAXS simulation of an air-substrate bilayer with cylindrical particles embedded.

{{< show-ex file="scatter2d/ConstantBackground.py" >}}
