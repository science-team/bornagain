+++
title = "Options"
weight = 80
+++

## Simulation options

The function `simulation.options()` gives access to a
SimulationOptions object. This object can be modified
through the following setter functions:

- [setMonteCarloIntegration(_switch_, _n_)](mc)
- [setNumberOfThreads(_n_)](threads)
- [setIncludeSpecular(_switch_)](specular)
- [setUseAvgMaterials(_switch_)](avgmat)
