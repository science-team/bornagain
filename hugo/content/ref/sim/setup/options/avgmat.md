+++
title = "Average materials"
weight = 40
+++

## Option: use average materials

With the setting
```
simulation.options().setUseAvgMaterials(True)
```
the refractive properties of material layers are computed
by taking the average of the matrix material and the embedded particles.
