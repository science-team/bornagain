+++
title = "Monte-Carlo"
weight = 10
+++

## Option: Monte-Carlo integration

If for some reason the midpoint of a detector pixel is not representative
for the average scattering intensity of the entire pixel it is possible
to resort to Monte-Carlo integration.

To request Monte-Carlo integration over detector pixels, use
```python
simulation.options().setMonteCarloIntegration(True, n)
```
where `n` is the number of scattering intensity evaluations per pixel.

Example: [Monte-Carlo](/ref/sim/class/scattering/ex/mc).