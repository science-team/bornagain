+++
title = "Include specular"
weight = 40
+++

## Option: include specular reflection

To include the specular reflected beam intensity
along with the scattered intensity in a GISAS simulation, use
```
simulation.options().setIncludeSpecular(True)
```

Example: [GISAS with specular peak](/ref/sim/class/specular/ex).
