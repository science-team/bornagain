+++
title = "# threads"
weight = 20
+++

## Option: number of threads

Multithreading can be controlled through
```python
simulation.options().setNumberOfThreads(n)
```
where `n` is the number of threads to be used during the simulation.

If this option is not set, or set to `n=0`, then the default value
will be taken from the hardware.