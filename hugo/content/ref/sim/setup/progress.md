+++
title = "Progress monitor"
weight = 81
+++

### Terminal progress monitor

To monitor progress of a time-consuming simulation, use
```python
simulation.setTerminalProgressMonitor()
simulation.simulate()
```

While the simulation is running, progress will be printed in the form
```
... 17%
```

To prevent problems if the script is run in batch mode,
use the conditional
```python
if not "__no_terminal__" in globals():
    simulation.setTerminalProgressMonitor()
```

This is used for example in

{{< show-ex file="scatter2d/FindPeaks.py" >}}
