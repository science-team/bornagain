This directory contains the the unconfigured source version
of the Python scripting examples, for developer use only.

The public version of the examples has been moved to directory
auto/Examples.

Another version, with mini detectors for use in automatized
testing, is in directory auto/MiniExamples.

These versions are generated at compile time by command 'make'.

The source scripts in the present directory contain code
snippets like "<%= test_mode ? 11 : 200 %>". They contain ruby code,
which is executed by the embedded ruby interpreter erb,
as programmed in CMakeLists.txt.
