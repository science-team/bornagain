#!/usr/bin/env python3
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, std_samples, std_simulations

material_particle = ba.RefractiveMaterial("Particle", 1e-6, 0)
ff = ba.Cylinder(3.6*nm, 7.2*nm)


def get_sample(omega):
    particle = ba.Particle(material_particle, ff)
    particle.rotate(ba.RotationY(omega*deg))
    return std_samples.sas_sample_with_particle(particle)


def get_simulation(sample):
    n = <%= test_mode ? 11 : 201 %>
    return std_simulations.sas(sample, n)


if __name__ == '__main__':
    results = []
    for omega in [0, 45, 90]:
        title = r'$\theta=%d^\circ$' % omega
        sample = get_sample(omega)
        simulation = get_simulation(sample)
        result = simulation.simulate()
        result.setTitle(title)
        results.append(result)

    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.plot_to_row(results, **plotargs)
    bp.export(**plotargs)
    <%- else -%>
    bp.plot_to_row(results)
    bp.plt.show()
    <%- end -%>
