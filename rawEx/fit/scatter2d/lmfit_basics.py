#!/usr/bin/env python3
"""
External minimize: using lmfit minimizers for BornAgain fits.
"""
import bornagain as ba
from bornagain import nm
import lmfit
import model2_hexlattice as model


if __name__ == '__main__':
    data = model.fake_data()

    fit_objective = ba.FitObjective()
    fit_objective.addFitPair(model.get_simulation, data, 1)
    fit_objective.initPrint(10)

    P = lmfit.Parameters()
    P.add('radius', value=7*nm, min=5*nm, max=8*nm)
    P.add('length', value=10*nm, min=8*nm, max=14*nm)

    result = lmfit.minimize(fit_objective.evaluate_residuals, P)
    fit_objective.finalize(result)

    print(result.params.pretty_print())
    print(lmfit.fit_report(result))
