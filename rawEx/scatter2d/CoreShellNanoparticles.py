#!/usr/bin/env python3
"""
Core shell nanoparticles
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, std_samples


def get_sample():
    """
    A sample with box-shaped core-shell particles.
    """

    # Materials
    material_Core = ba.RefractiveMaterial("Core", 6e-5, 2e-8)
    material_Shell = ba.RefractiveMaterial("Shell", 1e-4, 2e-8)

    # Form factors
    ff_1 = ba.Box(12*nm, 12*nm, 7*nm)
    ff_2 = ba.Box(16*nm, 16*nm, 8*nm)

    # Particles
    core = ba.Particle(material_Core, ff_1)
    shell = ba.Particle(material_Shell, ff_2)
    particle = ba.CoreAndShell(core, shell)

    return std_samples.sas_sample_with_particle(particle)


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = <%= test_mode ? 11 : 200 %>
    detector = ba.SphericalDetector(n, -1*deg, 1*deg, n, 0., 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
    <%- else -%>
    bp.plot_datafield(result)
    bp.plt.show()
    <%- end -%>
