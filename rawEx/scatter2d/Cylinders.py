#!/usr/bin/env python3
"""
Basic example of a DWBA simulation of a GISAS experiment.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm


def get_sample():
    """
    Standard sample model: dilute random assembly of cylinders on a substrate.
    """
    from bornagain import std_samples
    return std_samples.cylinders()


def get_simulation(sample):
    # Beam
    wavelength = 0.1*nm
    alpha_i = 0.2*deg
    beam = ba.Beam(1e9, wavelength, alpha_i)

    # Detector
    n = <%= test_mode ? 11 : 200 %>
    detector = ba.SphericalDetector(n, -2*deg, 2*deg, n, 0., 3*deg)

    return ba.ScatteringSimulation(beam, sample, detector)


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
    <%- else -%>
    bp.plot_datafield(result)
    bp.plt.show()
    <%- end -%>
