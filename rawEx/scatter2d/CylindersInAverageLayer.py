#!/usr/bin/env python3
"""
Square lattice of cylinders inside finite layer with usage of average material
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, R3


def get_sample(cyl_height=5*nm):
    """
    A sample with cylinders on a substrate.
    """
    # defining materials
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_layer = ba.RefractiveMaterial("Layer", 3e-6, 2e-8)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-6, 2e-8)
    material_particle = ba.RefractiveMaterial("Particle", 3e-5, 2e-8)

    # cylindrical particle
    cylinder_ff = ba.Cylinder(5*nm, cyl_height)
    cylinder = ba.Particle(material_particle, cylinder_ff)
    cylinder.translate(R3(0, 0, -cyl_height))
    particle_layout = ba.ParticleLayout()
    particle_layout.addParticle(cylinder)

    # interference function
    interference = ba.Interference2DLattice(ba.SquareLattice2D(15*nm, 0))
    pdf = ba.Profile2DCauchy(300*nm, 300*nm, 0)
    interference.setDecayFunction(pdf)
    particle_layout.setInterference(interference)

    vacuum_layer = ba.Layer(vacuum)
    intermediate_layer = ba.Layer(material_layer, 5*nm)
    intermediate_layer.addLayout(particle_layout)
    substrate_layer = ba.Layer(material_substrate)

    sample = ba.Sample()
    sample.addLayer(vacuum_layer)
    sample.addLayer(intermediate_layer)
    sample.addLayer(substrate_layer)
    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = <%= test_mode ? 11 : 100 %>
    detector = ba.SphericalDetector(n, -2*deg, 2*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    simulation.options().setUseAvgMaterials(True)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
    <%- else -%>
    bp.plot_datafield(result)
    bp.plt.show()
    <%- end -%>
