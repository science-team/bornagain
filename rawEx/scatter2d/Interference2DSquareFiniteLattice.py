#!/usr/bin/env python3
"""
Cylinders on a 2D square lattice
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, nm2


def get_sample():
    """
    A sample with cylinders on a substrate, forming a 2D square lattice.
    """

    # Materials
    material_particle = ba.RefractiveMaterial("Particle", 0.0006, 2e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Form factors
    ff = ba.Cylinder(3*nm, 3*nm)

    # Particles
    particle = ba.Particle(material_particle, ff)

    # 2D lattices
    lattice = ba.BasicLattice2D(25*nm, 25*nm, 90*deg, 0)

    # Interference functions
    iff = ba.InterferenceFinite2DLattice(lattice, 40, 40)
    iff.setPositionVariance(1*nm2)

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(particle)
    layout.setInterference(iff)
    layout.setTotalParticleSurfaceDensity(0.0016)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = <%= test_mode ? 11 : 200 %>
    detector = ba.SphericalDetector(n, -2*deg, 2*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
    <%- else -%>
    bp.plot_datafield(result)
    bp.plt.show()
    <%- end -%>
