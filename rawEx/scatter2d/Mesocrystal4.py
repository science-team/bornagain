#!/usr/bin/env python3
"""
Cylindrical mesocrystal with hexagonal lattice of soft spheres on a substrate.
Basis made of two different materials.
With slicing.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm, R3


def get_sample():
    # Materials
    material_particle1 = ba.RefractiveMaterial("Particle1", 0.0004, 3e-08)
    material_particle2 = ba.RefractiveMaterial("Particle2", 0.0008, 5e-08)
    material_substrate = ba.RefractiveMaterial("Substrate", 6e-06, 2e-08)
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)

    # Particles
    inner_ff = ba.FuzzySphere(4.7*nm, .4*nm)

    inner_particle1 = ba.Particle(material_particle1, inner_ff)
    inner_particle2 = ba.Particle(material_particle2, inner_ff)

    # 3D lattice
    lattice_a = 6.21
    lattice_c = 6.57
    sigma_a = 1.16
    position_variance = sigma_a**2/3

    lattice = ba.HexagonalLattice(lattice_a*2, lattice_c*2*2.3)
    lattice.setSelectionRule(ba.SimpleSelectionRule(-1, 1, 1, 3))

    bas_a = lattice.basisVectorA()
    bas_b = lattice.basisVectorB()
    bas_c = lattice.basisVectorC()

    position_0 = R3(0, 0, 0)
    position_1 = 1.0/3*(2*bas_a + bas_b + bas_c)
    basis = ba.Compound()
    basis.addComponent(inner_particle1, position_0)
    basis.addComponent(inner_particle2, position_1)

    # Crystals
    crystal = ba.Crystal(basis, lattice, position_variance)

    # Mesocrystals
    outer_ff = ba.Cylinder(20*nm, 50*nm)
    outer_particle = ba.Mesocrystal(crystal, outer_ff)

    # Particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(outer_particle, 1)
    layout.setTotalParticleSurfaceDensity(0.1)

    # Layers
    layer_1 = ba.Layer(vacuum)
    layer_1.addLayout(layout)
    layer_1.setNumberOfSlices(10)
    layer_2 = ba.Layer(material_substrate)

    # Sample
    sample = ba.Sample()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1e9, 0.1*nm, 0.2*deg)
    n = <%= test_mode ? 11 : 200 %>
    detector = ba.SphericalDetector(n, -2*deg, 2*deg, n, 0, 2*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    simulation.options().setUseAvgMaterials(True)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
    <%- else -%>
    bp.plot_datafield(result)
    bp.plt.show()
    <%- end -%>
