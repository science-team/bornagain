#!/usr/bin/env python3
"""
Simulation of grating using very long boxes and 1D lattice.
Monte-carlo integration is used to get rid of
large-particle form factor oscillations.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, micrometer, nm


def get_sample(lattice_rotation_angle=0*deg):
    """
    A sample with a grating on a substrate.
    lattice_rotation_angle = 0 - beam parallel to grating lines
    lattice_rotation_angle = 90*deg - beam perpendicular to grating lines
    """
    # Materials
    vacuum = ba.RefractiveMaterial("Vacuum", 0, 0)
    material_si = ba.RefractiveMaterial("Si", 5.7816e-6, 1.0229e-7)

    box_length, box_width, box_height = 50*micrometer, 70*nm, 50*nm
    lattice_length = 150*nm

    # Particle layout
    interference = ba.Interference1DLattice(
        lattice_length, 90*deg - lattice_rotation_angle)

    pdf = ba.Profile1DGauss(450)
    interference.setDecayFunction(pdf)

    box_ff = ba.LongBoxLorentz(box_length, box_width, box_height)
    box = ba.Particle(material_si, box_ff)
    box.rotate(ba.RotationZ(lattice_rotation_angle))
    particle_layout = ba.ParticleLayout()
    particle_layout.addParticle(box)
    particle_layout.setInterference(interference)

    sigma, hurst, corrLength = 5*nm, 0.5, 10*nm
    autocorr = ba.SelfAffineFractalModel(sigma, hurst, corrLength)
    transient = ba.TanhTransient()
    roughness = ba.Roughness(autocorr, transient)

    # Sample
    vacuum_layer = ba.Layer(vacuum)
    vacuum_layer.addLayout(particle_layout)
    substrate_layer = ba.Layer(material_si, roughness)

    sample = ba.Sample()
    sample.addLayer(vacuum_layer)
    sample.addLayer(substrate_layer)
    return sample


def get_simulation(sample):
    beam = ba.Beam(1e8, 0.134*nm, 0.4*deg)
    n = <%= test_mode ? 51 : 200 %>
    detector = ba.SphericalDetector(n, -0.5*deg, 0.5*deg, n, 0, 0.5*deg)
    simulation = ba.ScatteringSimulation(beam, sample, detector)
    simulation.options().setMonteCarloIntegration(True, <%= test_mode ? 1000 : 100 %>)
    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.plot_datafield(result, **plotargs)
    bp.export(**plotargs)
    <%- else -%>
    bp.plot_datafield(result)
    bp.plt.show()
    <%- end -%>
