#!/usr/bin/env python3
"""
Reflectivity of a multilayer with different wavelength distributions.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, std_samples


lambda0 = 0.154

def get_sample():
    return std_samples.alternating_layers()


def simulate(sample, distr, title):
    n = 300
    scan = ba.AlphaScan(n, 0.5*deg, 1*deg)
    scan.setWavelength(lambda0)
    if distr:
        scan.setWavelengthDistribution(distr)

    result = ba.SpecularSimulation(scan, sample).simulate()
    result.setTitle(title)
    return result


if __name__ == '__main__':
    sample = get_sample()

    results = [
        simulate(sample, ba.DistributionLorentz(
            lambda0, 0.01177 * lambda0), "Lorentz"),
        simulate(sample, ba.DistributionGaussian(
            lambda0, 0.01 * lambda0), "Gauss"),
        simulate(sample, ba.DistributionLogNormal(
            lambda0, 0.011), "LogNormal"),
    ]

    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.plot_multicurve(results, **plotargs)
    bp.export(**plotargs)
    <%- else -%>
    bp.plot_multicurve(results)
    bp.plt.show()
    <%- end -%>
