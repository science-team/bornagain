#!/usr/bin/env python3
"""
Reflectivity of a multilayer with different widths of the
Gaussian distribution of incident angles alpha_i.
"""
import numpy as np, os, sys
import bornagain as ba
from bornagain import ba_plot as bp, deg, std_samples


def get_sample():
    return std_samples.alternating_layers()


def simulate(sample, alpha_width, title):
    n = 200
    scan = ba.AlphaScan(n, 2*deg/n, 2*deg)
    scan.setWavelength(0.154)
    scan.setGrazingAngleDistribution(ba.DistributionGaussian(0, alpha_width, 25, 3.))

    result = ba.SpecularSimulation(scan, sample).simulate()
    result.setTitle(title)
    return result


if __name__ == '__main__':
    sample = get_sample()

    results = [
        simulate(sample, 0, "0 deg"),
        simulate(sample, 0.003 * deg, "0.003 deg"),
        simulate(sample, 0.01 * deg, "0.01 deg"),
        simulate(sample, 0.03 * deg, "0.03 deg"),
    ]
    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.plot_multicurve(results, **plotargs)
    bp.export(**plotargs)
    <%- else -%>
    bp.plot_multicurve(results)
    bp.plt.show()
    <%- end -%>
