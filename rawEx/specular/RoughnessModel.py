#!/usr/bin/env python3
"""
Example of simulating a rough sample with a
tanh and Nevot-Croce roughness model using BornAgain.

"""
import bornagain as ba
from bornagain import angstrom, nm, ba_plot as bp, deg


def get_sample(transient):
    # create materials
    vacuum = ba.MaterialBySLD("Vacuum", 0, 0)
    material_ti = ba.MaterialBySLD("Ti", -1.9493e-06, 0)
    material_ni = ba.MaterialBySLD("Ni", 9.4245e-06, 0)
    material_substrate = ba.MaterialBySLD("SiSubstrate", 2.0704e-06, 0)

    # Roughness
    autocorr = ba.SelfAffineFractalModel(1*nm, 0.7, 25*nm)
    roughness = ba.Roughness(autocorr, transient)

    # create layers
    ambient_layer = ba.Layer(vacuum)
    ti_layer = ba.Layer(material_ti, 3*nm, roughness)
    ni_layer = ba.Layer(material_ni, 7*nm, roughness)
    substrate_layer = ba.Layer(material_substrate, roughness)

    # create periodic stack
    n_repetitions = 10
    stack = ba.LayerStack(n_repetitions)
    stack.addLayer(ti_layer)
    stack.addLayer(ni_layer)
    
    # create sample
    sample = ba.Sample()
    sample.addLayer(ambient_layer)
    sample.addStack(stack)
    sample.addLayer(substrate_layer)
    
    return sample


def get_simulation(sample):
    n = <%= test_mode ? 50 : 500 %>
    scan = ba.AlphaScan(n, 2*deg/n, 2*deg)
    scan.setWavelength(1.54*angstrom)

    return ba.SpecularSimulation(scan, sample)


def simulate(roughness_model, title):
    """
    Runs simulation and returns its result.
    """
    sample = get_sample(roughness_model)
    simulation = get_simulation(sample)
    result = simulation.simulate()
    result.setTitle(title)
    return result


if __name__ == '__main__':
    results = [
        simulate(ba.ErfTransient(), "Névot-Croce"),
        simulate(ba.TanhTransient(), "Tanh"),
    ]
    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.plot_multicurve(results, **plotargs)
    bp.export(**plotargs)
    <%- else -%>
    bp.plot_multicurve(results)
    bp.plt.show()
    <%- end -%>
