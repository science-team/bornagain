#!/usr/bin/env python3
"""
Plot an SLD profile for a sample with a sliced particle layer.
"""

import bornagain as ba
from bornagain import angstrom, ba_plot as bp, deg, nm, sample_tools
import numpy as np


def get_sample():
    # materials
    vacuum = ba.MaterialBySLD("Vacuum", 0, 0)
    material_particle = ba.MaterialBySLD("Particle", 5e-6, 0)
    material_substrate = ba.MaterialBySLD("SiSubstrate", 2.0704e-06, 0)

    # layers
    ambient_layer = ba.Layer(vacuum)
    substrate_layer = ba.Layer(material_substrate)

    # particle layout
    ff = ba.Cone(5*nm, 10*nm, 75*deg)
    particle = ba.Particle(material_particle, ff)
    layout = ba.ParticleLayout()
    layout.addParticle(particle)
    iff = ba.Interference2DLattice(ba.SquareLattice2D(10*nm, 0))
    layout.setInterference(iff)
    ambient_layer.addLayout(layout)
    ambient_layer.setNumberOfSlices(20)

    # sample
    sample = ba.Sample()
    sample.addLayer(ambient_layer)
    sample.addLayer(substrate_layer)

    return sample


if __name__ == '__main__':
    sample = get_sample()
    zpoints, slds = sample_tools.materialProfile(sample, 400)

    bp.plt.figure()
    bp.plt.plot(zpoints, np.real(slds))
    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    bp.export(**plotargs)
    <%- else -%>
    bp.plt.show()
    <%- end -%>
