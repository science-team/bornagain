#!/usr/bin/env python3
"""
Depth profile of an opaque sample.
"""
import bornagain as ba
from bornagain import ba_plot as bp, deg, nm

#  beam data
ai_min = 0  # minimum incident angle
ai_max = 1*deg  # maximum incident angle
wl = 0.03*nm  # wavelength

#  depth position span
z_min = -400*nm
z_max = 100*nm


def get_sample():
    # Define materials
    material_vac = ba.RefractiveMaterial("Vac", 0, 0)
    material_A = ba.RefractiveMaterial("A", 1e-5, 3e-6)

    # Define layers
    layer_top = ba.Layer(material_vac)
    layer_1 = ba.Layer(material_A, 300*nm)
    layer_bot = ba.Layer(material_vac)

    # Define sample
    sample = ba.Sample()
    sample.addLayer(layer_top)
    sample.addLayer(layer_1)
    sample.addLayer(layer_bot)

    return sample


def get_simulation(sample):
    """
    Returns a depth-probe simulation.
    """
    nz = 500
    na = 10*nz

    scan = ba.AlphaScan(na, ai_min, ai_max)
    scan.setWavelength(wl)
    footprint = ba.FootprintSquare(0.01)
    scan.setFootprint(footprint)

    z_axis = ba.EquiDivision("z", nz, z_min, z_max)
    simulation = ba.DepthprobeSimulation(scan, sample, z_axis)

    return simulation


if __name__ == '__main__':
    sample = get_sample()
    simulation = get_simulation(sample)
    result = simulation.simulate()
    <%- if test_mode or figure_mode -%>
    plotargs = bp.parse_commandline()
    <%- else -%>
    plotargs = {}
    <%- end -%>
    plotargs['aspect'] = 'auto'
    plotargs['intensity_min'] = 1e-200
    plotargs['intensity_max'] = 1
    bp.plot_datafield(result, **plotargs)
    <%- if test_mode or figure_mode -%>
    bp.export(**plotargs)
    <%- else -%>
    bp.plt.show()
    <%- end -%>
