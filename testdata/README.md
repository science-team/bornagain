## Data examples

The example data in this directory, kindly provided by various colleagues,
are used in our script examples to demonstrate how to apply BornAgain
to experimental data.


### Scattering2D

#### Galaxi

3 layers system (substrate, teflon, vacuum).
Vacuum layer is populated with spheres with size distribution.
Measurement on GALAXI at FZ Jülich (M. Ganeva, unpublished).


#### faked_gisas1

Created by devtools/fakeData/fake-gisas1.py


#### SANSDRaw

Kindly provided by Apostolos Vagias.

Written by Nicos?

Many metadata, then comma and newline separated table of integer raw counts.


### Specular

#### genx_*

Simulated with GenX; creating script probably lost.

Column 1 is the double incident angle in degrees.
Column 2 is the simulated intensity.


#### honeycomb*

Experimental data by A. Glavic et al,
https://doi.org/10.1002/advs.201700856

Column 1 is qz, apparently in 1/angstrom.
Column 2 the intensity from the Genx fit.
Column 3 the experimental intensity.
Column 4 the error of column 3.
Column 5 the q resolution.


#### ILL-*

Experimental data from D17, D50, FIGARO, processed through Motofit,
kindly provided by Philipp Gutfreund and Thomas Saerbeck.

Column 1 is qz, apparently in 1/nm.
Column 2 the intensity.
Column 3 the error.
Column 4 the q resolution fwhm.


#### MAFO-Saturated

From https://www.nist.gov/document/spinelfilmzip,
extracted by devtools/expData/xtractNIST.py

Column 1 seems to be qz in 1/nm.
Columns 2-4 seem to be standard: I, dI, dq.


#### MLZ-TREFF-Ni58

Experimental scan from TREFF instrument (https://doi.org/10.17815/jlsrf-3-161)
Provided by Egor Vezhlev.

Column 1 is q in 1/angstrom.

Sample: Ni_58 film on a SiO2 wafer.
Wavelength: 4.73 angstrom

Slit 0.2 mm
Distance between the collimation slits: 1840mm --> angular divergence 0.006 deg

Ni_58 layer: SLD: Re=9.408e-6 A^-2, Im=0
SiO2 substrate: SLD: Re=2.0704e-6 A^-2, Im=0


#### RvsQ_36563_36662.txt.gz

Pt layer on Si substrate, experimental data by M. Fitzsimmons et al.,
published in https://doi.org/10.5281/zenodo.4072376

Column 1 is qz, apparently in 1/angstrom.
Columns 2-4 seem to be standard: I, dI, dq.
