The test images for fabio package are obtained from:

Karthik S, Mandal PK, Thirugnanasambandam A, & Gautham N. (2019).
Crystal structures of disordered Z-type helices [Data set].
In Nucleosides, Nucleotides and Nucleic Acids.
Zenodo. https://doi.org/10.5281/zenodo.2546760
